import { configure, setAddon, addDecorator } from '@storybook/react';
import { setOptions } from '@storybook/addon-options'
import centered from '@storybook/addon-centered';

import JSXAddon from 'storybook-addon-jsx';

setAddon(JSXAddon);
addDecorator(centered);

const req = require.context('../src/stories/', true, /stories\.jsx$/)

function loadStories() {
  req.keys().forEach(req)
}

setOptions({
  name: 'MODULES LIBRARY',
  goFullScreen: false,
  showStoriesPanel: true,
  showAddonPanel: true,
  showSearchBox: false,
  addonPanelInRight: true,
  sortStoriesByKind: false,
});

configure(loadStories, module);