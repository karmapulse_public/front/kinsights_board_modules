import React, { Component } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';

import Wordcloud from './visualizations/WordCloudTrends';
import ListTopTrends from './visualizations/ListTopTrends';
import fetchTopTT from './FbTTCommentsDataFlow';
import LoadingModule from '../LoadingModule';

const propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    view: PropTypes.string,
    services: PropTypes.object,
    dateRange: PropTypes.object
};

const defaultProps = {
    fields: {},
    side: 'left',
    color: '#666',
    services: {},
    dateRange: {},
    view: ''
};

class FbTopTopicsComments extends Component {
    constructor(props) {
        super(props);
        this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color,
            moduleView: props.view,
        };
    }

    componentDidMount() {
        this.fetchData(this.props);
    }

    componentWillReceiveProps(nextProps) {
        this.fetchData(nextProps);
    }

    fetchData(newProps) {
        const { startDate, endDate } = newProps.dateRange;
        const { fields: { search_id } } = newProps;
        const params = {
            recipe_id: 'module_fb_top_phrases_in_comments',
            rule_id: search_id,
            initial_date: moment(startDate).utc().toISOString(),
            final_date: moment(endDate).utc().toISOString(),
            filters: {
                top: newProps.fields.top_limit
            }
        };

        fetchTopTT(this.props.services.facebook, params)
            .subscribe(
                (data) => {
                    this.setState({
                        moduleData: data,
                        moduleConfig: newProps.fields,
                        moduleLoading: false,
                        moduleColor: newProps.color,
                        moduleSide: newProps.side,
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    renderByViz() {
        const { moduleData, moduleConfig } = this.state;
        if (moduleData) {
            const visualizations = {
                wordcloud_top_trends: params => (
                    <Wordcloud {...params} />
                ),
                list_top_trends: params => (
                    <ListTopTrends {...params} />
                ),
            };
            const v = this.state.moduleView !== '' ? this.state.moduleView : moduleConfig.visualization;
            return visualizations[v](this.state);
        }
        return (<LoadingModule />);
    }
    render() {
        return (
            <div className="m-fb-top-topics-comments">
                {this.renderByViz()}
            </div>
        );
    }
}

FbTopTopicsComments.propTypes = propTypes;
FbTopTopicsComments.defaultProps = defaultProps;

export default FbTopTopicsComments;
