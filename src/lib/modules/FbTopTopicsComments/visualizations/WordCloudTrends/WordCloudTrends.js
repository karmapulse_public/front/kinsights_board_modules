import React from 'react';
import PropTypes from 'prop-types';
import styles from './wordCloudTrendsStyles';
import WordCloud from '../../../Charts/WordCloud';
import { numberWithCommas } from '../../../../helpers/number';
import { toPostOrder, evalAritmetic } from '../../../../helpers/resolveAritmetic';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.array,
    moduleColor: PropTypes.string,
};

const defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleColor: '#666',
};

const WordCloudTrends = ({
    moduleConfig, moduleColor, moduleData
}) => {
    const colors = moduleConfig.fontColor || ['#5979b8'];
    const width = moduleConfig.width || 400;
    const height = moduleConfig.height || 400;
    const { values } = moduleData;
    const maxAngle = moduleConfig.maxAngle || 0;
    const minAngle = moduleConfig.minAngle || 0;
    const outlineFn = moduleConfig.outlineFn || '0';
    const outlineColor = moduleConfig.outlineColor || '#5979b8';
    const outlineNumbers = moduleConfig.outlineFn ? (() => {
        const array = [];
        const postOutlineFn = toPostOrder(outlineFn);
        for (let n = 1; n <= values.length; n += 1) {
            array.push(evalAritmetic(postOutlineFn, n));
        }
        return array;
    })() : [];
    const colorScale = (elem, index, chooseColor) => {
        if (outlineNumbers.includes(index)) {
            return outlineColor;
        }
        return chooseColor();
    };

    const renderTitle = () => {
        if (moduleConfig.show_title) {
            const { title = 'RELACIÓN DE TEMAS' } = moduleConfig;
            return (
                <div className="wordcloud__label">
                    <h3>{title}</h3>
                </div>
            );
        }
        return '';
    };

    return (
        <div className="wordcloud-top-trends" {...styles(moduleColor)}>
            {renderTitle()}
            <div className="wordcloud__chart">
                <div>
                    <WordCloud
                        colors={colors}
                        wordCountKey="total"
                        wordKey="theme"
                        fontFamily="Roboto"
                        enableTooltip
                        height={height}
                        maxWords={300}
                        width={width}
                        words={values}
                        maxAngle={maxAngle}
                        minAngle={minAngle}
                        colorScale={colorScale}
                        tooltip={data => (
                            <div className="tooltip-example">
                                <p>{data.text}</p>
                                <p>{numberWithCommas(data.total)} menciones</p>
                            </div>
                        )}
                    />
                </div>
            </div>
        </div>
    );
};

WordCloudTrends.propTypes = propTypes;
WordCloudTrends.defaultProps = defaultProps;

export default WordCloudTrends;
