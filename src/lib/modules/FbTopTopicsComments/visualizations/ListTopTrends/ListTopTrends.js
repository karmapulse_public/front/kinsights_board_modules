import React from 'react';
import PropTypes from 'prop-types';
import maxBy from 'lodash/maxBy';
import { css } from 'glamor';

import styles from './listTopTrendsStyles';
import NoDataModule from '../../../NoDataModule';
import { numberWithCommas } from '../../../../helpers/number';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.array,
    moduleColor: PropTypes.string,
    moduleSide: PropTypes.string,
    showColors: PropTypes.bool,
};

const defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleColor: '#666',
    moduleSide: 'right',
    showColors: false,
};

const ListTopTrends = ({
    moduleConfig, moduleData, moduleColor,
    moduleSide, showColors
}) => {
    const colors = ['#c75a93', '#8176cc', '#cc5f43', '#5ba966', '#54a2d3'];
    const softColors = [
        'rgba(199,89,147,0.21)',
        'rgba(129,117,204,0.21)',
        'rgba(204,95,66,0.21)',
        'rgba(91,169,101,0.21)',
        'rgba(84,161,211,0.21)'
    ];

    const maxValue = maxBy(moduleData.values, 'total');
    const dataFormat = moduleData.values.map((item, index) => (
        {
            id: index + 1,
            subject: item.theme,
            total: item.total,
            fullMark: maxValue.total
        }
    ));

    const renderTitle = () => {
        if (moduleConfig.show_title) {
            const { title = 'RELACIÓN DE TEMAS' } = moduleConfig;
            return (
                <div className="list__label">
                    <h3>{title}</h3>
                </div>
            );
        }
        return '';
    };

    const showColor = (index, { subject, total, id }) => {
        const dotColor = css({
            backgroundColor: softColors[index],
            border: `1px solid ${colors[index]}`
        });
        const value = subject.length < 35 ? subject : subject.substring(0, 35).concat('...');

        if (showColors) {
            return (
                <li // eslint-disable-line
                    className="list__item"
                    key={`${value}${index}`}
                >
                    <div className="list__dot" {...dotColor} />
                    <h4 className="list__name">{value}</h4>
                    <h5 className="list__value" style={{ color: colors[index] }}>
                        {numberWithCommas(total)}
                    </h5>
                </li>
            );
        }
        return (
            <li // eslint-disable-line
                className="list__item"
                key={`${value}${index}`}
            >
                <h5 className="index">{id}.</h5>
                <h4 className="list__name">{value}</h4>
                <h5 className="list__value">
                    {numberWithCommas(total)}
                </h5>
            </li>
        );
    };

    const renderItems = () => (
        dataFormat.map((item, index) => showColor(index, item))
    );

    const renderSubtitles = () => {
        const { subtitle } = moduleConfig;
        const list = subtitle ? subtitle.list : 'Listado';
        const count = subtitle ? subtitle.count : 'Palabras';
        return (
            <div className="list-top-titles">
                <h5>{list}</h5>
                <h5>{count}</h5>
            </div>
        );
    };

    return (
        <div className="list-top-trends" {...styles(moduleColor, moduleSide)}>
            {renderTitle()}
            {renderSubtitles()}
            {
                dataFormat.length === 0 ?
                    <NoDataModule /> :
                    <ul>
                        {renderItems()}
                    </ul>
            }
        </div>
    );
};

ListTopTrends.propTypes = propTypes;
ListTopTrends.defaultProps = defaultProps;

export default ListTopTrends;
