import React from 'react';
import PropTypes from 'prop-types';
import { PieChart, Pie, Cell } from 'recharts';

import { numberWithCommas } from '../../../../helpers/number';
import TwitterIcon from '../../../../helpers/icons/twitterIcon';
import styles from './PieChartRemasterStyles';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleSide: PropTypes.string,
    moduleColor: PropTypes.string
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleSide: 'left',
    moduleColor: '#666'
};

const PieChartRemaster = ({ moduleConfig, moduleData, moduleSide, moduleColor }) => {
    const colors = {
        positivo: '#63d12f',
        negativo: '#ea3c3c',
        neutral: '#89a7ce'
    };

    const formatLabels = moduleData.values.map((item) => {
        if (moduleConfig.sentiment_labels.hasOwnProperty(item.key)) {
            return {
                key: item.key,
                name: moduleConfig.sentiment_labels[item.key],
                value: item.doc_count,
                percentage: ((item.doc_count * 100) / moduleData.total).toFixed(1).replace(/\.0$/, '')
            };
        }
        return {
            key: item.key,
            name: item.key.charAt(0).toUpperCase() + item.key.slice(1),
            value: item.doc_count,
            percentage: ((item.doc_count * 100) / moduleData.total).toFixed(1).replace(/\.0$/, '')
        };
    });

    const renderLabels = () => formatLabels.map((type, index) => (
        <div className={`pie-chart__percentages__${type.key}`} key={index}>
            <div className="pie-chart__percentages__group" >
                <div className="pie-chart__percentages__indicator" />
                <span>{type.percentage}%</span>
            </div>
            <div className="pie-chart__percentages__group">
                <div className="pie-chart__percentages__twitter" >
                    <TwitterIcon color="#4990e2" />
                </div>
                <span>{numberWithCommas(type.value)}</span>
            </div>
        </div>
    ));

    const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
        const RADIAN = Math.PI / 180;
        const radius = innerRadius + ((outerRadius - innerRadius) * 0.2);
        const x = cx + (radius * Math.cos(-midAngle * RADIAN));
        const y = cy + (radius * Math.sin(-midAngle * RADIAN));
        return (
            <text
                x={x}
                y={y}
                fill="white"
                fontWeight="100"
                fontSize="10"
                textAnchor={x > cx ? 'start' : 'end'}
                dominantBaseline="central"
            >
                {`${(percent * 100).toFixed(1).replace(/\.0$/, '')}%`}
            </text>
        );
    };

    const { title = 'SENTIMIENTO' } = moduleConfig;
    return (
        <div className="pie-chart" {...styles(moduleSide, moduleColor)}>
            <div className="pie-chart__label">
                <h3>{title}</h3>
            </div>
            <div className="pie-chart__content">
                <div className="pie-chart__percentages">
                    {renderLabels()}
                </div>
                <div className="pie-chart__graphic">
                    <PieChart width={129} height={129}>
                        <Pie
                            innerRadius={35}
                            outerRadius={65}
                            labelLine={false}
                            data={formatLabels}
                            label={renderCustomizedLabel}
                        >
                            {
                                formatLabels.map((entry, index) => (
                                    <Cell key={index} fill={colors[`${entry.key}`]} />
                                ))
                            }
                        </Pie>
                    </PieChart>
                </div>
            </div>
            <div className="pie-chart__footer">
                <h5>Positivo</h5>
                <h5>Neutral</h5>
                <h5>Negativo</h5>
                <div className="pie-chart__footer__twitter">
                    <TwitterIcon color="#4990e2" />
                    <h5>Tweets</h5>
                </div>
            </div>
        </div>
    );
};

PieChartRemaster.propTypes = propTypes;
PieChartRemaster.defaultProps = defaultProps;

export default PieChartRemaster;
