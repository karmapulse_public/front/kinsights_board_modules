import { css } from 'glamor';

const styles = (side, color) => {
    const bgColor = 'transparent';
    return css({
        minWidth: 300,
        height: '100%',
        backgroundColor: bgColor,
        color: 'rgba(0,0,0, 0.5)',

        ' .pie-chart__label': {
            '> h3': {
                textAlign: side,
                padding: '15px 20px',
                textTransform: 'uppercase',
                fontSize: 16,
                fontWeight: 400,
                color
            }
        },
        ' .pie-chart__content': {
            display: 'flex',
            justifyContent: 'space-around',
            alignItems: 'center'
        },
        ' .pie-chart__percentages': {
            display: 'flex',
            flexDirection: 'column',
            order: side === 'left' ? 2 : 0,
            justifyContent: 'space-between',
            ' >div': {
                position: 'relative',
                ' span': {
                    fontWeight: 'bold',
                },
                ' .pie-chart__percentages__group': {
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: side === 'left' ? 'flex-start' : 'flex-end',
                    ' span': {
                        margin: '0px 5px',
                        order: side === 'left' ? 2 : 0,
                    },
                    ' div': {
                        order: 1,
                    },
                    '&:first-of-type': {
                        marginBottom: 6,
                        ' span': {
                            fontSize: 13,
                            color: 'currentColor',
                        },
                    },
                    '&:nth-of-type(2) span': {
                        fontSize: 12,
                        color: '#000000',
                        opacity: '0.5'
                    },
                },
                '&:nth-of-type(2)': {
                    margin: '20px 0px'
                },
            },
            '&__positive, &__positivo': {
                color: '#63d12f',
            },
            '&__negative, &__negativo': {
                color: '#ea3c3c'
            },
            '&__neutral': {
                color: '#89a7ce'
            },
            '&__indicator': {
                width: 9,
                height: 9,
                borderRadius: '50%',
                backgroundColor: 'currentColor'
            },
            '&__twitter': {
                position: 'relative',
                width: 10,
                height: 8,
                ' svg': {
                    position: 'absolute',
                    width: '100%',
                    height: '100%'
                }
            }
        },
        ' .pie-chart__graphic': {
            display: 'flex',
            order: 1,
        },
        ' .pie-chart__footer': {
            display: 'flex',
            width: '100%',
            padding: '39px 15px 20px',
            fontSize: '10px',
            color: 'rgba(0,0,0, 0.5)',
            justifyContent: 'center',

            ' >h5': {
                position: 'relative',
                padding: '0 12px',

                ' &::before': {
                    position: 'absolute',
                    width: 6,
                    height: 6,
                    top: 1,
                    left: 0,
                    content: "''",
                    background: '#89a7ce',
                    borderRadius: '50%'
                },

                ' &:first-child::before': {
                    background: '#87cf8b',
                },
                ' &:nth-child(3)::before': {
                    background: '#ea3c3c',
                },
            },
            '&__twitter': {
                ' h5': {
                    display: 'inline-block',
                    margin: '0px 4px'
                },
                ' svg': {
                    width: 10,
                    height: 8
                }
            }
        },
    });
};

export default styles;
