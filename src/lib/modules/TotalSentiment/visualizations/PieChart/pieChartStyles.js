import { css } from 'glamor';

const styles = (side, color) => {
    const bgColor = 'transparent';

    return css({
        minWidth: 300,
        height: '100%',
        backgroundColor: bgColor,
        color: 'rgba(0,0,0, 0.5)',

        ' .pie-chart__label': {
            '> h3': {
                padding: '15px 20px',
                textTransform: 'uppercase',
                fontSize: 16,
                fontWeight: 400,
                textAlign: side,
                color
            }
        },
        ' .pie-chart__content': {
            display: 'flex',
            padding: side === 'left' ? '0px 0px 20px 20px' : '0px 20px 20px 0px'
        },
        ' .pie-chart__percentages': {
            order: side === 'left' ? 1 : 2,
            width: 'auto',
            ' >div': {
                position: 'relative',
                paddingLeft: 15,
                ' &:not(:last-of-type)': {
                    marginBottom: 25,
                },
                ' >span': {
                    '&:first-of-type': {
                        fontSize: 12,
                        fontWeight: 300,
                        color: '#485465',
                        '@media screen and (min-width:1024px)': {
                            fontSize: 16
                        }
                    },
                    '&:nth-of-type(2)': {
                        float: 'right',
                        marginLeft: 5,
                        fontSize: 16,
                        fontWeight: 300,
                        textAlign: side,
                        color: 'currentColor',
                    },
                    '&:nth-of-type(3)': {
                        display: 'block',
                        marginTop: 5,
                        fontWeight: 700,
                        fontSize: 12,
                        color: '#000',
                        opacity: 0.5
                    }
                },
            },
            '&__positive': {
                color: '#63d12f',
            },
            '&__negative': {
                color: '#ea3c3c'
            },
            '&__positivo': {
                color: '#63d12f',
            },
            '&__negativo': {
                color: '#ea3c3c'
            },
            '&__neutral': {
                color: '#89a7ce'
            },
            '&__indicator': {
                position: 'absolute',
                left: 0,
                top: 3,
                width: 9,
                height: 9,
                borderRadius: '50%',
                backgroundColor: 'currentColor'
            }
        },
        ' .pie-chart__graphic': {
            flex: 1,
            order: side === 'left' ? 2 : 1,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
        }
    });
};

export default styles;
