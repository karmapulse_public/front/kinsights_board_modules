import { css } from 'glamor';

const styles = (side, color, hasClick) => {
    const bgColor = 'transparent';
    return css({
        minWidth: 300,
        height: '100%',
        backgroundColor: bgColor,
        color: 'rgba(0,0,0, 0.5)',

        ' .bar-chart__label': {
            '> h3': {
                textAlign: side,
                padding: '15px 20px',
                textTransform: 'uppercase',
                fontSize: 16,
                fontWeight: 400,
                color
            }
        },
        ' .neutral': {
            padding: '5px 0px'
        },
        ' .padding': {
            cursor: hasClick ? 'pointer' : 'default',
            '&__positive, &__positivo': {
                paddingTop: 12.5,
                paddingBottom: 14.5,
            },
        },
        ' .bar-chart__content': {
            position: 'relative',
            display: 'flex',
            justifyContent: 'space-around',
            alignItems: 'center'
        },
        ' .bar-chart__total-mentions': {
            display: 'flex',
            justifyContent: 'space-around',
            alignItems: 'center',
            flexDirection: 'column',
            color,
            fontSize: 24,
            fontWeight: 'bold',
            cursor: hasClick ? 'pointer' : 'default',
            ' p': {
                paddingTop: 5,
                color: '#000000',
                opacity: 0.5,
                fontSize: 12,
                textTransform: 'capitalize',
                fontWeight: 'normal',
            },
            '&--header': {
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-around',
                ' DialogIcon': {
                    paddingRight: 50,
                },
            },
            '&--h': {
                paddingLeft: 13,
            },
        },
        ' .bar-chart__sentiment': {
            display: 'flex',
            justifyContent: 'space-around',
            alignItems: 'center',
            flexDirection: 'column',
            ' p': {
                color: '#547bbc',
                fontSize: 24,
                fontWeight: 'bold',
            },
        },
        ' .bar-chart__porcentage': {
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            flexDirection: 'row',
        },
        ' .bar-chart__header': {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-around',
            paddingTop: 27,
            paddingBottom: 27,
            ' p': {
                textTransform: 'capitalize',
                fontSize: 13,
                fontWeight: 'bold',
            },
        },
        //-----------
        ' .bar__chart__chart': {
            width: '90px',
            height: 5,
            backgroundColor: '#ececec',
            '&__positive, &__positivo': {
                backgroundColor: '#63d12f',
                height: 'inherit',
                float: side,
            },
            '&__negative, &__negativo': {
                backgroundColor: '#ea3c3c',
                height: 'inherit',
                float: side,
            },
            '&__neutral': {
                backgroundColor: '#89a7ce',
                height: 'inherit',
                float: side,
            },
        },
        //----------
        ' .bar-chart__percentages': {
            display: 'flex',
            fontSize: 16,
            fontWeight: 'bold',
            paddingLeft: 15.5,
            '&__positive, &__positivo': {
                color: '#63d12f',
            },
            '&__negative, &__negativo': {
                color: '#ea3c3c'
            },
            '&__neutral': {
                color: '#89a7ce'
            },
        },
        //-------------------
    });
};

export default styles;
