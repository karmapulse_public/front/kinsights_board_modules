import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import DialogIcon from '../../../../helpers/icons/ceo/dialogIcon';
import { numberWithCommas } from '../../../../helpers/number';
import withDrawer from '../../../withDrawer';
import styles from './BarChartStyles';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleSide: PropTypes.string,
    moduleColor: PropTypes.string,
    onClick: PropTypes.func
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleSide: 'left',
    moduleColor: '#666',
    onClick: () => ({})
};

const BarChart = ({ moduleConfig, moduleData, moduleSide, moduleColor, onClick }) => {
    const refExport = useRef(null);
    const getPercentage = n => ((n * 100) / moduleData.total).toFixed(1).replace(/\.0$/, '');
    const valores = moduleData.values;
    const filterSentiment = (item, value) => item.key === value;
    const iN = moduleData.values.findIndex(it => filterSentiment(it, 'neutral'));
    const iP = moduleData.values.findIndex(it => filterSentiment(it, 'positivo'));
    const iNeg = moduleData.values.findIndex(it => filterSentiment(it, 'negativo'));
    const titleConfig = sentiment => ({
        title: moduleConfig.title || 'Tweets en el tiempo',
        subtitleLeft: sentiment ? `Tweets con sentimiento ${sentiment}` : '',
        subtitleRight: '',
    });
    const onRealClick = moduleConfig.hasClick ? onClick : () => ({});
    const renderList = () => (
        <div className="bar-chart__header">
            <div
                className="padding"
                onClick={() => onRealClick({ filters: { sentiment: 'positivo' } }, titleConfig('positivo'))}
            >
                <div>
                    <p>
                        Positivo
                    </p>
                    <div className="bar-chart__porcentage">
                        <div className="bar__chart__chart">
                            <div
                                style={{ width: `${valores[iP] ? Math.trunc(getPercentage(valores[iP].doc_count)) : 0}%` }}
                                className="bar__chart__chart__positive"
                            />
                        </div>
                        <div className="bar-chart__percentages">
                            <p className="bar-chart__percentages__positive">
                                {`${valores[iP] ? getPercentage(valores[iP].doc_count) : 0}%`}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div
                className="padding"
                onClick={() => onRealClick({ filters: { sentiment: 'neutral' } }, titleConfig('neutral'))}
            >
                <div className="neutral">
                    <p>
                        Neutral
                    </p>
                    <div className="bar-chart__porcentage">
                        <div className="bar__chart__chart">
                            <div
                                style={{ width: `${valores[iN] ? Math.trunc(getPercentage(valores[iN].doc_count)) : 0}%` }}
                                className="bar__chart__chart__neutral"
                            />
                        </div>
                        <div className="bar-chart__percentages">
                            <p className="bar-chart__percentages__neutral">
                                {`${valores[iN] ? getPercentage(valores[iN].doc_count) : 0}%`}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div
                className="padding"
                onClick={() => onRealClick({ filters: { sentiment: 'negativo' } }, titleConfig('negativo'))}
            >
                <div>
                    <p>
                        Negativo
                    </p>
                    <div className="bar-chart__porcentage">
                        <div className="bar__chart__chart">
                            <div
                                style={{ width: `${valores[iNeg] ? Math.trunc(getPercentage(valores[iNeg].doc_count)) : 0}%` }}
                                className="bar__chart__chart__negative"
                            />
                        </div>
                        <div className="bar-chart__percentages">
                            <p className="bar-chart__percentages__negative">
                                {`${valores[iNeg] ? getPercentage(valores[iNeg].doc_count) : 0}%`}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

    const { title = 'tuits totales', buttonExport } = moduleConfig;
    return (
        <div
            ref={refExport}
            {...styles(moduleSide, moduleColor, moduleConfig.hasClick)}
        >
            <div className="bar-chart__label">
                <h3> {title} </h3>
            </div>
            <div className="bar-chart__content">
                {
                    // buttonExport(refExport, title)
                }
                <div
                    className="bar-chart__total-mentions"
                    onClick={() => onRealClick({}, titleConfig())}
                >
                    <div className="bar-chart__total-mentions--header">
                        <DialogIcon color={moduleColor} />
                        <div className="bar-chart__total-mentions--h">
                            {numberWithCommas(moduleData.total)}
                        </div>
                    </div>
                    <p> menciones totales </p>
                </div>
                <div>
                    {renderList()}
                </div>
            </div>
        </div>
    );
};

BarChart.propTypes = propTypes;
BarChart.defaultProps = defaultProps;

export default withDrawer(BarChart);
