import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import Typography from 'kpulse_design_system/dist/Typography';
import withDrawer from '../../../withDrawer';

import styles from './cardInlineStyles';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleSide: PropTypes.string,
    moduleColor: PropTypes.string,
    onClick: PropTypes.func
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleSide: 'left',
    moduleColor: '#666',
    onClick: () => ({})
};

const { Paragraph } = Typography;

const CardInline = ({ moduleConfig, moduleData, moduleSide, moduleColor, onClick }) => {
    const refExport = useRef(null);
    const colors = {
        positivo: '#63d12f',
        negativo: '#ea3c3c',
        neutral: '#89a7ce'
    };
    const label = {
        positivo: 'positivo',
        negativo: 'negativo',
        neutral: 'neutral'
    };
    const onRealClick = moduleConfig.hasClick ? onClick : () => ({});

    const titleConfig = sentiment => ({
        title: moduleConfig.title || 'Tweets en el tiempo',
        subtitleLeft: sentiment ? `Tweets con sentimiento ${label[sentiment]}` : '',
        subtitleRight: '',
    });

    const formatLabels = Object.keys(colors).map((item) => {
        const i = moduleData.values.filter(elto => elto.key === item)[0];
        return {
            key: item,
            name: item.charAt(0).toUpperCase() + item.slice(1),
            percentage: i ? ((i.doc_count * 100) / moduleData.total).toFixed(1).replace(/\.0$/, '') : 0
        };
    });

    const renderLabels = () => formatLabels.map((type, index) => (
        <div
            key={index}
            className={type.key}
            onClick={() => onRealClick({ filters: { sentiment: type.key } }, titleConfig(type.key))}
        >
            <Paragraph small bold color={colors[type.key]}>{type.key}</Paragraph>
            <Paragraph large>{type.percentage}%</Paragraph>
        </div>
    ));

    const { title = 'SENTIMIENTO', hasClick, buttonExport } = moduleConfig;
    return (
        <div
            ref={refExport}
            className="card-sentiment-inline"
            {...styles(moduleSide, moduleColor, hasClick)}
        >
            <div className="sentiment-inline__title">
                <h3>{title}</h3>
            </div>
            <div
                className="sentiment-inline__percentages"
            >
                {
                    // buttonExport(refExport, title, {
                    //     top: '-25px',
                    //     right: '5px'
                    // })
                }
                {renderLabels()}
            </div>
        </div>
    );
};

CardInline.propTypes = propTypes;
CardInline.defaultProps = defaultProps;

export default withDrawer(CardInline);
