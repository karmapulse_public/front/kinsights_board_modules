import { Observable } from 'rxjs';
import { fetchXHR } from '../../helpers/http';


export default (url, params) => (
    Observable.create((observer) => {
        Observable.fromPromise(fetchXHR(url, 'GET', params))
            .subscribe(
            (data) => {
                const buckets = data.json.response.aggregations.sentiment.buckets;
                const sum = buckets.reduce((acc, val) => (
                    acc + val.doc_count
                ), 0);

                observer.next({
                    total: sum,
                    values: buckets
                });
            },
            (error) => { observer.error(error); }
            );
    })
);
