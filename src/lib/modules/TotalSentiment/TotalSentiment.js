import React, { Component } from 'react';
import PropTypes from 'prop-types';

import fetchTotalSentiment from './TotalSentimentDataFlow';

import LoadingModule from '../LoadingModule';
import PieChart from './visualizations/PieChart';
import PieChartRemaster from './visualizations/PieChartRemaster';
import BarChart from './visualizations/BarChart';
import CardInline from './visualizations/CardInline';

const propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    dateRange: PropTypes.object,
    services: PropTypes.object
};

const defaultProps = {
    fields: {},
    side: 'left',
    color: '#666',
    dateRange: {},
    services: {}
};

class TotalSentiment extends Component {
    constructor(props) {
        super(props);

        this.state = {
            moduleServices: props.services,
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color,
            moduleRange: props.dateRange
        };
    }

    componentDidMount() {
        this.updateDataFlow(this.props);
    }

    componentDidUpdate(prevProps) {
        const { startDate, endDate } = this.props.dateRange;
        const { fields: { hasClick, search_id } } = this.props;
        if (startDate !== prevProps.dateRange.startDate ||
            endDate !== prevProps.dateRange.endDate ||
            search_id !== prevProps.fields.search_id
        ) {
            this.updateDataFlow(this.props);
        }
        if (hasClick !== prevProps.fields.hasClick) {
            this.changeState(hasClick);
        }
    }

    changeState(hasClick) {
        this.setState({
            moduleConfig: {
                ...this.state.moduleConfig,
                hasClick
            }
        });
    }

    updateDataFlow(props) {
        const { fields, dateRange: { startDate, endDate }} = props;
        const params = {
            recipe_id: 'module_tw_total_sentiment',
            rule_id: fields.search_id,
            initial_date: startDate.startOf('day').utc().toISOString(),
            final_date: endDate.endOf('day').utc().toISOString()
        };

        fetchTotalSentiment(props.services.twitter, params)
            .subscribe(
                (data) => {
                    this.setState({
                        moduleConfig: props.fields,
                        moduleData: data,
                        moduleLoading: false,
                        moduleSide: props.side,
                        moduleColor: props.color,
                        moduleRange: props.dateRange
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    renderByViz() {
        const { moduleConfig, moduleData } = this.state;

        if (moduleData) {
            const visualizations = {
                pie_chart: params => (
                    <PieChart {...params} />
                ),
                pie_chart_remaster: params => (
                    <PieChartRemaster {...params} />
                ),
                bar_chart: params => (
                    <BarChart {...params} />
                ),
                card_sentiment_inlne: params => (
                    <CardInline {...params} />
                ),
            };

            return visualizations[moduleConfig.visualization](this.state);
        }

        return (<LoadingModule />);
    }

    render() {
        return (
            <div className="m-total-sentiment">
                {this.renderByViz()}
            </div>
        );
    }
}

TotalSentiment.propTypes = propTypes;
TotalSentiment.defaultProps = defaultProps;

export default TotalSentiment;
