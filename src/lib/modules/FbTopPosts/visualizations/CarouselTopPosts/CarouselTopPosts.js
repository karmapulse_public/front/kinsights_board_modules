import React, { Component } from 'react';
import PropTypes from 'prop-types';
import toPairs from 'lodash/toPairs';
import moment from 'moment';
import NoDataModule from '../../../NoDataModule';

import { numberWithCommas } from '../../../../helpers/number';
import {
    LikeIcon,
    LoveIcon,
    HahaIcon,
    WowIcon,
    SadIcon,
    AngryIcon
} from '../../../../helpers/icons/fb';
import {
    AngleLeft,
    AngleRight
} from '../../../../helpers/icons/gob';
import styles from './styles';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    noDataYet: PropTypes.func
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    noDataYet: null
};

const mapDenominations = {
    photo: {
        name: 'Imagen',
        icon: ''
    },
    video: {
        name: 'Video',
        icon: ''
    },
    link: {
        name: 'Enlace',
        icon: ''
    },
    text: {
        name: 'Texto',
        icon: '='
    },
    status: {
        name: 'Estatus',
        icon: ''
    }
};

const mapReactions = reactions => ({
    LIKE: {
        text: 'Me gusta',
        value: reactions.LIKE,
        icon: <LikeIcon />
    },
    LOVE: {
        text: 'Me encanta',
        value: reactions.LOVE,
        icon: <LoveIcon />
    },
    HAHA: {
        text: 'Me divierte',
        value: reactions.HAHA,
        icon: <HahaIcon />,
    },
    WOW: {
        text: 'Me asombra',
        value: reactions.WOW,
        icon: <WowIcon />
    },
    SAD: {
        text: 'Me entristece',
        value: reactions.SAD,
        icon: <SadIcon />
    },
    ANGRY: {
        text: 'Me enfada',
        value: reactions.ANGRY,
        icon: <AngryIcon />
    }
});

const formatBody = (body) => {
    if (body.length > 70) {
        return `${body.substring(0, 65)}...`;
    }
    return body;
};

const getReactionsIcons = (reactions) => {
    const reactionsOrdered = mapReactions(reactions);
    const reactionsArray = toPairs(reactionsOrdered);
    const reactionsFiltered = reactionsArray.filter(
        reaction => (reaction[1].value !== 0)
    );
    return reactionsFiltered.map(
        (reaction, index) => (
            <li key={index}>
                {reaction[1].icon}
            </li>
        )
    );
};

const renderReactionsTooltip = (total, reactions) => {
    const reactionsOrdered = mapReactions(reactions);
    const reactionsArray = toPairs(reactionsOrdered);
    const reactionsFiltered = reactionsArray.filter(
        reaction => (reaction[1].value !== 0)
    );

    return (
        <div className="post__tooltip">
            <h5>Total: {numberWithCommas(total)}</h5>
            {
                reactionsFiltered.map(
                    (reaction, index) => (
                        <h5 key={index}>
                            {reaction[1].icon}
                            <span>{reaction[1].text}:</span>
                            <span>{numberWithCommas(reaction[1].value)}</span>
                        </h5>
                    )
                )
            }
        </div>
    );
};

class CarouselTopPosts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            postActive: 0
        };
        this.numPosts = props.moduleData.data.length;
        this.changeLeft = this.changeLeft.bind(this);
        this.changeRight = this.changeRight.bind(this);
    }

    changeLeft() {
        if (this.state.postActive > 0) {
            this.setState({
                postActive: this.state.postActive - 1
            });
        }
    }

    changeRight() {
        if (this.state.postActive < (this.numPosts - 1)) {
            this.setState({
                postActive: this.state.postActive + 1
            });
        }
    }

    renderActivePost() {
        const post = this.props.moduleData.data[this.state.postActive];
        const postDate = moment(post.publication.date).format('DD MMM YY');

        return (
            <div className="post">
                <h4>
                    <span>{`${this.state.postActive + 1}.`}</span>
                    <span>{mapDenominations[post.denomination].name}</span>
                    <span>{postDate}</span>
                </h4>
                <a
                    href={post.publication.media.url}
                    target="_blank"
                    rel="noopener noreferrer"
                    className="post__body"
                >
                    <div
                        className={post.denomination}
                        style={{ backgroundImage: `url(${post.publication.media.cover ? post.publication.media.cover.replace('http:', 'https:') : ''})` }}
                    >
                        {mapDenominations[post.denomination].icon}
                    </div>
                    <p>{formatBody(post.publication.body)}</p>
                </a>
                <div className="post__interactions">
                    <div>
                        <h5>
                            Compartidas: <span>{numberWithCommas(post.shares_total)}</span>
                        </h5>
                        <h5>
                            Comentarios: <span>{numberWithCommas(post.comments_total)}</span>
                        </h5>
                    </div>
                    <div>
                        <h5>Reacciones desglosadas</h5>
                        <ul>{getReactionsIcons(post.reactions)}</ul>
                        <span>{numberWithCommas(post.reactions_total)}</span>
                        {renderReactionsTooltip(post.reactions_total, post.reactions)}
                    </div>
                </div>
            </div>
        );
    }

    renderControls() {
        return (
            <div className="controls">
                <button
                    onClick={this.changeLeft}
                >
                    <AngleLeft
                        size="13px"
                        color="#000"
                        opacity={this.state.postActive === 0 ? 0.2 : 1}
                    />
                </button>
                <button
                    onClick={this.changeRight}
                >
                    <AngleRight
                        size="13px"
                        color="#000"
                        opacity={this.state.postActive === (this.numPosts - 1) ? 0.2 : 1}
                    />
                </button>
            </div>
        );
    }

    render() {
        const { moduleConfig, moduleData, moduleColor } = this.props;
        return (
            <div className="top-posts" {...styles(moduleColor)}>
                <div className="top-posts__label">
                    <h3>{moduleConfig.title}</h3>
                </div>
                {
                    (() => {
                        if (this.props.noDataYet !== null) {
                            return this.props.noDataYet();
                        }

                        return (moduleData.total !== 0) ?
                            <div className="top-posts__content">
                                {this.renderActivePost()}
                                {this.renderControls()}
                            </div>
                            : <NoDataModule />;
                    })()
                }
            </div>
        );
    }
}


CarouselTopPosts.propTypes = propTypes;
CarouselTopPosts.defaultProps = defaultProps;

export default CarouselTopPosts;
