import React from 'react';
import PropTypes from 'prop-types';
import toPairs from 'lodash/toPairs';
import Slider from 'kpulse_design_system/dist/Carousel';
import { faFacebookF } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import NoDataModule from '../../../NoDataModule';
import styles from './styles';

import Gallery from '../../../../helpers/customComponents/Gallery';
import Header from '../../../../helpers/customComponents/Header';
import BodyText from '../../../../helpers/customComponents/BodyText';
import Card from '../../../../helpers/customComponents/Card';
import Link from '../../../../helpers/customComponents/Link';

import { numberWithCommas } from '../../../../helpers/number';
import {
    LikeIcon,
    LoveIcon,
    HahaIcon,
    WowIcon,
    SadIcon,
    AngryIcon
} from '../../../../helpers/icons/fb';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    noDataYet: PropTypes.func
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    noDataYet: null
};

const mapReactions = reactions => ({
    LIKE: {
        text: 'Me gusta',
        value: reactions.LIKE,
        icon: <LikeIcon />
    },
    LOVE: {
        text: 'Me encanta',
        value: reactions.LOVE,
        icon: <LoveIcon />
    },
    HAHA: {
        text: 'Me divierte',
        value: reactions.HAHA,
        icon: <HahaIcon />,
    },
    WOW: {
        text: 'Me asombra',
        value: reactions.WOW,
        icon: <WowIcon />
    },
    SAD: {
        text: 'Me entristece',
        value: reactions.SAD,
        icon: <SadIcon />
    },
    ANGRY: {
        text: 'Me enfada',
        value: reactions.ANGRY,
        icon: <AngryIcon />
    }
});

const formatBody = (body) => {
    if (body.length > 195) {
        return `${body.substring(0, 190)}...`;
    }
    return body;
};

const getReactionsIcons = (reactions) => {
    const reactionsOrdered = mapReactions(reactions);
    const reactionsArray = toPairs(reactionsOrdered);
    const reactionsFiltered = reactionsArray.filter(
        reaction => (reaction[1].value !== 0)
    );
    return reactionsFiltered.map(
        (reaction, index) => (
            <li key={index}>
                {reaction[1].icon}
            </li>
        ));
};

const renderReactionsTooltip = (total, reactions) => {
    const reactionsOrdered = mapReactions(reactions);
    const reactionsArray = toPairs(reactionsOrdered);
    const reactionsFiltered = reactionsArray.filter(
        reaction => (reaction[1].value !== 0)
    );

    return (
        <div className="card__tooltip">
            {
                reactionsFiltered.map(
                    (reaction, index) => (
                        <h5 key={index}>
                            {reaction[1].icon}
                            <span>{numberWithCommas(reaction[1].value)}</span>
                        </h5>
                    )
                )
            }
        </div>
    );
};

const CarouselTopPosts = ({ moduleData, moduleConfig, moduleColor, noDataYet }) => {
    const id = moduleConfig.fields.search_id === undefined ? '' : moduleConfig.fields.search_id;
    
    const renderContent = () => (
        <div style={{ position: 'relative' }}>
            <Slider width="100%" height="410px" dotsColor="#b38e5d" arrowsColor="#d4c19c" arrowsHeight="20px">
                {
                    moduleData.data.map((item, index) => {
                        const mappedItem = { ...item.publication, source_type: 'facebook', rule_id: id, posted_time: item.publication.date };
                        return (
                            <div className="card" key={index}>
                                <div className="card__number">
                                    <p>
                                        {index + 1}
                                    </p>
                                </div>
                                <div className="card__data">
                                    <Card key={index}>
                                        <Header noSource item={mappedItem} services={moduleConfig} />
                                        <BodyText text={formatBody(item.publication.body)} type={item.denomination} />
                                        {(() => {
                                            if (item.denomination === 'photo') {
                                                return (
                                                    <Gallery
                                                        items={[{ url: item.publication.media.cover }]}
                                                        height="212px"
                                                        width="100%"
                                                        type="facebook"
                                                    />
                                                );
                                            }
                                            if (item.denomination === 'video') {
                                                return (
                                                    <Gallery
                                                        items={[{ url: item.publication.media.cover }]}
                                                        height="212px"
                                                        width="100%"
                                                        type="facebook"
                                                        video
                                                    />
                                                );
                                            }
                                            if (item.denomination === 'link') {
                                                return (
                                                    // eslint-disable-next-line jsx-a11y/anchor-is-valid
                                                    <Link
                                                        url={item.publication.media.url}
                                                        imageUrl={item.publication.media.cover}
                                                        title={item.publication.media.title}
                                                        text={item.publication.body}
                                                        type={item.denomination}
                                                    />
                                                );
                                            }
                                            return null;
                                        })()}
                                    </Card>
                                </div>
                                <div className="card__interactions">
                                    <h5>
                                        <span>{numberWithCommas(item.shares_total)}</span> Compartidas
                                    </h5>
                                    <h5>
                                        <span>{numberWithCommas(item.comments_total)}</span> Comentarios
                                    </h5>
                                    <div>
                                        <h5>{numberWithCommas(item.reactions_total)}</h5>
                                        <ul>{getReactionsIcons(item.reactions)}</ul>
                                        {renderReactionsTooltip(item.reactions_total, item.reactions)}
                                    </div>
                                </div>
                            </div>
                        );
                    })
                }
            </Slider>
        </div>
    );

    return (
        <div className="top-posts" {...styles}>
            <div className="top-posts__label">
                <h3>PUBLICACIONES CON MÁS INTERACCIÓN</h3>
                <div>
                    <FontAwesomeIcon icon={faFacebookF} color="white" />
                </div>
            </div>
            {
                (() => {
                    if (noDataYet !== null) {
                        return noDataYet();
                    }

                    return (moduleData.total !== 0) ?
                        renderContent()
                        : <NoDataModule />;
                })()
            }
        </div>
    );
};


CarouselTopPosts.propTypes = propTypes;
CarouselTopPosts.defaultProps = defaultProps;

export default CarouselTopPosts;
