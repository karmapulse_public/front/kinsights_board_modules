import React from 'react';
import PropTypes from 'prop-types';
import toPairs from 'lodash/toPairs';
import NoDataModule from '../../../NoDataModule';

import { numberWithCommas } from '../../../../helpers/number';
import {
    LikeIcon,
    LoveIcon,
    HahaIcon,
    WowIcon,
    SadIcon,
    AngryIcon
} from '../../../../helpers/icons/fb';
import styles from './styles';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666'
};

const TableTopPosts = ({ moduleConfig, moduleData, moduleColor }) => {
    const mapDenominations = {
        photo: {
            name: 'Imagen',
            icon: ''
        },
        video: {
            name: 'Video',
            icon: '>'
        },
        link: {
            name: 'Enlace',
            icon: ''
        },
        text: {
            name: 'Texto',
            icon: '='
        },
        status: {
            name: 'Estatus',
            icon: ''
        }
    };

    const mapReactions = reactions => ({
        LIKE: {
            text: 'Me gusta',
            value: reactions.LIKE,
            icon: <LikeIcon />
        },
        LOVE: {
            text: 'Me encanta',
            value: reactions.LOVE,
            icon: <LoveIcon />
        },
        HAHA: {
            text: 'Me divierte',
            value: reactions.HAHA,
            icon: <HahaIcon />,
        },
        WOW: {
            text: 'Me asombra',
            value: reactions.WOW,
            icon: <WowIcon />
        },
        SAD: {
            text: 'Me entristece',
            value: reactions.SAD,
            icon: <SadIcon />
        },
        ANGRY: {
            text: 'Me enfada',
            value: reactions.ANGRY,
            icon: <AngryIcon />
        }
    });

    const formatBody = (body) => {
        if (body.length > 70) {
            return `${body.substring(0, 65)}...`;
        }
        return body;
    };

    const getReactionsIcons = (reactions) => {
        const reactionsOrdered = mapReactions(reactions);
        const reactionsArray = toPairs(reactionsOrdered);
        const reactionsFiltered = reactionsArray.filter(
            reaction => (reaction[1].value !== 0)
        );
        return reactionsFiltered.map(
            (reaction, index) => (
                <li key={index}>
                    {reaction[1].icon}
                </li>
            )
        );
    };

    const renderReactionsTooltip = (total, reactions) => {
        const reactionsOrdered = mapReactions(reactions);
        const reactionsArray = toPairs(reactionsOrdered);
        const reactionsFiltered = reactionsArray.filter(
            reaction => (reaction[1].value !== 0)
        );

        return (
            <div className="top-posts__tooltip">
                <h5>Total: {numberWithCommas(total)}</h5>
                {
                    reactionsFiltered.map(
                        (reaction, index) => (
                            <h5 key={index}>
                                {reaction[1].icon}
                                <span>{reaction[1].text}:</span>
                                <span>{numberWithCommas(reaction[1].value)}</span>
                            </h5>
                        )
                    )
                }
            </div>
        );
    };

    const renderPostsList = () => (
        <ul className="top-posts__list">
            { moduleData.data.map((post, index) => (
                <li key={index}>
                    <h4>
                        <span>{mapDenominations[post.denomination].name}</span>
                        Interacciones: {numberWithCommas(post.interactions_total)}
                    </h4>
                    <a
                        href={post.publication.media.url}
                        target="_blank"
                        rel="noopener noreferrer"
                        className="top-posts__post"
                    >
                        <div
                            className={post.denomination}
                            style={{ backgroundImage: `url(${post.publication.media.cover ? post.publication.media.cover.replace('http:', 'https:') : ''})` }}
                        >
                            {mapDenominations[post.denomination].icon}
                        </div>
                        <p>{formatBody(post.publication.body)}</p>
                    </a>
                    <div className="top-posts__interactions">
                        <div>
                            <h5>
                                Compartidas: <span>{numberWithCommas(post.shares_total)}</span>
                            </h5>
                            <h5>
                                Comentarios: <span>{numberWithCommas(post.comments_total)}</span>
                            </h5>
                        </div>
                        <div>
                            <h5>Reacciones desglosadas</h5>
                            <ul>{getReactionsIcons(post.reactions)}</ul>
                            <span>{numberWithCommas(post.reactions_total)}</span>
                            {renderReactionsTooltip(post.reactions_total, post.reactions)}
                        </div>
                    </div>
                </li>
            ))}
        </ul>
    );

    return (
        <div className="top-posts" {...styles(moduleColor)}>
            <div className="top-posts__label">
                <h3>{moduleConfig.title}</h3>
            </div>
            {
                (moduleData.total !== 0) ?
                    renderPostsList()
                : <NoDataModule />
            }
        </div>
    );
};

TableTopPosts.propTypes = propTypes;
TableTopPosts.defaultProps = defaultProps;

export default TableTopPosts;
