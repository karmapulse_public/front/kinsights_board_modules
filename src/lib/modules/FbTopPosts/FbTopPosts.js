import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import fetchFbTopPosts from './FbTopPostsDataFlow';

import LoadingModule from '../LoadingModule';
import NoDataYet from '../NoDataYet';
import TableTopPosts from './visualizations/TableTopPosts';
import CarouselTopPosts from './visualizations/CarouselTopPosts';
import SliderTopPost from './visualizations/SliderTopPost';

const propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    dateRange: PropTypes.object,
    services: PropTypes.object,
    showNoDataYet: PropTypes.bool
};

const defaultProps = {
    fields: {},
    side: 'left',
    color: '#444',
    dateRange: {},
    services: {},
    showNoDataYet: false
};

class FbTopPosts extends Component {
    constructor(props) {
        super(props);

        this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color
        };
    }

    componentDidMount() {
        this.updateDataFlow(this.props);
    }

    componentWillReceiveProps(nextProps) {
        this.updateDataFlow(nextProps);
    }

    updateDataFlow(props) {
        const params = {
            rule_id: props.fields.search_id,
            initial_date: props.dateRange.startDate.toISOString(),
            final_date: props.dateRange.endDate.toISOString(),
            module_id: 'module_fb_top_posts',
            top: props.top
        };

        fetchFbTopPosts(props.services.facebook, params)
            .subscribe(
                (data) => {
                    this.setState({
                        moduleConfig: props,
                        moduleData: data,
                        moduleLoading: false,
                        moduleSide: props.side,
                        moduleColor: props.color
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    renderByViz() {
        const { moduleConfig, moduleData } = this.state;
        const { dateRange: { startDate, endDate }, showNoDataYet } = this.props;
        let renderNoDataYet = null;

        if (showNoDataYet && moment().diff(endDate, 'minutes') <= 0 && endDate.diff(startDate, 'days') <= 1) {
            renderNoDataYet = () => <NoDataYet />;
        }
        if (moduleData) {
            const visualizations = {
                table_top_posts: params => (
                    <TableTopPosts {...params} />
                ),
                carousel_top_posts: params => (
                    <CarouselTopPosts {...params} noDataYet={renderNoDataYet} />
                ),
                slider_top_posts: params => (
                    <SliderTopPost {...params} noDataYet={renderNoDataYet} />
                )
            };

            return visualizations[moduleConfig.fields.visualization](this.state);
        }

        return (<LoadingModule />);
    }

    render() {
        return (
            <div className="m-fb-top-posts">
                {this.renderByViz()}
            </div>
        );
    }
}

FbTopPosts.propTypes = propTypes;
FbTopPosts.defaultProps = defaultProps;

export default FbTopPosts;
