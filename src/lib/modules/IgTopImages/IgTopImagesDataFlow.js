import { Observable } from 'rxjs';
import { fetchXHR } from '../../helpers/http';

export default (url, params) => (
    Observable.create((observer) => {
        Observable.fromPromise(fetchXHR(url, 'GET', params))
            .subscribe(
            (data) => {
                let ig;
                const formatHits = data.json.response.map((item) => {
                    ig = item;
                    return {
                        ig: {
                            id: ig._source.id,
                            body: ig._source.body || '',
                            displayName: ig._source.media_type,
                            image: ig._source.media_url,
                            postedTime: ig._source.posted_time,
                            link: ig._source.link,
                            totalInteraction: ig._source.engagement_count,
                            coments: ig._source.comments_count,
                            favorites: ig._source.likes_count
                        }
                    };
                });

                observer.next(formatHits);
            },
            (error) => { observer.error(error); }
            );
    })
);
