import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'kpulse_design_system/dist/Carousel';

import { faInstagram } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import NoDataModule from '../../../NoDataModule';
import { numberWithCommas } from '../../../../helpers/number';
import Gallery from '../../../../helpers/customComponents/Gallery';
import BodyText from '../../../../helpers/customComponents/BodyText';
import Card from '../../../../helpers/customComponents/Card';
import Video from '../../../../helpers/customComponents/Video';
import styles from './contentCarouselStyles';

const propTypes = {
    moduleData: PropTypes.array,
    moduleConfig: PropTypes.object,
    moduleColor: PropTypes.string,
    moduleSide: PropTypes.string,
    noDataYet: PropTypes.func,
};

const defaultProps = {
    moduleData: [],
    moduleConfig: {},
    moduleColor: '#666',
    moduleSide: 'left',
    noDataYet: null,
};
const formatBody = (body) => {
    if (body.length > 70) {
        return `${body.substring(0, 65)}...`;
    }
    return body;
};

const SliderTopPost = ({ moduleData, moduleConfig, moduleColor, noDataYet }) => {    

    const renderContent = () => (
        <div style={{ position: 'relative' }}>
            <Slider width="100%" height="410px" dotsColor="#b38e5d" arrowsColor="#d4c19c" arrowsHeight="20px">
                {
                    moduleData.map((item, index) => (
                        <div className="card" key={index}>
                            <div className="card__number">
                                <p>
                                    {index + 1}
                                </p>
                            </div>
                            <div className="card__data">
                                <Card key={index}>
                                    <BodyText text={formatBody(item.ig.body)} type="instagram" />
                                    {(() => {
                                        if (item.ig.displayName === 'IMAGE') {
                                            return (
                                                <Gallery
                                                    items={[{ url: item.ig.image }]}
                                                    width="100%"
                                                    height="212px"
                                                    type="instagram"
                                                    url={[item.ig.link]}
                                                />
                                            );
                                        }
                                        if (item.ig.displayName === 'VIDEO') {
                                            const soruces = [{
                                                url: item.ig.link
                                            }];
                                            return (
                                                <Video
                                                    sources={soruces}
                                                    height="212px"
                                                />
                                            );
                                        }
                                        if (item.ig.displayName === 'CAROUSEL_ALBUM') {
                                            return (
                                                <Gallery
                                                    items={[{ url: item.ig.image }]}
                                                    width="100%"
                                                    height="212px"
                                                    type="instagram"
                                                    url={[item.ig.link]}
                                                />
                                            );
                                        }
                                        return null;
                                    })()}
                                </Card>
                            </div>
                            <div className="card__interactions">
                                <h5>
                                    <span>{numberWithCommas(item.ig.favorites)}</span> Me gusta
                                </h5>
                                <h5>
                                    <span>{numberWithCommas(item.ig.coments)}</span> Comentarios
                                </h5>
                            </div>
                        </div>
                    ))
                }
            </Slider>
        </div>
    );

    return (
        <div
            className="top-posts"
            {...styles}
        >
            <div className="top-posts__label">
                <h3>PUBLICACIONES CON MÁS INTERACCIÓN</h3>
                <div>
                    <FontAwesomeIcon icon={faInstagram} color="white" />
                </div>
            </div>
            {
                (() => {
                    if (noDataYet !== null) {
                        return noDataYet();
                    }

                    return (moduleData.length > 0) ?
                        renderContent()
                        : <NoDataModule color={moduleColor} />;
                })()
            }
        </div>
    );
}

SliderTopPost.propTypes = propTypes;
SliderTopPost.defaultProps = defaultProps;

export default SliderTopPost;
