import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import NoDataModule from '../../../NoDataModule';
import { numberWithCommas } from '../../../../helpers/number';
import LikeIcon from '../../../../helpers/icons/ceo/likesIcon';
import ComentsIcon from '../../../../helpers/icons/ceo/dialogIcon';

import {
    AngleLeft,
    AngleRight
} from '../../../../helpers/icons/gob';
import styles from './contentCarouselStyles';

const propTypes = {
    moduleData: PropTypes.array,
    moduleConfig: PropTypes.object,
    moduleColor: PropTypes.string,
    moduleSide: PropTypes.string,
    noDataYet: PropTypes.func,
};

const defaultProps = {
    moduleData: [],
    moduleConfig: {},
    moduleColor: '#666',
    moduleSide: 'left',
    noDataYet: null,
};
const formatBody = (body) => {
    if (body.length > 70) {
        return `${body.substring(0, 65)}...`;
    }
    return body;
};

class ContentCarousel extends Component {

    constructor(props) {
        super(props);

        this.state = {
            igActive: 0,
        };
        this.handleNext = this.handleNext.bind(this);
        this.handlePrev = this.handlePrev.bind(this);
    }

    handlePrev() {
        if (this.state.igActive > 0) {
            this.setState({
                igActive: this.state.igActive - 1
            });
        }
    }

    handleNext() {
        const igActive = this.state.igActive + 1;
        if (igActive <= this.props.moduleData.length - 1 && this.state.igActive < 2) {
            this.setState({
                igActive
            });
        }
    }

    renderigActive() {
        const { igActive } = this.state;
        const ig = this.props.moduleData[igActive];
        const igDate = moment(ig.ig.postedTime).format('DD MMM YY');
        const etiquetas = ig.ig.displayName;

        const types = (() => {
            if (etiquetas === 'IMAGE') {
                return 'Imagen';
            }
            if (etiquetas === 'VIDEO') {
                return 'Video';
            }
            return 'Carrusel';
        });

        return (
            <div className="carousel__ig" >
                <h4>
                    <span>{`${igActive + 1}.`}</span>
                    <span>{types()}</span>
                    <span>{igDate}</span>
                </h4>
                <a
                    href={ig.ig.link}
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    <div
                        style={{ backgroundImage: `url(${ig.ig.image ? ig.ig.image.replace('http:', 'https:') : ''})` }}
                    />
                    <p
                        dangerouslySetInnerHTML={{
                            __html: formatBody(ig.ig.body)
                        }}
                    />
                </a>
                <div className="ig__interactions">
                    <div>
                        <LikeIcon color="rgba(0,0,0,0.5)" />
                        <div>
                            <span>Likes:</span>
                            <span>{numberWithCommas(ig.ig.favorites)}</span>
                        </div>
                    </div>
                    <div>
                        <ComentsIcon color="rgba(0,0,0,0.5)" />
                        <div>
                            <span>Comentarios:</span>
                            <span>{numberWithCommas(ig.ig.coments)}</span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderControls() {
        let opacityNext = 1;
        if (this.props.moduleData.length - 1 === this.state.igActive || this.state.igActive === 2) {
            opacityNext = 0.2;
        }

        return (
            <div className="controls">
                <button
                    onClick={this.handlePrev}
                >
                    <AngleLeft
                        size="13px"
                        color="#000"
                        opacity={this.state.igActive === 0 ? 0.2 : 1}
                    />
                </button>
                <button
                    onClick={this.handleNext}
                >
                    <AngleRight
                        size="13px"
                        color="#000"
                        opacity={opacityNext}
                    />
                </button>
            </div>
        );
    }

    render() {
        const { title = 'CONTENIDO MÁS COMPARTIDO' } = this.props.moduleConfig;
        return (
            <div
                className="content_carousel"
                {...styles(this.props.moduleSide, this.props.moduleColor)}
            >
                <div className="carousel__label">
                    <h3>{title}</h3>
                </div>
                {
                    (() => {
                        if (this.props.noDataYet !== null) {
                            return this.props.noDataYet();
                        }

                        return (this.props.moduleData.length > 0) ?
                            <div className="carousel__content">
                                {this.renderigActive()}
                                {this.renderControls()}
                            </div>
                            : <NoDataModule color={this.props.moduleColor} />;
                    })()
                }
            </div>
        );
    }
}

ContentCarousel.propTypes = propTypes;
ContentCarousel.defaultProps = defaultProps;

export default ContentCarousel;
