import React, { Component } from 'react';
import PropTypes from 'prop-types';
import clone from 'lodash/clone';
import moment from 'moment-timezone';
import isUndefined from 'lodash/isUndefined';


import fetchTopAudience from './TopAudienceDataFlow';

import LoadingModule from '../LoadingModule';
import CardListAudience from './visualizations/CardListAudience';
import CardTopAudience from './visualizations/CardTopAudience';

const propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    services: PropTypes.object,
    dateRange: PropTypes.object
};

const defaultProps = {
    fields: {},
    side: 'left',
    color: '#666',
    services: {},
    dateRange: {}
};

class TotalAudience extends Component {
    constructor(props) {
        super(props);

        this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color
        };
    }

    componentDidMount() {
        let { startDate, endDate } = this.props.dateRange;

        const tz = 'America/Mexico_City';
        const strStartDate = clone(startDate).format('YYYY-MM-DD hh:mm:ss');
        const strEndDate = clone(endDate).format('YYYY-MM-DD hh:mm:ss');

        const utcOffsetStart = moment.tz(strStartDate, tz).isDST() ? '-05:00' : '-06:00';
        const utcOffsetEnd = moment.tz(strEndDate, tz).isDST() ? '-05:00' : '-06:00';

        startDate = startDate.utcOffset(utcOffsetStart).set({
            hour: 0,
            minute: 0,
            second: 0,
            millisecond: 0
        }).toISOString();

        endDate = endDate.utcOffset(utcOffsetEnd).set({
            minute: 5 * (Math.floor(moment().minute() / 5)),
            second: 0,
            millisecond: 0
        }).toISOString();

        const params = {
            recipe_id: 'module_tw_top_sentiment_audience',
            rule_id: this.state.moduleConfig.search_id,
            initial_date: startDate,
            final_date: endDate
        };

        fetchTopAudience(this.props.services.twitter, params)
            .subscribe(
                (data) => {
                    let formattedData = null;
                    if (this.state.moduleConfig.visualization === 'card_list_audience') {
                        formattedData = {
                            audiencePositiveList: isUndefined(data.positivo) ? [] : data.positivo,
                            audienceNegativeList: isUndefined(data.negativo) ? [] : data.negativo,
                        };
                    } else {
                        formattedData = data;
                    }
                    this.setState({
                        moduleData: formattedData,
                        moduleLoading: false
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    componentWillReceiveProps(nextProps) {
        let { startDate, endDate } = nextProps.dateRange;

        const tz = 'America/Mexico_City';
        const strStartDate = clone(startDate).format('YYYY-MM-DD hh:mm:ss');
        const strEndDate = clone(endDate).format('YYYY-MM-DD hh:mm:ss');

        const utcOffsetStart = moment.tz(strStartDate, tz).isDST() ? '-05:00' : '-06:00';
        const utcOffsetEnd = moment.tz(strEndDate, tz).isDST() ? '-05:00' : '-06:00';

        startDate = startDate.utcOffset(utcOffsetStart).set({
            hour: 0,
            minute: 0,
            second: 0,
            millisecond: 0
        }).toISOString();

        endDate = endDate.utcOffset(utcOffsetEnd).set({
            hour: 23,
            minute: 59,
            second: 59,
            millisecond: 999
        }).toISOString();

        const params = {
            recipe_id: 'module_tw_top_sentiment_audience',
            rule_id: this.state.moduleConfig.search_id,
            initial_date: startDate,
            final_date: endDate
        };

        fetchTopAudience(nextProps.services.twitter, params)
            .subscribe(
                (data) => {
                    let formattedData = null;
                    if (nextProps.fields.visualization === 'card_list_audience') {
                        formattedData = {
                            audiencePositiveList: isUndefined(data.positivo) ? [] : data.positivo,
                            audienceNegativeList: isUndefined(data.negativo) ? [] : data.negativo,
                        };
                    } else {
                        formattedData = data;
                    }

                    this.setState({
                        moduleData: formattedData,
                        moduleLoading: false,
                        moduleSide: nextProps.side,
                        moduleColor: nextProps.color
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    renderByViz() {
        const { moduleConfig, moduleData } = this.state;

        if (moduleData) {
            const visualizations = {
                card_list_audience: params => (
                    <CardListAudience {...params} />
                ),
                card_top_audience: params => (
                    <CardTopAudience {...params} />
                )
            };

            return visualizations[this.props.view || moduleConfig.visualization](this.state);
        }

        return (<LoadingModule />);
    }

    render() {
        return (
            <div className="m-total-audience">
                {this.renderByViz()}
            </div>
        );
    }
}

TotalAudience.propTypes = propTypes;
TotalAudience.defaultProps = defaultProps;

export default TotalAudience
