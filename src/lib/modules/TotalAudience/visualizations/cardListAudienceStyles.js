import { css } from 'glamor';

const styles = (side, tab, color) => {
    const bgColor = 'transparent';

    return css({
        minWidth: 300,
        height: '100%',
        backgroundColor: bgColor,
        color: 'rgba(0,0,0, 0.5)',
        position: 'relative',

        ' .audience__label': {
            '> h3': {
                padding: '15px 20px 20px',
                textTransform: 'uppercase',
                fontSize: 16,
                fontWeight: 400,
                textAlign: side,
                color
            }
        },
        ' .audience__tabs': {
            position: 'absolute',
            zIndex: 10,
            top: 10,
            left: side === 'left' ? 'auto' : 0,
            right: side === 'left' ? 0 : 'auto',
            width: 160,
            display: 'flex',
            ' &::before': {
                content: '""',
                position: 'absolute',
                left: tab === 'positive' ? 0 : 80,
                bottom: 0,
                zIndex: 99,
                display: 'block',
                width: 80,
                height: 2,
                backgroundColor: tab === 'positive' ? '#3cb04d' : '#d0021b',
                transition: 'all .3s ease-in-out'
            },
            ' >button': {
                width: 80,
                padding: 0,
                lineHeight: 2,
                fontSize: 11,
                fontWeight: 400,
                fontFamily: 'inherit',
                textTransform: 'uppercase',
                textAlign: 'center',
                cursor: 'pointer',
                backgroundColor: '#fff',
                borderTop: 'none',
                borderLeft: 'none',
                borderRight: 'none',
                transition: 'all .3s ease-in-out',
                ' &:focus': {
                    outline: 'none'
                }
            },
            ' &__positive': {
                borderBottom: '2px solid #3cb04d',
                color: '#3cb04d',
                opacity: tab === 'positive' ? 1 : 0.2,
            },
            ' &__negative': {
                borderBottom: '2px solid #d0021b',
                color: '#d0021b',
                opacity: tab === 'negative' ? 1 : 0.2
            },
        },
        ' .audience__list': {
            paddingLeft: side === 'left' ? 20 : 0,
            paddingRight: side === 'right' ? 20 : 0,
            ' &__header': {
                display: 'flex',
                ' h4': {
                    color: '#000',
                    opacity: '0.35',
                    fontWeight: 700,
                    fontSize: 12,
                    textTransform: 'uppercase',
                    textAlign: side,

                    '&:first-of-type': {
                        flex: 1,
                        order: side === 'left' ? 1 : 2
                    },
                    '&:last-of-type': {
                        width: 80,
                        order: side === 'left' ? 2 : 1
                    }
                }
            },
            ' &__content': {
                padding: '5px 0px 20px',
                ' li': {
                    display: 'flex',
                    alignItems: 'center',
                    borderBottom: tab === 'positive' ? '2px solid #3cb04d' : '2px solid #d0021b',
                    ' &:not(:first-of-type)': {
                        marginTop: 12
                    }
                },
                ' picture': {
                    width: 37,
                    height: 37,
                    marginRight: side === 'left' ? 15 : 0,
                    marginLeft: side === 'right' ? 15 : 0,
                    position: 'relative',
                    bottom: -2,
                    order: side === 'left' ? 1 : 3,

                    ' img': {
                        width: '100%',
                        height: '100%',
                    }
                },
                ' h4': {
                    flex: 1,
                    order: 2,
                    fontSize: 12,
                    color: 'rgba(0,0,0,.8)',
                    textAlign: side,
                    ' a': {
                        textDecoration: 'none'
                    },
                    ' span': {
                        display: 'block',
                        fontSize: 11,
                        fontWeight: 500,
                        color: 'rgba(0,0,0,.5)'
                    }
                },
                ' h5': {
                    width: 80,
                    fontSize: 16,
                    textAlign: side,
                    color: tab === 'positive' ? '#3cb04d' : '#d0021b',
                    order: side === 'left' ? 3 : 1
                }
            },
            '@media screen and (min-width:1024px)': {
                width: 280,
                float: side
            }
        },
        ' .audience__user': {
            height: 'calc(100% - 53px)',
            display: 'none',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
            '>picture': {
                width: 77,
                height: 77,
                marginBottom: 15,

                ' img': {
                    width: '100%',
                    height: '100%',
                }
            },
            '>h4': {
                fontSize: 12,
                textAlign: 'center',
                color: 'rgba(0,0,0,0.8)',
                ' a': {
                    textDecoration: 'none'
                },
                ' span': {
                    display: 'block',
                    marginTop: 3,
                    fontSize: 11,
                    color: 'rgba(0,0,0,0.5)'
                }
            },
            '>h5': {
                marginTop: 15,
                fontSize: 20,
                color: tab === 'positive' ? '#3cb04d' : '#d0021b',
            },
            '@media screen and (min-width:1024px)': {
                display: 'flex'
            }
        }
    });
};

export default styles;
