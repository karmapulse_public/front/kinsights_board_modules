import { Observable } from 'rxjs';
import { fetchXHR } from '../../helpers/http';

export default (url, params) => (
    Observable.create((observer) => {
        Observable.fromPromise(fetchXHR(url, 'GET', params))
            .subscribe(
            (data) => {
                const sentiments = data.json.response.aggregations.sentiment.buckets;
                const items = {};

                sentiments.forEach((item) => {
                    items[item.key] = item.top_users.buckets;
                });

                observer.next(items);
            },
            (error) => { observer.error(error); }
            );
    })
);
