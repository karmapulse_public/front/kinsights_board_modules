import React, { Component } from 'react';
import PropTypes from 'prop-types';


import fetchFbProfile from './TopInteractionDataFlow';

import LoadingModule from '../LoadingModule';
import SliderInteractions from './visualizations/SliderInteractions';

const propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    dateRange: PropTypes.object,
    services: PropTypes.object
};

const defaultProps = {
    fields: {},
    side: 'left',
    color: '#444',
    dateRange: {},
    services: {}
};

class TopInteraction extends Component {
    constructor(props) {
        super(props);

        this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color
        };
    }

    componentDidMount() {
        this.updateDataFlow(this.props);
    }

    componentWillReceiveProps(nextProps) {
        this.updateDataFlow(nextProps);
    }

    updateDataFlow(props) {
        const params = {
            rule_id: [props.fields.search_id],
            initial_date: props.dateRange.startDate.toISOString(),
            final_date: props.dateRange.endDate.toISOString(),
            recipe_id: 'module_tw_interactions_tweets',
            filters: {
                username: props.fields.name_user
            }
        };

        fetchFbProfile(props.services.twitter, params)
            .subscribe(
                (data) => {
                    this.setState({
                        moduleConfig: props.fields,
                        moduleData: data,
                        moduleLoading: false,
                        moduleSide: props.side,
                        moduleColor: props.color
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    renderByViz() {
        const { moduleConfig, moduleData } = this.state;

        if (moduleData) {
            const visualizations = {
                slider_interactions: params => (
                    <SliderInteractions {...params} />
                ),
            };

            return visualizations[moduleConfig.visualization](this.state);
        }

        return (<LoadingModule />);
    }

    render() {
        return (
            <div className="m-fb-total-numbers">
                {this.renderByViz()}
            </div>
        );
    }
}

TopInteraction.propTypes = propTypes;
TopInteraction.defaultProps = defaultProps;

export default TopInteraction;
