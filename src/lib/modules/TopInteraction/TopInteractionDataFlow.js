import { Observable } from 'rxjs';
import { fetchXHR } from '../../helpers/http';

export default (url, params) => (
    Observable.create((observer) => {
        console.log(url, 'GET', params);
        Observable.fromPromise(fetchXHR(url, 'GET', params))
            .subscribe(
                (data) => {
                    observer.next(data.json.response);
                },
                (error) => { observer.error(error); }
            );
    })
);
