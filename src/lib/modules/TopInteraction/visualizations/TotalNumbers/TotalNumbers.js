import React from 'react';
import PropTypes from 'prop-types';
import LikesIcon from '../../../../helpers/icons/ceo/likesIcon';
import ReplicesIcon from '../../../../helpers/icons/ceo/dialogIcon';
import RetweetsIcon from '../../../../helpers/icons/ceo/retweetsIcon';

import styles from './styles';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string
};

const defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleColor: '#666'
};

const TotalNumbers = ({ moduleData, moduleConfig, moduleColor }) => {
    const interactionTotal = (moduleData.data.total_likes) + (moduleData.data.total_replies) + (moduleData.data.total_retweets);
    const color = '#6ab9dc';
    return (
        <div {...styles(moduleColor)}>
            <div className="total-num-int__label">
                <h3>{moduleConfig.title}</h3>
            </div>
            <div className="total-num-int__content">
                <div className="total-num-int__content__total">
                    <h4>
                        Total de interacciones
                    </h4>
                    <p>
                        {interactionTotal}
                    </p>
                </div>
                <div className="total-num-int__content__data">
                    <h4>
                        <ReplicesIcon color={color} />
                        Respuestas
                    </h4>
                    <p>
                        {moduleData.data.total_replies}
                    </p>
                </div>
                <div className="total-num-int__content__data">
                    <h4>
                        <LikesIcon color={color} />
                        Favoritos
                    </h4>
                    <p>
                        {moduleData.data.total_likes}
                    </p>
                </div>
                <div className="total-num-int__content__data">
                    <h4>
                        <RetweetsIcon color={color} />
                        Retweet
                    </h4>
                    <p>
                        {moduleData.data.total_retweets}
                    </p>
                </div>
            </div>
        </div>
    );
};

TotalNumbers.propTypes = propTypes;
TotalNumbers.defaultProps = defaultProps;

export default TotalNumbers;
