import React from 'react';
import PropTypes from 'prop-types';
import { faTwitter } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { numberWithCommas } from '../../../../helpers/number';
import styles from './styles';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string
};

const defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleColor: '#666'
};

const TotalNumbers = ({ moduleData, moduleConfig, moduleColor }) => {
    const interactionTotal = (moduleData.data.total_likes) + (moduleData.data.total_replies) + (moduleData.data.total_retweets);
    const color = '#6ab9dc';
    return (
        <div className="list-interactions" {...styles(moduleColor)}>
            <div className="list-interactions__label">
                <h3>INTERACCIONES TWITTER</h3>
                <div>
                    <FontAwesomeIcon icon={faTwitter} color="white" />
                </div>
            </div>
            <div className="list-interactions__content">
                <h4>
                    <span>{numberWithCommas(interactionTotal)}</span>
                    <p>Totales </p>
                </h4>
                <div style={{ position: 'relative', marginTop: '31px' }}>
                    <div className="slider__card">
                        <h5>
                            <span>{numberWithCommas(moduleData.data.total_likes)}</span>
                            <p>Me gusta</p>
                        </h5>
                        <h5>
                            <span>{numberWithCommas(moduleData.data.total_retweets)}</span>
                            <p>Retweets</p>
                        </h5>
                        <h5>
                            <span>{numberWithCommas(moduleData.data.total_replies)}</span>
                            <p>Respuestas</p>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    );
};

TotalNumbers.propTypes = propTypes;
TotalNumbers.defaultProps = defaultProps;

export default TotalNumbers;
