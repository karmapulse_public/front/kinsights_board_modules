import React, { Component } from 'react';
import PropTypes from 'prop-types';
import clone from 'lodash/clone';
import moment from 'moment-timezone';

import fetchTopTrends from './TopTrendsNewDataFlow';

import LoadingModule from '../LoadingModule';
import ListTopTrends from './visualizations/ListTopTrends';
import ListTopTrendsSentiment from './visualizations/ListTopTrendsSentiment';
import WordCloudTrends from './visualizations/WordCloudTrends';

const propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    services: PropTypes.object,
    dateRange: PropTypes.object,
    view: PropTypes.string,
    showColors: PropTypes.bool,
    filters: PropTypes.object,
};

const defaultProps = {
    fields: {},
    side: 'left',
    color: '#666',
    services: {},
    dateRange: {},
    view: '',
    filters: {},
    showColors: false,
};

class TopTrendsNew extends Component {
    constructor(props) {
        super(props);

        this.state = {
            moduleConfig: props.fields,
            moduleServices: props.services,
            moduleRange: props.dateRange,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color,
            moduleView: props.view,
            showColors: props.showColors
        };

        this.renderByViz = this.renderByViz.bind(this);
    }

    componentDidMount() {
        this.updateDataFlow(this.props);
    }

    componentDidUpdate(prevProps) {
        const { startDate, endDate } = this.props.dateRange;
        const { fields: { hasClick, search_id } } = this.props;
        if (startDate !== prevProps.dateRange.startDate ||
            endDate !== prevProps.dateRange.endDate ||
            search_id !== prevProps.fields.search_id) {
            this.updateDataFlow(this.props);
        }
        if (hasClick !== prevProps.fields.hasClick) {
            this.changeState(hasClick);
        }
    }

    changeState(hasClick) {
        this.setState({
            moduleConfig: {
                ...this.state.moduleConfig,
                hasClick
            }
        });
    }

    updateDataFlow(props) {
        const { fields, dateRange: { startDate, endDate }, filters } = props;
        const params = {
            recipe_id: 'module_tw_top_phrases',
            rule_id: fields.search_id,
            initial_date: startDate.startOf('day').utc().toISOString(),
            final_date: endDate.endOf('day').utc().toISOString(),
            filters: {
                top: fields.top_limit,
                ...filters
            }
        };

        fetchTopTrends(props.services.twitter, params)
            .subscribe(
                (data) => {
                    this.setState({
                        moduleData: data,
                        moduleConfig: props.fields,
                        moduleLoading: false,
                        moduleView: props.view,
                        moduleColor: props.color,
                        moduleSide: props.side,
                        showColors: props.showColors
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    renderByViz() {
        const { moduleConfig, moduleData } = this.state;

        if (moduleData) {
            const visualizations = {
                list_top_trends: params => (
                    <ListTopTrends {...params} />
                ),
                wordclouds_top_trends: params => (
                    <WordCloudTrends {...params} />
                ),
                list_top_trends_sentiment: params => (
                    <ListTopTrendsSentiment {...params} />
                )
            };
            const v = this.state.moduleView !== '' ? this.state.moduleView : moduleConfig.visualization;
            return visualizations[v](this.state);
        }

        return (<LoadingModule />);
    }

    render() {
        return (
            <div className="m-top-trends">
                {this.renderByViz()}
            </div>
        );
    }
}

TopTrendsNew.propTypes = propTypes;
TopTrendsNew.defaultProps = defaultProps;

export default TopTrendsNew;
