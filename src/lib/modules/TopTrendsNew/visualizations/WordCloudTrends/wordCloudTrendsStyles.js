import { css } from 'glamor';

const styles = (color) => {
    const bgColor = 'transparent';

    return css({
        width: '100%',
        height: 'auto',
        backgroundColor: bgColor,
        color: 'rgba(0,0,0, 0.5)',

        ' .wordcloud__label': {
            width: '100%',
            '> h3': {
                padding: '15px 20px 20px',
                textTransform: 'uppercase',
                fontSize: 16,
                fontWeight: 400,
                textAlign: 'left',
                color
            }
        },
        ' .wordcloud__chart': {
            width: '100%',
            height: 220
        },
        ' .chart__tooltip': {
            padding: 10,
            textAlign: 'center',
            border: 'dashed #000 1px',
            borderRadius: 5,
        },
    });
};

export default styles;
