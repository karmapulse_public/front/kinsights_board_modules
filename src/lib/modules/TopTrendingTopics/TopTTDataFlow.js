import { Observable } from 'rxjs';
import isEmpty from 'lodash/isEmpty';
import { fetchXHR } from '../../helpers/http';

export default (url, params) => (
    Observable.create((observer) => {
        Observable.fromPromise(fetchXHR(url, 'GET', params))
            .subscribe(
                (data) => {
                    const { response } = data.json;
                    const values = [];
                    Object.keys(response).forEach((keys) => {
                        if (!isEmpty(response[keys])) {
                            values.push(response[keys]);
                        }
                    });
                    observer.next({
                        values
                    });
                },
                (error) => { observer.error(error); }
            );
    })
);
