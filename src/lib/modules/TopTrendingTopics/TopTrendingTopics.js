import React, { Component } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';

import ListTopTT from './visualizations/ListTopTT';
import BannerTop from './visualizations/BannerTop';
import fetchTopTT from './TopTTDataFlow';
import LoadingModule from '../LoadingModule';

const propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    view: PropTypes.string,
    services: PropTypes.object,
    dateRange: PropTypes.object
};

const defaultProps = {
    fields: {},
    side: 'left',
    color: '#666',
    services: {},
    dateRange: {},
    view: ''
};

class TopTrendingTopics extends Component {
    constructor(props) {
        super(props);
        this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color,
            moduleView: props.view,
        };
    }

    componentDidMount() {
        this.fetchData(this.props);
    }

    componentWillReceiveProps(nextProps) {
        this.fetchData(nextProps);
    }

    fetchData(newProps) {
        const { startDate, endDate } = newProps.dateRange;
        const params = {
            recipe_id: 'module_tw_top_trends',
            region_id: parseInt(newProps.fields.region_id.split('|')[1].trim(), 10),
            initial_date: moment(startDate).utc().toISOString(),
            final_date: moment(endDate).utc().toISOString(),
            filters: {
                top: newProps.fields.top_limit
            }
        };

        fetchTopTT(this.props.services.twitter, params)
            .subscribe(
                (data) => {
                    this.setState({
                        moduleData: data,
                        moduleConfig: newProps.fields,
                        moduleLoading: false,
                        moduleColor: newProps.color,
                        moduleSide: newProps.side,
                        region: newProps.fields.region_id.split('|')[0].trim()
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    renderByViz() {
        const { moduleData, moduleConfig } = this.state;
        if (moduleData) {
            const visualizations = {
                list_top_trends: params => (
                    <ListTopTT {...params} />
                ),
                banner_top_trends: params => (
                    <BannerTop {...params} />
                )
            };
            const v = this.state.moduleView !== '' ? this.state.moduleView : moduleConfig.visualization;
            return visualizations[v](this.state);
        }
        return (<LoadingModule />);
    }
    render() {
        return (
            <div className="m-top-trending-topics">
                {this.renderByViz()}
            </div>
        );
    }
}

TopTrendingTopics.propTypes = propTypes;
TopTrendingTopics.defaultProps = defaultProps;

export default TopTrendingTopics;
