import React from 'react';
import PropTypes from 'prop-types';

import styles from './ListTopTTStyles';
import NoDataModule from '../../../NoDataModule';
import { numberWithCommas } from '../../../../helpers/number';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.array,
    moduleColor: PropTypes.string,
    moduleSide: PropTypes.string,
    region: PropTypes.string
};

const defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleColor: '#666',
    moduleSide: 'right',
    region: ''
};

const ListTopTT = ({
    moduleConfig, moduleData, moduleColor, moduleSide, region
}) => {
    const dataFormat = moduleData.values.map((item, index) => (
        {
            id: index + 1,
            subject: item.trend,
            total: item.total
        }
    ));

    const renderTitle = () => {
        if (moduleConfig.show_title) {
            const { title = 'RELACIÓN DE TEMAS' } = moduleConfig;
            return (
                <div className="tt__label">
                    <h3>{title}</h3>
                </div>
            );
        }
        return '';
    };

    const showItem = (index, { subject, total, id }) => {
        const value = subject.length < 35 ? subject : subject.substring(0, 35).concat('...');
        const regionName = region === 'Mundial' ? 'Todo el mundo' : region;
        return (
            <li className="tt__item" key={index}>
                <h5 className="tt__index">{id}.</h5>
                <div className="tt__nameplace">
                    <h4 className="tt__name">{value}</h4>
                    <p className="tt__place">{regionName}</p>
                </div>
                <h5 className="tt__value">
                    {numberWithCommas(total)}
                </h5>
            </li>
        );
    };

    const renderItems = () => (
        dataFormat.map((item, index) => showItem(index, item))
    );

    return (
        <div className="trending-topics" {...styles(moduleColor, moduleSide)}>
            {renderTitle()}
            <div className="tt__titles">
                <h5>Tema</h5>
                <h5>Tweets</h5>
            </div>
            {
                dataFormat.length === 0 ?
                    <NoDataModule /> :
                    <ul>
                        {renderItems()}
                    </ul>
            }
        </div>
    );
};

ListTopTT.propTypes = propTypes;
ListTopTT.defaultProps = defaultProps;

export default ListTopTT;
