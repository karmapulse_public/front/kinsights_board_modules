import React from 'react';
import PropTypes from 'prop-types';

import styles from './BannerTopStyles';
import NoDataModule from '../../../NoDataModule';
import { numberWithCommas } from '../../../../helpers/number';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.array,
    moduleColor: PropTypes.string,
    moduleSide: PropTypes.string,
    region: PropTypes.string
};

const defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleColor: '#666',
    moduleSide: 'right',
    region: ''
};

const displayRegion = (region) => {
    const names = {
        'Ciudad de México': 'CDMX'
    };
    return names[region] || region;
};

const ListTopTT = ({
    moduleConfig, moduleData, moduleColor, moduleSide, region
}) => {
    const dataFormat = moduleData.values.map((item, index) => (
        {
            id: index + 1,
            subject: item.trend,
            total: item.total
        }
    ));

    const renderTitle = () => {
        if (moduleConfig.show_title) {
            const { title = 'Tendencias' } = moduleConfig;
            return (
                <div className="tt__label">
                    <h3>{title}</h3>
                    <h3>{displayRegion(region)}</h3>
                </div>
            );
        }
        return '';
    };

    const showItem = (index, { subject, total, id }) => {
        const value = subject.length < 35 ? subject : subject.substring(0, 35).concat('...');
        return (
            <li className="tt__item" key={index}>
                <h5 className="tt__index">{id}.</h5>
                <div className="tt__nameplace">
                    <h4 className="tt__name">{value}</h4>
                    <p className="tt__value">{numberWithCommas(total)}</p>
                </div>
            </li>
        );
    };

    const renderItems = () => (
        dataFormat.map((item, index) => showItem(index, item))
    );

    return (
        <div className="trending-topics" {...styles(moduleColor, moduleSide)}>
            {renderTitle()}
            {
                dataFormat.length === 0 ?
                    <NoDataModule /> :
                    <ul>
                        {renderItems()}
                    </ul>
            }
        </div>
    );
};

ListTopTT.propTypes = propTypes;
ListTopTT.defaultProps = defaultProps;

export default ListTopTT;
