import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import getIntervalByDates from '../../helpers/getIntervalByDates';
import setDates from '../../helpers/setDates';
import fetchTimeTrendGrouper from './TimeTrendGrouperDataFlow';
import LoadingModule from '../LoadingModule';
import Lines from './visualizations/Line';

const propTypes = {
    fields: PropTypes.object,
    labels: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    dateRange: PropTypes.object,
    services: PropTypes.object,
    data: PropTypes.array,
    filters: PropTypes.object,
    withCache: PropTypes.bool,
};

const defaultProps = {
    fields: {},
    labels: {
        x: '',
        y: ''
    },
    side: 'left',
    color: '#444',
    dateRange: {},
    services: {},
    data: [],
    filters: {},
    withCache: true
};


const mapColors = (data) => {
    const colors = {};
    data.forEach((item) => {
        colors[item.title] = item.color;
    });
    return colors;
};

const mapData = (data) => {
    let maxIndex = 0;
    data.forEach((item, index) => {
        maxIndex = item.intervals.length < data[maxIndex].intervals.length ? maxIndex : index;
    });

    return data[maxIndex].intervals.map((item, index) => {
        const values = (() => {
            const objectResult = {};
            data.forEach((elto) => {
                objectResult[elto.title || 'total'] = elto.intervals[index] ? elto.intervals[index].total : 0;
            });
            return objectResult;
        })();
        return {
            key: moment(item.date).format(),
            ...values
        };
    });
};
class TimeTrendGrouper extends Component {
    constructor(props) {
        super(props);
        this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color
        };
    }

    componentDidMount() {
        const { moduleConfig: { type } } = this.state;
        const { dateRange } = this.props;
        if (type === 'agrupador') {
            if (this.props.data.length) {
                this.setState({
                    moduleData: mapData(this.props.data),
                    colors: mapColors(this.props.data),
                    moduleLoading: false,
                    intervalType: getIntervalByDates(moment(dateRange.startDate), moment(dateRange.endDate))
                });
            }
        } else if (type === 'simple') {
            const params = {
                recipe_id: 'module_tw_histogram',
                rule_id: this.state.moduleConfig.search_id,
                ...setDates(this.props.dateRange, this.props.withCache),
                filters: { ...this.props.filters },
            };

            params.filters.interval = getIntervalByDates(moment(params.initial_date), moment(params.final_date));
            fetchTimeTrendGrouper(this.props.services.twitter, params)
                .subscribe(
                    (data) => {
                        this.setState({
                            moduleData: mapData([data]),
                            moduleLoading: false,
                            colors: { total: this.props.color },
                            intervalType: params.filters.interval
                        });
                    },
                    (error) => {
                        console.log(error);
                    },
                    null
                );
        }
    }

    componentWillReceiveProps(nextProps) {
        const { moduleConfig: { type } } = this.state;
        if (type === 'agrupador') {
            const { dateRange } = nextProps;
            if (nextProps.data.length) {
                this.setState({
                    moduleData: mapData(nextProps.data),
                    colors: mapColors(nextProps.data),
                    moduleLoading: false,
                    intervalType: getIntervalByDates(moment(dateRange.startDate), moment(dateRange.endDate))
                });
            }
        } else if (type === 'simple') {
            const params = {
                recipe_id: 'module_tw_histogram',
                rule_id: this.state.moduleConfig.search_id,
                ...setDates(nextProps.dateRange, this.props.withCache),
                filters: { ...nextProps.filters },
                noUtc: true
            };
            params.filters.interval = getIntervalByDates(moment(params.initial_date), moment(params.final_date));

            fetchTimeTrendGrouper(this.props.services.twitter, params)
                .subscribe(
                    (data) => {
                        this.setState({
                            moduleData: mapData([data]),
                            colors: { total: this.props.color },
                            moduleLoading: false,
                            intervalType: params.filters.interval
                        });
                    },
                    (error) => {
                        console.log(error);
                    },
                    null
                );
        }
    }

    renderByViz() {
        const { moduleConfig, moduleData, intervalType } = this.state;

        if (moduleData) {
            const visualizations = {
                lines: params => (
                    <Lines
                        {...params}
                        labels={this.props.labels}
                        intervalType={intervalType}
                    />
                )
            };
            return visualizations[this.props.view || moduleConfig.visualization](this.state);
        }

        return (<LoadingModule />);
    }

    render() {
        return this.renderByViz();
    }
}

TimeTrendGrouper.propTypes = propTypes;
TimeTrendGrouper.defaultProps = defaultProps;

export default TimeTrendGrouper;
