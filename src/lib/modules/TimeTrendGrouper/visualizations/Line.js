import React from 'react';
import PropTypes from 'prop-types';
import {
    LineChart,
    Line,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    ResponsiveContainer,
    Label,
} from 'recharts';

import AxisX from '../../Charts/CustomComponents/AxisX';
import AxisY from '../../Charts/CustomComponents/AxisY';
import TooltipCustom from '../../Charts/CustomComponents/TooltipCustom';
import NoDataModule from '../../NoDataModule';

const propTypes = {
    moduleConfig: PropTypes.object,
    labels: PropTypes.object,
    moduleData: PropTypes.array,
    moduleSide: PropTypes.string,
    moduleColor: PropTypes.string,
    colors: PropTypes.object,
    noUtc: PropTypes.bool,
    intervalType: PropTypes.string,
};

const defaultProps = {
    moduleConfig: {},
    labels: {
        x: '',
        y: ''
    },
    moduleData: [],
    moduleSide: 'left',
    moduleColor: '#666',
    colors: {},
    noUtc: false,
    intervalType: 'hour'
};

const LinesChart = (props) => {
    const renderTooltip = innerProps => (
        <TooltipCustom
            {...innerProps}
            noUtc
        />
    );


    const renderLine = () => {
        const { moduleData, colors, labels } = props;

        return (
            <ResponsiveContainer>
                <LineChart
                    data={moduleData}
                    margin={{ top: 30, right: 30, left: 0, bottom: 25 }}
                >
                    <XAxis
                        dataKey="key"
                        tickLine={false}
                        stroke="rgba(230,236,240, 0.15)"
                        tick={<AxisX color="#FFF" intervalType={props.intervalType} noUtc />}
                    >
                        <Label value={labels.x} className="column-data__label" offset={-7} position="insideBottom" />
                    </XAxis>
                    <YAxis
                        axisLine={false}
                        tick={<AxisY color="#FFF" style={{ fontSize: '0.625em' }} />}
                    >
                        <Label value={labels.y} className="column-data__label" offset={-25} angle={-90} position="left" />
                    </YAxis>
                    <CartesianGrid vertical={false} />
                    <Tooltip content={renderTooltip} intervalType={props.intervalType} />
                    {
                        Object.keys(moduleData[0]).map((item) => {
                            if (item !== 'key') {
                                return (
                                    <Line
                                        type="linear"
                                        strokeWidth={2}
                                        key={item}
                                        dataKey={item}
                                        stroke={colors[item]}
                                        dot={true}
                                        activeDot={{ stroke: colors[item], strokeWidth: 0 }}
                                    />
                                );
                            }
                            return null;
                        }
                        )
                    }
                </LineChart>
            </ResponsiveContainer>
        );
    };

    const renderLineBasic = () => (
        <div className="line-chart" >
            <div className="line-chart__label">
                <h3>{props.moduleConfig.title}</h3>
            </div>
            {
                (() => {
                    if (props.moduleData.length === 0) {
                        return (
                            <div className="noData">
                                <NoDataModule />
                            </div>
                        );
                    }
                    return (
                        <div className="line-chart__chart">
                            <div className="chart-line">
                                {renderLine()}
                            </div>
                        </div>
                    );
                })()
            }
        </div>
    );

    return renderLineBasic();
};


LinesChart.propTypes = propTypes;
LinesChart.defaultProps = defaultProps;

export default LinesChart;
