import { Observable } from 'rxjs';
import isEmpty from 'lodash/isEmpty';
import { fetchXHR } from '../../helpers/http';

export default (url, params) => (
    Observable.create((observer) => {
        Observable.fromPromise(fetchXHR(url, 'GET', params))
            .subscribe(
            (data) => {
                const colors = {
                    positive: {
                        color: '#63d12f',
                        name: 'Positivo'
                    },
                    negative: {
                        color: '#ea3c3c',
                        name: 'Negativo'
                    },
                    neutral: {
                        color: '#89a7ce',
                        name: 'Neutral'
                    }
                };
                const { response } = data.json;
                const values = [];
                Object.keys(response).forEach((keys) => {
                    if (!isEmpty(response[keys])) {
                        values.push({
                            children: response[keys],
                            ...colors[keys]
                        });
                    }
                });
                observer.next({
                    values
                });
            },
            (error) => { observer.error(error); }
            );
    })
);
