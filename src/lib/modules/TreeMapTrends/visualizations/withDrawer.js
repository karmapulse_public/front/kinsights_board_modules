import React, { Component } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import toLower from 'lodash/toLower';

import DrawerFeed from '../../DrawerFeed';
import DrawerContext from '../../../helpers/context/ctxDrawer';

export default (WrappedComponent) => {
    class HOCDrawerTreemap extends Component {
        constructor(props) {
            super(props);
            this.handleClickEvent = this.handleClickEvent.bind(this);
            this.handleCloseDrawer = this.handleCloseDrawer.bind(this);
            this.initialState = {
                openDrawer: false,
                drawerData: {
                    type: 'TWITTER',
                    service: '',
                    title: '',
                    subtitleLeft: '',
                    subtitleRight: '',
                    queryData: {}
                }
            };
            this.state = this.initialState;
        }

        handleCloseDrawer() {
            this.setState({ ...this.initialState });
        }

        handleClickEvent(data) {
            const {
                moduleRange, moduleConfig, moduleServices
            } = this.props;
            const { title = 'TWEETS EN EL TIEMPO' } = moduleConfig;
            const queryData = {
                'recipe-id': 'module_tw_explore_tweets',
                'rule-id': moduleConfig.search_id,
                'initial-date': moment(moduleRange.startDate).format(),
                'final-date': moment(moduleRange.endDate).format(),
                filters: {
                    limit: 10,
                    phrase: data.name,
                    sentiment: toLower(data.root.name)
                }
            };
            this.setState({
                openDrawer: true,
                drawerData: {
                    type: 'TWITTER',
                    service: moduleServices.twitter,
                    title,
                    subtitleLeft: `Tweets con el tema "${data.name}" con sentimiento ${data.root.name}`,
                    subtitleRight: '',
                    queryData
                }
            });
        }

        render() {
            return (
                <DrawerContext.Provider
                    value={{
                        open: this.state.openDrawer,
                        drawerData: this.state.drawerData,
                        onCloseDrawer: this.handleCloseDrawer
                    }}
                >
                    <WrappedComponent
                        {...this.props}
                        onClickSquare={this.handleClickEvent}
                    />
                    <DrawerFeed />
                </DrawerContext.Provider>
            );
        }
    }

    HOCDrawerTreemap.propTypes = {
        moduleColor: PropTypes.string,
        intervalType: PropTypes.string,
        moduleData: PropTypes.shape({}),
        moduleRange: PropTypes.shape({}),
        moduleConfig: PropTypes.shape({}),
        moduleServices: PropTypes.shape({})
    };

    HOCDrawerTreemap.defaultProps = {
        moduleConfig: {},
        moduleData: {},
        moduleRange: {},
        moduleServices: {},
        moduleColor: '#666',
        intervalType: 'month'
    };

    return HOCDrawerTreemap;
};
