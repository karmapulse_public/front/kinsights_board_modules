import { css } from 'glamor';

const styles = (color, side) => {
    const bgColor = 'transparent';

    return css({
        position: 'relative',
        width: '100%',
        height: 'auto',
        display: 'flex',
        alignItems: 'center',
        flexWrap: 'wrap',
        backgroundColor: bgColor,
        color,
        ' .recharts-layer': {
            '.recharts-treemap-depth-0': {
                fill: 'transparent'
            },
            '.recharts-treemap-depth-2': {
                fill: '#FFF'
            },

        },
        '>ul': {
            width: '100%',
            padding: '20px'
        },

        ' .treemap__label': {
            width: '100%',
            '> h3': {
                textTransform: 'uppercase',
                textAlign: 'left',
            },
        },

        ' .chart__tooltip': {
            padding: '5px 15px 5px 0px',
            opacity: 0.8,
            color: '#000',
            display: 'flex',
            background: '#FFFFFF',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'flex-start',
            ' h4': {
                fontSize: 15,
                opacity: 1,
                marginTop: 5,
                fontWeight: 900,
                marginLeft: 15,
                marginBottom: 5,
                '&.Negativo': {
                    color: '#ea3c3c',
                },
                '&.Positivo': {
                    color: '#63d12f',
                },
                '&.Neutral': {
                    color: '#89a7ce',
                },
            },
        },
        ' .toltipText': {
            fontSize: 12,
            opacity: 1,
            marginLeft: 15,
            marginBottom: 5,
        },

        ' .treemap-trends__body': {
            width: '100%',
            margin: '0px auto',
            ' &__title': {
                display: 'flex',
                paddingTop: 5,
                flexDirection: side === 'left' ? 'row' : 'row-reverse',
                justifyContent: 'space-between',

                ' >h5': {
                    fontSize: '10px',
                }
            },

            ' .treemap__item': {
                padding: '15px 0',
                borderBottom: 'solid 1px #e3e3e3',

                ' &:last-child': {
                    borderBottom: 'none',
                },

                ' &__data': {
                    position: 'relative',
                    display: 'flex',
                    alignItems: 'center',
                    flexDirection: side === 'left' ? 'row' : 'row-reverse',

                    ' .index': {
                        width: 20,
                        height: 20,
                        fontSize: '13px',
                        fontWeight: 500,
                        textAlign: 'center',
                        lineHeight: '20px',
                        background: '#f2f2f2'
                    },

                    ' .subject': {
                        padding: '0 10px',
                        fontSize: '13px',
                        fontWeight: 500,
                    },

                    ' .total': {
                        position: 'absolute',
                        right: side === 'right' ? 'auto' : '0',
                        left: side === 'left' ? 'auto' : '0',
                        fontSize: '14px',
                        fontWeight: 500
                    },
                    ' .list__dot': {
                        width: 11,
                        height: 11,
                        borderRadius: 200,
                    },
                },
            }
        },
        ' .noData': {
            paddingTop: '40px',
            width: '100%',
            height: 250,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
        },

        ' .treemap-trends__footer': {
            width: '100%',
            ' >h5': {
                position: 'relative',
                ' &::before': {
                    position: 'absolute',
                    width: 6,
                    height: 6,
                    top: 1,
                    left: 0,
                    content: "''",
                    background: '#89a7ce',
                    borderRadius: '50%'
                },
                ' &:first-child::before': {
                    background: '#63d12f',
                },
                ' &:last-child::before': {
                    background: '#ea3c3c',
                }
            }
        }
    });
};

export default styles;
