import React, { Component } from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';

import withDrawer from './withDrawer';
import Treemap from '../../Charts/Treemap';
import styles from './treeMapTopTrendsStyles';
import NoDataModule from '../../NoDataModule';
import { numberWithCommas } from '../../../helpers/number';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    onClickSquare: PropTypes.func
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    onClickSquare: () => ({})
};

class TreeMapTopTrends extends Component {
    constructor(props) {
        super(props);
        this.state = {
            width: props.moduleConfig.width,
        };
        this.contenedor = null;
        this.changeWidth = this.changeWidth.bind(this);
    }

    componentDidMount() {
        window.addEventListener('resize', () => {
            this.changeWidth(this.contenedor.clientWidth);
        });
        this.changeWidth(this.contenedor.clientWidth);
    }

    changeWidth(newWidth) {
        this.setState({
            width: (newWidth - 30)
        });
    }

    render() {
        const { moduleColor, moduleConfig, moduleData, onClickSquare } = this.props;
        const { width } = this.state;
        const { title = 'TOP DE TEMAS', height, buttonExport } = moduleConfig;
        const { values } = moduleData;
        const onClick = moduleConfig.hasClick ? onClickSquare : () => ({});
        return (
            <div
                className="treemap-trends-sentiment"
                {...styles(moduleColor)}
                ref={(container) => {
                    this.contenedor = container;
                }}
            >
                <div className="treemap__label">
                    <h3>{title}</h3>
                </div>
                {
                    (isEmpty(values))
                        ? (
                            <div className="noData">
                                <NoDataModule color={moduleColor} />
                            </div>
                        )
                        : (
                            <div
                                className="treemap-trends__chart"
                            >
                                <div className="treemap-trends__body">
                                    {
                                        //buttonExport(this.contenedor, title)
                                    }
                                    <div>
                                        <Treemap
                                            data={values}
                                            dataKey="total"
                                            enableTooltip
                                            fill="#fff"
                                            fontFamily="Roboto"
                                            fontSize={12}
                                            height={height}
                                            stroke="#fff"
                                            width={width}
                                            onDataClick={onClick}
                                            tooltip={(props) => {
                                                const payload = props.payload.length >= 1 ? props.payload[0].payload : { name: '', value: '' };
                                                const sentiment = payload.root ? payload.root.name : '';
                                                return (
                                                    <div className="chart__tooltip">
                                                        <h4 className={`${sentiment}`}>
                                                            {payload.name}
                                                        </h4>
                                                        <h5 className="toltipText">
                                                            {numberWithCommas(parseFloat(payload.total))} menciones
                                                        </h5>
                                                        <h5 className="toltipText">
                                                            {sentiment}
                                                        </h5>
                                                    </div>
                                                );
                                            }}
                                        />
                                    </div>
                                </div>
                                <div className="treemap-trends__footer">
                                    <h5>Positivo</h5>
                                    <h5>Neutral</h5>
                                    <h5>Negativo</h5>
                                </div>
                            </div>
                        )
                }
            </div>
        );
    }
}

TreeMapTopTrends.propTypes = propTypes;
TreeMapTopTrends.defaultProps = defaultProps;

export default withDrawer(TreeMapTopTrends);
