import React, { Component } from 'react';
import PropTypes from 'prop-types';
import isNull from 'lodash/isNull';

import setDates from '../../helpers/setDates';

import fetchTreeMapTrends from './TreeMapTrendsDataFlow';
import LoadingModule from '../LoadingModule';
import TreeMapTopTrends from './visualizations/TreeMapTopTrends';

const propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    dateRange: PropTypes.object,
    services: PropTypes.object,
    size: PropTypes.object,
    filters: PropTypes.object,
    withCache: PropTypes.bool,
};

const defaultProps = {
    fields: {},
    side: 'left',
    color: '#666',
    dateRange: {},
    services: {},
    size: {
        width: 600,
        height: 400
    },
    filters: {},
    withCache: true
};

class TreeMapTrends extends Component {
    constructor(props) {
        super(props);
        this.state = {
            moduleConfig: { ...props.fields, ...props.size },
            moduleServices: props.services,
            moduleRange: props.dateRange,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color
        };
    }

    componentDidMount() {
        this.updateDataFlow(this.props);
    }

    componentDidUpdate(prevProps) {
        const { startDate, endDate } = this.props.dateRange;
        const { fields: { hasClick, search_id } } = this.props;
        if (startDate !== prevProps.dateRange.startDate ||
            endDate !== prevProps.dateRange.endDate ||
            search_id !== prevProps.fields.search_id) {
            this.updateDataFlow(this.props);
        }
        if (hasClick !== prevProps.fields.hasClick) {
            this.changeState(hasClick);
        }
    }

    changeState(hasClick) {
        this.setState({
            moduleConfig: {
                ...this.state.moduleConfig,
                hasClick
            }
        });
    }

    updateDataFlow(props) {
        const { fields, dateRange, filters, withCache, size } = props;
        const params = {
            recipe_id: 'module_tw_tree_map',
            rule_id: fields.search_id,
            ...setDates(dateRange, withCache),
            filters: {
                top: fields.top_limit,
                ...filters
            }
        };

        fetchTreeMapTrends(props.services.twitter, params)
            .subscribe(
                (data) => {
                    this.setState({
                        moduleData: data,
                        moduleConfig: { ...fields, ...size },
                        moduleLoading: false,
                        moduleServices: props.services,
                        moduleColor: props.color,
                        moduleSide: props.side,
                        moduleRange: props.dateRange
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }


    renderByViz() {
        const { moduleConfig, moduleData } = this.state;
        if (!isNull(moduleData)) {
            const visualizations = {
                treemap_top_trends: params => (
                    <TreeMapTopTrends {...params} />
                )
            };
            return visualizations[this.props.view || moduleConfig.visualization](this.state);
        }
        return (<LoadingModule />);
    }

    render() {
        return (
            <div className="m-treemap-trends">
                {this.renderByViz()}
            </div>
        );
    }
}

TreeMapTrends.propTypes = propTypes;
TreeMapTrends.defaultProps = defaultProps;

export default TreeMapTrends;
