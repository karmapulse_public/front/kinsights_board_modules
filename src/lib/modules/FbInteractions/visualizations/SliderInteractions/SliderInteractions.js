import React from 'react';
import PropTypes from 'prop-types';
import { faFacebookF } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Slider from 'kpulse_design_system/dist/Carousel';

import {
    LikeIcon,
    LoveIcon,
    HahaIcon,
    WowIcon,
    SadIcon,
    AngryIcon
} from '../../../../helpers/icons/fb';

import { numberWithCommas } from '../../../../helpers/number';

import styles from './styles';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    noDataYet: PropTypes.func
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    noDataYet: null
};

const SliderInteractions = ({ moduleData, moduleConfig, moduleColor, noDataYet }) => {
    const {
        interactions_total,
        reactions_total,
        shares_total,
        comments_total,
        reactions,
        reactions_avg,
        shares_avg,
        comments_avg
    } = moduleData;

    const renderReactions = () => (
        <Slider width="90%" height="103px" withDots={false} arrowsColor="#d4c19c">
            <div className="slider__card">
                <h5>
                    <span>{numberWithCommas(comments_total)}</span>
                    <p>Comentarios</p>
                </h5>
                <h5>
                    <span>{numberWithCommas(shares_total)}</span>
                    <p>Compartidos</p>
                </h5>
                <h5>
                    <span>{numberWithCommas(reactions_total)}</span>
                    <p>Reacciones</p>
                </h5>
            </div>
            <div className="slider__card">
                <h5>
                    <span>{numberWithCommas(reactions.LIKE)}</span>
                    <LikeIcon />
                </h5>
                <h5>
                    <span>{numberWithCommas(reactions.LOVE)}</span>
                    <LoveIcon />
                </h5>
                <h5>
                    <span>{numberWithCommas(reactions.HAHA)}</span>
                    <HahaIcon />
                </h5>
            </div>
            <div className="slider__card">
                <h5>
                    <span>{numberWithCommas(reactions.WOW)}</span>
                    <WowIcon />
                </h5>
                <h5>
                    <span>{numberWithCommas(reactions.SAD)}</span>
                    <SadIcon />
                </h5>
                <h5>
                    <span>{numberWithCommas(reactions.ANGRY)}</span>
                    <AngryIcon />
                </h5>
            </div>
        </Slider>
    );

    const renderContent = () => (
        <div className="list-interactions__content">
            <h4>
                <span>{numberWithCommas(interactions_total)}</span>
                <p>Totales </p>
            </h4>
            <div style={{ position: 'relative' }}>
                {renderReactions()}
            </div>
        </div>
    );

    return (
        <div className="list-interactions" {...styles(moduleColor)}>
            <div className="list-interactions__label">
                <h3>INTERACCIONES FACEBOOK</h3>
                <div>
                    <FontAwesomeIcon icon={faFacebookF} color="white" />
                </div>
            </div>
            {noDataYet !== null ? noDataYet() : renderContent()}
        </div>
    );
};

SliderInteractions.propTypes = propTypes;
SliderInteractions.defaultProps = defaultProps;

export default SliderInteractions;
