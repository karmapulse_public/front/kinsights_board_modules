import { css } from 'glamor';

const styles = color => css({
    width: '100%',
    minWidth: '320px',
    maxWidth: '500px',
    ' h4': {
        fontSize: 13,
        fontWeight: 500,
        '&:not(:last-of-type)': {
            marginBottom: 20
        },
        '&:not(:first-of-type)': {
            color: 'rgba(0,0,0,0.5)'
        },
        ' span': {
            fontSize: 16,
            float: 'right',
            color: '#000'
        }
    },
    ' div:not(:first-of-type)': {
        padding: '30px 40px'
    },
    ' ul': {
        display: 'flex',
        flexWrap: 'wrap',
        alignItems: 'center',
        marginBottom: 40,
        ' li': {
            width: '33.3333%',
            marginTop: 20,
        },
        ' svg': {
            verticalAlign: 'middle'
        },
        ' span': {
            color: '#000',
            marginLeft: 10,
            fontSize: 12,
            fontWeight: 500,
            verticalAlign: 'middle'
        }
    },
    ' .list-interactions': {
        ' &__label': {
            display: 'flex',
            justifyContent: 'space-between',
            padding: '15px 20px 15px 20px',
            borderTop: '1px solid #dfdfdf',
            color,

            ' h3': {
                fontSize: '13px',
                fontWeight: 600
            },
        }
    }
});

export default styles;
