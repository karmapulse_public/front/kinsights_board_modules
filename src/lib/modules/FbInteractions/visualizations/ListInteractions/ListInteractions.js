import React from 'react';
import PropTypes from 'prop-types';

import {
    LikeIcon,
    LoveIcon,
    HahaIcon,
    WowIcon,
    SadIcon,
    AngryIcon
} from '../../../../helpers/icons/fb';

import { numberWithCommas } from '../../../../helpers/number';

import styles from './styles';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    noDataYet: PropTypes.func
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    noDataYet: null
};

const ListInteractions = ({ moduleData, moduleConfig, moduleColor, noDataYet }) => {
    const {
        interactions_total,
        reactions_total,
        shares_total,
        comments_total,
        reactions,
        reactions_avg,
        shares_avg,
        comments_avg
    } = moduleData;


    const renderReactions = () => (
        <ul>
            <li>
                <LikeIcon />
                <span>{numberWithCommas(reactions.LIKE)}</span>
            </li>
            <li>
                <LoveIcon />
                <span>{numberWithCommas(reactions.LOVE)}</span>
            </li>
            <li>
                <HahaIcon />
                <span>{numberWithCommas(reactions.HAHA)}</span>
            </li>
            <li>
                <WowIcon />
                <span>{numberWithCommas(reactions.WOW)}</span>
            </li>
            <li>
                <SadIcon />
                <span>{numberWithCommas(reactions.SAD)}</span>
            </li>
            <li>
                <AngryIcon />
                <span>{numberWithCommas(reactions.ANGRY)}</span>
            </li>
        </ul>
    );

    const renderContent = () => (
        <div className="list-interactions__content">
            <h4>
                Interacciones totales
                <span>{numberWithCommas(interactions_total)}</span>
            </h4>
            <h4>
                Comentarios totales
                <span>{numberWithCommas(comments_total)}</span>
            </h4>
            <h4>
                Contenido total compartido
                <span>{numberWithCommas(shares_total)}</span>
            </h4>
            <h4>
                Reacciones totales
                <span>{numberWithCommas(reactions_total)}</span>
            </h4>
            {renderReactions()}
            <h4>
                Promedio de comentarios por publicación
                <span>{numberWithCommas(Math.floor(comments_avg))}</span>
            </h4>
            <h4>
                Promedio de veces compartida por publicación
                <span>{numberWithCommas(Math.floor(shares_avg))}</span>
            </h4>
            <h4>
                Promedio reacciones por publicación
                <span>{numberWithCommas(Math.floor(reactions_avg))}</span>
            </h4>
        </div>
    );

    return (
        <div className="list-interactions" {...styles(moduleColor)}>
            <div className="list-interactions__label">
                <h3>{moduleConfig.title}</h3>
            </div>
            {noDataYet !== null ? noDataYet() : renderContent()}
        </div>
    );
};

ListInteractions.propTypes = propTypes;
ListInteractions.defaultProps = defaultProps;

export default ListInteractions;
