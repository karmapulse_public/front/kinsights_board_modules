import { css } from 'glamor';

const styles = (side, color) => css({
    minWidth: 300,
    height: '100%',
    color: 'rgba(0,0,0, 0.5)',

    ' .mentions': {

        ' &__title': {
            '>h3': {
                padding: '15px 20px',
                textTransform: 'uppercase',
                fontSize: 16,
                fontWeight: 400,
                textAlign: side,
                color
            }
        },

        ' &__body': {
            padding: '0 30px 30px',

            ' &__total': {
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',

                ' >div': {
                    display: 'flex',
                    alignItems: 'center',

                    ' h4': {
                        paddingLeft: 10,
                        fontSize: 20,
                        fontWeight: 500,
                        color: '#e4006a'
                    },
                },

                ' h5': {
                    paddingTop: 5,
                    fontSize: 12
                }
            },

            ' &__numbers': {
                display: 'flex',
                justifyContent: 'center',
                paddingTop: 30,

                ' div': {
                    textAlign: 'center',
                    width: '33.33333%',

                    ' h4': {
                        fontSize: 16,
                        fontWeight: 700,
                        color: '#000000'
                    },

                    ' h5': {
                        paddingTop: 5,
                        fontSize: 12
                    },

                    ' &:nth-child(2)': {
                        borderLeft: '1px solid #e4e4e4',
                        borderRight: '1px solid #e4e4e4'
                    }
                }
            }
        }
    }
});

export default styles;
