import React from 'react';
import PropTypes from 'prop-types';

import { numberWithCommas } from '../../../../helpers/number';
import styles from './cardTotalNumbersStyles';
import ConversationIcon from '../../../../helpers/icons/ceo/conversationIcon';
import NoDataModule from '../../../NoDataModule';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleSide: PropTypes.string,
    moduleColor: PropTypes.string,
    noDataYet: PropTypes.func
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleSide: 'left',
    moduleColor: '#666',
    noDataYet: null,
};

const CardTotalNumbers = ({ moduleConfig, moduleData, moduleSide, moduleColor, noDataYet }) => {
    // New logic -> add originals with replies


    const { title = 'RELEVANCIA SOCIAL' } = moduleConfig;

    return (
        <div className="card-total-numbers" {...styles(moduleSide, moduleColor)} >
            <div className="mentions">
                <div className="mentions__title">
                    <h3>{title}</h3>
                </div>
                {
                    (() => {
                        if (noDataYet !== null) {
                            return noDataYet();
                        }
                        return (moduleData.total !== 0)
                            ? (
                                <div className="mentions__body">
                                    <div className="mentions__body__total">
                                        <div>
                                            <ConversationIcon color="#e4006a" />
                                            <h4>{numberWithCommas(moduleData.total)}</h4>
                                        </div>
                                        <h5>Publicaciones totales</h5>
                                    </div>
                                    <div className="mentions__body__numbers">
                                        <div>
                                            <h4>
                                                {numberWithCommas(moduleData.total_video)}
                                            </h4>
                                            <h5>Video</h5>
                                        </div>
                                        <div>
                                            <h4>
                                                {numberWithCommas(moduleData.total_image)}
                                            </h4>
                                            <h5>Imagen</h5>
                                        </div>
                                        <div>
                                            <h4>
                                                {numberWithCommas(moduleData.total_gallery)}
                                            </h4>
                                            <h5>Galería</h5>
                                        </div>
                                    </div>
                                </div>
                            )
                            : <NoDataModule color={moduleColor} />;
                    })()
                }
            </div>
        </div>
    );
};

CardTotalNumbers.propTypes = propTypes;
CardTotalNumbers.defaultProps = defaultProps;

export default CardTotalNumbers;
