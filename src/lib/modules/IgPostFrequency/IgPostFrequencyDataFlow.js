import { Observable } from 'rxjs';
import { fetchXHR } from '../../helpers/http';


export default (url, params) => (
    Observable.create((observer) => {
        Observable.fromPromise(fetchXHR(url, 'GET', params))
            .subscribe(
                (data) => {
                    observer.next(Object.assign(
                        {
                            total: data.json.response.total,
                            total_gallery: data.json.response.total_gallery,
                            total_image: data.json.response.total_image,
                            total_video: data.json.response.total_video,
                        },
                    ));
                },
                (error) => { observer.error(error); }
            );
    })
);
