import React, { Component } from 'react';
import PropTypes from 'prop-types';
import clone from 'lodash/clone';
import moment from 'moment-timezone';
import fetchTotalInstagram from './IgPostFrequencyDataFlow';

import LoadingModule from '../LoadingModule';
import NoDataYet from '../NoDataYet';
import CardTotalNumbers from './visualizations/CardTotalNumbers';

const propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    dateRange: PropTypes.object,
    services: PropTypes.object,
    showNoDataYet: PropTypes.bool
};

const defaultProps = {
    fields: {},
    side: 'left',
    color: '#666',
    dateRange: {},
    services: {},
    showNoDataYet: false
};

class IgPostFrequency extends Component {
    constructor(props) {
        super(props);

        this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color
        };
    }

    componentDidMount() {
        let { startDate, endDate } = this.props.dateRange;

        const tz = 'America/Mexico_City';
        const strStartDate = clone(startDate).format('YYYY-MM-DD hh:mm:ss');
        const strEndDate = clone(endDate).format('YYYY-MM-DD hh:mm:ss');

        const utcOffsetStart = moment.tz(strStartDate, tz).isDST() ? '-05:00' : '-06:00';
        const utcOffsetEnd = moment.tz(strEndDate, tz).isDST() ? '-05:00' : '-06:00';

        startDate = startDate.utcOffset(utcOffsetStart).set({
            hour: 0,
            minute: 0,
            second: 0,
            millisecond: 0
        }).toISOString();

        endDate = endDate.utcOffset(utcOffsetEnd).set({
            minute: 5 * (Math.floor(moment().minute() / 5)),
            second: 0,
            millisecond: 0
        }).toISOString();

        const params = {
            recipe_id: 'module_ig_total_media',
            rule_id: this.state.moduleConfig.search_id,
            initial_date: startDate,
            final_date: endDate
        };

        fetchTotalInstagram(this.props.services.twitter, params)
            .subscribe(
                (data) => {
                    this.setState({
                        moduleData: data,
                        moduleLoading: false
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    componentWillReceiveProps(nextProp) {
        let { startDate, endDate } = nextProp.dateRange;

        const tz = 'America/Mexico_City';
        const strStartDate = clone(startDate).format('YYYY-MM-DD hh:mm:ss');
        const strEndDate = clone(endDate).format('YYYY-MM-DD hh:mm:ss');

        const utcOffsetStart = moment.tz(strStartDate, tz).isDST() ? '-05:00' : '-06:00';
        const utcOffsetEnd = moment.tz(strEndDate, tz).isDST() ? '-05:00' : '-06:00';

        startDate = startDate.utcOffset(utcOffsetStart).set({
            hour: 0,
            minute: 0,
            second: 0,
            millisecond: 0
        }).toISOString();

        endDate = endDate.utcOffset(utcOffsetEnd).set({
            hour: 23,
            minute: 59,
            second: 59,
            millisecond: 999
        }).toISOString();

        const params = {
            recipe_id: 'module_ig_total_media',
            rule_id: nextProp.fields.search_id,
            initial_date: startDate,
            final_date: endDate
        };

        fetchTotalInstagram(this.props.services.twitter, params)
            .subscribe(
                (data) => {
                    this.setState({
                        moduleConfig: nextProp.fields,
                        moduleData: data,
                        moduleLoading: false,
                        moduleSide: nextProp.side,
                        moduleColor: nextProp.color
                    });

                    this.forceUpdate();
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    renderByViz() {
        const { moduleConfig, moduleData } = this.state;
        const { dateRange: { startDate, endDate }, showNoDataYet } = this.props;
        let renderNoDataYet = null;

        if (showNoDataYet && moment().diff(endDate, 'minutes') <= 0 && endDate.diff(startDate, 'days') <= 1) {
            renderNoDataYet = () => <NoDataYet />;
        }
        if (moduleData) {
            const visualizations = {
                card_total_numbers: params => (
                    <CardTotalNumbers {...params} noDataYet={renderNoDataYet} />
                )
            };

            return visualizations[moduleConfig.visualization](this.state);
        }

        return (<LoadingModule />);
    }

    render() {
        return (
            <div className="m-total-tweets">
                {this.renderByViz()}
            </div>
        );
    }
}

IgPostFrequency.propTypes = propTypes;
IgPostFrequency.defaultProps = defaultProps;

export default IgPostFrequency;
