import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import {
    LineChart,
    Line,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Label,
    ResponsiveContainer
} from 'recharts';
import styles from './styles';
import NoDataModule from '../../../NoDataModule';
import AxisX from '../../../Charts/CustomComponents/AxisX';
import AxisY from '../../../Charts/CustomComponents/AxisY';
import TooltipCustom from '../../../Charts/CustomComponents/TooltipCustom';
import { numberWithCommas } from '../../../../helpers/number';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    viewLabels: PropTypes.string,
    moduleColor: PropTypes.string,
    intervalType: PropTypes.string,
    colors: PropTypes.object
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    viewLabels: '',
    intervalType: 'day',
    colors: {
        twitter: '#00acee',
        facebook: '#3b5998',
        // instagram: '#bc2a8d',
    }
};

const TimeLine = (props) => {
    const {
        moduleConfig, moduleData,
        moduleColor, colors, intervalType
    } = props;
    const { title = 'SENTIMIENTO EN EL TIEMPO' } = moduleConfig;
    const dataKeys = [
        {
            key: 'twitter',
            color: '#00acee'
        },
        {
            key: 'facebook',
            color: '#3b5998'
        },
        // {
        //     key: 'instagram',
        //     color: '#bc2a8d'
        // }
    ];
    const nameArray = ['twitter', 'facebook'];//, 'instagram'];
    const labels = moduleConfig.labelTitles || { x: 'Periodo', y: 'Volumen de datos' };

    const renderTooltip = innerProps => (
        <TooltipCustom
            {...innerProps}
            noUtc
        />
    );

    const renderLines = () => (
        <ResponsiveContainer>
            <LineChart
                data={moduleData.data}
                margin={{
                    top: 30, right: 30, left: 0, bottom: 25
                }}
            >
                <XAxis
                    dataKey="key"
                    tickLine={false}
                    stroke="rgba(230,236,240, 0.15)"
                    tick={
                        <AxisX
                            style={{ fontSize: 10, fontWeight: 600, stroke: 'rgba(0, 0, 0, 0.8)', strokeWidth: '0.05px' }}
                            color="#000"
                            intervalType={intervalType}
                            noUtc
                        />
                    }
                >
                    <Label value={labels.x} className="line-data__label" offset={-10} position="insideBottom" />
                </XAxis>
                <YAxis
                    axisLine={false}
                    tick={<AxisY color="#000" style={{ fontSize: '0.625em' }} />}
                >
                    <Label value={labels.y} className="line-data__label" offset={-15} angle={-90} position="left" />
                </YAxis>
                <CartesianGrid vertical={false} />
                <Tooltip content={renderTooltip} intervalType={intervalType} />
                {
                    nameArray.map(item => (
                        <Line
                            type="linear"
                            strokeWidth={2}
                            key={item}
                            dataKey={item}
                            stroke={colors[item]}
                            dot
                            activeDot={{ stroke: colors[item], strokeWidth: 0 }}
                        />
                    ))
                }
            </LineChart>
        </ResponsiveContainer>
    );
    return (
        <div className="multisrc-timeline-chart" {...styles}>
            {
                (moduleData.total === 0)
                    ? (
                        <div className="noData">
                            <NoDataModule color={moduleColor} />
                        </div>
                    )
                    : (
                        <Fragment>
                            <div className="multisrc-timeline__chart">
                                {renderLines()}
                            </div>
                            <div className="multisrc-timeline__totals">
                                <div>
                                    <h5 style={{ color: dataKeys[0].color }}>
                                        {numberWithCommas(moduleData.total_twitter)}
                                    </h5>
                                    <h6>{moduleConfig.totalTitles ? moduleConfig.totalTitles.tw : 'Twitter'}</h6>
                                </div>
                                <div>
                                    <h5 style={{ color: dataKeys[1].color }}>
                                        {numberWithCommas(moduleData.total_facebook)}
                                    </h5>
                                    <h6>{moduleConfig.totalTitles ? moduleConfig.totalTitles.fb : 'Facebook'}</h6>
                                </div>
                                {/* <div>
                                    <h5 style={{ color: dataKeys[2].color }}>
                                        {numberWithCommas(moduleData.total_instagram)}
                                    </h5>
                                    <h6>{moduleConfig.totalTitles ? moduleConfig.totalTitles.in : 'Instagram'}</h6>
                                </div> */}
                            </div>
                        </Fragment>
                    )
            }
        </div>
    );
};

TimeLine.propTypes = propTypes;
TimeLine.defaultProps = defaultProps;

export default TimeLine;
