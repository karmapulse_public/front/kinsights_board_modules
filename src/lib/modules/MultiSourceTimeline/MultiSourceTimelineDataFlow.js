import { Observable } from 'rxjs';
import moment from 'moment';
import paddingDates from '../../helpers/paddingDates';
import { fetchXHR } from '../../helpers/http';

const mapData = (data) => {
    let maxKey = 'twitter';
    Object.keys(data).forEach((key) => {
        maxKey = data[key].length < data[maxKey].length ? maxKey : key;
    });

    return data[maxKey].map((item, index) => {
        const values = (() => {
            const objectResult = {};
            Object.keys(data).forEach((key) => {
                const elto = data[key];
                objectResult[key] = elto.length ? elto[index].total : 0;
            });
            return objectResult;
        })();
        return {
            key: moment(item.date).format(),
            ...values
        };
    });
};

export default (url, params) => (
    Observable.create((observer) => {
        Observable.fromPromise(fetchXHR(url, 'GET', params))
            .subscribe(
                (data) => {
                    const dataDura = data.json.response;
                    const filters = {
                        filters: { interval: 'day' }
                    };
                    const padDataDura = {
                        twitter: paddingDates(dataDura.twitter_timeline, { ...params, ...filters }),
                        facebook: paddingDates(dataDura.facebook_timeline, { ...params, ...filters }),
                        // instagram: paddingDates(dataDura.instagram_timeline, { ...params, ...filters }),
                    };
                    observer.next({
                        data: mapData(padDataDura),
                        total_twitter: dataDura.total_twitter,
                        total_facebook: dataDura.total_facebook,
                        // total_instagram: dataDura.total_instagram,
                    });
                },
                (error) => { observer.error(error); }
            );
    })
);