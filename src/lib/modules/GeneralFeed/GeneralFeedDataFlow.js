import { Observable } from 'rxjs';
import { fetchXHR } from '../../helpers/http';

export default (url, params) => (
    Observable.create((observer) => {
        Observable.fromPromise(fetchXHR(url, 'GET', params))
            .subscribe(
                (data) => {
                    observer.next({
                        data: data.json.response
                    });
                },
                (error) => { observer.error(error); }
            );
    })
);