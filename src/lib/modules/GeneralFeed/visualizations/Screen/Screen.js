import React from 'react';
import PropTypes from 'prop-types';
import groupBy from 'lodash/groupBy';
import isUndefined from 'lodash/isUndefined';
import './masonry.css';
// eslint-disable-next-line import/first
import Masonry from 'react-masonry-css';

import Header from '../../../../helpers/customComponents/Header';
import Footer from '../../../../helpers/customComponents/Footer';
import BodyText from '../../../../helpers/customComponents/BodyText';
import Card from '../../../../helpers/customComponents/Card';
import styles from './ScreenStyles';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    viewLabels: PropTypes.string,
    moduleColor: PropTypes.string,
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    viewLabels: '',
};

const ID = () => `_${Math.random().toString(36).substr(2, 9)}`;

const List = (props) => {

    const isSource = (source, item) => {
        const t = {
            instagram: () => {
                if (item.media_type === 'IMAGE') {
                    return [item.media_url, item.media_type, 'INSTAGRAM'];
                }
                if (item.media_type === 'VIDEO') {
                    return [item.media_url, item.media_type, 'INSTAGRAM'];
                }
                if (item.media_type === 'CAROUSEL_ALBUM') {
                    const e = item.images[0];
                    return isUndefined(e.url) ? [e.media_url, item.media_type, 'INSTAGRAM'] : [e.url, item.media_type, 'INSTAGRAM'];
                }
                return null;
            },
            facebook: () => {
                if (item.type === 'photo' || item.type === 'video') {
                    const e = item.images[0];
                    return isUndefined(e.url)
                        ? [e.media_url, item.type.toUpperCase(), 'FACEBOOK']
                        : [e.url, item.type.toUpperCase(), 'FACEBOOK'];
                }
                return '';
            },
            twitter: () => {
                if (item.media_type === 'image') {
                    const e = item.images[0];
                    return isUndefined(e.url)
                        ? [e.media_url, item.media_type.toUpperCase(), 'TWITTER']
                        : [e.url, item.media_type.toUpperCase(), 'TWITTER'];
                }
                if (item.media_type === 'gif') {
                    return [item.gif.cover, item.media_type.toUpperCase(), 'TWITTER'];
                }
                if (item.media_type === 'video') {
                    return [item.video.cover, item.media_type.toUpperCase(), 'TWITTER'];
                }
                return [];
            }
        };
        return t[source]();
    };
    const noSource = Object.keys(groupBy(props.moduleData.data, i => i.source_type)).length <= 1;
    return (
        <div {...styles()}>
            <Masonry
                breakpointCols={3}
                className="my-masonry-grid"
                columnClassName="my-masonry-grid_column"
            >
                {props.moduleData.data.map((item) => {
                    const type = isSource(item.source_type, item);
                    return (
                        <Card key={`${item.id}${ID()}`} isScreen link={item.link}>
                            <Header
                                noDate
                                noSource={noSource}
                                isScreen
                                item={item}
                                services={props.moduleConfig}
                            />
                            <div>
                                <BodyText text={item.body} type={item.source_type} />
                                {/* <Sentiment item={item} noSource={noSource} /> */}
                                <Footer item={item} type={type[1]} isScreen />
                            </div>
                        </Card>
                    );
                })}
            </Masonry>
        </div>
    );
};

List.propTypes = propTypes;
List.defaultProps = defaultProps;

export default List;
