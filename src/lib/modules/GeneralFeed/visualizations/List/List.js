import React from 'react';
import PropTypes from 'prop-types';
import groupBy from 'lodash/groupBy';

import Gallery from '../../../../helpers/customComponents/Gallery';
import Header from '../../../../helpers/customComponents/Header';
import Sentiment from '../../../../helpers/customComponents/Sentiment';
import BodyText from '../../../../helpers/customComponents/BodyText';
import Card from '../../../../helpers/customComponents/Card';
import Video from '../../../../helpers/customComponents/Video';
import Link from '../../../../helpers/customComponents/Link';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    viewLabels: PropTypes.string,
    moduleColor: PropTypes.string,
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    viewLabels: '',
};


// <Gallery items={item.} />
const List = (props) => {
    const isSource = (source, item) => {
        const t = {
            instagram: () => (
                <React.Fragment>
                    {(() => {
                        if (item.media_type === 'IMAGE') {
                            return (
                                <Gallery
                                    items={item.images}
                                    height="300px"
                                    type={source}
                                    url={item.media_url}
                                />
                            );
                        }
                        if (item.media_type === 'VIDEO') {
                            const soruces = [{
                                url: item.media_url
                            }];
                            return (
                                <Video
                                    sources={soruces}
                                    height="300px"
                                />
                            );
                        }
                        if (item.media_type === 'CAROUSEL_ALBUM') {
                            return (
                                <Gallery
                                    items={item.images}
                                    height="300px"
                                    type={source}
                                    url={item.media_url}
                                />
                            );
                        }
                        return null;
                    })()}
                    <BodyText text={item.body} type={source} />
                </React.Fragment>
            ),
            facebook: () => (
                <React.Fragment>
                    <BodyText text={item.body} type={source} />
                    {(() => {
                        if (item.type === 'photo') {
                            return (
                                <Gallery
                                    items={item.images}
                                    height="300px"
                                    type={source}
                                    url={item.attachment.url}
                                />
                            );
                        }
                        if (item.type === 'video') {
                            return (
                                <Gallery
                                    items={item.images}
                                    height="300px"
                                    type={source}
                                    video
                                    url={item.attachment.url}
                                />
                            );
                        }
                        if (item.type === 'link') {
                            return (
                                // eslint-disable-next-line jsx-a11y/anchor-is-valid
                                <Link
                                    url={item.attachment.url}
                                    imageUrl={item.attachment.media}
                                    title={item.attachment.title}
                                    text={item.attachment.description}
                                    type={source}
                                />
                            );
                        }
                        return null;
                    })()}
                </React.Fragment>
            ),
            twitter: () => (
                <React.Fragment>
                    <BodyText text={item.body} type={source} />
                    {(() => {
                        if (item.media_type === 'image') {
                            return (
                                <Gallery
                                    items={item.images}
                                    height="300px"
                                    type={source}
                                    url={item.link}
                                />
                            );
                        }
                        if (item.media_type === 'gif') {
                            return (
                                <Video
                                    sources={JSON.parse(item.gif.variants)}
                                    cover={item.gif.cover}
                                    type={source}
                                    gif
                                />
                            );
                        }
                        if (item.media_type === 'video') {
                            return (
                                <Video
                                    sources={JSON.parse(item.video.variants)}
                                    cover={item.video.cover}
                                    type={source}
                                />
                            );
                        }
                        if (item.urls) {
                            return (
                                // eslint-disable-next-line jsx-a11y/anchor-is-valid
                                <Link
                                    url={item.urls[0].url}
                                    title={item.urls[0].title}
                                    text={item.urls[0].description}
                                    type={source}
                                />
                            );
                        }
                        return null;
                    })()}
                </React.Fragment>
            )
        };
        // console.log(t[source]);
        return t[source]();
    };
    const noSource = Object.keys(groupBy(props.moduleData.data, i => i.source_type)).length <= 1;
    return (
        <React.Fragment>
            {props.moduleData.data.map(item => (
                <Card key={item.id}>
                    <Header noSource={noSource} item={item} services={props.moduleConfig} />
                    {
                        isSource(item.source_type, item)
                    }
                    <Sentiment item={item} noSource={noSource} />
                </Card>
            ))}
        </React.Fragment>
    );
};

List.propTypes = propTypes;
List.defaultProps = defaultProps;

export default List;
