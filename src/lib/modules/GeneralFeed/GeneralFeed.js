import React, { Component } from 'react';
import PropTypes from 'prop-types';

import clone from 'lodash/clone';
import moment from 'moment-timezone';
import fetchGeneralFeed from './GeneralFeedDataFlow';

import LoadingModule from '../LoadingModule';
import List from './visualizations/List';
import Screen from './visualizations/Screen';

const propTypes = {
    side: PropTypes.string,
    color: PropTypes.string,
    fields: PropTypes.object,
    dateRange: PropTypes.object,
    services: PropTypes.object,
    filters: PropTypes.object,
    viewLabels: PropTypes.string,
    tw_search_id: PropTypes.string,
    fb_search_id: PropTypes.string,
    in_search_id: PropTypes.string,
};

const defaultProps = {
    side: 'left',
    color: '#666',
    fields: {},
    dateRange: {},
    services: {},
    filters: {},
    viewLabels: '',
    tw_search_id: '',
    fb_search_id: '',
    in_search_id: '',
};

class GeneralFeed extends Component {
    constructor(props) {
        super(props);
        this.state = {
            moduleConfig: Object.assign({}, props.fields, props.services),
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color
        };
    }

    componentDidMount() {
        const { dateRange, tw_search_id, fb_search_id, in_search_id } = this.props;
        let { startDate, endDate } = dateRange;

        const params = {
            recipe_id: 'module_general_feed',
            'rule-id': tw_search_id, // twitter rule id
            'facebook-rule-id': fb_search_id,
            'instagram-rule-id': in_search_id,
        };

        if (dateRange && Object.keys(dateRange).length > 0) {
            const tz = 'America/Mexico_City';
            const strStartDate = clone(startDate).format('YYYY-MM-DD hh:mm:ss');
            const strEndDate = clone(endDate).format('YYYY-MM-DD hh:mm:ss');

            const utcOffsetStart = moment.tz(strStartDate, tz).isDST() ? '-05:00' : '-06:00';
            const utcOffsetEnd = moment.tz(strEndDate, tz).isDST() ? '-05:00' : '-06:00';

            startDate = startDate.utcOffset(utcOffsetStart).set({
                hour: 0,
                minute: 0,
                second: 0,
                millisecond: 0
            }).toISOString();

            endDate = endDate.utcOffset(utcOffsetEnd).set({
                minute: 5 * (Math.floor(moment().minute() / 5)),
                second: 0,
                millisecond: 0
            }).toISOString();

            params.initial_date = startDate;
            params.final_date = endDate;
        }

        if (this.props.filters) {
            params.filters = {
                ...this.props.filters
            };
        }

        fetchGeneralFeed(this.props.services.twitter, params)
            .subscribe(
                (data) => {
                    this.setState({
                        moduleData: data
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    componentWillReceiveProps(nextProp) {
        let { startDate, endDate } = nextProp.dateRange;
        const { tw_search_id, fb_search_id, in_search_id } = nextProp;

        const params = {
            recipe_id: 'module_general_feed',
            'rule-id': tw_search_id, // twitter rule id
            'facebook-rule-id': fb_search_id,
            'instagram-rule-id': in_search_id,
        };


        if (nextProp.dateRange && Object.keys(nextProp.dateRange).length > 0) {
            const tz = 'America/Mexico_City';
            const strStartDate = clone(startDate).format('YYYY-MM-DD hh:mm:ss');
            const strEndDate = clone(endDate).format('YYYY-MM-DD hh:mm:ss');

            const utcOffsetStart = moment.tz(strStartDate, tz).isDST() ? '-05:00' : '-06:00';
            const utcOffsetEnd = moment.tz(strEndDate, tz).isDST() ? '-05:00' : '-06:00';

            startDate = startDate.utcOffset(utcOffsetStart).set({
                hour: 0,
                minute: 0,
                second: 0,
                millisecond: 0
            }).toISOString();

            endDate = endDate.utcOffset(utcOffsetEnd).set({
                minute: 5 * (Math.floor(moment().minute() / 5)),
                second: 0,
                millisecond: 0
            }).toISOString();

            params.initial_date = startDate;
            params.final_date = endDate;
        }

        if (this.props.filters) {
            params.filters = {
                ...this.props.filters
            };
        }

        fetchGeneralFeed(this.props.services.twitter, params)
            .subscribe(
                (data) => {
                    this.setState({
                        moduleConfig: Object.assign({}, nextProp.fields, nextProp.services),
                        moduleData: data,
                        moduleLoading: false,
                        moduleSide: nextProp.side,
                        moduleColor: nextProp.color
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    renderByViz() {
        const { moduleConfig, moduleData } = this.state;

        if (moduleData) {
            const visualizations = {
                list: params => (
                    <List {...params} />
                ),
                screen: params => (
                    <Screen {...params} />
                ),
            };
            return visualizations[moduleConfig.visualization](this.state);
        }

        return (<LoadingModule />);
    }

    render() {
        return (
            <div className="m-general-feed">
                {this.renderByViz()}
            </div>
        );
    }
}

GeneralFeed.propTypes = propTypes;
GeneralFeed.defaultProps = defaultProps;

export default GeneralFeed;
