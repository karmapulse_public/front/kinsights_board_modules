import React, { Component } from 'react';
import PropTypes from 'prop-types';

import setDates from '../../helpers/setDates';

import fetchTopImages from './TopImagesDataFlow';

import LoadingModule from '../LoadingModule';
import ContentGallery from './visualizations/ContentGallery';
import ContentCarousel from './visualizations/ContentCarousel';
import ContentGrid from './visualizations/ContentGrid';

const propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    services: PropTypes.object,
    dateRange: PropTypes.object,
    filters: PropTypes.object,
    size: PropTypes.number,
    withCache: PropTypes.bool
};

const defaultProps = {
    fields: {},
    side: 'left',
    color: '#666',
    services: {},
    dateRange: {},
    filters: {},
    size: 0,
    withCache: true
};

class TopImages extends Component {
    constructor(props) {
        super(props);

        this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color
        };
    }

    componentDidMount() {
        let filters = {};
        if (this.props.size > 0) {
            filters.size = this.props.size;
        }
        filters = {
            ...filters,
            ...this.props.filters
        };
        const params = {
            recipe_id: 'module_tw_top_images',
            rule_id: this.state.moduleConfig.search_id,
            ...setDates(this.props.dateRange, this.props.withCache),
            filters: {
                ...filters
            },
        };

        if (this.props.size) {
            params.size = this.props.size;
        }

        const showChildTweet = this.state.moduleConfig.show_child_tweet || false;

        fetchTopImages(this.props.services.twitter, params, showChildTweet)
            .subscribe(
                data => this.setState({
                    moduleData: data,
                    moduleLoading: false
                }),
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    componentWillReceiveProps(nextProp) {
        let filters = {};
        if (nextProp.size > 0) {
            filters.size = nextProp.size;
        }
        filters = {
            ...filters,
            ...nextProp.filters
        };
        const params = {
            recipe_id: 'module_tw_top_images',
            rule_id: nextProp.fields.search_id,
            ...setDates(nextProp.dateRange, this.props.withCache),
            filters: { ...filters }
        };

        if (this.props.size) {
            params.size = this.props.size;
        }

        const showChildTweet = this.state.moduleConfig.show_child_tweet || false;

        fetchTopImages(nextProp.services.twitter, params, showChildTweet)
            .subscribe(
                (data) => {
                    this.setState({
                        moduleConfig: nextProp.fields,
                        moduleData: data,
                        moduleLoading: false,
                        moduleSide: nextProp.side,
                        moduleColor: nextProp.color
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    renderByViz() {
        const { moduleConfig, moduleData } = this.state;

        if (moduleData) {
            const visualizations = {
                content_gallery: params => (
                    <ContentGallery {...params} />
                ),
                content_carousel: params => (
                    <ContentCarousel {...params} />
                ),
                content_grid: params => (
                    <ContentGrid {...params} />
                )
            };

            return visualizations[this.props.view || moduleConfig.visualization](this.state);
        }

        return (<LoadingModule />);
    }

    render() {
        return (
            <div className="m-shared-content">
                {this.renderByViz()}
            </div>
        );
    }
}

TopImages.propTypes = propTypes;
TopImages.defaultProps = defaultProps;

export default TopImages;
