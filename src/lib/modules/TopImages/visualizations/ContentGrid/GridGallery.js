import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { css } from 'glamor';


import NoDataModule from '../../../NoDataModule';
import styles from './gridGalleryStyles';
import imageDefault from '../../../../helpers/icons/imageDeleted';
import TwitterLogo from '../../../../helpers/icons/twitterLogoIcon';
import { numberWithCommas } from '../../../../helpers/number';
import ModalDialog from '../../../ModalDialog';

const propTypes = {
    moduleData: PropTypes.array,
    moduleConfig: PropTypes.object,
    moduleColor: PropTypes.string,
    moduleSide: PropTypes.string
};

const defaultProps = {
    moduleData: [],
    moduleConfig: {},
    moduleColor: '#666',
    moduleSide: 'left'
};

class GridGallery extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openModal: false,
            imageInModal: {},
            images: [],
            tweetActive: 0
        };
        this.handleOpenModal = this.handleOpenModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.renderModal = this.renderModal.bind(this);
    }

    handleOpenModal(item) {
        this.setState({
            openModal: true,
            imageInModal: item
        });
    }
    handleCloseModal() {
        this.setState({
            openModal: false
        });
    }

    renderModal() {
        const { imageInModal } = this.state;
        if (this.state.openModal) {
            const userInfo = {
                image: imageInModal.tweet.image,
                name: imageInModal.tweet.displayName,
                username: imageInModal.tweet.preferredUsername,
            };
            return (
                <ModalDialog
                    handleCloseModal={this.handleCloseModal}
                    tweetId={imageInModal.tweet.id}
                    postedTime={imageInModal.tweet.postedTime}
                    image={imageInModal.topImage}
                    user={userInfo}
                    body={imageInModal.tweet.body}
                    link={imageInModal.tweet.link}
                    showActions
                />
            );
        }
        return '';
    }

    renderImages() {
        const { tweetActive } = this.state;
        return (
            this.props.moduleData.map(
                (item, index) => {
                    const backgroundImage = css({
                        backgroundImage: `url(${item.topImage ? item.topImage.replace('http:', 'https:') : ''}), url(${imageDefault})`,
                        backgroundPosition: 'center',
                        backgroundSize: 'cover',
                        opacity: tweetActive === index ? 1 : 0.6
                    });
                    return (
                        <div
                            onClick={() => { this.handleOpenModal(item); }}
                            key={index}
                            className="item"
                            {...backgroundImage}
                        >
                            <div className="item__tag">
                                <TwitterLogo />
                                <span>
                                    {numberWithCommas(item.total)}
                                </span>
                            </div>
                        </div>
                    );
                }
            )
        );
    }

    render() {
        const { title = 'GALERÍA' } = this.props.moduleConfig;
        const { moduleData } = this.props;

        return (
            <div className="grid-gallery" {...styles()}>
                <div className="grid-gallery__label">
                    <h3>
                        {title}
                    </h3>
                </div>
                <div className="grid-gallery__images">
                    {moduleData.length ? this.renderImages() : <NoDataModule />}
                </div>
                {this.renderModal()}
            </div>
        );
    }
}

GridGallery.propTypes = propTypes;
GridGallery.defaultProps = defaultProps;

export default GridGallery;
