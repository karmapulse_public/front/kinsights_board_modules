import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import NoDataModule from '../../../NoDataModule';
import { numberWithCommas } from '../../../../helpers/number';
import formatBodyText from '../../../../helpers/formatBodyText';
import RtIcon from '../../../../helpers/icons/rtIcon';
import LikeIcon from '../../../../helpers/icons/ceo/likesIcon';

import {
    AngleLeft,
    AngleRight
} from '../../../../helpers/icons/gob';
import styles from './contentCarouselStyles';

const propTypes = {
    moduleData: PropTypes.array,
    moduleConfig: PropTypes.object,
    moduleColor: PropTypes.string,
    moduleSide: PropTypes.string
};

const defaultProps = {
    moduleData: [],
    moduleConfig: {},
    moduleColor: '#666',
    moduleSide: 'left'
};

class ContentCarousel extends Component {

    constructor(props) {
        super(props);

        this.state = {
            tweetActive: 0,
        };
        this.handleNext = this.handleNext.bind(this);
        this.handlePrev = this.handlePrev.bind(this);
    }

    handlePrev() {
        if (this.state.tweetActive > 0) {
            this.setState({
                tweetActive: this.state.tweetActive - 1
            });
        }
    }

    handleNext() {
        const tweetActive = this.state.tweetActive + 1;
        if (tweetActive <= this.props.moduleData.length - 1 && this.state.tweetActive < 2) {
            this.setState({
                tweetActive
            });
        }
    }

    renderTweetActive() {
        const { tweetActive } = this.state;
        const tweet = this.props.moduleData[tweetActive];
        const tweetDate = moment(tweet.tweet.postedTime).format('DD MMM YY');
        return (
            <div className="carousel__tweet" >
                <h4>
                    <span>{`${tweetActive + 1}.`}</span>
                    <div>
                        <span>@{tweet.tweet.preferredUsername}</span>
                    </div>
                    <div>
                        <span>{tweetDate}</span>
                    </div>
                </h4>
                <a
                    href={tweet.tweet.link}
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    <div
                        style={{ backgroundImage: `url(${tweet.topImage ? tweet.topImage.replace('http:', 'https:') : ''})` }}
                    />
                    <p
                        dangerouslySetInnerHTML={{
                            __html: formatBodyText(tweet.tweet.body)
                        }}
                    />
                </a>
                <div className="tweet__interactions">
                    <div>
                        <RtIcon color="rgba(0,0,0,0.5)" />
                        <div>
                            <span>Retweets</span>
                            <span>{numberWithCommas(tweet.tweet.retweets)}</span>
                        </div>
                    </div>
                    <div>
                        <LikeIcon color="rgba(0,0,0,0.5)" />
                        <div>
                            <span>Favoritos</span>
                            <span>{numberWithCommas(tweet.tweet.favorites)}</span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderControls() {
        let opacityNext = 1;
        if (this.props.moduleData.length - 1 === this.state.tweetActive || this.state.tweetActive === 2) {
            opacityNext = 0.2;
        }

        return (
            <div className="controls">
                <button
                    onClick={this.handlePrev}
                >
                    <AngleLeft
                        size="13px"
                        color="#000"
                        opacity={this.state.tweetActive === 0 ? 0.2 : 1}
                    />
                </button>
                <button
                    onClick={this.handleNext}
                >
                    <AngleRight
                        size="13px"
                        color="#000"
                        opacity={opacityNext}
                    />
                </button>
            </div>
        );
    }

    render() {
        const { title = 'CONTENIDO MÁS COMPARTIDO' } = this.props.moduleConfig;
        return (
            <div
                className="content_carousel"
                {...styles(this.props.moduleSide, this.props.moduleColor)}
            >
                <div className="carousel__label">
                    <h3>{title}</h3>
                </div>
                {
                    (this.props.moduleData.length > 0) ?
                        <div className="carousel__content">
                            {this.renderTweetActive()}
                            {this.renderControls()}
                        </div>
                    : <NoDataModule color={this.props.moduleColor} />
                }
            </div>
        );
    }
}

ContentCarousel.propTypes = propTypes;
ContentCarousel.defaultProps = defaultProps;

export default ContentCarousel;
