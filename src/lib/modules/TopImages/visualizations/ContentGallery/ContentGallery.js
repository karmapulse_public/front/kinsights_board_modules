import React, { Component } from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import moment from 'moment';
import { css } from 'glamor';

import ModalDialog from '../../../ModalDialog';
import { numberWithCommas } from '../../../../helpers/number';
import formatBodyText from '../../../../helpers/formatBodyText';
import retweetIcon from '../../../../helpers/icons/retweetIcon';
import styles from './contentGalleryStyles';

const propTypes = {
    moduleData: PropTypes.array,
    moduleConfig: PropTypes.object,
    moduleColor: PropTypes.string,
    moduleSide: PropTypes.string
};

const defaultProps = {
    moduleData: [],
    moduleConfig: {},
    moduleColor: '#666',
    moduleSide: 'left'
};

const imageNoUser = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZAAAAGQCAMAAAC3Ycb+AAACKFBMVEXM1t3K1Nu7xs6tusOisLqYprGMm6eGlaJ/j5x4iZZzhJJuf45sfYzJ09vBzNSsucKXprGEk6B0hJJmeIdld4bAy9OlsryLmqZxgpC3w8uXpbC+ydGZp7J0hZPL1dyrt8GAkJ3H0tmeq7ZwgZDG0NiaqLNtfoygrbhtfo2jsLtqfIrDzdWDk6CyvsdvgI6cqrTJ09qJmKTDztV8jJm/ytK8x8+6xs66xc5vgI+9ydHBzNN3iJWNnKe3wstneYjG0dhyg5GJmaWotL5sfoyHl6PI0tqap7Jpe4mNnKhneIeIl6OKmaWRoKtrfIuhrrigrrh2h5S1wMm0wMmOnaiFlaFoeomntL5rfYuToq3Ez9aqt8CQn6t6ipezv8icqrWIl6R2hpR1hpTI09qms72WpK+HlqN3h5VpeonCzdSRn6uFlKF6i5h6iphwgY+9yNC7x8+qt8GfrbefrLeElKB+jpt9jZqdq7Wksbu4xMy4w8yCkp+SoKyir7qir7nF0NfFz9eerLa1wcrK1dyHlqKruMGcqbR1hpOxvcawvMWPnanH0dl7i5nI0tl5ipeuusNtf42otb9ugI6QnqqWpLBoeohqe4qUo66bqbOGlqKToa14iJZpe4qvu8R5iZe8x9C5xc25xM3Ez9fCzdW3w8yVpK+qtsCdqrWVo66RoKyUoq62wsqOnamjsLqtucO7xs/Ezta2wsuuusSPnqmvvMWCkZ6SoayptsCVo692ayFsAAAIy0lEQVR4AezBMQEAAAQAMKB/ZbcO2+IDAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALJ69tiDBzMJojAIgH1ra/61bdv5Z3UJrGYePnVVKByJxmLxRCIei0Uj4VAw4PclQZal0plszpE3nFw2k06B7MgXiiX5QalYyMMwKmei4kK0UoYxVK3VxbVcrQoDqNFsiUetZgOkV7vTFQXdThukT68/EEWDfg+kx99wJBqMhn8gDfw50STnB6nKh0WjcB6kZDwRrSZjkHfTmWg3m4I8mi/EgMUc5MnSESOcJci91VqMWa9ALm22YtB2A3JlFxOjYjuQC/uuGNbdg352iItx8SPoR/u4WHA6g35y6YoVpQvoB72rWHLtgb76a4k1rT/QNzex6Ab6YixWjUEf3R9i1eMO+uD5z9496Ia2hUEAnmt7jo3g2lZt2zi2att959qI0zXJP98r7L38I4MnLOMm7HiZPHGZsGNlMYEsHMO+zmYC2V/jaJbDJHJwJPuEiXyCI1juWSaS8QMOs7eYTB4OsVP5TCb/FA6yAib0D+yAXwuZUOGvsP1ymFQRbJ/XmdjrsL3OMLFi2B6/FDKxwl9gu0qYXAlsx7VSJvfVNWyzMgooxzaroIAK2JZKSqiCbaqmhGrYhtxSSijNRTJ+Sffr+vFqKKIGtub/Woqo/R+A1VFGHQDLoYwcwF6rp4z612DfUsi3sAYKaYC9RyHv+Xs0UskXjYiuiVKaEF0zpTQjuhYq8SKSW0gphbmIrZViWhFbG8W0IbZ2imlHbB0U04nQbn1BMV/cQmT/Us6/iOw25dxGZHco5w4iu0s5dxHZPcq5h8juU859BPaAgh44ylpLpYNIVTig9HsK+h5xPaQUPxo+oqBHiOsxBT1BXBkU9BRx1VJQ4WVEdZOSfkJUzyjpuUNONDgE/gUlvUBUpynpNKJ6SUmvEFUeJV108pSWBtc40VLtbiEa3FGki5K+QVTf+IMI8AfR1U1J3Yiqh5J6/EH8QfxB/EG8hgjwLkuYP4i+R5T0yE1DtJxxkwoNblxxnpLOI6peSupFVH2U1Ieo3qCkNxBVPyUNIKqfKOlrhPUfBf3n/BAtGYhrkIIGEdcQBQ0hrmEKGnFnBKeHqBiloDEE9jnlfI7IfqScHxHZOOWMI7JzlDOByC5nU8xXkwhtimI+Q2zDPhZq+Zhi/kJwP1PKz4hu3JteLae+oJAvfoESv4kMwqYp5Aps8ill/DEJ2AxlzACw/+spov4m1th5rShrG8umhOwxbLCXWoXL7LVZCrj0GrbYHAXMY4ctMLkF7LLFDCaWsYg9rPI/JvVFFfaxciZ1HaaUtF6Mg+y1q0zm6ms4xHKXmMjS8io79aAcVwCFAfjEHCVnHefGySg2RrXtNrbu1o1t29Y71ubiarr/9xAfwW+0jbIqRtsJfksQWQVWgeAPnpSz4sqf0J+BFyvMK4zgb8bGWUHjYwT/UPWSFfOyiuDfJiZZEZMTZBMQ7haw7AqmBLIVZD9kmT3MJrCHu6GAZVNgcCewl246i2WRNa0jR4BbuM8MS2zmWrgbOQyE0ppZlsxsTalA4CS3/rnh0+y008Nz/W4kDYgcmF8wssOMC/MDkQQSEy4nLF5bWmY7LC9dW0y4LBDIqC1jRf/glqHGd9U/IJB/ERjgv+pbY7j1QL+S0U6guJG299IslrS290YIAAAAAAAAAAAAAAAAAAAAAAAA4P/wZC2lpNk8VZwcId6798jf//G9e2JEcvGUubkkZe0JgWLc1kumH9bF8l/F1j2cLtlwI5DV5krMmUun2WanL52JWdkkkEP6M0MQOyTI8CydpARbpYZ6dkq9oXSLJAHrXrUsiVqvdXISbJeFsoRCy7bJYZC+48+S899JJwdApF5kmVj1kWQfMHnPsoxmvU0ENuvatbLsrLtdBLYQ9opYEcY9gf4FkvYTWTGJ+0n0N7D1qpIVVXmwRX8Ct+cqWXGVh7fpd8At6iar4maUG/0CJopYNcYJ+hEcDbOqajvpG2gbDGaVBR+30WdQGsAaMF5KH8CayBohrhGEtVSyZlS2hJGLSxNZU8Q0cmnPzrPGJJ6Q6xKusQZdE8hFmYpYk4pM5JI6ZlijCjrQlcrQVmMfa9i79u4BPc8oCKBwbX731LZt27Zt27Zt2/b2uoEyHL1bOMHNzDz54fgGXz3ejUO41pU99dieEC8993PDMBIVRjq5gmi0GyV2uzjfanMONc61cTC8ylAkMz/aet8TVXq+t91jZ0uUabnT9PSqAurM6Ge4xwxQWKRW9CgIUaTbBZS6YHKwVa0JajUx+Pr9kKFY9sHc+qMPqvUxtiAp2hDlGhY1FWQM6o2x1KM2Blyx02M6Jky30uPjVUy4+trIg7cJRjQx8fitXwYzyli4xX6BIdf199iIKRu196gzDlPG9VJ+8XMGY84Ujb8IZamtuce8hDlpnuIR70AMGqh38FsDSWLM+AmjPunscboJRjU5rTLIZ8z6rPKFhWEKX1oV62FYvYrqghzDtGPaelS6hmnXKikLMgjjBsVvdGHmqdoS9sG85Zq2h9VxoLqiJ+9AHBio5+m7GRcOaukxoCUutBygJEhJnCipo8eB1jjR+kDs0f9b7NebzsCNGRq2uTdwpJn8Hosv4cilxeKDtMKVfeKDTMSVTPpE6wTOfBEeZDXOfJXdo2vCm/Wig7zCnW+i5+4VcKdCR8FBvuPQRcFBduPQbrk9KiUcSpXiOO7fxNHccVw6LrVHLZyqJTTID5z6ITRIPZyqJ7PHQ9x6GLt0WaqIDLIItxZJ7LEeUWLk2wLHWggM0hbH2gqcvN/BsTsVY5kuy4nYFcoyU1yQe7hWVlqP/QnX0n5hQd7h3Lv4Z7CyvBAWZCvObZXV4/AInBtxWFSQJ4gSa8NWEaSVqCBvIsgUUUHWRpC1knrUXxFBVtQXFOQ+gfvxmRTxSRa/0wFR4qJ0JIGRMTiJ4cnvrCOwTk6PYQRgmJggDwjAgyJ54CdHF8F5TMtp0wAAAABJRU5ErkJggg==';


class ContentGallery extends Component {

    constructor(props) {
        super(props);

        this.state = {
            tweetActive: 0,
            openModal: false,
        };

        this.handleOpenModal = this.handleOpenModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.handleByIndex = this.handleByIndex.bind(this);
        this.handleNext = this.handleNext.bind(this);
        this.handlePrev = this.handlePrev.bind(this);
        this.renderModal = this.renderModal.bind(this);
        this.renderTweetActive = this.renderTweetActive.bind(this);
        this.renderThumbnails = this.renderThumbnails.bind(this);
    }

    handleOpenModal() {
        this.setState({
            openModal: true,
        });
    }

    handleCloseModal() {
        this.setState({
            openModal: false,
        });
    }

    handleByIndex(index) {
        this.setState({
            tweetActive: index
        });
    }

    handlePrev() {
        const { moduleData } = this.props;
        const { tweetActive } = this.state;
        const newTweetActive = tweetActive === 0 ? (moduleData.length - 1) : tweetActive - 1;
        this.setState({
            tweetActive: newTweetActive
        });
    }

    handleNext() {
        const { moduleData } = this.props;
        const { tweetActive } = this.state;
        const newTweetActive = tweetActive === (moduleData.length - 1) ? 0 : tweetActive + 1;
        this.setState({
            tweetActive: newTweetActive
        });
    }

    renderModal() {
        const { tweetActive } = this.state;
        const tweet = this.props.moduleData[tweetActive];

        if (this.state.openModal) {
            return (
                <ModalDialog
                    handleCloseModal={this.handleCloseModal}
                    postedTime={tweet.tweet.postedTime}
                    image={tweet.topImage}
                    body={tweet.tweet.body}
                    link={tweet.tweet.link}
                    user={{
                        image: tweet.tweet.image,
                        name: tweet.tweet.displayName,
                        username: tweet.tweet.preferredUsername,
                        link: tweet.tweet.userLink
                    }}
                />
            );
        }
        return '';
    }

    renderThumbnails() {
        const { tweetActive } = this.state;
        return (
            this.props.moduleData.map(
                (item, index) => {
                    const backgroundImage = css({
                        backgroundImage: `url(${item.topImage ? item.topImage.replace('http:', 'https:') : ''})`,
                        backgroundPosition: 'center',
                        backgroundSize: 'cover',
                        opacity: tweetActive === index ? 1 : 0.6
                    });
                    return (
                        <button
                            key={index}
                            {...backgroundImage}
                            onClick={() => { this.handleByIndex(index); }}
                        />
                    );
                }
            )
        );
    }

    renderTweetActive() {
        const { tweetActive } = this.state;
        const tweet = this.props.moduleData[tweetActive];

        const backgroundImage = css({
            backgroundImage: `url(${tweet.topImage ? tweet.topImage.replace('http:', 'https:') : ''})`,
            backgroundPosition: 'center',
            backgroundSize: 'cover',
        });

        const tweetDate = moment(tweet.tweet.postedTime).format('DD/MM/YY');
        const tweetHour = moment(tweet.tweet.postedTime).format('HH:mm');

        return (
            <div className="gallery__tweet" >
                <button
                    className="gallery__tweet__image"
                    {...backgroundImage}
                    onClick={() => { this.handleOpenModal(tweet); }}
                />
                <div className="gallery__tweet__overlay">
                    <div className="gallery__tweet__header">
                        <div className="gallery__tweet__counter">
                            <span>{tweetActive + 1}</span>
                        </div>
                        <div className="gallery__tweet__retweets">
                            {retweetIcon()}
                            <span>{numberWithCommas(tweet.total)}</span>
                        </div>
                    </div>
                    <div className="gallery__tweet__user">
                        <img
                            src={tweet.tweet.image ? tweet.tweet.image.replace('http:', 'https:') : ''}
                            alt=""
                            ref={(a) => { this.imgPicture = a; }}
                            onError={() => { this.imgPicture.src = imageNoUser; }}
                        />
                        <h4>
                            <a
                                href={tweet.tweet.userLink}
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                {tweet.tweet.displayName}
                                <span>{tweet.tweet.preferredUsername}</span>
                            </a>
                        </h4>
                        <div className="gallery__tweet__date">
                            <a
                                href={tweet.tweet.link}
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                <span>{tweetDate}</span>
                                <span>{tweetHour}</span>
                            </a>
                        </div>
                    </div>
                    <p
                        className="gallery__tweet__body"
                        dangerouslySetInnerHTML={{
                            __html: formatBodyText(tweet.tweet.body)
                        }}
                    />
                </div>
            </div>
        );
    }

    render() {
        const { title = 'CONTENIDO MÁS COMPARTIDO' } = this.props.moduleConfig;
        if (isEmpty(this.props.moduleData)) {
            return null;
        }

        return (
            <div
                className="content_gallery"
                {...styles(this.props.moduleSide, this.props.moduleColor)}
            >
                <div className="gallery__label">
                    <h3>{title}</h3>
                </div>
                <div className="gallery__container">
                    {this.renderTweetActive()}
                    <button
                        onClick={this.handlePrev}
                        className="gallery__buttons gallery__left"
                    />
                    <button
                        onClick={this.handleNext}
                        className="gallery__buttons gallery__right"
                    />
                </div>
                <div className="gallery__thumbnails">
                    {this.renderThumbnails()}
                </div>
                {this.renderModal()}
            </div>
        );
    }
}

ContentGallery.propTypes = propTypes;
ContentGallery.defaultProps = defaultProps;

export default ContentGallery;
