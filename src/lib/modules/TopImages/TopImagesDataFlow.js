import { Observable } from 'rxjs';
import { fetchXHR } from '../../helpers/http';

export default (url, params, showChildTweet) => (
    Observable.create((observer) => {
        Observable.fromPromise(fetchXHR(url, 'GET', params))
            .subscribe(
                (data) => {
                    let tweet;

                    const formatHits = data.json.response.aggregations.images.top_images.buckets.map((item) => {
                        tweet = item.reverse_images_tweet.top_images_tweets.hits.hits[0]._source;
                        if (showChildTweet && tweet.child_tweet) { tweet = tweet.child_tweet; }
                        if (data.json.response.aggregations.images.doc_count === 0) {
                            return {
                                topImage: {},
                                total: 0,
                                tweet: {}
                            };
                        }
                        return {
                            topImage: item.key,
                            total: item.doc_count,
                            tweet: {
                                id: tweet.id,
                                body: tweet.body,
                                displayName: tweet.user.name,
                                image: tweet.user.image,
                                preferredUsername: tweet.user.username,
                                postedTime: tweet.posted_time,
                                userLink: tweet.user.link,
                                link: tweet.link,
                                retweets: tweet.retweets,
                                favorites: tweet.favorites
                            }
                        };
                    });

                    observer.next(formatHits);
                },
                (error) => { observer.error(error); }
            );
    })
);
