import React from 'react';
import PropTypes from 'prop-types';
import { PieChart, Pie, Cell } from 'recharts';
import NoUser from '../../../../helpers/icons/noUser';

import styles from './PieChartStyles';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleSide: PropTypes.string,
    moduleColor: PropTypes.string
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleSide: 'left',
    moduleColor: '#666'
};

const PieChartRemaster = ({ moduleConfig, moduleData, moduleSide, moduleColor }) => {
    const colors = {
        positive: '#89cf42',
        negative: '#e18181'
    };
    const { finalData, total } = moduleData;

    if (total === 0) {
        return (
            <div className="data__piechart--noData">
                <p>Reputación social del autor</p>
                <NoUser />
                <p>No se encontraron datos relacionados al autor</p>
            </div>
        );
    }


    const { title = 'Reputación social' } = moduleConfig;

    return (
        <div className="pie-chart" {...styles(moduleSide, moduleColor)}>
            <div className="pie-chart__label">
                <h3>{title}</h3>
            </div>
            <div className="pie-chart__content">
                <div className="pie-chart__graphic">
                    <PieChart width={130} height={130}>
                        <Pie
                            innerRadius={35}
                            outerRadius={65}
                            labelLine={false}
                            data={finalData}
                        >
                            {
                                finalData.map((entry, index) => (
                                    <Cell key={index} fill={colors[`${entry.key}`]} />
                                ))
                            }
                        </Pie>
                    </PieChart>
                </div>
            </div>
            <div className="pie-chart__footer">
                <h5>Tweets positivos: <p> {finalData[0].value} </p></h5>
                <h5>Tweets negativos: <p> {finalData[1].value} </p></h5>
            </div>
            <div className="pie-chart__sentiments">
                <h4> Positivo </h4>
                <h5> Negativo </h5>
            </div>
        </div>
    );
};

PieChartRemaster.propTypes = propTypes;
PieChartRemaster.defaultProps = defaultProps;

export default PieChartRemaster;
