import { Observable } from 'rxjs';
import { fetchXHR } from '../../helpers/http';

export default (url, params) => (
    Observable.create((observer) => {
        Observable.fromPromise(fetchXHR(url, 'GET', params))
            .subscribe(
                (data) => {
                    const { response } = data.json;
                    const finalData = [
                        {
                            key: 'positive',
                            name: 'Positivo',
                            value: response.percentage_positive,
                        },
                        {
                            key: 'negative',
                            name: 'Negativo',
                            value: response.percentage_negative,
                        },
                    ];
                    observer.next({ finalData, total: response.total });
                },
                (error) => { observer.error(error); }
            );
    })
);
