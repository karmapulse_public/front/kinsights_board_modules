import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import fetchFbProfile from './TopInteractionDataFlow';

import LoadingModule from '../LoadingModule';
import NoDataYet from '../NoDataYet';
import TotalNumbers from './visualizations/TotalNumbers';
import SliderInteractions from './visualizations/SliderInteractions';

const propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    dateRange: PropTypes.object,
    services: PropTypes.object,
    showNoDataYet: PropTypes.bool
};

const defaultProps = {
    fields: {},
    side: 'left',
    color: '#444',
    dateRange: {},
    services: {},
    showNoDataYet: false
};

class IgTotalInteraction extends Component {
    constructor(props) {
        super(props);

        this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color
        };
    }

    componentDidMount() {
        this.updateDataFlow(this.props);
    }

    componentWillReceiveProps(nextProps) {
        this.updateDataFlow(nextProps);
    }

    updateDataFlow(props) {
        const params = {
            rule_id: [props.fields.search_id],
            initial_date: props.dateRange.startDate.toISOString(),
            final_date: props.dateRange.endDate.toISOString(),
            recipe_id: 'module_ig_total_interaction',
        };

        fetchFbProfile(this.props.services.twitter, params)
            .subscribe(
                (data) => {
                    this.setState({
                        moduleConfig: props.fields,
                        moduleData: data,
                        moduleLoading: false,
                        moduleSide: props.side,
                        moduleColor: props.color
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    renderByViz() {
        const { moduleConfig, moduleData } = this.state;
        const { dateRange: { startDate, endDate }, showNoDataYet } = this.props;
        let renderNoDataYet = null;

        if (showNoDataYet && moment().diff(endDate, 'minutes') <= 0 && endDate.diff(startDate, 'days') <= 1) {
            renderNoDataYet = () => <NoDataYet />;
        }
        if (moduleData) {
            const visualizations = {
                total_numbers: params => (
                    <TotalNumbers {...params} noDataYet={renderNoDataYet} />
                ),
                slider_interactions: params => (
                    <SliderInteractions {...params} noDataYet={renderNoDataYet} />
                )
            };

            return visualizations[moduleConfig.visualization](this.state);
        }

        return (<LoadingModule />);
    }

    render() {
        return (
            <div className="m-ig-total-numbers">
                {this.renderByViz()}
            </div>
        );
    }
}

IgTotalInteraction.propTypes = propTypes;
IgTotalInteraction.defaultProps = defaultProps;

export default IgTotalInteraction;
