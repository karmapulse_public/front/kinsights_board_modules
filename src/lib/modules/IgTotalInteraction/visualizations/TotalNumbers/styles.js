import { css } from 'glamor';

const styles = color => css({
    width: '100%',
    minWidth: '320px',
    maxWidth: '500px',

    ' .total-num-int': {

        ' &__label': {
            display: 'flex',
            justifyContent: 'space-between',
            padding: '15px 20px 15px 20px',
            borderTop: '1px solid #dfdfdf',
            color,

            ' h3': {
                fontSize: '13px',
                fontWeight: 600
            },
        },
        ' &__content': {
            minHeight: '225px',
            padding: '30px 40px',

            ' &__data': {
                display: 'flex',
                justifyContent: 'space-between',
                padding: '10px 0px',

                ' h4': {
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    fontSize: '13px',
                    fontWeight: 600,
                    color: 'rgba(0, 0, 0, 0.5)',
                    ' svg': {
                        marginRight: 15,
                    },
                },
                ' p': {
                    fontWeight: 900,
                    fontSize: '16px',
                },
            },
            ' &__total': {
                display: 'flex',
                justifyContent: 'space-between',
                padding: '0px 0px 12px',

                ' h4': {
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    fontSize: '13px',
                    fontWeight: 700,
                    color: 'rgba(0, 0, 0, 0.5)',
                    ' svg': {
                        marginRight: 15,
                    },
                },
                ' p': {
                    fontWeight: 700,
                    fontSize: '16px',
                },
            }
        }
    }
});

export default styles;
