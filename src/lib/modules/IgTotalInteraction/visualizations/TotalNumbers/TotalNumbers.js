import React from 'react';
import PropTypes from 'prop-types';
import LikesIcon from '../../../../helpers/icons/ceo/likesIcon';
import ReplicesIcon from '../../../../helpers/icons/ceo/dialogIcon';
import { numberWithCommas } from '../../../../helpers/number';
import NoDataModule from '../../../NoDataModule';

import styles from './styles';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    noDataYet: PropTypes.string,
};

const defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleColor: '#666',
    noDataYet: null,
};

const TotalNumbers = ({ moduleData, moduleConfig, moduleColor, noDataYet }) => {
    const color = '#F8006A';
    return (
        <div {...styles(moduleColor)}>
            <div className="total-num-int__label">
                <h3>{moduleConfig.title}</h3>
            </div>
            {
                (() => {
                    if (noDataYet !== null) {
                        return noDataYet();
                    }
                    return (moduleData.total !== 0)
                        ? (
                            <div className="total-num-int__content">
                                <div className="total-num-int__content__total">
                                    <h4>
                                        Interacciones totales
                                    </h4>
                                    <p>
                                        {numberWithCommas(moduleData.total_comments + moduleData.total_likes)}
                                    </p>
                                </div>
                                <div className="total-num-int__content__data">
                                    <h4>
                                        <ReplicesIcon color={color} />
                                        Comentarios totales
                                    </h4>
                                    <p>
                                        {numberWithCommas(moduleData.total_comments)}
                                    </p>
                                </div>
                                <div className="total-num-int__content__data">
                                    <h4>
                                        Promedio por publicación
                                    </h4>
                                    <p>
                                        {numberWithCommas(Math.round(moduleData.average_comments))}
                                    </p>
                                </div>
                                <div className="total-num-int__content__data">
                                    <h4>
                                        <LikesIcon color={color} />
                                        Likes totales
                                    </h4>
                                    <p>
                                        {numberWithCommas(moduleData.total_likes)}
                                    </p>
                                </div>
                                <div className="total-num-int__content__data">
                                    <h4>
                                        Promedio por publicación
                                    </h4>
                                    <p>
                                        {numberWithCommas(Math.round(moduleData.average_likes))}
                                    </p>
                                </div>
                            </div>
                        )
                        : <NoDataModule color={moduleColor} />;
                })()
            }
        </div>
    );
};

TotalNumbers.propTypes = propTypes;
TotalNumbers.defaultProps = defaultProps;

export default TotalNumbers;
