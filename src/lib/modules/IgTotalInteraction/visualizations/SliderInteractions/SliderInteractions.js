import React from 'react';
import PropTypes from 'prop-types';
import { faInstagram } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { numberWithCommas } from '../../../../helpers/number';
import NoDataModule from '../../../NoDataModule';

import styles from './styles';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    noDataYet: PropTypes.string,
};

const defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleColor: '#666',
    noDataYet: null,
};


const SliderInteractions = ({ moduleData, moduleConfig, moduleColor, noDataYet }) => {

    return (
        <div className="list-interactions" {...styles}>
            <div className="list-interactions__label">
                <h3>INTERACCIONES INSTAGRAM</h3>
                <div>
                    <FontAwesomeIcon icon={faInstagram} color="white" />
                </div>
            </div>
            {
                (() => {
                    if (noDataYet !== null) {
                        return noDataYet();
                    }
                    return (moduleData.total !== 0)
                        ? (
                            <div className="list-interactions__content">
                                <h4>
                                    <span>{numberWithCommas(moduleData.total_comments + moduleData.total_likes)}</span>
                                    <p>Totales </p>
                                </h4>
                                <div style={{ position: 'relative', marginTop: '31px' }}>
                                    <div className="slider__card">
                                        <h5>
                                            <span>{numberWithCommas(moduleData.total_likes)}</span>
                                            <p>Me gusta</p>
                                        </h5>
                                        <h5>
                                            <span>{numberWithCommas(moduleData.total_comments)}</span>
                                            <p>Comentarios</p>
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        )
                        : <NoDataModule color={moduleColor} />;
                })()
            }
        </div>
    );
};

SliderInteractions.propTypes = propTypes;
SliderInteractions.defaultProps = defaultProps;

export default SliderInteractions;
