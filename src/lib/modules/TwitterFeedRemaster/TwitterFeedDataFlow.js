import { Observable } from 'rxjs';
import { fetchXHR } from '../../helpers/http';

export default (url, params) => (
    Observable.create((observer) => {
        Observable.fromPromise(fetchXHR(url, 'GET', params))
            .subscribe(
                (data) => {
                    observer.next({
                        tweets: data.json.response.data,
                        page: 0,
                        scrollActive: true
                    });
                },
                (error) => { observer.error(error); }
            );
    })
);