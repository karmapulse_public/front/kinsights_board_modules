import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import takeRight from 'lodash/takeRight';

import fetchTwitterFeed from './TwitterFeedDataFlow';

import LoadingModule from '../LoadingModule';
import FeedTwitter from './visualizations/List';

const propTypes = {
    fields: PropTypes.object,
    dateRange: PropTypes.object,
    services: PropTypes.object,
    filters: PropTypes.object
};

const defaultProps = {
    fields: {},
    dateRange: {},
    services: {},
    filters: {},
};

const TwitterFeedRemaster = (props) => {
    const [innerState, setInnerState] = useState({
        data: null,
        loading: true,
        error: null
    });

    const fetchData = () => {
        const { dateRange: { startDate, endDate } } = props;
        const params = {
            recipe_id: 'module_tw_feed',
            rule_id: props.fields.search_id,
            initial_date: startDate.toISOString(),
            final_date: endDate.toISOString(),
            filters: { ...props.filters }
        };

        fetchTwitterFeed(props.services.twitter, params)
            .subscribe(
                data => setInnerState(prevValue => ({
                    ...prevValue,
                    data: Object.assign({}, data, {
                        page: 0
                    }),
                    loading: false
                })),
                (error) => {
                    console.log(error);
                },
                null
            );
    };

    useEffect(() => { fetchData(); }, [setInnerState]);

    const infiniteScrollHandler = (page) => {
        const lastTweet = takeRight(innerState.data.tweets);
        const { dateRange: { startDate, endDate } } = props;

        const params = {
            recipe_id: 'module_tw_feed',
            rule_id: props.fields.search_id,
            initial_date: startDate.toISOString(),
            final_date: endDate.toISOString(),
            search_after: lastTweet[0].sort,
            filters: { ...props.filters }
        };

        fetchTwitterFeed(props.services.twitter, params, page)
            .subscribe(
                (data) => {
                    const newTweets = innerState.data.tweets
                        .concat(data.tweets);
                    setInnerState({
                        data: {
                            tweets: newTweets,
                            page
                        },
                        loading: false,
                        error: false
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    };

    const renderByViz = () => {
        const { data } = innerState;

        if (data) {
            const visualizations = {
                list_tweets: params => (
                    <FeedTwitter
                        {...params}
                        scroll={infiniteScrollHandler}
                    />
                )
            };
            return visualizations[props.fields.visualization]({ ...innerState, services: props.services });
        }

        return (<LoadingModule />);
    };

    return (
        <div className="m-twitter-feed">
            {renderByViz()}
        </div>
    );
};

TwitterFeedRemaster.propTypes = propTypes;
TwitterFeedRemaster.defaultProps = defaultProps;

export default TwitterFeedRemaster;
