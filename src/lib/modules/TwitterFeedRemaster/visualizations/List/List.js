import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';

import Gallery from '../../../../helpers/customComponents/Gallery';
import Header from '../../../../helpers/customComponents/Header';
import Sentiment from '../../../../helpers/customComponents/Sentiment';
import BodyText from '../../../../helpers/customComponents/BodyText';
import Card from '../../../../helpers/customComponents/Card';
import Video from '../../../../helpers/customComponents/Video';
import Link from '../../../../helpers/customComponents/Link';

import styles from './styles';

const propTypes = {
    services: PropTypes.object,
    data: PropTypes.object,
    scroll: PropTypes.func
};

const defaultProps = {
    services: {},
    data: null,
    scroll: () => ({})
};

const List = (props) => {
    const refContainer = useRef();
    const prevScroll = useRef(null);
    const renderBody = item => (
        <React.Fragment>
            <BodyText text={item.body} link={item} type="twitter" />
            {(() => {
                if (item.media_type === 'image') {
                    return (
                        <Gallery
                            items={item.images}
                            height="300px"
                            type="twitter"
                            url={item.link}
                        />
                    );
                }
                if (item.media_type === 'gif') {
                    return (
                        <Video
                            sources={JSON.parse(item.gif.variants)}
                            cover={item.gif.cover}
                            type="twitter"
                            gif
                        />
                    );
                }
                if (item.media_type === 'video') {
                    return (
                        <Video
                            sources={JSON.parse(item.video.variants)}
                            cover={item.video.cover}
                            type="twitter"
                        />
                    );
                }
                if (item.urls) {
                    return (
                        // eslint-disable-next-line jsx-a11y/anchor-is-valid
                        <Link
                            url={item.urls[0].url}
                            title={item.urls[0].title}
                            text={item.urls[0].description}
                            type="twitter"
                        />
                    );
                }
                return null;
            })()}
        </React.Fragment>
    );

    const makeScroll = (e) => {
        const div = e.target;
        const isUserScrollingDown = position => (
            prevScroll.current < position.scrollTop
        );
        const isScrollExpectedPercent = (position, percent) => (
            ((position.scrollTop + position.clientHeight) / position.scrollHeight) > (percent / 100)
        );
        if (isUserScrollingDown(div) && isScrollExpectedPercent(div, 95)) {
            props.scroll(props.data.page + 10);
        }

        prevScroll.current = div.scrollTop;
    };

    useEffect(() => {
        const container = refContainer.current;
        container.addEventListener('scroll', makeScroll);
        return () => container.removeEventListener('scroll', makeScroll);
    }, [refContainer, prevScroll, props.scroll, props.data.page]);

    return (
        <div {...styles} ref={refContainer} className="twitter-list__container">
            {props.data.tweets.map(item => (
                <Card key={item._source.id}>
                    <Header noSource item={item._source} services={props.services} />
                    {

                        renderBody(item._source)
                    }
                    <Sentiment item={item._source} noSource />
                </Card>
            ))}
        </div>
    );
};

List.propTypes = propTypes;
List.defaultProps = defaultProps;

export default List;
