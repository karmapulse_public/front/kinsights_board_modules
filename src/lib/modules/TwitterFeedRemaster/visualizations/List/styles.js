import { css } from 'glamor';

export default css({
    '.twitter-list__container': {
        height: '100%',
        overflow: 'auto'
    }
});
