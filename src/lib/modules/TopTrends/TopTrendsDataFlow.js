import { Observable } from 'rxjs';
import isUndefined from 'lodash/isUndefined';
import find from 'lodash/find';
import { fetchXHR } from '../../helpers/http';

const formatSentiment = (buckets) => {
    const collect = [];
    const sentimentsTypes = ['positivo', 'neutral', 'negativo'];
    sentimentsTypes.map((item) => {
        const s = find(buckets, { key: item });
        if (!isUndefined(s)) return collect.push(s);
        return collect.push({ key: item, doc_count: 0 });
    });
    return collect;
};

const formatSentimentTop = (eltos) => {
    const collect = [];
    const mapSentiment = {
        positive: 'positivo',
        negative: 'negativo',
        neutral: 'neutral'
    };
    Object.keys(eltos).forEach((key) => {
        if (key.includes('total_')) {
            collect.push({
                key: mapSentiment[key.split('_')[1]],
                doc_count: eltos[key]
            });
        }
    });
    return collect;
};

export default (url, params) => (
    Observable.create((observer) => {
        Observable.fromPromise(fetchXHR(url, 'GET', params))
            .subscribe(
            (data) => {
                if (params.filters.query === 'search') {
                    const formatData = params.filters.words.split(',').map((item, index) => ({
                        key: item,
                        doc_count: data.json.response.aggregations.entity.buckets[`${index}`].doc_count,
                        sentiment: formatSentiment(data.json.response.aggregations.entity.buckets[`${index}`].sentiment_per_entity.buckets)
                    }));

                    formatData.sort((a, b) => (
                        a.doc_count - b.doc_count
                    )).reverse();
                    observer.next(formatData);
                } else {
                    const sentiment = Object.assign(
                        {},
                        data.json.response.map(item => ({
                            key: item.theme,
                            doc_count: item.total,
                            sentiment: formatSentimentTop(item)
                        }))
                    );
                    observer.next(Object.values(sentiment));
                }
            },
            (error) => { observer.error(error); }
            );
    })
);
