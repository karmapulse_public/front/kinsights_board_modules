import React, { Component } from 'react';
import PropTypes from 'prop-types';
import clone from 'lodash/clone';
import moment from 'moment-timezone';

import fetchTopTrends from './TopTrendsDataFlow';

import LoadingModule from '../LoadingModule';
import ListTopTrends from './visualizations/ListTopTrends';
import ListTopTrendsSentiment from './visualizations/ListTopTrendsSentiment';
import RadarTopTrends from './visualizations/RadarTopTrends';
import BarsTopTrends from './visualizations/BarsTopTrends';

const propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    services: PropTypes.object,
    dateRange: PropTypes.object,
    view: PropTypes.string,
    showColors: PropTypes.bool
};

const defaultProps = {
    fields: {},
    side: 'left',
    color: '#666',
    services: {},
    dateRange: {},
    view: '',
    showColors: false
};

class TopTrends extends Component {
    constructor(props) {
        super(props);

        this.state = {
            moduleServices: props.services,
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color,
            moduleView: props.view,
            showColors: props.showColors,
            moduleRange: props.dateRange
        };

        this.renderByViz = this.renderByViz.bind(this);
    }

    componentDidMount() {
        this.updateDataFlow(this.props);
    }

    componentDidUpdate(prevProps) {
        const { startDate, endDate } = this.props.dateRange;
        const { fields: { hasClick, search_id } } = this.props;
        if (startDate !== prevProps.dateRange.startDate ||
            endDate !== prevProps.dateRange.endDate ||
            search_id !== prevProps.fields.search_id) {
            this.updateDataFlow(this.props);
        }
        if (hasClick !== prevProps.fields.hasClick) {
            this.changeState(hasClick);
        }
    }

    changeState(hasClick) {
        this.setState({
            moduleConfig: {
                ...this.state.moduleConfig,
                hasClick
            }
        });
    }

    updateDataFlow(props) {
        const { fields, dateRange: { startDate, endDate } } = props;
        const params = {
            recipe_id: 'module_tw_list_trends',
            rule_id: fields.search_id,
            initial_date: startDate.startOf('day').utc().toISOString(),
            final_date: endDate.endOf('day').utc().toISOString(),
            filters: {
                entity: fields.entity_type,
                query: fields.query_type,
                words: fields.search_entities
            }
        };

        fetchTopTrends(props.services.twitter, params)
            .subscribe(
                (data) => {
                    this.setState({
                        moduleData: data,
                        moduleConfig: props.fields,
                        moduleLoading: false,
                        moduleView: props.view,
                        moduleColor: props.color,
                        moduleSide: props.side,
                        showColors: props.showColors,
                        moduleRange: props.dateRange
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    renderByViz() {
        const { moduleConfig, moduleData } = this.state;

        if (moduleData) {
            const visualizations = {
                list_top_trends: params => (
                    <ListTopTrends {...params} />
                ),
                radar_top_trends: params => (
                    <RadarTopTrends {...params} />
                ),
                bars_top_trends: params => (
                    <BarsTopTrends {...params} />
                ),
                list_top_trends_sentiment: params => (
                    <ListTopTrendsSentiment {...params} />
                )
            };
            const v = this.state.moduleView !== '' ? this.state.moduleView : moduleConfig.visualization;
            return visualizations[v](this.state);
        }

        return (<LoadingModule />);
    }

    render() {
        return (
            <div className="m-top-trends">
                {this.renderByViz()}
            </div>
        );
    }
}

TopTrends.propTypes = propTypes;
TopTrends.defaultProps = defaultProps;

export default TopTrends;
