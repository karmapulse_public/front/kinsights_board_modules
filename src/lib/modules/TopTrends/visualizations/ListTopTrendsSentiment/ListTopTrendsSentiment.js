import React from 'react';
import PropTypes from 'prop-types';
import maxBy from 'lodash/maxBy';
import { css } from 'glamor';

import styles from './listTopTrendsSentimentStyles';
import { numberWithCommas } from '../../../../helpers/number';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.array,
    moduleColor: PropTypes.string,
    moduleSide: PropTypes.string,
    showColors: PropTypes.bool
};

const defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleColor: '#666',
    moduleSide: 'left',
    showColors: false
};

const ListTopTrendsSentiment = (
    { moduleConfig, moduleData, moduleColor, moduleSide, showColors }
) => {
    const colors = ['#c75a93', '#8176cc', '#cc5f43', '#5ba966', '#54a2d3'];

    const softColors = [
        'rgba(199,89,147,0.21)',
        'rgba(129,117,204,0.21)',
        'rgba(204,95,66,0.21)',
        'rgba(91,169,101,0.21)',
        'rgba(84,161,211,0.21)'
    ];
    const entityType = {
        hashtags: {
            symbol: '#',
            name: 'Hashtags'
        },
        mentions: {
            symbol: '@',
            name: 'Menciones'
        },
        keywords: {
            symbol: '',
            name: 'Palabras'
        }
    };

    const dataFormat = moduleData.map((item, index) => (
        {
            id: index + 1,
            subject: entityType[moduleConfig.entity_type].symbol + item.key,
            total: item.doc_count,
            sentiment: item.sentiment,
            maxValue: maxBy(item.sentiment, 'doc_count')
        }
    ));

    const renderTitle = () => {
        if (moduleConfig.show_title) {
            const { title = 'RELACIÓN DE TEMAS' } = moduleConfig;
            return (
                <div className="list__label">
                    <h3>{title}</h3>
                </div>
            );
        }
        return '';
    };

    const renderSentiment = (sentiment, total, max) => {
        const percentage = value => ((total < 1) ? 0 : value * 100 / total).toFixed(1).replace(/\.0$/, ''); // eslint-disable-line
        return sentiment.map((s, index) => (
            <h5
                key={index}
                className={`${s.key} ${(s.key === max.key && s.doc_count > 0) ? 'active' : ''}`}
            >
                {percentage(s.doc_count)}%
            </h5>
        ));
    };

    const showColor = (index, { subject, total, id }) => {
        const dotColor = css({
            backgroundColor: softColors[index],
            border: `1px solid ${colors[index]}`
        });
        const value = subject.length < 35 ? subject : subject.substring(0, 35).concat('...');

        if (showColors) {
            return (
                <div className="list__item__data">
                    <div className="list__dot" {...dotColor} />
                    <h4 className="subject">{value}</h4>
                    <h5 className="total" style={{ color: colors[index] }}>
                        {numberWithCommas(total)}
                    </h5>
                </div>
            );
        }
        return (
            <div className="list__item__data">
                <h5 className="index">{id}.</h5>
                <h4 className="subject">{value}</h4>
                <h5 className="total">{numberWithCommas(total)}</h5>
            </div>
        );
    };

    const renderItems = () => (
        dataFormat.map(
            (item, index) => (
                <li className="list__item" key={index}>
                    {showColor(index, item)}
                    <div className="list__item__sentiment">
                        {renderSentiment(item.sentiment, item.total, item.maxValue)}
                    </div>
                </li>
            )
        )
    );

    return (
        <div className="list-top-trends-sentiment" {...styles(moduleColor, moduleSide)} >
            {renderTitle()}
            <div className="list-sentiment__body">
                <div className="list-sentiment__body__title">
                    <h5>Listado</h5>
                    <h5>{entityType[moduleConfig.entity_type].name}</h5>
                </div>
                <ul>
                    {renderItems()}
                </ul>
            </div>
            <div className="list-sentiment__footer">
                <h5>Positivo</h5>
                <h5>Neutral</h5>
                <h5>Negativo</h5>
            </div>
        </div>
    );
};

ListTopTrendsSentiment.propTypes = propTypes;
ListTopTrendsSentiment.defaultProps = defaultProps;

export default ListTopTrendsSentiment;
