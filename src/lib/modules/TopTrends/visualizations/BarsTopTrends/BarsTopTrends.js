import React from 'react';
import PropTypes from 'prop-types';
import maxBy from 'lodash/maxBy';
import { css } from 'glamor';

import styles from './barsTopTrendsStyles';
import { numberWithCommas } from '../../../../helpers/number';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.array,
    moduleColor: PropTypes.string
};

const defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleColor: '#666'
};

const BarsTopTrends = ({ moduleConfig, moduleData, moduleColor }) => {
    const colors = ['#c75a93', '#8176cc', '#cc5f43', '#5ba966', '#54a2d3'];

    const symbol = {
        hashtags: '#',
        mentions: '@',
        keywords: ''
    };

    const maxValue = maxBy(moduleData, 'doc_count');

    const dataFormat = moduleData.map(item => (
        {
            subject: symbol[moduleConfig.entity_type] + item.key,
            total: item.doc_count,
            fullMark: maxValue.doc_count
        }
    ));

    const renderTitle = () => {
        if (moduleConfig.show_title) {
            const { title = 'RELACIÓN DE TEMAS' } = moduleConfig;
            return (
                <div className="bars-list__label">
                    <h3>{title}</h3>
                </div>
            );
        }
        return '';
    };

    const renderBars = (index, num) => {
        const customStyles = css({
            width: `${(num * 100) / maxValue.doc_count}%`,
            height: 5,
            marginTop: 10,
            backgroundColor: colors[index]
        });
        return <div {...customStyles} className="bars-list__bar" />;
    };

    const renderItems = () => (
        dataFormat.map(
            (item, index) => (
                <li className="bars-list__item" key={index}>
                    <span>{`${index + 1}.`}</span>
                    <h4 className="bars-list__name">{item.subject}</h4>
                    <h5 className="bars-list__value">{numberWithCommas(item.total)} Menciones</h5>
                    {renderBars(index, item.total)}
                </li>
            )
        )
    );

    return (
        <div className="bars-top-trends" {...styles(moduleColor)}>
            {renderTitle()}
            <ul>
                {renderItems()}
            </ul>
        </div>
    );
};

BarsTopTrends.propTypes = propTypes;
BarsTopTrends.defaultProps = defaultProps;

export default BarsTopTrends;
