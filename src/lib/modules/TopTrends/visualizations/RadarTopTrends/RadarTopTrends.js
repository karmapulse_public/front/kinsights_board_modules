import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import maxBy from 'lodash/maxBy';
import {
    Radar,
    RadarChart,
    PolarGrid,
    PolarAngleAxis,
    ResponsiveContainer
} from 'recharts';

import styles from './radarTopTrendsStyles';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.array,
    moduleColor: PropTypes.string
};

const defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleColor: '#666'
};

const RadarTopTrends = ({ moduleConfig, moduleData, moduleColor }) => {
    const refExport = useRef(null);
    const colors = ['#c75a93', '#8176cc', '#cc5f43', '#5ba966', '#54a2d3'];
    const maxValue = maxBy(moduleData, 'doc_count');

    const dataFormat = moduleData.map(item => (
        {
            subject: item.key,
            total: item.doc_count,
            fullMark: maxValue.doc_count
        }
    ));

    const renderTitle = () => {
        if (moduleConfig.show_title) {
            const { title = 'RELACIÓN DE TEMAS' } = moduleConfig;
            return (
                <div className="radar__label">
                    <h3>{title}</h3>
                </div>
            );
        }
        return '';
    };

    const CustomizedTick = data => (
        <g>
            <circle
                cx={data.x}
                cy={data.y}
                r="5"
                stroke={colors[data.index]}
                fill={colors[data.index]}
                fillOpacity={0.2}
            />
        </g>
    );

    return (
        <div
            className="radar-top-trends"
            {...styles(moduleColor)}
            ref={refExport}
        >
            {renderTitle()}
            <div className="radar__chart">
                {
                    //moduleConfig.buttonExport(refExport, moduleConfig.title)
                }
                <ResponsiveContainer>
                    <RadarChart data={dataFormat}>
                        <Radar
                            dataKey="total"
                            stroke="#d0021b"
                            strokeWidth={1}
                            fill="#d0021b"
                            fillOpacity={0.2}
                        />
                        <PolarGrid stroke="#eee" />
                        <PolarAngleAxis
                            tick={data => CustomizedTick(data)}
                        />
                    </RadarChart>
                </ResponsiveContainer>
            </div>
        </div>
    );
};

RadarTopTrends.propTypes = propTypes;
RadarTopTrends.defaultProps = defaultProps;

export default RadarTopTrends;
