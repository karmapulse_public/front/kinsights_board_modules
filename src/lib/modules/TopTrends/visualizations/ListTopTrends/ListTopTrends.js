import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import maxBy from 'lodash/maxBy';
import { css } from 'glamor';

import styles from './listTopTrendsStyles';
import NoDataModule from '../../../NoDataModule';
import { numberWithCommas } from '../../../../helpers/number';
import withDrawer from '../../../withDrawer';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.array,
    moduleColor: PropTypes.string,
    moduleSide: PropTypes.string,
    showColors: PropTypes.bool,
    onClick: PropTypes.func
};

const defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleColor: '#666',
    moduleSide: 'left',
    showColors: false,
    onClick: () => ({})
};

const ListTopTrends = ({
    moduleConfig,
    moduleData,
    moduleColor,
    moduleSide,
    showColors,
    onClick
}) => {
    const refExport = useRef(null);
    const colors = ['#c75a93', '#8176cc', '#cc5f43', '#5ba966', '#54a2d3'];
    const softColors = [
        'rgba(199,89,147,0.21)',
        'rgba(129,117,204,0.21)',
        'rgba(204,95,66,0.21)',
        'rgba(91,169,101,0.21)',
        'rgba(84,161,211,0.21)'
    ];

    const entityType = {
        hashtags: {
            symbol: '#',
            name: 'Hashtags',
            keyFilter: 'hashtag',
            filterForm: word => word.substring(1, word.length)
        },
        mentions: {
            symbol: '@',
            name: 'Menciones',
            keyFilter: 'mention',
            filterForm: word => word.substring(1, word.length)
        },
        keywords: {
            symbol: '',
            name: 'Palabras',
            keyFilter: 'phrase',
            filterForm: word => word
        }
    };

    const maxValue = maxBy(moduleData, 'doc_count');
    const titleConfigDw = theme => ({
        title: moduleConfig.title || 'Tweets en el tiempo',
        subtitleLeft: `Tweets con ${theme}`,
        subtitleRight: '',
    });

    const dataFormat = moduleData.map((item, index) => (
        {
            id: index + 1,
            subject: entityType[moduleConfig.entity_type].symbol + item.key,
            total: item.doc_count,
            fullMark: maxValue.doc_count
        }
    ));

    const renderTitle = () => {
        if (moduleConfig.show_title) {
            const { title = 'RELACIÓN DE TEMAS' } = moduleConfig;
            return (
                <div className="list__label">
                    <h3>{title}</h3>
                </div>
            );
        }
        return '';
    };

    const showColor = (index, { subject, total, id }) => {
        const dotColor = css({
            backgroundColor: softColors[index],
            border: `1px solid ${colors[index]}`
        });
        const filterRawData = {
            [`${entityType[moduleConfig.entity_type].keyFilter}`]: entityType[moduleConfig.entity_type].filterForm(subject)
        };
        const value = subject.length < 35 ? subject : subject.substring(0, 35).concat('...');

        if (showColors) {
            return (
                <li className="list__item" key={index}>
                    <div className="list__dot" {...dotColor} />
                    <h4 // eslint-disable-line
                        className="list__name"
                        onClick={() => onClick({ filters: { ...filterRawData } }, titleConfigDw(value))}
                    >
                        {value}
                    </h4>
                    <h5 className="list__value" style={{ color: colors[index] }}>
                        {numberWithCommas(total)}
                    </h5>
                </li>
            );
        }
        return (
            <li className="list__item" key={index}>
                <h5 className="index">{id}.</h5>
                <h4 // eslint-disable-line
                    className="list__name"
                    onClick={() => onClick({ filters: { ...filterRawData } }, titleConfigDw(value))}
                >
                    {value}
                </h4>
                <h5 className="list__value">
                    {numberWithCommas(total)}
                </h5>
            </li>
        );
    };

    const renderItems = () => (
        dataFormat.map((item, index) =>
            showColor(index, item))
    );

    return (
        <div
            ref={refExport}
            className="list-top-trends"
            {...styles(moduleColor, moduleSide, moduleConfig.hasClick)}
        >
            {renderTitle()}
            {
                // moduleConfig.buttonExport(refExport, moduleConfig.title, {
                //     top: '25px'
                // })
            }
            <div className="list-top-titles">
                <h5>Listado</h5>
                <h5>{entityType[moduleConfig.entity_type].name}</h5>
            </div>
            {
                dataFormat.length === 0 ?
                    <NoDataModule /> :
                    <ul>
                        {renderItems()}
                    </ul>
            }
        </div>
    );
};

ListTopTrends.propTypes = propTypes;
ListTopTrends.defaultProps = defaultProps;

export default withDrawer(ListTopTrends);
