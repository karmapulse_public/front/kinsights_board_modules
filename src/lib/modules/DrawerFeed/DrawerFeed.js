import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import FeedTweets from './visualizations/FeedTweets';
import FeedMilitants from './visualizations/FeedMilitants';
import DrawerContext from '../../helpers/context/ctxDrawer';
import drawerStyles, { overlayStyles } from './DrawerStyles';

const propTypes = {
    open: PropTypes.bool,
    drawerData: PropTypes.object,
    onCloseDrawer: PropTypes.func
};

const defaultProps = {
    open: false,
    onCloseDrawer: () => ({}),
    drawerData: {
        type: 'TWITTER',
        title: '',
        subtitleLeft: '',
        subtitleRight: '',
        queryData: {}
    }
};

class DrawerCmp extends Component {
    constructor(props) {
        super(props);
        this.handleChangeSubtitle = this.handleChangeSubtitle.bind(this);
        const { subtitleLeft, subtitleRight } = props.drawerData;
        this.state = {
            subtitleLeft,
            subtitleRight
        };
    }

    componentWillReceiveProps(nextProps) {
        const { subtitleLeft, subtitleRight } = nextProps.drawerData;
        this.setState({
            subtitleLeft,
            subtitleRight
        });
    }

    handleChangeSubtitle(type, data) {
        this.setState({ [`subtitle${type}`]: data });
    }

    render() {
        const { open, onCloseDrawer, drawerData } = this.props;
        const { subtitleLeft, subtitleRight } = this.state;
        const visualizations = {
            TWITTER: params => (
                <FeedTweets
                    queryData={params}
                    service={drawerData.service}
                    onChangeSubtitle={this.handleChangeSubtitle}
                />
            ),
            MILITANTS: params => (
                <FeedMilitants
                    queryData={params}
                    service={drawerData.service}
                    onChangeSubtitle={this.handleChangeSubtitle}
                />
            ),
        };
        return (
            <Fragment>
                <div className="raw-data__container" {...drawerStyles(open, drawerData.type)}>
                    {(() => {
                        if (!open) return null;
                        return (
                            <Fragment>
                                <div className="raw-data__header">
                                    <div>
                                        <h3>{drawerData.title}</h3>
                                        <div className="raw-data__header__subtitle">
                                            <div>
                                                <span>{subtitleLeft}</span>
                                            </div>
                                            <div>
                                                <span>{subtitleRight}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {visualizations[drawerData.type](drawerData.queryData)}
                            </Fragment>
                        );
                    })()}
                </div>
                <div // eslint-disable-line
                    className="raw-data__overlay"
                    {...overlayStyles(open)}
                    onClick={onCloseDrawer}
                />
            </Fragment>
        );
    }
}

DrawerCmp.propTypes = propTypes;
DrawerCmp.defaultProps = defaultProps;

const HocDrawerFeed = () => (
    <DrawerContext.Consumer>
        {({ open, onCloseDrawer, drawerData }) => (
            <DrawerCmp
                open={open}
                drawerData={drawerData}
                onCloseDrawer={onCloseDrawer}
            />
        )}
    </DrawerContext.Consumer>
);

export default HocDrawerFeed;
