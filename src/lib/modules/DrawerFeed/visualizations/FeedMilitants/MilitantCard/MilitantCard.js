import React, { useState } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import upperFirst from 'lodash/upperFirst';
import Button from 'kpulse_design_system/dist/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronUp, faChevronDown } from '@fortawesome/free-solid-svg-icons';

import styles from './styles';

const capitalLetter = (str) => {
    if (str && str !== '') {
        const newStr = str.split(' ');
        for (let i = 0, x = newStr.length; i < x; i += 1) {
            if (newStr[i][0]) {
                newStr[i] = newStr[i][0].toUpperCase() + newStr[i].substr(1);
            }
        }
        return newStr.join(' ');
    }
    return '';
};

const dictZone = {
    R: 'Rural',
    U: 'Urbana',
    M: 'Mixta'
};

const hasText = (str) => {
    if (str === '' || str === 'NULL' || str === 'Null') {
        return '---';
    }
    return str;
};

const MilitantCard = ({ militant }) => {
    const [expanded, setExpanded] = useState(false);
    const curClassName = expanded ? 'detail-militant--expanded' : 'detail-militant--collapsed';

    const mName = capitalLetter(militant.name);
    const mPaternal = upperFirst(militant.paternal_surname);
    const mMaternal = upperFirst(militant.maternal_surname);
    const mStreet = hasText(upperFirst(militant.street));
    const mSuburb = capitalLetter(militant.suburb);
    const mMunicipality = upperFirst(militant.municipality);
    const mState = upperFirst(militant.state);
    const mAge = moment().diff(militant.birthdate, 'years');
    return (
        <div {...styles}>
            <div className="main-militant">
                <div>
                    <h6>Nombre:</h6>
                    <p>{`${mName} ${mPaternal} ${mMaternal}`}</p>
                </div>
                <div>
                    <h6>Sexo:</h6>
                    <p>{militant.gender}</p>
                </div>
                <div>
                    <h6>Edad:</h6>
                    <p>{mAge}</p>
                </div>
                <div>
                    <h6>Celular:</h6>
                    <p>{hasText(militant.mobile)}</p>
                </div>
                <div>
                    <h6>Teléfono:</h6>
                    <p>{hasText(militant.phone)}</p>
                </div>
                <div>
                    <h6>Clave Electoral:</h6>
                    <p>{militant.electoral_code}</p>
                </div>
                <div>
                    <h6>Dirección:</h6>
                    <p>{`${mStreet} ${militant.exterior_number} ${militant.interior_number} ${mSuburb}, ${mMunicipality}, ${mState}, C.P. ${militant.postal_code}`}</p>
                </div>
                {(() => {
                    if (expanded) {
                        return null;
                    }
                    return (
                        <div>
                            <Button
                                ghost
                                bgColor="#FFF"
                                style={{ margin: 10 }}
                                onClick={() => setExpanded(!expanded)}
                            >
                                Más información
                                <FontAwesomeIcon icon={faChevronDown} />
                            </Button>
                        </div>
                    );
                })()}
            </div>
            <div className={`detail-militant ${curClassName}`}>
                <div className="detail-militant__location">
                    <h5>Ubicación</h5>
                    <div className="detail-militant__location--grid">
                        <div className="detail-militant__location--grid--1">
                            <div>
                                <h6>Estado:</h6>
                                <p>{mState}</p>
                            </div>
                            <div>
                                <h6>Estado ID:</h6>
                                <p>{militant.state_id}</p>
                            </div>
                            <div>
                                <h6>Municipio:</h6>
                                <p>{mMunicipality}</p>
                            </div>
                            <div>
                                <h6>Municipio ID:</h6>
                                <p>{militant.municipality_id}</p>
                            </div>
                            <div>
                                <h6>Cabecera local:</h6>
                                <p>{upperFirst(militant.local_headboard)}</p>
                            </div>
                            <div>
                                <h6>Cabecera local ID:</h6>
                                <p>{`${militant.local_headboard_id} (${militant.local_headboard_roman_id})`}</p>
                            </div>
                            <div>
                                <h6>Cabecera federal:</h6>
                                <p>{upperFirst(militant.federal_headboard)}</p>
                            </div>
                            <div>
                                <h6>Cabecera federal ID:</h6>
                                <p>{militant.federal_headboard_id}</p>
                            </div>
                        </div>
                        <div className="detail-militant__location--grid--2">
                            <div>
                                <h6>Calle:</h6>
                                <p>{mStreet}</p>
                            </div>
                            <div>
                                <h6>Colonia:</h6>
                                <p>{hasText(mSuburb)}</p>
                            </div>
                            <div>
                                <h6>Número exterior:</h6>
                                <p>{militant.exterior_number}</p>
                            </div>
                            <div>
                                <h6>Manzana:</h6>
                                <p>{militant.square}</p>
                            </div>
                            <div>
                                <h6>Número interior:</h6>
                                <p>{hasText(militant.interior_number)}</p>
                            </div>
                            <div>
                                <h6>Código postal:</h6>
                                <p>{militant.postal_code}</p>
                            </div>
                        </div>
                    </div>
                    <div className="detail-militant__location--flex">
                        <div>
                            <h6>Sección:</h6>
                            <p>{militant.section_id}</p>
                        </div>
                        <div>
                            <h6>Zona:</h6>
                            <p>{dictZone[militant.type]}</p>
                        </div>
                    </div>
                </div>
                <div className="detail-militant__personal">
                    <h5>Datos personales</h5>
                    <div className="detail-militant__personal--row">
                        <div>
                            <h6>Nombre:</h6>
                            <p>{mName}</p>
                        </div>
                        <div>
                            <h6>Apellido paterno:</h6>
                            <p>{mPaternal}</p>
                        </div>
                        <div>
                            <h6>Apellido materno:</h6>
                            <p>{mMaternal}</p>
                        </div>
                    </div>
                    <div className="detail-militant__personal--row">
                        <div>
                            <h6>Edad:</h6>
                            <p>{mAge}</p>
                        </div>
                        <div>
                            <h6>Sexo:</h6>
                            <p>{militant.gender}</p>
                        </div>
                        <div>
                            <h6>Fecha de nacimiento:</h6>
                            <p>{moment(militant.birthdate).format('L')}</p>
                        </div>
                    </div>
                    <div className="detail-militant__personal--row">
                        <div>
                            <h6>Clave electoral:</h6>
                            <p>{militant.electoral_code}</p>
                        </div>
                        <div>
                            <h6>Número de credencial:</h6>
                            <p>{hasText(militant.electoral_card_number)}</p>
                        </div>
                    </div>
                    <div className="detail-militant__personal--row">
                        <div>
                            <h6>Teléfono:</h6>
                            <p>{hasText(militant.phone)}</p>
                        </div>
                        <div>
                            <h6>Celular:</h6>
                            <p>{hasText(militant.mobile)}</p>
                        </div>
                        <div>
                            <h6>Correo:</h6>
                            <p>{hasText(militant.email)}</p>
                        </div>
                    </div>
                </div>
                <div className="detail-militant__afiliation">
                    <h5>Afiliación</h5>
                    <div className="detail-militant__afiliation--row">
                        <div>
                            <h6>Fecha de Afiliación:</h6>
                            <p>{moment(militant.affilation_date).format('L')}</p>
                        </div>
                        <div>
                            <h6>Cargo:</h6>
                            <p>{hasText(militant.partisan_position)}</p>
                        </div>
                        <div>
                            <h6>Categoria:</h6>
                            <p>{hasText(militant.category)}</p>
                        </div>
                    </div>
                </div>
                <div className="detail-militant__button">
                    <Button
                        ghost
                        bgColor="#FFF"
                        onClick={() => setExpanded(false)}
                    >
                        Menos información
                        <FontAwesomeIcon icon={faChevronUp} />
                    </Button>
                </div>
            </div>
        </div>
    );
};

MilitantCard.propTypes = {
    militant: PropTypes.shape({})
};

MilitantCard.defaultProps = {
    militant: {}
};

export default MilitantCard;
