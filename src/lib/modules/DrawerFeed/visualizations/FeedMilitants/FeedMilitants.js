import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cloneDeep from 'lodash/cloneDeep';

import fetchExp from './queries';
import ListMilitants from './ListMilitants';
import { loadingBoxStyles } from './styles';
import LoadingModule from '../../../LoadingModule';

const propTypes = {
    service: PropTypes.string,
    queryData: PropTypes.object,
};

const defaultProps = {
    service: '',
    queryData: {}
};

class FeedTweets extends Component {
    constructor(props) {
        super(props);
        this.fetchMoreData = this.fetchMoreData.bind(this);
        this.state = {
            skip: 0,
            data: [],
            isLoading: true,
            paginationId: '',
            isReloading: false,
            queryData: props.queryData
        };
    }

    componentDidMount() {
        const { queryData } = this.props;
        this.fetchData(queryData);
    }

    fetchData(queryData) {
        fetchExp(this.props.service, queryData)
            .subscribe(
                (response) => {
                    let newData = cloneDeep(this.state.data);
                    if (this.state.isReloading) {
                        newData = response.data;
                    } else {
                        newData.push(...response.data);
                    }
                    // setStringPosts(
                    //          ${response.data.explorationInGraphic.total} de ${datasetActive.consumption_total} tweets`);
                    this.setState({
                        data: newData,
                        isLoading: false,
                        skip: response.data.length,
                        paginationId: response.pagination_id
                    });
                },
                (error) => {
                    console.log(error); // eslint-disable-line
                    this.setState({
                        data: [],
                        isLoading: false
                    });
                },
                null
            );
    }

    fetchMoreData() {
        const { queryData, paginationId } = this.state;
        const newQuery = {
            ...queryData,
            filters: {
                ...queryData.filters,
                pagination_id: paginationId
            }
        };
        this.fetchData(newQuery);
    }

    render() {
        const {
            data, isLoading, isReloading, skip
        } = this.state;
        const { queryData } = this.props;
        if (isLoading) {
            return (
                <div {...loadingBoxStyles}>
                    <LoadingModule />
                </div>
            );
        }
        return (
            <ListMilitants
                data={data}
                skip={skip}
                isReloading={isReloading}
                limit={queryData.filters.limit}
                fetchMoreData={this.fetchMoreData}
            />
        );
    }
}

FeedTweets.propTypes = propTypes;
FeedTweets.defaultProps = defaultProps;

export default FeedTweets;
