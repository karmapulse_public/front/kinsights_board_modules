import { Observable } from 'rxjs';
import { fetchXHR } from '../../../../helpers/http';

export default (url, params) => (
    Observable.create((observer) => {
        Observable.fromPromise(fetchXHR(url, 'GET', params))
            .subscribe(
                (data) => {
                    const { response } = data.json;
                    observer.next({
                        ...response
                    });
                },
                (error) => { observer.error(error); }
            );
    })
);
