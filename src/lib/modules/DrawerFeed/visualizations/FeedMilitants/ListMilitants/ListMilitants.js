import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import Button from 'kpulse_design_system/dist/Button';

import LoadingModule from '../../../../LoadingModule';
import MilitantCard from '../MilitantCard';
import { listStyles } from './styles';

const propTypes = {
    skip: PropTypes.number,
    limit: PropTypes.number,
    isReloading: PropTypes.bool,
    fetchMoreData: PropTypes.func,
    data: PropTypes.arrayOf(PropTypes.shape({})),
};

const defaultProps = {
    skip: 1,
    limit: 0,
    data: [],
    isReloading: false,
    fetchMoreData: () => ({})
};

const ListMilitants = (props) => {
    const {
        data, fetchMoreData, skip, limit, isReloading
    } = props;
    const renderShowMore = () => {
        if (isEmpty(data)) {
            return <h4 className="nomore">La información estará lista pronto</h4>;
        } else if (skip < limit) {
            return <h4 className="nomore">No hay más datos que mostrar</h4>;
        }
        return (
            <div className="showmore">
                <Button
                    large
                    round
                    secondary
                    bgColor="#FFF"
                    style={{ margin: '20px auto' }}
                    onClick={() => { fetchMoreData(); }}
                >
                    Mostrar más
                </Button>
            </div>
        );
    };
    return (
        <div {...listStyles()}>
            {(() => {
                if (isReloading) {
                    return (
                        <div className="loading">
                            <LoadingModule />
                        </div>
                    );
                }
                return (
                    <Fragment>
                        {data.map(m => <MilitantCard key={m.id} militant={m} />)}
                        {renderShowMore()}
                    </Fragment>
                );
            })()}
        </div>
    );
};

ListMilitants.propTypes = propTypes;
ListMilitants.defaultProps = defaultProps;

export default ListMilitants;
