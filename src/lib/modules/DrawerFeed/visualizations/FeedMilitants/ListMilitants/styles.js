import { css } from 'glamor';

export const listStyles = () => css({
    backgroundColor: '#263238',
    width: '100%',
    height: 'calc(100% - 75px)',
    overflow: 'auto',
    padding: '0px',
    ' .loading': {
        height: '90%',
        position: 'relative',
        ' > div': {
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)'
        }
    },
    ' .nomore': {
        textAlign: 'center',
        margin: 0,
        padding: '25px 0 35px',
        boxSizing: 'border-box',
        fontWeight: 500,
        fontSize: 14,
        color: '#ffff',
    }
});

export const cardFeedStyles = () => css({
    position: 'relative',
    width: 560,
});
