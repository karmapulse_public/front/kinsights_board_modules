import React from 'react';
import PropTypes from 'prop-types';
import isEqual from 'lodash/isEqual';
import isUndefined from 'lodash/isUndefined';
import replyIcon from '../../../../../../helpers/icons/replyIcon';

const propTypes = {
    childTweet: PropTypes.object,
    type: PropTypes.string,
    intl: PropTypes.object, //eslint-disable-line
};

const defaultProps = {
    childTweet: {},
    type: ''
};
const Reply = (props) => {
    if (isEqual(props.type, 'reply')) {
        let users;
        if (isUndefined(props.childTweet.mentions)) {
            const regex = /^https?:\/\/(www\.)?twitter\.com\/(#!\/)?([^/]+)(\/\w+)*$/;
            const regexNew = /^https?:\/\/(www\.)?twitter\.com\/\/(#!\/)?([^/]+)(\/\w+)*$/;
            let match = regex.exec(props.childTweet.link);
            if (!match) match = regexNew.exec(props.childTweet.link);
            users = () => (
                <p>
                    En respuesta a
                    <a href={`http://www.twitter.com/${match[3]}`}>@{match[3]}</a>
                </p>
            );
        } else if (props.childTweet.mentions.length > 1) {
            users = () => (
                <p>
                    En respuesta a
                    <a href={`http://www.twitter.com/${props.childTweet.mentions[0]}`}>
                        @{props.childTweet.mentions[0]}
                    </a>y a {props.childTweet.mentions.length - 1 } más
                </p>
            );
        } else {
            users = () => (
                <p>
                    En respuesta a
                    <a href={`http://www.twitter.com/${props.childTweet.mentions[0]}`}>
                        @{props.childTweet.mentions[0]}
                    </a>
                </p>
            );
        }
        return (
            <div className="twitter-card__reply">
                {replyIcon({ color: '#000' })}
                {users()}
            </div>
        );
    }
    return <div />;
};

Reply.propTypes = propTypes;
Reply.defaultProps = defaultProps;

export default Reply;
