import React from 'react';
import PropTypes from 'prop-types';
import isEqual from 'lodash/isEqual';
import Gallery from '../../TwitterGallery';

const propTypes = {
    childTweet: PropTypes.object,
    type: PropTypes.string
};

const defaultProps = {
    childTweet: {},
    type: ''
};
const Quote = ({ childTweet, type }) => {
    if (isEqual(type, 'quote')) {
        const tweetRender = () => ({
            body: { __html: childTweet.body },
            name: { __html: childTweet.user.name },
            username: { __html: `@${childTweet.user.username}` }
        });
        return (
            <div className="twitter-card__quote">
                {
                    (() => {
                        if (!childTweet.images) {
                            return <div />;
                        }
                        return (
                            <div className="container__media">
                                <Gallery
                                    items={childTweet.images}
                                />
                            </div>
                        );
                    })()
                }
                <div>
                    <div className="container__header">
                        <div>
                            <a
                                className="container__header__name"
                                href={childTweet.user.link}
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                <span dangerouslySetInnerHTML={tweetRender().name} />
                            </a>
                            <a
                                className="container__header__username"
                                href={childTweet.user.link}
                                target="_blank"
                                rel="noopener noreferrer"
                            >
                                <span dangerouslySetInnerHTML={tweetRender().username} />
                            </a>
                        </div>
                    </div>
                    <div className="container__body" >
                        <div dangerouslySetInnerHTML={tweetRender().body} />
                    </div>
                </div>
            </div>
        );
    }
    return <div />;
};

Quote.propTypes = propTypes;
Quote.defaultProps = defaultProps;

export default Quote;
