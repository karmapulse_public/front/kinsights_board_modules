import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import isEqual from 'lodash/isEqual';
import noUser from '../../../../../../helpers/icons/NoUserIcon';
import { RetweetIconM } from '../../../../../../helpers/icons/retweetIcon';
import UserIcon from '../../../../../../helpers/icons/userIcon';
import { nFormatter } from '../../../../../../helpers/number';

const propTypes = {
    childTweet: PropTypes.object,
    type: PropTypes.string,
};

const defaultProps = {
    childTweet: {},
    type: ''
};

class Retweet extends Component {
    constructor(props) {
        super(props);
        this.handleNoImage = this.handleNoImage.bind(this);
    }

    handleNoImage() {
        const Image = this.imgContainerRetweet.getElementsByClassName('profilephotoRetweet');
        this.imgContainerRetweet.removeChild(Image[0]);
        ReactDOM.render(noUser(), this.imgContainerRetweet);
    }

    render() {
        const { childTweet, type } = this.props;

        if (isEqual(type, 'retweet')) {
            const tweetRender = () => ({
                username: { __html: `@${childTweet.user.username}` }
            });
            return (
                <div className="twitter-card__retweets">
                    {RetweetIconM()}
                    <a
                        className="twitter-card__retweets__user-image"
                        href={childTweet.user.link}
                        target="_blank"
                        rel="noopener noreferrer"
                        ref={(c) => { this.imgContainerRetweet = c; }}
                    >
                        <img
                            src={childTweet.user.image}
                            alt="User profile"
                            className="profilephotoRetweet"
                            onError={this.handleNoImage}
                        />
                    </a>
                    <a
                        href={childTweet.user.link}
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        <span dangerouslySetInnerHTML={tweetRender().username} />
                    </a>
                    <p>retwitteó</p>
                    <div className="twitter-card__retweets__counters">
                        <div className="twitter-card__retweets__counters__followers">
                            <h5>
                                <UserIcon color="#000" />
                                {nFormatter(childTweet.user.followers)}
                            </h5>
                        </div>
                    </div>
                    <div className="twitter-card__retweets__line" />
                </div>
            );
        }
        return <div />;
    }
}

Retweet.propTypes = propTypes;
Retweet.defaultProps = defaultProps;

export default Retweet;
