import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import Gallery from '../TwitterGallery';
import TwitterCard from '../TwitterCard';
import LoadingModule from '../../../../LoadingModule';
import { listTweetsStyles } from './ListTweetsStyles';

const propTypes = {
    skip: PropTypes.number,
    limit: PropTypes.number,
    isReloading: PropTypes.bool,
    fetchMoreData: PropTypes.func,
    data: PropTypes.arrayOf(PropTypes.shape({})),
};

const defaultProps = {
    skip: 1,
    limit: 0,
    data: [],
    isReloading: false,
    fetchMoreData: () => ({})
};

const ListTweets = (props) => {
    const {
        data, fetchMoreData, skip, limit, isReloading
    } = props;
    const renderShowMore = () => {
        if (skip < limit) {
            return <h4 className="nomore">No hay más datos que mostrar</h4>;
        }
        return (
            <div className="showmore">
                <button
                    onClick={() => { fetchMoreData(); }}
                >
                    Mostrar más
                </button>
            </div>
        );
    };
    return (
        <div {...listTweetsStyles()}>
            {(() => {
                if (isReloading) {
                    return (
                        <div className="tweet__loading">
                            <LoadingModule />
                        </div>
                    );
                }
                return (
                    <Fragment>
                        {data.map(tweet => (
                            <TwitterCard
                                tweet={tweet}
                                key={tweet.id}
                                viewLabels="vacio"
                            >
                                <Gallery items={tweet.images} />
                            </TwitterCard>
                        ))}
                        {renderShowMore()}
                    </Fragment>
                );
            })()}
        </div>
    );
};

ListTweets.propTypes = propTypes;
ListTweets.defaultProps = defaultProps;

export default ListTweets;
