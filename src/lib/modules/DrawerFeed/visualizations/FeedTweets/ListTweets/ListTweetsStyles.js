import { css } from 'glamor';

export const listTweetsStyles = () => css({
    backgroundColor: '#f5f5f5',
    height: 'calc(100vh - 65px)',
    overflow: 'auto',
    padding: '23px 25px 0px',
    boxSizing: 'border-box',
    ' .tweet__filter': {
        display: 'flex',
        alignItems: 'center',
        marginBottom: 20,
        ' > p': {
            fontSize: 14,
            color: '#000',
            marginRight: 5,
        },
        '&__item': {
            padding: '6px 10px',
            margin: '0px 10px',
            fontSize: 14,
            color: '#3a3a3a',
            cursor: 'pointer',
            transition: 'all 0.5s ease',
            '&:hover, &.active': {
                borderRadius: 2,
                backgroundColor: 'rgba(220,220,220, 0.46)',
            }
        },
        ' &__line': {
            height: 25,
            width: 1,
            margin: 0,
            borderLeft: 'solid 1px #979797',
            opacity: 0.15,
        }
    },
    ' .tweet__loading': {
        height: '90%',
        position: 'relative',
        ' > div': {
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)'
        }
    },
    ' .showmore': {
        textAlign: 'center',

        ' button': {
            color: '#03a9f4',
            width: 560,
            padding: '10px 0',
            border: 'none',
            backgroundColor: '#FFF',
            boxShadow: '0 2px 2px 0 rgba(0, 0, 0, 0.12), 0 0 2px 0 rgba(0, 0, 0, 0.12)',
            marginBottom: 22,
            fontSize: 14,
            fontWeight: 400,
            textAlign: 'center',
            textTransform: 'uppercase',
            cursor: 'pointer',
        }
    },

    ' .nomore': {
        textAlign: 'center',
        margin: 0,
        padding: '25px 0 35px',
        boxSizing: 'border-box',
        fontWeight: 500,
        fontSize: 14,
        color: '#ffff',
    }
});

export const cardFeedStyles = () => css({
    position: 'relative',
    width: 560,
});
