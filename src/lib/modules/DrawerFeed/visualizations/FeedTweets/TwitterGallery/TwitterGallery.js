import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';

import styles from './TwitterGalleryStyles';
// import imageDefault from '../../../../../helpers/icons/imageDeleted';

const propTypes = {
    items: PropTypes.array
};

const defaultProps = {
    items: []
};

const TwitterGallery = (props) => {
    const images = () => (props.items.map(
        (e, i) => (
            <figure
                key={i}
                style={{ backgroundImage: `url(${e.url ? e.url.replace('http:', 'https:') : ''})` }}
            />
        )
    ));

    const galeryItems = () => {
        if (!isEmpty(props.items)) {
            const n = props.items.length;
            return (
                <div {...styles(n)}>
                    { images() }
                </div>
            );
        }
        return '';
    };

    return (
        <div>
            {galeryItems()}
        </div>
    );
};

TwitterGallery.propTypes = propTypes;
TwitterGallery.defaultProps = defaultProps;

export default TwitterGallery;
