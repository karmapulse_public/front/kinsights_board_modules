import { css } from 'glamor';

const titleSection = () => {
    const color = 'rgba(0,0,0,0.3)';
    const collet = {
        position: 'absolute',
        top: 0,
        right: 0,
        margin: 0,
        padding: 20,
        fontSize: 12,
        fontWeight: 500
    };
    const icon = {
        percepcindepersonaje: `url("data:image/svg+xml; utf8,  <svg xmlns='http://www.w3.org/2000/svg' width='16' height='19' viewBox='0 0 16 19'><g fill='${color}' fill-rule='evenodd'><path d='M8 8C6.346 8 5 6.654 5 5h6c0 1.654-1.346 3-3 3m0-8C5.243 0 3 2.243 3 5s2.243 5 5 5 5-2.243 5-5-2.243-5-5-5M5.08 11.36C1.988 12.162 0 14.256 0 17v2h6.778L5.08 11.36zM10.92 11.36L9.222 19H16v-2c0-2.744-1.988-4.838-5.08-5.64M7 14l1-3 1 3-1 4z'/></g></svg>")`,
        monitoreoagendapoltica: `url("data:image/svg+xml; utf8,  <svg xmlns='http://www.w3.org/2000/svg' width='18' height='18' viewBox='0 0 18 18'><path fill='${color}' fill-rule='evenodd' d='M15.545 9.818h-3.272V6.545h3.272v3.273zm-4.909 0H7.364V6.545h3.272v3.273zm0 4.91H7.364v-3.273h3.272v3.272zm-4.909-4.91H2.455V6.545h3.272v3.273zm0 4.91H2.455v-3.273h3.272v3.272zM16.364 1.635h-1.637V0h-1.636v4.09h-1.636V1.637h-4.91V0H4.91v4.09H3.273V1.637H1.636C.734 1.636 0 2.371 0 3.273v12.272c0 .903.734 1.637 1.636 1.637h14.728c.902 0 1.636-.734 1.636-1.637V3.273c0-.902-.734-1.637-1.636-1.637z'/></svg>")`,
        reportedemografico: `url("data:image/svg+xml; utf8,  <svg xmlns='http://www.w3.org/2000/svg' width='20' height='19' viewBox='0 0 20 19'><g fill='${color}' fill-rule='evenodd'><path d='M19.76 16.807l-1.567-7.048c-.219-.986-1.183-1.76-2.193-1.76h-1.52C12.701 10.986 10 14 10 14s-2.702-3.014-4.48-6H4c-1.01 0-1.974.774-2.193 1.76L.24 16.807c-.123.554 0 1.112.337 1.532.337.42.856.66 1.423.66h16c.567 0 1.085-.24 1.422-.66.337-.42.46-.978.337-1.532'/><path d='M8 4a2 2 0 1 1 4 0 2 2 0 0 1-4 0m6 0a4 4 0 0 0-8 0c0 2.21 4 7 4 7s4-4.79 4-7'/></g></svg>")`,
        monitoreodecomunicacion: `url("data:image/svg+xml; utf8,  <svg xmlns='http://www.w3.org/2000/svg' width='21' height='18' viewBox='0 0 21 18'><path fill='${color}' fill-rule='evenodd' d='M16 8h-6V6h6v2zm0 4h-6v-2h6v2zM7 8.25a1.25 1.25 0 1 1 0-2.5 1.25 1.25 0 0 1 0 2.5zm0 4a1.25 1.25 0 1 1 0-2.5 1.25 1.25 0 0 1 0 2.5zM11 0C5.486 0 1 4.038 1 9c0 1.735.541 3.392 1.573 4.836L0 18h11c5.514 0 10-4.038 10-9s-4.486-9-10-9z'/></svg>")`,
        analisisdeopinion: `url("data:image/svg+xml; utf8,  <svg xmlns='http://www.w3.org/2000/svg' width='20' height='24' viewBox='0 0 20 24'><g fill='${color}' fill-rule='evenodd'><path d='M10 6c1.654 0 3-1.346 3-3 0-1.655-1.346-3-3-3S7 1.344 7 3c0 1.654 1.346 3 3 3M6 15h1v4a1 1 0 0 0 1 1h4a1 1 0 0 0 1-1v-4h1a1 1 0 0 0 1-1v-2c0-2.757-2.243-5-5-5s-5 2.243-5 5v2a1 1 0 0 0 1 1'/><path d='M16.34 16.823l-.578 1.914C17.61 19.297 18 19.903 18 20c0 .514-2.75 2-8 2s-8-1.486-8-2c0-.1.417-.738 2.396-1.309l-.555-1.92C.667 17.684 0 19.03 0 20c0 2.747 5.184 4 10 4 4.817 0 10-1.253 10-4 0-.946-.635-2.264-3.66-3.177'/></g></svg>")`,
    }
    Object.keys(icon).map((item) => {
        collet[` &#${item}::before`] = {
            position: 'absolute',
            top: 15,
            left: '-10px',
            content: icon[item],
            width: 18,
            height: 18
        };
    });
    return collet;
}

const styles = color => css({
    position: 'relative',
    // Headers
    // ----------------------
    ' h1': {
        fontSize: '1.6em',
        margin: '1em 0 .5em'
    },
    ' h2': {
        fontSize: '1.5em',
        margin: '1em 0 .5em'
    },
    ' h3': {
        fontSize: '1.4em',
        margin: '1em 0 .5em'
    },
    ' h4': {
        fontSize: '1.3em',
        margin: '1em 0 .5em'
    },
    ' h5': {
        fontSize: '1.2em',
        margin: '1em 0 .5em'
    },
    ' h6': {
        fontSize: '1.1em',
        margin: '1em 0 .5em'
    },
    // Paragraph
    // ----------------------
    ' p': {
        lineHeight: 1.2,
        fontWeight: 300
    },
    ' em': {
        fontStyle: 'italic'
    },
    ' strong': {
        fontWeight: 700
    },
    ' a': {
        color,
        textDecoration: 'none'
    },
    // Lists
    // ----------------------
    ' li': {
        lineHeight: 1.2
    },
    ' ul': {
        margin: '1em 0',
        paddingLeft: 20,
        listStyleType: 'disc'
    },
    ' ol': {
        margin: '1em 0',
        paddingLeft: 20,
        listStyleType: 'decimal'
    },
    ' .module-markdown': {
        position: 'relative',
        padding: 20,

        ' h1': {
            margin: 0,
            padding: '30px 0 20px 0 ',
            fontSize: 24,
            fontWeight: 500,
        },
        ' h2': titleSection(),
        ' p, li': {
            marginBottom: '1em',
            padding: 0,
            fontSize: 14,
            opacity: '0.84',
            lineHeight: '1.5',
            ' img': {
                display: 'block',
                margin: '40px auto',
                maxWidth: 550,
                boxShadow: '0 2px 8px 0 rgba(0, 0, 0, 0.1)'
            }
        }
    },
    ' .date': {
        position: 'absolute',
        top: 0,
        left: 0,
        margin: 0,
        padding: 20,
        opacity: 0.5,
        fontSize: 12,
        fontWeight: 500,
        textTransform: 'capitalize',
    }
});

export default styles;
