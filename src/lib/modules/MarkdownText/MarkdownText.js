import React from 'react';
import PropTypes from 'prop-types';
import showdown from 'showdown';
import moment from 'moment';
import styles from './MarkdownTextStyles';

const propTypes = {
    fields: PropTypes.object,
    sys: PropTypes.object,
    color: PropTypes.string
};

const defaultProps = {
    fields: {},
    sys: {},
    color: '#555'
};

const MarkdownText = ({ fields: { markdown_text, special_format }, sys: { updatedAt }, color }) => {
    const converter = new showdown.Converter();
    const descriptionText = converter.makeHtml(markdown_text);
    const date = moment(updatedAt).format('MMM DD, YYYY H:mm');
    const s = !special_format ? styles : () => ({});
    return (
        <div
            {...s()}
            className="m-markdown-text"
        >
            <h4 className="date">{date}</h4>
            <div
                className="module-markdown"
                dangerouslySetInnerHTML={{
                    __html: descriptionText
                }}
            />
        </div>
    );
};

MarkdownText.propTypes = propTypes;
MarkdownText.defaultProps = defaultProps;

export default MarkdownText;
