import React from 'react';
import PropTypes from 'prop-types';
import NoDataModule from '../../../NoDataModule';
import { numberWithCommas } from '../../../../helpers/number';

import styles from './styles';
import { ConversationIcon } from '../../../../helpers/icons/gob';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    noDataYet: PropTypes.func
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    noDataYet: null
};

const PostTotals = ({ moduleColor, moduleData, moduleConfig, noDataYet }) => {

    const moduleImage = (moduleData.data.reduce((prev, item) => (prev += item.image), 0));
    const moduleLink = (moduleData.data.reduce((prev, item) => (prev += item.link), 0));
    const moduleText = (moduleData.data.reduce((prev, item) => (prev += item.text), 0));
    const moduleVideo = (moduleData.data.reduce((prev, item) => (prev += item.video), 0));

    return (
        <article className="card-total-numers" {...styles(moduleColor)}>
            <div className="mentions">
                <div className="mentions__title">
                    <h3>{moduleConfig.title}</h3>
                </div>
            </div>
            {
                (() => {
                    if (noDataYet === null) {
                        return (moduleData.total !== 0)
                            ? (
                                <div className="mentions__body">
                                    <div className="mentions__body__total">
                                        <div>
                                            <ConversationIcon />
                                            <h4>{numberWithCommas(moduleData.total)}</h4>
                                        </div>
                                        <h5>Publicaciones totales</h5>
                                    </div>
                                    <div className="mentions__body__numbers">
                                        <div>
                                            <h4 className="mentions__body__numbers__video">
                                                {moduleVideo}
                                            </h4>
                                            <h5>
                                                Video
                                            </h5>
                                        </div>
                                        <div>
                                            <h4 className="mentions__body__numbers__imagenes">
                                                {moduleImage}
                                            </h4>
                                            <h5>
                                                Imagen
                                            </h5>
                                        </div>
                                        <div>
                                            <h4 className="mentions__body__numbers__texto">
                                                {moduleText}
                                            </h4>
                                            <h5>
                                                Texto
                                            </h5>
                                        </div>
                                        <div>
                                            <h4 className="mentions__body__numbers__link">
                                                {moduleLink}
                                            </h4>
                                            <h5>
                                                Link
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                            )
                            : (<NoDataModule />);
                    }

                    return noDataYet();
                })()

            }
        </article>
    );
};

PostTotals.propTypes = propTypes;
PostTotals.defaultProps = defaultProps;

export default PostTotals;
