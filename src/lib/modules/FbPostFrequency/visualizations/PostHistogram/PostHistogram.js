import React from 'react';
import PropTypes from 'prop-types';
import NoDataModule from '../../../NoDataModule';
import { numberWithCommas } from '../../../../helpers/number';

import styles from './styles';
import ChartColumns from '../../../Charts/ChartColumns/ChartColumns';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666'
};

const PostHistogram = ({ moduleColor, moduleData, moduleConfig }) => {
    const dataKeys = [
        {
            key: 'text',
            color: '#6f9cbc'
        },
        {
            key: 'link',
            color: '#c4d5cc'
        },
        {
            key: 'image',
            color: '#ce9089'
        },
        {
            key: 'video',
            color: '#f3ccad'
        },
    ];
    const nameArray = [
        'Texto', 'Link', 'Imagen', 'Video'
    ];
    return (
        <article className="post-histogram" {...styles(moduleColor)}>
            <div className="post-histogram__label">
                <h3>{moduleConfig.title}</h3>
            </div>
            {
                (moduleData.total !== 0)
                ? (
                    <div className="post-histogram__content">
                        <h4>Publicaciones totales: <span>{numberWithCommas(moduleData.total)}</span></h4>
                        <ChartColumns
                            dataKeys={dataKeys}
                            nameArray={nameArray}
                            data={moduleData.data}
                        />
                    </div>
                )
                : (<NoDataModule />)
            }
        </article>
    );
};

PostHistogram.propTypes = propTypes;
PostHistogram.defaultProps = defaultProps;

export default PostHistogram;
