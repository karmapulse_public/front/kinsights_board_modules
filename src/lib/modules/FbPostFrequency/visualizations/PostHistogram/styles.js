import { css } from 'glamor';

const styles = color => css({
    ' .tooltip_custom': {
        backgroundColor: 'rgba(255, 255, 255, .9)',
        boxShadow: '2px 2px 10px rgba(0, 0, 0, .2)',
        fontSize: 12,
        padding: 10,
        borderRadius: 2,
        letterSpacing: '1px',
        lineHeight: '20px',
    },
    ' h4': {
        padding: '20px 40px',
        fontSize: '13px',
        fontWeight: 500,
        ' span': {
            fontSize: 16,
            float: 'right'
        }
    },
    ' .post-histogram': {

        ' &__label': {
            display: 'flex',
            justifyContent: 'space-between',
            padding: '15px 20px 15px 20px',
            borderTop: '1px solid #dfdfdf',

            ' h3': {
                fontSize: '13px',
                fontWeight: 600,
                color
            },
        },
        ' &__content': {
            padding: '0px 30px 20px',

            ' .recharts-cartesian-grid-horizontal line': {
                stroke: ' rgba(204,204,204,0.2)'
            }
        }
    }
});

export default styles;
