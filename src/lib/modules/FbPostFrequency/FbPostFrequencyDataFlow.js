import { Observable } from 'rxjs';
import { requestPost } from '../../helpers/http';

// const fakeData = {
//     randomDate: (start, end) =>
//         (new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime())).getTime()), // eslint-disable-line
//     randomNumber: (max, min) =>
//         (Math.floor(Math.random() * (max - min)) + min)
// };

// const data = {
//     total: 38405,
//     data: (() => {
//         const array = [];
//         for (let i = 0; i < 30; i++) {
//             array.push({
//                 date: fakeData.randomDate(new Date(2017, 0, 1), new Date()),
//                 image: fakeData.randomNumber(300, 0),
//                 text: fakeData.randomNumber(300, 0),
//                 video: fakeData.randomNumber(300, 0),
//                 link: fakeData.randomNumber(300, 0)
//             });
//         }
//         return array;
//     })()
// };

export default (url, params) => (
    Observable.create((observer) => {
        Observable.fromPromise(requestPost(url, 'POST', params))
            .subscribe(
                (data) => {
                    observer.next(data.json);
                },
                (error) => { observer.error(error); }
            );
    })
);
