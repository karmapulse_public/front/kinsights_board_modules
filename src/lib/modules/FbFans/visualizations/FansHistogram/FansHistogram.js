import React from 'react';
import PropTypes from 'prop-types';
import NoDataModule from '../../../NoDataModule';

import styles from './styles';
import { numberWithCommas } from '../../../../helpers/number';
import ChartColumns from '../../../Charts/ChartColumns/ChartColumns';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666'
};

const FansHistogram = ({ moduleColor, moduleData, moduleConfig }) => {
    const dataKeys = [
        {
            key: 'total',
            color: '#6587bc'
        }
    ];
    const nameArray = [
        'Seguidores'
    ];
    const typeClass = moduleData.total_percent > 0 ? 'positive' : 'negative';
    return (
        <div className="fans-histogram" {...styles(moduleColor)}>
            <div className="fans-histogram__label">
                <h3>{moduleConfig.title}</h3>
            </div>
            {
                (moduleData.data.length > 0)
                ? (
                    <div className="fans-histogram__content">
                        <div>
                            <div className="fans-histogram__info">
                                <h4>Seguidores totales:
                                    <span>{numberWithCommas(moduleData.total)}</span>
                                </h4>
                                <h4>Crecimiento en periodo actual:
                                    <span className={`fans-histogram__percent--${typeClass}`}>
                                        {moduleData.total_percent.toFixed(2).replace(/\.0$/, '') || 0} %
                                    </span>
                                </h4>
                            </div>
                            <ChartColumns
                                dataKeys={dataKeys}
                                data={moduleData.data}
                                nameArray={nameArray}
                            />
                        </div>
                    </div>
                )
                : <NoDataModule color={moduleColor} />
            }
        </div>
    );
};

FansHistogram.propTypes = propTypes;
FansHistogram.defaultProps = defaultProps;

export default FansHistogram;
