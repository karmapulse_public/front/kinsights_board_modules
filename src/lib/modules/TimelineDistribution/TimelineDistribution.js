import React, { Component } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import isNull from 'lodash/isNull';
import isUndefined from 'lodash/isUndefined';

import LoadingModule from '../LoadingModule';
import { calculateNewInterval } from '../../helpers/getTimeInterval';
import { fetchTweetsDist, fetchSentimentDist } from './TDistributionDataflow';

import TweetsHistogram from './visualizations/TweetsHistogram';
import SentimentHistogram from './visualizations/SentimentHistogram';

const propTypes = {
    fields: PropTypes.object,
    color: PropTypes.string,
    dateRange: PropTypes.object,
    services: PropTypes.object
};

const defaultProps = {
    fields: {},
    color: '#666',
    dateRange: {},
    services: {},
};

class TimelineDistribution extends Component {
    constructor(props) {
        super(props);
        this.setDateFilter = this.setDateFilter.bind(this);
        this.myRef = React.createRef();
        this.state = {
            moduleServices: props.services,
            moduleConfig: props.fields,
            moduleRange: props.dateRange,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleColor: props.color,
            intervalType: props.fields.intervalType || 'month',
            selectInterval: isUndefined(props.fields.selectInterval) ? true : props.fields.selectInterval
        };
    }

    componentDidMount() {
        this.setDateFilter();
    }

    componentDidUpdate(prevProps) {
        const { startDate, endDate } = this.props.dateRange;
        const { hasClick, search_id } = this.props.fields;
        if (startDate !== prevProps.dateRange.startDate ||
            endDate !== prevProps.dateRange.endDate ||
            search_id !== prevProps.fields.search_id) {
            this.setDateFilter('', this.props);
        }
        if (hasClick !== prevProps.fields.hasClick) {
            this.changeState(hasClick);
        }
    }

    setDateFilter(interval = '', newProps = this.props) {
        const { startDate, endDate } = newProps.dateRange;
        const startString = moment(startDate).startOf('day');
        const finishString = moment(endDate).endOf('day');
        let newInterval = this.state.intervalType;
        if (this.state.selectInterval) {
            newInterval = interval !== '' ? interval :
                calculateNewInterval(startString, finishString, this.myRef);
        }
        this.fetchData({
            rule_id: newProps.fields.search_id,
            initial_date: startString.utc().toISOString(),
            final_date: finishString.utc().toISOString()
        }, newInterval, newProps.dateRange);
    }

    fetchData(newParams, timeInterval, newDateRange) {
        const { moduleConfig } = this.state;
        const recipeId = {
            timeline_lines: 'module_tw_histogram',
            timeline_sentiment: 'module_tw_distribution_sentiment'
        };
        const fetchQuery = {
            timeline_lines: fetchTweetsDist,
            timeline_sentiment: fetchSentimentDist
        };
        const params = {
            recipe_id: recipeId[moduleConfig.visualization],
            rule_id: newParams.rule_id,
            initial_date: newParams.initial_date,
            final_date: newParams.final_date,
            filters: {
                entity: moduleConfig.entity,
                interval: timeInterval,
                timezone: 'America/Mexico_City'
            }
        };
        fetchQuery[moduleConfig.visualization](this.props.services.twitter, params)
            .subscribe(
                (data) => {
                    this.setState({
                        moduleData: data,
                        moduleRange: newDateRange,
                        moduleLoading: false,
                        intervalType: timeInterval
                    });
                },
                (error) => {
                    console.log(error); // eslint-disable-line
                    this.setState({
                        moduleData: {},
                        moduleRange: this.state.moduleRange,
                        moduleLoading: false,
                        intervalType: timeInterval
                    });
                },
                null
            );
    }

    changeState(hasClick) {
        this.setState({
            moduleConfig: {
                ...this.state.moduleConfig,
                hasClick
            }
        });
    }

    renderByViz() {
        const { moduleConfig, moduleData } = this.state;
        if (!isNull(moduleData)) {
            const visualizations = {
                timeline_lines: params => (
                    <TweetsHistogram
                        {...params}
                        changeInterval={this.setDateFilter}
                    />
                ),
                timeline_sentiment: params => (
                    <SentimentHistogram
                        {...params}
                        changeInterval={this.setDateFilter}
                    />
                ),
            };
            return visualizations[moduleConfig.visualization](this.state);
        }

        return (<LoadingModule />);
    }

    render() {
        return (
            <div className="m-timeline-distribution" ref={this.myRef}>
                {this.renderByViz()}
            </div>
        );
    }
}

TimelineDistribution.propTypes = propTypes;
TimelineDistribution.defaultProps = defaultProps;

export default TimelineDistribution;
