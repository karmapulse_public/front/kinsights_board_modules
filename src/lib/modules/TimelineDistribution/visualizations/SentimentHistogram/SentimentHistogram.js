import React, { Component, Fragment } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';

import styles from './styles';
import NoDataModule from '../../../NoDataModule';
import Intervals from '../../../Charts/Intervals';
import { labelType } from '../../../../helpers/paddingDates';
import { numberWithCommas } from '../../../../helpers/number';
import DrawerContext from '../../../../helpers/context/ctxDrawer';
import ChartColumns from '../../../Charts/ChartColumns/ChartColumns';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleRange: PropTypes.object,
    moduleColor: PropTypes.string,
    moduleServices: PropTypes.string,
    intervalType: PropTypes.string,
    changeInterval: PropTypes.func
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleRange: {},
    moduleServices: {},
    moduleColor: '#666',
    intervalType: 'month',
    changeInterval: () => ({})
};

class SentimentHistogram extends Component {
    constructor(props) {
        super(props);
        this.handleClickEvent = this.handleClickEvent.bind(this);
        this.handleCloseDrawer = this.handleCloseDrawer.bind(this);
        this.initialState = {
            openDrawer: false,
            drawerData: {
                type: 'TWITTER',
                service: '',
                title: '',
                subtitleLeft: '',
                subtitleRight: '',
                queryData: {}
            }
        };
        this.state = this.initialState;
    }

    handleCloseDrawer() {
        this.setState({ ...this.initialState });
    }

    handleClickEvent(data) {
        const {
            intervalType, moduleRange, moduleConfig, moduleServices
        } = this.props;
        if (!moduleConfig.hasClick) return;
        const { title = 'TWEETS EN EL TIEMPO' } = moduleConfig;
        let startDate;
        let finishDate;
        let subtitleLeft;
        if (data.activeLabel) {
            startDate = moment(data.activeLabel).isBefore(moduleRange.startDate) ?
                moment(moduleRange.startDate).format() :
                moment(data.activeLabel).format();
            finishDate = moment(data.activeLabel).add(1, intervalType).isAfter(moduleRange.endDate) ?
                moment(moduleRange.endDate).format() :
                moment(data.activeLabel).add(1, intervalType).subtract(1, 'ms').format();
            subtitleLeft = `${labelType(startDate, finishDate, intervalType)}`;
        } else {
            startDate = moment(moduleRange.startDate).format();
            finishDate = moment(moduleRange.endDate).format();
            subtitleLeft = `${labelType(startDate, finishDate, intervalType)}`;
        }
        const queryData = {
            'recipe-id': 'module_tw_explore_tweets',
            'rule-id': moduleConfig.search_id,
            'initial-date': startDate,
            'final-date': finishDate,
            filters: {
                limit: 10,
            }
        };
        this.setState({
            openDrawer: true,
            drawerData: {
                type: 'TWITTER',
                service: moduleServices.twitter,
                title,
                subtitleLeft,
                subtitleRight: '',
                queryData
            }
        });
    }

    handleClickSentiment(sentiment) {
        const {
            moduleRange, moduleConfig, moduleServices
        } = this.props;
        if (!moduleConfig.hasClick) return;
        const { title = 'TWEETS EN EL TIEMPO' } = moduleConfig;
        const finalWord = sentiment === 'neutral' ? 'es' : 's';
        const queryData = {
            'recipe-id': 'module_tw_explore_tweets',
            'rule-id': moduleConfig.search_id,
            'initial-date': moment(moduleRange.startDate).format(),
            'final-date': moment(moduleRange.endDate).format(),
            filters: {
                limit: 10,
                sentiment
            }
        };
        this.setState({
            openDrawer: true,
            drawerData: {
                type: 'TWITTER',
                service: moduleServices.twitter,
                title,
                subtitleLeft: `Tweets ${sentiment}${finalWord}`,
                subtitleRight: '',
                queryData
            }
        });
    }

    render() {
        const {
            moduleConfig, moduleData,
            moduleColor, moduleRange,
            intervalType, changeInterval
        } = this.props;
        const { title = 'SENTIMIENTO EN EL TIEMPO' } = moduleConfig;
        const dataKeys = [
            {
                key: 'positivo',
                color: '#89cf41'
            },
            {
                key: 'neutral',
                color: '#95bbd4'
            },
            {
                key: 'negativo',
                color: '#e18080'
            }
        ];
        const nameArray = ['Positivo', 'Neutral', 'Negativo'];
        const labels = moduleConfig.labelTitles || { x: 'Periodo', y: 'Volumen de tweets' };
        
        return (
            <div className="sentiment-histogram-chart" {...styles(moduleColor)}>
                <div className="sentiment-histogram__label">
                    <h3>{title}</h3>
                </div>
                {
                    this.props.selectInterval ? (
                        <Intervals
                            dateRange={moduleRange}
                            intervalType={intervalType}
                            handleChangeInterval={changeInterval}
                        />
                    ) : null
                }
                {
                    (moduleData.total === 0)
                        ? (
                            <div className="noData">
                                <NoDataModule color={moduleColor} />
                            </div>
                        )
                        : (
                            <Fragment>
                                <DrawerContext.Provider
                                    value={{
                                        open: this.state.openDrawer,
                                        drawerData: this.state.drawerData,
                                        onCloseDrawer: this.handleCloseDrawer
                                    }}
                                >
                                    <div className="sentiment-histogram__chart">
                                        <ChartColumns
                                            labels={labels}
                                            dataKeys={dataKeys}
                                            nameArray={nameArray}
                                            data={moduleData.values}
                                            intervalType={intervalType}
                                            onBarClick={this.handleClickEvent}
                                        />
                                    </div>
                                </DrawerContext.Provider>
                                <div className="sentiment-histogram__totals">
                                    <div // eslint-disable-line
                                        onClick={() => this.handleClickSentiment('positivo')}
                                    >
                                        <h5 style={{ color: dataKeys[0].color }}>
                                            {numberWithCommas(moduleData.total_positivo)}
                                        </h5>
                                        <h6>{moduleConfig.totalTitles ? moduleConfig.totalTitles.pos : 'Total de tweets positivos'}</h6>
                                    </div>
                                    <div // eslint-disable-line
                                        onClick={() => this.handleClickSentiment('neutral')}
                                    >
                                        <h5 style={{ color: dataKeys[1].color }}>
                                            {numberWithCommas(moduleData.total_neutral)}
                                        </h5>
                                        <h6>{moduleConfig.totalTitles ? moduleConfig.totalTitles.ne : 'Total de tweets positivos'}</h6>
                                    </div>
                                    <div // eslint-disable-line
                                        onClick={() => this.handleClickSentiment('negativo')}
                                    >
                                        <h5 style={{ color: dataKeys[2].color }}>
                                            {numberWithCommas(moduleData.total_negativo)}
                                        </h5>
                                        <h6>{moduleConfig.totalTitles ? moduleConfig.totalTitles.neg : 'Total de tweets positivos'}</h6>
                                    </div>
                                </div>
                            </Fragment>
                        )
                }
            </div>
        );
    }
}

SentimentHistogram.propTypes = propTypes;
SentimentHistogram.defaultProps = defaultProps;

export default SentimentHistogram;
