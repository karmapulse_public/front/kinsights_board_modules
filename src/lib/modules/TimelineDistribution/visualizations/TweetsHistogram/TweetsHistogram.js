import React, { Component, Fragment } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';

import styles from './styles';
import Lines from '../../../Charts/ChartsLines';
import NoDataModule from '../../../NoDataModule';
import Intervals from '../../../Charts/Intervals';
import { labelType } from '../../../../helpers/paddingDates';
import { numberWithCommas } from '../../../../helpers/number';
import withDrawer from '../../../withDrawer';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleRange: PropTypes.object,
    moduleColor: PropTypes.string,
    moduleServices: PropTypes.object,
    intervalType: PropTypes.string,
    changeInterval: PropTypes.func
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleRange: {},
    moduleServices: {},
    moduleColor: '#666',
    intervalType: 'month',
    changeInterval: () => ({})
};

const labelInterval = {
    month: 'mes',
    week: 'semana',
    day: 'día',
    hour: 'hora'
};

class TweetsHistogram extends Component {
    constructor(props) {
        super(props);
        this.handleClickEvent = this.handleClickEvent.bind(this);
        this.handleCloseDrawer = this.handleCloseDrawer.bind(this);
        this.refExport = React.createRef(null);
        this.initialState = {
            openDrawer: false,
            drawerData: {
                type: 'TWITTER',
                service: '',
                title: '',
                subtitleLeft: '',
                subtitleRight: '',
                queryData: {}
            }
        };
        this.state = this.initialState;
    }

    handleCloseDrawer() {
        this.setState({ ...this.initialState });
    }

    handleClickEvent(data) {
        const {
            intervalType, moduleRange, moduleConfig, onClick
        } = this.props;
        const { title = 'TWEETS EN EL TIEMPO' } = moduleConfig;

        let startDate;
        let finishDate;
        let subtitleLeft;
        if (data.payload) {
            startDate = moment(data.payload.key).isBefore(moduleRange.startDate) ?
                moment(moduleRange.startDate).format() :
                moment(data.payload.key).format();
            finishDate = moment(data.payload.key).add(1, intervalType).isAfter(moduleRange.endDate) ?
                moment(moduleRange.endDate).format() :
                moment(data.payload.key).add(1, intervalType).subtract(1, 'ms').format();
            subtitleLeft = `${data.dataKey} del ${labelType(startDate, finishDate, intervalType)}`;
        } else {
            startDate = moment(moduleRange.startDate).format();
            finishDate = moment(moduleRange.endDate).format();
            subtitleLeft = `${data.dataKey}`;
        }
        onClick({
            'initial-date': startDate,
            'final-date': finishDate,
        }, {
            title,
            subtitleLeft,
            subtitleRight: '',
        });
    }

    render() {
        const {
            moduleConfig, moduleData,
            moduleColor, moduleRange,
            intervalType, changeInterval
        } = this.props;
        const { title = 'TWEETS EN EL TIEMPO', entity, lineColor, hasClick, buttonExport } = moduleConfig;
        const labels = { x: 'Periodo', y: 'Volumen de tweets' };
        return (
            <div
                ref={this.refExport}
                className="tweets-histogram-chart"
                {...styles(moduleColor)}
            >
                <div className="tweets-histogram__label">
                    <h3>{title}</h3>
                </div>
                {
                    // buttonExport(this.refExport, title, {
                    //     top: '30px'
                    // })
                }
                <Intervals
                    dateRange={moduleRange}
                    intervalType={intervalType}
                    handleChangeInterval={changeInterval}
                />
                {
                    (moduleData.total === 0)
                        ? (
                            <div className="noData">
                                <NoDataModule color={moduleColor} />
                            </div>
                        )
                        : (
                            <Fragment>
                                <div className="tweets-histogram__chart">
                                    <Lines
                                        type="single"
                                        entity={entity}
                                        labels={labels}
                                        colors={[lineColor]}
                                        data={moduleData.values}
                                        intervalType={intervalType}
                                        onLineClick={this.handleClickEvent}
                                        hasClick={hasClick}
                                    />
                                </div>
                                <div className="tweets-histogram__totals">
                                    <div>
                                        <h5>{numberWithCommas(moduleData.total)}</h5>
                                        <h6>Total de tweets</h6>
                                    </div>
                                    <div>
                                        <h5>{numberWithCommas(Math.ceil(moduleData.average))}</h5>
                                        <h6>Promedio de tweets por {labelInterval[intervalType]}</h6>
                                    </div>
                                    <div>
                                        <h5>{numberWithCommas(moduleData.maximum)}</h5>
                                        <h6>Máximo de tweets por {labelInterval[intervalType]}</h6>
                                    </div>
                                </div>
                            </Fragment>
                        )
                }
            </div>
        );
    }
}

TweetsHistogram.propTypes = propTypes;
TweetsHistogram.defaultProps = defaultProps;

export default withDrawer(TweetsHistogram);
