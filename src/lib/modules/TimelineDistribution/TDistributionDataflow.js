import { Observable } from 'rxjs';
import { fetchXHR } from '../../helpers/http';

export const fetchTweetsDist = (url, params) => (
    Observable.create((observer) => {
        Observable.fromPromise(fetchXHR(url, 'GET', params))
            .subscribe(
                (data) => {
                    const formatData = data.json.response.intervals.map(item => (
                        {
                            key: item.date,
                            [params.filters.entity]: item.total
                        }
                    ));
                    observer.next({
                        total: data.json.response.total,
                        average: data.json.response.average,
                        maximum: data.json.response.maximum,
                        values: {
                            aggregations: {
                                buckets: formatData
                            }
                        }
                    });
                },
                (error) => { observer.error(error); }
            );
    })
);

export const fetchSentimentDist = (url, params) => (
    Observable.create((observer) => {
        Observable.fromPromise(fetchXHR(url, 'GET', params))
            .subscribe(
                (data) => {
                    const formatData = data.json.response.intervals.map(item => (
                        {
                            date: item.date,
                            neutral: item.total_neutral,
                            positivo: item.total_positive,
                            negativo: item.total_negative,
                        }
                    ));
                    observer.next({
                        total: data.json.response.total,
                        total_positivo: data.json.response.total_positive,
                        total_neutral: data.json.response.total_neutral,
                        total_negativo: data.json.response.total_negative,
                        values: formatData
                    });
                },
                (error) => { observer.error(error); }
            );
    })
);
