import { css } from 'glamor';

const styles = color => css({
    ' .single-image__title': {
        ' h3': {
            textTransform: 'uppercase',
            color
        }
    }
});

export default styles;
