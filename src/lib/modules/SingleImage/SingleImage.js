import React from 'react';
import PropTypes from 'prop-types';

import styles from './SingleImageStyles';

const propTypes = {
    maxWidth: PropTypes.string,
    maxHeight: PropTypes.string,
    fields: PropTypes.object,
    color: PropTypes.string
};

const defaultProps = {
    maxWidth: 'none',
    maxHeight: 'none',
    fields: {},
    color: '#555'
};

const SingleImage = (props) => {
    const img = props.fields.image.fields;
    return (
        <div className="m-single-image" {...styles(props.color)}>
            <div className="single-image__title">
                <h3>{props.fields.title}</h3>
            </div>
            <img
                alt={img.title}
                src={img.file.url ? img.file.url.replace('http:', 'https:') : ''}
                style={{
                    maxWidth: props.maxWidth,
                    maxHeight: props.maxHeight
                }}
            />
        </div>
    );
};

SingleImage.propTypes = propTypes;
SingleImage.defaultProps = defaultProps;

export default SingleImage;
