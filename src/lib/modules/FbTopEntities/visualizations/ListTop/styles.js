import { css } from 'glamor';

const styles = color => css({
    position: 'relative',
    width: '100%',
    minWidth: '320px',
    maxWidth: '500px',
    ' .list-top': {

        ' &__label': {
            padding: '15px 20px 15px 20px',
            borderTop: '1px solid #dfdfdf',

            ' h3': {
                fontSize: '13px',
                fontWeight: 600,
                color,
            }
        },
        ' &__content': {
            position: 'relative',
            padding: '25px 40px 25px 40px',

            ' header': {
                display: 'flex',
                justifyContent: 'space-between',
                paddingBottom: 15,

                ' h4': {
                    fontSize: '13px',
                    textTransform: 'capitalize',
                    letterSpacing: '0.2px',
                    color: 'rgba(0,0,0,0.5)'
                }
            },

            ' & ul.table': {
                minHeight: 180,
                ' li': {
                    position: 'relative',
                    display: 'flex',
                    justifyContent: 'space-between',
                    padding: '10px 0px',

                    ' div': {
                        position: 'relative',
                        display: 'flex',
                        alignItems: 'center',

                        ' p': {
                            marginLeft: 15,
                            fontSize: '13px',
                            fontWeight: 600,
                            opacity: '0.5',
                        },

                        ' i': {
                            position: 'absolute',
                            width: 8,
                            height: 8,
                            borderRadius: '50%',

                            ' &.positivo': {
                                backgroundColor: '#89cf42'
                            },
                            ' &.neutral': {
                                backgroundColor: '#95bbd4'
                            },
                            ' &.negativo': {
                                backgroundColor: '#e18181'
                            },
                        }
                    },
                    ' > p': {
                        fontSize: '16px',
                        fontWeight: 500,
                        opacity: '1',
                    }
                }
            },

            ' footer': {
                ' & ul': {
                    display: 'flex',
                    justifyContent: 'center',

                    ' li': {
                        display: 'flex',
                        alignItems: 'center',
                        padding: '20px 10px 0 10px',

                        ' p': {
                            marginLeft: 15,
                            fontSize: '10px',
                            fontWeight: 600,
                        },

                        ' i': {
                            position: 'absolute',
                            width: 8,
                            height: 8,
                            borderRadius: '50%',

                            ' &.positivo': {
                                backgroundColor: '#89cf42'
                            },
                            ' &.neutral': {
                                backgroundColor: '#95bbd4'
                            },
                            ' &.negativo': {
                                backgroundColor: '#e18181'
                            },
                        }
                    },
                }
            },

            ' &.temas-negativos-en-comentarios, &.temas-positivos-en-comentarios': {
                padding: '25px 40px 25px 60px',

                ' &::before': {
                    position: 'absolute',
                    content: '""',
                    top: 27,
                    left: 40,
                    height: 'calc(100% - 55px)',
                    border: '1px dashed #979797'
                },

                ' &::after': {
                    position: 'absolute',
                    content: '""',
                    top: 27,
                    left: 36,
                    height: 10,
                    width: 10,
                    borderRadius: '50%',
                },

                ' & ul.table li > p': {
                    fontSize: '19px',
                },

                ' & ul.table li div p': {
                    marginLeft: 0
                },

                ' footer': {
                    display: 'none',
                }
            },

            ' &.temas-negativos-en-comentarios': {

                ' &::after': {
                    backgroundColor: '#e18181'
                },

                ' & ul.table li > p': {
                    color: '#e18181',
                    fontSize: '16px',
                }
            },

            ' &.temas-positivos-en-comentarios': {

                ' &::after': {
                    backgroundColor: '#89cf42'
                },

                ' & ul.table li > p': {
                    color: '#89cf42',
                    fontSize: '16px',
                }
            },
        }
    },
    ' .no-data-module': {
        height: 250,
        paddingTop: 0,
        justifyContent: 'center'
    }
});

export default styles;
