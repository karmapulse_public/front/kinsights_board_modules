import React from 'react';
import PropTypes from 'prop-types';
import truncate from 'lodash/truncate';
import { nFormatter } from '../../../../helpers/number';

const propTypes = {
    data: PropTypes.array,
    entity: PropTypes.string
};

const defaultProps = {
    data: [],
    entity: ''
};

const types = (entity) => {
    const type = {
        'hashtags en publicaciones': 'Hashtags',
        'temas en comentarios': 'Temas',
        'temas en publicaciones': 'Temas',
        'temas positivos en comentarios': 'Temas',
        'temas negativos en comentarios': 'Temas'
    };
    return type[entity];
};

const Table = ({ entity, data }) => (
    <div>
        <header>
            <h4>{types(entity)}</h4>
            <h4>Apariciones</h4>
        </header>
        <ul className="table">
            {
                data.map((i, index) => {
                    if (index < 5) {
                        return (
                            <li key={`${i.sentiment}-${index}`}>
                                <div>
                                    <i className={i.sentiment} />
                                    <p>
                                        <span>{index + 1}.</span>
                                        <span>
                                            {(types(entity) === 'Hashtags') ? ' #' : ' '}
                                            {truncate(i.value, { length: 50 })}
                                        </span>
                                    </p>
                                </div>
                                <p>{nFormatter(i.total)}</p>
                            </li>
                        );
                    }
                    return null;
                })
            }
        </ul>
        <footer>
            <ul>
                <li>
                    <i className="positivo" />
                    <p>Positivo</p>
                </li>
                <li>
                    <i className="neutral" />
                    <p>Neutral</p>
                </li>
                <li>
                    <i className="negativo" />
                    <p>Negativo</p>
                </li>
            </ul>
        </footer>
    </div>
);

Table.propTypes = propTypes;
Table.defaultProps = defaultProps;

export default Table;
