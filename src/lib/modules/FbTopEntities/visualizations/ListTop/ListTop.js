import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import Table from './Table';
import NoDataModule from '../../../NoDataModule';

import styles from './styles';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    noDataYet: PropTypes.func
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    noDataYet: null
};

const ListTop = ({ moduleData, moduleConfig, moduleColor, noDataYet }) => {
    const refExport = useRef(null);
    return (
        <article ref={refExport} className="list-top" {...styles(moduleColor)} >
            <div className="list-top__label">
                <h3>{moduleConfig.title}</h3>
            </div>
            {
                //moduleConfig.buttonExport(refExport, moduleConfig.title)
            }
            {
                (() => {
                    if (noDataYet !== null) {
                        return noDataYet();
                    }
                    return (moduleData.data.length > 0)
                        ? (
                            <div className={`list-top__content ${moduleConfig.entity_type.replace(/ /gi, '-')}`}>
                                <Table
                                    entity={moduleConfig.entity_type}
                                    data={moduleData.data}
                                />
                            </div>
                        )
                        : <NoDataModule color={moduleColor} />;
                })()
            }
        </article>
    );
};

ListTop.propTypes = propTypes;
ListTop.defaultProps = defaultProps;

export default ListTop;
