import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import fetchFbTopEntities from './FbTopEntitiesDataFlow';

import LoadingModule from '../LoadingModule';
import NoDataYet from '../NoDataYet';
import ListTop from './visualizations/ListTop';

const propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    dateRange: PropTypes.object,
    services: PropTypes.object,
    showNoDataYet: PropTypes.bool
};

const defaultProps = {
    fields: {},
    side: 'left',
    color: '#444',
    dateRange: {},
    services: {},
    showNoDataYet: false
};

class FbTopEntities extends Component {
    constructor(props) {
        super(props);

        this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color
        };
    }

    componentDidMount() {
        this.updateDataFlow(this.props);
    }

    componentWillReceiveProps(nextProps) {
        this.updateDataFlow(nextProps);
    }

    updateDataFlow(props) {
        const params = {
            rule_id: props.fields.search_id,
            initial_date: props.dateRange.startDate.toISOString(),
            final_date: props.dateRange.endDate.toISOString(),
            module_id: (() => {
                const types = {
                    'hashtags en publicaciones': 'module_fb_top_hashtags_posts',
                    'temas en comentarios': 'module_fb_top_themes_comments',
                    'temas en publicaciones': 'module_fb_top_themes_posts',
                    'temas positivos en comentarios': 'module_fb_top_positive_themes_comments',
                    'temas negativos en comentarios': 'module_fb_top_negative_themes_comments'
                };
                return types[props.fields.entity_type];
            })()
        };

        fetchFbTopEntities(props.services.facebook, params)
            .subscribe(
                (data) => {
                    this.setState({
                        moduleConfig: props.fields,
                        moduleData: data,
                        moduleLoading: false,
                        moduleSide: props.side,
                        moduleColor: props.color
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    renderByViz() {
        const { moduleConfig, moduleData } = this.state;
        const { dateRange: { startDate, endDate }, showNoDataYet } = this.props;
        let renderNoDataYet = null;

        if (showNoDataYet && moment().diff(endDate, 'minutes') <= 0 && endDate.diff(startDate, 'days') <= 1) {
            renderNoDataYet = () => <NoDataYet />;
        }

        if (moduleData) {
            const visualizations = {
                list_top: params => (
                    <ListTop {...params} noDataYet={renderNoDataYet} />
                )
            };

            return visualizations[moduleConfig.visualization](this.state);
        }

        return (<LoadingModule />);
    }

    render() {
        return (
            <div className="m-fb-top-entities">
                {this.renderByViz()}
            </div>
        );
    }
}

FbTopEntities.propTypes = propTypes;
FbTopEntities.defaultProps = defaultProps;

export default FbTopEntities;
