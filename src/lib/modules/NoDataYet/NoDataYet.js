import React from 'react';
import NoDataYetIcon from '../../helpers/icons/noDataYet';

const NoDataYet = () => (
    <div className="noDataYet">
        <div className="icon">
            <NoDataYetIcon />
        </div>
        <h5>
            Los datos podrán ser mostrados hasta las 12:00am
        </h5>
    </div>
);

export default NoDataYet;
