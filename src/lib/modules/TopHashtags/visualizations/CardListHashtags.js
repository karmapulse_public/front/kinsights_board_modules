import React from 'react';
import PropTypes from 'prop-types';
import { css } from 'glamor';

import styles from './cardListHashtagsStyles';
import { numberWithCommas } from '../../../helpers/number';

const colors = ['#c95885', '#5ba85f', '#9d64c6', '#ab973d', '#6493cd'];

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.array,
    moduleSide: PropTypes.string,
    moduleColor: PropTypes.string
};

const defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleSide: 'left',
    moduleColor: '#666'
};

const CardListHashtags = ({ moduleConfig, moduleData, moduleSide, moduleColor }) => {
    const showBars = moduleConfig.show_chart_bars;

    const renderBars = (index) => {
        const customStyles = css({
            width: `${100 - (index * 15)}%`,
            height: 5,
            marginTop: 10,
            backgroundColor: colors[index]
        });
        if (showBars) {
            return <div {...customStyles} className="hashtags__bar" />;
        }

        return '';
    };

    const { title = 'HASHTAG MÁS USADO' } = moduleConfig;

    return (
        <div className="card_list_hashtags" {...styles(moduleSide, moduleColor)}>
            <div className="hashtags__label">
                <h3>{title}</h3>
            </div>
            <ul className="hashtags__list">
                {
                    moduleData.map(
                        (hashtag, index) => (
                            <li key={index}>
                                <span>{`${index + 1}.`}</span>
                                <h4>#{hashtag.label}</h4>
                                <h5>Menciones: {numberWithCommas(hashtag.total)}</h5>
                                {renderBars(index)}
                            </li>
                        )
                    )
                }
            </ul>
        </div>
    );
};

CardListHashtags.propTypes = propTypes;
CardListHashtags.defaultProps = defaultProps;

export default CardListHashtags;
