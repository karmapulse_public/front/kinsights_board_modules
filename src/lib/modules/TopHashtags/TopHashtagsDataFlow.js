import { Observable } from 'rxjs';
import { fetchXHR } from '../../helpers/http';

export default (url, params) => (
    Observable.create((observer) => {
        Observable.fromPromise(fetchXHR(url, 'GET', params))
            .subscribe(
            (data) => {
                const sentiment = Object.assign(
                    {},
                    data.json.response.aggregations.entity.buckets.map(item => ({
                        label: item.key,
                        total: item.doc_count,
                    }))
                );
                observer.next(Object.values(sentiment));
            },
            (error) => { observer.error(error); }
            );
    })
);
