import React, { Component } from 'react';
import PropTypes from 'prop-types';
import clone from 'lodash/clone';
import moment from 'moment-timezone';

import fetchTopHashtags from './TopHashtagsDataFlow';

import LoadingModule from '../LoadingModule';
import CardListHashtags from './visualizations/CardListHashtags';

const propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    dateRange: PropTypes.object,
    services: PropTypes.object
};

const defaultProps = {
    fields: {},
    side: 'left',
    color: '#444',
    dateRange: {},
    services: {}
};

class TopHashtags extends Component {
    constructor(props) {
        super(props);

        this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color
        };
    }

    componentDidMount() {
        let { startDate, endDate } = this.props.dateRange;

        const tz = 'America/Mexico_City';
        const strStartDate = clone(startDate).format('YYYY-MM-DD hh:mm:ss');
        const strEndDate = clone(endDate).format('YYYY-MM-DD hh:mm:ss');

        const utcOffsetStart = moment.tz(strStartDate, tz).isDST() ? '-05:00' : '-06:00';
        const utcOffsetEnd = moment.tz(strEndDate, tz).isDST() ? '-05:00' : '-06:00';

        startDate = startDate.utcOffset(utcOffsetStart).set({
            hour: 0,
            minute: 0,
            second: 0,
            millisecond: 0
        }).toISOString();

        endDate = endDate.utcOffset(utcOffsetEnd).set({
            minute: 5 * (Math.floor(moment().minute() / 5)),
            second: 0,
            millisecond: 0
        }).toISOString();

        const params = {
            recipe_id: 'module_tw_list_trends',
            rule_id: this.state.moduleConfig.search_id,
            initial_date: startDate,
            final_date: endDate,
            filters: {
                entity: 'hashtags',
                query: 'top'
            }
        };

        fetchTopHashtags(this.props.services.twitter, params)
            .subscribe(
                (data) => {
                    this.setState({
                        moduleData: data,
                        moduleLoading: false
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    componentWillReceiveProps(nextProps) {
        let { startDate, endDate } = nextProps.dateRange;

        const tz = 'America/Mexico_City';
        const strStartDate = clone(startDate).format('YYYY-MM-DD hh:mm:ss');
        const strEndDate = clone(endDate).format('YYYY-MM-DD hh:mm:ss');

        const utcOffsetStart = moment.tz(strStartDate, tz).isDST() ? '-05:00' : '-06:00';
        const utcOffsetEnd = moment.tz(strEndDate, tz).isDST() ? '-05:00' : '-06:00';

        startDate = startDate.utcOffset(utcOffsetStart).set({
            hour: 0,
            minute: 0,
            second: 0,
            millisecond: 0
        }).toISOString();

        endDate = endDate.utcOffset(utcOffsetEnd).set({
            hour: 23,
            minute: 59,
            second: 59,
            millisecond: 999
        }).toISOString();

        const params = {
            search_id: nextProps.fields.search_id,
            dateRange: {
                startDate,
                endDate
            }
        };

        fetchTopHashtags(nextProps.services.twitter, params)
            .subscribe(
                (data) => {
                    this.setState({
                        moduleConfig: nextProps.fields,
                        moduleData: data,
                        moduleLoading: false,
                        moduleSide: nextProps.side,
                        moduleColor: nextProps.color
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    renderByViz() {
        const { moduleConfig, moduleData } = this.state;

        if (moduleData) {
            const visualizations = {
                card_list_hashtags: params => (
                    <CardListHashtags {...params} />
                )
            };

            return visualizations[this.props.view || moduleConfig.visualization](this.state);
        }

        return (<LoadingModule />);
    }

    render() {
        return (
            <div className="m-hashtags">
                {this.renderByViz()}
            </div>
        );
    }
}

TopHashtags.propTypes = propTypes;
TopHashtags.defaultProps = defaultProps;

export default TopHashtags
