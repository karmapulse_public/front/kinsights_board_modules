import React, { Component } from 'react';
import PropTypes from 'prop-types';
import clone from 'lodash/clone';
import moment from 'moment-timezone';

import fetchTopUsers from './TopUsersDataFlow';

import LoadingModule from '../LoadingModule';
import List from './visualizations/List';

const propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    dateRange: PropTypes.object,
    services: PropTypes.object
};

const defaultProps = {
    fields: {},
    side: 'left',
    color: '#444',
    dateRange: {},
    services: {}
};

class TopUsers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            moduleConfig: props.fields,
            moduleServices: props.services,
            moduleRange: props.dateRange,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
        };
    }

    componentDidMount() {
        let { startDate, endDate } = this.props.dateRange;

        const tz = 'America/Mexico_City';
        const strStartDate = clone(startDate).format('YYYY-MM-DD hh:mm:ss');
        const strEndDate = clone(endDate).format('YYYY-MM-DD hh:mm:ss');

        const utcOffsetStart = moment.tz(strStartDate, tz).isDST() ? '-05:00' : '-06:00';
        const utcOffsetEnd = moment.tz(strEndDate, tz).isDST() ? '-05:00' : '-06:00';

        startDate = startDate.utcOffset(utcOffsetStart).set({
            hour: 0,
            minute: 0,
            second: 0,
            millisecond: 0
        }).toISOString();

        endDate = endDate.utcOffset(utcOffsetEnd).set({
            minute: 5 * (Math.floor(moment().minute() / 5)),
            second: 0,
            millisecond: 0
        }).toISOString();

        const params = {
            recipe_id: 'module_tw_top_mentions',
            rule_id: this.state.moduleConfig.search_id,
            initial_date: startDate,
            final_date: endDate,
            filters: {
                query: 'top'
            }
        };

        fetchTopUsers(this.props.services.twitter, params)
            .subscribe(
                (data) => {
                    this.setState({
                        moduleData: data,
                        moduleLoading: false
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    componentWillReceiveProps(nextProps) {
        let { startDate, endDate } = nextProps.dateRange;

        const tz = 'America/Mexico_City';
        const strStartDate = clone(startDate).format('YYYY-MM-DD hh:mm:ss');
        const strEndDate = clone(endDate).format('YYYY-MM-DD hh:mm:ss');

        const utcOffsetStart = moment.tz(strStartDate, tz).isDST() ? '-05:00' : '-06:00';
        const utcOffsetEnd = moment.tz(strEndDate, tz).isDST() ? '-05:00' : '-06:00';

        startDate = startDate.utcOffset(utcOffsetStart).set({
            hour: 0,
            minute: 0,
            second: 0,
            millisecond: 0
        }).toISOString();

        endDate = endDate.utcOffset(utcOffsetEnd).set({
            hour: 23,
            minute: 59,
            second: 59,
            millisecond: 999
        }).toISOString();

        const params = {
            recipe_id: 'module_tw_top_mentions',
            rule_id: this.state.moduleConfig.search_id,
            initial_date: startDate,
            final_date: endDate,
            filters: {
                query: 'top'
            }
        };

        fetchTopUsers(nextProps.services.twitter, params)
            .subscribe(
                (data) => {
                    this.setState({
                        moduleConfig: nextProps.fields,
                        moduleData: data,
                        moduleLoading: false,
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    renderByViz() {
        const { moduleConfig, moduleData } = this.state;

        if (moduleData) {
            const visualizations = {
                list: params => (
                    <List {...params} />
                )
            };

            return visualizations[this.props.view || moduleConfig.visualization](this.state);
        }

        return (<LoadingModule />);
    }

    render() {
        return (
            <div className="m-top-users">
                {this.renderByViz()}
            </div>
        );
    }
}

TopUsers.propTypes = propTypes;
TopUsers.defaultProps = defaultProps;

export default TopUsers;
