import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import NoDataModule from '../../NoDataModule';

import styles from './ListStyles';
import withDrawer from './withDrawer';
import { numberWithCommas } from '../../../helpers/number';

const propTypes = {
    moduleData: PropTypes.array,
    moduleSide: PropTypes.string,
    moduleColor: PropTypes.string,
    moduleRange: PropTypes.object,
    moduleConfig: PropTypes.object,
    moduleServices: PropTypes.object,
    onClickUser: PropTypes.func
};

const defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleSide: 'left',
    moduleColor: '#666',
    moduleServices: {},
    moduleRange: {},
    onClickUser: () => ({})
};

const ListUsers = ({
    moduleConfig, moduleData, moduleSide, moduleColor, onClickUser
}) => {
    const { title = 'HASHTAG MÁS USADO' } = moduleConfig;
    return (
        <div className="list" {...styles(moduleSide, moduleColor)}>
            <div className="top-users__label">
                <h3>{title}</h3>
            </div>
            <ul className="top-users__list">
                <h4><span>Usuarios</span><span>Menciones</span></h4>
                {
                    (!isEmpty(moduleData))
                        ? (
                            moduleData.map((user, index) => (
                                <li key={index}>
                                    <h5 // eslint-disable-line
                                        onClick={() => onClickUser(user.mention)}
                                    >
                                        <span>{`${index + 1}.`}</span>
                                        <span>@{user.mention}</span>
                                    </h5>
                                    <h6>{numberWithCommas(user.total)}</h6>
                                </li>
                            ))
                        )
                        : (
                            <div className="noData">
                                <NoDataModule color={moduleColor} />
                            </div>
                        )
                }
            </ul>
        </div>
    );
};

ListUsers.propTypes = propTypes;
ListUsers.defaultProps = defaultProps;

export default withDrawer(ListUsers);
