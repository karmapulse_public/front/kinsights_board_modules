import React, { Component } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';

import DrawerFeed from '../../DrawerFeed';
import DrawerContext from '../../../helpers/context/ctxDrawer';

export default (WrappedComponent) => {
    class HOCDrawerListUsers extends Component {
        constructor(props) {
            super(props);
            this.handleClickEvent = this.handleClickEvent.bind(this);
            this.handleCloseDrawer = this.handleCloseDrawer.bind(this);
            this.initialState = {
                openDrawer: false,
                drawerData: {
                    type: 'TWITTER',
                    service: '',
                    title: '',
                    subtitleLeft: '',
                    subtitleRight: '',
                    queryData: {}
                }
            };
            this.state = this.initialState;
        }

        handleCloseDrawer() {
            this.setState({ ...this.initialState });
        }

        handleClickEvent(mention) {
            const {
                moduleRange, moduleConfig, moduleServices
            } = this.props;
            const { title = 'TWEETS EN EL TIEMPO' } = moduleConfig;
            const queryData = {
                'recipe-id': 'module_tw_explore_tweets',
                'rule-id': moduleConfig.search_id,
                'initial-date': moment(moduleRange.startDate).format(),
                'final-date': moment(moduleRange.endDate).format(),
                filters: {
                    limit: 10,
                    mention
                }
            };
            this.setState({
                openDrawer: true,
                drawerData: {
                    type: 'TWITTER',
                    service: moduleServices.twitter,
                    title,
                    subtitleLeft: `Tweets mencionando a @${mention}`,
                    subtitleRight: '',
                    queryData
                }
            });
        }

        render() {
            return (
                <DrawerContext.Provider
                    value={{
                        open: this.state.openDrawer,
                        drawerData: this.state.drawerData,
                        onCloseDrawer: this.handleCloseDrawer
                    }}
                >
                    <WrappedComponent
                        {...this.props}
                        onClickUser={this.handleClickEvent}
                    />
                    <DrawerFeed />
                </DrawerContext.Provider>
            );
        }
    }

    HOCDrawerListUsers.propTypes = {
        moduleColor: PropTypes.string,
        intervalType: PropTypes.string,
        moduleData: PropTypes.shape({}),
        moduleRange: PropTypes.shape({}),
        moduleConfig: PropTypes.shape({}),
        moduleServices: PropTypes.shape({})
    };

    HOCDrawerListUsers.defaultProps = {
        moduleConfig: {},
        moduleData: {},
        moduleRange: {},
        moduleServices: {},
        moduleColor: '#666',
        intervalType: 'month'
    };

    return HOCDrawerListUsers;
};
