import { css } from 'glamor';

const styles = (side, color) => {
    const bgColor = 'transparent';

    return css({
        minWidth: 300,
        height: '100%',
        backgroundColor: bgColor,
        color: 'rgba(0,0,0, 0.5)',
        ' h4': {
            display: 'flex',
            width: '100%',
            justifyContent: 'space-between',
            alignItems: 'center'
        },
        ' ul': {
            padding: 0,
            margin: 0,
        },
        ' li': {
            display: 'flex',
            justifyContent: 'space-between'
        },
        ' .noData': {
            paddingTop: '40px',
            width: '100%',
            height: 250,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
        },

    });
};

export default styles;
