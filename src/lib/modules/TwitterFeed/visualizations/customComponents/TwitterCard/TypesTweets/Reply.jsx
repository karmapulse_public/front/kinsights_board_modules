import React from 'react';
import PropTypes from 'prop-types';
import { injectIntl } from 'react-intl';
import isEqual from 'lodash/isEqual';
import isUndefined from 'lodash/isUndefined';
import iconsExploration from 'helpers/icons/dataExploration';

const propTypes = {
    childTweet: PropTypes.object,
    type: PropTypes.string,
    intl: PropTypes.object, //eslint-disable-line
};

const defaultProps = {
    childTweet: {},
    type: ''
};
const Reply = (props) => {
    if (isEqual(props.type, 'reply')) {
        let users;
        if (isUndefined(props.childTweet.mentions)) {
            const regex = /^http?:\/\/(www\.)?twitter\.com\/(#!\/)?([^/]+)(\/\w+)*$/;
            const match = regex.exec(props.childTweet.link);

            users = () => (
                <p>
                    {props.intl.formatMessage({ id: 'DataExploration.TextResponseToReply' })}
                    <a href={`http://www.twitter.com/${match[3]}`}>@{match[3]}</a>
                </p>
            );
        } else if (props.childTweet.mentions.length > 1) {
            users = () => (
                <p>
                    {props.intl.formatMessage({ id: 'DataExploration.TextResponseToReply' })}
                    <a href={`http://www.twitter.com/${props.childTweet.mentions[0]}`}>
                        @{props.childTweet.mentions[0]}
                    </a>{props.intl.formatMessage({ id: 'DataExploration.TextAndToReply' })} {props.childTweet.mentions.length - 1 } {props.intl.formatMessage({ id: 'DataExploration.TextMoreReply' })}
                </p>
            );
        } else {
            users = () => (
                <p>
                    {props.intl.formatMessage({ id: 'DataExploration.TextResponseToReply' })}
                    <a href={`http://www.twitter.com/${props.childTweet.mentions[0]}`}>
                        @{props.childTweet.mentions[0]}
                    </a>
                </p>
            );
        }
        return (
            <div className="twitter-card__reply">
                {iconsExploration.replyIcon()}
                {users()}
            </div>
        );
    }
    return <div />;
};

Reply.propTypes = propTypes;
Reply.defaultProps = defaultProps;

export default injectIntl(Reply);
