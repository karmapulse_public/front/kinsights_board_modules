import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import { Observable } from 'rxjs';

import TwitterCardMobile from './customComponents/TwitterCardMobile';
import Gallery from './customComponents/TwitterGallery';
import styles from './FeedTwitterStyles';

const propTypes = {
    scroll: PropTypes.func,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    miniMode: PropTypes.bool
};

const defaultProps = {
    scroll: 'hey',
    moduleData: {},
    moduleColor: '#666',
    miniMode: false
};

class FeedTwitterMobile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tweets: [],
            scrollActive: true
        };
    }

    componentDidMount() {
        const feed = ReactDOM.findDOMNode(this);
        const feedTweets = feed.getElementsByClassName('feed-container-list')[0];
        const isUserScrollingDown = positions => (
            positions[0].sT < positions[1].sT
        );

        const isScrollExpectedPercent = (position, percent) =>
            ((position.sT + position.cH) / position.sH) > (percent / 100)
        ;

        Observable.fromEvent(feedTweets, 'scroll')
            .map(event => ({
                sH: event.target.scrollHeight,
                sT: event.target.scrollTop,
                cH: event.target.clientHeight
            }))
            .pairwise()
            .filter(positions => (
                isUserScrollingDown(positions) &&
                    isScrollExpectedPercent(positions[1], 95)
                    && this.state.scrollActive
            ))
            .debounceTime(100)
            .subscribe(
                () => {
                    this.props.scroll(
                        this.props.moduleData.page + 18
                    );

                    this.setState({
                        scrollActive: false
                    });
                },
                error => console.log(error),
                null
            );
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            scrollActive: nextProps.moduleData.scrollActive
        });
    }

    renderTweet(tweet, index) {
        return (
            <TwitterCardMobile {...tweet} key={index}>
                <Gallery items={tweet._source.images} />
            </TwitterCardMobile>
        );
    }

    render() {
        const { moduleData, moduleColor, miniMode } = this.props;
        const { tweets } = moduleData;
        return (
            <div className="feed-twitter-mobile" {...styles(moduleColor)}>
                <div className="feed-container-list">
                    {(() => {
                        if (miniMode) {
                            return this.renderTweet(tweets[0], 0);
                        }
                        return tweets.map(
                            (tweet, index) => (this.renderTweet(tweet, index))
                        );
                    })()}
                </div>
            </div>
        );
    }
}

FeedTwitterMobile.propTypes = propTypes;
FeedTwitterMobile.defaultProps = defaultProps;

export default FeedTwitterMobile;
