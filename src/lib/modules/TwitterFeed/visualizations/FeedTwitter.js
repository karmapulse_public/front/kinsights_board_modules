import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import { Observable } from 'rxjs';

import TwitterCard from './customComponents/TwitterCard';
import Gallery from './customComponents/TwitterGallery';
import styles from './FeedTwitterStyles';
import NoDataModule from '../../NoDataModule';
import TrumpetIcon from '../../../helpers/icons/trumpetIcon';

const propTypes = {
    scroll: PropTypes.func,
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    viewLabels: PropTypes.string,
    moduleColor: PropTypes.string,
    miniMode: PropTypes.bool
};

const defaultProps = {
    scroll: 'hey',
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    viewLabels: '',
    miniMode: false
};

class FeedTwitter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tweets: [],
            scrollActive: true
        };
    }

    componentDidMount() {
        const feed = ReactDOM.findDOMNode(this);
        const feedTweets = feed.getElementsByClassName('feed-container-list')[0];
        const isUserScrollingDown = positions => (
            positions[0].sT < positions[1].sT
        );

        const isScrollExpectedPercent = (position, percent) =>
            ((position.sT + position.cH) / position.sH) > (percent / 100)
        ;

        Observable.fromEvent(feedTweets, 'scroll')
            .map(event => ({
                sH: event.target.scrollHeight,
                sT: event.target.scrollTop,
                cH: event.target.clientHeight
            }))
            .pairwise()
            .filter(positions => (
                isUserScrollingDown(positions) &&
                    isScrollExpectedPercent(positions[1], 95)
                    && this.state.scrollActive
            ))
            .debounceTime(100)
            .subscribe(
                () => {
                    this.props.scroll(
                        this.props.moduleData.page + 10
                    );

                    this.setState({
                        scrollActive: false
                    });
                },
                error => console.log(error),
                null
            );
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            scrollActive: nextProps.moduleData.scrollActive
        });
    }

    renderTopBar(newTweets) {
        return (
            <button
                className="feed-new-data"
                onClick={
                    () => {
                        const feed = ReactDOM.findDOMNode(this);
                        const feedTweets = feed.getElementsByClassName('feed-container-list')[0];
                        feedTweets.scrollTop = 0;
                        this.props.dropNewTweets();
                    }}
            >
                {newTweets.length} nuevos tweets
            </button>
        );
    }

    renderTweet(tweet, index) {
        return (
            <TwitterCard {...tweet} key={index} viewLabels={this.props.viewLabels}>
                <Gallery items={tweet._source.images} />
            </TwitterCard>
        );
    }

    render() {
        const { moduleConfig, moduleData, moduleColor, miniMode, newTweets } = this.props;
        const { tweets } = moduleData;
        const { title = 'FEED DE TWEETS' } = moduleConfig;
        return (
            <div className="feed-twitter" {...styles(moduleColor)}>
                <div className="feed-label">
                    <h1>
                        <TrumpetIcon color="#FFF" />
                        {title}
                    </h1>
                    {newTweets.length ? (this.renderTopBar(newTweets)) : null}
                </div>
                <div className="feed-container-list">
                    {(() => {
                        if (!tweets.length) {
                            return <NoDataModule />;
                        }
                        if (miniMode) {
                            return this.renderTweet(tweets[0], 0);
                        }
                        return tweets.map(
                            (tweet, index) => (this.renderTweet(tweet, index))
                        );
                    })()}
                </div>
            </div>
        );
    }
}

FeedTwitter.propTypes = propTypes;
FeedTwitter.defaultProps = defaultProps;

export default FeedTwitter;
