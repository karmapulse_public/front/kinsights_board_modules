import React, { Component } from 'react';
import PropTypes from 'prop-types';
import takeRight from 'lodash/takeRight';
import findIndex from 'lodash/findIndex';
import isEqual from 'lodash/isEqual';
import isEmpty from 'lodash/isEmpty';

import setDates from '../../helpers/setDates';
import fetchTwitterFeed from './TwitterFeedDataFlow';

import LoadingModule from '../LoadingModule';
import FeedTwitter from './visualizations/FeedTwitter';

const propTypes = {
    side: PropTypes.string,
    color: PropTypes.string,
    fields: PropTypes.object,
    dateRange: PropTypes.object,
    services: PropTypes.object,
    miniMode: PropTypes.bool,
    viewLabels: PropTypes.string,
    filters: PropTypes.object,
    withCache: PropTypes.bool,
    type: PropTypes.string
};

const defaultProps = {
    side: 'left',
    color: '#666',
    fields: {},
    dateRange: {},
    services: {},
    viewLabels: '',
    miniMode: false,
    filters: {},
    withCache: true,
    type: 'search'
};

const spliceNewData = (newData, oldData) => {
    if (isEmpty(oldData)) return newData.length;
    const idLastOld = oldData[0]._source.id;
    const indexDiff = findIndex(newData, elto => (elto._source.id === idLastOld));
    if (indexDiff === -1) {
        return newData.length;
    }
    return indexDiff;
};
class TwitterFeed extends Component {
    constructor(props) {
        super(props);

        this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color,
            miniMode: props.miniMode,
            filters: props.filters,
            newTweets: []
        };

        this.scrollHandler = this.infiniteScrollHandler.bind(this);
        this.dropNewTweets = this.dropNewTweets.bind(this);
    }

    componentDidMount() {
        const { dateRange, withCache } = this.props;
        const params = {
            recipe_id: 'module_tw_feed',
            rule_id: this.state.moduleConfig.search_id,
            is_curated: false,
            ...setDates(dateRange, withCache),
            filters: { ...this.props.filters }
        };

        fetchTwitterFeed(this.props.services.twitter, params)
            .subscribe(
                data => this.setState({
                    moduleData: Object.assign({}, data, {
                        scrollActive: true,
                        page: 0
                    }),
                    moduleLoading: false
                }),
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    componentWillReceiveProps(nextProps) {
        const filters = nextProps.filters;
        const { moduleData, newTweets } = this.state;
        const { dateRange } = nextProps;

        const params = {
            recipe_id: 'module_tw_feed',
            rule_id: this.state.moduleConfig.search_id,
            is_curated: false,
            ...setDates(dateRange, this.props.withCache),
            filters: { ...nextProps.filters }
        };

        if (!isEqual(this.props.filters, nextProps.filters) || nextProps.type === 'search') {
            fetchTwitterFeed(this.props.services.twitter, params, 0)
                .subscribe(
                    (data) => {
                        this.setState({
                            filters,
                            moduleData: {
                                tweets: data.tweets,
                                page: 0,
                                scrollActive: true
                            },
                            moduleLoading: false,
                            newTweets: [],
                        });
                    },
                    (error) => {
                        console.log(error);
                    },
                    null
                );
        } else if (this.props.dateRange !== nextProps.dateRange) {
            fetchTwitterFeed(this.props.services.twitter, params, 0)
                .subscribe(
                    (data) => {
                        const oldTweets = moduleData ? moduleData.tweets : [];
                        const currentData = newTweets.length ? newTweets : oldTweets;
                        const indexDiff = spliceNewData(data.tweets, currentData);
                        this.setState({
                            newTweets: data.tweets.slice(0, indexDiff).concat(newTweets),
                        });
                    },
                    (error) => {
                        console.log(error);
                    },
                    null
                );
        } else {
            this.setState({
                moduleColor: nextProps.color
            });
        }
    }

    infiniteScrollHandler(page) {
        const lastTweet = takeRight(this.state.moduleData.tweets);
        const { dateRange, withCache } = this.props;

        const params = {
            recipe_id: 'module_tw_feed',
            rule_id: this.state.moduleConfig.search_id,
            is_curated: false,
            ...setDates(dateRange, withCache),
            search_after: lastTweet[0].sort,
            filters: { ...this.state.filters }
        };

        fetchTwitterFeed(this.props.services.twitter, params, page)
            .subscribe(
                (data) => {
                    const newTweets = this.state.moduleData.tweets
                        .concat(data.tweets);
                    this.setState({
                        moduleData: {
                            tweets: newTweets,
                            page,
                            scrollActive: true
                        },
                        moduleLoading: false
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    dropNewTweets() {
        this.setState({
            moduleData: {
                tweets: this.state.newTweets.concat(this.state.moduleData.tweets),
                page: 0,
                scrollActive: true
            },
            newTweets: [],
        });
    }

    renderByViz() {
        const { moduleConfig, moduleData } = this.state;

        if (moduleData) {
            const visualizations = {
                list_tweets: params => (
                    <FeedTwitter
                        {...params}
                        scroll={this.scrollHandler}
                        viewLabels={this.props.viewLabels}
                        dropNewTweets={this.dropNewTweets}
                    />
                )
            };
            return visualizations[moduleConfig.visualization](this.state);
        }

        return (<LoadingModule />);
    }

    render() {
        return (
            <div className="m-twitter-feed">
                {this.renderByViz()}
            </div>
        );
    }
}

TwitterFeed.propTypes = propTypes;
TwitterFeed.defaultProps = defaultProps;

export default TwitterFeed
