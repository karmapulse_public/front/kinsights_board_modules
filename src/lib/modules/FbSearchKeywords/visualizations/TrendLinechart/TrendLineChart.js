import React, { useRef } from 'react';
import PropTypes from 'prop-types';

import styles from './trendLineChartStyles';
import Lines from '../../../Charts/ChartsLines';
import NoDataModule from '../../../NoDataModule';

const colors = [
    { r: 199, g: 89, b: 147 },
    { r: 129, g: 117, b: 204 },
    { r: 204, g: 95, b: 66 },
    { r: 91, g: 169, b: 101 },
    { r: 84, g: 161, b: 211 }
];

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    noDataYet: PropTypes.func
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    noDataYet: null
};

const TrendLineChart = ({ moduleConfig, moduleData, moduleColor, noDataYet }) => {
    const { title = 'TEMAS EN EL TIEMPO', buttonExport } = moduleConfig;
    const refExport = useRef(null);
    return (
        <div
            ref={refExport}
            className="trend-line-chart"
            {...styles(moduleColor)}
        >
            <div className="trend__label">
                <h3>{title}</h3>
            </div>
            {
                (() => {
                    if (noDataYet !== null) {
                        return noDataYet();
                    }
                    return (moduleData.values.aggregations.keywords.buckets.reduce((prev, item) => (prev += item.doc_count), 0) === 0) ?
                        (
                            <div className="noData">
                                <NoDataModule color={moduleColor} />
                            </div>
                        )
                        : (
                            <div className="trend__chart">
                                {
                                    // buttonExport(refExport, title, {
                                    //     top: '-20px'
                                    // })
                                }
                                <Lines
                                    data={moduleData.values}
                                    type="multiple"
                                    colors={colors}
                                    entity="keywords"
                                />
                            </div>
                        );
                })()
            }
        </div>
    );
};

TrendLineChart.propTypes = propTypes;
TrendLineChart.defaultProps = defaultProps;

export default TrendLineChart;
