import { Observable } from 'rxjs';
import { requestPost } from '../../helpers/http';

export default (url, params) => (
    Observable.create((observer) => {
        Observable.fromPromise(requestPost(url, 'POST', params))
            .subscribe(
                (data) => {
                    const parsedData = {};
                    parsedData.total = data.json.total;
                    parsedData.values = {
                        aggregations: {
                            keywords: {
                                buckets: (() => {
                                    const arr = [];
                                    for (const key in data.json.data) {
                                        arr.push({
                                            key,
                                            doc_count: data.json.data[key].doc_count,
                                            keywords_by_time: {
                                                buckets: data.json.data[key].keywords_by_time.buckets
                                            }
                                        });
                                    }
                                    return arr;
                                })()
                            }
                        }
                    };
                    observer.next(parsedData);
                },
                (error) => {
                    observer.error(error);
                }
            );
    })
);
