import { Observable } from 'rxjs';
import { fetchXHR } from '../../helpers/http';
import paddingDates from '../../helpers/paddingDates';

export default (url, params) => (
    Observable.create((observer) => {
        Observable.fromPromise(fetchXHR(url, 'GET', params))
            .subscribe(
                (data) => {
                    if (data.json.response.total !== 0) {
                        observer.next({
                            intervals: paddingDates(data.json.response.intervals, params),
                            total: data.json.response.total
                        });
                    } else {
                        observer.next({
                            intervals: [],
                            total: 0
                        });
                    }
                },
                (error) => { observer.error(error); }
            );
    })
);
