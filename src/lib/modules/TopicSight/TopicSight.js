import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import 'moment-timezone';
import setDates from '../../helpers/setDates';
import getIntervalByDates from '../../helpers/getIntervalByDates';
import fetchTopicSight from './TopSightDataFlow';

import LoadingModule from '../LoadingModule';
import AreaItem from './visualizations/AreaItem';

const propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    dateRange: PropTypes.object,
    services: PropTypes.object,
    topic: PropTypes.object,
    extraFunction: PropTypes.func,
    goToURL: PropTypes.func,
};

const defaultProps = {
    fields: {},
    side: 'left',
    color: '#444',
    dateRange: {},
    services: {},
    extraFunction: () => ({}),
    topic: {},
    goToURL: () => ({}),
};

const getData = (dateRange, searchId, serviceRecipesTw, thenfunction) => {
    // const date = moment().format('YYYY-MM-DD hh:mm:ss');
    // const utcOffset = moment.tz(date, tz).isDST() ? '-05:00' : '-06:00';
    // const startDate = moment().subtract(1, 'days').utcOffset(utcOffset).toISOString();
    // const endDate = moment().utcOffset(utcOffset).toISOString();

    const params = {
        recipe_id: 'module_tw_histogram',
        rule_id: searchId,
        ...setDates(dateRange),
        filters: {
            interval: 'day'
        }
    };

    params.filters.interval = getIntervalByDates(moment(params.initial_date), moment(params.final_date));

    fetchTopicSight(serviceRecipesTw, params)
        .subscribe(
            (resultData) => {
                thenfunction(resultData);
            },
            (error) => {
                console.log(error);
            },
            null
        );
};

class TopicSight extends Component {
    constructor(props) {
        super(props);

        this.state = {
            moduleConfig: { ...props.fields, ...props.topic },
            moduleData: {},
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color,
        };
    }

    componentDidMount() {
        getData(
            this.props.dateRange,
            this.state.moduleConfig.search_id,
            this.props.services.twitter,
            (data) => {
                this.props.extraFunction(data);
                this.setState({
                    moduleData: data,
                    moduleLoading: false
                });
            }
        );
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.dateRange !== this.props.dateRange) {
            getData(
                nextProps.dateRange,
                this.state.moduleConfig.search_id,
                this.props.services.twitter,
                (data) => {
                    this.props.extraFunction(data);
                    this.setState({
                        moduleData: data,
                    });
                }
            );
        }
    }

    renderByViz() {
        const { moduleConfig, moduleData } = this.state;
        if (moduleData) {
            const visualizations = {
                area_chart: params => (
                    <AreaItem {...params} />
                )
            };
            const mergeProps = Object.assign({}, this.state, { goToURL: this.props.goToURL });
            return visualizations[this.props.view || moduleConfig.visualization](mergeProps);
        }

        return (<LoadingModule />);
    }

    render() {
        return (
            this.renderByViz()
        );
    }
}

TopicSight.propTypes = propTypes;
TopicSight.defaultProps = defaultProps;

export default TopicSight;
