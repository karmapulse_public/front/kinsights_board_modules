import React from 'react';
import PropTypes from 'prop-types';
import AreaChart from '../../Charts/ChartArea';
import { numberWithCommas } from '../../../helpers/number';
import RightArrowIcon from '../../../helpers/icons/seg/rightArrowIcon';
import styles from './AreaItemStyles';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleSide: PropTypes.string,
    moduleColor: PropTypes.string,
    goToURL: PropTypes.func
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleSide: 'left',
    moduleColor: '#666',
    goToURL: () => ({})
};

const AreaItem = ({ moduleConfig, moduleData, moduleSide, moduleColor, goToURL }) => {
    const { title, slug } = moduleConfig;
    const dataKeys = [{ key: 'total', color: moduleColor }];

    return (
        <div
            className="topicSight__topic"
            onClick={() => goToURL(slug.substring(1))}
            {...styles(moduleColor)}
        >
            <h1>
                {title}
                <RightArrowIcon />
            </h1>
            <div className="topicSight__topic__chart">
                <h2>{numberWithCommas(moduleData.total || 0)}</h2>
                <span>Menciones</span>
                <div>
                    <AreaChart
                        data={moduleData.intervals}
                        dataKeys={dataKeys}
                        hasAxis={false}
                        hasGrid={false}
                        hasLegend={false}
                    />
                </div>
            </div>
        </div>
    );
};

AreaItem.propTypes = propTypes;
AreaItem.defaultProps = defaultProps;

export default AreaItem;
