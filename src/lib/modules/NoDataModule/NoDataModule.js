import React from 'react';
import { css } from 'glamor';
import NoDataIcon from '../../helpers/icons/noDataIcon';

const styles = css({
    width: '100%',
    height: '100%',
    minHeight: 200,
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    paddingTop: 45,

    ' h1:last-child': {
        marginTop: 20,
        fontSize: 13,
        fontWeight: 600,
        letterSpacing: '1.1px',
        opacity: '0.5'
    }
});

const NoDataModule = ({ color }) => (
    <div className="no-data-module" {...styles} >
        <NoDataIcon color="rgba(0,0,0,0.5)" />
        <h1>NO HAY DATOS PARA MOSTRAR</h1>
    </div>
);

export default NoDataModule;
