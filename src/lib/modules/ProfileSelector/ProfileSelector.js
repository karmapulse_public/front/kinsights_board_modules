import React, { Component } from 'react';
import PropTypes from 'prop-types';

import profileSelectorStyles from './profileSelectorStyles';

const propTypes = {
    fields: PropTypes.object,
    columns: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    owner: PropTypes.bool,
    changeProfile: PropTypes.func,
};

const defaultProps = {
    fields: {},
    columns: {},
    side: '',
    color: '',
    owner: {},
    changeProfile: () => {}
};

class ProfileSelector extends Component {
    constructor(props) {
        super(props);

        let profiles = [];
        profiles.push(this.props.fields.owner);
        profiles = profiles.concat(this.props.fields.rivals);

        this.state = {
            dropDownStatus: false,
            profiles,
            columns: props.columns,
            side: props.side,
            color: props.color,
            //kloutscore: null
        };

        this.toggleDropDown = this.toggleDropDown.bind(this);
        this.changeProfile = this.changeProfile.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            columns: nextProps.columns,
            side: nextProps.side,
            color: nextProps.color
        });
        // }
    }

    changeProfile(index) {
        const params = {};

        if (this.props.owner) {
            params.columns = {
                left: index,
                right: this.state.columns.right
            };
        } else {
            params.columns = {
                left: this.state.columns.left,
                right: index
            };
        }

        this.props.changeProfile(params);

        this.setState({
            dropDownStatus: false
        });
    }

    toggleDropDown() {
        this.setState({
            dropDownStatus: !this.state.dropDownStatus
        });
    }

    renderDropDown() {
        const profiles = this.state.profiles.map((item, index) => {
            if (this.state.columns.left === index || this.state.columns.right === index) {
                return Object.assign(item, { used: true });
            }
            return Object.assign(item, { used: false });
        });

        if (this.state.dropDownStatus) {
            return profiles.map((item, index) => (
                <div
                    style={item.used ? { display: 'none' } : {}}
                    onClick={() => this.changeProfile(index)}
                    className="item"
                    key={index}
                >
                    <img src={item.fields.profile_picture.fields.file.url ? item.fields.profile_picture.fields.file.url.replace('http:', 'https:') : ''} alt="" />
                    <div className="item__info">
                        <h1>{item.fields.name}</h1>
                        <h2>@{item.fields.twitter_account}</h2>
                        <h3>{item.fields.description}</h3>
                    </div>
                </div>
            ));
        }

        return '';
    }

    // renderKloutscore(search_id) {
    //     if (!isUndefined(search_id)) {
    //         if (this.state.kloutscore !== null) {
    //             return (
    //                 <div className="profile-selector__ks">
    //                     <KsIconOrange />
    //                     <span>{this.state.kloutscore}</span>
    //                 </div>
    //             );
    //         }
    //         return (
    //             <div className="profile-selector__ks">
    //                 <KsIconOrange />
    //                 <span>{this.state.kloutscore}</span>
    //             </div>
    //         );
    //     }

    //     return null;
    // }

    render() {
        const {
            profile_picture,
            name,
            twitter_account,
            description
        } = this.state.profiles[this.state.columns[this.props.side]].fields;
        return (
            <div
                className="c-profile-selector"
                {...profileSelectorStyles(this.state.side, this.state.color)}
            >
                <img src={profile_picture.fields.file.url ? profile_picture.fields.file.url.replace('http:', 'https:') : ''} alt="" />
                <div className="profile-selector__names">
                    {/* this.renderKloutscore(search_id) */}
                    <div>
                        <h1>
                            <span>{name}</span>
                            <span>@{twitter_account}</span>
                        </h1>
                        <h2>{description}</h2>
                    </div>
                </div>
                <div
                    className="profile-selector__dropdown"
                    onClick={this.toggleDropDown}
                >
                    <div className="profile-selector__dropdown__triangle" />
                </div>
                <div className="drop-list">
                    {this.renderDropDown()}
                </div>
            </div>
        );
    }
}

ProfileSelector.propTypes = propTypes;
ProfileSelector.defaultProps = defaultProps;

export default ProfileSelector;
