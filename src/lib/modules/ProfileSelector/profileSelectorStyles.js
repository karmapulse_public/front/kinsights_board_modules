import { css } from 'glamor';

const styles = (side, color) => (
    css({
        width: '100%',
        height: 66,
        position: 'relative',
        paddingLeft: side === 'left' ? '66px' : '35px',
        paddingRight: side === 'left' ? '35px' : '66px',
        backgroundColor: color === undefined ? '#666' : color,

        '> img': {
            position: 'absolute',
            top: 0,
            left: side === 'left' ? 0 : 'auto',
            right: side === 'right' ? 0 : 'auto',
            width: 66,
            height: 66
        },

        ' .profile-selector__names': {
            height: '100%',
            display: 'flex',
            justifyContent: 'flex-start',
            alignItems: 'center',
            flexDirection: side === 'left' ? 'row' : 'row-reverse',

            '>div': {
                padding: '0px 10px',
                '> h1': {
                    fontSize: 12,
                    fontWeight: '300',
                    lineHeight: 1.3,
                    color: '#FFF',
                    textAlign: side,
                    '>span:first-child': {
                        display: 'block',
                        width: '100%',
                        height: 12,
                        overflow: 'hidden'
                    },
                    '>span:nth-child(2)': {
                        textAlign: side,
                        display: 'block'
                    }
                },

                '> h2': {
                    width: '100%',
                    height: 12,
                    overflow: 'hidden',
                    marginTop: 5,
                    fontSize: 11,
                    fontWeight: '300',
                    lineHeight: 1.3,
                    color: '#FFF',
                    textAlign: side
                }
            }
        },

        // ' .profile-selector__ks': {
        //     width: 30,
        //     height: 'inherit',
        //     padding: '0px !important',
        //     display: 'flex',
        //     flexDirection: 'column',
        //     justifyContent: 'center',
        //     alignItems: 'center',
        //     backgroundColor: '#e0e0e0',

        //     ' svg': {
        //         width: 18
        //     },
        //     ' span': {
        //         fontSize: 14,
        //         fontWeight: 700,
        //         color: '#d85624'
        //     }
        // },

        ' .profile-selector__dropdown': {
            position: 'absolute',
            top: 0,
            right: side === 'left' ? 0 : 'auto',
            left: side === 'right' ? 0 : 'auto',
            width: 35,
            height: 'inherit',
            backgroundColor: 'rgba(0,0,0,.2)',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            cursor: 'pointer',

            '&__triangle': {
                width: 5,
                height: 5,
                boxSizing: 'border-box',
                borderTop: '5px solid #fff',
                borderRight: '5px solid transparent',
                borderLeft: '5px solid transparent'
            }
        },

        ' .drop-list': {
            width: 290,
            maxHeight: 272,
            overflowY: 'auto',
            backgroundColor: '#f6f6f6',
            position: 'absolute',
            top: '100%',
            right: side === 'left' ? '0' : 'auto',
            left: side === 'right' ? '0' : 'auto',
            zIndex: '99 !important',
            boxShadow: '0 3px 4px 0 rgba(0, 0, 0, 0.3)',
            cursor: 'pointer',

            '@media screen and (min-width:1024px)': {
                width: 400,
                float: side
            },

            ' .item': {
                width: '100%',
                height: 60,
                display: 'flex',
                flexDirection: 'row',
                padding: 9,
                borderBottom: '1px solid #e9e9e9',

                ':hover': {
                    backgroundColor: '#e9e9e9'
                },

                ':last-child': {
                    borderBottom: 'none'
                },

                ' img': {
                    width: 38,
                    height: 38,
                    paddingRight: 10
                },

                '&__info': {

                    ' h1': {
                        color: '#313131',
                        fontSize: 12,
                        fontWeight: 700,
                        paddingBottom: 2
                    },

                    ' h2': {
                        color: '#4a90e2',
                        fontSize: 11,
                        fontWeight: 300,
                        paddingBottom: 2
                    },

                    ' h3': {
                        color: '#000',
                        fontSize: 11,
                        fontWeight: 300,
                        paddingBottom: 2
                    }
                }
            }
        }
    })
);

export default styles;
