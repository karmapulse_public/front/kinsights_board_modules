import { Observable } from 'rxjs';
import request from '../../helpers/http';

const defaultParams = {
    index: 'search_tweets'
};

export default (url, paramsData = defaultParams) => {
    const params = Object.assign(defaultParams, paramsData);

    const query = {
        index: params.index,
        es_query: {
            size: 0,
            query: {
                bool: {
                    filter: [
                        {
                            term: {
                                rule_id: params.search_id
                            }
                        },
                        {
                            term: {
                                'user.username': {
                                    value: params.username
                                }
                            }
                        }
                    ]
                }
            },
            aggs: {
                totalKlout: {
                    terms: {
                        field: 'user.klout',
                        size: 1
                    }
                }
            }
        }
    };

    return Observable.create((observer) => {
        const formatData = data => (
            data.aggregations.totalKlout.buckets[0].key
        );

        Observable.fromPromise(request(url, 'GET', query))
            .subscribe(
            data => (
                observer.next(formatData(data.json))
            ),
            error => observer.error(error),
                null
            );
    });
};
