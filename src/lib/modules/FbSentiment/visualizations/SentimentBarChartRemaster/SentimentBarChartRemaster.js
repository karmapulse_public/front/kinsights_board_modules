import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import NoDataModule from '../../../NoDataModule';
import DialogIcon from '../../../../helpers/icons/ceo/dialogIcon';
import { numberWithCommas } from '../../../../helpers/number';
import styles from './BarChartRemasterStyles';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    noDataYet: PropTypes.func,
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    noDataYet: null,
};

const SentimentBarChartRemaster = ({ moduleData, moduleConfig, moduleColor, noDataYet }) => {
    const refExport = useRef(null);
    const getPercent = n => ((n * 100) / moduleData.comments_total).toFixed(1).replace(/\.0$/, '');

    const { positivo, negativo, neutral } = moduleData.comments_sentiment;

    const renderContent = () => (
        <div className="bar-chart__header">
            <div className="padding">
                <div>
                    <p>
                        Positivo
                    </p>
                    <div className="bar-chart__porcentage">
                        <div className="bar__chart__chart">
                            <div
                                style={{ width: `${Math.trunc(getPercent(positivo))}%` }}
                                className={'bar__chart__chart__positive'}
                            />
                        </div>
                        <div className="bar-chart__percentages">
                            <p className={'bar-chart__percentages__positive'}>
                                {`${getPercent(positivo)}%`}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="padding">
                <div className="neutral">
                    <p>
                        Neutral
                    </p>
                    <div className="bar-chart__porcentage">
                        <div className="bar__chart__chart">
                            <div
                                style={{ width: `${Math.trunc(getPercent(neutral))}%` }}
                                className={'bar__chart__chart__neutral'}
                            />
                        </div>
                        <div className="bar-chart__percentages">
                            <p className={'bar-chart__percentages__neutral'}>
                                {`${getPercent(neutral)}%`}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="padding">
                <div>
                    <p>
                        Negativo
                    </p>
                    <div className="bar-chart__porcentage">
                        <div className="bar__chart__chart">
                            <div
                                style={{ width: `${Math.trunc(getPercent(negativo))}%` }}
                                className={'bar__chart__chart__negative'}
                            />
                        </div>
                        <div className="bar-chart__percentages">
                            <p className={'bar-chart__percentages__negative'}>
                                {`${getPercent(negativo)}%`}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
    return (
        <div ref={refExport} {...styles(moduleColor)}>
            <div className="bar-chart__label">
                <h3>{moduleConfig.title}</h3>
            </div>
            {
                (() => {
                    if (noDataYet !== null) {
                        return noDataYet();
                    }
                    return moduleData.comments_total !== 0 ?
                        <div className="bar-chart__content">
                            {
                                //moduleConfig.buttonExport(refExport, moduleConfig.title)
                            }
                            <div className="bar-chart__total-mentions">
                                <div className="bar-chart__total-mentions--header">
                                    <DialogIcon color={moduleColor} />
                                    <div className="bar-chart__total-mentions--h">
                                        {numberWithCommas(moduleData.comments_total)}
                                    </div>
                                </div>
                                <p> comentarios totales </p>
                            </div>
                            <div>
                                {renderContent()}
                            </div>
                        </div>
                        : <NoDataModule />;
                })()
            }
        </div>
    );
};


SentimentBarChartRemaster.propTypes = propTypes;
SentimentBarChartRemaster.defaultProps = defaultProps;

export default SentimentBarChartRemaster;
