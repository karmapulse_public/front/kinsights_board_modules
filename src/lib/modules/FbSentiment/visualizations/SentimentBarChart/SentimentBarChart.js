import React from 'react';
import PropTypes from 'prop-types';
import NoDataModule from '../../../NoDataModule';

import { numberWithCommas } from '../../../../helpers/number';
import styles from './styles';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666'
};

const SentimentBarChart = ({ moduleData, moduleConfig, moduleColor }) => {
    const getPercent = n => (
        n === 0 ? 0 :
        (n * 100) / (moduleData.comments_total)
    );

    const { positivo, negativo, neutral } = moduleData.comments_sentiment;

    const renderContent = () => (
        <div className="sentiment-bar__content">
            <h4>Comentarios totales: <span>{numberWithCommas(moduleData.comments_total)}</span></h4>
            <h5>Comentarios por sentimiento: </h5>
            <div className="sentiment-bar__percent positive">
                <h6>Positivo</h6>
                <div className="sentiment-bar__bar">
                    <div
                        style={{ width: `${getPercent(positivo)}%` }}
                    />
                </div>
                <span>{`${getPercent(positivo).toFixed(1).replace(/\.0$/, '')}%`}</span>
            </div>
            <div className="sentiment-bar__percent neutral">
                <h6>Neutral</h6>
                <div className="sentiment-bar__bar">
                    <div
                        style={{ width: `${getPercent(neutral)}%` }}
                    />
                </div>
                <span>{`${getPercent(neutral).toFixed(1).replace(/\.0$/, '')}%`}</span>
            </div>
            <div className="sentiment-bar__percent negative">
                <h6>Negativo</h6>
                <div className="sentiment-bar__bar">
                    <div style={{ width: `${getPercent(negativo)}%` }} />
                </div>
                <span>{`${getPercent(negativo).toFixed(1).replace(/\.0$/, '')}%`}</span>
            </div>
        </div>
    );

    return (
        <div className="sentiment-bar" {...styles(moduleColor)}>
            <div className="sentiment-bar__label">
                <h3>{moduleConfig.title}</h3>
            </div>
            {
                moduleData.comments_total !== 0 ?
                    renderContent()
                : <NoDataModule />
            }
        </div>
    );
};

SentimentBarChart.propTypes = propTypes;
SentimentBarChart.defaultProps = defaultProps;

export default SentimentBarChart;
