import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';


import fetchFbSentiment from './FbSentimentDataFlow';

import LoadingModule from '../LoadingModule';
import NoDataYet from '../NoDataYet';
import SentimentBarChart from './visualizations/SentimentBarChart';
import SentimentBarChartRemaster from './visualizations/SentimentBarChartRemaster';

const propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    dateRange: PropTypes.object,
    services: PropTypes.object,
    showNoDataYet: PropTypes.bool
};

const defaultProps = {
    fields: {},
    side: 'left',
    color: '#444',
    dateRange: {},
    services: {},
    showNoDataYet: false
};
const ref = React.createRef();

class FbSentiment extends Component {
    constructor(props) {
        super(props);

        this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color
        };
    }

    componentDidMount() {
        this.updateDataFlow(this.props);
    }

    componentWillReceiveProps(nextProps) {
        this.updateDataFlow(nextProps);
    }

    updateDataFlow(props) {
        const params = {
            rule_id: props.fields.search_id,
            initial_date: props.dateRange.startDate.toISOString(),
            final_date: props.dateRange.endDate.toISOString(),
            module_id: 'module_fb_sentiment_comments'
        };

        fetchFbSentiment(props.services.facebook, params)
            .subscribe(
                (data) => {
                    this.setState({
                        moduleConfig: props.fields,
                        moduleData: data,
                        moduleLoading: false,
                        moduleSide: props.side,
                        moduleColor: props.color
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    renderByViz() {
        const { moduleConfig, moduleData } = this.state;
        const { dateRange: { startDate, endDate }, showNoDataYet } = this.props;
        let renderNoDataYet = null;

        if (showNoDataYet && moment().diff(endDate, 'minutes') <= 0 && endDate.diff(startDate, 'days') <= 1) {
            renderNoDataYet = () => <NoDataYet />;
        }

        if (moduleData) {
            const visualizations = {
                sentiment_bar_chart: params => (
                    <SentimentBarChart {...params} />
                ),
                sentiment_bar_chart_remaster: params => (
                    <SentimentBarChartRemaster {...params} noDataYet={renderNoDataYet} />
                )
            };

            return visualizations[moduleConfig.visualization](this.state);
        }

        return (<LoadingModule />);
    }

    render() {
        return (
            <div className="m-fb-sentiment" ref={ref}>
                {this.renderByViz()}
            </div>
        );
    }
}

FbSentiment.propTypes = propTypes;
FbSentiment.defaultProps = defaultProps;

export default FbSentiment;
