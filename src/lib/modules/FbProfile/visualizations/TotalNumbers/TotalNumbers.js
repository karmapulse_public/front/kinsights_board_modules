import React from 'react';
import PropTypes from 'prop-types';
import isUndefined from 'lodash/isUndefined';
import orderBy from 'lodash/orderBy';
import NoDataModule from '../../../NoDataModule';
import { numberWithCommas } from '../../../../helpers/number';

import styles from './styles';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string
};

const defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleColor: '#666'
};

const TotalNumbers = ({ moduleData, moduleConfig, moduleColor }) => {
    const data = () => {
        const array = [];
        const entries = Object.entries(moduleData);
        const types = {
            fans_total: {
                title: 'Seguidores totales',
                index: 1
            },
            fans_reached: {
                title: 'Seguidores nuevos',
                index: 2
            },
            posts_total: {
                title: 'Publicaciones realizadas',
                index: 3
            },
            posts_fans_total: {
                title: 'Publicaciones de seguidores',
                index: 4
            },
            interactions_total: {
                title: 'Interacciones totales',
                index: 5
            }
        };

        entries.map(item => (
            array.push({
                key: item[0],
                title: (types[item[0]]) ? types[item[0]].title : undefined,
                index: (types[item[0]]) ? types[item[0]].index : undefined,
                total: item[1]
            })
        ));
        return orderBy(array, ['index']);
    };

    return (
        <article className="total-numbers" {...styles(moduleColor)} >
            <div className="total-numbers__label">
                <h3>{moduleConfig.title}</h3>
            </div>
            {
                (data()[0].total !== '0') ?
                    <div className="total-numbers__content">
                        {data().map((item) => {
                            if (!isUndefined(item.title)) {
                                return (
                                    <div key={item.key} className="total-numbers__content__data">
                                        <h4>{item.title}</h4>
                                        <h4
                                            className={
                                                item.key === 'fans_reached' ?
                                                (item.total > 0 ? 'positive' : 'negative')
                                                : ''
                                            }
                                        >
                                            {numberWithCommas(item.total)}
                                        </h4>
                                    </div>
                                );
                            }
                            return null;
                        })}
                    </div>
                    : <NoDataModule color={moduleColor} />
            }
        </article>
    );
};

TotalNumbers.propTypes = propTypes;
TotalNumbers.defaultProps = defaultProps;

export default TotalNumbers;
