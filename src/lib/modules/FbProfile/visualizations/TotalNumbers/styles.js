import { css } from 'glamor';

const styles = color => css({
    width: '100%',
    minWidth: '320px',
    maxWidth: '500px',

    ' .total-numbers': {

        ' &__label': {
            display: 'flex',
            justifyContent: 'space-between',
            padding: '15px 20px 15px 20px',
            borderTop: '1px solid #dfdfdf',
            color,

            ' h3': {
                fontSize: '13px',
                fontWeight: 600
            },
        },
        ' &__content': {
            padding: '25px 40px 25px 40px',

            ' &__data': {
                display: 'flex',
                justifyContent: 'space-between',
                padding: '10px 0px',

                ' >h4': {
                    fontSize: '13px',
                    fontWeight: 600,
                    opacity: '0.5',
                },
                ' >h4:last-child': {
                    fontSize: '16px',
                    fontWeight: 600,
                    opacity: '1',
                },
                ' .positive': {
                    color: '#89cf42',
                    '&::before': {
                        content: '"+"'
                    }
                },
                ' .negative': {
                    color: '#e18181'
                }
            }
        }
    }
});

export default styles;
