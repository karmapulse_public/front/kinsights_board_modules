import { css } from 'glamor';

const styles = () => (
    css({
        position: 'fixed',
        top: 0,
        left: 0,
        zIndex: 9999,
        width: '100vw',
        height: '100vh',
        backgroundColor: 'rgba(0,0,0,1)',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',

        ' .modal-dialog__content': {
            maxWidth: 500,
            width: '100%',
            height: '100%',
            display: 'flex',
            flexDirection: 'column'
        },

        ' .modal-dialog__close': {
            width: '100%',
            height: 50,
            cursor: 'pointer',
            position: 'relative',
            ' &::before': {
                content: '""',
                position: 'absolute',
                top: 15,
                right: 15,
                width: 3,
                height: 21,
                backgroundColor: '#FFF',
                transform: 'rotate(45deg)'
            },
            ' &::after': {
                content: '""',
                position: 'absolute',
                top: 15,
                right: 15,
                width: 3,
                height: 21,
                backgroundColor: '#FFF',
                transform: 'rotate(-45deg)'
            }
        },

        ' .modal-dialog__image': {
            width: '100%',
            flex: 1,
            backgroundColor: 'transparent'
        },

        ' .modal-dialog__tweet': {
            width: '100%',
            padding: '20px 10px',
            boxSizing: 'border-box',
            color: '#fff',
            fontSize: 13,
            backgroundColor: 'rgba(0,0,0,.4)',
            '&__user': {
                display: 'flex',
                alignItems: 'center',
                ' img': {
                    width: 40,
                    height: 40,
                    borderRadius: 4,
                    marginRight: 10,
                },
                ' h4': {
                    flex: 1,
                    ' span': {
                        display: 'block',
                        marginTop: 3,
                        fontSize: 12,
                        opacity: 0.54
                    }
                },
                ' a': {
                    color: '#FFF',
                    fontSize: 14,
                    textDecoration: 'none'
                }
            },
            ' &__date': {
                ' span': {
                    display: 'block',
                    color: '#FFF',
                    fontSize: 12,
                    textAlign: 'right',
                    opacity: 0.54,
                    '&:last-of-type': {
                        marginTop: 3
                    }
                },
                ' a': {
                    color: '#FFF',
                    textDecoration: 'none'
                }
            },
            ' &__body': {
                marginTop: 10,
                lineHeight: 1.29,
                fontSize: 14,
                fontWeight: 300,
                opacity: 0.84,
                ' a': {
                    color: '#fff',
                    textDecoration: 'none'
                }
            },
            '&__actions': {
                padding: '10px 0px',
                ' >a': {
                    marginRight: 30
                }
            }
        }
    })
);

export default styles;
