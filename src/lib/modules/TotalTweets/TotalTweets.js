import React, { Component } from 'react';
import PropTypes from 'prop-types';

import fetchTotalTweets from './TotalTweetsDataFlow';

import LoadingModule from '../LoadingModule';
import CardHorizontalBars from './visualizations/CardHorizontalBars';
import CardTotalNumbers from './visualizations/CardTotalNumbers';
import CardInline from './visualizations/CardInline';

const propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    services: PropTypes.object,
    dateRange: PropTypes.object
};

const defaultProps = {
    fields: {},
    side: 'left',
    color: '#666',
    services: {},
    dateRange: {}
};

class TotalTweets extends Component {
    constructor(props) {
        super(props);

        this.state = {
            moduleServices: props.services,
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color,
            moduleRange: props.dateRange
        };
    }

    componentDidMount() {
        this.updateDataFlow(this.props);
    }

    componentDidUpdate(prevProps) {
        const { startDate, endDate } = this.props.dateRange;
        const { fields: { hasClick, search_id } } = this.props;
        if (startDate !== prevProps.dateRange.startDate ||
            endDate !== prevProps.dateRange.endDate ||
            search_id !== prevProps.fields.search_id) {
            this.updateDataFlow(this.props);
        }
        if (hasClick !== prevProps.fields.hasClick) {
            this.changeState(hasClick);
        }
    }

    changeState(hasClick) {
        this.setState({
            moduleConfig: {
                ...this.state.moduleConfig,
                hasClick
            }
        });
    }

    updateDataFlow(props) {
        const { fields, dateRange: { startDate, endDate }, services } = props;
        const params = {
            recipe_id: 'module_tw_total_tweets',
            rule_id: fields.search_id,
            initial_date: startDate.utc().toISOString(),
            final_date: endDate.utc().toISOString()
        };

        fetchTotalTweets(services.twitter, params)
            .subscribe(
                (data) => {
                    this.setState({
                        moduleConfig: props.fields,
                        moduleData: data,
                        moduleLoading: false,
                        moduleSide: props.side,
                        moduleColor: props.color,
                        moduleRange: props.dateRange
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    renderByViz() {
        const { moduleConfig, moduleData } = this.state;

        if (moduleData) {
            const visualizations = {
                card_horizontal_bars: params => (
                    <CardHorizontalBars {...params} />
                ),
                card_total_numbers: params => (
                    <CardTotalNumbers {...params} />
                ),
                card_total_inline: params => (
                    <CardInline {...params} />
                ),
            };
            return visualizations[moduleConfig.visualization](this.state);
        }

        return (<LoadingModule />);
    }

    render() {
        return (
            <div className="m-total-tweets">
                {this.renderByViz()}
            </div>
        );
    }
}

TotalTweets.propTypes = propTypes;
TotalTweets.defaultProps = defaultProps;

export default TotalTweets;
