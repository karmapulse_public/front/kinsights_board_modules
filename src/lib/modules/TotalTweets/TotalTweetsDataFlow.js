import { Observable } from 'rxjs';
import fromPairs from 'lodash/fromPairs';
import isUndefined from 'lodash/isUndefined';
import find from 'lodash/find';
import { fetchXHR } from '../../helpers/http';

const formatData = (buckets) => {
    const collect = [];
    const tweetsTypes = ['retweet', 'original', 'reply', 'quote'];
    tweetsTypes.map((item) => {
        const s = find(buckets, { key: item });
        if (!isUndefined(s)) return collect.push([[s.key], s.doc_count]);
        return collect.push([[item], 0]);
    });
    return collect;
};

export default (url, params) => (
    Observable.create((observer) => {
        Observable.fromPromise(fetchXHR(url, 'GET', params))
            .subscribe(
            (data) => {
                observer.next(Object.assign(
                    { total: data.json.response.hits.total },
                    fromPairs(formatData(data.json.response.aggregations.tweets_by_type.buckets))
                ));
            },
            (error) => { observer.error(error); }
            );
    })
);
