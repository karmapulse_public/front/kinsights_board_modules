import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import omit from 'lodash/omit';
import isUndefined from 'lodash/isUndefined';
import Typography from 'kpulse_design_system/dist/Typography';

import withDrawer from '../../../withDrawer';

import { numberWithCommas } from '../../../../helpers/number';
import uniqueId from '../../../../helpers/uniqueId';
import styles from './cardInlineStyles';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleSide: PropTypes.string,
    moduleColor: PropTypes.string,
    onClick: PropTypes.func
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleSide: 'left',
    moduleColor: '#666',
    onClick: () => ({})
};

const { Paragraph } = Typography;

const CardInline = ({
    moduleConfig, moduleData, moduleSide, moduleColor, onClick
}) => {
    const refExport = React.useRef(null);
    const mapLabels = {
        original: 'Originales',
        retweet: 'Retweets',
        quote: 'Citados',
        reply: 'Respuestas'
    };
    const titleConfig = type => ({
        title: moduleConfig.title || 'Tweets en el tiempo',
        subtitleLeft: type ? mapLabels[type] : '',
        subtitleRight: '',
    });
    const onRealClick = moduleConfig.hasClick ? onClick : () => ({});

    // New logic -> add originals with replies
    const validateAdd = () => {
        // if (!isUndefined(moduleData.reply)) {
        //     return moduleData.original + moduleData.reply;
        // }
        return moduleData.original;
    };

    let tweetsTotal = {};

    if (isUndefined(moduleData.original)) {
        tweetsTotal = moduleData;
    } else {
        tweetsTotal = Object.assign({}, moduleData, {
            original: validateAdd()
        });
    }

    const getMapLabelsKey = value => Object.entries(mapLabels).find(i => i[1] === value)[0];

    const formatedTweets = Object.keys(omit(tweetsTotal, ['total']))
        .map(type => ({
            label: mapLabels[type],
            total: tweetsTotal[type]
        }));

    const renderTotalNumbers = () => formatedTweets.map(type => (
        <div
            key={uniqueId()}
            className={type.label}
            onClick={() => { onRealClick({ filters: { type: getMapLabelsKey(type.label) } }, titleConfig(getMapLabelsKey(type.label))); }}
        >
            <Paragraph small color="rgba(0,0,0,0.5)">{type.label}</Paragraph>
            <Paragraph large>
                {numberWithCommas(type.total)}
            </Paragraph>
        </div>
    ));

    const { title = 'DISTRIBUCIÓN TWEETS', buttonExport } = moduleConfig;

    return (
        <div
            className="card-total-inline"
            {...styles(moduleSide, moduleColor, moduleConfig.hasClick)}
            ref={refExport}
        >
            <div className="mentions">
                <div className="mentions__title">
                    <h3>{title}</h3>
                </div>
                <div className="mentions__body">
                    {
                        // buttonExport(refExport, title, {
                        //     top: '-20px',
                        //     right: '5px'
                        // })
                    }
                    <div
                        className="mentions__body__total"
                        onClick={() => onRealClick({}, titleConfig())}
                    >
                        <div>
                            <Paragraph small bold>Total de tweets</Paragraph>
                            <Paragraph large bold color="#53b2db">{numberWithCommas(moduleData.total)}</Paragraph>
                        </div>
                    </div>
                    <div className="mentions__body__numbers">
                        {renderTotalNumbers()}
                    </div>
                </div>
            </div>
        </div>
    );
};

CardInline.propTypes = propTypes;
CardInline.defaultProps = defaultProps;

export default withDrawer(CardInline);
