import React from 'react';
import PropTypes from 'prop-types';
import omit from 'lodash/omit';
import isUndefined from 'lodash/isUndefined';

import { numberWithCommas } from '../../../../helpers/number';
import styles from './cardHorizontalBarsStyles';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleSide: PropTypes.string,
    moduleColor: PropTypes.string
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleSide: 'left',
    moduleColor: '#666'
};

const CardHorizontalBars = ({ moduleConfig, moduleData, moduleSide, moduleColor }) => {
    // New logic -> add originals with replies
    const validateAdd = () => {
        if (!isUndefined(moduleData.reply)) {
            return moduleData.original + moduleData.reply;
        }
        return moduleData.original;
    };

    let tweetsTotal = {};

    if (isUndefined(moduleData.original)) {
        tweetsTotal = moduleData;
    } else {
        tweetsTotal = Object.assign({}, moduleData, {
            original: validateAdd()
        });
    }

    const formatedTweets = Object.keys(
        omit(tweetsTotal, ['total', 'reply'])
    ).map(type => ({
        label: type.charAt(0).toUpperCase() + type.slice(1),
        total: tweetsTotal[type],
        percentage: Math.ceil((tweetsTotal[type] * 100) / tweetsTotal.total)
    }));

    const renderBars = () => formatedTweets.map((type, index) => (
        <div key={index}>
            <h4>{type.label}</h4>
            <div className="mentions__graphic__bar">
                <div className="percentage" style={{ width: type.percentage }} />
            </div>
            <h5>{numberWithCommas(type.total)}</h5>
        </div>
    ));

    const { title = 'MENCIONES' } = moduleConfig;

    return (
        <div className="card-horizontal" {...styles(moduleSide, moduleColor)}>
            <div className="mentions">
                <div className="mentions__label">
                    <h3>{title}</h3>
                </div>
                <div className="mentions__number">
                    <h4>Total de menciones</h4>
                    <h5>{numberWithCommas(moduleData.total)}</h5>
                </div>
                <div className="mentions__graphic">
                    <div className="card__divider" />
                    {renderBars()}
                </div>
            </div>
        </div>
    );
};

CardHorizontalBars.propTypes = propTypes;
CardHorizontalBars.defaultProps = defaultProps;

export default CardHorizontalBars;
