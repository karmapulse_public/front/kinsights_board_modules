import React from 'react';
import PropTypes from 'prop-types';
import omit from 'lodash/omit';
import isUndefined from 'lodash/isUndefined';

import { numberWithCommas } from '../../../../helpers/number';
import styles from './cardTotalNumbersStyles';
import ConversationIcon from '../../../../helpers/icons/ceo/conversationIcon';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleSide: PropTypes.string,
    moduleColor: PropTypes.string
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleSide: 'left',
    moduleColor: '#666'
};

const CardTotalNumbers = ({ moduleConfig, moduleData, moduleSide, moduleColor }) => {
    // New logic -> add originals with replies
    const validateAdd = () => {
        if (!isUndefined(moduleData.reply)) {
            return moduleData.original + moduleData.reply;
        }
        return moduleData.original;
    };

    let tweetsTotal = {};

    if (isUndefined(moduleData.original)) {
        tweetsTotal = moduleData;
    } else {
        tweetsTotal = Object.assign({}, moduleData, {
            original: validateAdd()
        });
    }

    const mapLabels = {
        original: 'Originales',
        retweet: 'Retweets',
        quote: 'Citados'
    };

    const formatedTweets = Object.keys(
        omit(tweetsTotal, ['total', 'reply'])
    ).map(type => ({
        label: mapLabels[type],
        total: tweetsTotal[type],
        color: (() => {
            const types = {
                original: '#7b98ba',
                quote: '#e68f4b',
                retweet: '#67c5b0'
            };
            return types[type];
        })()
    }));

    

    const renderTotalNumbers = () => formatedTweets.map((type, index) => (
        <div key={index}>
            {/* <h4 style={{ color: type.color }}> */}
            <h4>
                {numberWithCommas(type.total)}
            </h4>
            <h5>{type.label}</h5>
        </div>
    ));

    const { title = 'RELEVANCIA SOCIAL' } = moduleConfig;

    return (
        <div className="card-total-numbers" {...styles(moduleSide, moduleColor)} >
            <div className="mentions">
                <div className="mentions__title">
                    <h3>{title}</h3>
                </div>
                <div className="mentions__body">
                    <div className="mentions__body__total">
                        <div>
                            <ConversationIcon color="#6AB9DC" />
                            <h4>{numberWithCommas(moduleData.total)}</h4>
                        </div>
                        <h5>Menciones totales</h5>
                    </div>
                    <div className="mentions__body__numbers">
                        {renderTotalNumbers()}
                    </div>
                </div>
            </div>
        </div>
    );
};

CardTotalNumbers.propTypes = propTypes;
CardTotalNumbers.defaultProps = defaultProps;

export default CardTotalNumbers;
