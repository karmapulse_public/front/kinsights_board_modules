import React from 'react';
import PropTypes from 'prop-types';
import ChartColumns from '../../../Charts/ChartColumns';
import { numberWithCommas } from '../../../../helpers/number';

import styles from './styles';
import NoDataModule from '../../../NoDataModule';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666'
};

const PerformanceTable = (props) => {
    const { moduleConfig, moduleData, moduleColor } = props;

    const types = {
        video: 'Video',
        photo: 'Imagen',
        status: 'Texto',
        link: 'Enlace',
        angry: {
            text: 'Me enfada',
            color: '#e57272'
        },
        haha: {
            text: 'Me divierte',
            color: '#e8d17d'
        },
        like: {
            text: 'Me gusta',
            color: '#697fcd'
        },
        love: {
            text: 'Me encanta',
            color: '#c289c9'
        },
        sad: {
            text: 'Me entristece',
            color: '#8c7fac'
        },
        wow: {
            text: 'Me asombra',
            color: '#83c6b7'
        },
        comments: {
            text: 'Comentarios',
            color: '#cbd3d8'
        },
        shares: {
            text: 'Shares',
            color: '#acb8c0'
        }
    };

    const FormatData = () => {
        const data = [];
        const dataKeys = [];
        const nameArray = [];
        moduleData.data.forEach(el => (
            data.push({
                denomitation: types[el.denomination],
                like: el.reactions.LIKE,
                love: el.reactions.LOVE,
                haha: el.reactions.HAHA,
                wow: el.reactions.WOW,
                sad: el.reactions.SAD,
                angry: el.reactions.ANGRY,
                comments: el.comments_total,
                shares: el.shares_total
            })
        ));
        Object.keys(data[0]).forEach((i) => {
            if (i !== 'denomitation') {
                dataKeys.push(
                    {
                        key: i,
                        group: (i !== 'comments') ? (i !== 'shares') ? 'interactions' : undefined : undefined,
                        color: types[i].color
                    }
                );
                nameArray.push(types[i].text);
            }
        });

        const obj = {
            data,
            dataKeys,
            nameArray,
        };
        return obj;
    };
    return (
        <div className="performance-chart" {...styles(moduleColor)}>
            <div className="performance-chart__label">
                <h3>{moduleConfig.title}</h3>
            </div>
            {
                (moduleData.total !== 0)
                    ? (
                        <div className="performance-chart__content">
                            <h4>Interacciones totales: <span>{numberWithCommas(moduleData.total)}</span></h4>
                            <ChartColumns
                                dataKeys={FormatData().dataKeys}
                                nameArray={FormatData().nameArray}
                                data={FormatData().data}
                                type="multiple"
                            />
                        </div>
                    )
                    : <NoDataModule color={moduleColor} />
            }
        </div>
    );
};

PerformanceTable.propTypes = propTypes;
PerformanceTable.defaultProps = defaultProps;

export default PerformanceTable;
