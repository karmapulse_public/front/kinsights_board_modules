import React from 'react';
import PropTypes from 'prop-types';

import {
    LikeIcon,
    LoveIcon,
    HahaIcon,
    WowIcon,
    SadIcon,
    AngryIcon
} from '../../../../helpers/icons/fb';

import { numberWithCommas } from '../../../../helpers/number';

import styles from './styles';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666'
};

const PerformanceTable = (props) => {
    const { moduleConfig, moduleData, moduleColor } = props;

    const types = {
        video: 'video',
        photo: 'imagen',
        status: 'texto',
        link: 'link'
    };

    const renderHeader = () => (
        <div className="table__header">
            <div className="header__percent">
                <h4>Tipos de publicación</h4>
                <h4>Porcentaje de interacción</h4>
            </div>
            <div className="header__reactions">
                <LikeIcon />
                <LoveIcon />
                <HahaIcon />
                <WowIcon />
                <SadIcon />
                <AngryIcon />
            </div>
            <div className="header__interactions">
                <h4>Total de interacciones</h4>
            </div>
        </div>
    );

    const renderPercentBars = (name, num) => {
        const percent = (num / moduleData.total) * 100;
        return (
            <div className={`content__percent ${types[name]}`}>
                <div className="percent__bars" >
                    <span>{types[name]}</span>
                    <div
                        style={{ width: `${percent}%` }}
                    />
                </div>
                <span>{`${Math.round(percent)}%`}</span>
            </div>
        );
    };

    const renderReactions = reactions => (
        <div className="content__reactions">
            {
                reactions.map(
                    (reaction, i) => (
                        <span key={i}>
                            {numberWithCommas(reaction.total)}
                        </span>
                    )
                )
            }
        </div>
    );

    const renderContent = () => (
        moduleData.data.map(
            (item, index) => (
                <div className="table__content" key={index}>
                    {renderPercentBars(item.denomination, item.total)}
                    {renderReactions(item.reactions)}
                    <div className="content__interactions">
                        <span>{numberWithCommas(item.total)}</span>
                    </div>
                </div>
            )
        )
    );

    const renderFooter = () => (
        <div className="table__footer">
            <div className="key__colors">
                <span>Texto</span>
                <span>Link</span>
                <span>Imagen</span>
                <span>Video</span>
            </div>
            <div className="total">
                <span>Total: {numberWithCommas(moduleData.total)}</span>
            </div>
        </div>
    );

    const renderTable = () => (
        <div className="performance__table">
            {renderHeader()}
            {renderContent()}
            {renderFooter()}
        </div>
    );

    return (
        <div className="performance-table" {...styles(moduleColor)}>
            <div className="performance__label">
                <h3>{moduleConfig.title}</h3>
            </div>
            {renderTable()}
        </div>
    );
};

PerformanceTable.propTypes = propTypes;
PerformanceTable.defaultProps = defaultProps;

export default PerformanceTable;
