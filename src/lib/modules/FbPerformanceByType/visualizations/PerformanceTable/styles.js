import { css } from 'glamor';

const styles = color => css({
    minWidth: 1200,

    /* --------------------------
        Título de Tabla
    -------------------------- */
    ' .performance__label': {
        padding: '15px 20px',
        borderBottom: '1px solid #dfdfdf',
        ' h3': {
            fontSize: 13,
            fontWeight: 700
        }
    },

    /* --------------------------
        Headeeer de Tabla
    -------------------------- */
    ' .table__header': {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: 67,
        ' >div': {
            padding: '0px 40px',
            display: 'flex',
            alignItems: 'center',
            height: 'inherit'
        },
        ' h4': {
            fontSize: 13,
            letterSpacing: 0.2,
            opacity: 0.5
        },
        ' .header__percent': {
            minWidth: 480,
            justifyContent: 'space-between',
            borderRight: '1px solid #dfdfdf'
        },
        ' .header__reactions': {
            minWidth: 490,
            padding: 0,
            justifyContent: 'space-around',
            borderRight: '1px solid #dfdfdf'
        },
        ' .header__interactions': {
            minWidth: 230
        }
    },

    /* --------------------------
        Contenido de Tabla
    -------------------------- */
    ' .table__content': {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: 43,
        ' >div': {
            padding: '0px 40px',
            display: 'flex',
            alignItems: 'center',
            height: 'inherit'
        },
        ' .content__percent': {
            minWidth: 480,
            borderRight: '1px solid #dfdfdf',
            justifyContent: 'space-between',
            ' span': {
                fontSize: 16,
                letterSpacing: 0.2,
                fontWeight: 500
            },
            ' .percent__bars': {
                position: 'relative',
                width: 300,
                height: 4,
                backgroundColor: '#ededed',
                ' span': {
                    position: 'absolute',
                    top: -18,
                    left: 0,
                    fontSize: 13,
                    letterSpacing: 0.2,
                    fontWeight: 500,
                    textTransform: 'capitalize'
                },
                ' div': {
                    maxWidth: 300,
                    height: 4
                }
            },
            ' &.texto': {
                ' .percent__bars div': {
                    backgroundColor: '#6f9cbc'
                },
                ' >span': {
                    color: '#6f9cbc'
                }
            },
            ' &.link': {
                ' .percent__bars div': {
                    backgroundColor: '#c4d5cb'
                },
                ' >span': {
                    color: '#c4d5cb'
                }
            },
            ' &.imagen': {
                ' .percent__bars div': {
                    backgroundColor: '#ce9089'
                },
                ' >span': {
                    color: '#ce9089'
                }
            },
            ' &.video': {
                ' .percent__bars div': {
                    backgroundColor: '#f3ccac'
                },
                ' >span': {
                    color: '#f3ccac'
                }
            }
        },
        ' .content__reactions': {
            minWidth: 490,
            padding: 0,
            borderRight: '1px solid #dfdfdf',
            ' span': {
                width: 81.666666,
                height: 'inherit',
                fontSize: 13,
                fontWeight: 500,
                letterSpacing: 0.2,
                textAlign: 'center',
                lineHeight: '43px',
                '&:not(:last-of-type)': {
                    borderRight: '1px solid #dfdfdf'
                }
            }
        },
        ' .content__interactions': {
            minWidth: 230,
            ' span': {
                fontSize: 16,
                fontWeight: 500,
                letterSpacing: 0.2
            }
        }
    },

    /* --------------------------
        Footer de Tabla
    -------------------------- */
    ' .table__footer': {
        padding: '15px 0px 30px 40px',
        display: 'flex',
        justifyContent: 'space-between',
        ' .key__colors': {
            display: 'flex',
            alignItems: 'center',
            ' span': {
                fontSize: 10,
                fontWeight: 500,
                verticalAlign: 'middle',
                '&::before': {
                    content: '""',
                    display: 'inline-block',
                    width: 9,
                    height: 9,
                    marginRight: 5,
                    verticalAlign: 'middle',
                    borderRadius: 20
                },
                '&:not(:last-child)': {
                    marginRight: 20
                },
                '&:nth-child(1)::before': {
                    backgroundColor: '#6f9cbc'
                },
                '&:nth-child(2)::before': {
                    backgroundColor: '#c4d5cb'
                },
                '&:nth-child(3)::before': {
                    backgroundColor: '#ce9089'
                },
                '&:nth-child(4)::before': {
                    backgroundColor: '#f3ccac'
                }
            },
        },
        ' .total': {
            width: 230,
            paddingLeft: 40,
            ' span': {
                color,
                fontSize: 16,
                fontWeight: 500,
                letterSpacing: 0.2
            }
        }
    }
});

export default styles;
