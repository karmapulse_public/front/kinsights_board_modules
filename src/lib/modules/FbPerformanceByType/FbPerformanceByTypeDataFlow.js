import { Observable } from 'rxjs';
import { requestPost } from '../../helpers/http';

export default (url, params) => (
    Observable.create((observer) => {
        Observable.fromPromise(requestPost(url, 'POST', params))
        .subscribe(
            (data) => {
                observer.next(data.json);
            },
            (error) => {
                observer.error(error);
            }
        );
    })
);
