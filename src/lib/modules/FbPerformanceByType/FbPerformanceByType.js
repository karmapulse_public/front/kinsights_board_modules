import React, { Component } from 'react';
import PropTypes from 'prop-types';


import fetchFbPerformanceByType from './FbPerformanceByTypeDataFlow';

import LoadingModule from '../LoadingModule';
import PerformanceTable from './visualizations/PerformanceTable';
import PerformanceChart from './visualizations/PerformanceChart';

const propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    dateRange: PropTypes.object,
    services: PropTypes.object
};

const defaultProps = {
    fields: {},
    side: 'left',
    color: '#444',
    dateRange: {},
    services: {}
};

class FbPerformanceByType extends Component {
    constructor(props) {
        super(props);

        this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color
        };
    }

    componentDidMount() {
        this.updateDataFlow(this.props);
    }

    componentWillReceiveProps(nextProps) {
        this.updateDataFlow(nextProps);
    }

    updateDataFlow(props) {
        const params = {
            rule_id: props.fields.search_id,
            initial_date: props.dateRange.startDate.toISOString(),
            final_date: props.dateRange.endDate.toISOString(),
            module_id: 'module_fb_performance_by_type'
        };

        fetchFbPerformanceByType(props.services.facebook, params)
            .subscribe(
                (data) => {
                    this.setState({
                        moduleConfig: props.fields,
                        moduleData: data,
                        moduleLoading: false,
                        moduleSide: props.side,
                        moduleColor: props.color
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    renderByViz() {
        const { moduleConfig, moduleData } = this.state;
        if (moduleData) {
            const visualizations = {
                performance_table: params => (
                    <PerformanceTable {...params} />
                ),
                performance_chart: params => (
                    <PerformanceChart {...params} />
                )
            };

            return visualizations[moduleConfig.visualization](this.state);
        }

        return (<LoadingModule />);
    }

    render() {
        return (
            <div className="m-fb-performance-by-type">
                {this.renderByViz()}
            </div>
        );
    }
}

FbPerformanceByType.propTypes = propTypes;
FbPerformanceByType.defaultProps = defaultProps;

export default FbPerformanceByType;
