import React, { Component } from 'react';
import PropTypes from 'prop-types';
import isNull from 'lodash/isNull';
import setDates from '../../helpers/setDates';


import fetchMap, { fetchBoundingBox } from './MapDataFlow';
import LoadingModule from '../LoadingModule';
import DotMapa from './visualizations/DotMapa';

const propTypes = {
    fields: PropTypes.object,
    size: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    dateRange: PropTypes.object,
    services: PropTypes.object,
    filters: PropTypes.object,
    withCache: PropTypes.bool
};

const defaultProps = {
    fields: {},
    side: 'left',
    color: '#666',
    dateRange: {},
    size: {},
    services: {},
    filters: {},
    withCache: true
};

const mappingZoom = zoom => (Math.round((((zoom - 0) * 11) / 20) + 1));

const mapLng = (lng) => {
    if (lng >= -180 && lng <= 180) return lng;
    const dir = lng < 0 ? 1 : -1;
    const diff = Math.abs(lng) % 180;
    return (dir * (180 - diff));
};

class Mapa extends Component {
    constructor(props) {
        super(props);

        this.state = {
            moduleConfig: { ...props.fields, ...props.size },
            moduleData: {},
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color,
            boundingBox: {},
        };
        this.getData = this.getData.bind(this);
    }

    componentDidMount() {
        this.getInitialView(this.props);
    }

    componentWillReceiveProps(nextProps) {
        this.getInitialView(nextProps);
    }

    getInitialView(info) {
        const params = {
            recipe_id: 'module_tw_bounding_box',
            rule_id: info.fields.search_id,
            ...setDates(info.dateRange, this.props.withCache),
            filters: { ...info.filters },
        };

        fetchBoundingBox(this.props.services.twitter, params)
            .subscribe(
                (data) => {
                    this.setState({
                        ...data
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    getData(referencia, bounds) {
        const params = {
            recipe_id: 'module_tw_map',
            ...setDates(this.props.dateRange, this.props.withCache),
            rule_id: this.props.fields.search_id,
            filters: {
                bounding_box: {
                    top_left: {
                        ...bounds.top_left,
                        lon: mapLng(bounds.top_left.lon)
                    },
                    bottom_right: {
                        ...bounds.bottom_right,
                        lon: mapLng(bounds.bottom_right.lon)
                    }
                },
                precision: { value: mappingZoom(referencia.state.viewport.zoom) },
                ...this.props.filters
            }
        };
        fetchMap(this.props.services.twitter, params)
            .subscribe(
                (data) => {
                    this.setState({
                        ...data,
                        moduleLoading: false
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    renderByViz() {
        const { moduleConfig, moduleData } = this.state;
        if (!isNull(moduleData)) {
            const visualizations = {
                dot_map: params => (
                    <DotMapa {...params} getData={this.getData} />
                )
            };
            return visualizations[this.props.view || moduleConfig.visualization](this.state);
        }

        return (<LoadingModule />);
    }
    render() {
        return (
            <div className="m-map">
                {this.renderByViz()}
            </div>
        );
    }
}

Mapa.propTypes = propTypes;
Mapa.defaultProps = defaultProps;

export default Mapa;
