import React, { Component } from 'react';
import PropTypes from 'prop-types';

import isEmpty from 'lodash/isEmpty';
import styles from './dotMapsStyles';
import NoDataModule from '../../NoDataModule';
import Map from '../../Charts/Map';
import Dot from '../../../helpers/icons/dotMapIcon';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    boundingBox: PropTypes.object,
    getData: PropTypes.func,
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    boundingBox: {},
    getData: () => {},
};

class DotMapa extends Component {
    constructor(props) {
        super(props);
        this.state = {
            boundingBox: null,
            width: props.moduleConfig.width
        };
        this.contenedor = null;
        this.mapRef = null;
        this.viewportTimer = null;
        this.changeWidth = this.changeWidth.bind(this);
        this.onViewportChange = this.onViewportChange.bind(this);
    }

    componentDidMount() {
        window.addEventListener('resize', () => {
            this.changeWidth(this.contenedor.clientWidth);
        });
        this.changeWidth(this.contenedor.clientWidth);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.boundingBox !== this.props.boundingBox) {
            const { _sw, _ne } = nextProps.boundingBox;
            if (_sw && _ne) {
                const bounds = {
                    top_left: {
                        lon: _sw.lng,
                        lat: _ne.lat
                    },
                    bottom_right: {
                        lon: _ne.lng,
                        lat: _sw.lat
                    }
                };
                this.setState({
                    boundingBox: [[_sw.lng, _sw.lat], [_ne.lng, _ne.lat]],
                }, () => {
                    this.props.getData(this.mapRef, bounds);
                });
            } else {
                this.setState({
                    boundingBox: [],
                });
            }
        }
        if (nextProps.moduleData !== this.props.moduleData) {
            this.setState({
                module
            });
        }
    }

    renderIcon(color, data) { // eslint-disable-line
        return Dot({ // eslint-disable-line
            color,
            value: data.value,
            styles: {
                fontFamily: 'Roboto',
                fontSize: '14px',
                fontWeight: 900,
                letterSpacing: '0.5px',
                fill: '#ffffff',
            }
        });
    }

    onViewportChange(viewport) {
        clearTimeout(this.viewportTimer);
        this.viewportTimer = setTimeout(() => {
            const { bounding_box } = viewport;
            const { top_left, bottom_right } = bounding_box;
            this.props.getData(this.mapRef, bounding_box);
            this.setState({
                boundingBox: [[top_left.lon, bottom_right.lat], [bottom_right.lon, top_left.lat]],
            });
        }, 100);
    }

    changeWidth(newWidth) {
        this.setState({
            width: (newWidth - 30)
        });
    }

    render() {
        const { moduleData, moduleConfig, moduleColor } = this.props;
        const { boundingBox, width } = this.state;
        const { title = 'TOP DE TEMAS', height } = moduleConfig;
        const { dots } = moduleData;
        return (
            <div
                className="dot-map"
                {...styles(moduleColor)}
                ref={(container) => {
                    if (container) this.contenedor = container;
                }}
            >
                <div className="dot-map__label">
                    <h3>{title}</h3>
                </div>
                <div
                    className={`dot-map__chart ${isEmpty(boundingBox) ? 'no-map' : ''}`}
                >
                    { isEmpty(boundingBox) ? <NoDataModule /> : null }
                    <Map
                        ref={(map) => { if (map) this.mapRef = map; }}
                        token="pk.eyJ1Ijoia2FybWFib2FyZCIsImEiOiJtVDltdF9RIn0.HIXtQ102rscFzfF32gAVZA"
                        mapStyle="mapbox://styles/karmaboard/cjl6t6r4z12pw2sl4vd4wcsja"
                        data={dots}
                        valueName="total"
                        positionName="centroid"
                        boundingBox={isEmpty(boundingBox) ? [[1, 1], [2, 2]] : boundingBox}
                        width={width}
                        height={height}
                        colors={[
                            { key: 'positivo', color: '#89cf41' },
                            { key: 'negativo', color: '#e18080' },
                            { key: 'neutral', color: '#95bbd4' }
                        ]}
                        colorFind={
                            (colors, colorData) => (colors.find(elto => elto.key === colorData.sentiment))
                        }
                        singleIcon={this.renderIcon}
                        groupIcon={this.renderIcon}
                        onViewportChange={this.onViewportChange}
                    />
                </div>
            </div>
        );
    }

}

DotMapa.propTypes = propTypes;
DotMapa.defaultProps = defaultProps;

export default DotMapa;
