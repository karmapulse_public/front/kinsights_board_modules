import { Observable } from 'rxjs';
import { fetchXHR } from '../../helpers/http';

export default (url, params) => (
    Observable.create((observer) => {
        Observable.fromPromise(fetchXHR(url, 'GET', params))
            .subscribe(
            (data) => {
                observer.next({
                    moduleData: { dots: data.json.response.data }
                });
            },
            (error) => { observer.error(error); }
            );
    })
);

export const fetchBoundingBox = (url, params) => (
    Observable.create((observer) => {
        Observable.fromPromise(fetchXHR(url, 'GET', params))
            .subscribe(
                (response) => {
                    const bBox = response.json.response;
                    if (!bBox || Object.keys(bBox).length === 0) {
                        observer.next({
                            boundingBox: {},
                            moduleLoading: false,
                        });
                    } else {
                        const { bottom_right, top_left } = bBox;
                        /*
                            Se hace una suma de 2 para agregar un padding al componente del
                            mapa y así, no queden los punto cortados por el tamaño del mapa
                        */
                        let boundingBox = {
                            _sw: {
                                lng: top_left.lon - 2,
                                lat: bottom_right.lat - 2
                            },
                            _ne: {
                                lng: bottom_right.lon + 2,
                                lat: top_left.lat + 2
                            }
                        };
                        let moduleLoading = true;
                        if (top_left.lon === bottom_right.lon && top_left.lat === bottom_right.lat) {
                            boundingBox = {};
                            moduleLoading = false;
                        }
                        observer.next({
                            boundingBox,
                            moduleLoading,
                        });
                    }
                },
                (error) => { observer.error(error); }
            );
    })
);
