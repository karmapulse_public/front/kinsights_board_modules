import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import fetchTopTweets from './TopTweetsDataFlow';

import LoadingModule from '../LoadingModule';
import NoDataYet from '../NoDataYet';
import SliderTopTweets from './visualizations/SliderTopTweets';

const propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    dateRange: PropTypes.object,
    services: PropTypes.object,
    showNoDataYet: PropTypes.bool,
    top: PropTypes.number
};

const defaultProps = {
    fields: {},
    side: 'left',
    color: '#444',
    dateRange: {},
    services: {},
    showNoDataYet: false,
    top: 5
};

class TopTweets extends Component {
    constructor(props) {
        super(props);

        this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color
        };
    }

    componentDidMount() {
        this.updateDataFlow(this.props);
    }

    componentWillReceiveProps(nextProps) {
        this.updateDataFlow(nextProps);
    }

    updateDataFlow(props) {
        const params = {
            rule_id: props.fields.search_id,
            initial_date: props.dateRange.startDate.toISOString(),
            final_date: props.dateRange.endDate.toISOString(),
            recipe_id: 'module_tw_top_feed',
            filters: {
                top: props.top
            }
        };

        fetchTopTweets(props.services.twitter, params)
            .subscribe(
                (data) => {
                    this.setState({
                        moduleConfig: props,
                        moduleData: data,
                        moduleLoading: false,
                        moduleSide: props.side,
                        moduleColor: props.color
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    renderByViz() {
        const { moduleConfig, moduleData } = this.state;
        const { dateRange: { startDate, endDate }, showNoDataYet } = this.props;
        let renderNoDataYet = null;

        if (showNoDataYet && moment().diff(endDate, 'minutes') <= 0 && endDate.diff(startDate, 'days') <= 1) {
            renderNoDataYet = () => <NoDataYet />;
        }
        if (moduleData) {
            const visualizations = {
                slider_top_tweets: params => (
                    <SliderTopTweets {...params} noDataYet={renderNoDataYet} />
                )
            };

            return visualizations[moduleConfig.fields.visualization](this.state);
        }

        return (<LoadingModule />);
    }

    render() {
        return (
            <div className="m-tw-top-posts">
                {this.renderByViz()}
            </div>
        );
    }
}

TopTweets.propTypes = propTypes;
TopTweets.defaultProps = defaultProps;

export default TopTweets;
