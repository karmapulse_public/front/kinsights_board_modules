import { css } from 'glamor';

const styles = css({
    ' .top-tweets': {
        ' &__label': {
            display: 'flex',
            paddingLeft: '18px',
            alignItems: 'center',
            justifyContent: 'space-between',
            borderBottom: '1px solid rgba(212, 193, 156, 0.5)',
            ' h3': {
                fontSize: '14px',
                fontWeight: 700,
                color: '#b38e5d',
            },
            ' div': {
                width: '44px',
                height: '44px',
                display: 'flex',
                alignItems: 'center',
                background: '#00acee',
                justifyContent: 'center',
            }
        },
    },
    ' .carousel': {
        '&-items__item': {
            height: '100%'
        },
        '&-ctrDots--bottom': {
            marginTop: '20px !important'
        },
        '&-ctrArrow__left': {
            left: '15px !important'
        },
        '&-ctrArrow__right': {
            right: '15px !important',
            left: 'auto !important'
        },
    },
    ' .card': {
        display: 'flex',
        width: '100%',
        height: '100%',
        flexWrap: 'wrap',
        justifyContent: 'center',
        ' .custom-gallery': {
            padding: 0
        },
        ' .custom-link': {
            margin: 0
        },
        ' .video': {
            width: '100%',
            height: 212,
            margin: 0
        },
        '&__number': {
            position: 'absolute',
            top: 0,
            left: 0,
            ' >p': {
                width: '45px',
                height: '45px',
                display: 'flex',
                color: '#ffffff',
                alignItems: 'center',
                background: '#b38e5d',
                justifyContent: 'center',
            },
        },
        '&__data': {
            width: 360,
            ' section': {
                padding: 0,
                margin: 0,
                boxShadow: 'none',
                ' >article': {
                    padding: '20px 0 0 0'
                },
                ' > p:first-of-type': {
                    overflow: 'hidden',
                    padding: 0
                }
            },
        },
        '&__interactions': {
            position: 'relative',
            display: 'flex',
            width: 360,
            alignItems: 'center',
            justifyContent: 'flex-start',
            alignSelf: 'flex-end',
            ' >h5': {
                fontSize: 14,
                display: 'flex',
                fontWeight: 500,
                alignItems: 'center',
                flexDirection: 'column',
                justifyContent: 'center',
                color: 'rgba(78, 35, 46,0.5)',
                ' span': {
                    marginBottom: 5,
                    display: 'block',
                    color: '#4e232e',
                    fontSize: 14,
                    fontWeight: 700,
                    textAlign: 'center',
                },
                ':first-child': {
                    marginRight: 50
                }
            },
            ' >div': {
                width: 130,
                height: 40,
                position: 'relative',
                ' >h5': {
                    fontSize: 14,
                    width: '100%',
                    fontWeight: 700,
                    textAlign: 'center',
                    color: '#4e232e',
                    marginBottom: 5,
                },
                ' ul': {
                    height: 15,
                    width: '100%',
                    display: 'flex',
                    verticalAlign: 'middle',
                    justifyContent: 'space-around',
                    ' li': {
                        float: 'left',
                        ' svg': {
                            width: 20,
                            height: 20,
                        }
                    }
                },
                ' >span': {
                    marginLeft: 10,
                    verticalAlign: 'middle',
                    fontSize: 16,
                    lineHeight: '20px',
                    fontWeight: 500
                },
                '&:hover': {
                    ' .card__tooltip': {
                        opacity: 1,
                        visibility: 'visible'
                    }
                }
            },
            ' &::after': {
                content: '""',
                position: 'absolute',
                top: -12,
                left: 0,
                width: '100%',
                height: 2,
                backgroundColor: 'rgba(212, 193, 156, 0.5)',
            }
        },
        '&__tooltip': {
            zIndex: 10,
            right: '-10%',
            width: 114,
            bottom: '-50%',
            padding: '13px 15px',
            position: 'absolute',
            boxShadow: '0 2px 8px 0 rgba(0, 0, 0, 0.2)',
            backgroundColor: '#fff',
            visibility: 'hidden',
            opacity: 0,
            transition: 'all .2s ease',
            transform: 'translate(0px, -50%)',
            ' svg': {
                width: 17,
                height: 17,
                marginRight: 11,
                verticalAlign: 'middle',
            },
            ' >h5': {
                fontSize: 12,
                fontWeight: 700,
                color: 'rgb(78, 35, 46)',
                '&:not(:last-child)': {
                    marginBottom: 7
                }
            },
            ' span': {
                verticalAlign: 'middle',
                '&:first-of-type': {
                    marginRight: 5
                }
            }
        }
    },
});

export default styles;
