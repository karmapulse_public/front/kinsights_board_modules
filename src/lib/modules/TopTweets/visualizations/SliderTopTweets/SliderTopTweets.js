import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'kpulse_design_system/dist/Carousel';
import { faTwitter } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import NoDataModule from '../../../NoDataModule';
import styles from './styles';

import Gallery from '../../../../helpers/customComponents/Gallery';
import Header from '../../../../helpers/customComponents/Header';
import BodyText from '../../../../helpers/customComponents/BodyText';
import Card from '../../../../helpers/customComponents/Card';
import Link from '../../../../helpers/customComponents/Link';
import Video from '../../../../helpers/customComponents/Video';

import { numberWithCommas } from '../../../../helpers/number';

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    noDataYet: PropTypes.func
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    noDataYet: null
};

const SliderTopTweets = ({ moduleData, moduleConfig }) => {
    const source = 'twitter';
    const renderContent = () => (
        <div style={{ position: 'relative' }}>
            <Slider width="100%" height="410px" dotsColor="#b38e5d" arrowsColor="#d4c19c" arrowsHeight="20px">
                {
                    moduleData.map((item, index) => {
                        return (
                            <div className="card" key={index}>
                                <div className="card__number">
                                    <p>
                                        {index + 1}
                                    </p>
                                </div>
                                <div className="card__data">
                                    <Card key={index} wTextDynamic>
                                        <Header noSource item={item._source} services={moduleConfig} />
                                        <BodyText text={item._source.body} type={source} />
                                        {(() => {
                                            if (item._source.media_type === 'image') {
                                                return (
                                                    <Gallery
                                                        items={item._source.images}
                                                        height="212px"
                                                        type={source}
                                                        url={item._source.link}
                                                    />
                                                );
                                            }
                                            if (item._source.media_type === 'gif') {
                                                return (
                                                    <Video
                                                        sources={JSON.parse(item._source.gif.variants)}
                                                        cover={item._source.gif.cover}
                                                        type={source}
                                                        w
                                                        gif
                                                    />
                                                );
                                            }
                                            if (item._source.media_type === 'video') {
                                                return (
                                                    <Video
                                                        sources={JSON.parse(item._source.video.variants)}
                                                        cover={item._source.video.cover}
                                                        type={source}
                                                    />
                                                );
                                            }
                                            if (item._source.urls) {
                                                return (
                                                    // eslint-disable-next-line jsx-a11y/anchor-is-valid
                                                    <Link
                                                        url={item._source.urls[0].url}
                                                        title={item._source.urls[0].title}
                                                        text={item._source.urls[0].description}
                                                        type={source}
                                                    />
                                                );
                                            }
                                            return null;
                                        })()}
                                    </Card>
                                </div>
                                <div className="card__interactions">
                                    <h5>
                                        <span>{numberWithCommas(item._source.favorites)}</span> Me Gusta
                                    </h5>
                                    <h5>
                                        <span>{numberWithCommas(item._source.retweets)}</span> Retweets
                                    </h5>
                                </div>
                            </div>
                        );
                    })
                }
            </Slider>
        </div>
    );

    return (
        <div className="top-tweets" {...styles}>
            <div className="top-tweets__label">
                <h3>TWEETS CON MÁS INTERACCIÓN</h3>
                <div>
                    <FontAwesomeIcon icon={faTwitter} color="white" />
                </div>
            </div>
            {
                moduleData.length !== 0 ? renderContent() : <NoDataModule />
            }
        </div>
    );
};


SliderTopTweets.propTypes = propTypes;
SliderTopTweets.defaultProps = defaultProps;

export default SliderTopTweets;
