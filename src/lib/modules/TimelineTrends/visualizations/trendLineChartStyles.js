import { css } from 'glamor';

const styles = (color, hasClick) => (
    css({
        position: 'relative',
        width: '100%',
        height: 'auto',
        color: 'rgba(0,0,0, 0.5)',

        ' .card__divider': {
            height: 2,
            opacity: 0.2,
            backgroundColor: color,
        },
        ' .trend__label': {
            '> h3': {
                padding: '15px 20px 30px',
                fontSize: 16,
                fontWeight: 400,
                textAlign: 'left',
                textTransform: 'uppercase',
                color
            }
        },

        ' .trend__header': {
            width: '100%',
            height: 60,
            padding: '0px 20px',
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            color: '#FFF',
            backgroundColor: color === undefined ? '#666' : color,
            ' >h2': {
                fontSize: 16,
            },
            ' >h3': {
                opacity: 0.8
            },
            ' svg': {
                marginRight: 10
            },
        },

        ' .trend__chart': {
            width: '100%',
            height: 400,
            paddingRight: 30,
            ' svg': {
                ' text': {
                    lineHeight: 1.4,
                    fontSize: 12,
                },
                ' .recharts-cartesian-axis-line, .recharts-cartesian-axis-tick-line': {
                    stroke: 'transparent'
                },
                ' .recharts-cartesian-grid-horizontal line': {
                    strokeWidth: 1
                }
            },

            ' .recharts-legend-wrapper': {
                bottom: '0px !important',
                left: '0px !important',
                paddingLeft: 30,
                width: '100%',
                ' ul': {
                    margin: '15px 0px 0px !important',
                },
                ' ul > li': {
                    cursor: hasClick ? 'pointer' : 'default'
                }
            },

            ' .recharts-default-legend': {
                height: 100,
                display: 'flex',
                flexDirection: 'column',
                flexWrap: 'wrap',
                ' .recharts-legend-item': {
                    display: 'block !important',
                    padding: '0px 0px 15px !important',
                    margin: '0 !important',
                },
                ' .recharts-legend-item-text': {
                    fontSize: 12,
                    color: 'rgba(0,0,0,.8)'
                }
            },

            ' .recharts-default-tooltip': {
                border: 'none',
                borderRadius: 2,
                ' p': {
                    fontSize: 12,
                    color: '#000',
                    opacity: 0.4
                }
            },

            ' .recharts-tooltip-item-list': {

                ' li': {
                    fontSize: 12
                }
            },

            ' .recharts-tooltip-cursor': {
                stroke: '#eee'
            }
        },

        ' .chart-line': {
            width: '100%',
            height: 350,
            margin: '10px auto',
        },
        ' .chart-line-progress': {
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
        },
        ' .chart-line .tooltip_custom': {
            backgroundColor: '#FFF',
            boxShadow: '2px 2px 10px rgba(0, 0, 0, .2)',
            fontSize: 12,
            padding: 10,
            borderRadius: 2,
            letterSpacing: '1px',
            lineHeight: '20px',
        },
        ' .noData': {
            height: 250,
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
        },
    })
);

export default styles;
