import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import styles from './trendLineChartStyles';
import { numberWithCommas } from '../../../helpers/number';
import Lines from '../../Charts/ChartsLines';
import locationIcon from '../../../helpers/icons/locationIcon';
import NoDataModule from '../../NoDataModule';
import withDrawer from '../../withDrawer';
import { labelType } from '../../../helpers/paddingDates';

const colors = [
    { r: 199, g: 89, b: 147 },
    { r: 129, g: 117, b: 204 },
    { r: 204, g: 95, b: 66 },
    { r: 91, g: 169, b: 101 },
    { r: 84, g: 161, b: 211 }
];

const propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string
};

const defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666'
};

const TrendLineChart = (props) => {
    const refExport = useRef(null);
    const { moduleConfig, moduleData, moduleColor, onClick, timeInterval } = props;
    const { title = 'TEMAS EN EL TIEMPO', hasClick } = moduleConfig;
    const { location = 'LOCACIÓN' } = moduleConfig;
    const { entity_type, buttonExport } = moduleConfig;
    const onDotClick = (a) => {
        const startDate = moment(a.payload.key).utc();
        const finishDate = moment(a.payload.key).add(1, timeInterval).utc();
        // subtitleLeft = `${data.dataKey} del ${labelType(startDate, finishDate, intervalType)}`;
        onClick({
            'initial-date': startDate.format(),
            'final-date': finishDate.format(),
            filters: {
                phrase: a.dataKey
            }
        }, {
            title,
            subtitleLeft: `${a.dataKey} del ${labelType(startDate, finishDate, timeInterval)}`,
            subtitleRight: '',
        });
    };
    const onLegendClick = (word) => {
        onClick({
            filters: {
                phrase: word
            }
        }, {
            title,
            subtitleLeft: word,
            subtitleRight: '',
        });
    };
    return (
        <div
            ref={refExport}
            className="trend-line-chart"
            {...styles(moduleColor, hasClick)}
        >
            {/* <div className="trend__header">
                <h2>
                    {locationIcon()}
                    {location}
                </h2>
                <h3>{`Total: ${numberWithCommas(moduleData.total)} Tuits`}</h3>
            </div> */}
            <div className="trend__label">
                <h3>{title}</h3>
            </div>
            {
                // buttonExport(refExport, title)
            }
            {
                (moduleData.values.aggregations[entity_type].buckets.reduce(
                    (prev, item) => (prev += item.doc_count)
                , 0) === 0)
                    ? (
                        <div className="noData">
                            <NoDataModule color={moduleColor} />
                        </div>
                    )
                    : (
                        <div className="trend__chart">
                            <Lines
                                data={moduleData.values}
                                type="multiple"
                                colors={colors}
                                entity={moduleConfig.entity_type}
                                intervalType={timeInterval}
                                onLineClick={onDotClick}
                                onLegendClick={onLegendClick}
                                hasClick={hasClick}
                            />
                        </div>
                    )
            }
        </div>
    );
};

TrendLineChart.propTypes = propTypes;
TrendLineChart.defaultProps = defaultProps;

export default withDrawer(TrendLineChart);
