import { Observable } from 'rxjs';
import { fetchXHR } from '../../helpers/http';
import paddingDates from '../../helpers/paddingDates';

export default (url, params) => (
    Observable.create((observer) => {
        Observable.fromPromise(fetchXHR(url, 'GET', params))
            .subscribe(
                (data) => {
                    if (params.filters.query === 'search') {
                        const formatData = (params.filters.words.split(',').map((item, index) => ({
                            key: item,
                            doc_count: data.json.response.aggregations.entity.buckets[`${index}`].doc_count,
                            [`${params.filters.entity}_by_time`]: {
                                buckets: paddingDates(
                                    data.json.response.aggregations.entity.buckets[`${index}`].entity_by_time.buckets,
                                    params,
                                    'key_as_string',
                                    'doc_count'
                                )
                            }
                        }))).sort(
                            (a, b) => (a.doc_count - b.doc_count)
                        ).reverse();
                        observer.next({
                            total: data.json.response.hits.total,
                            values: {
                                aggregations: {
                                    [`${params.filters.entity}`]: {
                                        buckets: formatData
                                    }
                                }
                            }
                        });
                    } else {
                        const formatData = data.json.response.aggregations.entity.buckets.map((item, index) => (
                            {
                                key: item.key,
                                doc_count: item.doc_count,
                                [`${params.filters.entity}_by_time`]: {
                                    buckets: paddingDates(
                                        item.entity_by_time.buckets,
                                        params,
                                        'key_as_string',
                                        'doc_count'
                                    )
                                }
                            }
                        ));
                        observer.next({
                            total: data.json.response.hits.total,
                            values: {
                                aggregations: {
                                    [`${params.filters.entity}`]: {
                                        buckets: formatData
                                    }
                                }
                            }
                        });
                    }
                },
                (error) => { observer.error(error); }
            );
    })
);
