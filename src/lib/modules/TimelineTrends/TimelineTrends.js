import React, { Component } from 'react';
import PropTypes from 'prop-types';
import isNull from 'lodash/isNull';


import fetchTimelineTrends from './TimelineTrendsDataFlow';
import { calculateNewInterval } from '../../helpers/getTimeInterval';
import LoadingModule from '../LoadingModule';
import TrendLineChart from './visualizations/TrendLineChart';

const propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    dateRange: PropTypes.object,
    services: PropTypes.object
};

const defaultProps = {
    fields: {},
    side: 'left',
    color: '#666',
    dateRange: {},
    services: {}
};

class TimelineTrends extends Component {
    constructor(props) {
        super(props);
        this.myRef = React.createRef();
        this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color,
            moduleRange: props.dateRange,
            moduleServices: props.services,
        };
    }

    componentDidMount() {
        this.updateDataFlow(this.props);
    }

    componentDidUpdate(prevProps) {
        const { startDate, endDate } = this.props.dateRange;
        const { hasClick, search_id } = this.props.fields;
        if (startDate !== prevProps.dateRange.startDate ||
            endDate !== prevProps.dateRange.endDate ||
            search_id !== prevProps.fields.search_id) {
            this.updateDataFlow(this.props);
        }
        if (hasClick !== prevProps.fields.hasClick) {
            this.changeState(hasClick);
        }
    }

    changeState(hasClick) {
        this.setState({
            moduleConfig: {
                ...this.state.moduleConfig,
                hasClick
            }
        });
    }


    updateDataFlow(props) {
        const { fields, dateRange: { startDate, endDate } } = props;

        const timeInterval = calculateNewInterval(
            startDate.startOf('day'),
            endDate.endOf('day'),
            this.myRef
        );

        const params = {
            recipe_id: 'module_tw_timeline_trends',
            rule_id: fields.search_id,
            initial_date: startDate.startOf('day').utc().toISOString(),
            final_date: endDate.endOf('day').utc().toISOString(),
            filters: {
                entity: fields.entity_type,
                query: fields.query_type,
                words: fields.search_entities,
                interval: timeInterval
            }
        };

        if (fields.query_type === 'search') {
            params.searchEntities = fields.search_entities.split(',');
        }

        fetchTimelineTrends(props.services.twitter, params)
            .subscribe(
                (data) => {
                    this.setState({
                        moduleData: data,
                        moduleLoading: false,
                        moduleColor: props.color,
                        timeInterval,
                        moduleConfig: props.fields
                    });
                },
                (error) => {
                    console.log(error);
                },
                null
            );
    }

    renderByViz() {
        const { moduleConfig, moduleData } = this.state;
        if (!isNull(moduleData)) {
            const visualizations = {
                trend_line_chart: params => (
                    <TrendLineChart {...params} />
                )
            };
            return visualizations[this.props.view || moduleConfig.visualization](this.state);
        }

        return (<LoadingModule />);
    }
    render() {
        return (
            <div className="m-timeline-trends" ref={this.myRef}>
                {this.renderByViz()}
            </div>
        );
    }
}

TimelineTrends.propTypes = propTypes;
TimelineTrends.defaultProps = defaultProps;

export default TimelineTrends;
