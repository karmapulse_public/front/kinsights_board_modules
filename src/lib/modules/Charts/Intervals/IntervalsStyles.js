import { css } from 'glamor';

export default () => css({
    ' .interval': {
        '&__scale': {
            display: 'inline-block',
            marginRight: 10,
            padding: '5px 10px',
            borderRadius: 2,
            color: '#03a9f4',
            fontSize: 14,
            lineHeight: 1.29,
            cursor: 'pointer',
            transition: 'all 0.5s ease',
            '&:hover': {
                backgroundColor: 'rgba(220, 220, 220, 0.46)',
            },
            '&--active': {
                backgroundColor: 'rgba(220, 220, 220, 0.46) !important',
                cursor: 'default'
            },
            '&--inactive': {
                color: '#A6A6A6',
                cursor: 'default',
                '&:hover': {
                    backgroundColor: 'rgba(0,0,0,0)'
                }
            }
        }
    }
});
