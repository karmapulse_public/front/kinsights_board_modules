import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';

import intervalsStyles from './IntervalsStyles';

const propTypes = {
    dateRange: PropTypes.object,
    intervalType: PropTypes.string,
    handleChangeInterval: PropTypes.func,
};

const defaultProps = {
    dateRange: {},
    intervalType: 'month',
    handleChangeInterval: () => ({}),
};

const Intervals = (props) => {
    const activeClass = 'interval__scale--active';
    const disableClass = 'interval__scale--inactive';
    const dateDiff = moment(props.dateRange.endDate).diff(props.dateRange.startDate, 'days', true);
    return (
        <div className="chart-intervals" {...intervalsStyles()}>
            <button
                className={`interval__scale ${props.intervalType === 'hour' ? activeClass : ''} ${dateDiff > 15 ? disableClass : ''}`}
                onClick={() => {
                    if (dateDiff <= 15) {
                        props.handleChangeInterval('hour');
                    }
                }}
            >
                Horas
            </button>
            <button
                className={`interval__scale ${props.intervalType === 'day' ? activeClass : ''} ${dateDiff > 365 ? disableClass : ''}`}
                onClick={() => {
                    if (dateDiff <= 365) {
                        props.handleChangeInterval('day');
                    }
                }}
            >
                Días
            </button>
            <button
                className={`interval__scale ${props.intervalType === 'week' ? activeClass : ''} ${dateDiff > 2555 ? disableClass : ''}`}
                onClick={() => {
                    if (dateDiff <= 2555) {
                        props.handleChangeInterval('week');
                    }
                }}
            >
                Semanas
            </button>
            <button
                className={`interval__scale ${props.intervalType === 'month' ? activeClass : ''}`}
                onClick={() => props.handleChangeInterval('month')}
            >
                Meses
            </button>
        </div>
    );
};

Intervals.propTypes = propTypes;
Intervals.defaultProps = defaultProps;

export default Intervals;
