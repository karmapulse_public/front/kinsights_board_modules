import React from 'react';
import { styles } from './styles';


const Tooltip = (props) => {
    if (props.isEnabled) {
        const ocultar = props.x != '0' ? props.x : '-50px';
        return (
            <div
                {...styles()}
                className="react-chartsjs__tooltip"
                style={{ left: ocultar, top: props.y, zIndex: 100 }}
            >
                {props.children}
            </div>
        );
    }
    return null;
};

export default Tooltip;

