import React from 'react';
import PropTypes from 'prop-types';
import orderBy from 'lodash/orderBy';
import isUndefined from 'lodash/isUndefined';
import {
    AreaChart,
    Area,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    ResponsiveContainer
} from 'recharts';

import AxisX from '../CustomComponents/AxisX';
import AxisY from '../CustomComponents/AxisY';
import LegendCustom from '../CustomComponents/Legend';
import TooltipCustom from '../CustomComponents/TooltipCustom';

const propTypes = {
    data: PropTypes.array,
    dataKeys: PropTypes.array,
    nameArray: PropTypes.array,
    type: PropTypes.string,
    hasAxis: PropTypes.bool,
    hasGrid: PropTypes.bool,
    hasLegend: PropTypes.bool
};

const defaultProps = {
    data: [],
    dataKeys: [],
    nameArray: [],
    type: 'single',
    hasAxis: true,
    hasGrid: true,
    hasLegend: true
};

const ChartArea = ({ data, dataKeys, nameArray, type, hasAxis, hasGrid, hasLegend }) => {
    const renderAreaForTypes = () => {
        const types = {
            single: (() => (
                dataKeys.map(
                    (item, index) => (
                        <Area
                            key={index}
                            activeDot={false}
                            dataKey={item.key}
                            stackId="a"
                            fill={item.color}
                            stroke={item.color}
                            fillOpacity={1}
                        />
                    )
                )
            ))(),
            multiple: (() => (
                dataKeys.map(
                    (item, index) => {
                        if (isUndefined(item.group)) {
                            return (
                                <Area
                                    key={index}
                                    activeDot={false}
                                    dataKey={item.key}
                                    fill={item.color}
                                    stroke={item.color}
                                    fillOpacity={1}
                                />
                            );
                        }
                        return (
                            <Area
                                key={index}
                                activeDot={false}
                                dataKey={item.key}
                                stackId={item.group}
                                fill={item.color}
                                stroke={item.color}
                                fillOpacity={1}
                            />
                        );
                    }
                )
            ))()

        };
        return types[type];
    };

    return (
        <ResponsiveContainer>
            <AreaChart
                data={orderBy(data, ['date'], ['asc'])}
                margin={{ top: 20, right: 10, left: -30, bottom: 5 }}
            >
                { hasAxis &&
                    <XAxis
                        type="category"
                        dataKey="date"
                        tickLine={false}
                        interval={'preserveStartEnd'}
                        stroke="rgb(204, 204, 204)"
                        tick={
                            <AxisX
                                color="rgba(0, 0, 0, 0.8)"
                                style={{ fontSize: 10, fontWeight: 600, stroke: 'rgba(0, 0, 0, 0.8)', strokeWidth: '0.05px' }}
                            />
                        }
                    />
                }
                { hasAxis &&
                    <YAxis
                        axisLine={false}
                        tickLine={false}
                        tick={
                            <AxisY
                                color="rgba(0, 0, 0, 0.8)"
                                style={{ fontSize: 10, fontWeight: 600, stroke: 'rgba(0, 0, 0, 0.8)', strokeWidth: '0.05px' }}
                            />
                        }
                    />
                }
                { hasGrid && <CartesianGrid vertical={false} /> }
                <Tooltip content={<TooltipCustom />} />
                { hasLegend && <Legend nameArray={nameArray} height={36} content={<LegendCustom />} styles={{ fontSize: 10, justifyContent: 'center', textTransform: 'capitalize', fontWeight: 600 }} /> }
                {renderAreaForTypes()}
            </AreaChart>
        </ResponsiveContainer>
    );
};

ChartArea.propTypes = propTypes;
ChartArea.defaultProps = defaultProps;

export default ChartArea;
