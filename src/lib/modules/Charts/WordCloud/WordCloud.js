import React from 'react';
import invariant from 'invariant';
import d3WordCloud from './classD3';
import Tooltip from '../Tooltip/Tooltip';

import {
    propTypes,
    defaultProps
} from './defaultValues';

class WordCloud extends React.Component {
    container = React.createRef(); // eslint-disable-line
    chart = React.createRef(); // eslint-disable-line

    state = {
        tooltipContent: '',
        enableTooltip: this.props.enableTooltip,
        tooltipX: 0,
        tooltipY: 0,
    }

    componentDidMount() {
        this.validateProps();
        d3WordCloud.create(
            this.props,
            Object.assign({}, {
                chart: this.chart.current,
                container: this.container.current,
                tooltipState: this.tooltipState
            })
        );
    }

    componentWillReceiveProps(nextProps) {
        d3WordCloud.update(
            nextProps,
            Object.assign({}, {
                chart: this.chart.current,
                container: this.container.current,
                tooltipState: this.tooltipState
            })
        );
    }

    tooltipState = (arrs) => {
        this.setState(arrs);
    }

    validateProps = () => {
        const {
            maxAngle, minAngle, words, wordCountKey, wordKey
        } = this.props;
        invariant(
            Math.abs(minAngle) <= 90 && Math.abs(maxAngle) <= 90,
            'Angles must have values between -90 to 90 degrees'
        );
        invariant(minAngle <= maxAngle, 'minAngle must be <= maxAngle');
        if (words.length > 0) {
            const firstRow = words[0];
            // invariant(
            //     wordKey in firstRow,
            //     'Word key must be a valid key in the data'
            // );
            // invariant(
            //     wordCountKey in firstRow,
            //     'Word count key must be a valid key in the data'
            // );
        }
    }
    componentWillUnmount() {
        d3WordCloud.destroy(this.chart.current);
    }

    render() {
        const {
            tooltipContent,
            enableTooltip,
            tooltipX,
            tooltipY
        } = this.state;

        return (
            <div ref={this.container}>
                <div ref={this.chart} />
                <Tooltip x={tooltipX} y={tooltipY} isEnabled={enableTooltip} >
                    <h1>{tooltipContent}</h1>
                </Tooltip>
            </div>
        );
    }
}

WordCloud.propTypes = propTypes;
WordCloud.defaultProps = defaultProps;

export default WordCloud;
