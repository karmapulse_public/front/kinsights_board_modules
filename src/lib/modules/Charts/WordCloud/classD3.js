import cloud from 'd3-cloud';
import { event as currentEvent } from 'd3-selection';

import uniqBy from 'lodash/uniqBy';
import isNull from 'lodash/isNull';
import maxBy from 'lodash/maxBy';
import minBy from 'lodash/minBy';
import { D3, MIN_HEIGHT, MIN_WIDTH, DEFAULT_COLORS } from './defaultValues';

export default (() => {
    // let _fontScale;
    let _height;
    let _layout;
    let _svg;
    let _vis;
    let _width;
    let _words;
    let _fontScale;

    let PROPS = {};
    let CONFIG = {};

    const setDimensions = (height, width) => {
        const { container } = CONFIG;
        const {
            offsetHeight: parentHeight,
            offsetWidth: parentWidth
        } = container.parentNode;
        // console.log(parentHeight);
        _height = height || parentHeight;
        _width = width || parentWidth;

        if (typeof _height !== 'number' || _height < MIN_HEIGHT) {
            console.warn(`Invalid/small height provided, falling back to minimum value of ${MIN_HEIGHT}`);
            _height = MIN_HEIGHT;
        }

        if (typeof _width !== 'number' || _width < MIN_WIDTH) {
            console.warn(`Invalid/small width provided, falling back to minimum value of ${MIN_HEIGHT}`);
            _width = MIN_HEIGHT;
        }
    };

    const getScale = (scale) => {
        const scales = {
            linear: D3.scaleLinear,
            log: D3.scaleLog,
            sqrt: D3.scaleSqrt,
            fontSize: null
        };
        return scales[scale];
    };

    const fontSize = (value) => {
        const { words, fontScale: { minFont, maxFont } } = PROPS;
        const initialValues = {
            MAX: maxBy(words, 'total'),
            MIN: minBy(words, 'total'),
            FONT_MAX: maxFont,
            FONT_MIN: minFont,
        };
        const result = value === initialValues.MIN.total ?
            initialValues.FONT_MIN :
            (value / initialValues.MAX.total) * (initialValues.FONT_MAX - initialValues.FONT_MIN) +
            initialValues.FONT_MIN;
        return result;
    };

    const chooseRandom = array => array[Math.floor(Math.random() * array.length)];

    const onMouseOver = (d) => {
        const {
            enableTooltip, wordKey, wordCountKey, tooltip
        } = PROPS;
        const tooltipContent = tooltip ?
            tooltip(d) :
            `${d[wordKey]} (${d[wordCountKey]})`;
        if (enableTooltip) {
            CONFIG.tooltipState({
                tooltipContent,
                enableTooltip: true,
                tooltipX: currentEvent.pageX,
                tooltipY: currentEvent.pageY - 28,
            });
        }
    };

    const onMouseOut = () => {
        if (PROPS.enableTooltip) {
            CONFIG.tooltipState({
                enableTooltip: false,
            });
        }
    };

    const colorsScale = (d, i) => {
        const { colorScale, colors } = PROPS;
        return colorScale ?
            colorScale(d, i, () => chooseRandom(colors || DEFAULT_COLORS)) :
            chooseRandom(colors || DEFAULT_COLORS);
    };

    const transformText = (d) => {
        const translate = `translate(${d.x}, ${d.y})`;
        const rotate = typeof d.rotate === 'number' ? `rotate(${d.rotate})` : '';
        return translate + rotate;
    };

    const draw = (words) => {
        const {
            fontFamily, transitionDuration, onWordClick, wordKey
        } = PROPS;
        _words = _vis.selectAll('text').data(words);
        _words
            .enter()
            .append('text')
            .on('click', onWordClick)
            .on('mouseover', onMouseOver)
            .on('mouseout', onMouseOut)
            .attrs({
                cursor: onWordClick ? 'pointer' : 'default',
                fill: colorsScale,
                'font-family': fontFamily,
                'text-anchor': 'middle',
                transform: 'translate(0, 0) rotate(0)',
            })
            .transition()
            .duration(transitionDuration)
            .attrs({
                'font-size': d => `${d.size}px`,
                transform: transformText,
            })
            .text(d => d[wordKey]);

        _words
            .transition()
            .duration(transitionDuration)
            .attrs({
                fill: colorsScale,
                'font-family': fontFamily,
                'font-size': d => `${d.size}px`,
                transform: transformText,
            })
            .text(d => d[wordKey]);

        _words
            .exit()
            .transition()
            .duration(transitionDuration)
            .attr('fill-opacity', 0)
            .remove();
    };

    const update = (props, config) => {
        PROPS = props;
        CONFIG = config;
        const {
            fontFamily,
            height,
            maxAngle,
            maxWords,
            minAngle,
            orientations,
            scale,
            spiral,
            width,
            wordCountKey,
            words,
            wordKey
        } = PROPS;

        setDimensions(height, width);
        _svg
            .attrs({
                height: _height,
                width: _width
            })
            .style('background', 'transparent');

        _vis
            .attr(
                'transform',
                `translate(${_width / 2}, ${_height / 2})scale(1.0020877122879028,1.0020877122879028)`
            );

        const d3Scale = getScale(scale);
        const filteredWords = words.slice(0, maxWords);

        if (!isNull(d3Scale)) {
            _fontScale =
                uniqBy(filteredWords, wordCountKey).length > 1 ?
                    d3Scale().range([10, 100]) :
                    d3Scale().range([100, 100]);
            if (filteredWords.length) {
                _fontScale.domain([
                    D3.min(filteredWords, d => d[wordCountKey]),
                    D3.max(filteredWords, d => d[wordCountKey]),
                ]);
            }
        } else {
            _fontScale = fontSize;
        }

        if (typeof orientations === 'number' && orientations > 0) {
            let rotations = [];
            if (orientations === 1) {
                rotations = [minAngle];
            } else {
                rotations = [minAngle, maxAngle];
                const increment = (maxAngle - minAngle) / (orientations - 1);
                let rotation = minAngle + increment;
                while (rotation < maxAngle) {
                    rotations.push(rotation);
                    rotation += increment;
                }
            }
            _layout.rotate(() => chooseRandom(rotations));
        }
        _layout
            .size([_width, _height])
            .words(filteredWords)
            .padding(1)
            .text(d => d[wordKey])
            .font(fontFamily)
            .fontSize(d => _fontScale(d[wordCountKey]))
            .spiral(spiral)
            .on('end', words => draw(words))
            .start();
        _fontScale = null;
    };

    const create = (props, config) => {
        PROPS = props;
        CONFIG = config;

        const { height, width } = PROPS;
        const { chart } = CONFIG;

        D3
            .select(chart)
            .selectAll('*')
            .remove();

        setDimensions(height, width);
        _svg = D3.select(chart).append('svg');
        _vis = _svg.append('g');
        _layout = cloud();
        update(PROPS, CONFIG);
    };

    const destroy = (node) => {
        D3.select(node).selectAll('*').remove();
        _layout = null;
        _svg.remove();
        _vis.remove();
        _words.remove();
        _fontScale = [];
    };

    return {
        create,
        update,
        destroy
    };
})();
