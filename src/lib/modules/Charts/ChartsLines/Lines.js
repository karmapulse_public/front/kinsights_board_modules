import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
// import moment from 'moment';
import {
    LineChart,
    Line,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    Label,
    ResponsiveContainer
} from 'recharts';
import isNull from 'lodash/isNull';

import AxisX from '../CustomComponents/AxisX';
import AxisY from '../CustomComponents/AxisY';
import mapDataHistogram from './MapDataHistogram';
import LegendCustom from '../CustomComponents/Legend';
import TooltipCustom from '../CustomComponents/TooltipCustom';
import balanceJson from '../../../helpers/balanceJson';
import DrawerFeed from '../../DrawerFeed';

const propTypes = {
    data: PropTypes.object.isRequired, // eslint-disable-line
    type: PropTypes.string.isRequired, // eslint-disable-line
    entity: PropTypes.string.isRequired,
    intervalType: PropTypes.string,
    colors: PropTypes.array,
    labels: PropTypes.object,
    onLineClick: PropTypes.func
};

const defaultProps = {
    data: {}, // eslint-disable-line
    colors: ['#00C5FF'],
    type: 'single', // eslint-disable-line
    entity: 'tweets', // eslint-disable-line
    intervalType: 'month',
    onLineClick: null,
    labels: {
        x: '',
        y: '',
    }
};

class Lines extends React.Component {
    constructor(props) {
        super(props);
        this.renderLine = this.renderLine.bind(this);
        this.setData = this.setData.bind(this);
        this.state = {
            datum: [],
            hashtags: [],
            keywords: [],
            mentions: []
        };
    }

    componentDidMount() {
        this.setData(this.props);
    }

    componentWillReceiveProps(nextProps) {
        this.setData(nextProps);
    }

    setData(props) {
        const { type, data, entity } = props;
        const types = (ty, da) => {
            const t = {
                multiple: () => {
                    let datum;
                    if (entity === 'mentions') {
                        let bu = [];
                        if (da.aggregations.top_mentions) {
                            bu = da.aggregations.top_mentions.buckets;
                        } else if (da.aggregations.mentions) {
                            bu = da.aggregations.mentions.buckets;
                        }
                        const newBuckets = balanceJson(bu, entity);
                        datum = mapDataHistogram(newBuckets, entity);
                    } else if (entity === 'users') {
                        let bu = [];
                        if (da.aggregations.top_users) {
                            bu = da.aggregations.top_users.buckets;
                        } else if (da.aggregations.mentions) {
                            bu = da.aggregations.users.buckets;
                        }
                        const newBuckets = balanceJson(bu, entity);
                        datum = mapDataHistogram(newBuckets, entity);
                    } else if (entity === 'keywords') {
                        const { aggregations: { keywords: { buckets } } } = da;
                        const newBuckets = balanceJson(buckets, entity);
                        datum = mapDataHistogram(newBuckets, entity);
                    } else {
                        const { aggregations: { hashtags: { buckets } } } = da;
                        const newBuckets = balanceJson(buckets, entity);
                        datum = mapDataHistogram(newBuckets, entity);
                    }

                    const formatTime = datum.data.map(item => (
                        Object.assign(item, {
                            // key: Date.parse(moment.utc(item.key).utcOffset(n))
                            key: item.key
                        })
                    ));

                    const sorted = formatTime.sort((a, b) => (
                        a.key - b.key
                    ));

                    this.setState({
                        [`${entity}`]: datum[`${entity}`],
                        datum: sorted
                    });
                },
                single: () => {
                    const { aggregations: { buckets } } = da;
                    this.setState({
                        [`${entity}`]: [],
                        datum: buckets
                    });
                }
            };
            return t[ty]();
        };
        types(type, data);
    }

    renderLine() {
        const { colors, entity, onLineClick, hasClick } = this.props;
        const onClick = isNull(onLineClick) ? () => ({}) : onLineClick;
        const cursor = hasClick ? 'pointer' : 'default';
        if (this.state[`${entity}`].length > 0) {
            return this.state[`${entity}`].map((entry, index) => (
                <Line
                    key={index}
                    type="linear"
                    strokeWidth={2}
                    activeDot={{ onClick, cursor }}
                    dataKey={entry[`${entity}`]}
                    dot={{ strokeWidth: 2, r: 2 }}
                    stroke={`rgba(${colors[index].r}, ${colors[index].g}, ${colors[index].b}, 1)`}
                />
            ));
        }
        return (
            <Line
                type="linear"
                strokeWidth={2}
                dataKey={entity}
                stroke={colors[0]}
                activeDot={{ onClick, cursor }}
                dot={{ strokeWidth: 2, r: 2 }}
            />
        );
    }

    renderLineBasic() {
        if (this.state.datum.length > 0) {
            return (
                <ResponsiveContainer>
                    <LineChart
                        data={this.state.datum}
                    >
                        <XAxis
                            dataKey="key"
                            tickLine={false}
                            stroke="rgb(204, 204, 204)"
                            tick={<AxisX color="rgba(0, 0, 0, 0.8)" intervalType={this.props.intervalType} />}
                        >
                            <Label className="line-data__label" value={this.props.labels.x} offset={-15} position="insideBottom" />
                        </XAxis>
                        <YAxis
                            axisLine={false}
                            tick={<AxisY color="rgba(0, 0, 0, 0.8)" />}
                        >
                            <Label className="line-data__label" value={this.props.labels.y} offset={-15} position="left" angle={-90} />
                        </YAxis>
                        <CartesianGrid vertical={false} />
                        <Tooltip
                            content={
                                <TooltipCustom
                                    intervalType={this.props.intervalType === 'hour' ? 'hourPDay' : this.props.intervalType}
                                />
                            }
                        />
                        <Legend height={36} content={<LegendCustom onClick={this.props.onLegendClick} />} />
                        { this.renderLine() }
                    </LineChart>
                </ResponsiveContainer>
            );
        }
        return <div className="chart-line-progress" />;
    }

    render() {
        return (
            <Fragment>
                <div className="chart-line">
                    {this.renderLineBasic()}
                </div>
                {(() => {
                    if (!isNull(this.props.onLineClick)) {
                        return <DrawerFeed />;
                    }
                    return null;
                })()}
            </Fragment>
        );
    }
}


Lines.propTypes = propTypes;
Lines.defaultProps = defaultProps;

export default Lines;
