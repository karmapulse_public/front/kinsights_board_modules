import range from 'lodash/range';

const typeHistogram = (currentData, entity) => {
    const array = range(currentData[0][`${entity}_by_time`].buckets.length);

    array.forEach(i => (array[i] = {}));

    currentData.map(i => (i[`${entity}_by_time`].buckets.map((e, index) => {
        array[index][i.key] = e.doc_count;
        array[index].key = e.key;
        return array;
    })));

    return array;
};

const mapDataHistogram = (currentData, entity) => ({
    [`${entity}`]: (() => currentData.map(i => ({ [`${entity}`]: i.key })))(),
    data: typeHistogram(currentData, entity)
});

export default mapDataHistogram;
