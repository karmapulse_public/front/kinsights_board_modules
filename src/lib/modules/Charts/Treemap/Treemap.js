import React from 'react';
import { Treemap as TreemapRe, Tooltip, ResponsiveContainer } from 'recharts';
import CustomizedContent from './CustomizedContent';
import {
    propTypes,
    defaultProps
} from './defaultValues';

const NewToolTip = (props) => {
    const payload = (props.payload.length >= 1) ? props.payload[0].payload : { name: '', value: '' };
    return (
        <div className="react-chartsjs__tooltip" style={{ position: 'relative' }}>
            {payload.name} : {payload.value}
        </div>
    );
};

const Treemap = props => (
    <ResponsiveContainer width={props.width} height={props.height}>
        <TreemapRe
            data={props.data}
            dataKey={props.dataKey}
            ratio={1 / 3}
            stroke={props.stroke}
            fill={props.fill}
            fontFamily={props.fontFamily}
            fontSize={props.fontSize}
            content={
                <CustomizedContent
                    colors={props.colors}
                    onDataClick={props.onDataClick}
                />}
        >
            {
                props.enableTooltip
                    ? <Tooltip content={
                        props.tooltip ? props.tooltip : <NewToolTip />
                    }
                    />
                    : null
            }
        </TreemapRe>
    </ResponsiveContainer>
);

Treemap.propTypes = propTypes;
Treemap.defaultProps = defaultProps;

export default Treemap;
