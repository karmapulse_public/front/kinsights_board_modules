import PropTypes from 'prop-types';
import * as d3Array from 'd3-array';
import * as d3Scale from 'd3-scale';
import * as d3Selection from 'd3-selection';
import * as d3SelectionMulti from 'd3-selection-multi';
import * as d3ScaleChromatic from 'd3-scale-chromatic';

export const D3 = {
    ...d3Array,
    ...d3Scale,
    ...d3Selection,
    ...d3SelectionMulti,
    ...d3ScaleChromatic
};

export const MIN_HEIGHT = 400;
export const MIN_WIDTH = 600;

export const propTypes = {
    data: PropTypes.array.isRequired,
    dataCountKey: PropTypes.string,
    dataKey: PropTypes.string.isRequired,
    fontSize: PropTypes.number,
    fontFamily: PropTypes.string,
    stroke: PropTypes.string,
    fill: PropTypes.string,
    enableTooltip: PropTypes.bool,
    tooltip: PropTypes.func,
    height: PropTypes.number,
    width: PropTypes.number,
    onDataClick: PropTypes.func
};

export const defaultProps = {
    fontFamily: 'impact',
    fontSize: 20,
    stroke: '#fff',
    fill: '#fff',
    onDataClick: (() => ({})),
    height: MIN_HEIGHT,
    width: MIN_WIDTH,
    enableTooltip: false,
};
