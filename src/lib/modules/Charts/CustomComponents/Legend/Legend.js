import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';

const propTypes = {
    payload: PropTypes.array,
    nameArray: PropTypes.array,
    align: PropTypes.string,
    styles: PropTypes.object,
};

const defaultProps = {
    payload: [],
    nameArray: [],
    align: 'left',
    styles: {
        justifyContent: 'flex-start',
        fontSize: 12,
        fontWeight: 300,
        textTransform: 'uppercase'
    },
};

const Legend = ({ payload, nameArray, align, styles, onClick }) => (
    <ul
        className="legend-entry"
        style={{
            textAlign: align,
            display: 'flex',
            flexWrap: 'wrap',
            alignItems: 'center',
            margin: ' 10px 35px',
            justifyContent: styles.justifyContent
        }}
    >
        {
            payload.map((entry, index) => (
                <li
                    style={{
                        margin: '5px 10px',
                        fontSize: styles.fontSize,
                        textTransform: styles.textTransform,
                        fontWeight: styles.fontWeight,
                        display: 'flex',
                        justifyContent: 'center',
                    }}
                    key={`item-${index}`}
                    onClick={() => onClick(isEmpty(nameArray) ? entry.value : nameArray[index])}
                >
                    <div
                        style={{
                            width: 11,
                            height: 11,
                            marginRight: 10,
                            display: 'inline-block',
                            borderRadius: '50%',
                            border: `1px solid ${entry.color}`,
                            backgroundColor: `${entry.color.replace('1)', '0.2)')}`
                        }}
                    />
                    {
                        `${isEmpty(nameArray) ? entry.value : nameArray[index]}`
                    }
                </li>
            ))
        }
    </ul>
);

Legend.propTypes = propTypes;
Legend.defaultProps = defaultProps;
export default Legend;
