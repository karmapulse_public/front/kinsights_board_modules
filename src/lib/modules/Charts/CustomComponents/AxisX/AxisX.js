import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

const propTypes = {
    payload: PropTypes.object,
    x: PropTypes.number,
    y: PropTypes.number,
    style: PropTypes.object,
    color: PropTypes.string,
    noUtc: PropTypes.bool,
    intervalType: PropTypes.string
};

const defaultProps = {
    payload: {},
    x: 0,
    y: 0,
    style: {},
    color: '#666',
    noUtc: false,
    intervalType: 'day'
};

const renderDatesByType = (intervalType, date) => {
    const component = {
        hour: () => `${date.format('LT')}`,
        day: () => `${date.format('MMM DD')}`,
        week: () => `${date.format('MMM DD')}`,
        month: () => `${date.format('MMM YYYY')}`,
        year: () => `${date.format('YYYY')}`,
    };

    return component[intervalType]();
};

const CustomAxisXTick = ({ payload, x, y, color, style, noUtc, intervalType }) => {
    const value = moment(new Date(payload.value));

    const typeValue = () => {
        if (value.isValid()) {
            if (noUtc) {
                return renderDatesByType(intervalType, value);
            }
            return renderDatesByType(intervalType, value.utc());
        } return payload.value;
    };

    return (
        <text
            x={x}
            y={y}
            dy={15}
            textAnchor="middle"
            fill={color}
            style={{ textTransform: 'uppercase', ...style }}
        >
            {
                typeValue()
            }
        </text>
    );
};

CustomAxisXTick.propTypes = propTypes;
CustomAxisXTick.defaultProps = defaultProps;

export default CustomAxisXTick;
