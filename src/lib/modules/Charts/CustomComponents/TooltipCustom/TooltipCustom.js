import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import isUndefined from 'lodash/isUndefined';
import upperFirst from 'lodash/upperFirst';
import { numberWithCommas } from '../../../../helpers/number';

moment.locale('es');

const propTypes = {
    payload: PropTypes.array,
    noUtc: PropTypes.bool,
    intervalType: PropTypes.string
};

const defaultProps = {
    payload: [],
    noUtc: false,
    intervalType: 'month'
};

const reactions = {
    angry: 'Me enfada',
    haha: 'Me divierte',
    like: 'Me gusta',
    love: 'Me encanta',
    sad: 'Me entristece',
    wow: 'Me asombra',
    comments: 'Comentarios',
    shares: 'Shares'
};

const renderDates = (intervalType, date) => {
    const component = {
        hour: () => (
            <p>Hora: {date.format('LT')}</p>
        ),
        hourPDay: () => (
            <div>
                <p>Fecha: {upperFirst(date.format('MMM DD, YYYY'))}</p>
                <p>Hora: {date.format('hh:mm A')}</p>
            </div>
        ),
        day: () => (
            <p>Día: {date.format('MMM DD, YYYY')}</p>
        ),
        week: () => (
            <p>Semana: {date.format('MMM DD, YYYY')} - {date.add(1, 'w').format('MMM DD, YYYY')}</p>
        ),
        month: () => (
            <p>Mes: {date.format('MMM YYYY')}</p>
        ),
        year: () => (
            <p>Año: {date.format('YYYY')}</p>
        ),
    };

    return component[intervalType]();
};

// eslint-disable-next-line
const TooltipCustom = ({ payload, label, noUtc, intervalType }) => {
    let dateDiv = null;
    if (!isUndefined(label)) {
        const day = moment(new Date(label));
        if (day.isValid()) {
            if (noUtc) {
                dateDiv = renderDates(intervalType, day);
            } else {
                dateDiv = renderDates(intervalType, day.utc());
            }
        } else {
            dateDiv = <p>Tipo de post: {label}</p>;
        }
    }

    const labels = (name) => {
        if (name === 'doc_count') {
            return 'Total';
        } return isUndefined(reactions[name]) ? name : reactions[name];
    };

    const renderTotals = payload.map((i, index) => (
        <li key={index} style={{ color: i.color }}>
            {labels(upperFirst(i.name))}: {numberWithCommas(i.value)}
        </li>
    ));
    return (
        <div className="tooltip_custom">
            <ul>
                {renderTotals}
            </ul>
            {dateDiv}
        </div>
    );
};

TooltipCustom.propTypes = propTypes;
TooltipCustom.defaultProps = defaultProps;

export default TooltipCustom;
