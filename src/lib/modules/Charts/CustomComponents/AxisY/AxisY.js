import React from 'react';
import PropTypes from 'prop-types';
// import moment from 'moment';
import { nFormatter } from '../../../../helpers/number';


const propTypes = {
    payload: PropTypes.object,
    style: PropTypes.object,
    x: PropTypes.number,
    y: PropTypes.number,
    color: PropTypes.string,
};

const defaultProps = {
    payload: {},
    style: {
        fontSize: 14
    },
    x: 0,
    y: 0,
    color: '#666',
};

const CustomAxisYTick = ({ payload, x, y, color, style }) => {
    const value = nFormatter(payload.value);
    return (
        <text
            x={x}
            y={y}
            dy={5}
            textAnchor="end"
            fill={color}
            style={{ ...style }}
        >
            {value}
        </text>
    );
};

CustomAxisYTick.propTypes = propTypes;
CustomAxisYTick.defaultProps = defaultProps;

export default CustomAxisYTick;
