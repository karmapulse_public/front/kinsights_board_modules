import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import { numberWithCommas } from '../../../../helpers/number';
import { styles } from './styles';

moment.locale('es');

const propTypes = {
    payload: PropTypes.array,
};

const defaultProps = {
    payload: [],
};


// eslint-disable-next-line
const TooltipCustomYears = ({ payload }) => {
    const values = payload === null ? [] : payload;
    const renderTotals = values.map((i, index) => (
        <div key={index} className="container">
            <div className="data">
                <p>Año</p> <p> {moment(i.payload.date).format('YYYY')} </p>
            </div>
            <div className="data">
                <p> Volumen </p> <p>{numberWithCommas(i.value)} </p>
            </div>
        </div>
    ));
    return (
        <div {...styles()}>
            {renderTotals}
        </div>
    );
};

TooltipCustomYears.propTypes = propTypes;
TooltipCustomYears.defaultProps = defaultProps;

export default TooltipCustomYears;
