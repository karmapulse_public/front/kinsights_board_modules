import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import isNull from 'lodash/isNull';
import orderBy from 'lodash/orderBy';
import isUndefined from 'lodash/isUndefined';
import {
    BarChart,
    Bar,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    ResponsiveContainer,
    Label,
    Cell
} from 'recharts';

import AxisX from '../CustomComponents/AxisX';
import AxisY from '../CustomComponents/AxisY';
import LegendCustom from '../CustomComponents/Legend';
import TooltipCustom from '../CustomComponents/TooltipCustom';
import DrawerFeed from '../../DrawerFeed';

const propTypes = {
    data: PropTypes.array,
    dataKeys: PropTypes.array,
    type: PropTypes.string,
    labels: PropTypes.object,
    layout: PropTypes.string,
    nameArray: PropTypes.array,
    onBarClick: PropTypes.func,
    xDataKey: PropTypes.string,
    yDataKey: PropTypes.string,
    intervalType: PropTypes.string,
    customTooltip: PropTypes.element,
    customLegend: PropTypes.element,
    customAxisX: PropTypes.element
};

const defaultProps = {
    data: [],
    dataKeys: {},
    xDataKey: '',
    yDataKey: '',
    nameArray: [],
    type: 'single',
    layout: 'horizontal',
    intervalType: 'month',
    onBarClick: null,
    customTooltip: null,
    customLegend: null,
    customAxisX: null,
    labels: {
        x: '',
        y: ''
    }
};

const ChartColumns = ({
    data, dataKeys, nameArray, type, intervalType, labels, onBarClick,
    xDataKey, yDataKey, customTooltip, customLegend, customAxisX, layout
}) => {
    let XDataKey = '';
    if (xDataKey === '' && type === 'single') XDataKey = 'date';
    else if (xDataKey === '' && type !== 'single') XDataKey = 'denomitation';
    else XDataKey = xDataKey;

    const renderBarForTypes = () => {
        const types = {
            single: (() => dataKeys.map((item, index) => (
                <Bar
                    key={index}
                    stackId="a"
                    fill={item.color}
                    dataKey={item.key}
                />
            )))(),
            multiple: (() => dataKeys.map((item, index) => {
                if (isUndefined(item.group)) {
                    return (
                        <Bar
                            key={index}
                            fill={item.color}
                            dataKey={item.key}
                        />
                    );
                }
                return (
                    <Bar
                        key={index}
                        fill={item.color}
                        dataKey={item.key}
                        stackId={item.group}
                    />
                );
            }))(),
            simple: (() => (
                <Bar dataKey={yDataKey}>
                    {dataKeys.map(item => (
                        <Cell key={item.key} fill={item.color} />
                    ))}
                </Bar>
            ))()
        };
        return types[type];
    };
    const onClick = isNull(onBarClick) ? () => ({}) : onBarClick;

    const contentTooltip = isNull(customTooltip) ?
        (<TooltipCustom
            nameArray={nameArray}
            intervalType={intervalType === 'hour' ? 'hourPDay' : intervalType}
        />) : customTooltip;

    const contentLegend = isNull(customLegend) ?
        <LegendCustom /> : customLegend;

    const tickAxisX = isNull(customAxisX) ?
        (<AxisX
            color="rgba(0, 0, 0, 0.8)"
            intervalType={intervalType}
            style={{
                fontSize: 10,
                fontWeight: 600,
                strokeWidth: '0.05px',
                stroke: 'rgba(0, 0, 0, 0.8)',
            }}
        />) : customAxisX;

    if (layout === 'vertical') {
        return (
            <Fragment>
                <ResponsiveContainer width="100%" height="100%" minWidth={300} minHeight={300}>
                    <BarChart
                        layout="vertical"
                        data={orderBy(data, ['date'], ['asc'])}
                        onClick={dataClick => onClick(dataClick)}
                    >
                        <XAxis
                            type="number"
                            tickLine={false}
                            axisLine={false}
                            dataKey={yDataKey}
                            tick={
                                <AxisY
                                    color="rgba(0, 0, 0, 0.8)"
                                    style={{ fontSize: 10, fontWeight: 600, stroke: 'rgba(0, 0, 0, 0.8)', strokeWidth: '0.05px' }}
                                />
                            }
                        >
                            <Label className="column-data__label" value={labels.y} offset={-15} position="insideBottom" />
                        </XAxis>
                        <YAxis
                            type="category"
                            tickLine={false}
                            tickMargin={25}
                            tick={tickAxisX}
                            dataKey={XDataKey}
                            interval="preserveStartEnd"
                            stroke="rgb(204, 204, 204)"
                        >
                            <Label className="column-data__label" value={labels.x} offset={-15} position="left" angle={-90} />
                        </YAxis>
                        <CartesianGrid horizontal={false} />
                        <Tooltip
                            content={contentTooltip}
                            cursor={{ fill: '#000', fillOpacity: 0.05 }}
                        />
                        <Legend
                            height={36}
                            nameArray={nameArray}
                            content={contentLegend}
                            styles={{
                                fontSize: 10,
                                fontWeight: 600,
                                justifyContent: 'center',
                                textTransform: 'capitalize',
                            }}
                        />
                        {renderBarForTypes()}
                    </BarChart>
                </ResponsiveContainer>
                {(() => {
                    if (!isNull(onBarClick)) {
                        return <DrawerFeed />;
                    }
                    return null;
                })()}
            </Fragment>
        );
    }

    return (
        <Fragment>
            <ResponsiveContainer width="100%" height="100%" minWidth={300} minHeight={300}>
                <BarChart
                    data={orderBy(data, ['date'], ['asc'])}
                    onClick={dataClick => onClick(dataClick)}
                >
                    <XAxis
                        type="category"
                        tickLine={false}
                        tick={tickAxisX}
                        dataKey={XDataKey}
                        interval="preserveStartEnd"
                        stroke="rgb(204, 204, 204)"
                    >
                        <Label className="column-data__label" value={labels.x} offset={-15} position="insideBottom" />
                    </XAxis>
                    <YAxis
                        axisLine={false}
                        tickLine={false}
                        tick={
                            <AxisY
                                color="rgba(0, 0, 0, 0.8)"
                                style={{ fontSize: 10, fontWeight: 600, stroke: 'rgba(0, 0, 0, 0.8)', strokeWidth: '0.05px' }}
                            />
                        }
                    >
                        <Label className="column-data__label" value={labels.y} offset={-15} position="left" angle={-90} />
                    </YAxis>
                    <CartesianGrid vertical={false} />
                    <Tooltip
                        content={contentTooltip}
                        cursor={{ fill: '#000', fillOpacity: 0.05 }}
                    />
                    <Legend
                        height={36}
                        nameArray={nameArray}
                        content={contentLegend}
                        styles={{
                            fontSize: 10,
                            fontWeight: 600,
                            justifyContent: 'center',
                            textTransform: 'capitalize'
                        }}
                    />
                    {renderBarForTypes()}
                </BarChart>
            </ResponsiveContainer>
            {(() => {
                if (!isNull(onBarClick)) {
                    return <DrawerFeed />;
                }
                return null;
            })()}
        </Fragment>
    );
};

ChartColumns.propTypes = propTypes;
ChartColumns.defaultProps = defaultProps;

export default ChartColumns;
