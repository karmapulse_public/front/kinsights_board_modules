import React, { Component } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';

import DrawerFeed from './DrawerFeed';
import DrawerContext from '../helpers/context/ctxDrawer';

export default (WrappedComponent) => {
    class HOCDrawerTreemap extends Component {
        constructor(props) {
            super(props);
            this.handleClickEvent = this.handleClickEvent.bind(this);
            this.handleCloseDrawer = this.handleCloseDrawer.bind(this);
            this.initialState = {
                openDrawer: false,
                drawerData: {
                    type: 'TWITTER',
                    service: '',
                    title: '',
                    subtitleLeft: '',
                    subtitleRight: '',
                    queryData: {}
                }
            };
            this.state = this.initialState;
        }

        handleCloseDrawer() {
            this.setState({ ...this.initialState });
        }

        handleClickEvent(queryConfig, titleConfig) {
            const {
                moduleRange, moduleConfig, moduleServices
            } = this.props;
            if (!moduleConfig.hasClick) return;
            const queryData = {
                'recipe-id': 'module_tw_explore_tweets',
                'rule-id': moduleConfig.search_id,
                'initial-date': moment(moduleRange.startDate).format(),
                'final-date': moment(moduleRange.endDate).format(),
                ...queryConfig,
                filters: {
                    limit: 10,
                    ...queryConfig.filters
                },
            };
            this.setState({
                openDrawer: true,
                drawerData: {
                    type: 'TWITTER',
                    service: moduleServices.twitter,
                    queryData,
                    ...titleConfig,
                }
            });
        }

        render() {
            return (
                <DrawerContext.Provider
                    value={{
                        open: this.state.openDrawer,
                        drawerData: this.state.drawerData,
                        onCloseDrawer: this.handleCloseDrawer
                    }}
                >
                    <WrappedComponent
                        {...this.props}
                        onClick={this.handleClickEvent}
                    />
                    {!this.props.moduleConfig.hasClick ? null : <DrawerFeed />}
                </DrawerContext.Provider>
            );
        }
    }

    HOCDrawerTreemap.propTypes = {
        moduleColor: PropTypes.string,
        intervalType: PropTypes.string,
        moduleData: PropTypes.shape({}),
        moduleRange: PropTypes.shape({}),
        moduleConfig: PropTypes.shape({}),
        moduleServices: PropTypes.shape({})
    };

    HOCDrawerTreemap.defaultProps = {
        moduleConfig: {},
        moduleData: {},
        moduleRange: {},
        moduleServices: {},
        moduleColor: '#666',
        intervalType: 'month'
    };

    return HOCDrawerTreemap;
};
