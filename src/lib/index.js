import mapModuleType, { renderComponentByPosition, renderComponents } from './mapModuleType';

export { mapModuleType as default, renderComponentByPosition, renderComponents };
