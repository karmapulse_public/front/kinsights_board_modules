import React from 'react';
import find from 'lodash/find';
import UnassignedModule from './UnassignedModule';
import ModuleNotAvailable from './ModuleNotAvailable';
import uID from './helpers/uniqueId';
import withExport from './helpers/withExportPDF';

import TotalTweets from './modules/TotalTweets';
import TotalSentiment from './modules/TotalSentiment';
import TotalSentimentAuthor from './modules/TotalSentimentAuthor';
import TopHashtags from './modules/TopHashtags';
import TopUsers from './modules/TopUsers';
import TotalAudience from './modules/TotalAudience';
import TopImages from './modules/TopImages';
import ProfileSelector from './modules/ProfileSelector';
import TimelineTrends from './modules/TimelineTrends';
import TopTrends from './modules/TopTrends';
import TopTrendsNew from './modules/TopTrendsNew';
import TreeMapTrends from './modules/TreeMapTrends';
import Mapa from './modules/Mapa';
import TwitterFeed from './modules/TwitterFeed';
import SingleImage from './modules/SingleImage';
import MarkdownText from './modules/MarkdownText';
import FbPostFrequency from './modules/FbPostFrequency';
import FbPerformanceByType from './modules/FbPerformanceByType';
import FbTopEntities from './modules/FbTopEntities';
import FbTopPosts from './modules/FbTopPosts';
import FbFans from './modules/FbFans';
import FbProfile from './modules/FbProfile';
import FbSentiment from './modules/FbSentiment';
import FbInteractions from './modules/FbInteractions';
import FbSearchKeywords from './modules/FbSearchKeywords';
import TopTweets from './modules/TopTweets';
import TopInteraction from './modules/TopInteraction';
import TopOriginalInteraction from './modules/TopOriginalInteraction';
import TopicSight from './modules/TopicSight';
import TimeTrendGrouper from './modules/TimeTrendGrouper';
import IgPostFrequency from './modules/IgPostFrequency';
import IgTotalInteraction from './modules/IgTotalInteraction';
import IgTopImages from './modules/IgTopImages';
import TopTrendingTopics from './modules/TopTrendingTopics';
import TimelineDistribution from './modules/TimelineDistribution';
import GeneralFeed from './modules/GeneralFeed';
import TwitterFeedRemaster from './modules/TwitterFeedRemaster';
import FbTopTopicsComments from './modules/FbTopTopicsComments';
import MultiSourceTimeline from './modules/MultiSourceTimeline';

// Map modules by module type
export default (components) => {
    const modulesAvailable = {
        control_profile_selector: moduleConfig => ({
            component: ProfileSelector,
            moduleConfig
        }),
        module_total_tweets: moduleConfig => ({
            component: TotalTweets,
            moduleConfig
        }),
        module_total_sentiment: moduleConfig => ({
            component: TotalSentiment,
            moduleConfig
        }),
        module_total_sentiment_author: moduleConfig => ({
            component: TotalSentimentAuthor,
            moduleConfig
        }),
        module_hashtags: moduleConfig => ({
            component: TopHashtags,
            moduleConfig
        }),
        module_total_audience: moduleConfig => ({
            component: TotalAudience,
            moduleConfig
        }),
        module_shared_content: moduleConfig => ({
            component: TopImages,
            moduleConfig
        }),
        module_timeline_trends: moduleConfig => ({
            component: TimelineTrends,
            moduleConfig
        }),
        module_top_trends: moduleConfig => ({
            component: TopTrends,
            moduleConfig
        }),
        module_top_user: moduleConfig => ({
            component: TopUsers,
            moduleConfig
        }),
        module_top_trends_new: moduleConfig => ({
            component: TopTrendsNew,
            moduleConfig
        }),
        module_twitter_feed: moduleConfig => ({
            component: TwitterFeed,
            moduleConfig
        }),
        module_single_image: moduleConfig => ({
            component: SingleImage,
            moduleConfig
        }),
        module_markdown: moduleConfig => ({
            component: MarkdownText,
            moduleConfig
        }),
        module_fb_post_freq: moduleConfig => ({
            component: FbPostFrequency,
            moduleConfig
        }),
        module_fb_performance_by_type: moduleConfig => ({
            component: FbPerformanceByType,
            moduleConfig
        }),
        module_fb_top_entities: moduleConfig => ({
            component: FbTopEntities,
            moduleConfig
        }),
        module_fb_top_posts: moduleConfig => ({
            component: FbTopPosts,
            moduleConfig
        }),
        module_fb_fans: moduleConfig => ({
            component: FbFans,
            moduleConfig
        }),
        module_fb_profile: moduleConfig => ({
            component: FbProfile,
            moduleConfig
        }),
        module_fb_sentiment_comments: moduleConfig => ({
            component: FbSentiment,
            moduleConfig
        }),
        module_fb_total_interactions: moduleConfig => ({
            component: FbInteractions,
            moduleConfig
        }),
        mdulo_fb_search_keywords: moduleConfig => ({
            component: FbSearchKeywords,
            moduleConfig
        }),
        module_total_interaction: moduleConfig => ({
            component: TopInteraction,
            moduleConfig
        }),
        module_total_interaction_r: moduleConfig => ({
            component: TopOriginalInteraction,
            moduleConfig
        }),
        module_treemap_trends: moduleConfig => ({
            component: TreeMapTrends,
            moduleConfig
        }),
        module_map: moduleConfig => ({
            component: Mapa,
            moduleConfig
        }),
        module_topic_sight: moduleConfig => ({
            component: TopicSight,
            moduleConfig
        }),
        module_topic_grouper: moduleConfig => ({
            component: TimeTrendGrouper,
            moduleConfig
        }),
        module_ig_post_freq: moduleConfig => ({
            component: IgPostFrequency,
            moduleConfig
        }),
        module_ig_total_interaction: moduleConfig => ({
            component: IgTotalInteraction,
            moduleConfig
        }),
        module_ig_top_posts: moduleConfig => ({
            component: IgTopImages,
            moduleConfig
        }),
        top_trending_topics: moduleConfig => ({
            component: TopTrendingTopics,
            moduleConfig
        }),
        module_timeline_distribution: moduleConfig => ({
            component: TimelineDistribution,
            moduleConfig
        }),
        module_general_feed: moduleConfig => ({
            component: GeneralFeed,
            moduleConfig
        }),
        module_twitter_feed_remaster: moduleConfig => ({
            component: TwitterFeedRemaster,
            moduleConfig
        }),
        module_fb_tt_comments: moduleConfig => ({
            component: FbTopTopicsComments,
            moduleConfig
        }),
        module_multisource_timeline: moduleConfig => ({
            component: MultiSourceTimeline,
            moduleConfig
        }),
        module_top_tweets: moduleConfig => ({
            component: TopTweets,
            moduleConfig
        }),
    };

    const componentsMapped = components.map((component, index) => {
        const componentType = component.sys ? component.sys.contentType.sys.id : null;

        if (componentType === null) {
            return {
                position: index,
                component: UnassignedModule
            };
        } else if (modulesAvailable[componentType]) {
            return {
                position: component.fields.position || index,
                component: modulesAvailable[componentType](component)
            };
        }
        return {
            position: component.position,
            component: ModuleNotAvailable
        };
    });

    return componentsMapped;
};

// Render module in specific position
export const renderComponentByPosition = (mapModules, position, params = {}) => {
    const module = find(mapModules, {
        position
    });

    if (module.component.component) {
        return withExport(<module.component.component />, {
            ...module.component.moduleConfig,
            ...params
        });
    }

    return (<UnassignedModule />);
};

export const renderComponents = (mapModules, params = {}) => (
    mapModules.map((module) => {
        if (module.component.component) {
            return withExport(<module.component.component />, {
                key: uID(),
                ...module.component.moduleConfig,
                ...params
            });
        }
        return (<UnassignedModule />);
    })
);
