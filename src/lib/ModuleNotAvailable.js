import React from 'react';
import { css } from 'glamor';

const styles = css({
    minWidth: 300,
    height: 150,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    color: '#707070',

    ' h1:last-child': {
        fontSize: 12,
        fontWeight: 700,
        color: '#AE9BD6'
    }
});

const ModuleNotAvailable = () => (
    <div className="unassigned-module" {...styles}>
        <h1>Módulo no disponible</h1>
    </div>
);

export default ModuleNotAvailable;
