import moment from 'moment';

const getTimeInterval = (startDate, finishDate) => {
    let blockType = null;

    const unixFormatStartDate = Date.parse(new Date(startDate));
    const unixFormatFinishDate = Date.parse(new Date(finishDate));

    const minutes = 1000 * 60;
    const hours = minutes * 60;
    const days = hours * 24;
    const months = days * 30;

    const timeRange = unixFormatFinishDate - unixFormatStartDate;

    const min = Math.round(timeRange / minutes);
    const hrs = Math.round(timeRange / hours);
    const d = Math.round(timeRange / days);
    const m = Math.round(timeRange / months);

    if (d >= 180) {
        blockType = 'month';
    } else if (d >= 32) {
        blockType = 'week';
    } else if (d > 1) {
        blockType = 'day';
    } else if (hrs === 1) {
        blockType = '1m';
    } else {
        blockType = 'hour';
    }

    return blockType;
};

export default getTimeInterval;

export const calculateNewInterval = (startFilter, finishFilter, component) => {
    const width = component.current.offsetWidth;
    const dateDiff = finishFilter.utc().diff(startFilter.utc(), 'days', true);

    if (width <= 550) {
        if (dateDiff <= 2) {
            return 'hour';
        } else if (dateDiff <= 30) {
            return 'day';
        } else if (dateDiff <= 273) {
            return 'week';
        }
        return 'month';
    }
    if (dateDiff <= 15) {
        return 'hour';
    } else if (dateDiff <= 365) {
        return 'day';
    } else if (dateDiff <= 2555) {
        return 'week';
    }
    return 'month';
};
