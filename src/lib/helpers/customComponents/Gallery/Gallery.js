import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import isUndefined from 'lodash/isUndefined';
import styles from './GalleryStyles';

const propTypes = {
    items: PropTypes.array,
    width: PropTypes.string,
    height: PropTypes.string,
    type: PropTypes.string,
    url: PropTypes.string,
    video: PropTypes.bool,
    gif: PropTypes.bool,
};

const defaultProps = {
    items: [],
    width: 'auto',
    height: '100%',
    type: '',
    url: '',
    video: false,
    gif: false
};

const Gallery = (props) => {
    const images = () => (
        props.items.slice(0, 4).map((e, i) => (
            <div
                key={i}
                className={`item ${(props.items.length > 4 && i === 3) ? 'item-more' : ''}`}
                style={{ backgroundImage: `url(${isUndefined(e.url) ? e.media_url.replace('http:', 'https:') : e.url.replace('http:', 'https:')}), url(https://cdn.karmapulse.com/noData-Feeds.svg)` }}
            />
        ))
    );

    const galeryItems = () => {
        if (!isEmpty(props.items)) {
            const n = props.items.length;
            return (
                <a
                    className="custom-gallery"
                    href={props.url}
                    target="_blank"
                    rel="noopener noreferrer"
                    {...styles(n, props.type, props.width, props.height, props.gif, props.video)}
                >
                    { images() }
                </a>
            );
        }
        return '';
    };

    return galeryItems();
};

Gallery.propTypes = propTypes;
Gallery.defaultProps = defaultProps;

export default Gallery;
