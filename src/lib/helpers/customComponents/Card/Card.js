import React, { Children } from 'react';
import PropTypes from 'prop-types';

import styles from './CardStyles';

const propTypes = {
    children: PropTypes.array,
    backgroundImage: PropTypes.string,
    wTextDynamic: PropTypes.bool,
    isScreen: PropTypes.bool,
    link: PropTypes.string,

};

const defaultProps = {
    children: [],
    backgroundImage: '',
    link: '',
    wTextDynamic: false,
    isScreen: false
};

const Card = ({ children, backgroundImage, wTextDynamic, isScreen, link
}) => {
    let classBody = '';
    if (wTextDynamic === true) {
        if (children[2] === null) {
            classBody = 'complete';
        } else {
            classBody = 'partial';
        }
    }
    if (isScreen) {
        return (
            <section {...styles(backgroundImage, classBody)}>
                <a href={link} target="_blank" rel="noreferrer">
                    {children}
                </a>
            </section>
        );
    }
    return (
        <section {...styles(backgroundImage, classBody)}>
            {children}
        </section>
    );
};


Card.propTypes = propTypes;
Card.defaultProps = defaultProps;

export default Card;
