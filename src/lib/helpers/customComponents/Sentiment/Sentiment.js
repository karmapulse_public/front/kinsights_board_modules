import React from 'react';
import PropTypes from 'prop-types';

import styles from './SentimentStyles';

const propTypes = {
    item: PropTypes.object,
    noSource: PropTypes.bool,
};

const defaultProps = {
    item: {},
    noSource: false
};

const Sentiment = (props) => {
    const { item: { classification_sentiment }, noSource } = props;
    if (!noSource || classification_sentiment === undefined) return null;

    const colors = {
        positivo: '#63d12f',
        negativo: '#ea3c3c',
        neutral: '#89a7ce'
    };

    return (
        <div {...styles(colors[classification_sentiment])}>
            <span>{classification_sentiment}</span>
        </div>
    );
};


Sentiment.propTypes = propTypes;
Sentiment.defaultProps = defaultProps;

export default Sentiment;
