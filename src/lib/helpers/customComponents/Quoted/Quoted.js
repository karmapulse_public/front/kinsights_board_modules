import React from 'react';
import PropTypes from 'prop-types';
import Typography from 'kpulse_design_system/dist/Typography';
import Gallery from '../Gallery';


import styles from './QuotedStyles';

const propTypes = {
    width: PropTypes.string,
    height: PropTypes.string,
    child_tweet: PropTypes.shape({
        user: PropTypes.shape({
            username: PropTypes.string,
            name: PropTypes.string,
        }),
        link: PropTypes.string,
        body: PropTypes.string,
        images: PropTypes.array
    })
};

const defaultProps = {
    width: 'auto',
    height: '100%',
    child_tweet: {
        user: {
            username: '',
            name: '',
        },
        link: '',
        body: '',
        images: []
    }
};

const stylesText = {
    width: '100%',
    maxHeight: 40,
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap'
};

const Quoted = (props) => {
    const { Paragraph, Title } = Typography;
    const { child_tweet: { user, link, body, images } } = props;
    const padding = images && images.length > 0 ? '15px 10px' : '15px 15px';
    const textClass = images && images.length > 0 ? 'quoted-w-img' : 'quoted-wo-img';
    return (
        <div className="custom-quoted" {...styles(padding, props.width, props.height)}>
            <a href={link} target="_blank" rel="noopener noreferrer">
                {
                    images && images.length > 0 ?
                        // <div
                        //     className="quoted-img"
                        //     style={{ backgroundImage: `url(${props.imageUrl})` }}
                        // /> : null
                        <Gallery items={images} width="74px" height="74px" /> : null
                }
                <div className={textClass}>
                    <Title styles={stylesText}level={6} bold>{user.name}</Title>
                    <Paragraph styles={{ ...stylesText, marginBottom: '10px' }} color="rgba(0,0,0,0.5)">@{user.username}</Paragraph>
                    <Paragraph styles={stylesText}>{body}</Paragraph>
                </div>
            </a>
        </div>
    );
};

Quoted.propTypes = propTypes;
Quoted.defaultProps = defaultProps;

export default Quoted;
