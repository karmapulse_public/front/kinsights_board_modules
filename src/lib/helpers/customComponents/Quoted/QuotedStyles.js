import { css } from 'glamor';

const styles = (padding, generalWidth, generalHeight) => css({
    width: generalWidth,
    height: generalHeight,
    border: 'solid 1px rgba(0, 0, 0, 0.1)',
    padding,
    marginBottom: '10px',
    ' > a': {
        display: 'flex',
        alignItems: 'center',
        textDecoration: 'none'
    },
    ' .custom-gallery': {
        margin: '0px 10px 0px 0px',
    },
    ' .quoted-w-img': {
        width: 'calc(100% - 99px)',
        height: '100%',
        color: '#000',
    },
    ' .quoted-wo-img': {
        width: 'calc(100% - 20px)',
        height: '100%',
        color: '#000',
    },
});

export default styles;
