import { css } from 'glamor';

const s = {
    instagram: '0 15px 0 15px',
    facebook: '0 30px 0 55px',
    twitter: '0 30px 0 55px'
};

const styles = type => css({
    padding: s[type],
    marginBottom: 10,
});
export default styles;
