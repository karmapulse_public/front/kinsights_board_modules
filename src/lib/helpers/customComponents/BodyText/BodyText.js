import React from 'react';
import PropTypes from 'prop-types';
import Typography from 'kpulse_design_system/dist/Typography';

import styles from './BodyTextStyles';

const propTypes = {
    text: PropTypes.string,
    type: PropTypes.string,
};

const defaultProps = {
    text: [],
    type: '',
};

const BodyText = ({ text, type }) => {
    const { Paragraph } = Typography;
    return (
        <Paragraph styles={styles(type)}>
            {text}
        </Paragraph>
    );
};


BodyText.propTypes = propTypes;
BodyText.defaultProps = defaultProps;

export default BodyText;
