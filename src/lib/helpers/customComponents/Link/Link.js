import React from 'react';
import PropTypes from 'prop-types';
import Typography from 'kpulse_design_system/dist/Typography';


import styles from './LinkStyles';

const propTypes = {
    width: PropTypes.string,
    height: PropTypes.string,
    url: PropTypes.string,
    imageUrl: PropTypes.string,
    title: PropTypes.string,
    text: PropTypes.string,
    displayUrl: PropTypes.string,
    type: PropTypes.string,
};

const defaultProps = {
    width: 'auto',
    height: '100%',
    url: '',
    imageUrl: '',
    title: '',
    text: '',
    displayUrl: '',
    type: '',
};

const stylesText = {
    width: 'calc(100% - 30px)',
    maxHeight: 40,
    overflow: 'hidden',
};

const Link = (props) => {
    const { Paragraph, Title } = Typography;
    return (
        <div className="custom-link" {...styles(props.width, props.height, props.type)}>
            <a href={props.url} target="_blank" rel="noopener noreferrer">
                {
                    props.imageUrl && props.imageUrl !== '' ?
                        <div
                            className="link-img"
                            style={{ backgroundImage: `url(${props.imageUrl ? props.imageUrl.replace('http:', 'https:') : ''})` }}
                        /> : null
                }
                <div>
                    <Title styles={stylesText}level={6} bold>{props.title}</Title>
                    <Paragraph styles={stylesText}>{props.text}</Paragraph>
                    <Paragraph styles={stylesText} color="rgba(0,0,0,0.5)">{props.displayUrl}</Paragraph>
                </div>
            </a>
        </div>
    );
};

Link.propTypes = propTypes;
Link.defaultProps = defaultProps;

export default Link;
