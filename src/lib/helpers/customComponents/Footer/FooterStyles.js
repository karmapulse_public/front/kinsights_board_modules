import { css } from 'glamor';

const styles = css({
    width: '100%',
    display: 'flex',
    padding: '0px 15px',
    justifyContent: 'flex-end',
    boxSizing: 'border-box',
    ' .tag': {
        position: 'absolute',
        bottom: 0,
        right: 0,
        padding: '10px',
        ' svg': {
            padding: '0px 5px',
            fontSize: '1.8rem'
        },
    },
    ' .dateTop': {
        position: 'absolute',
        top: 10,
        right: 10,
        // padding: '5px 15px',
        // background: '#000',
    }
});

export default styles;
