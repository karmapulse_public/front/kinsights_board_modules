import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFont, faImage, faVideo } from '@fortawesome/free-solid-svg-icons';

import Typography from 'kpulse_design_system/dist/Typography';
import upperFirst from 'lodash/upperFirst';
import moment from 'moment';

import styles from './FooterStyles';

const propTypes = {
    item: PropTypes.object,
    type: PropTypes.string,
    isScreen: PropTypes.bool
};

const defaultProps = {
    item: {},
    isScreen: false
};

const Header = (props) => {
    const { Paragraph } = Typography;
    const { item, type, isScreen } = props;
    const stringTypeVideo = {
        VIDEO: '#444444',
        GIF: '#444444',
    };
    const stringTypeImage = {
        GIF: '#444444',
        IMAGE: '#444444',
        PHOTO: '#444444',
        CAROUSEL_ALBUM: '#444444',
    };

    if (isScreen) {
        return (
            <article {...styles}>
                <div className="tag">
                    <FontAwesomeIcon icon={faImage} color={stringTypeImage[type] || '#DDDDDD'} />
                    <FontAwesomeIcon icon={faVideo} color={stringTypeVideo[type] || '#DDDDDD'} />
                </div>
                <div className="dateTop">
                    <Paragraph small color="rgba(0, 0, 0, 0.5)">
                        {upperFirst(moment(item.posted_time).format('MMM D, YYYY • HH:mm'))}
                    </Paragraph>
                </div>
            </article>
        );
    }
    return (
        <article {...styles}>
            <div className="tag">
                <Paragraph small bold color="#FFF">
                    {stringType[type] ? stringType[type] : type}
                </Paragraph>
            </div>
            <Paragraph small color="rgba(0, 0, 0, 0.5)">
                {upperFirst(moment(item.posted_time).format('MMM D, YYYY • HH:mm'))}
            </Paragraph>
        </article>
    );
};


Header.propTypes = propTypes;
Header.defaultProps = defaultProps;

export default Header;
