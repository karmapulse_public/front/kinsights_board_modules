import React from 'react';
import PropTypes from 'prop-types';
import { faPlay, faPause, faVolumeUp, faVolumeMute, faExpandArrowsAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './video.css';
import styles from './VideoStyles';

const propTypes = {
    sources: PropTypes.array,
    cover: PropTypes.string,
    type: PropTypes.string,
    loop: PropTypes.bool,
    gif: PropTypes.bool
};

const defaultProps = {
    sources: [],
    type: '',
    cover: '',
    loop: false,
    gif: false,
};

class Video extends React.Component {
    constructor(props) {
        super(props);
        this.togglePlayPause = this.togglePlayPause.bind(this);
        this.openFullScreen = this.openFullScreen.bind(this);
        this.updateProgressBar = this.updateProgressBar.bind(this);
        this.updateVolumeBar = this.updateVolumeBar.bind(this);
        this.timeUpdate = this.timeUpdate.bind(this);
        this.pauseVideo = this.pauseVideo.bind(this);
        this.playVideo = this.playVideo.bind(this);
        this.mute = this.mute.bind(this);
        this.ended = this.ended.bind(this);
        this.videoStatus = 'standBy';
        this.Player = React.createRef();
        this.PlayPauseButton = React.createRef();
        this.progressBar = React.createRef();
        this.MuteButton = React.createRef();
        this.volumeBar = React.createRef();
        this.FullScreen = React.createRef();
    }
    componentDidMount() {
        this.Player.current.controls = false;
        const mediaPlayer = this.Player.current;
        mediaPlayer.pause();
    }

    componentWillReceiveProps() {
        const mediaPlayer = this.Player.current;
        // const progressBar = this.progressBar;
        mediaPlayer.pause();
        this.setState({ videoStatus: 'standBy' });
    }

    playVideo() {
        const mediaPlayer = this.Player.current;
        const buttons = this.PlayPauseButton.current;
        const play = buttons.getElementsByClassName('video__controls__play--play')[0];
        const pause = buttons.getElementsByClassName('video__controls__play--pause')[0];
        play.style.display = 'none';
        pause.style.display = 'block';
        this.setState({ videoStatus: 'playing' });
        mediaPlayer.play();
    }

    pauseVideo() {
        const mediaPlayer = this.Player.current;
        const buttons = this.PlayPauseButton.current;
        const play = buttons.getElementsByClassName('video__controls__play--play')[0];
        const pause = buttons.getElementsByClassName('video__controls__play--pause')[0];
        play.style.display = 'block';
        pause.style.display = 'none';
        this.setState({ videoStatus: 'paused' });
        mediaPlayer.pause();
    }

    togglePlayPause() {
        const mediaPlayer = this.Player.current;
        if (!this.props.gif) {
            const buttons = this.PlayPauseButton.current;
            const play = buttons.getElementsByClassName('video__controls__play--play')[0];
            const pause = buttons.getElementsByClassName('video__controls__play--pause')[0];
            if (mediaPlayer.paused) {
                mediaPlayer.play();
                play.style.display = 'none';
                pause.style.display = 'block';
                this.setState({ videoStatus: 'playing' });
            } else {
                mediaPlayer.pause();
                play.style.display = 'block';
                pause.style.display = 'none';
                this.setState({ videoStatus: 'paused' });
            }
        } else {
            mediaPlayer.play();
            this.setState({ videoStatus: 'playing' });
        }
    }
    mute() {
        const mediaPlayer = this.Player.current;
        const buttons = this.MuteButton.current;
        const mute = buttons.getElementsByClassName('video__controls__volume--mute')[0];
        const volume = buttons.getElementsByClassName('video__controls__volume--volume')[0];
        if (mediaPlayer.muted === false) {
            mediaPlayer.muted = true;
            volume.style.display = 'none';
            mute.style.display = 'block';
        } else {
            mediaPlayer.muted = false;
            volume.style.display = 'block';
            mute.style.display = 'none';
        }
    }

    updateProgressBar() {
        const mediaPlayer = this.Player.current;
        const { progressBar } = this;
        const time = mediaPlayer.duration * (progressBar.current.value / 100);
        mediaPlayer.currentTime = time;
    }

    updateVolumeBar() {
        const mediaPlayer = this.Player.current;
        const { volumeBar } = this;
        mediaPlayer.volume = volumeBar.current.value;
    }

    timeUpdate() {
        if (!this.props.gif) {
            const mediaPlayer = this.Player.current;
            const { progressBar } = this;
            const value = (100 / mediaPlayer.duration) * mediaPlayer.currentTime;
            progressBar.current.value = value;
        }
    }

    openFullScreen() {
        const mediaPlayer = this.Player.current;
        if (mediaPlayer.requestFullscreen) {
            mediaPlayer.requestFullscreen();
        } else if (mediaPlayer.mozRequestFullScreen) {
            mediaPlayer.mozRequestFullScreen(); // Firefox
        } else if (mediaPlayer.webkitRequestFullscreen) {
            mediaPlayer.webkitRequestFullscreen(); // Chrome and Safari
        }
        this.setState({ videoStatus: 'fullscreen' });
    }

    ended() {
        this.setState({ videoStatus: 'standBy' });
    }

    renderButtonsBar() {
        if (!this.props.gif) {
            return (
                <div className="video__controls">
                    <button
                        onClick={this.togglePlayPause}
                        ref={this.PlayPauseButton}
                        className="video__controls__play"
                        title="play"
                    >
                        <div className="video__controls__play--play">{<FontAwesomeIcon icon={faPlay} />}</div>
                        <div className="video__controls__play--pause">{<FontAwesomeIcon icon={faPause} />}</div>
                    </button>
                    <input
                        className="video__controls__progress"
                        type="range"
                        ref={this.progressBar}
                        onChange={this.updateProgressBar}
                        onMouseDown={this.pauseVideo}
                        onMouseUp={this.playVideo}
                    />
                    <div className="video__controls__volume">
                        <button onClick={this.mute} ref={this.MuteButton} >
                            <div className="video__controls__volume--volume">{<FontAwesomeIcon icon={faVolumeUp} />}</div>
                            <div className="video__controls__volume--mute">{<FontAwesomeIcon icon={faVolumeMute} />}</div>
                        </button>
                        <input
                            type="range"
                            ref={this.volumeBar}
                            onChange={this.updateVolumeBar}
                            min="0"
                            max="1"
                            step="0.1"
                        />
                    </div>
                    <button
                        ref={this.FullScreen}
                        className="video__controls__fullscreen"
                        onClick={this.openFullScreen}
                        title="fullscreen"
                    > {<FontAwesomeIcon icon={faExpandArrowsAlt} />}
                    </button>
                </div>
            );
        }
        return <div />;
    }
    render() {
        let videoClass = 'standBy';
        if (this.state) {
            videoClass = this.state.videoStatus;
        }
        const Sources = [];
        this.props.sources.map((source, index) => (
            Sources.push(
                <source key={index} src={source.url ? source.url.replace('http:', 'https:') : ''} type={source.content_type} />
            )
        ));
        const gif = () => (<h1>GIF</h1>);
        const httpsCover = this.props.cover.replace('http:', 'https:');
        return (
            <div className={`video ${videoClass}`} {...styles(this.props.type)}>
                <video
                    ref={this.Player}
                    className="video__player"
                    loop={this.props.loop}
                    controls
                    poster={httpsCover}
                    onTimeUpdate={this.timeUpdate}
                    onEnded={this.ended}
                >
                    {Sources}
                </video>
                { this.renderButtonsBar() }
                <button onClick={this.togglePlayPause} className="video__playFull">
                    {this.props.gif ? gif() : <FontAwesomeIcon icon={faPlay} />}
                </button>
            </div>
        );
    }
}
Video.propTypes = propTypes;
Video.defaultProps = defaultProps;

export default Video;
