import { css } from 'glamor';

const s = {
    instagram: '0',
    facebook: '0 30px 0 55px',
    twitter: '0 30px 10px 55px'
};
const styles = type => (css({
    margin: s[type],
}));

export default styles;
