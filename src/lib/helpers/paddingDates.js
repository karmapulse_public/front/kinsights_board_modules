import moment from 'moment';
import upperFirst from 'lodash/upperFirst';

const getResetInterval = (intervalType) => {
    const resetObject = {
        minute: 0,
        second: 0,
        millisecond: 0
    };

    if (intervalType === 'day' || intervalType === 'week' || intervalType === 'month') { resetObject.hour = 0; }
    if (intervalType === 'date') { resetObject.date = 1; }

    return resetObject;
};

const paddingDates = (intervals, { initial_date, final_date, filters: { interval } }, dateKey = 'date', countKey = 'total') => {
    if (intervals.length === 0) return [];
    const initialDate = moment(initial_date).set({ ...getResetInterval(interval) });
    const finalDate = moment(final_date).set({ ...getResetInterval(interval) });

    let date = moment(intervals[0][dateKey]);
    let diffs = date.diff(initialDate, true);
    while (diffs >= 0) {
        date.subtract(1, `${interval}s`);
        intervals.unshift({ [dateKey]: date.toISOString(), [countKey]: 0, key: date.toDate().getTime() });
        diffs = date.diff(initialDate);
    }

    date = moment(intervals[intervals.length - 1][dateKey]);
    diffs = finalDate.diff(date, true);
    while (diffs > 0) {
        date.add(1, `${interval}s`);
        intervals.push({ [dateKey]: date.toISOString(), [countKey]: 0, key: date.toDate().getTime() });
        diffs = finalDate.diff(date, interval);
    }

    return intervals;
};

export default paddingDates;

export const labelType = (startDate, finishDate, type) => {
    const label = {
        hour: `${upperFirst(moment(startDate).format('MMM DD, YYYY  h:mm A'))}`,
        day: `${upperFirst(moment(startDate).format('MMM DD, YYYY'))}`,
        week: `${upperFirst(moment(startDate).format('MMM DD, YYYY'))} al ${upperFirst(moment(finishDate).format('MMM DD, YYYY'))}`,
        month: `${upperFirst(moment(startDate).format('MMM YYYY'))}`,
    };
    return label[type];
};
