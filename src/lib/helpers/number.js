export function numberWithCommas(num) {
    const number = num || 0;
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

export function nFormatter(num) {
    if (num >= 1000000000) {
        const fnum = (num / 1000000000).toFixed(1).replace(/\.0$/, '');
        return `${numberWithCommas(fnum)}G`;
    }
    if (num >= 1000000) {
        const fnum = (num / 1000000).toFixed(1).replace(/\.0$/, '');
        return `${numberWithCommas(fnum)}M`;
    }
    if (num >= 1000) {
        const fnum = (num / 1000).toFixed(1).replace(/\.0$/, '');
        return `${numberWithCommas(fnum)}K`;
    }
    return num;
}

export function getPercentage(n, total) {
    if (total === 0) {
        return 0;
    }
    const percentage = (n * 100) / total;
    return Math.round(percentage * 100) / 100;
}
