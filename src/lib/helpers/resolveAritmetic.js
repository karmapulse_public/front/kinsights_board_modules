const operators = {
    '+': { value: 1, op: (v1, v2) => v1 + v2 },
    '-': { value: 1, op: (v1, v2) => v1 - v2 },
    '*': { value: 2, op: (v1, v2) => v1 * v2 },
    '/': { value: 2, op: (v1, v2) => v1 / v2 },
    '^': { value: 3, op: (v1, v2) => v1 ** v2 },
};

const isOperator = character => operators[character];

const greaterPrecedence = (op1, op2) => ( // first one greater?
    operators[op1].value - operators[op2].value >= 0
);

export const toPostOrder = (chain) => {
    const stack = [];
    const outList = [];
    const chainP = chain.split(' ');
    while (chainP.length !== 0) {
        const actualChar = chainP[0];
        chainP.shift();
        // Es Número
        if (parseInt(actualChar, 10)) {
            outList.push(parseInt(actualChar, 10));
        // Es incognita
        } else if (actualChar === 'n') {
            outList.push('n');
        // Es parentesis izquierdo
        } else if (actualChar === '(') {
            stack.unshift(actualChar);
        // Es parentesis derecho
        } else if (actualChar === ')') {
            while (stack.length !== 0 && stack[0] !== '(') {
                outList.push(stack[0]);
                stack.shift();
            }
            if (stack[0] === '(') { stack.shift(); }
        // Es parentesis operador
        } else if (isOperator(actualChar)) {
            while (stack.length !== 0 && isOperator(stack[0]) && greaterPrecedence(stack[0], actualChar)) {
                outList.push(stack[0]);
                stack.shift();
            }
            stack.unshift(actualChar);
        }
    }
    while (stack.length !== 0) {
        outList.push(stack[0]);
        stack.shift();
    }
    return outList;
};

export const evalAritmetic = (postOrderT, value) => {
    const postOrder = [...postOrderT];
    let i = 0;
    while (postOrder.length !== 1) {
        const actualChar = postOrder[i];
        // Es incognita
        if (actualChar === 'n') {
            postOrder[i] = value;
            i += 1;
            // Es operador
        } else if (isOperator(actualChar)) {
            const resul = operators[actualChar].op(postOrder[i - 2], postOrder[i - 1]);
            postOrder.splice(i - 2, 3, resul);
            i -= 1;
        } else {
            i += 1;
        }
    }
    return postOrder[0];
};
