import { createContext } from 'react';

export default createContext({
    open: false,
    onCloseDrawer: () => ({}),
    drawerData: {
        type: 'TWITTER',
        title: '',
        subtitleLeft: '',
        subtitleRight: '',
        queryData: {}
    }
});
