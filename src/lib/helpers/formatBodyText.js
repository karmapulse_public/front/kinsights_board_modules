import isUndefined from 'lodash/isUndefined';

/**
 * Find urls patter in tweets body text
 */
function findUrlInBodyText(text) {
    const regex = /(https?:\/\/[^\s]+)/g;
    return text.replace(regex, (url) => {
        const URL = url;
        return `<a href="${URL}" target="_blank">${URL}</a>`;
    });
}

/**
 * Find hastags patter in tweets body text
 */
function findHashtagInBodyText(text) {
    const regex = /(^#|[^&]#)([a-z0-9]+[^\s!@#$%^&*()=+./,\[{\]]+)/gi;
    return text.replace(regex, (hashtag) => {
        const h = (hashtag.replace('#', '')).trim();
        return `<a href="https://twitter.com/hashtag/${h}" target="_blank">${hashtag} </a>`;
    });
}

/**
 * Find users patter in tweets body text
 */
function findUserInBodyText(text) {
    const regex = /(^|\s)@(\w+)/g;
    const regex1 = /(^|\s)@(\w+)/g;
    const tweet = text.replace(regex, '$1<a href="http://www.twitter.com/$2" target="_blank">@$2</a>');
    return tweet.replace(regex1, '$1#<a href="http://search.twitter.com/search?q=%23$2" target="_blank" >"@$2</a>');
}

export default function formatBodyText(tweet) {
    if (!isUndefined(tweet)) {
        const withUrl = findUrlInBodyText(tweet);
        const withHashtag = findHashtagInBodyText(withUrl);
        return findUserInBodyText(withHashtag);
    }
    return '';
}
