/**
 * DialogIcon SVG
 */

import React from 'react';

const AnalysisIcon = props => (
    /* eslint-disable */
    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
        <path fill={props.color} fillRule="nonzero" d="M1.647 19.131A1.141 1.141 0 0 1 .5 17.995V3.435C.5 1.551 2.04.026 3.941.026H16.56C18.459.027 20 1.552 20 3.434v9.087c0 1.882-1.54 3.407-3.441 3.407H7.503a4.747 4.747 0 0 0-3.2 1.232l-.046.046L2.41 18.84c-.21.186-.481.29-.763.29zM3.941 2.298c-.634 0-1.147.509-1.147 1.136v12.011a7.1 7.1 0 0 1 4.709-1.788h9.056c.634 0 1.147-.509 1.147-1.136V3.434c0-.627-.513-1.136-1.147-1.136H3.94z"/>
    </svg>

    /* eslint-enable */
);

export default AnalysisIcon;
