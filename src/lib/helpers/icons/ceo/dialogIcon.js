/**
 * DialogIcon SVG
 */

import React from 'react';

const DialogIcon = props => (
    /* eslint-disable */
    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
        <path fill={props.color} fillRule="evenodd" d="M2 2v12h4v2l2.667-2H18V2H2zm2 18v-4H2c-1.103 0-2-.897-2-2V2C0 .897.897 0 2 0h16c1.103 0 2 .897 2 2v12c0 1.103-.897 2-2 2H9.333L4 20z" />
    </svg>

    /* eslint-enable */
);

export default DialogIcon;
