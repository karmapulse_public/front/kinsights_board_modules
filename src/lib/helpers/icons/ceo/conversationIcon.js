/**
 * ConversationIcon SVG
 */

import React from 'react';

const ConversationIcon = props => (
    /* eslint-disable */
    <svg width="16" height="15" viewBox="0 0 16 15">
        <g fill={props.color} fillRule="evenodd">
            <path d="M4.115 11.636a6.18 6.18 0 0 1-.047-.727c0-3.213 2.46-5.818 5.496-5.818 1.328 0 2.546.499 3.496 1.329.02-.198.03-.399.03-.602C13.09 2.61 10.503 0 7.32 0 4.136 0 1.547 2.61 1.547 5.818c0 1.102.3 2.151.873 3.073L0 11.636h4.115z" />
            <path d="M14.352 11.92c.242-.547.369-1.14.369-1.738 0-2.407-1.997-4.364-4.452-4.364-2.454 0-4.45 1.957-4.45 4.364 0 2.406 1.996 4.363 4.45 4.363H16l-1.648-2.626z" />
        </g>
    </svg>


    /* eslint-enable */
);

export default ConversationIcon;
