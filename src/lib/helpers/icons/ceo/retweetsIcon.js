/**
 * DialogIcon SVG
 */

import React from 'react';

const AnalysisIcon = props => (
    /* eslint-disable */
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="14" viewBox="0 0 24 14">
        <path fill={props.color} fillRule="nonzero" d="M4.727 13.652a.874.874 0 0 1-.878-.868V5.709L0 5.707 5.23.06l5.232 5.65H6.607v5.211h4.139l2.712 2.731H4.727zM23.5 7.945h-3.85V.87A.874.874 0 0 0 18.772 0h-8.73l2.71 2.73h4.139v5.213l-3.852.002 5.231 5.649 5.23-5.649z"/>
    </svg>

    /* eslint-enable */
);

export default AnalysisIcon;
