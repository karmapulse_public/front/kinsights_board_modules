/**
 * RetweetIcon SVG
 */

import React from 'react';

const RetweetIcon = (color = 'none') => (
    /* eslint-disable */
    <svg width="13px" height="12px" viewBox="0 0 13 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
        <g id="Page-1" stroke="none" strokeWidth="1" fill={color} fillRule="evenodd">
            <g id="Versus-768" transform="translate(-373.000000, -1354.000000)" fill="#FFFFFF">
                <g id="CandidatoIzquierda" transform="translate(86.000000, 56.000000)">
                    <g id="Galería" transform="translate(0.000000, 1058.000000)">
                        <g id="ImagenGaleria" transform="translate(0.000000, 37.000000)">
                            <g id="TWEET" transform="translate(0.000000, 195.000000)">
                                <path d="M295,8 L293.586,9.414 L295.586,11.414 L291,11.414 C288.794,11.414 287,13.208 287,15.414 C287,17.62 288.794,19.414 291,19.414 L292,19.414 L292,17.414 L291,17.414 C289.897,17.414 289,16.517 289,15.414 C289,14.312 289.897,13.414 291,13.414 L295.586,13.414 L293.586,15.414 L295,16.828 L299.414,12.414 L295,8 Z" id="Fill-386"></path>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
    /* eslint-enable */
);

export const RetweetIconM = () => (
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="12" viewBox="0 0 16 12">
        <defs>
            <path id="a" d="M4.986 0H0v4.797h4.986V0z" />
            <path id="c" d="M0 5h5V0H0v5z" />
        </defs>
        <g fill="none" fillRule="evenodd">
            <path fill="#000" d="M11 12H5.253C3.459 12 2 10.619 2 8.92V4h1.169v4.92c0 1.088.935 1.973 2.084 1.973H11V12z" />
            <g>
                <mask id="b" fill="#fff">
                    <use xlinkHref="#a" />
                </mask>
                <path fill="#000" d="M4.986 4.797L2.493 0 0 4.797z" mask="url(#b)" />
            </g>
            <path fill="#000" d="M14 8h-1.169V3.103c0-1.101-.946-1.996-2.108-1.996H5V0h5.723C12.53 0 14 1.392 14 3.103V8z" />
            <g transform="translate(11 7)">
                <mask id="d" fill="#fff">
                    <use xlinkHref="#c" />
                </mask>
                <path fill="#000" d="M0 0l2.5 5L5 0z" mask="url(#d)" />
            </g>
        </g>
    </svg>
);

export default RetweetIcon;
