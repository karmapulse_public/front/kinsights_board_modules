/**
 * DialogIcon SVG
 */

import React from 'react';

const noDataYet = () => (
    /* eslint-disable */
    <svg xmlns="http://www.w3.org/2000/svg" width="39" height="39" viewBox="0 0 39 39">
        <g fill="none" fillRule="nonzero">
            <path fill="#858585" d="M0 28h6v11H0z" />
            <path fill="#C6C6C6" d="M11 39V10h6v29z" />
            <path fill="#ACACAC" d="M22 39V0h6v39z" />
            <path fill="#D4D4D4" d="M33 39V20h6v19z" />
        </g>
    </svg>

    /* eslint-enable */
);

export default noDataYet;
