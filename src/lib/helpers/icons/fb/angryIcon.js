/**
 * AngryIcon SVG
 */

import React from 'react';

const AngryIcon = props => (
    /* eslint-disable */
    <svg xmlns="http://www.w3.org/2000/svg" width="23" height="23" viewBox="0 0 23 23">
        <defs>
            <linearGradient id="gradientAngry" x1="50%" x2="50%" y1="0%" y2="100%">
                <stop offset="0%" stopColor="#F65167" />
                <stop offset="100%" stopColor="#FFDA69" />
            </linearGradient>
        </defs>
        <g fill="none" fillRule="evenodd">
            <circle cx="11.5" cy="11.5" r="11.5" fill="url(#gradientAngry)" />
            <path fill="#252B39" d="M11.4 19.268c2.06 0 3.732-.226 3.732-.79 0-.225-1.671-.925-3.733-.925-2.061 0-3.732.813-3.732.925 0 .436 1.67.79 3.732.79zM15.56 14.09a21.09 21.09 0 0 1-1.911.054c-.241-.003-.322-.004-.434-.004a.504.504 0 1 1 0-1.008c.117 0 .2 0 .444.003 2.477.026 3.986-.227 6.058-1.37a.504.504 0 1 1 .487.883c-.97.535-1.83.891-2.705 1.122a1.21 1.21 0 1 1-1.939.32zm-10.082-.297c-.907-.23-1.793-.592-2.796-1.145a.504.504 0 0 1 .487-.883c2.072 1.143 3.581 1.396 6.058 1.37.244-.003.327-.003.444-.003a.504.504 0 0 1 0 1.008c-.113 0-.193.001-.434.004a21.717 21.717 0 0 1-1.793-.046 1.21 1.21 0 1 1-1.966-.305z" />
        </g>
    </svg>
    /* eslint-enable */
);

export default AngryIcon;
