export { default as LikeIcon } from './likeIcon';
export { default as LoveIcon } from './loveIcon';
export { default as HahaIcon } from './hahaIcon';
export { default as WowIcon } from './wowIcon';
export { default as SadIcon } from './sadIcon';
export { default as AngryIcon } from './angryIcon';
