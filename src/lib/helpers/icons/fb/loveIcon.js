/**
 * LoveIcon SVG
 */

import React from 'react';

const LoveIcon = props => (
    /* eslint-disable */
    <svg xmlns="http://www.w3.org/2000/svg" width="23" height="23" viewBox="0 0 23 23">
        <g fill="none" fillRule="evenodd">
            <circle cx="11.5" cy="11.5" r="11.5" fill="#F55065"/>
            <path fill="#FFF" d="M4.74 10.126a3.632 3.632 0 0 1 6.76-2.51 3.631 3.631 0 0 1 6.78 2.427 1.422 1.422 0 0 1-.021.146c-.706 3.631-6.658 7.767-6.658 7.767s-6.658-4.338-6.86-7.767a1.173 1.173 0 0 1-.002-.063z"/>
        </g>
    </svg>
    /* eslint-enable */
);

export default LoveIcon;
