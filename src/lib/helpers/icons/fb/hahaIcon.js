/**
 * HahaIcon SVG
 */

import React from 'react';

const HahaIcon = props => (
    /* eslint-disable */
    <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" width="23" height="23" viewBox="0 0 23 23">
        <defs>
            <path id="a" d="M0 0a7.465 7.465 0 0 0 14.93 0"/>
        </defs>
        <g fill="none" fillRule="evenodd">
            <circle cx="11.5" cy="11.5" r="11.5" fill="#FFDA69"/>
            <g transform="translate(4.035 11.298)">
                <mask id="b" fill="#fff">
                    <use xlinkHref="#a"/>
                </mask>
                <use fill="#252B39" xlinkHref="#a"/>
                <path fill="#F55065" d="M0 9.28s1.816 1.413 7.254 1.413 7.474-1.412 7.474-1.412L7.254 2.42 0 9.281z" mask="url(#b)" transform="rotate(-180 7.364 6.557)"/>
            </g>
            <path fill="#252B39" d="M7.719 6.995c.268.16.519.648-.012 1.104L5 9.857a.606.606 0 0 1-.66-1.016l1.972-1.28L4.329 6.37a.606.606 0 0 1 .623-1.038L7.72 6.995zm7.511 0l2.767-1.663a.605.605 0 1 1 .623 1.038L16.637 7.56l1.971 1.28a.605.605 0 1 1-.66 1.016l-2.706-1.758c-.531-.456-.28-.943-.012-1.104z"/>
        </g>
    </svg>

    /* eslint-enable */
);

export default HahaIcon;
