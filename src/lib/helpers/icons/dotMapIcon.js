/**
 * Twitter SVG
 */

import React from 'react';
import lightenDarkenColors from '../lightenDarkenColors';

const Icon = ({ color, value, styles }) => (
    <svg xmlns="http://www.w3.org/2000/svg" width="42" height="42" viewBox="0 0 42 42">
        <g fill="none" fillRule="evenodd" transform="translate(1 1)">
            <circle cx="20" cy="20" r="20" fill={color} stroke={lightenDarkenColors(color, -30)} strokeWidth="2" />
            <text {...styles} textAnchor="middle">
                <tspan x="46%" y="55%">{value}</tspan>
            </text>
        </g>
    </svg>

);
/* eslint-enable */

export default Icon;
