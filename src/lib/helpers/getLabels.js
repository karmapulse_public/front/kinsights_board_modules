import cloneDeep from 'lodash/cloneDeep';
// getMilitantLabel
export default (fq, cntParam = null) => {
    const filterQuery = cloneDeep(fq);
    if (cntParam) delete filterQuery[cntParam];
    let label = 'Militantes';

    if (filterQuery.gender && filterQuery.gender === 'H') label = `${label} hombres de`;
    else if (filterQuery.gender && filterQuery.gender === 'M') label = `${label} mujeres de`;
    else label = `${label} de`;

    if (filterQuery.local_headboard) label = `${label} ${filterQuery.local_headboard}`;
    else if (filterQuery.federal_headboard) label = `${label} ${filterQuery.federal_headboard}`;
    else if (filterQuery.municipality) label = `${label} ${filterQuery.municipality}`;
    else if (filterQuery.state) label = `${label} ${filterQuery.state}`;
    else label = `${label} la República`;

    if (filterQuery.section_id) label = `${label} dentro la sección ${filterQuery.section_id}`;

    if (filterQuery.region && filterQuery.region === 'U') label = `${label} en zona urbana`;
    else if (filterQuery.region && filterQuery.region === 'R') label = `${label} en zona rural`;
    else if (filterQuery.region && filterQuery.region === 'M') label = `${label} en zona mixta`;

    if (filterQuery.exists) {
        const arrayExists = filterQuery.exists.split(',');
        const dictType = {
            phone: 'teléfono',
            mobile: 'celular',
            email: 'email',
            twitter: 'Twitter'
        };
        arrayExists.forEach((exist, index) => {
            if (index === 0) label = `${label} con ${dictType[exist]}`;
            else label = `${label} y ${dictType[exist]}`;
        });
    }

    return label;
};
