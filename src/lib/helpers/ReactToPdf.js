import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import JsPdf from 'jspdf';
import html2canvas from 'html2canvas';

class ReactToPdf extends PureComponent {
    constructor(props) {
        super(props);
        this.toPdf = this.toPdf.bind(this);
        this.targetRef = React.createRef();
    }

    toPdf() {
        const { targetRef, filename, onComplete } = this.props;
        const source = targetRef || this.targetRef;
        const targetComponent = source.current || source;
        if (!targetComponent) {
            throw new Error(
                'Target ref must be used or informed. See https://github.com/ivmarcos/react-to-pdf#usage.'
            );
        }
        html2canvas(targetComponent).then(
            (canvas) => {
                const imgData = canvas.toDataURL('image/png', 1.0);
                const pdf = new JsPdf('l', 'mm', 'a4');
                const width = canvas.width * 0.264583333;
                const height = canvas.height * 0.264583333;
                const sizeElto = {
                    width,
                    height
                };
                let ratio = 1;

                while (
                    sizeElto.width > 297 ||
                    sizeElto.height > 210
                ) {
                    sizeElto.width = width / ratio;
                    sizeElto.height = height / ratio;

                    ratio += 0.5;
                }
                pdf.addImage(
                    imgData,
                    'PNG',
                    (297 - sizeElto.width) / 2,
                    (210 - sizeElto.height) / 2,
                    sizeElto.width,
                    sizeElto.height
                );
                pdf.save(filename);
                if (onComplete) onComplete();
            }
        );
    }

    render() {
        const { children } = this.props;
        return children({ toPdf: this.toPdf, targetRef: this.targetRef });
    }
}

ReactToPdf.propTypes = {
    filename: PropTypes.string.isRequired,
    children: PropTypes.func.isRequired,
    onComplete: PropTypes.func,
    targetRef: PropTypes.oneOfType([
        PropTypes.func,
        PropTypes.shape({ current: PropTypes.instanceOf(Element) })
    ])
};

ReactToPdf.defaultProps = {
    onComplete: undefined,
    targetRef: undefined
};

export default ReactToPdf;
