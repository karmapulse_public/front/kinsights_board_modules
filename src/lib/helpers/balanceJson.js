import find from 'lodash/find';
import flattenDeep from 'lodash/flattenDeep';
import unionBy from 'lodash/unionBy';

export default (buckets, entity) => {
    const bucketsOriginal = JSON.parse(JSON.stringify(buckets));

    const getI = flattenDeep(bucketsOriginal.map(i => i[`${entity}_by_time`].buckets.map(o => o)));
    const max = unionBy(getI, 'key');

    const resultBuckets = JSON.parse(JSON.stringify(buckets));

    const y = resultBuckets.map((item) => {
        const collect = [];

        max.forEach((maxItem) => {
            const xx = find(item[`${entity}_by_time`].buckets, { key: maxItem.key });

            if (typeof xx === 'undefined') {
                collect.push(Object.assign(maxItem, { doc_count: 0 }));
            }
        });
        //eslint-disable-next-line
        item[`${entity}_by_time`].buckets = item[`${entity}_by_time`].buckets.concat(collect);

        return item;
    });

    return y;
};
