
import clone from 'lodash/clone';
import moment from 'moment';
import 'moment-timezone';

/*
    withCache sirve para calcular o no la ultima fecha en multiplo de 5
    y hacer uso del cache de elastic (preguntar a josé)
*/
const setDates = (dateRange, withCache = false) => {
    let { startDate, endDate } = dateRange;

    const tz = 'America/Mexico_City';
    const strStartDate = clone(startDate).format('YYYY-MM-DD hh:mm:ss');
    const strEndDate = clone(endDate).format('YYYY-MM-DD hh:mm:ss');

    const utcOffsetStart = moment.tz(strStartDate, tz).isDST() ? '-05:00' : '-06:00';
    const utcOffsetEnd = moment.tz(strEndDate, tz).isDST() ? '-05:00' : '-06:00';

    startDate = startDate.utcOffset(utcOffsetStart);
    endDate = endDate.utcOffset(utcOffsetEnd);

    if (withCache) {
        startDate = startDate.set({
            hour: 0,
            minute: 0,
            second: 0,
            millisecond: 0
        });
        endDate = endDate.set({
            minute: 5 * (Math.floor(moment().minute() / 5)),
            second: 0,
            millisecond: 0
        });
    }

    return {
        initial_date: startDate.toISOString(),
        final_date: endDate.toISOString(),
    };
};

export default setDates;
