import React from 'react';
import { css } from 'glamor';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileExport } from '@fortawesome/free-solid-svg-icons';

import ReactToPdf from './ReactToPdf';

const defaultStyles = {
    position: 'absolute',
    top: '0px',
    right: '10px',
    zIndex: 2,
    cursor: 'pointer',
    ':hover > svg': {
        color: 'rgba(0, 0, 0, 0.6)'
    },
    ' > svg': {
        transition: 'color 0.25s ease'
    }
};

const withExportPDF = (Component, componentProps) => {
    const bodyRef = React.createRef();

    const buttonComponent = (ref, name, styles) => (
        <div
            className="export-button"
            {...css({
                ...defaultStyles,
                ...styles
            })}
        >
            <ReactToPdf
                targetRef={bodyRef}
                filename="div-blue.pdf"
            >
                {({ toPdf }) => (
                    <FontAwesomeIcon
                        onClick={toPdf}
                        icon={faFileExport}
                        color="rgba(0, 0, 0, 0.3)"
                    />
                )}
            </ReactToPdf>
        </div>
    );

    componentProps.fields.buttonExport = componentProps.fields.hasExport ?
        buttonComponent : null;

    return (
        <section className="export-body" ref={bodyRef}>
            {React.cloneElement(Component, componentProps)}
        </section>
    );
};

export default withExportPDF;
