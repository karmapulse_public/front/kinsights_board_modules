import React from 'react';

import { storiesOf } from '@storybook/react';
import { withNotes } from '@storybook/addon-notes';
import moment from 'moment';

import TopUsers from '../../lib/modules/TopUsers';

import README from './README.md';

const params = {
    services: {
        twitter: 'https://dev-serverless.karmapulse.com/insights/recipes-twitter',
        facebook: 'https://dev-serverless.karmapulse.com/elasticsearch-fb/query_recipe_facebook'
    },
    fields: {
        search_id: '5c2f950edda98a00d879f4f3',
        visualization: 'list',
        top_limit: 10
    },
    dateRange: {
        endDate: moment('2019-06-10T11:28-05:00'),
        startDate: moment('2017-03-08T00:00-05:00'),
    }
};

storiesOf('TopUsers', module)
    .addWithJSX(
        'default',
        withNotes(README)(() => (
            <TopUsers
                {...params}
            />
        ))
    );
