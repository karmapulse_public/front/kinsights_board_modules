import React from 'react';
import { css } from 'glamor';
import { storiesOf } from '@storybook/react';

import TopTrends from '../../lib/modules/TopTrends';

import data from '../data';
import Container from '../container';

const { tw } = data;

const paramsList = {
    fields: {
        search_id: tw.id,
        top_limit: 10,
        subtitle: {
            list: 'Temas',
            count: 'Menciones'
        },
        entity_type: 'keywords',
        query_type: 'top',
        hasExport: true
    },
    dateRange: { ...tw.defaultDates },
    color: '#380000',
};

const stylesRadar = css({
    width: '200px'
});

storiesOf('TopThemes', module)
    .add(
        'List',
        (() => (
            <Container
                visualization="list_top_trends"
                childrenParams={paramsList}
                showCtrRawData
            >
                <TopTrends />
            </Container>
        ))
    )
    .add(
        'Radar',
        (() => (
            <Container
                styles={stylesRadar}
                visualization="radar_top_trends"
                childrenParams={paramsList}
            >
                <TopTrends />
            </Container>
        ))
    );
