import React from 'react';

import { storiesOf } from '@storybook/react';
// import { action } from '@storybook/addon-actions';
import { withNotes } from '@storybook/addon-notes';
import moment from 'moment';

import TopTrendingTopics from '../../lib/modules/TopTrendingTopics';

import README from './README.md';

const params = {
    services: {
        twitter: 'https://dev-serverless.karmapulse.com/insights/recipes-twitter',
        facebook: 'https://dev-serverless.karmapulse.com/elasticsearch-fb/query_recipe_facebook'
    },
    fields: {
        region_id: 'Ciudad de México | 116545',
        top_limit: 10,
        show_title: true,
        visualization: 'banner_top_trends',
    },
    dateRange: {
        endDate: moment('2019-06-02T20:28-05:00'),
        startDate: moment('2019-05-01T20:00-05:00'),
    },
    side: 'left',
    color: '#380000'
};

storiesOf('TopTrendingTopics', module)
    .addWithJSX(
        'Trending Topics',
        withNotes(README)(() => (
            <TopTrendingTopics
                {...params}
            />
        ))
    );
