import React from 'react';

import { storiesOf } from '@storybook/react';
import { withNotes } from '@storybook/addon-notes';

import TotalSentiment from '../../../lib/modules/FbSentiment';

import data from '../../data';
import Container from '../../container';

import README from './README.md';

const { fb } = data;

const params = {
    fields: {
        search_id: fb.id,
        hasExport: true
    },
    dateRange: { ...fb.defaultDates },
    top: '5',
    showNoDataYet: false
};

storiesOf('FbModules', module)
    .addWithJSX(
        'Barras (old)',
        withNotes(README)(() => (
            <Container
                visualization="sentiment_bar_chart"
                childrenParams={params}
            >
                <TotalSentiment />
            </Container>
        ))
    )
    .addWithJSX(
        'Barras (remaster)',
        withNotes(README)(() => (
            <Container
                visualization="sentiment_bar_chart_remaster"
                childrenParams={params}
            >
                <TotalSentiment />
            </Container>
        ))
    );
