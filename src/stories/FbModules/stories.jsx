import React from 'react';

import { storiesOf } from '@storybook/react';
import { withNotes } from '@storybook/addon-notes';

import TotalSentiment from '../../lib/modules/FbSentiment';
import FbTopPosts from '../../lib/modules/FbTopPosts';
import FbInteractions from '../../lib/modules/FbInteractions';
import FbSearchKeywords from '../../lib/modules/FbSearchKeywords';

import data from '../data';
import Container from '../container';

import README from './README.md';

const { fb } = data;

const params = {
    services: {
        twitter: 'https://dev-serverless.karmapulse.com/insights/recipes-twitter',
        facebook: 'https://dev-serverless.karmapulse.com/elasticsearch-fb/query_recipe_facebook',
        perfilFacebookToken: 'Bearer ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SnpkV0lpT2lKaGNIQmZkRzlyWlc0aUxDSmhkV1FpT2lJMU1UZzFZV0k1TWpRNE1qVmlZV1EwWmpaa1ltWXhORFE0TldReFpUQTJPVGsxTldOak16VTBZVE5pWWpGaE5XVm1OekJtTURCalpEWXdNR1prTlRkbE1qTXhabVkzT1daaE5UQmxZbUUyT0RCaU16TTJOalUxTnpReVltWXlaV1FpTENKelpYSjJhV05sWDNSdmEyVnVYM0IxWW14cFl5STZJalV4T0RWaFlqa3lORGd5TldKaFpEUm1ObVJpWmpFME5EZzFaREZsTURZNU9UVTFZMk16TlRSaE0ySmlNV0UxWldZM01HWXdNR05rTmpBd1ptUTFOMlV5TXpGbVpqYzVabUUxTUdWaVlUWTRNR0l6TXpZMk5UVTNOREppWmpKbFpDSXNJbkJsY20xcGMzTnBiMjV6SWpwN2ZTd2lhV0YwSWpveE5USXlNVGs0TlRNM0xDSmxlSEFpT2pRMk56VTNPVGcxTXpkOS5vLWFzMXVJd3EyUWs3b1NWeFpqbHhiUVZxQ3NzbmFKT1Vucmo4bFppOXJz',

    },
    fields: {
        search_id: fb.id,
        visualization: 'sentiment_bar_chart_remaster',
    },
    dateRange: { ...fb.defaultDates },
    top: '5',
    showNoDataYet: false
};

const paramsTotalInteractions = {
    ...params,
    fields: {
        ...params.fields,
        visualization: 'slider_interactions',
    }
};
const paramsTopEntities = {
    ...params,
    fields: {
        ...params.fields,
        visualization: 'slider_top_posts',
    }
};
const paramsTopEnt = {
    ...params,
    fields: {
        ...params.fields,
        visualization: 'carousel_top_posts',
    }
};
const paramsSearchKeywords = {
    ...params,
    fields: {
        ...params.fields,
        visualization: 'search_histogram',
        keywords: ['zelda', 'link', 'ganon', 'mario'],
    }
};

storiesOf('FbModules', module)
    .addWithJSX(
        'TotalSentiment',
        withNotes(README)(() => (
            <Container
                visualizations={[
                    'sentiment_bar_chart',
                    'sentiment_bar_chart_remaster'
                ]}
                childrenParams={params}
            >
                <TotalSentiment />
            </Container>
        ))
    )
    .addWithJSX(
        'FbTopPosts',
        withNotes(README)(() => (
            <Container>
                <FbTopPosts
                    {...paramsTopEntities}
                />
            </Container>
        ))
    )
    .addWithJSX(
        'FbTopPostsDefault',
        withNotes(README)(() => (
            <div {...styles2()}>
                <FbTopPosts
                    {...paramsTopEnt}
                />
            </div>
        ))
    )
    .addWithJSX(
        'TotalInteractions',
        withNotes(README)(() => (
            <div {...styles()}>
                <FbInteractions
                    {...paramsTotalInteractions}
                />
            </div>
        ))
    )
    .addWithJSX(
        'FbSearchKeywords',
        withNotes(README)(() => (
            <FbSearchKeywords
                {...paramsSearchKeywords}
            />
        ))
    );
