import React from 'react';

import { storiesOf } from '@storybook/react';

import { css } from 'glamor';

import TopTrendsNew from '../../lib/modules/FbTopTopicsComments';

import data from '../data';
import Container from '../container';

const { fb } = data;

const paramsList = {
    fields: {
        search_id: fb.id,
        top_limit: 10,
        title: 'Lista',
        hasExport: true,
        subtitle: {
            list: 'Temas',
            count: 'Menciones'
        }
    },
    dateRange: { ...fb.defaultDates },
    color: '#380000'
};

// const paramsCloud = {
//     services: {
//         twitter: 'https://dev-serverless.karmapulse.com/insights/recipes-twitter',
//         facebook: 'https://dev-serverless.karmapulse.com/insights/recipes-facebook'
//     },
//     fields: {
//         search_id: '5b215bce6067bb000e8c7ca3',
//         visualization: 'wordcloud_top_trends',
//         fontColor: ['#48a0ce'],
//         top_limit: 10,
//         width: 200,
//         height: 200,
//         minAngle: 20,
//         maxAngle: 90,
//         outlineFn: '3 - n',
//         outlineColor: '#F00'
//     },
//     dateRange: {
//         startDate: moment('2018-07-01'),
//         endDate: moment('2018-07-31'),
//     },
// };

storiesOf('FbTopTopics', module)
    .add(
        'List',
        (() => (
            <Container
                visualization="list_top_trends"
                childrenParams={paramsList}
                fbUrl={1}
            >
                <TopTrendsNew />
            </Container>
        ))
    );
    // .addWithJSX(
    //     'Wordcloud Topics',
    //     withNotes(README)(() => (
    //         <TopTrendsNew
    //             {...paramsCloud}
    //         />
    //     ))
    // );
