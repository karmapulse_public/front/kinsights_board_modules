import React from 'react';

import { storiesOf } from '@storybook/react';
import { withNotes } from '@storybook/addon-notes';
import moment from 'moment';

import MultiSourceTimeline from '../../lib/modules/MultiSourceTimeline';

import README from './README.md';

const params = {
    services: {
        twitter: 'https://dev-serverless.karmapulse.com/insights/recipes-twitter',
        facebook: 'https://dev-serverless.karmapulse.com/elasticsearch-fb/query_recipe_facebook',
        perfilFacebook: 'https://dev-kairos-fb.karmapulse.com/v1/datasets/historical',
        perfilFacebookToken: 'Bearer ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SnpkV0lpT2lKaGNIQmZkRzlyWlc0aUxDSmhkV1FpT2lJMU1UZzFZV0k1TWpRNE1qVmlZV1EwWmpaa1ltWXhORFE0TldReFpUQTJPVGsxTldOak16VTBZVE5pWWpGaE5XVm1OekJtTURCalpEWXdNR1prTlRkbE1qTXhabVkzT1daaE5UQmxZbUUyT0RCaU16TTJOalUxTnpReVltWXlaV1FpTENKelpYSjJhV05sWDNSdmEyVnVYM0IxWW14cFl5STZJalV4T0RWaFlqa3lORGd5TldKaFpEUm1ObVJpWmpFME5EZzFaREZsTURZNU9UVTFZMk16TlRSaE0ySmlNV0UxWldZM01HWXdNR05rTmpBd1ptUTFOMlV5TXpGbVpqYzVabUUxTUdWaVlUWTRNR0l6TXpZMk5UVTNOREppWmpKbFpDSXNJbkJsY20xcGMzTnBiMjV6SWpwN2ZTd2lhV0YwSWpveE5USXlNVGs0TlRNM0xDSmxlSEFpT2pRMk56VTNPVGcxTXpkOS5vLWFzMXVJd3EyUWs3b1NWeFpqbHhiUVZxQ3NzbmFKT1Vucmo4bFppOXJz',
        perfilInstagram: 'https://dev-kairos-ig.karmapulse.com/v1/datasets/historical',
        perfilInstagramToken: 'Bearer ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SnpkV0lpT2lKaGNIQmZkRzlyWlc0aUxDSmhkV1FpT2lJMVpqQmpNMkUxTmpJMU5URXdNR015T1dZeFl6TTRZamRsWkRBNU1EWXdaV1ZtWVRCbU9XRTBPVFExWmpZek5UazRNelUyT0dVd01qTXdNMkV3WldGbU5qVTJZVFJpTldVMU0yVXpaV0kxTXpsbE5UZ3lNMk0xWVRVNE9EZGtabVVpTENKelpYSjJhV05sWDNSdmEyVnVYM0IxWW14cFl5STZJalZtTUdNellUVTJNalUxTVRBd1l6STVaakZqTXpoaU4yVmtNRGt3TmpCbFpXWmhNR1k1WVRRNU5EVm1Oak0xT1Rnek5UWTRaVEF5TXpBellUQmxZV1kyTlRaaE5HSTFaVFV6WlRObFlqVXpPV1UxT0RJell6VmhOVGc0TjJSbVpTSXNJbkJsY20xcGMzTnBiMjV6SWpwN0ltbHVjM1JoWjNKaGJTSTZXeUpwYm5OMFlXZHlZVzA2YzJWbElpd2lhVzV6ZEdGbmNtRnRPbU55WldGMFpTSXNJbWx1YzNSaFozSmhiVHBsWkdsMElpd2lhVzV6ZEdGbmNtRnRPbkpsYlc5MlpTSmRmU3dpYVdGMElqb3hOVFF6T0RVeE5ETXlMQ0psZUhBaU9qUTJPVGMwTlRFME16SjkuRVdPTkNvWTQ3MlNBVUEtSXFyR1hLZWU1OVZZUFJuWlpnQkxSTnZhaV9Xdw==',
    },
    fields: {
        visualization: 'line',
    },
    tw_search_id: '5c2f950edda98a00d879f4f3',
    fb_search_id: '5abad89a55c091000e145030',
    in_search_id: '5c537c2bbe7e240075546b36',
    dateRange: {
        startDate: moment('2018-07-01'),
        endDate: moment('2018-07-31'),
    }
};

storiesOf('MultiSourceTimeline', module)
    .addWithJSX(
        'Lista',
        withNotes(README)(() => (
            <div
                style={{ width: '500px' }}
            >
                <MultiSourceTimeline
                    {...params}
                />
            </div>
        ))
    );
