import React from 'react';
import { css } from 'glamor';
import { storiesOf } from '@storybook/react';

import TreeMapTrends from '../../lib/modules/TreeMapTrends';

import data from '../data';
import Container from '../container';

const { tw } = data;

const params = {
    fields: {
        search_id: tw.id,
        top_limit: 10,
        hasExport: true
    },
    dateRange: { ...tw.defaultDates }
};

const styles = css({
    ' .treemap-trends__footer': {
        display: 'flex'
    }
});

storiesOf('Treemap Trends', module)
    .add(
        'Treemap',
        (() => (
            <Container
                visualization="treemap_top_trends"
                childrenParams={params}
                styles={styles}
                showCtrRawData
            >
                <TreeMapTrends />
            </Container>
        ))
    );
