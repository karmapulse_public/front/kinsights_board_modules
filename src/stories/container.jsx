import React, { useState } from 'react';
import { css } from 'glamor';
import Toggle from 'kpulse_design_system/dist/Toggle';
import withExport from '../lib/helpers/withExportPDF';

const defaultStyles = css({
    display: 'flex',
    width: '100%',
    height: '100%',
    margin: '25px',
    border: '2px solid rgba(255, 255, 255, 0)',
    alignItems: 'center',
    justifyContent: 'center',
    transition: 'all ease 0.5s',
    '&:hover': {
        border: '2px solid rgba(255, 0, 0, 0.6)'
    },
    '&:first-child': {
        display: 'block',
        marginRight: 'auto',
    }
});

const setVisualization = (params, visualization, hasClick, fbUrl) => ({
    ...params,
    services: {
        twitter: 'https://serverless.karmapulse.com/insights/recipes-twitter',
        facebook: !fbUrl ? 'https://dev-serverless.karmapulse.com/elasticsearch-fb/query_recipe_facebook' : 'https://dev-serverless.karmapulse.com/insights/recipes-facebook',
        // perfilFacebookToken: 'Bearer ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SnpkV0lpT2lKaGNIQmZkRzlyWlc0aUxDSmhkV1FpT2lJMU1UZzFZV0k1TWpRNE1qVmlZV1EwWmpaa1ltWXhORFE0TldReFpUQTJPVGsxTldOak16VTBZVE5pWWpGaE5XVm1OekJtTURCalpEWXdNR1prTlRkbE1qTXhabVkzT1daaE5UQmxZbUUyT0RCaU16TTJOalUxTnpReVltWXlaV1FpTENKelpYSjJhV05sWDNSdmEyVnVYM0IxWW14cFl5STZJalV4T0RWaFlqa3lORGd5TldKaFpEUm1ObVJpWmpFME5EZzFaREZsTURZNU9UVTFZMk16TlRSaE0ySmlNV0UxWldZM01HWXdNR05rTmpBd1ptUTFOMlV5TXpGbVpqYzVabUUxTUdWaVlUWTRNR0l6TXpZMk5UVTNOREppWmpKbFpDSXNJbkJsY20xcGMzTnBiMjV6SWpwN2ZTd2lhV0YwSWpveE5USXlNVGs0TlRNM0xDSmxlSEFpT2pRMk56VTNPVGcxTXpkOS5vLWFzMXVJd3EyUWs3b1NWeFpqbHhiUVZxQ3NzbmFKT1Vucmo4bFppOXJz',
    },
    fields: {
        ...params.fields,
        visualization,
        hasClick
    }
});

const container = ({ children, styles, childrenParams, visualization, showCtrRawData, fbUrl }) => { // eslint-disable-line
    const [activeRaw, setActiveRaw] = useState(false);
    const toggleRawData = showCtrRawData ? (
        <Toggle
            activeColor="#58af6e"
            disabledColor="#0a1b2f"
            bgCircle="#1b314a"
            borderCircle="#394f69"
            textColor="#0a1b2f"
            onClick={() => setActiveRaw(!activeRaw)}
        >
            Data Dura
        </Toggle>
    ) : null;

    return (
        <React.Fragment>
            <div id="supercontainer" {...defaultStyles} {...styles}>
                { toggleRawData }
                {
                    withExport(children, {
                        ...setVisualization(
                            { ...childrenParams },
                            visualization,
                            activeRaw,
                            fbUrl
                        )})
                }
            </div>
        </React.Fragment>
    );
};

export default container;
