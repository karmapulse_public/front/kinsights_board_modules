import React from 'react';

import { storiesOf } from '@storybook/react';

import { css } from 'glamor';

import TimelineTrends from '../../lib/modules/TimelineTrends';

import data from '../data';
import Container from '../container';

const { tw } = data;

const params = {
    fields: {
        search_id: tw.id,
        // search_entities: 'coca, cocacola, refresco, pepsi',
        query_type: 'top',
        entity_type: 'keywords',
        hasExport: true
    },
    dateRange: { ...tw.defaultDates },
    color: '#380000',
};

const styles = css({
    display: 'block',
    width: '550px',
    height: '250px',
    ' .chart-line': {
        height: '250px !important',
    }
});

storiesOf('TimeLine Trends', module)
    .add(
        'Line',
        (() => (
            <Container
                styles={styles}
                visualization="trend_line_chart"
                childrenParams={params}
                showCtrRawData
            >
                <TimelineTrends />
            </Container>
        ))
    );
