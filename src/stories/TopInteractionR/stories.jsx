import React from 'react';

import { storiesOf } from '@storybook/react';
import { withNotes } from '@storybook/addon-notes';
import moment from 'moment';
import { css } from 'glamor';

import TotalSentiment from '../../lib/modules/TopOriginalInteraction';

import README from './README.md';

const styles = () => css({
    width: '351px',
    height: '209px',
    border: '1px solid red'
});
const params = {
    services: {
        twitter: 'https://dev-serverless.karmapulse.com/insights/recipes-twitter',
    },
    fields: {
        search_id: '5c336d01f114f600d7c66f96',
        visualization: 'slider_interactions',
    },
    dateRange: {
        startDate: moment('2018-12-08T15:34:00.000Z'),
        endDate: moment('2019-01-07T15:14:00.000Z'),
    },
    showNoDataYet: false
};


storiesOf('TopInteractionsR', module)
    .addWithJSX(
        'TopInteractionsR',
        withNotes(README)(() => (
            <div {...styles()}>
                <TotalSentiment
                    {...params}
                />
            </div>
        ))
    );
