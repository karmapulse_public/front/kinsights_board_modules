import moment from 'moment';

const data = {
    tw: {
        id: '6111aea158633500ed7cbf82',
        defaultDates: {
            startDate: moment('2021-07-11'),
            endDate: moment('2023-01-01'),
        }
    },
    fb: {
        id: '5fb7e31fb0770c008c2f0a7c',
        defaultDates: {
            startDate: moment('2019-08-28'),
            endDate: moment('2020-01-20'),
        }
    }
};

export default data;
