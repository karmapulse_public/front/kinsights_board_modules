import React from 'react';

import { storiesOf } from '@storybook/react';

import { css } from 'glamor';

import TimelineDistribution from '../../lib/modules/TimelineDistribution';

import data from '../data';
import Container from '../container';

const { tw } = data;

const params = {
    fields: {
        search_id: tw.id,
        entity: 'Tweets',
        lineColor: '#f5a623',
        hasExport: true
    },
    dateRange: { ...tw.defaultDates },
    color: '#380000'
};

const paramsSentiment = {
    fields: {
        search_id: tw.id,
        totalTitles: {
            pos: 'Positivo',
            ne: 'Neutral',
            neg: 'Negativo'
        },
        labelTitles: {
            x: 'PERIODO',
            y: 'NÚMERO DE PUBLICACIONES'
        },
    },
    dateRange: { ...tw.defaultDates },
    color: '#380000'
};

const styles = css({
    display: 'block',
    width: '550px',
    height: '250px',
    ' .chart-line': {
        height: '250px !important',
    }
});

storiesOf('TimeLine Distribution', module)
    .add(
        'Simple Line',
        (() => (
            <Container
                styles={styles}
                visualization="timeline_lines"
                childrenParams={params}
                showCtrRawData
            >
                <TimelineDistribution />
            </Container>
        ))
    )
    .add(
        'Line with Sentiment',
        (() => (
            <Container
                styles={styles}
                visualization="timeline_sentiment"
                childrenParams={paramsSentiment}
            >
                <TimelineDistribution />
            </Container>
        ))
    );

