import React from 'react';
import { css } from 'glamor';
import { storiesOf } from '@storybook/react';
import { withNotes } from '@storybook/addon-notes';
import moment from 'moment';

import TotalSentiment from '../../lib/modules/TopTweets';

import README from './README.md';

const styles = css({
    width: '535px',
    height: '501px',
    border: '1px solid red'
});

const params = {
    services: {
        twitter: 'https://dev-serverless.karmapulse.com/insights/recipes-twitter',
        facebook: 'https://dev-serverless.karmapulse.com/elasticsearch-fb/query_recipe_facebook'
    },
    fields: {
        search_id: '5cb0a5ba6ea92900d831394b',
        visualization: 'slider_top_tweets',
    },
    dateRange: {
        startDate: moment('2019-03-28'),
        endDate: moment('2019-04-12'),
    }
};

storiesOf('TopTweets', module)
    .addWithJSX(
        'Slider',
        withNotes(README)(() => (
            <div {...styles}>
                <TotalSentiment
                    {...params}
                />
            </div>
        ))
    );
