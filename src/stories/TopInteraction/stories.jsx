import React from 'react';

import { storiesOf } from '@storybook/react';

import { css } from 'glamor';

import TopInteraction from '../../lib/modules/TopInteraction';


import data from '../data';
import Container from '../container';

const { tw } = data;

const params = {
    fields: {
        search_id: tw.id,
    },
    dateRange: { ...tw.defaultDates }
};

storiesOf('Top Interaction', module)
    .add(
        'Block (SEGOB)',
        (() => (
            <Container
                visualization="slider_interactions"
                childrenParams={params}
                styles={css({
                    width: '351px',
                    height: '209px',
                })}
            >
                <TopInteraction />
            </Container>
        ))
    )