import React from 'react';

import { storiesOf } from '@storybook/react';
import { withNotes } from '@storybook/addon-notes';
import moment from 'moment';

import GeneralFeed from '../../lib/modules/TwitterFeedRemaster';

import README from './README.md';

const params = {
    services: {
        twitter: 'https://dev-serverless.karmapulse.com/insights/recipes-twitter'
    },
    fields: {
        visualization: 'list_tweets',
        search_id: '5c2fc30cf114f600d7c66f92',
    },
    filters: {
        type: 'original'
    },
    dateRange: {
        startDate: moment('2019-01-04'),
        endDate: moment('2019-01-03'),
    }
};

storiesOf('TwitterFeed', module)
    .addWithJSX(
        'Lista',
        withNotes(README)(() => (
            <GeneralFeed
                {...params}
            />
        ))
    );
