import React from 'react';

import { storiesOf } from '@storybook/react';
import { withNotes } from '@storybook/addon-notes';
import moment from 'moment';

import GeneralFeed from '../../lib/modules/GeneralFeed';

import README from './README.md';

const params = {
    services: {
        twitter: 'https://serverless.karmapulse.com/insights/recipes-twitter',
        facebook: 'https://serverless.karmapulse.com/elasticsearch-fb/query_recipe_facebook',
        perfilFacebook: 'https://kairos-fb.karmapulse.com/v1/datasets/historical',
        perfilFacebookToken: 'Bearer ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SnpkV0lpT2lKaGNIQmZkRzlyWlc0aUxDSmhkV1FpT2lKbE5UZG1ZMkkyTVdaa05USTFaalJpTm1aaU1EUTNNVE15TVRVeU1XTTJNV1ZtWWpaaU56YzJNVGs1TVRNelpUbG1NR1V6TTJZeE9UZGhPREpqWWpGbU16UXpOV05qTkdSa09UazBaRE01TW1FNFpESmtaV0ZrWldOall6RXdZelFpTENKelpYSjJhV05sWDNSdmEyVnVYM0IxWW14cFl5STZJbVUxTjJaallqWXhabVExTWpWbU5HSTJabUl3TkRjeE16SXhOVEl4WXpZeFpXWmlObUkzTnpZeE9Ua3hNek5sT1dZd1pUTXpaakU1TjJFNE1tTmlNV1l6TkRNMVkyTTBaR1E1T1RSa016a3lZVGhrTW1SbFlXUmxZMk5qTVRCak5DSXNJbkJsY20xcGMzTnBiMjV6SWpwN2ZTd2lhV0YwSWpveE5USXlPRGcwTkRNekxDSmxlSEFpT2pRMk56WTBPRFEwTXpOOS5rZzduMmE5RTV4RzBsRWZKUnJtSDZMdG9nelBqUExaNjdsZHM2R01rQkE4',
        perfilInstagram: 'https://beta-kairos-ig.karmapulse.com/v1/datasets/historical',
        perfilInstagramToken: 'Bearer ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SnpkV0lpT2lKaGNIQmZkRzlyWlc0aUxDSmhkV1FpT2lJeU5tSmpZekEwTmpFeU5qTTRPRFZsTURreE1qTXlObUZoTkRNM01EazFZVEEwWkRVd05qYzVabVZtT0dZNU5HRmtNMkUwTXpsaVkyVmlaRE13TlRRek5XSXhNMk0wTTJFM09UTXdOVGd3TnpRMk9UWTJObUUwTTJObE56TmxNemtpTENKelpYSjJhV05sWDNSdmEyVnVYM0IxWW14cFl5STZJakkyWW1Oak1EUTJNVEkyTXpnNE5XVXdPVEV5TXpJMllXRTBNemN3T1RWaE1EUmtOVEEyTnpsbVpXWTRaamswWVdRellUUXpPV0pqWldKa016QTFORE0xWWpFell6UXpZVGM1TXpBMU9EQTNORFk1TmpZMllUUXpZMlUzTTJVek9TSXNJbkJsY20xcGMzTnBiMjV6SWpwN2ZTd2lhV0YwSWpveE5UWXdPREE1TURZeExDSmxlSEFpT2pRM01UUTBNRGt3TmpGOS5Gb2hUZGR0M1QxeVRBLWE0bG1iOUktUkk5TnVwSGRpejl4VTMwaUdSbDhV',
    },
    fields: {
        visualization: 'list',
    },
    filters: {
        type: 'original'
    },
    tw_search_id: '5fc6ac6991836600e38fe779',
    fb_search_id: '5fb7e31fb0770c008c2f0a7c',
    in_search_id: '5fb7e6a8fa933c009d2d809e',
    dateRange: {}
};

storiesOf('GeneralFeed', module)
    .addWithJSX(
        'Lista',
        withNotes(README)(() => (
            <GeneralFeed
                {...params}
            />
        ))
    )
    .addWithJSX(
        'For Screens',
        withNotes(README)(() => (
            <GeneralFeed
                {...{ ...params, fields: { visualization: 'screen' } }}
            />
        ))
    );
