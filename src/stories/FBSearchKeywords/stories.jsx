import React from 'react';
import { css } from 'glamor';
import { storiesOf } from '@storybook/react';

import FbSearchKeywords from '../../lib/modules/FbSearchKeywords';

import data from '../data';
import Container from '../container';

const { fb } = data;

const params = {
    fields: {
        search_id: fb.id,
        keywords: ['zelda', 'link', 'ganon', 'mario'],
        hasExport: true
    },
    dateRange: { ...fb.defaultDates },
    top: '5',
    showNoDataYet: false
};

const styles = css({
    width: '550px',
});

storiesOf('FBSearchKeywords', module)
    .add(
        'Line',
        (() => (
            <Container
                styles={styles}
                visualization="search_histogram"
                childrenParams={params}
            >
                <FbSearchKeywords />
            </Container>
        ))
    );
