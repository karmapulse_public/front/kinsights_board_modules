import React from 'react';

import { storiesOf } from '@storybook/react';

import TotalTweets from '../../lib/modules/TotalTweets';

import data from '../data';
import Container from '../container';

const { tw } = data;

const params = {
    fields: {
        search_id: tw.id,
        hasExport: true
    },
    dateRange: { ...tw.defaultDates }
};

storiesOf('TotalTweets', module)
    .add(
        'Barras',
        (() => (
            <Container
                visualization="card_horizontal_bars"
                childrenParams={params}
            >
                <TotalTweets />
            </Container>
        ))
    )
    .add(
        'Bloque',
        (() => (
            <Container
                visualization="card_total_numbers"
                childrenParams={params}
            >
                <TotalTweets />
            </Container>
        ))
    )
    .add(
        'Inline',
        (() => (
            <Container
                visualization="card_total_inline"
                childrenParams={params}
                showCtrRawData
            >
                <TotalTweets />
            </Container>
        ))
    );
