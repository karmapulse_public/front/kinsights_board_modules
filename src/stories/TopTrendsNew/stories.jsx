import React from 'react';

import { storiesOf } from '@storybook/react';

import TopTrendsNew from '../../lib/modules/TopTrendsNew';


import data from '../data';
import Container from '../container';

const { tw } = data;

const paramsList = {
    fields: {
        search_id: tw.id,
        top_limit: 20,
        hasExport: true,
        subtitle: {
            list: 'Temas',
            count: 'Menciones'
        }
    },
    dateRange: { ...tw.defaultDates },
    color: '#380000',
};

const paramsCloud = {
    fields: {
        search_id: tw.id,
        fontColor: ['#48a0ce'],
        top_limit: 20,
        width: 500,
        height: 500,
        outlineFn: '2 * n + 1',
        outlineColor: '#F00',
        hasExport: true
    },
    dateRange: { ...tw.defaultDates },
};

storiesOf('TopTrends Remaster', module)
    .add(
        'List',
        (() => (
            <Container
                visualization="list_top_trends"
                childrenParams={paramsList}
                showCtrRawData
            >
                <TopTrendsNew />
            </Container>
        ))
    )
    .add(
        'List with sentiment',
        (() => (
            <Container
                visualization="list_top_trends_sentiment"
                childrenParams={paramsList}
            >
                <TopTrendsNew />
            </Container>
        ))
    )
    .add(
        'Word Cloud',
        (() => (
            <Container
                visualization="wordclouds_top_trends"
                childrenParams={paramsCloud}
                showCtrRawData
            >
                <TopTrendsNew />
            </Container>
        ))
    );
