import React from 'react';

import { storiesOf } from '@storybook/react';

import TotalSentiment from '../../lib/modules/TotalSentiment';


import data from '../data';
import Container from '../container';

const { tw } = data;

const params = {
    fields: {
        search_id: tw.id,
        hasExport: true,
        sentiment_labels: {
            negativo: 'Negativo',
            neutral: 'Neutral',
            positivo: 'Positivo'
        },
    },
    dateRange: { ...tw.defaultDates }
};

storiesOf('TotalSentiment', module)
    .add(
        'Bar',
        (() => (
            <Container
                visualization="bar_chart"
                childrenParams={params}
                showCtrRawData
            >
                <TotalSentiment />
            </Container>
        ))
    )
    .add(
        'Inline',
        (() => (
            <Container
                visualization="card_sentiment_inlne"
                childrenParams={params}
                showCtrRawData
            >
                <TotalSentiment />
            </Container>
        ))
    )
    .add(
        'Pie',
        (() => (
            <Container
                visualization="pie_chart"
                childrenParams={params}
            >
                <TotalSentiment />
            </Container>
        ))
    )
    .add(
        'Pie Remaster',
        (() => (
            <Container
                visualization="pie_chart_remaster"
                childrenParams={params}
            >
                <TotalSentiment />
            </Container>
        ))
    );
