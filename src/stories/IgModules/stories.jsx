import React from 'react';

import { storiesOf } from '@storybook/react';
import { withNotes } from '@storybook/addon-notes';
import moment from 'moment';
import { css } from 'glamor';

import TotalSentiment from '../../lib/modules/IgTotalInteraction';
import IgTopImages from '../../lib/modules/IgTopImages';

import README from './README.md';

const styles = () => css({
    width: '351px',
    height: '209px',
    border: '1px solid red'
});
const styles2 = () => css({
    width: '535px',
    height: '501px',
    border: '1px solid red'
});
const params = {
    services: {
        twitter: 'https://dev-serverless.karmapulse.com/insights/recipes-twitter',
        facebook: 'https://dev-serverless.karmapulse.com/elasticsearch-fb/query_recipe_facebook',
        perfilFacebook: 'https://dev-kairos-fb.karmapulse.com/v1/datasets/historical',
        perfilFacebookToken: 'Bearer ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SnpkV0lpT2lKaGNIQmZkRzlyWlc0aUxDSmhkV1FpT2lJMU1UZzFZV0k1TWpRNE1qVmlZV1EwWmpaa1ltWXhORFE0TldReFpUQTJPVGsxTldOak16VTBZVE5pWWpGaE5XVm1OekJtTURCalpEWXdNR1prTlRkbE1qTXhabVkzT1daaE5UQmxZbUUyT0RCaU16TTJOalUxTnpReVltWXlaV1FpTENKelpYSjJhV05sWDNSdmEyVnVYM0IxWW14cFl5STZJalV4T0RWaFlqa3lORGd5TldKaFpEUm1ObVJpWmpFME5EZzFaREZsTURZNU9UVTFZMk16TlRSaE0ySmlNV0UxWldZM01HWXdNR05rTmpBd1ptUTFOMlV5TXpGbVpqYzVabUUxTUdWaVlUWTRNR0l6TXpZMk5UVTNOREppWmpKbFpDSXNJbkJsY20xcGMzTnBiMjV6SWpwN2ZTd2lhV0YwSWpveE5USXlNVGs0TlRNM0xDSmxlSEFpT2pRMk56VTNPVGcxTXpkOS5vLWFzMXVJd3EyUWs3b1NWeFpqbHhiUVZxQ3NzbmFKT1Vucmo4bFppOXJz',
        perfilInstagram: 'https://dev-kairos-ig.karmapulse.com/v1/datasets/historical',
        perfilInstagramToken: 'Bearer ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SnpkV0lpT2lKaGNIQmZkRzlyWlc0aUxDSmhkV1FpT2lJMVpqQmpNMkUxTmpJMU5URXdNR015T1dZeFl6TTRZamRsWkRBNU1EWXdaV1ZtWVRCbU9XRTBPVFExWmpZek5UazRNelUyT0dVd01qTXdNMkV3WldGbU5qVTJZVFJpTldVMU0yVXpaV0kxTXpsbE5UZ3lNMk0xWVRVNE9EZGtabVVpTENKelpYSjJhV05sWDNSdmEyVnVYM0IxWW14cFl5STZJalZtTUdNellUVTJNalUxTVRBd1l6STVaakZqTXpoaU4yVmtNRGt3TmpCbFpXWmhNR1k1WVRRNU5EVm1Oak0xT1Rnek5UWTRaVEF5TXpBellUQmxZV1kyTlRaaE5HSTFaVFV6WlRObFlqVXpPV1UxT0RJell6VmhOVGc0TjJSbVpTSXNJbkJsY20xcGMzTnBiMjV6SWpwN0ltbHVjM1JoWjNKaGJTSTZXeUpwYm5OMFlXZHlZVzA2YzJWbElpd2lhVzV6ZEdGbmNtRnRPbU55WldGMFpTSXNJbWx1YzNSaFozSmhiVHBsWkdsMElpd2lhVzV6ZEdGbmNtRnRPbkpsYlc5MlpTSmRmU3dpYVdGMElqb3hOVFF6T0RVeE5ETXlMQ0psZUhBaU9qUTJPVGMwTlRFME16SjkuRVdPTkNvWTQ3MlNBVUEtSXFyR1hLZWU1OVZZUFJuWlpnQkxSTnZhaV9Xdw==',

    },
    fields: {
        search_id: '5c6b34e9822f66008b63135b',
        visualization: 'slider_interactions',
    },
    dateRange: {
        startDate: moment('2019-07-25T05:00:00.000Z'),
        endDate: moment('2019-08-03T04:10:00.000Z'),
    },
    showNoDataYet: false
};

const paramsTotalInteractions = {
    ...params,
    fields: {
        ...params.fields,
        visualization: 'slider_top_posts',
        top: 5
    }
};
storiesOf('IgModules', module)
    .addWithJSX(
        'IgSentiment',
        withNotes(README)(() => (
            <div {...styles()}>
                <TotalSentiment
                    {...params}
                />
            </div>
        ))
    )
    .addWithJSX(
        'IgTopPost',
        withNotes(README)(() => (
            <div {...styles2()}>
                <IgTopImages
                    {...paramsTotalInteractions}
                />
            </div>
        ))
    );
