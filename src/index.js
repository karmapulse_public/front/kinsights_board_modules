import mapModuleType, { renderComponentByPosition, renderComponents } from './lib/mapModuleType';

export { mapModuleType as default, renderComponentByPosition, renderComponents };
