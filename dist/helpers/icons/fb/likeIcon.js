/**
 * LikeIcon SVG
 */

import React from 'react';

var LikeIcon = function LikeIcon(props) {
    return (
        /* eslint-disable */
        React.createElement(
            "svg",
            { xmlns: "http://www.w3.org/2000/svg", width: "23", height: "23", viewBox: "0 0 23 23" },
            React.createElement(
                "g",
                { fill: "none", fillRule: "evenodd" },
                React.createElement("circle", { cx: "11.5", cy: "11.5", r: "11.5", fill: "#548CFF" }),
                React.createElement("path", { fill: "#FFF", d: "M15.75 8.679a1.44 1.44 0 0 1 .1-.004c.724 0 1.312.542 1.312 1.211 0 .393-.203.742-.517.963a.99.99 0 0 1 .517.853c0 .439-.309.812-.74.951.322.177.538.498.538.865 0 .45-.326.833-.776.962a.782.782 0 0 1 .372.652c0 .415-.353.757-.807.802v.005H12.42a3.328 3.328 0 0 1-3.329-3.334v-.933c0-.902.4-1.71 1.031-2.26-.612-.073-1.035-.261-.942-.434.223-.415 2.42-2.421 2.522-3.127.1-.706.373-1.816.821-1.816h.135c.593 0 1.124.48 1.158 1.07 0 0 .32 2.347-.891 3.57h2.824v.004zM5.245 10.9a1.412 1.412 0 1 1 2.824 0v4.428a1.412 1.412 0 1 1-2.824 0V10.9z" })
            )
        )
        /* eslint-enable */

    );
};

export default LikeIcon;