/**
 * WowIcon SVG
 */

import React from 'react';

var WowIcon = function WowIcon(props) {
    return (
        /* eslint-disable */
        React.createElement(
            "svg",
            { xmlns: "http://www.w3.org/2000/svg", width: "23", height: "23", viewBox: "0 0 23 23" },
            React.createElement(
                "g",
                { fill: "none", fillRule: "evenodd" },
                React.createElement("circle", { cx: "11.5", cy: "11.5", r: "11.5", fill: "#FFDA69" }),
                React.createElement("ellipse", { cx: "11.5", cy: "15.939", fill: "#252B37", rx: "3.43", ry: "4.439" }),
                React.createElement("path", { fill: "#252B37", d: "M17.35 10.86c-.987.174-1.968-.708-2.19-1.97-.223-1.262.397-2.426 1.384-2.6.988-.175 1.97.707 2.192 1.97.222 1.261-.398 2.425-1.386 2.6zm-11.527.078c-.987-.174-1.608-1.339-1.385-2.6.222-1.262 1.203-2.144 2.191-1.97.988.174 1.608 1.338 1.385 2.6-.222 1.262-1.203 2.144-2.19 1.97zm12.85-7.929a.504.504 0 0 1-.736.69 1.839 1.839 0 0 0-1.252-.582c-.44-.023-.863.109-1.207.368a.504.504 0 1 1-.607-.805 2.846 2.846 0 0 1 1.867-.57c.747.038 1.434.364 1.935.899zm-12.379.197a1.839 1.839 0 0 0-1.251.581.504.504 0 0 1-.736-.69c.5-.534 1.188-.86 1.934-.899.68-.035 1.336.17 1.868.57a.504.504 0 1 1-.608.806 1.837 1.837 0 0 0-1.207-.368z" })
            )
        )
        /* eslint-enable */

    );
};

export default WowIcon;