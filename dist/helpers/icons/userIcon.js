/**
 * UserIcon SVG
 */

import React from 'react';

var UserIcon = function UserIcon(props) {
    return (
        /* eslint-disable */
        React.createElement(
            "svg",
            { xmlns: "http://www.w3.org/2000/svg", width: "14", height: "14", viewBox: "0 0 14 14" },
            React.createElement(
                "g",
                { fill: props.color, fillRule: "evenodd" },
                React.createElement("path", { d: "M7 7c1.93 0 3.5-1.57 3.5-3.5S8.93 0 7 0 3.5 1.57 3.5 3.5 5.07 7 7 7M7 7.875c-4.121 0-7 2.159-7 5.25V14h14v-.875c0-3.091-2.879-5.25-7-5.25" })
            )
        )

        /* eslint-enable */

    );
};

export default UserIcon;