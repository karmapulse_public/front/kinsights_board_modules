/**
 * DialogIcon SVG
 */

import React from 'react';

var noDataYet = function noDataYet() {
    return (
        /* eslint-disable */
        React.createElement(
            "svg",
            { xmlns: "http://www.w3.org/2000/svg", width: "39", height: "39", viewBox: "0 0 39 39" },
            React.createElement(
                "g",
                { fill: "none", fillRule: "nonzero" },
                React.createElement("path", { fill: "#858585", d: "M0 28h6v11H0z" }),
                React.createElement("path", { fill: "#C6C6C6", d: "M11 39V10h6v29z" }),
                React.createElement("path", { fill: "#ACACAC", d: "M22 39V0h6v39z" }),
                React.createElement("path", { fill: "#D4D4D4", d: "M33 39V20h6v19z" })
            )
        )

        /* eslint-enable */

    );
};

export default noDataYet;