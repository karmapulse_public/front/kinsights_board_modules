/**
 * LocationIcon SVG
 */

import React from 'react';

var LocationIcon = function LocationIcon(active) {
    return (
        /* eslint-disable */
        React.createElement(
            "svg",
            { width: "9px", height: "16px", viewBox: "0 0 9 16", version: "1.1", xmlns: "http://www.w3.org/2000/svg", xmlnsXlink: "http://www.w3.org/1999/xlink" },
            React.createElement(
                "g",
                { id: "Page-1", stroke: "none", strokeWidth: "1", fill: "none", fillRule: "evenodd" },
                React.createElement(
                    "g",
                    { id: "GraficasCandidato-768", transform: "translate(-109.000000, -77.000000)", fill: "#FFFFFF" },
                    React.createElement(
                        "g",
                        { id: "time_graph-copy-2", transform: "translate(89.000000, 57.000000)" },
                        React.createElement(
                            "g",
                            { id: "Fill-522-+-Fill-523", transform: "translate(20.000000, 20.000000)" },
                            React.createElement("path", { d: "M2.625375,4.57310476 C2.625375,3.52167619 3.465375,2.66834286 4.500375,2.66834286 C5.535375,2.66834286 6.375375,3.52167619 6.375375,4.57310476 C6.375375,5.62453333 5.535375,6.47786667 4.500375,6.47786667 C3.465375,6.47786667 2.625375,5.62453333 2.625375,4.57310476 M9.000375,4.57310476 C9.000375,2.04815238 6.985125,0.00167619048 4.500375,0.00167619048 C2.014875,0.00167619048 0.000375,2.04815238 0.000375,4.57310476 C0.000375,7.62072381 4.500375,12.9540571 4.500375,12.9540571 C4.500375,12.9540571 9.000375,7.62072381 9.000375,4.57310476", id: "Fill-522" }),
                            React.createElement("polygon", { id: "Fill-523", points: "1.50075 15.239619 7.50075 15.239619 7.50075 13.7158095 1.50075 13.7158095" })
                        )
                    )
                )
            )
        )
        /* eslint-enable */

    );
};

export default LocationIcon;