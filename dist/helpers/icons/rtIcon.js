/**
 * RtIcon SVG
 */

import React from 'react';

var RtIcon = function RtIcon(props) {
    return (
        /* eslint-disable */
        React.createElement(
            "svg",
            { xmlns: "http://www.w3.org/2000/svg", width: "25", height: "13", viewBox: "0 0 25 13" },
            React.createElement("path", { fill: props.color, fillRule: "evenodd", d: "M15.285.369H4.849c-.72 0-1.303.568-1.304 1.27l-.007 4.498H1.892c-.861 0-1.214.594-.783 1.321l2.958 4.997c.43.726 1.135.726 1.565 0L8.59 7.458c.43-.727.078-1.321-.783-1.321h-1.66l.005-3.225h9.133c.72 0 1.304-.569 1.304-1.271 0-.703-.584-1.272-1.304-1.272zM9.865 13H20.3c.72 0 1.304-.568 1.305-1.27l.007-4.498h1.646c.861 0 1.213-.595.783-1.322L21.083.914c-.43-.727-1.135-.727-1.565 0L16.56 5.91c-.43.727-.078 1.322.783 1.322h1.66l-.005 3.224H9.865c-.72 0-1.304.57-1.304 1.272 0 .703.584 1.272 1.304 1.272z" })
        )
        /* eslint-enable */

    );
};

export default RtIcon;