/**
 * NoUserIcon SVG
 */

import React from 'react';
/* eslint-disable */

var NoUserIcon = function NoUserIcon() {
    return React.createElement(
        "svg",
        { width: "48px", height: "48px", viewBox: "0 0 48 48", version: "1.1", xmlns: "http://www.w3.org/2000/svg", xmlnsXlink: "http://www.w3.org/1999/xlink" },
        React.createElement(
            "g",
            { id: "Exploracion-/-Graficas", stroke: "none", strokeWidth: "1", fill: "none", fillRule: "evenodd" },
            React.createElement(
                "g",
                { id: "Datasets---exploracion", transform: "translate(-373.000000, -410.000000)" },
                React.createElement(
                    "g",
                    { id: "Page-1", transform: "translate(373.000000, 410.000000)" },
                    React.createElement("polygon", { id: "Fill-1", fill: "#3949AB", points: "0 47.7701647 47.7703529 47.7701647 47.7703529 -0.000188235294 0 -0.000188235294" }),
                    React.createElement("path", { d: "M23.8850824,31.3179294 C18.8479059,31.3179294 8.79143529,33.8459294 8.79143529,38.8642824 L8.79143529,42.6374588 L38.9787294,42.6374588 L38.9787294,38.8642824 C38.9787294,33.8459294 28.9222588,31.3179294 23.8850824,31.3179294 M23.8850824,27.5447529 C28.0544941,27.5447529 31.4314353,24.1678118 31.4314353,19.9984 C31.4314353,15.8280471 28.0544941,12.4511059 23.8850824,12.4511059 C19.7156706,12.4511059 16.3387294,15.8280471 16.3387294,19.9984 C16.3387294,24.1678118 19.7156706,27.5447529 23.8850824,27.5447529", id: "Fill-2", fill: "#FFFFFF" })
                )
            )
        )
    );
};
/* eslint-enable */

export default NoUserIcon;