/**
 * DialogIcon SVG
 */

import React from 'react';

var AnalysisIcon = function AnalysisIcon(props) {
    return (
        /* eslint-disable */
        React.createElement(
            "svg",
            { xmlns: "http://www.w3.org/2000/svg", width: "20", height: "18", viewBox: "0 0 20 18" },
            React.createElement("path", { fill: props.color, fillRule: "nonzero", d: "M14.137.025A5.897 5.897 0 0 0 9.75 2.041 5.897 5.897 0 0 0 5.362.025C2.34.025 0 2.33 0 5.305c0 3.648 3.315 6.624 8.385 11.04l1.365 1.248 1.365-1.248c4.972-4.512 8.385-7.488 8.385-11.04 0-2.976-2.34-5.28-5.363-5.28zm-4.29 14.976H9.75l-.098-.096c-4.68-4.128-7.702-6.816-7.702-9.6 0-1.92 1.463-3.36 3.412-3.36 1.463 0 2.925.96 3.51 2.304h1.853c.488-1.344 1.95-2.304 3.412-2.304 1.95 0 3.413 1.44 3.413 3.36 0 2.784-3.023 5.472-7.703 9.696z" })
        )

        /* eslint-enable */

    );
};

export default AnalysisIcon;