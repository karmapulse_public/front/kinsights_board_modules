import React from 'react';

var LikesIcon = function LikesIcon() {
    return (
        /* eslint-disable */
        React.createElement(
            "svg",
            { xmlns: "http://www.w3.org/2000/svg", width: "18", height: "15", viewBox: "0 0 18 15" },
            React.createElement("path", { fill: "none", fillRule: "evenodd", stroke: "#FFF", strokeLinecap: "round", strokeLinejoin: "round", d: "M15.328 2.672a4 4 0 0 0-5.657 0c-.28.28-.491.598-.671.929a3.948 3.948 0 0 0-.672-.93 4 4 0 0 0-5.657 5.657L9 14.5l6.328-6.172a3.997 3.997 0 0 0 0-5.656z" })
        )
        /* eslint-enable */

    );
};

export default LikesIcon;