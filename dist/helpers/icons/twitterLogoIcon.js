import React from 'react';

var TwitterLogoIcon = function TwitterLogoIcon() {
    return (
        /* eslint-disable */
        React.createElement(
            "svg",
            { xmlns: "http://www.w3.org/2000/svg", width: "9", height: "9", viewBox: "0 0 9 9" },
            React.createElement("path", { fill: "#FFF", fillRule: "nonzero", d: "M4.5 0A4.505 4.505 0 0 0 0 4.5C0 6.981 2.019 9 4.5 9S9 6.981 9 4.5 6.981 0 4.5 0zm2.007 3.47c.002.045.004.09.004.135 0 1.368-1.042 2.946-2.948 2.946a2.93 2.93 0 0 1-1.587-.465 2.098 2.098 0 0 0 1.533-.429 1.037 1.037 0 0 1-.967-.72 1.044 1.044 0 0 0 .467-.017 1.036 1.036 0 0 1-.83-1.015v-.013c.14.077.299.124.469.13a1.035 1.035 0 0 1-.32-1.384 2.94 2.94 0 0 0 2.134 1.083 1.036 1.036 0 0 1 1.765-.945c.236-.047.457-.132.658-.252a1.04 1.04 0 0 1-.456.574c.21-.025.41-.08.595-.163-.138.207-.314.39-.517.535z" })
        )
        /* eslint-enable */

    );
};

export default TwitterLogoIcon;