/**
 * Twitter SVG
 */

import React from 'react';
import lightenDarkenColors from '../lightenDarkenColors';

var Icon = function Icon(_ref) {
    var color = _ref.color,
        value = _ref.value,
        styles = _ref.styles;
    return React.createElement(
        'svg',
        { xmlns: 'http://www.w3.org/2000/svg', width: '42', height: '42', viewBox: '0 0 42 42' },
        React.createElement(
            'g',
            { fill: 'none', fillRule: 'evenodd', transform: 'translate(1 1)' },
            React.createElement('circle', { cx: '20', cy: '20', r: '20', fill: color, stroke: lightenDarkenColors(color, -30), strokeWidth: '2' }),
            React.createElement(
                'text',
                Object.assign({}, styles, { textAnchor: 'middle' }),
                React.createElement(
                    'tspan',
                    { x: '46%', y: '55%' },
                    value
                )
            )
        )
    );
};
/* eslint-enable */

export default Icon;