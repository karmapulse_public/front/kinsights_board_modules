/**
 * NoData SVG
 */

import React from 'react';

var NoData = function NoData(props) {
    return (
        /* eslint-disable */
        React.createElement(
            "svg",
            { xmlns: "http://www.w3.org/2000/svg", width: "500", height: "300", viewBox: "0 0 500 300" },
            React.createElement(
                "g",
                { fill: "none", fillRule: "evenodd" },
                React.createElement("path", { fill: "#D6D6D6", d: "M0 0h500v300H0z" }),
                React.createElement(
                    "text",
                    { fill: "#000", fontFamily: "Roboto-Regular, Roboto", fontSize: "14", opacity: ".5" },
                    React.createElement(
                        "tspan",
                        { x: "183.264", y: "183" },
                        "Imagen no disponible"
                    )
                ),
                React.createElement("path", { fill: "#000", fillRule: "nonzero", d: "M274.281 109H224.72c-3.159 0-5.719 2.574-5.719 5.75v34.5c0 3.176 2.56 5.75 5.719 5.75h49.562c3.159 0 5.719-2.574 5.719-5.75v-34.5c0-3.176-2.56-5.75-5.719-5.75zm-.715 40.25h-48.132a.717.717 0 0 1-.715-.719V115.47c0-.397.32-.719.715-.719h48.132c.395 0 .715.322.715.719v33.062c0 .397-.32.719-.715.719zm-39.316-29.708c-2.632 0-4.766 2.145-4.766 4.791 0 2.647 2.134 4.792 4.766 4.792s4.766-2.145 4.766-4.792c0-2.646-2.134-4.791-4.766-4.791zm-3.812 23.958h38.125v-9.583l-10.427-10.484a1.424 1.424 0 0 0-2.022 0l-14.239 14.317-4.708-4.734a1.424 1.424 0 0 0-2.022 0l-4.707 4.734v5.75z", opacity: ".3" })
            )
        )

        /* eslint-enable */

    );
};

export default NoData;