/**
 * RetweetIcon SVG
 */

import React from 'react';

var RetweetIcon = function RetweetIcon() {
    var color = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'none';
    return (
        /* eslint-disable */
        React.createElement(
            'svg',
            { width: '13px', height: '12px', viewBox: '0 0 13 12', version: '1.1', xmlns: 'http://www.w3.org/2000/svg', xmlnsXlink: 'http://www.w3.org/1999/xlink' },
            React.createElement(
                'g',
                { id: 'Page-1', stroke: 'none', strokeWidth: '1', fill: color, fillRule: 'evenodd' },
                React.createElement(
                    'g',
                    { id: 'Versus-768', transform: 'translate(-373.000000, -1354.000000)', fill: '#FFFFFF' },
                    React.createElement(
                        'g',
                        { id: 'CandidatoIzquierda', transform: 'translate(86.000000, 56.000000)' },
                        React.createElement(
                            'g',
                            { id: 'Galer\xEDa', transform: 'translate(0.000000, 1058.000000)' },
                            React.createElement(
                                'g',
                                { id: 'ImagenGaleria', transform: 'translate(0.000000, 37.000000)' },
                                React.createElement(
                                    'g',
                                    { id: 'TWEET', transform: 'translate(0.000000, 195.000000)' },
                                    React.createElement('path', { d: 'M295,8 L293.586,9.414 L295.586,11.414 L291,11.414 C288.794,11.414 287,13.208 287,15.414 C287,17.62 288.794,19.414 291,19.414 L292,19.414 L292,17.414 L291,17.414 C289.897,17.414 289,16.517 289,15.414 C289,14.312 289.897,13.414 291,13.414 L295.586,13.414 L293.586,15.414 L295,16.828 L299.414,12.414 L295,8 Z', id: 'Fill-386' })
                                )
                            )
                        )
                    )
                )
            )
        )
        /* eslint-enable */

    );
};

export var RetweetIconM = function RetweetIconM() {
    return React.createElement(
        'svg',
        { xmlns: 'http://www.w3.org/2000/svg', width: '16', height: '12', viewBox: '0 0 16 12' },
        React.createElement(
            'defs',
            null,
            React.createElement('path', { id: 'a', d: 'M4.986 0H0v4.797h4.986V0z' }),
            React.createElement('path', { id: 'c', d: 'M0 5h5V0H0v5z' })
        ),
        React.createElement(
            'g',
            { fill: 'none', fillRule: 'evenodd' },
            React.createElement('path', { fill: '#000', d: 'M11 12H5.253C3.459 12 2 10.619 2 8.92V4h1.169v4.92c0 1.088.935 1.973 2.084 1.973H11V12z' }),
            React.createElement(
                'g',
                null,
                React.createElement(
                    'mask',
                    { id: 'b', fill: '#fff' },
                    React.createElement('use', { xlinkHref: '#a' })
                ),
                React.createElement('path', { fill: '#000', d: 'M4.986 4.797L2.493 0 0 4.797z', mask: 'url(#b)' })
            ),
            React.createElement('path', { fill: '#000', d: 'M14 8h-1.169V3.103c0-1.101-.946-1.996-2.108-1.996H5V0h5.723C12.53 0 14 1.392 14 3.103V8z' }),
            React.createElement(
                'g',
                { transform: 'translate(11 7)' },
                React.createElement(
                    'mask',
                    { id: 'd', fill: '#fff' },
                    React.createElement('use', { xlinkHref: '#c' })
                ),
                React.createElement('path', { fill: '#000', d: 'M0 0l2.5 5L5 0z', mask: 'url(#d)' })
            )
        )
    );
};

export default RetweetIcon;