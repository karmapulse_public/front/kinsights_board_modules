/**
 * VerifiedIcon SVG
 */

import React from 'react';

var VerifiedIcon = function VerifiedIcon(props) {
    return (
        /* eslint-disable */
        React.createElement(
            "svg",
            { xmlns: "http://www.w3.org/2000/svg", width: "12", height: "12", viewBox: "0 0 12 12" },
            React.createElement(
                "g",
                { fill: "none", fillRule: "evenodd" },
                React.createElement("circle", { cx: "6", cy: "6", r: "6", fill: "#4990E2" }),
                React.createElement("path", { fill: "#FFF", d: "M5.821 8.494L3.2 6.528l.791-1.055 1.597 1.198L8.353 3.2l1.031.822z" })
            )
        )

        /* eslint-enable */

    );
};

export default VerifiedIcon;