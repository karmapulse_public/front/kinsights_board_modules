export function numberWithCommas(num) {
    var number = num || 0;
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

export function nFormatter(num) {
    if (num >= 1000000000) {
        var fnum = (num / 1000000000).toFixed(1).replace(/\.0$/, '');
        return numberWithCommas(fnum) + 'G';
    }
    if (num >= 1000000) {
        var _fnum = (num / 1000000).toFixed(1).replace(/\.0$/, '');
        return numberWithCommas(_fnum) + 'M';
    }
    if (num >= 1000) {
        var _fnum2 = (num / 1000).toFixed(1).replace(/\.0$/, '');
        return numberWithCommas(_fnum2) + 'K';
    }
    return num;
}

export function getPercentage(n, total) {
    if (total === 0) {
        return 0;
    }
    var percentage = n * 100 / total;
    return Math.round(percentage * 100) / 100;
}