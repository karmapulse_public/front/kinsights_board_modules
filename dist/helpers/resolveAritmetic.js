function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var operators = {
    '+': { value: 1, op: function op(v1, v2) {
            return v1 + v2;
        } },
    '-': { value: 1, op: function op(v1, v2) {
            return v1 - v2;
        } },
    '*': { value: 2, op: function op(v1, v2) {
            return v1 * v2;
        } },
    '/': { value: 2, op: function op(v1, v2) {
            return v1 / v2;
        } },
    '^': { value: 3, op: function op(v1, v2) {
            return Math.pow(v1, v2);
        } }
};

var isOperator = function isOperator(character) {
    return operators[character];
};

var greaterPrecedence = function greaterPrecedence(op1, op2) {
    return (// first one greater?
        operators[op1].value - operators[op2].value >= 0
    );
};

export var toPostOrder = function toPostOrder(chain) {
    var stack = [];
    var outList = [];
    var chainP = chain.split(' ');
    while (chainP.length !== 0) {
        var actualChar = chainP[0];
        chainP.shift();
        // Es Número
        if (parseInt(actualChar, 10)) {
            outList.push(parseInt(actualChar, 10));
            // Es incognita
        } else if (actualChar === 'n') {
            outList.push('n');
            // Es parentesis izquierdo
        } else if (actualChar === '(') {
            stack.unshift(actualChar);
            // Es parentesis derecho
        } else if (actualChar === ')') {
            while (stack.length !== 0 && stack[0] !== '(') {
                outList.push(stack[0]);
                stack.shift();
            }
            if (stack[0] === '(') {
                stack.shift();
            }
            // Es parentesis operador
        } else if (isOperator(actualChar)) {
            while (stack.length !== 0 && isOperator(stack[0]) && greaterPrecedence(stack[0], actualChar)) {
                outList.push(stack[0]);
                stack.shift();
            }
            stack.unshift(actualChar);
        }
    }
    while (stack.length !== 0) {
        outList.push(stack[0]);
        stack.shift();
    }
    return outList;
};

export var evalAritmetic = function evalAritmetic(postOrderT, value) {
    var postOrder = [].concat(_toConsumableArray(postOrderT));
    var i = 0;
    while (postOrder.length !== 1) {
        var actualChar = postOrder[i];
        // Es incognita
        if (actualChar === 'n') {
            postOrder[i] = value;
            i += 1;
            // Es operador
        } else if (isOperator(actualChar)) {
            var resul = operators[actualChar].op(postOrder[i - 2], postOrder[i - 1]);
            postOrder.splice(i - 2, 3, resul);
            i -= 1;
        } else {
            i += 1;
        }
    }
    return postOrder[0];
};