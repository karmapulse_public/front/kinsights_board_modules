import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFont, faImage, faVideo } from '@fortawesome/free-solid-svg-icons';

import Typography from 'kpulse_design_system/dist/Typography';
import upperFirst from 'lodash/upperFirst';
import moment from 'moment';

import styles from './FooterStyles';

var propTypes = {
    item: PropTypes.object,
    type: PropTypes.string,
    isScreen: PropTypes.bool
};

var defaultProps = {
    item: {},
    isScreen: false
};

var Header = function Header(props) {
    var Paragraph = Typography.Paragraph;
    var item = props.item,
        type = props.type,
        isScreen = props.isScreen;

    var stringTypeVideo = {
        VIDEO: '#444444',
        GIF: '#444444'
    };
    var stringTypeImage = {
        GIF: '#444444',
        IMAGE: '#444444',
        PHOTO: '#444444',
        CAROUSEL_ALBUM: '#444444'
    };

    if (isScreen) {
        return React.createElement(
            'article',
            styles,
            React.createElement(
                'div',
                { className: 'tag' },
                React.createElement(FontAwesomeIcon, { icon: faImage, color: stringTypeImage[type] || '#DDDDDD' }),
                React.createElement(FontAwesomeIcon, { icon: faVideo, color: stringTypeVideo[type] || '#DDDDDD' })
            ),
            React.createElement(
                'div',
                { className: 'dateTop' },
                React.createElement(
                    Paragraph,
                    { small: true, color: 'rgba(0, 0, 0, 0.5)' },
                    upperFirst(moment(item.posted_time).format('MMM D, YYYY • HH:mm'))
                )
            )
        );
    }
    return React.createElement(
        'article',
        styles,
        React.createElement(
            'div',
            { className: 'tag' },
            React.createElement(
                Paragraph,
                { small: true, bold: true, color: '#FFF' },
                stringType[type] ? stringType[type] : type
            )
        ),
        React.createElement(
            Paragraph,
            { small: true, color: 'rgba(0, 0, 0, 0.5)' },
            upperFirst(moment(item.posted_time).format('MMM D, YYYY • HH:mm'))
        )
    );
};

Header.propTypes = propTypes;
Header.defaultProps = defaultProps;

export default Header;