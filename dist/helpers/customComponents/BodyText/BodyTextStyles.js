import { css } from 'glamor';

var s = {
    instagram: '0 15px 0 15px',
    facebook: '0 30px 0 55px',
    twitter: '0 30px 0 55px'
};

var styles = function styles(type) {
    return css({
        padding: s[type],
        marginBottom: 10
    });
};
export default styles;