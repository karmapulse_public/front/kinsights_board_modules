import React from 'react';
import PropTypes from 'prop-types';
import Typography from 'kpulse_design_system/dist/Typography';

import styles from './BodyTextStyles';

var propTypes = {
    text: PropTypes.string,
    type: PropTypes.string
};

var defaultProps = {
    text: [],
    type: ''
};

var BodyText = function BodyText(_ref) {
    var text = _ref.text,
        type = _ref.type;
    var Paragraph = Typography.Paragraph;

    return React.createElement(
        Paragraph,
        { styles: styles(type) },
        text
    );
};

BodyText.propTypes = propTypes;
BodyText.defaultProps = defaultProps;

export default BodyText;