import React, { Children } from 'react';
import PropTypes from 'prop-types';

import styles from './CardStyles';

var propTypes = {
    children: PropTypes.array,
    backgroundImage: PropTypes.string,
    wTextDynamic: PropTypes.bool,
    isScreen: PropTypes.bool,
    link: PropTypes.string

};

var defaultProps = {
    children: [],
    backgroundImage: '',
    link: '',
    wTextDynamic: false,
    isScreen: false
};

var Card = function Card(_ref) {
    var children = _ref.children,
        backgroundImage = _ref.backgroundImage,
        wTextDynamic = _ref.wTextDynamic,
        isScreen = _ref.isScreen,
        link = _ref.link;

    var classBody = '';
    if (wTextDynamic === true) {
        if (children[2] === null) {
            classBody = 'complete';
        } else {
            classBody = 'partial';
        }
    }
    if (isScreen) {
        return React.createElement(
            'section',
            styles(backgroundImage, classBody),
            React.createElement(
                'a',
                { href: link, target: '_blank', rel: 'noreferrer' },
                children
            )
        );
    }
    return React.createElement(
        'section',
        styles(backgroundImage, classBody),
        children
    );
};

Card.propTypes = propTypes;
Card.defaultProps = defaultProps;

export default Card;