import { css } from 'glamor';

var styles = function styles(bgImg, classText) {
    var height = {};
    if (classText === 'partial') {
        height = { maxHeight: '45px' };
    } else if (classText === 'complete') {
        height = { maxHeight: 'none' };
    }
    return css({
        padding: '0 0 30px 0',
        marginBottom: 5,
        boxShadow: '0 2px 11px 0 rgba(0, 0, 0, 0.1)',
        backgroundImage: bgImg,
        backgroundColor: '#ffffff',
        ' > article + p': Object.assign({}, height),
        ' a': {
            textDecoration: 'none'
        }
    });
};
export default styles;