import { css } from 'glamor';

var s = {
    instagram: '#db287b',
    facebook: '#547bbc',
    twitter: '#53b2db'
};

var t = {
    retweet: '15px 30px',
    original: '30px',
    replay: '30px 15px',
    default: '30px'
};

var styles = function styles(source, type) {
    return css({
        position: 'relative',
        width: '100%',
        height: '100%',
        display: 'grid',
        marginBottom: 10,
        padding: '0 0 0 15px',
        boxSizing: 'border-box',
        gridTemplateColumns: '30px auto 35px',
        gridTemplateRows: t[type],
        gridGap: '15px 10px',
        // gridTemplateAreas: '". retweet ." "avatar header ." ". response ."',
        paddingTop: 20,
        // background: 'red',

        ' .retweet': {
            gridColumn: ' 2 / 3',
            // gridArea: 'retweet',
            display: 'flex'
        },

        ' .avatar': {
            gridColumn: '1 / 2'
            // gridArea: 'avatar',
        },

        ' .header': {
            gridColumn: '2 / 3',
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between'
            // gridArea: 'header',
        },

        ' .reply': {
            gridColumn: '2 / 3'
            // gridArea: 'response',
        },

        ' div': {
            margin: 0,
            padding: 0,

            ' &.sourceType': {
                position: 'absolute',
                top: 0,
                right: 0,
                width: 35,
                height: 35,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                background: s[source]
            },
            ' &.sourceTypeAvatar': {
                width: 35,
                height: 35,
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                background: s[source],
                borderRadius: '50%'
            }
            // gridTemplateRows: 'repeat(2, 49%)',
        } });
};
export default styles;