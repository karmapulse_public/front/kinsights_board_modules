import { css } from 'glamor';

var styles = function styles(color) {
    return css({
        width: '100%',
        padding: '0px 50px',
        fontSize: '11px',
        fontWeight: 700,
        textTransform: 'capitalize',
        ' > span': {
            position: 'relative',
            marginLeft: '15px',
            ':after': {
                content: '""',
                position: 'absolute',
                top: '50%',
                right: 'calc(100% + 5px)',
                width: '8px',
                height: '8px',
                borderRadius: '50%',
                background: color,
                transform: 'translate(0px, -50%)'
            }

        }
    });
};
export default styles;