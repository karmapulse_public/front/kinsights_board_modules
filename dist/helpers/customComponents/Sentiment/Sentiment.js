import React from 'react';
import PropTypes from 'prop-types';

import styles from './SentimentStyles';

var propTypes = {
    item: PropTypes.object,
    noSource: PropTypes.bool
};

var defaultProps = {
    item: {},
    noSource: false
};

var Sentiment = function Sentiment(props) {
    var classification_sentiment = props.item.classification_sentiment,
        noSource = props.noSource;

    if (!noSource || classification_sentiment === undefined) return null;

    var colors = {
        positivo: '#63d12f',
        negativo: '#ea3c3c',
        neutral: '#89a7ce'
    };

    return React.createElement(
        'div',
        styles(colors[classification_sentiment]),
        React.createElement(
            'span',
            null,
            classification_sentiment
        )
    );
};

Sentiment.propTypes = propTypes;
Sentiment.defaultProps = defaultProps;

export default Sentiment;