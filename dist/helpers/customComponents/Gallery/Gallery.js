import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import isUndefined from 'lodash/isUndefined';
import styles from './GalleryStyles';

var propTypes = {
    items: PropTypes.array,
    width: PropTypes.string,
    height: PropTypes.string,
    type: PropTypes.string,
    url: PropTypes.string,
    video: PropTypes.bool,
    gif: PropTypes.bool
};

var defaultProps = {
    items: [],
    width: 'auto',
    height: '100%',
    type: '',
    url: '',
    video: false,
    gif: false
};

var Gallery = function Gallery(props) {
    var images = function images() {
        return props.items.slice(0, 4).map(function (e, i) {
            return React.createElement('div', {
                key: i,
                className: 'item ' + (props.items.length > 4 && i === 3 ? 'item-more' : ''),
                style: { backgroundImage: 'url(' + (isUndefined(e.url) ? e.media_url.replace('http:', 'https:') : e.url.replace('http:', 'https:')) + '), url(https://cdn.karmapulse.com/noData-Feeds.svg)' }
            });
        });
    };

    var galeryItems = function galeryItems() {
        if (!isEmpty(props.items)) {
            var n = props.items.length;
            return React.createElement(
                'a',
                Object.assign({
                    className: 'custom-gallery',
                    href: props.url,
                    target: '_blank',
                    rel: 'noopener noreferrer'
                }, styles(n, props.type, props.width, props.height, props.gif, props.video)),
                images()
            );
        }
        return '';
    };

    return galeryItems();
};

Gallery.propTypes = propTypes;
Gallery.defaultProps = defaultProps;

export default Gallery;