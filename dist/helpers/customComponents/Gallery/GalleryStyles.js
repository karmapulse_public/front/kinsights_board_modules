import { css } from 'glamor';

var s = {
    instagram: '0',
    facebook: '0 30px 0 55px',
    twitter: '0 30px 0 55px'
};
var nImg = {
    1: {
        ' .item:first-child': {
            gridArea: '1 / 1 / 3 / 3'
        }
    },
    2: {
        ' .item:first-child': {
            gridArea: '1 / 1 / 3 / 2'
        },
        ' .item:nth-child(2)': {
            gridArea: '1 / 2 / 3 / 3'
        }
    },
    3: {
        ' .item:first-child': {
            gridArea: '1 / 1 / 3 / 2'
        },
        ' .item:nth-child(4)': {
            gridArea: '2 / 2 / 3 / 3'
        }
    }
};

var content = function content(isGif, isVideo) {
    if (!(isGif && isVideo)) {
        return {};
    }
    return {
        '&:after': {
            position: 'absolute',
            content: isGif ? 'GIF' : 'VIDEO',
            top: 0,
            right: 0,
            display: 'flex',
            width: isGif ? '30px' : '45px',
            height: '20px',
            fontSize: '12px',
            fontWeight: 700,
            color: '#FFF',
            background: '#000',
            alignItems: 'center',
            justifyContent: 'center'
        }
    };
};

var styles = function styles(n, type, generalWidth, generalHeight, gif, video) {
    return css(Object.assign({
        position: 'relative',
        width: generalWidth,
        height: generalHeight,
        padding: s[type],
        marginBottom: 15,
        overflow: 'hidden',
        display: 'grid',
        gridGap: '1px 1px',
        gridTemplateRows: 'repeat(2, 50%)',
        gridTemplateColumns: 'repeat(2, 50%)',
        textDecoration: 'none'
    }, nImg[n], {
        ' .item': {
            backgroundPosition: 'center center',
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover',
            '&-more': {
                position: 'relative'
            },
            '&-more:after': {
                position: 'absolute',
                content: '""',
                top: 0,
                left: 0,
                zIndex: 0,
                width: '100%',
                height: '100%',
                backgroundColor: 'rgba(0, 0, 0, 0.7)'
            },
            '&-more:before': {
                position: 'absolute',
                content: n - 3 + '+',
                top: '50%',
                left: '50%',
                zIndex: 1,
                fontSize: '22px',
                fontWeight: 700,
                color: '#FFF',
                transform: 'translate(-50%, -50%)'
            }
        }
    }, content(gif, video)));
};
export default styles;