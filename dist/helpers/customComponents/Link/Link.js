import React from 'react';
import PropTypes from 'prop-types';
import Typography from 'kpulse_design_system/dist/Typography';

import styles from './LinkStyles';

var propTypes = {
    width: PropTypes.string,
    height: PropTypes.string,
    url: PropTypes.string,
    imageUrl: PropTypes.string,
    title: PropTypes.string,
    text: PropTypes.string,
    displayUrl: PropTypes.string,
    type: PropTypes.string
};

var defaultProps = {
    width: 'auto',
    height: '100%',
    url: '',
    imageUrl: '',
    title: '',
    text: '',
    displayUrl: '',
    type: ''
};

var stylesText = {
    width: 'calc(100% - 30px)',
    maxHeight: 40,
    overflow: 'hidden'
};

var Link = function Link(props) {
    var Paragraph = Typography.Paragraph,
        Title = Typography.Title;

    return React.createElement(
        'div',
        Object.assign({ className: 'custom-link' }, styles(props.width, props.height, props.type)),
        React.createElement(
            'a',
            { href: props.url, target: '_blank', rel: 'noopener noreferrer' },
            props.imageUrl && props.imageUrl !== '' ? React.createElement('div', {
                className: 'link-img',
                style: { backgroundImage: 'url(' + (props.imageUrl ? props.imageUrl.replace('http:', 'https:') : '') + ')' }
            }) : null,
            React.createElement(
                'div',
                null,
                React.createElement(
                    Title,
                    { styles: stylesText, level: 6, bold: true },
                    props.title
                ),
                React.createElement(
                    Paragraph,
                    { styles: stylesText },
                    props.text
                ),
                React.createElement(
                    Paragraph,
                    { styles: stylesText, color: 'rgba(0,0,0,0.5)' },
                    props.displayUrl
                )
            )
        )
    );
};

Link.propTypes = propTypes;
Link.defaultProps = defaultProps;

export default Link;