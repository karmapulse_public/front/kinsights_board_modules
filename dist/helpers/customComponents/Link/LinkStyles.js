import { css } from 'glamor';

var s = {
    instagram: '0',
    facebook: '0 30px 0 55px',
    twitter: '0 30px 10px 55px'
};

var styles = function styles(generalWidth, generalHeight, type) {
    return css({
        width: generalWidth,
        height: generalHeight,
        margin: s[type],
        border: 'solid 1px rgba(0, 0, 0, 0.1)',
        boxSizing: 'border-box',
        ' > a': {
            width: 'inherit',
            height: 'inherit',
            textDecoration: 'none',
            cursor: 'pointer'
        },
        ' .link-img': {
            width: '100%',
            height: '60%',
            minHeight: '90px',
            backgroundPosition: 'center center',
            backgroundRepeat: 'no-repeat',
            backgroundSize: 'cover'
        },
        ' > a > div:last-child': {
            width: '100%',
            height: '40%',
            padding: '15px 15px 20px',
            color: '#000'
        }
    });
};

export default styles;