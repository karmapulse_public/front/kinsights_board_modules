import React from 'react';
import PropTypes from 'prop-types';
import Typography from 'kpulse_design_system/dist/Typography';
import Gallery from '../Gallery';

import styles from './QuotedStyles';

var propTypes = {
    width: PropTypes.string,
    height: PropTypes.string,
    child_tweet: PropTypes.shape({
        user: PropTypes.shape({
            username: PropTypes.string,
            name: PropTypes.string
        }),
        link: PropTypes.string,
        body: PropTypes.string,
        images: PropTypes.array
    })
};

var defaultProps = {
    width: 'auto',
    height: '100%',
    child_tweet: {
        user: {
            username: '',
            name: ''
        },
        link: '',
        body: '',
        images: []
    }
};

var stylesText = {
    width: '100%',
    maxHeight: 40,
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    whiteSpace: 'nowrap'
};

var Quoted = function Quoted(props) {
    var Paragraph = Typography.Paragraph,
        Title = Typography.Title;
    var _props$child_tweet = props.child_tweet,
        user = _props$child_tweet.user,
        link = _props$child_tweet.link,
        body = _props$child_tweet.body,
        images = _props$child_tweet.images;

    var padding = images && images.length > 0 ? '15px 10px' : '15px 15px';
    var textClass = images && images.length > 0 ? 'quoted-w-img' : 'quoted-wo-img';
    return React.createElement(
        'div',
        Object.assign({ className: 'custom-quoted' }, styles(padding, props.width, props.height)),
        React.createElement(
            'a',
            { href: link, target: '_blank', rel: 'noopener noreferrer' },
            images && images.length > 0 ?
            // <div
            //     className="quoted-img"
            //     style={{ backgroundImage: `url(${props.imageUrl})` }}
            // /> : null
            React.createElement(Gallery, { items: images, width: '74px', height: '74px' }) : null,
            React.createElement(
                'div',
                { className: textClass },
                React.createElement(
                    Title,
                    { styles: stylesText, level: 6, bold: true },
                    user.name
                ),
                React.createElement(
                    Paragraph,
                    { styles: Object.assign({}, stylesText, { marginBottom: '10px' }), color: 'rgba(0,0,0,0.5)' },
                    '@',
                    user.username
                ),
                React.createElement(
                    Paragraph,
                    { styles: stylesText },
                    body
                )
            )
        )
    );
};

Quoted.propTypes = propTypes;
Quoted.defaultProps = defaultProps;

export default Quoted;