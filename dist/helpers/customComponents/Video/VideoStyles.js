import { css } from 'glamor';

var s = {
    instagram: '0',
    facebook: '0 30px 0 55px',
    twitter: '0 30px 10px 55px'
};
var styles = function styles(type) {
    return css({
        margin: s[type]
    });
};

export default styles;