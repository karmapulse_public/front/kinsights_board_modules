var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React from 'react';
import PropTypes from 'prop-types';
import { faPlay, faPause, faVolumeUp, faVolumeMute, faExpandArrowsAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './video.css';
import styles from './VideoStyles';

var propTypes = {
    sources: PropTypes.array,
    cover: PropTypes.string,
    type: PropTypes.string,
    loop: PropTypes.bool,
    gif: PropTypes.bool
};

var defaultProps = {
    sources: [],
    type: '',
    cover: '',
    loop: false,
    gif: false
};

var Video = function (_React$Component) {
    _inherits(Video, _React$Component);

    function Video(props) {
        _classCallCheck(this, Video);

        var _this = _possibleConstructorReturn(this, (Video.__proto__ || Object.getPrototypeOf(Video)).call(this, props));

        _this.togglePlayPause = _this.togglePlayPause.bind(_this);
        _this.openFullScreen = _this.openFullScreen.bind(_this);
        _this.updateProgressBar = _this.updateProgressBar.bind(_this);
        _this.updateVolumeBar = _this.updateVolumeBar.bind(_this);
        _this.timeUpdate = _this.timeUpdate.bind(_this);
        _this.pauseVideo = _this.pauseVideo.bind(_this);
        _this.playVideo = _this.playVideo.bind(_this);
        _this.mute = _this.mute.bind(_this);
        _this.ended = _this.ended.bind(_this);
        _this.videoStatus = 'standBy';
        _this.Player = React.createRef();
        _this.PlayPauseButton = React.createRef();
        _this.progressBar = React.createRef();
        _this.MuteButton = React.createRef();
        _this.volumeBar = React.createRef();
        _this.FullScreen = React.createRef();
        return _this;
    }

    _createClass(Video, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            this.Player.current.controls = false;
            var mediaPlayer = this.Player.current;
            mediaPlayer.pause();
        }
    }, {
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps() {
            var mediaPlayer = this.Player.current;
            // const progressBar = this.progressBar;
            mediaPlayer.pause();
            this.setState({ videoStatus: 'standBy' });
        }
    }, {
        key: 'playVideo',
        value: function playVideo() {
            var mediaPlayer = this.Player.current;
            var buttons = this.PlayPauseButton.current;
            var play = buttons.getElementsByClassName('video__controls__play--play')[0];
            var pause = buttons.getElementsByClassName('video__controls__play--pause')[0];
            play.style.display = 'none';
            pause.style.display = 'block';
            this.setState({ videoStatus: 'playing' });
            mediaPlayer.play();
        }
    }, {
        key: 'pauseVideo',
        value: function pauseVideo() {
            var mediaPlayer = this.Player.current;
            var buttons = this.PlayPauseButton.current;
            var play = buttons.getElementsByClassName('video__controls__play--play')[0];
            var pause = buttons.getElementsByClassName('video__controls__play--pause')[0];
            play.style.display = 'block';
            pause.style.display = 'none';
            this.setState({ videoStatus: 'paused' });
            mediaPlayer.pause();
        }
    }, {
        key: 'togglePlayPause',
        value: function togglePlayPause() {
            var mediaPlayer = this.Player.current;
            if (!this.props.gif) {
                var buttons = this.PlayPauseButton.current;
                var play = buttons.getElementsByClassName('video__controls__play--play')[0];
                var pause = buttons.getElementsByClassName('video__controls__play--pause')[0];
                if (mediaPlayer.paused) {
                    mediaPlayer.play();
                    play.style.display = 'none';
                    pause.style.display = 'block';
                    this.setState({ videoStatus: 'playing' });
                } else {
                    mediaPlayer.pause();
                    play.style.display = 'block';
                    pause.style.display = 'none';
                    this.setState({ videoStatus: 'paused' });
                }
            } else {
                mediaPlayer.play();
                this.setState({ videoStatus: 'playing' });
            }
        }
    }, {
        key: 'mute',
        value: function mute() {
            var mediaPlayer = this.Player.current;
            var buttons = this.MuteButton.current;
            var mute = buttons.getElementsByClassName('video__controls__volume--mute')[0];
            var volume = buttons.getElementsByClassName('video__controls__volume--volume')[0];
            if (mediaPlayer.muted === false) {
                mediaPlayer.muted = true;
                volume.style.display = 'none';
                mute.style.display = 'block';
            } else {
                mediaPlayer.muted = false;
                volume.style.display = 'block';
                mute.style.display = 'none';
            }
        }
    }, {
        key: 'updateProgressBar',
        value: function updateProgressBar() {
            var mediaPlayer = this.Player.current;
            var progressBar = this.progressBar;

            var time = mediaPlayer.duration * (progressBar.current.value / 100);
            mediaPlayer.currentTime = time;
        }
    }, {
        key: 'updateVolumeBar',
        value: function updateVolumeBar() {
            var mediaPlayer = this.Player.current;
            var volumeBar = this.volumeBar;

            mediaPlayer.volume = volumeBar.current.value;
        }
    }, {
        key: 'timeUpdate',
        value: function timeUpdate() {
            if (!this.props.gif) {
                var mediaPlayer = this.Player.current;
                var progressBar = this.progressBar;

                var value = 100 / mediaPlayer.duration * mediaPlayer.currentTime;
                progressBar.current.value = value;
            }
        }
    }, {
        key: 'openFullScreen',
        value: function openFullScreen() {
            var mediaPlayer = this.Player.current;
            if (mediaPlayer.requestFullscreen) {
                mediaPlayer.requestFullscreen();
            } else if (mediaPlayer.mozRequestFullScreen) {
                mediaPlayer.mozRequestFullScreen(); // Firefox
            } else if (mediaPlayer.webkitRequestFullscreen) {
                mediaPlayer.webkitRequestFullscreen(); // Chrome and Safari
            }
            this.setState({ videoStatus: 'fullscreen' });
        }
    }, {
        key: 'ended',
        value: function ended() {
            this.setState({ videoStatus: 'standBy' });
        }
    }, {
        key: 'renderButtonsBar',
        value: function renderButtonsBar() {
            if (!this.props.gif) {
                return React.createElement(
                    'div',
                    { className: 'video__controls' },
                    React.createElement(
                        'button',
                        {
                            onClick: this.togglePlayPause,
                            ref: this.PlayPauseButton,
                            className: 'video__controls__play',
                            title: 'play'
                        },
                        React.createElement(
                            'div',
                            { className: 'video__controls__play--play' },
                            React.createElement(FontAwesomeIcon, { icon: faPlay })
                        ),
                        React.createElement(
                            'div',
                            { className: 'video__controls__play--pause' },
                            React.createElement(FontAwesomeIcon, { icon: faPause })
                        )
                    ),
                    React.createElement('input', {
                        className: 'video__controls__progress',
                        type: 'range',
                        ref: this.progressBar,
                        onChange: this.updateProgressBar,
                        onMouseDown: this.pauseVideo,
                        onMouseUp: this.playVideo
                    }),
                    React.createElement(
                        'div',
                        { className: 'video__controls__volume' },
                        React.createElement(
                            'button',
                            { onClick: this.mute, ref: this.MuteButton },
                            React.createElement(
                                'div',
                                { className: 'video__controls__volume--volume' },
                                React.createElement(FontAwesomeIcon, { icon: faVolumeUp })
                            ),
                            React.createElement(
                                'div',
                                { className: 'video__controls__volume--mute' },
                                React.createElement(FontAwesomeIcon, { icon: faVolumeMute })
                            )
                        ),
                        React.createElement('input', {
                            type: 'range',
                            ref: this.volumeBar,
                            onChange: this.updateVolumeBar,
                            min: '0',
                            max: '1',
                            step: '0.1'
                        })
                    ),
                    React.createElement(
                        'button',
                        {
                            ref: this.FullScreen,
                            className: 'video__controls__fullscreen',
                            onClick: this.openFullScreen,
                            title: 'fullscreen'
                        },
                        ' ',
                        React.createElement(FontAwesomeIcon, { icon: faExpandArrowsAlt })
                    )
                );
            }
            return React.createElement('div', null);
        }
    }, {
        key: 'render',
        value: function render() {
            var videoClass = 'standBy';
            if (this.state) {
                videoClass = this.state.videoStatus;
            }
            var Sources = [];
            this.props.sources.map(function (source, index) {
                return Sources.push(React.createElement('source', { key: index, src: source.url ? source.url.replace('http:', 'https:') : '', type: source.content_type }));
            });
            var gif = function gif() {
                return React.createElement(
                    'h1',
                    null,
                    'GIF'
                );
            };
            var httpsCover = this.props.cover.replace('http:', 'https:');
            return React.createElement(
                'div',
                Object.assign({ className: 'video ' + videoClass }, styles(this.props.type)),
                React.createElement(
                    'video',
                    {
                        ref: this.Player,
                        className: 'video__player',
                        loop: this.props.loop,
                        controls: true,
                        poster: httpsCover,
                        onTimeUpdate: this.timeUpdate,
                        onEnded: this.ended
                    },
                    Sources
                ),
                this.renderButtonsBar(),
                React.createElement(
                    'button',
                    { onClick: this.togglePlayPause, className: 'video__playFull' },
                    this.props.gif ? gif() : React.createElement(FontAwesomeIcon, { icon: faPlay })
                )
            );
        }
    }]);

    return Video;
}(React.Component);

Video.propTypes = propTypes;
Video.defaultProps = defaultProps;

export default Video;