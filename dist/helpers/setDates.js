
import clone from 'lodash/clone';
import moment from 'moment';
import 'moment-timezone';

/*
    withCache sirve para calcular o no la ultima fecha en multiplo de 5
    y hacer uso del cache de elastic (preguntar a josé)
*/
var setDates = function setDates(dateRange) {
    var withCache = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
    var startDate = dateRange.startDate,
        endDate = dateRange.endDate;


    var tz = 'America/Mexico_City';
    var strStartDate = clone(startDate).format('YYYY-MM-DD hh:mm:ss');
    var strEndDate = clone(endDate).format('YYYY-MM-DD hh:mm:ss');

    var utcOffsetStart = moment.tz(strStartDate, tz).isDST() ? '-05:00' : '-06:00';
    var utcOffsetEnd = moment.tz(strEndDate, tz).isDST() ? '-05:00' : '-06:00';

    startDate = startDate.utcOffset(utcOffsetStart);
    endDate = endDate.utcOffset(utcOffsetEnd);

    if (withCache) {
        startDate = startDate.set({
            hour: 0,
            minute: 0,
            second: 0,
            millisecond: 0
        });
        endDate = endDate.set({
            minute: 5 * Math.floor(moment().minute() / 5),
            second: 0,
            millisecond: 0
        });
    }

    return {
        initial_date: startDate.toISOString(),
        final_date: endDate.toISOString()
    };
};

export default setDates;