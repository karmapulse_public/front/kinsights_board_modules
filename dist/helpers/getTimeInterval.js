import moment from 'moment';

var getTimeInterval = function getTimeInterval(startDate, finishDate) {
    var blockType = null;

    var unixFormatStartDate = Date.parse(new Date(startDate));
    var unixFormatFinishDate = Date.parse(new Date(finishDate));

    var minutes = 1000 * 60;
    var hours = minutes * 60;
    var days = hours * 24;
    var months = days * 30;

    var timeRange = unixFormatFinishDate - unixFormatStartDate;

    var min = Math.round(timeRange / minutes);
    var hrs = Math.round(timeRange / hours);
    var d = Math.round(timeRange / days);
    var m = Math.round(timeRange / months);

    if (d >= 180) {
        blockType = 'month';
    } else if (d >= 32) {
        blockType = 'week';
    } else if (d > 1) {
        blockType = 'day';
    } else if (hrs === 1) {
        blockType = '1m';
    } else {
        blockType = 'hour';
    }

    return blockType;
};

export default getTimeInterval;

export var calculateNewInterval = function calculateNewInterval(startFilter, finishFilter, component) {
    var width = component.current.offsetWidth;
    var dateDiff = finishFilter.utc().diff(startFilter.utc(), 'days', true);

    if (width <= 550) {
        if (dateDiff <= 2) {
            return 'hour';
        } else if (dateDiff <= 30) {
            return 'day';
        } else if (dateDiff <= 273) {
            return 'week';
        }
        return 'month';
    }
    if (dateDiff <= 15) {
        return 'hour';
    } else if (dateDiff <= 365) {
        return 'day';
    } else if (dateDiff <= 2555) {
        return 'week';
    }
    return 'month';
};