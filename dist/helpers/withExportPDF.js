import React from 'react';
import { css } from 'glamor';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileExport } from '@fortawesome/free-solid-svg-icons';

import ReactToPdf from './ReactToPdf';

var defaultStyles = {
    position: 'absolute',
    top: '0px',
    right: '10px',
    zIndex: 2,
    cursor: 'pointer',
    ':hover > svg': {
        color: 'rgba(0, 0, 0, 0.6)'
    },
    ' > svg': {
        transition: 'color 0.25s ease'
    }
};

var withExportPDF = function withExportPDF(Component, componentProps) {
    var bodyRef = React.createRef();

    var buttonComponent = function buttonComponent(ref, name, styles) {
        return React.createElement(
            'div',
            Object.assign({
                className: 'export-button'
            }, css(Object.assign({}, defaultStyles, styles))),
            React.createElement(
                ReactToPdf,
                {
                    targetRef: bodyRef,
                    filename: 'div-blue.pdf'
                },
                function (_ref) {
                    var toPdf = _ref.toPdf;
                    return React.createElement(FontAwesomeIcon, {
                        onClick: toPdf,
                        icon: faFileExport,
                        color: 'rgba(0, 0, 0, 0.3)'
                    });
                }
            )
        );
    };

    componentProps.fields.buttonExport = componentProps.fields.hasExport ? buttonComponent : null;

    return React.createElement(
        'section',
        { className: 'export-body', ref: bodyRef },
        React.cloneElement(Component, componentProps)
    );
};

export default withExportPDF;