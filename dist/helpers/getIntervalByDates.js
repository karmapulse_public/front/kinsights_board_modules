
var getIntervalByDates = function getIntervalByDates(initDate, finishDate) {
    if (finishDate.diff(initDate, 'days', true) <= 7) {
        return 'hour'; // 1 a 7 días ... minutos 0
    } else if (finishDate.diff(initDate, 'months', true) <= 3) {
        return 'day'; // 8 días a 3 meses ... horas 0
    } else if (finishDate.diff(initDate, 'months', true) <= 6) {
        return 'week'; // 3 meses a 6 meses ... días primer lunes - horas 0
    }
    return 'month'; // Mayor a 6 meses ... dia 1 hora 0
};

export default getIntervalByDates;