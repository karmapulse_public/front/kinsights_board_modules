var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

export default function request(url) {
    var method = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'GET';
    var params = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
    var headers = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {};

    return new Promise(function (resolve, reject) {
        var req = new XMLHttpRequest();

        if (method === 'GET') {
            url = url + '?index=' + params.index + '&es_query=' + encodeURIComponent(JSON.stringify(params.es_query));
        }

        req.open(method, url, true);
        req.setRequestHeader('Content-Type', 'application/json');

        if (headers) {
            Object.keys(headers).forEach(function (key) {
                req.setRequestHeader(key, headers[key]);
            });
        }

        if (method === 'POST') {
            req.send(JSON.stringify(params));
        } else {
            req.send();
        }

        req.onreadystatechange = function handleResponse() {
            if (this.responseText !== undefined && this.responseText !== '' && this.responseText !== null && this.responseText !== ' ' && this.responseText[this.responseText.length - 1] === '}' && this.readyState === 4 && this.status === 200 || this.readyState === 4 && this.status === 400) {
                resolve({
                    json: JSON.parse(this.responseText),
                    status: {
                        info: this.statusText,
                        code: this.status
                    }
                });
            }
        };

        req.onerror = function handleError(error) {
            reject(error);
        };
    });
}

export function requestPost(url) {
    var method = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'GET';
    var data = arguments[2];

    return new Promise(function (resolve, reject) {
        var rq = new XMLHttpRequest();

        rq.open(method, url, true);
        rq.setRequestHeader('Content-Type', 'application/JSON');
        rq.send(JSON.stringify(data));

        rq.onreadystatechange = function handleResponse() {
            if (this.responseText !== undefined && this.responseText !== '') {
                var response = {
                    json: JSON.parse(this.responseText),
                    status: {
                        info: this.statusText,
                        code: this.status
                    }
                };
                resolve(response);
            }
        };
        request.onerror = function handleError(error) {
            reject(error);
        };
    });
}

export var convertData = function convertData(data) {
    var string = '';
    var counter = 0;
    for (var prop in data) {
        var propName = prop.replace('_', '-');
        var value = data[prop];
        if (counter !== 0) {
            string += '&';
        }
        if ((typeof value === 'undefined' ? 'undefined' : _typeof(value)) === 'object') {
            if (Array.isArray(value)) {
                value = value.join();
            } else {
                value = encodeURIComponent(JSON.stringify(value));
            }
        }
        string = string + propName + '=' + value;
        counter++;
    }
    return string;
};

export function fetchXHR(url, method, data) {
    return new Promise(function (resolve, reject) {
        var request = new XMLHttpRequest();
        var urlParsed = method === 'GET' ? url + '?' + convertData(data) : url;

        request.open(method, urlParsed, true);
        request.setRequestHeader('Content-Type', 'application/json; charset=utf-8');

        if (method === 'POST') {
            request.send(JSON.stringify(data));
        } else {
            request.send();
        }

        request.onreadystatechange = function handleResponse() {
            if (this.responseText !== undefined && this.responseText !== '' && this.responseText !== null && this.responseText !== ' ' && this.responseText[this.responseText.length - 1] === '}' && this.readyState === 4 && this.status === 200 || this.readyState === 4 && this.status === 400) {
                var response = {
                    json: JSON.parse(this.responseText),
                    status: {
                        info: this.statusText,
                        code: this.status
                    }
                };
                resolve(response);
            }
        };
        request.onerror = function handleError(error) {
            reject(error);
        };
    });
}