import isUndefined from 'lodash/isUndefined';

/**
 * Find urls patter in tweets body text
 */
function findUrlInBodyText(text) {
    var regex = /(https?:\/\/[^\s]+)/g;
    return text.replace(regex, function (url) {
        var URL = url;
        return '<a href="' + URL + '" target="_blank">' + URL + '</a>';
    });
}

/**
 * Find hastags patter in tweets body text
 */
function findHashtagInBodyText(text) {
    var regex = /(^#|[^&]#)([a-z0-9]+[^\s!@#$%^&*()=+./,\[{\]]+)/gi;
    return text.replace(regex, function (hashtag) {
        var h = hashtag.replace('#', '').trim();
        return '<a href="https://twitter.com/hashtag/' + h + '" target="_blank">' + hashtag + ' </a>';
    });
}

/**
 * Find users patter in tweets body text
 */
function findUserInBodyText(text) {
    var regex = /(^|\s)@(\w+)/g;
    var regex1 = /(^|\s)@(\w+)/g;
    var tweet = text.replace(regex, '$1<a href="http://www.twitter.com/$2" target="_blank">@$2</a>');
    return tweet.replace(regex1, '$1#<a href="http://search.twitter.com/search?q=%23$2" target="_blank" >"@$2</a>');
}

export default function formatBodyText(tweet) {
    if (!isUndefined(tweet)) {
        var withUrl = findUrlInBodyText(tweet);
        var withHashtag = findHashtagInBodyText(withUrl);
        return findUserInBodyText(withHashtag);
    }
    return '';
}