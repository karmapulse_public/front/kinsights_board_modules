import find from 'lodash/find';
import flattenDeep from 'lodash/flattenDeep';
import unionBy from 'lodash/unionBy';

export default (function (buckets, entity) {
    var bucketsOriginal = JSON.parse(JSON.stringify(buckets));

    var getI = flattenDeep(bucketsOriginal.map(function (i) {
        return i[entity + '_by_time'].buckets.map(function (o) {
            return o;
        });
    }));
    var max = unionBy(getI, 'key');

    var resultBuckets = JSON.parse(JSON.stringify(buckets));

    var y = resultBuckets.map(function (item) {
        var collect = [];

        max.forEach(function (maxItem) {
            var xx = find(item[entity + '_by_time'].buckets, { key: maxItem.key });

            if (typeof xx === 'undefined') {
                collect.push(Object.assign(maxItem, { doc_count: 0 }));
            }
        });
        //eslint-disable-next-line
        item[entity + '_by_time'].buckets = item[entity + '_by_time'].buckets.concat(collect);

        return item;
    });

    return y;
});