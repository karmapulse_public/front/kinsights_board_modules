import { createContext } from 'react';

export default createContext({
    open: false,
    onCloseDrawer: function onCloseDrawer() {
        return {};
    },
    drawerData: {
        type: 'TWITTER',
        title: '',
        subtitleLeft: '',
        subtitleRight: '',
        queryData: {}
    }
});