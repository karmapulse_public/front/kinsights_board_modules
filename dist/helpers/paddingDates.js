function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import moment from 'moment';
import upperFirst from 'lodash/upperFirst';

var getResetInterval = function getResetInterval(intervalType) {
    var resetObject = {
        minute: 0,
        second: 0,
        millisecond: 0
    };

    if (intervalType === 'day' || intervalType === 'week' || intervalType === 'month') {
        resetObject.hour = 0;
    }
    if (intervalType === 'date') {
        resetObject.date = 1;
    }

    return resetObject;
};

var paddingDates = function paddingDates(intervals, _ref) {
    var initial_date = _ref.initial_date,
        final_date = _ref.final_date,
        interval = _ref.filters.interval;
    var dateKey = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'date';
    var countKey = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 'total';

    if (intervals.length === 0) return [];
    var initialDate = moment(initial_date).set(Object.assign({}, getResetInterval(interval)));
    var finalDate = moment(final_date).set(Object.assign({}, getResetInterval(interval)));

    var date = moment(intervals[0][dateKey]);
    var diffs = date.diff(initialDate, true);
    while (diffs >= 0) {
        var _intervals$unshift;

        date.subtract(1, interval + 's');
        intervals.unshift((_intervals$unshift = {}, _defineProperty(_intervals$unshift, dateKey, date.toISOString()), _defineProperty(_intervals$unshift, countKey, 0), _defineProperty(_intervals$unshift, 'key', date.toDate().getTime()), _intervals$unshift));
        diffs = date.diff(initialDate);
    }

    date = moment(intervals[intervals.length - 1][dateKey]);
    diffs = finalDate.diff(date, true);
    while (diffs > 0) {
        var _intervals$push;

        date.add(1, interval + 's');
        intervals.push((_intervals$push = {}, _defineProperty(_intervals$push, dateKey, date.toISOString()), _defineProperty(_intervals$push, countKey, 0), _defineProperty(_intervals$push, 'key', date.toDate().getTime()), _intervals$push));
        diffs = finalDate.diff(date, interval);
    }

    return intervals;
};

export default paddingDates;

export var labelType = function labelType(startDate, finishDate, type) {
    var label = {
        hour: '' + upperFirst(moment(startDate).format('MMM DD, YYYY  h:mm A')),
        day: '' + upperFirst(moment(startDate).format('MMM DD, YYYY')),
        week: upperFirst(moment(startDate).format('MMM DD, YYYY')) + ' al ' + upperFirst(moment(finishDate).format('MMM DD, YYYY')),
        month: '' + upperFirst(moment(startDate).format('MMM YYYY'))
    };
    return label[type];
};