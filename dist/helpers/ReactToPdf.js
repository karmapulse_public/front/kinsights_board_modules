var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import JsPdf from 'jspdf';
import html2canvas from 'html2canvas';

var ReactToPdf = function (_PureComponent) {
    _inherits(ReactToPdf, _PureComponent);

    function ReactToPdf(props) {
        _classCallCheck(this, ReactToPdf);

        var _this = _possibleConstructorReturn(this, (ReactToPdf.__proto__ || Object.getPrototypeOf(ReactToPdf)).call(this, props));

        _this.toPdf = _this.toPdf.bind(_this);
        _this.targetRef = React.createRef();
        return _this;
    }

    _createClass(ReactToPdf, [{
        key: 'toPdf',
        value: function toPdf() {
            var _props = this.props,
                targetRef = _props.targetRef,
                filename = _props.filename,
                onComplete = _props.onComplete;

            var source = targetRef || this.targetRef;
            var targetComponent = source.current || source;
            if (!targetComponent) {
                throw new Error('Target ref must be used or informed. See https://github.com/ivmarcos/react-to-pdf#usage.');
            }
            html2canvas(targetComponent).then(function (canvas) {
                var imgData = canvas.toDataURL('image/png', 1.0);
                var pdf = new JsPdf('l', 'mm', 'a4');
                var width = canvas.width * 0.264583333;
                var height = canvas.height * 0.264583333;
                var sizeElto = {
                    width: width,
                    height: height
                };
                var ratio = 1;

                while (sizeElto.width > 297 || sizeElto.height > 210) {
                    sizeElto.width = width / ratio;
                    sizeElto.height = height / ratio;

                    ratio += 0.5;
                }
                pdf.addImage(imgData, 'PNG', (297 - sizeElto.width) / 2, (210 - sizeElto.height) / 2, sizeElto.width, sizeElto.height);
                pdf.save(filename);
                if (onComplete) onComplete();
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var children = this.props.children;

            return children({ toPdf: this.toPdf, targetRef: this.targetRef });
        }
    }]);

    return ReactToPdf;
}(PureComponent);

ReactToPdf.propTypes = {
    filename: PropTypes.string.isRequired,
    children: PropTypes.func.isRequired,
    onComplete: PropTypes.func,
    targetRef: PropTypes.oneOfType([PropTypes.func, PropTypes.shape({ current: PropTypes.instanceOf(Element) })])
};

ReactToPdf.defaultProps = {
    onComplete: undefined,
    targetRef: undefined
};

export default ReactToPdf;