var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import fetchFbProfile from './TopInteractionDataFlow';

import LoadingModule from '../LoadingModule';
import TotalNumbers from './visualizations/TotalNumbers';
import SliderInteractions from './visualizations/SliderInteractions';

var propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    dateRange: PropTypes.object,
    services: PropTypes.object
};

var defaultProps = {
    fields: {},
    side: 'left',
    color: '#444',
    dateRange: {},
    services: {}
};

var TopInteraction = function (_Component) {
    _inherits(TopInteraction, _Component);

    function TopInteraction(props) {
        _classCallCheck(this, TopInteraction);

        var _this = _possibleConstructorReturn(this, (TopInteraction.__proto__ || Object.getPrototypeOf(TopInteraction)).call(this, props));

        _this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color
        };
        return _this;
    }

    _createClass(TopInteraction, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            this.updateDataFlow(this.props);
        }
    }, {
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProps) {
            this.updateDataFlow(nextProps);
        }
    }, {
        key: 'updateDataFlow',
        value: function updateDataFlow(props) {
            var _this2 = this;

            var params = {
                rule_id: [props.fields.search_id],
                initial_date: props.dateRange.startDate.toISOString(),
                final_date: props.dateRange.endDate.toISOString(),
                recipe_id: 'module_tw_top_engagement_user',
                filters: {
                    username: props.fields.name_user
                }
            };

            fetchFbProfile(props.services.twitter, params).subscribe(function (data) {
                _this2.setState({
                    moduleConfig: props.fields,
                    moduleData: data,
                    moduleLoading: false,
                    moduleSide: props.side,
                    moduleColor: props.color
                });
            }, function (error) {
                console.log(error);
            }, null);
        }
    }, {
        key: 'renderByViz',
        value: function renderByViz() {
            var _state = this.state,
                moduleConfig = _state.moduleConfig,
                moduleData = _state.moduleData;


            if (moduleData) {
                var visualizations = {
                    total_numbers: function total_numbers(params) {
                        return React.createElement(TotalNumbers, params);
                    },
                    slider_interactions: function slider_interactions(params) {
                        return React.createElement(SliderInteractions, params);
                    }
                };

                return visualizations[moduleConfig.visualization](this.state);
            }

            return React.createElement(LoadingModule, null);
        }
    }, {
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'm-fb-total-numbers' },
                this.renderByViz()
            );
        }
    }]);

    return TopInteraction;
}(Component);

TopInteraction.propTypes = propTypes;
TopInteraction.defaultProps = defaultProps;

export default TopInteraction;