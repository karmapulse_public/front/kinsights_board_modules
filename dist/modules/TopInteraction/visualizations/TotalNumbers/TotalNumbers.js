import React from 'react';
import PropTypes from 'prop-types';
import LikesIcon from '../../../../helpers/icons/ceo/likesIcon';
import ReplicesIcon from '../../../../helpers/icons/ceo/dialogIcon';
import RetweetsIcon from '../../../../helpers/icons/ceo/retweetsIcon';

import styles from './styles';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string
};

var defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleColor: '#666'
};

var TotalNumbers = function TotalNumbers(_ref) {
    var moduleData = _ref.moduleData,
        moduleConfig = _ref.moduleConfig,
        moduleColor = _ref.moduleColor;

    var interactionTotal = moduleData.data.total_likes + moduleData.data.total_replies + moduleData.data.total_retweets;
    var color = '#6ab9dc';
    return React.createElement(
        'div',
        styles(moduleColor),
        React.createElement(
            'div',
            { className: 'total-num-int__label' },
            React.createElement(
                'h3',
                null,
                moduleConfig.title
            )
        ),
        React.createElement(
            'div',
            { className: 'total-num-int__content' },
            React.createElement(
                'div',
                { className: 'total-num-int__content__total' },
                React.createElement(
                    'h4',
                    null,
                    'Total de interacciones'
                ),
                React.createElement(
                    'p',
                    null,
                    interactionTotal
                )
            ),
            React.createElement(
                'div',
                { className: 'total-num-int__content__data' },
                React.createElement(
                    'h4',
                    null,
                    React.createElement(ReplicesIcon, { color: color }),
                    'Respuestas'
                ),
                React.createElement(
                    'p',
                    null,
                    moduleData.data.total_replies
                )
            ),
            React.createElement(
                'div',
                { className: 'total-num-int__content__data' },
                React.createElement(
                    'h4',
                    null,
                    React.createElement(LikesIcon, { color: color }),
                    'Favoritos'
                ),
                React.createElement(
                    'p',
                    null,
                    moduleData.data.total_likes
                )
            ),
            React.createElement(
                'div',
                { className: 'total-num-int__content__data' },
                React.createElement(
                    'h4',
                    null,
                    React.createElement(RetweetsIcon, { color: color }),
                    'Retweet'
                ),
                React.createElement(
                    'p',
                    null,
                    moduleData.data.total_retweets
                )
            )
        )
    );
};

TotalNumbers.propTypes = propTypes;
TotalNumbers.defaultProps = defaultProps;

export default TotalNumbers;