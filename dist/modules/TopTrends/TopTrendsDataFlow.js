import { Observable } from 'rxjs';
import isUndefined from 'lodash/isUndefined';
import find from 'lodash/find';
import { fetchXHR } from '../../helpers/http';

var formatSentiment = function formatSentiment(buckets) {
    var collect = [];
    var sentimentsTypes = ['positivo', 'neutral', 'negativo'];
    sentimentsTypes.map(function (item) {
        var s = find(buckets, { key: item });
        if (!isUndefined(s)) return collect.push(s);
        return collect.push({ key: item, doc_count: 0 });
    });
    return collect;
};

var formatSentimentTop = function formatSentimentTop(eltos) {
    var collect = [];
    var mapSentiment = {
        positive: 'positivo',
        negative: 'negativo',
        neutral: 'neutral'
    };
    Object.keys(eltos).forEach(function (key) {
        if (key.includes('total_')) {
            collect.push({
                key: mapSentiment[key.split('_')[1]],
                doc_count: eltos[key]
            });
        }
    });
    return collect;
};

export default (function (url, params) {
    return Observable.create(function (observer) {
        Observable.fromPromise(fetchXHR(url, 'GET', params)).subscribe(function (data) {
            if (params.filters.query === 'search') {
                var formatData = params.filters.words.split(',').map(function (item, index) {
                    return {
                        key: item,
                        doc_count: data.json.response.aggregations.entity.buckets['' + index].doc_count,
                        sentiment: formatSentiment(data.json.response.aggregations.entity.buckets['' + index].sentiment_per_entity.buckets)
                    };
                });

                formatData.sort(function (a, b) {
                    return a.doc_count - b.doc_count;
                }).reverse();
                observer.next(formatData);
            } else {
                var sentiment = Object.assign({}, data.json.response.map(function (item) {
                    return {
                        key: item.theme,
                        doc_count: item.total,
                        sentiment: formatSentimentTop(item)
                    };
                }));
                observer.next(Object.values(sentiment));
            }
        }, function (error) {
            observer.error(error);
        });
    });
});