import { css } from 'glamor';

var styles = function styles(color) {
    var bgColor = 'transparent';

    return css({
        width: '100%',
        height: 'auto',
        backgroundColor: bgColor,
        color: 'rgba(0,0,0, 0.5)',

        ' .radar__label': {
            width: '100%',
            '> h3': {
                padding: '15px 20px 20px',
                textTransform: 'uppercase',
                fontSize: 16,
                fontWeight: 400,
                textAlign: 'left',
                color: color
            }
        },
        ' .radar__chart': {
            position: 'relative',
            width: '100%',
            height: 220
        }
    });
};

export default styles;