import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import maxBy from 'lodash/maxBy';
import { Radar, RadarChart, PolarGrid, PolarAngleAxis, ResponsiveContainer } from 'recharts';

import styles from './radarTopTrendsStyles';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.array,
    moduleColor: PropTypes.string
};

var defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleColor: '#666'
};

var RadarTopTrends = function RadarTopTrends(_ref) {
    var moduleConfig = _ref.moduleConfig,
        moduleData = _ref.moduleData,
        moduleColor = _ref.moduleColor;

    var refExport = useRef(null);
    var colors = ['#c75a93', '#8176cc', '#cc5f43', '#5ba966', '#54a2d3'];
    var maxValue = maxBy(moduleData, 'doc_count');

    var dataFormat = moduleData.map(function (item) {
        return {
            subject: item.key,
            total: item.doc_count,
            fullMark: maxValue.doc_count
        };
    });

    var renderTitle = function renderTitle() {
        if (moduleConfig.show_title) {
            var _moduleConfig$title = moduleConfig.title,
                title = _moduleConfig$title === undefined ? 'RELACIÓN DE TEMAS' : _moduleConfig$title;

            return React.createElement(
                'div',
                { className: 'radar__label' },
                React.createElement(
                    'h3',
                    null,
                    title
                )
            );
        }
        return '';
    };

    var CustomizedTick = function CustomizedTick(data) {
        return React.createElement(
            'g',
            null,
            React.createElement('circle', {
                cx: data.x,
                cy: data.y,
                r: '5',
                stroke: colors[data.index],
                fill: colors[data.index],
                fillOpacity: 0.2
            })
        );
    };

    return React.createElement(
        'div',
        Object.assign({
            className: 'radar-top-trends'
        }, styles(moduleColor), {
            ref: refExport
        }),
        renderTitle(),
        React.createElement(
            'div',
            { className: 'radar__chart' },
            React.createElement(
                ResponsiveContainer,
                null,
                React.createElement(
                    RadarChart,
                    { data: dataFormat },
                    React.createElement(Radar, {
                        dataKey: 'total',
                        stroke: '#d0021b',
                        strokeWidth: 1,
                        fill: '#d0021b',
                        fillOpacity: 0.2
                    }),
                    React.createElement(PolarGrid, { stroke: '#eee' }),
                    React.createElement(PolarAngleAxis, {
                        tick: function tick(data) {
                            return CustomizedTick(data);
                        }
                    })
                )
            )
        )
    );
};

RadarTopTrends.propTypes = propTypes;
RadarTopTrends.defaultProps = defaultProps;

export default RadarTopTrends;