import React from 'react';
import PropTypes from 'prop-types';
import maxBy from 'lodash/maxBy';
import { css } from 'glamor';

import styles from './listTopTrendsSentimentStyles';
import { numberWithCommas } from '../../../../helpers/number';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.array,
    moduleColor: PropTypes.string,
    moduleSide: PropTypes.string,
    showColors: PropTypes.bool
};

var defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleColor: '#666',
    moduleSide: 'left',
    showColors: false
};

var ListTopTrendsSentiment = function ListTopTrendsSentiment(_ref) {
    var moduleConfig = _ref.moduleConfig,
        moduleData = _ref.moduleData,
        moduleColor = _ref.moduleColor,
        moduleSide = _ref.moduleSide,
        showColors = _ref.showColors;

    var colors = ['#c75a93', '#8176cc', '#cc5f43', '#5ba966', '#54a2d3'];

    var softColors = ['rgba(199,89,147,0.21)', 'rgba(129,117,204,0.21)', 'rgba(204,95,66,0.21)', 'rgba(91,169,101,0.21)', 'rgba(84,161,211,0.21)'];
    var entityType = {
        hashtags: {
            symbol: '#',
            name: 'Hashtags'
        },
        mentions: {
            symbol: '@',
            name: 'Menciones'
        },
        keywords: {
            symbol: '',
            name: 'Palabras'
        }
    };

    var dataFormat = moduleData.map(function (item, index) {
        return {
            id: index + 1,
            subject: entityType[moduleConfig.entity_type].symbol + item.key,
            total: item.doc_count,
            sentiment: item.sentiment,
            maxValue: maxBy(item.sentiment, 'doc_count')
        };
    });

    var renderTitle = function renderTitle() {
        if (moduleConfig.show_title) {
            var _moduleConfig$title = moduleConfig.title,
                title = _moduleConfig$title === undefined ? 'RELACIÓN DE TEMAS' : _moduleConfig$title;

            return React.createElement(
                'div',
                { className: 'list__label' },
                React.createElement(
                    'h3',
                    null,
                    title
                )
            );
        }
        return '';
    };

    var renderSentiment = function renderSentiment(sentiment, total, max) {
        var percentage = function percentage(value) {
            return (total < 1 ? 0 : value * 100 / total).toFixed(1).replace(/\.0$/, '');
        }; // eslint-disable-line
        return sentiment.map(function (s, index) {
            return React.createElement(
                'h5',
                {
                    key: index,
                    className: s.key + ' ' + (s.key === max.key && s.doc_count > 0 ? 'active' : '')
                },
                percentage(s.doc_count),
                '%'
            );
        });
    };

    var showColor = function showColor(index, _ref2) {
        var subject = _ref2.subject,
            total = _ref2.total,
            id = _ref2.id;

        var dotColor = css({
            backgroundColor: softColors[index],
            border: '1px solid ' + colors[index]
        });
        var value = subject.length < 35 ? subject : subject.substring(0, 35).concat('...');

        if (showColors) {
            return React.createElement(
                'div',
                { className: 'list__item__data' },
                React.createElement('div', Object.assign({ className: 'list__dot' }, dotColor)),
                React.createElement(
                    'h4',
                    { className: 'subject' },
                    value
                ),
                React.createElement(
                    'h5',
                    { className: 'total', style: { color: colors[index] } },
                    numberWithCommas(total)
                )
            );
        }
        return React.createElement(
            'div',
            { className: 'list__item__data' },
            React.createElement(
                'h5',
                { className: 'index' },
                id,
                '.'
            ),
            React.createElement(
                'h4',
                { className: 'subject' },
                value
            ),
            React.createElement(
                'h5',
                { className: 'total' },
                numberWithCommas(total)
            )
        );
    };

    var renderItems = function renderItems() {
        return dataFormat.map(function (item, index) {
            return React.createElement(
                'li',
                { className: 'list__item', key: index },
                showColor(index, item),
                React.createElement(
                    'div',
                    { className: 'list__item__sentiment' },
                    renderSentiment(item.sentiment, item.total, item.maxValue)
                )
            );
        });
    };

    return React.createElement(
        'div',
        Object.assign({ className: 'list-top-trends-sentiment' }, styles(moduleColor, moduleSide)),
        renderTitle(),
        React.createElement(
            'div',
            { className: 'list-sentiment__body' },
            React.createElement(
                'div',
                { className: 'list-sentiment__body__title' },
                React.createElement(
                    'h5',
                    null,
                    'Listado'
                ),
                React.createElement(
                    'h5',
                    null,
                    entityType[moduleConfig.entity_type].name
                )
            ),
            React.createElement(
                'ul',
                null,
                renderItems()
            )
        ),
        React.createElement(
            'div',
            { className: 'list-sentiment__footer' },
            React.createElement(
                'h5',
                null,
                'Positivo'
            ),
            React.createElement(
                'h5',
                null,
                'Neutral'
            ),
            React.createElement(
                'h5',
                null,
                'Negativo'
            )
        )
    );
};

ListTopTrendsSentiment.propTypes = propTypes;
ListTopTrendsSentiment.defaultProps = defaultProps;

export default ListTopTrendsSentiment;