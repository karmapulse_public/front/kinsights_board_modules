import { css } from 'glamor';

var styles = function styles(color, side) {
    var bgColor = 'transparent';

    return css({
        width: '100%',
        height: 'auto',
        display: 'flex',
        alignItems: 'center',
        flexWrap: 'wrap',
        backgroundColor: bgColor,
        color: 'rgba(0,0,0, 0.8)',

        '>ul': {
            width: '100%',
            padding: '20px'
        },

        ' .list__label': {
            width: '100%',
            '> h3': {
                padding: '20px',
                textTransform: 'uppercase',
                fontSize: 16,
                fontWeight: 400,
                textAlign: 'left',
                color: color
            },
            ' ~ ul': {
                paddingTop: 0
            }
        },

        ' .list-sentiment__body': {
            width: '100%',
            padding: '0 15px',

            ' &__title': {
                display: 'flex',
                paddingTop: 5,
                flexDirection: side === 'left' ? 'row' : 'row-reverse',
                justifyContent: 'space-between',

                ' >h5': {
                    fontSize: '10px'

                }
            },

            ' .list__item': {
                padding: '15px 0',
                borderBottom: 'solid 1px #e3e3e3',

                ' &:last-child': {
                    borderBottom: 'none'
                },

                ' &__data': {
                    position: 'relative',
                    display: 'flex',
                    alignItems: 'center',
                    flexDirection: side === 'left' ? 'row' : 'row-reverse',

                    ' .index': {
                        width: 20,
                        height: 20,
                        fontSize: '13px',
                        fontWeight: 500,
                        textAlign: 'center',
                        lineHeight: '20px',
                        background: '#f2f2f2'
                    },

                    ' .subject': {
                        padding: '0 10px',
                        fontSize: '13px',
                        fontWeight: 500
                    },

                    ' .total': {
                        position: 'absolute',
                        right: side === 'right' ? 'auto' : '0',
                        left: side === 'left' ? 'auto' : '0',
                        fontSize: '14px',
                        fontWeight: 500
                    },
                    ' .list__dot': {
                        width: 11,
                        height: 11,
                        borderRadius: 200
                    }
                },

                ' &__sentiment': {
                    display: 'flex',
                    paddingTop: 10,
                    justifyContent: side === 'right' ? 'flex-end' : 'flex-start',

                    ' .positivo': {
                        width: 45,
                        height: 20,
                        margin: '0 10px 0 0',
                        borderRadius: '2px',
                        fontWeight: 500,
                        fontSize: '13px',
                        lineHeight: '20px',
                        textAlign: 'center',
                        background: '#none',
                        color: '#87cf8b',

                        ' &.active': {
                            background: '#87cf8b',
                            color: '#fff'
                        }
                    },

                    ' .negativo': {
                        width: 45,
                        height: 20,
                        margin: '0 0 0 10px',
                        borderRadius: '2px',
                        fontWeight: 500,
                        fontSize: '13px',
                        lineHeight: '20px',
                        textAlign: 'center',
                        background: '#none',
                        color: '#ea3c3c',
                        ' &.active': {
                            background: '#ea3c3c',
                            color: '#fff'
                        }
                    },

                    ' .neutral': {
                        width: 45,
                        height: 20,
                        margin: '0 10px',
                        borderRadius: '2px',
                        fontWeight: 500,
                        fontSize: '13px',
                        lineHeight: '20px',
                        textAlign: 'center',
                        background: 'none',
                        color: '#89a7ce',

                        ' &.active': {
                            background: '#89a7ce',
                            color: '#fff'
                        }
                    }
                }
            }
        },

        ' .list-sentiment__footer': {
            display: 'flex',
            width: '100%',
            padding: '20px 15px',
            fontSize: '10px',
            color: 'rgba(0,0,0, 0.5)',
            justifyContent: 'center',

            ' >h5': {
                position: 'relative',
                padding: '0 12px',

                ' &::before': {
                    position: 'absolute',
                    width: 6,
                    height: 6,
                    top: 1,
                    left: 0,
                    content: "''",
                    background: '#89a7ce',
                    borderRadius: '50%'
                },

                ' &:first-child::before': {
                    background: '#87cf8b'
                },
                ' &:last-child::before': {
                    background: '#ea3c3c'
                }
            }
        }
    });
};

export default styles;