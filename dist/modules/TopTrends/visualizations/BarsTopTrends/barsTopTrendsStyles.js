import { css } from 'glamor';

var styles = function styles(color) {
    var bgColor = 'transparent';

    return css({
        width: '100%',
        height: 'auto',
        padding: '0 20px',
        display: 'flex',
        alignItems: 'center',
        backgroundColor: bgColor,
        color: 'rgba(0,0,0, 0.5)',

        '>ul': {
            width: '100%',
            padding: '20px 0px'
        },

        ' .bars-list__item': {
            position: 'relative',
            width: '100%',
            paddingLeft: 40,
            boxSizing: 'border-box',
            '&:not(:last-of-type)': {
                marginBottom: 15
            },
            ' h4': {
                fontSize: 14,
                lineHeight: 1.2,
                fontWeight: 400,
                textAlign: 'left',
                color: '#000',
                opacity: 0.8,
                '@media screen and (min-width:1024px)': {
                    fontSize: 16
                }
            },
            ' h5': {
                marginTop: 3,
                fontSize: 12,
                fontWeight: 400,
                textAlign: 'left',
                color: '#000',
                opacity: 0.5
            },
            ' span': {
                position: 'absolute',
                left: 0,
                width: 30,
                height: 30,
                backgroundColor: '#efefef',
                color: '#000',
                fontSize: 14,
                fontWeight: 400,
                lineHeight: '30px',
                textAlign: 'center'
            }
        }
    });
};

export default styles;