import React from 'react';
import PropTypes from 'prop-types';
import maxBy from 'lodash/maxBy';
import { css } from 'glamor';

import styles from './barsTopTrendsStyles';
import { numberWithCommas } from '../../../../helpers/number';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.array,
    moduleColor: PropTypes.string
};

var defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleColor: '#666'
};

var BarsTopTrends = function BarsTopTrends(_ref) {
    var moduleConfig = _ref.moduleConfig,
        moduleData = _ref.moduleData,
        moduleColor = _ref.moduleColor;

    var colors = ['#c75a93', '#8176cc', '#cc5f43', '#5ba966', '#54a2d3'];

    var symbol = {
        hashtags: '#',
        mentions: '@',
        keywords: ''
    };

    var maxValue = maxBy(moduleData, 'doc_count');

    var dataFormat = moduleData.map(function (item) {
        return {
            subject: symbol[moduleConfig.entity_type] + item.key,
            total: item.doc_count,
            fullMark: maxValue.doc_count
        };
    });

    var renderTitle = function renderTitle() {
        if (moduleConfig.show_title) {
            var _moduleConfig$title = moduleConfig.title,
                title = _moduleConfig$title === undefined ? 'RELACIÓN DE TEMAS' : _moduleConfig$title;

            return React.createElement(
                'div',
                { className: 'bars-list__label' },
                React.createElement(
                    'h3',
                    null,
                    title
                )
            );
        }
        return '';
    };

    var renderBars = function renderBars(index, num) {
        var customStyles = css({
            width: num * 100 / maxValue.doc_count + '%',
            height: 5,
            marginTop: 10,
            backgroundColor: colors[index]
        });
        return React.createElement('div', Object.assign({}, customStyles, { className: 'bars-list__bar' }));
    };

    var renderItems = function renderItems() {
        return dataFormat.map(function (item, index) {
            return React.createElement(
                'li',
                { className: 'bars-list__item', key: index },
                React.createElement(
                    'span',
                    null,
                    index + 1 + '.'
                ),
                React.createElement(
                    'h4',
                    { className: 'bars-list__name' },
                    item.subject
                ),
                React.createElement(
                    'h5',
                    { className: 'bars-list__value' },
                    numberWithCommas(item.total),
                    ' Menciones'
                ),
                renderBars(index, item.total)
            );
        });
    };

    return React.createElement(
        'div',
        Object.assign({ className: 'bars-top-trends' }, styles(moduleColor)),
        renderTitle(),
        React.createElement(
            'ul',
            null,
            renderItems()
        )
    );
};

BarsTopTrends.propTypes = propTypes;
BarsTopTrends.defaultProps = defaultProps;

export default BarsTopTrends;