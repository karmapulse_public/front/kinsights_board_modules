function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import { Observable } from 'rxjs';
import { fetchXHR } from '../../helpers/http';

export var fetchTweetsDist = function fetchTweetsDist(url, params) {
    return Observable.create(function (observer) {
        Observable.fromPromise(fetchXHR(url, 'GET', params)).subscribe(function (data) {
            var formatData = data.json.response.intervals.map(function (item) {
                return _defineProperty({
                    key: item.date
                }, params.filters.entity, item.total);
            });
            observer.next({
                total: data.json.response.total,
                average: data.json.response.average,
                maximum: data.json.response.maximum,
                values: {
                    aggregations: {
                        buckets: formatData
                    }
                }
            });
        }, function (error) {
            observer.error(error);
        });
    });
};

export var fetchSentimentDist = function fetchSentimentDist(url, params) {
    return Observable.create(function (observer) {
        Observable.fromPromise(fetchXHR(url, 'GET', params)).subscribe(function (data) {
            var formatData = data.json.response.intervals.map(function (item) {
                return {
                    date: item.date,
                    neutral: item.total_neutral,
                    positivo: item.total_positive,
                    negativo: item.total_negative
                };
            });
            observer.next({
                total: data.json.response.total,
                total_positivo: data.json.response.total_positive,
                total_neutral: data.json.response.total_neutral,
                total_negativo: data.json.response.total_negative,
                values: formatData
            });
        }, function (error) {
            observer.error(error);
        });
    });
};