var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import isNull from 'lodash/isNull';
import isUndefined from 'lodash/isUndefined';

import LoadingModule from '../LoadingModule';
import { calculateNewInterval } from '../../helpers/getTimeInterval';
import { fetchTweetsDist, fetchSentimentDist } from './TDistributionDataflow';

import TweetsHistogram from './visualizations/TweetsHistogram';
import SentimentHistogram from './visualizations/SentimentHistogram';

var propTypes = {
    fields: PropTypes.object,
    color: PropTypes.string,
    dateRange: PropTypes.object,
    services: PropTypes.object
};

var defaultProps = {
    fields: {},
    color: '#666',
    dateRange: {},
    services: {}
};

var TimelineDistribution = function (_Component) {
    _inherits(TimelineDistribution, _Component);

    function TimelineDistribution(props) {
        _classCallCheck(this, TimelineDistribution);

        var _this = _possibleConstructorReturn(this, (TimelineDistribution.__proto__ || Object.getPrototypeOf(TimelineDistribution)).call(this, props));

        _this.setDateFilter = _this.setDateFilter.bind(_this);
        _this.myRef = React.createRef();
        _this.state = {
            moduleServices: props.services,
            moduleConfig: props.fields,
            moduleRange: props.dateRange,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleColor: props.color,
            intervalType: props.fields.intervalType || 'month',
            selectInterval: isUndefined(props.fields.selectInterval) ? true : props.fields.selectInterval
        };
        return _this;
    }

    _createClass(TimelineDistribution, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            this.setDateFilter();
        }
    }, {
        key: 'componentDidUpdate',
        value: function componentDidUpdate(prevProps) {
            var _props$dateRange = this.props.dateRange,
                startDate = _props$dateRange.startDate,
                endDate = _props$dateRange.endDate;
            var _props$fields = this.props.fields,
                hasClick = _props$fields.hasClick,
                search_id = _props$fields.search_id;

            if (startDate !== prevProps.dateRange.startDate || endDate !== prevProps.dateRange.endDate || search_id !== prevProps.fields.search_id) {
                this.setDateFilter('', this.props);
            }
            if (hasClick !== prevProps.fields.hasClick) {
                this.changeState(hasClick);
            }
        }
    }, {
        key: 'setDateFilter',
        value: function setDateFilter() {
            var interval = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
            var newProps = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : this.props;
            var _newProps$dateRange = newProps.dateRange,
                startDate = _newProps$dateRange.startDate,
                endDate = _newProps$dateRange.endDate;

            var startString = moment(startDate).startOf('day');
            var finishString = moment(endDate).endOf('day');
            var newInterval = this.state.intervalType;
            if (this.state.selectInterval) {
                newInterval = interval !== '' ? interval : calculateNewInterval(startString, finishString, this.myRef);
            }
            this.fetchData({
                rule_id: newProps.fields.search_id,
                initial_date: startString.utc().toISOString(),
                final_date: finishString.utc().toISOString()
            }, newInterval, newProps.dateRange);
        }
    }, {
        key: 'fetchData',
        value: function fetchData(newParams, timeInterval, newDateRange) {
            var _this2 = this;

            var moduleConfig = this.state.moduleConfig;

            var recipeId = {
                timeline_lines: 'module_tw_histogram',
                timeline_sentiment: 'module_tw_distribution_sentiment'
            };
            var fetchQuery = {
                timeline_lines: fetchTweetsDist,
                timeline_sentiment: fetchSentimentDist
            };
            var params = {
                recipe_id: recipeId[moduleConfig.visualization],
                rule_id: newParams.rule_id,
                initial_date: newParams.initial_date,
                final_date: newParams.final_date,
                filters: {
                    entity: moduleConfig.entity,
                    interval: timeInterval,
                    timezone: 'America/Mexico_City'
                }
            };
            fetchQuery[moduleConfig.visualization](this.props.services.twitter, params).subscribe(function (data) {
                _this2.setState({
                    moduleData: data,
                    moduleRange: newDateRange,
                    moduleLoading: false,
                    intervalType: timeInterval
                });
            }, function (error) {
                console.log(error); // eslint-disable-line
                _this2.setState({
                    moduleData: {},
                    moduleRange: _this2.state.moduleRange,
                    moduleLoading: false,
                    intervalType: timeInterval
                });
            }, null);
        }
    }, {
        key: 'changeState',
        value: function changeState(hasClick) {
            this.setState({
                moduleConfig: Object.assign({}, this.state.moduleConfig, {
                    hasClick: hasClick
                })
            });
        }
    }, {
        key: 'renderByViz',
        value: function renderByViz() {
            var _this3 = this;

            var _state = this.state,
                moduleConfig = _state.moduleConfig,
                moduleData = _state.moduleData;

            if (!isNull(moduleData)) {
                var visualizations = {
                    timeline_lines: function timeline_lines(params) {
                        return React.createElement(TweetsHistogram, Object.assign({}, params, {
                            changeInterval: _this3.setDateFilter
                        }));
                    },
                    timeline_sentiment: function timeline_sentiment(params) {
                        return React.createElement(SentimentHistogram, Object.assign({}, params, {
                            changeInterval: _this3.setDateFilter
                        }));
                    }
                };
                return visualizations[moduleConfig.visualization](this.state);
            }

            return React.createElement(LoadingModule, null);
        }
    }, {
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'm-timeline-distribution', ref: this.myRef },
                this.renderByViz()
            );
        }
    }]);

    return TimelineDistribution;
}(Component);

TimelineDistribution.propTypes = propTypes;
TimelineDistribution.defaultProps = defaultProps;

export default TimelineDistribution;