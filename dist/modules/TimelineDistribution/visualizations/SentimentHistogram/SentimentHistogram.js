var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component, Fragment } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';

import styles from './styles';
import NoDataModule from '../../../NoDataModule';
import Intervals from '../../../Charts/Intervals';
import { labelType } from '../../../../helpers/paddingDates';
import { numberWithCommas } from '../../../../helpers/number';
import DrawerContext from '../../../../helpers/context/ctxDrawer';
import ChartColumns from '../../../Charts/ChartColumns/ChartColumns';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleRange: PropTypes.object,
    moduleColor: PropTypes.string,
    moduleServices: PropTypes.string,
    intervalType: PropTypes.string,
    changeInterval: PropTypes.func
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleRange: {},
    moduleServices: {},
    moduleColor: '#666',
    intervalType: 'month',
    changeInterval: function changeInterval() {
        return {};
    }
};

var SentimentHistogram = function (_Component) {
    _inherits(SentimentHistogram, _Component);

    function SentimentHistogram(props) {
        _classCallCheck(this, SentimentHistogram);

        var _this = _possibleConstructorReturn(this, (SentimentHistogram.__proto__ || Object.getPrototypeOf(SentimentHistogram)).call(this, props));

        _this.handleClickEvent = _this.handleClickEvent.bind(_this);
        _this.handleCloseDrawer = _this.handleCloseDrawer.bind(_this);
        _this.initialState = {
            openDrawer: false,
            drawerData: {
                type: 'TWITTER',
                service: '',
                title: '',
                subtitleLeft: '',
                subtitleRight: '',
                queryData: {}
            }
        };
        _this.state = _this.initialState;
        return _this;
    }

    _createClass(SentimentHistogram, [{
        key: 'handleCloseDrawer',
        value: function handleCloseDrawer() {
            this.setState(Object.assign({}, this.initialState));
        }
    }, {
        key: 'handleClickEvent',
        value: function handleClickEvent(data) {
            var _props = this.props,
                intervalType = _props.intervalType,
                moduleRange = _props.moduleRange,
                moduleConfig = _props.moduleConfig,
                moduleServices = _props.moduleServices;

            if (!moduleConfig.hasClick) return;
            var _moduleConfig$title = moduleConfig.title,
                title = _moduleConfig$title === undefined ? 'TWEETS EN EL TIEMPO' : _moduleConfig$title;

            var startDate = void 0;
            var finishDate = void 0;
            var subtitleLeft = void 0;
            if (data.activeLabel) {
                startDate = moment(data.activeLabel).isBefore(moduleRange.startDate) ? moment(moduleRange.startDate).format() : moment(data.activeLabel).format();
                finishDate = moment(data.activeLabel).add(1, intervalType).isAfter(moduleRange.endDate) ? moment(moduleRange.endDate).format() : moment(data.activeLabel).add(1, intervalType).subtract(1, 'ms').format();
                subtitleLeft = '' + labelType(startDate, finishDate, intervalType);
            } else {
                startDate = moment(moduleRange.startDate).format();
                finishDate = moment(moduleRange.endDate).format();
                subtitleLeft = '' + labelType(startDate, finishDate, intervalType);
            }
            var queryData = {
                'recipe-id': 'module_tw_explore_tweets',
                'rule-id': moduleConfig.search_id,
                'initial-date': startDate,
                'final-date': finishDate,
                filters: {
                    limit: 10
                }
            };
            this.setState({
                openDrawer: true,
                drawerData: {
                    type: 'TWITTER',
                    service: moduleServices.twitter,
                    title: title,
                    subtitleLeft: subtitleLeft,
                    subtitleRight: '',
                    queryData: queryData
                }
            });
        }
    }, {
        key: 'handleClickSentiment',
        value: function handleClickSentiment(sentiment) {
            var _props2 = this.props,
                moduleRange = _props2.moduleRange,
                moduleConfig = _props2.moduleConfig,
                moduleServices = _props2.moduleServices;

            if (!moduleConfig.hasClick) return;
            var _moduleConfig$title2 = moduleConfig.title,
                title = _moduleConfig$title2 === undefined ? 'TWEETS EN EL TIEMPO' : _moduleConfig$title2;

            var finalWord = sentiment === 'neutral' ? 'es' : 's';
            var queryData = {
                'recipe-id': 'module_tw_explore_tweets',
                'rule-id': moduleConfig.search_id,
                'initial-date': moment(moduleRange.startDate).format(),
                'final-date': moment(moduleRange.endDate).format(),
                filters: {
                    limit: 10,
                    sentiment: sentiment
                }
            };
            this.setState({
                openDrawer: true,
                drawerData: {
                    type: 'TWITTER',
                    service: moduleServices.twitter,
                    title: title,
                    subtitleLeft: 'Tweets ' + sentiment + finalWord,
                    subtitleRight: '',
                    queryData: queryData
                }
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var _props3 = this.props,
                moduleConfig = _props3.moduleConfig,
                moduleData = _props3.moduleData,
                moduleColor = _props3.moduleColor,
                moduleRange = _props3.moduleRange,
                intervalType = _props3.intervalType,
                changeInterval = _props3.changeInterval;
            var _moduleConfig$title3 = moduleConfig.title,
                title = _moduleConfig$title3 === undefined ? 'SENTIMIENTO EN EL TIEMPO' : _moduleConfig$title3;

            var dataKeys = [{
                key: 'positivo',
                color: '#89cf41'
            }, {
                key: 'neutral',
                color: '#95bbd4'
            }, {
                key: 'negativo',
                color: '#e18080'
            }];
            var nameArray = ['Positivo', 'Neutral', 'Negativo'];
            var labels = moduleConfig.labelTitles || { x: 'Periodo', y: 'Volumen de tweets' };

            return React.createElement(
                'div',
                Object.assign({ className: 'sentiment-histogram-chart' }, styles(moduleColor)),
                React.createElement(
                    'div',
                    { className: 'sentiment-histogram__label' },
                    React.createElement(
                        'h3',
                        null,
                        title
                    )
                ),
                this.props.selectInterval ? React.createElement(Intervals, {
                    dateRange: moduleRange,
                    intervalType: intervalType,
                    handleChangeInterval: changeInterval
                }) : null,
                moduleData.total === 0 ? React.createElement(
                    'div',
                    { className: 'noData' },
                    React.createElement(NoDataModule, { color: moduleColor })
                ) : React.createElement(
                    Fragment,
                    null,
                    React.createElement(
                        DrawerContext.Provider,
                        {
                            value: {
                                open: this.state.openDrawer,
                                drawerData: this.state.drawerData,
                                onCloseDrawer: this.handleCloseDrawer
                            }
                        },
                        React.createElement(
                            'div',
                            { className: 'sentiment-histogram__chart' },
                            React.createElement(ChartColumns, {
                                labels: labels,
                                dataKeys: dataKeys,
                                nameArray: nameArray,
                                data: moduleData.values,
                                intervalType: intervalType,
                                onBarClick: this.handleClickEvent
                            })
                        )
                    ),
                    React.createElement(
                        'div',
                        { className: 'sentiment-histogram__totals' },
                        React.createElement(
                            'div',
                            { // eslint-disable-line
                                onClick: function onClick() {
                                    return _this2.handleClickSentiment('positivo');
                                }
                            },
                            React.createElement(
                                'h5',
                                { style: { color: dataKeys[0].color } },
                                numberWithCommas(moduleData.total_positivo)
                            ),
                            React.createElement(
                                'h6',
                                null,
                                moduleConfig.totalTitles ? moduleConfig.totalTitles.pos : 'Total de tweets positivos'
                            )
                        ),
                        React.createElement(
                            'div',
                            { // eslint-disable-line
                                onClick: function onClick() {
                                    return _this2.handleClickSentiment('neutral');
                                }
                            },
                            React.createElement(
                                'h5',
                                { style: { color: dataKeys[1].color } },
                                numberWithCommas(moduleData.total_neutral)
                            ),
                            React.createElement(
                                'h6',
                                null,
                                moduleConfig.totalTitles ? moduleConfig.totalTitles.ne : 'Total de tweets positivos'
                            )
                        ),
                        React.createElement(
                            'div',
                            { // eslint-disable-line
                                onClick: function onClick() {
                                    return _this2.handleClickSentiment('negativo');
                                }
                            },
                            React.createElement(
                                'h5',
                                { style: { color: dataKeys[2].color } },
                                numberWithCommas(moduleData.total_negativo)
                            ),
                            React.createElement(
                                'h6',
                                null,
                                moduleConfig.totalTitles ? moduleConfig.totalTitles.neg : 'Total de tweets positivos'
                            )
                        )
                    )
                )
            );
        }
    }]);

    return SentimentHistogram;
}(Component);

SentimentHistogram.propTypes = propTypes;
SentimentHistogram.defaultProps = defaultProps;

export default SentimentHistogram;