import { css } from 'glamor';

var styles = function styles(color) {
    return css({
        width: 700,

        ' .tooltip_custom': {
            backgroundColor: 'rgba(255, 255, 255, .9)',
            boxShadow: '2px 2px 10px rgba(0, 0, 0, .2)',
            fontSize: 12,
            padding: 10,
            borderRadius: 2,
            letterSpacing: '1px',
            lineHeight: '20px'
        },
        ' h4': {
            padding: '20px 40px',
            fontSize: '13px',
            fontWeight: 500,
            ' span': {
                fontSize: 16,
                float: 'right'
            }
        },
        ' .sentiment-histogram': {
            ' &__label': {
                '> h3': {
                    padding: '15px 20px 30px',
                    fontSize: 16,
                    fontWeight: 400,
                    textAlign: 'left',
                    textTransform: 'uppercase',
                    color: color
                }
            },
            ' &__chart': {
                padding: 30,
                ' .recharts-cartesian-grid-horizontal line': {
                    stroke: ' rgba(204,204,204,0.2)'
                },
                ' .recharts-legend-wrapper': {
                    bottom: '0px !important',
                    left: '0px !important',
                    width: '345px !important'
                },
                ' .column-data__label': {
                    fontSize: 12,
                    textAlign: 'justify',
                    opacity: 0.5,
                    color: 'rgb(43, 43, 43)',
                    textAnchor: 'middle'
                }
            },
            ' &__totals': {
                width: '100%',
                display: 'flex',
                justifyContent: 'space-around',
                ' h5': {
                    fontSize: 28,
                    fontWeight: 500
                },
                ' h6': {
                    fontSize: 17
                }
            }
        }
    });
};

export default styles;