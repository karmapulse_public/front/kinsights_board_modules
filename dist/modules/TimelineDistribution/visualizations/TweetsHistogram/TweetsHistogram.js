var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component, Fragment } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';

import styles from './styles';
import Lines from '../../../Charts/ChartsLines';
import NoDataModule from '../../../NoDataModule';
import Intervals from '../../../Charts/Intervals';
import { labelType } from '../../../../helpers/paddingDates';
import { numberWithCommas } from '../../../../helpers/number';
import withDrawer from '../../../withDrawer';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleRange: PropTypes.object,
    moduleColor: PropTypes.string,
    moduleServices: PropTypes.object,
    intervalType: PropTypes.string,
    changeInterval: PropTypes.func
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleRange: {},
    moduleServices: {},
    moduleColor: '#666',
    intervalType: 'month',
    changeInterval: function changeInterval() {
        return {};
    }
};

var labelInterval = {
    month: 'mes',
    week: 'semana',
    day: 'día',
    hour: 'hora'
};

var TweetsHistogram = function (_Component) {
    _inherits(TweetsHistogram, _Component);

    function TweetsHistogram(props) {
        _classCallCheck(this, TweetsHistogram);

        var _this = _possibleConstructorReturn(this, (TweetsHistogram.__proto__ || Object.getPrototypeOf(TweetsHistogram)).call(this, props));

        _this.handleClickEvent = _this.handleClickEvent.bind(_this);
        _this.handleCloseDrawer = _this.handleCloseDrawer.bind(_this);
        _this.refExport = React.createRef(null);
        _this.initialState = {
            openDrawer: false,
            drawerData: {
                type: 'TWITTER',
                service: '',
                title: '',
                subtitleLeft: '',
                subtitleRight: '',
                queryData: {}
            }
        };
        _this.state = _this.initialState;
        return _this;
    }

    _createClass(TweetsHistogram, [{
        key: 'handleCloseDrawer',
        value: function handleCloseDrawer() {
            this.setState(Object.assign({}, this.initialState));
        }
    }, {
        key: 'handleClickEvent',
        value: function handleClickEvent(data) {
            var _props = this.props,
                intervalType = _props.intervalType,
                moduleRange = _props.moduleRange,
                moduleConfig = _props.moduleConfig,
                onClick = _props.onClick;
            var _moduleConfig$title = moduleConfig.title,
                title = _moduleConfig$title === undefined ? 'TWEETS EN EL TIEMPO' : _moduleConfig$title;


            var startDate = void 0;
            var finishDate = void 0;
            var subtitleLeft = void 0;
            if (data.payload) {
                startDate = moment(data.payload.key).isBefore(moduleRange.startDate) ? moment(moduleRange.startDate).format() : moment(data.payload.key).format();
                finishDate = moment(data.payload.key).add(1, intervalType).isAfter(moduleRange.endDate) ? moment(moduleRange.endDate).format() : moment(data.payload.key).add(1, intervalType).subtract(1, 'ms').format();
                subtitleLeft = data.dataKey + ' del ' + labelType(startDate, finishDate, intervalType);
            } else {
                startDate = moment(moduleRange.startDate).format();
                finishDate = moment(moduleRange.endDate).format();
                subtitleLeft = '' + data.dataKey;
            }
            onClick({
                'initial-date': startDate,
                'final-date': finishDate
            }, {
                title: title,
                subtitleLeft: subtitleLeft,
                subtitleRight: ''
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var _props2 = this.props,
                moduleConfig = _props2.moduleConfig,
                moduleData = _props2.moduleData,
                moduleColor = _props2.moduleColor,
                moduleRange = _props2.moduleRange,
                intervalType = _props2.intervalType,
                changeInterval = _props2.changeInterval;
            var _moduleConfig$title2 = moduleConfig.title,
                title = _moduleConfig$title2 === undefined ? 'TWEETS EN EL TIEMPO' : _moduleConfig$title2,
                entity = moduleConfig.entity,
                lineColor = moduleConfig.lineColor,
                hasClick = moduleConfig.hasClick,
                buttonExport = moduleConfig.buttonExport;

            var labels = { x: 'Periodo', y: 'Volumen de tweets' };
            return React.createElement(
                'div',
                Object.assign({
                    ref: this.refExport,
                    className: 'tweets-histogram-chart'
                }, styles(moduleColor)),
                React.createElement(
                    'div',
                    { className: 'tweets-histogram__label' },
                    React.createElement(
                        'h3',
                        null,
                        title
                    )
                ),
                React.createElement(Intervals, {
                    dateRange: moduleRange,
                    intervalType: intervalType,
                    handleChangeInterval: changeInterval
                }),
                moduleData.total === 0 ? React.createElement(
                    'div',
                    { className: 'noData' },
                    React.createElement(NoDataModule, { color: moduleColor })
                ) : React.createElement(
                    Fragment,
                    null,
                    React.createElement(
                        'div',
                        { className: 'tweets-histogram__chart' },
                        React.createElement(Lines, {
                            type: 'single',
                            entity: entity,
                            labels: labels,
                            colors: [lineColor],
                            data: moduleData.values,
                            intervalType: intervalType,
                            onLineClick: this.handleClickEvent,
                            hasClick: hasClick
                        })
                    ),
                    React.createElement(
                        'div',
                        { className: 'tweets-histogram__totals' },
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'h5',
                                null,
                                numberWithCommas(moduleData.total)
                            ),
                            React.createElement(
                                'h6',
                                null,
                                'Total de tweets'
                            )
                        ),
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'h5',
                                null,
                                numberWithCommas(Math.ceil(moduleData.average))
                            ),
                            React.createElement(
                                'h6',
                                null,
                                'Promedio de tweets por ',
                                labelInterval[intervalType]
                            )
                        ),
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'h5',
                                null,
                                numberWithCommas(moduleData.maximum)
                            ),
                            React.createElement(
                                'h6',
                                null,
                                'M\xE1ximo de tweets por ',
                                labelInterval[intervalType]
                            )
                        )
                    )
                )
            );
        }
    }]);

    return TweetsHistogram;
}(Component);

TweetsHistogram.propTypes = propTypes;
TweetsHistogram.defaultProps = defaultProps;

export default withDrawer(TweetsHistogram);