var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';

import Wordcloud from './visualizations/WordCloudTrends';
import ListTopTrends from './visualizations/ListTopTrends';
import fetchTopTT from './FbTTCommentsDataFlow';
import LoadingModule from '../LoadingModule';

var propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    view: PropTypes.string,
    services: PropTypes.object,
    dateRange: PropTypes.object
};

var defaultProps = {
    fields: {},
    side: 'left',
    color: '#666',
    services: {},
    dateRange: {},
    view: ''
};

var FbTopTopicsComments = function (_Component) {
    _inherits(FbTopTopicsComments, _Component);

    function FbTopTopicsComments(props) {
        _classCallCheck(this, FbTopTopicsComments);

        var _this = _possibleConstructorReturn(this, (FbTopTopicsComments.__proto__ || Object.getPrototypeOf(FbTopTopicsComments)).call(this, props));

        _this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color,
            moduleView: props.view
        };
        return _this;
    }

    _createClass(FbTopTopicsComments, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            this.fetchData(this.props);
        }
    }, {
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProps) {
            this.fetchData(nextProps);
        }
    }, {
        key: 'fetchData',
        value: function fetchData(newProps) {
            var _this2 = this;

            var _newProps$dateRange = newProps.dateRange,
                startDate = _newProps$dateRange.startDate,
                endDate = _newProps$dateRange.endDate;
            var search_id = newProps.fields.search_id;

            var params = {
                recipe_id: 'module_fb_top_phrases_in_comments',
                rule_id: search_id,
                initial_date: moment(startDate).utc().toISOString(),
                final_date: moment(endDate).utc().toISOString(),
                filters: {
                    top: newProps.fields.top_limit
                }
            };

            fetchTopTT(this.props.services.facebook, params).subscribe(function (data) {
                _this2.setState({
                    moduleData: data,
                    moduleConfig: newProps.fields,
                    moduleLoading: false,
                    moduleColor: newProps.color,
                    moduleSide: newProps.side
                });
            }, function (error) {
                console.log(error);
            }, null);
        }
    }, {
        key: 'renderByViz',
        value: function renderByViz() {
            var _state = this.state,
                moduleData = _state.moduleData,
                moduleConfig = _state.moduleConfig;

            if (moduleData) {
                var visualizations = {
                    wordcloud_top_trends: function wordcloud_top_trends(params) {
                        return React.createElement(Wordcloud, params);
                    },
                    list_top_trends: function list_top_trends(params) {
                        return React.createElement(ListTopTrends, params);
                    }
                };
                var v = this.state.moduleView !== '' ? this.state.moduleView : moduleConfig.visualization;
                return visualizations[v](this.state);
            }
            return React.createElement(LoadingModule, null);
        }
    }, {
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'm-fb-top-topics-comments' },
                this.renderByViz()
            );
        }
    }]);

    return FbTopTopicsComments;
}(Component);

FbTopTopicsComments.propTypes = propTypes;
FbTopTopicsComments.defaultProps = defaultProps;

export default FbTopTopicsComments;