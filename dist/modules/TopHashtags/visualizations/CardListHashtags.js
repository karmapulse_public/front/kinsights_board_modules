import React from 'react';
import PropTypes from 'prop-types';
import { css } from 'glamor';

import styles from './cardListHashtagsStyles';
import { numberWithCommas } from '../../../helpers/number';

var colors = ['#c95885', '#5ba85f', '#9d64c6', '#ab973d', '#6493cd'];

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.array,
    moduleSide: PropTypes.string,
    moduleColor: PropTypes.string
};

var defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleSide: 'left',
    moduleColor: '#666'
};

var CardListHashtags = function CardListHashtags(_ref) {
    var moduleConfig = _ref.moduleConfig,
        moduleData = _ref.moduleData,
        moduleSide = _ref.moduleSide,
        moduleColor = _ref.moduleColor;

    var showBars = moduleConfig.show_chart_bars;

    var renderBars = function renderBars(index) {
        var customStyles = css({
            width: 100 - index * 15 + '%',
            height: 5,
            marginTop: 10,
            backgroundColor: colors[index]
        });
        if (showBars) {
            return React.createElement('div', Object.assign({}, customStyles, { className: 'hashtags__bar' }));
        }

        return '';
    };

    var _moduleConfig$title = moduleConfig.title,
        title = _moduleConfig$title === undefined ? 'HASHTAG MÁS USADO' : _moduleConfig$title;


    return React.createElement(
        'div',
        Object.assign({ className: 'card_list_hashtags' }, styles(moduleSide, moduleColor)),
        React.createElement(
            'div',
            { className: 'hashtags__label' },
            React.createElement(
                'h3',
                null,
                title
            )
        ),
        React.createElement(
            'ul',
            { className: 'hashtags__list' },
            moduleData.map(function (hashtag, index) {
                return React.createElement(
                    'li',
                    { key: index },
                    React.createElement(
                        'span',
                        null,
                        index + 1 + '.'
                    ),
                    React.createElement(
                        'h4',
                        null,
                        '#',
                        hashtag.label
                    ),
                    React.createElement(
                        'h5',
                        null,
                        'Menciones: ',
                        numberWithCommas(hashtag.total)
                    ),
                    renderBars(index)
                );
            })
        )
    );
};

CardListHashtags.propTypes = propTypes;
CardListHashtags.defaultProps = defaultProps;

export default CardListHashtags;