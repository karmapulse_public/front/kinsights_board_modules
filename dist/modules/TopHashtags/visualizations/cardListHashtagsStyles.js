import { css } from 'glamor';

var styles = function styles(side, color) {
    var bgColor = 'transparent';

    return css({
        minWidth: 300,
        height: '100%',
        backgroundColor: bgColor,
        color: 'rgba(0,0,0, 0.5)',

        ' .hashtags__label': {
            '> h3': {
                padding: '15px 20px',
                textTransform: 'uppercase',
                fontSize: 16,
                fontWeight: 400,
                textAlign: side,
                color: color
            }
        },

        ' .hashtags__bar': {
            float: side
        },

        ' .hashtags__list': {
            padding: '0px 20px 10px',
            display: 'flex',
            flexDirection: 'row',
            flexWrap: 'wrap',
            alignItems: 'flex-start',
            justifyContent: side === 'left' ? 'flex-start' : 'flex-end',

            ' li': {
                position: 'relative',
                width: 300,
                marginBottom: 15,
                paddingLeft: side === 'left' ? 40 : 20,
                paddingRight: side === 'right' ? 40 : 20,
                boxSizing: 'border-box'
            },
            ' h4': {
                fontSize: 14,
                lineHeight: 1.2,
                fontWeight: 400,
                textAlign: side,
                color: '#000',
                opacity: 0.8,
                '@media screen and (min-width:1024px)': {
                    fontSize: 16
                }
            },
            ' h5': {
                marginTop: 3,
                fontSize: 12,
                fontWeight: 700,
                textAlign: side,
                color: color,
                opacity: 0.8
            },
            ' span': {
                position: 'absolute',
                left: side === 'left' ? 0 : 'auto',
                right: side === 'right' ? 0 : 'auto',
                width: 30,
                height: 30,
                backgroundColor: '#efefef',
                color: color,
                fontSize: 14,
                fontWeight: 400,
                lineHeight: '30px',
                textAlign: 'center'
            }
        }
    });
};

export default styles;