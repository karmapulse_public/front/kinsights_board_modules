import { Observable } from 'rxjs';
import { fetchXHR } from '../../helpers/http';

export default (function (url, params) {
    return Observable.create(function (observer) {
        Observable.fromPromise(fetchXHR(url, 'GET', params)).subscribe(function (data) {
            var sentiment = Object.assign({}, data.json.response.aggregations.entity.buckets.map(function (item) {
                return {
                    label: item.key,
                    total: item.doc_count
                };
            }));
            observer.next(Object.values(sentiment));
        }, function (error) {
            observer.error(error);
        });
    });
});