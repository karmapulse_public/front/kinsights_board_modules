import React from 'react';
import PropTypes from 'prop-types';
import showdown from 'showdown';
import moment from 'moment';
import styles from './MarkdownTextStyles';

var propTypes = {
    fields: PropTypes.object,
    sys: PropTypes.object,
    color: PropTypes.string
};

var defaultProps = {
    fields: {},
    sys: {},
    color: '#555'
};

var MarkdownText = function MarkdownText(_ref) {
    var _ref$fields = _ref.fields,
        markdown_text = _ref$fields.markdown_text,
        special_format = _ref$fields.special_format,
        updatedAt = _ref.sys.updatedAt,
        color = _ref.color;

    var converter = new showdown.Converter();
    var descriptionText = converter.makeHtml(markdown_text);
    var date = moment(updatedAt).format('MMM DD, YYYY H:mm');
    var s = !special_format ? styles : function () {
        return {};
    };
    return React.createElement(
        'div',
        Object.assign({}, s(), {
            className: 'm-markdown-text'
        }),
        React.createElement(
            'h4',
            { className: 'date' },
            date
        ),
        React.createElement('div', {
            className: 'module-markdown',
            dangerouslySetInnerHTML: {
                __html: descriptionText
            }
        })
    );
};

MarkdownText.propTypes = propTypes;
MarkdownText.defaultProps = defaultProps;

export default MarkdownText;