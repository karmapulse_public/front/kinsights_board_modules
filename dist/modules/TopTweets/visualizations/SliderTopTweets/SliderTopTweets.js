import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'kpulse_design_system/dist/Carousel';
import { faTwitter } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import NoDataModule from '../../../NoDataModule';
import styles from './styles';

import Gallery from '../../../../helpers/customComponents/Gallery';
import Header from '../../../../helpers/customComponents/Header';
import BodyText from '../../../../helpers/customComponents/BodyText';
import Card from '../../../../helpers/customComponents/Card';
import Link from '../../../../helpers/customComponents/Link';
import Video from '../../../../helpers/customComponents/Video';

import { numberWithCommas } from '../../../../helpers/number';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    noDataYet: PropTypes.func
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    noDataYet: null
};

var SliderTopTweets = function SliderTopTweets(_ref) {
    var moduleData = _ref.moduleData,
        moduleConfig = _ref.moduleConfig;

    var source = 'twitter';
    var renderContent = function renderContent() {
        return React.createElement(
            'div',
            { style: { position: 'relative' } },
            React.createElement(
                Slider,
                { width: '100%', height: '410px', dotsColor: '#b38e5d', arrowsColor: '#d4c19c', arrowsHeight: '20px' },
                moduleData.map(function (item, index) {
                    return React.createElement(
                        'div',
                        { className: 'card', key: index },
                        React.createElement(
                            'div',
                            { className: 'card__number' },
                            React.createElement(
                                'p',
                                null,
                                index + 1
                            )
                        ),
                        React.createElement(
                            'div',
                            { className: 'card__data' },
                            React.createElement(
                                Card,
                                { key: index, wTextDynamic: true },
                                React.createElement(Header, { noSource: true, item: item._source, services: moduleConfig }),
                                React.createElement(BodyText, { text: item._source.body, type: source }),
                                function () {
                                    if (item._source.media_type === 'image') {
                                        return React.createElement(Gallery, {
                                            items: item._source.images,
                                            height: '212px',
                                            type: source,
                                            url: item._source.link
                                        });
                                    }
                                    if (item._source.media_type === 'gif') {
                                        return React.createElement(Video, {
                                            sources: JSON.parse(item._source.gif.variants),
                                            cover: item._source.gif.cover,
                                            type: source,
                                            w: true,
                                            gif: true
                                        });
                                    }
                                    if (item._source.media_type === 'video') {
                                        return React.createElement(Video, {
                                            sources: JSON.parse(item._source.video.variants),
                                            cover: item._source.video.cover,
                                            type: source
                                        });
                                    }
                                    if (item._source.urls) {
                                        return (
                                            // eslint-disable-next-line jsx-a11y/anchor-is-valid
                                            React.createElement(Link, {
                                                url: item._source.urls[0].url,
                                                title: item._source.urls[0].title,
                                                text: item._source.urls[0].description,
                                                type: source
                                            })
                                        );
                                    }
                                    return null;
                                }()
                            )
                        ),
                        React.createElement(
                            'div',
                            { className: 'card__interactions' },
                            React.createElement(
                                'h5',
                                null,
                                React.createElement(
                                    'span',
                                    null,
                                    numberWithCommas(item._source.favorites)
                                ),
                                ' Me Gusta'
                            ),
                            React.createElement(
                                'h5',
                                null,
                                React.createElement(
                                    'span',
                                    null,
                                    numberWithCommas(item._source.retweets)
                                ),
                                ' Retweets'
                            )
                        )
                    );
                })
            )
        );
    };

    return React.createElement(
        'div',
        Object.assign({ className: 'top-tweets' }, styles),
        React.createElement(
            'div',
            { className: 'top-tweets__label' },
            React.createElement(
                'h3',
                null,
                'TWEETS CON M\xC1S INTERACCI\xD3N'
            ),
            React.createElement(
                'div',
                null,
                React.createElement(FontAwesomeIcon, { icon: faTwitter, color: 'white' })
            )
        ),
        moduleData.length !== 0 ? renderContent() : React.createElement(NoDataModule, null)
    );
};

SliderTopTweets.propTypes = propTypes;
SliderTopTweets.defaultProps = defaultProps;

export default SliderTopTweets;