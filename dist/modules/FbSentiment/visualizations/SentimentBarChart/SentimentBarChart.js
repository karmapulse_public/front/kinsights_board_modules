import React from 'react';
import PropTypes from 'prop-types';
import NoDataModule from '../../../NoDataModule';

import { numberWithCommas } from '../../../../helpers/number';
import styles from './styles';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666'
};

var SentimentBarChart = function SentimentBarChart(_ref) {
    var moduleData = _ref.moduleData,
        moduleConfig = _ref.moduleConfig,
        moduleColor = _ref.moduleColor;

    var getPercent = function getPercent(n) {
        return n === 0 ? 0 : n * 100 / moduleData.comments_total;
    };

    var _moduleData$comments_ = moduleData.comments_sentiment,
        positivo = _moduleData$comments_.positivo,
        negativo = _moduleData$comments_.negativo,
        neutral = _moduleData$comments_.neutral;


    var renderContent = function renderContent() {
        return React.createElement(
            'div',
            { className: 'sentiment-bar__content' },
            React.createElement(
                'h4',
                null,
                'Comentarios totales: ',
                React.createElement(
                    'span',
                    null,
                    numberWithCommas(moduleData.comments_total)
                )
            ),
            React.createElement(
                'h5',
                null,
                'Comentarios por sentimiento: '
            ),
            React.createElement(
                'div',
                { className: 'sentiment-bar__percent positive' },
                React.createElement(
                    'h6',
                    null,
                    'Positivo'
                ),
                React.createElement(
                    'div',
                    { className: 'sentiment-bar__bar' },
                    React.createElement('div', {
                        style: { width: getPercent(positivo) + '%' }
                    })
                ),
                React.createElement(
                    'span',
                    null,
                    getPercent(positivo).toFixed(1).replace(/\.0$/, '') + '%'
                )
            ),
            React.createElement(
                'div',
                { className: 'sentiment-bar__percent neutral' },
                React.createElement(
                    'h6',
                    null,
                    'Neutral'
                ),
                React.createElement(
                    'div',
                    { className: 'sentiment-bar__bar' },
                    React.createElement('div', {
                        style: { width: getPercent(neutral) + '%' }
                    })
                ),
                React.createElement(
                    'span',
                    null,
                    getPercent(neutral).toFixed(1).replace(/\.0$/, '') + '%'
                )
            ),
            React.createElement(
                'div',
                { className: 'sentiment-bar__percent negative' },
                React.createElement(
                    'h6',
                    null,
                    'Negativo'
                ),
                React.createElement(
                    'div',
                    { className: 'sentiment-bar__bar' },
                    React.createElement('div', { style: { width: getPercent(negativo) + '%' } })
                ),
                React.createElement(
                    'span',
                    null,
                    getPercent(negativo).toFixed(1).replace(/\.0$/, '') + '%'
                )
            )
        );
    };

    return React.createElement(
        'div',
        Object.assign({ className: 'sentiment-bar' }, styles(moduleColor)),
        React.createElement(
            'div',
            { className: 'sentiment-bar__label' },
            React.createElement(
                'h3',
                null,
                moduleConfig.title
            )
        ),
        moduleData.comments_total !== 0 ? renderContent() : React.createElement(NoDataModule, null)
    );
};

SentimentBarChart.propTypes = propTypes;
SentimentBarChart.defaultProps = defaultProps;

export default SentimentBarChart;