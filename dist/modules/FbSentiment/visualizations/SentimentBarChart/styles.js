import { css } from 'glamor';

var styles = function styles(color) {
    return css({
        width: '100%',
        minWidth: '320px',
        maxWidth: '500px',
        ' .sentiment-bar': {
            ' &__label': {
                padding: '15px 20px',
                borderTop: '1px solid #dfdfdf',
                ' h3': {
                    fontSize: '13px',
                    fontWeight: 600,
                    color: color
                }
            },
            ' &__content': {
                padding: '20px 40px 40px',
                ' h4': {
                    fontSize: 13,
                    fontWeight: 500,
                    ' span': {
                        fontSize: 16,
                        float: 'right'
                    }
                },
                ' h5': {
                    marginTop: 20,
                    marginBottom: 12,
                    fontSize: 13,
                    opacity: 0.5
                }
            },
            '&__percent': {
                position: 'relative',
                '&:not(:last-of-type)': {
                    marginBottom: 12
                },
                ' h6': {
                    fontSize: 13,
                    opacity: 0.5,
                    letterSpacing: 0.2,
                    fontWeight: 600,
                    marginBottom: 7
                },
                ' span': {
                    position: 'absolute',
                    right: 0,
                    bottom: -5,
                    fontSize: 16,
                    fontWeight: 500,
                    letterSpacing: 0.2
                },
                '&.neutral': {
                    ' >div>div': {
                        backgroundColor: '#95bbd4'
                    },
                    ' >span': {
                        color: '#95bbd4'
                    }
                },
                '&.positive': {
                    ' >div>div': {
                        backgroundColor: '#89cf42'
                    },
                    ' >span': {
                        color: '#89cf42'
                    }
                },
                '&.negative': {
                    ' >div>div': {
                        backgroundColor: '#e18181'
                    },
                    ' >span': {
                        color: '#e18181'
                    }
                }
            },
            '&__bar': {
                width: 270,
                height: 5,
                position: 'relative',
                backgroundColor: '#ededed',
                ' >div': {
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    height: 5
                }
            }
        }
    });
};

export default styles;