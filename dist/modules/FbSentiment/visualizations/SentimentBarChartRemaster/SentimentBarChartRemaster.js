import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import NoDataModule from '../../../NoDataModule';
import DialogIcon from '../../../../helpers/icons/ceo/dialogIcon';
import { numberWithCommas } from '../../../../helpers/number';
import styles from './BarChartRemasterStyles';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    noDataYet: PropTypes.func
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    noDataYet: null
};

var SentimentBarChartRemaster = function SentimentBarChartRemaster(_ref) {
    var moduleData = _ref.moduleData,
        moduleConfig = _ref.moduleConfig,
        moduleColor = _ref.moduleColor,
        noDataYet = _ref.noDataYet;

    var refExport = useRef(null);
    var getPercent = function getPercent(n) {
        return (n * 100 / moduleData.comments_total).toFixed(1).replace(/\.0$/, '');
    };

    var _moduleData$comments_ = moduleData.comments_sentiment,
        positivo = _moduleData$comments_.positivo,
        negativo = _moduleData$comments_.negativo,
        neutral = _moduleData$comments_.neutral;


    var renderContent = function renderContent() {
        return React.createElement(
            'div',
            { className: 'bar-chart__header' },
            React.createElement(
                'div',
                { className: 'padding' },
                React.createElement(
                    'div',
                    null,
                    React.createElement(
                        'p',
                        null,
                        'Positivo'
                    ),
                    React.createElement(
                        'div',
                        { className: 'bar-chart__porcentage' },
                        React.createElement(
                            'div',
                            { className: 'bar__chart__chart' },
                            React.createElement('div', {
                                style: { width: Math.trunc(getPercent(positivo)) + '%' },
                                className: 'bar__chart__chart__positive'
                            })
                        ),
                        React.createElement(
                            'div',
                            { className: 'bar-chart__percentages' },
                            React.createElement(
                                'p',
                                { className: 'bar-chart__percentages__positive' },
                                getPercent(positivo) + '%'
                            )
                        )
                    )
                )
            ),
            React.createElement(
                'div',
                { className: 'padding' },
                React.createElement(
                    'div',
                    { className: 'neutral' },
                    React.createElement(
                        'p',
                        null,
                        'Neutral'
                    ),
                    React.createElement(
                        'div',
                        { className: 'bar-chart__porcentage' },
                        React.createElement(
                            'div',
                            { className: 'bar__chart__chart' },
                            React.createElement('div', {
                                style: { width: Math.trunc(getPercent(neutral)) + '%' },
                                className: 'bar__chart__chart__neutral'
                            })
                        ),
                        React.createElement(
                            'div',
                            { className: 'bar-chart__percentages' },
                            React.createElement(
                                'p',
                                { className: 'bar-chart__percentages__neutral' },
                                getPercent(neutral) + '%'
                            )
                        )
                    )
                )
            ),
            React.createElement(
                'div',
                { className: 'padding' },
                React.createElement(
                    'div',
                    null,
                    React.createElement(
                        'p',
                        null,
                        'Negativo'
                    ),
                    React.createElement(
                        'div',
                        { className: 'bar-chart__porcentage' },
                        React.createElement(
                            'div',
                            { className: 'bar__chart__chart' },
                            React.createElement('div', {
                                style: { width: Math.trunc(getPercent(negativo)) + '%' },
                                className: 'bar__chart__chart__negative'
                            })
                        ),
                        React.createElement(
                            'div',
                            { className: 'bar-chart__percentages' },
                            React.createElement(
                                'p',
                                { className: 'bar-chart__percentages__negative' },
                                getPercent(negativo) + '%'
                            )
                        )
                    )
                )
            )
        );
    };
    return React.createElement(
        'div',
        Object.assign({ ref: refExport }, styles(moduleColor)),
        React.createElement(
            'div',
            { className: 'bar-chart__label' },
            React.createElement(
                'h3',
                null,
                moduleConfig.title
            )
        ),
        function () {
            if (noDataYet !== null) {
                return noDataYet();
            }
            return moduleData.comments_total !== 0 ? React.createElement(
                'div',
                { className: 'bar-chart__content' },
                React.createElement(
                    'div',
                    { className: 'bar-chart__total-mentions' },
                    React.createElement(
                        'div',
                        { className: 'bar-chart__total-mentions--header' },
                        React.createElement(DialogIcon, { color: moduleColor }),
                        React.createElement(
                            'div',
                            { className: 'bar-chart__total-mentions--h' },
                            numberWithCommas(moduleData.comments_total)
                        )
                    ),
                    React.createElement(
                        'p',
                        null,
                        ' comentarios totales '
                    )
                ),
                React.createElement(
                    'div',
                    null,
                    renderContent()
                )
            ) : React.createElement(NoDataModule, null);
        }()
    );
};

SentimentBarChartRemaster.propTypes = propTypes;
SentimentBarChartRemaster.defaultProps = defaultProps;

export default SentimentBarChartRemaster;