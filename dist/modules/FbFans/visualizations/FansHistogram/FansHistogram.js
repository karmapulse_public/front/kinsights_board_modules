import React from 'react';
import PropTypes from 'prop-types';
import NoDataModule from '../../../NoDataModule';

import styles from './styles';
import { numberWithCommas } from '../../../../helpers/number';
import ChartColumns from '../../../Charts/ChartColumns/ChartColumns';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666'
};

var FansHistogram = function FansHistogram(_ref) {
    var moduleColor = _ref.moduleColor,
        moduleData = _ref.moduleData,
        moduleConfig = _ref.moduleConfig;

    var dataKeys = [{
        key: 'total',
        color: '#6587bc'
    }];
    var nameArray = ['Seguidores'];
    var typeClass = moduleData.total_percent > 0 ? 'positive' : 'negative';
    return React.createElement(
        'div',
        Object.assign({ className: 'fans-histogram' }, styles(moduleColor)),
        React.createElement(
            'div',
            { className: 'fans-histogram__label' },
            React.createElement(
                'h3',
                null,
                moduleConfig.title
            )
        ),
        moduleData.data.length > 0 ? React.createElement(
            'div',
            { className: 'fans-histogram__content' },
            React.createElement(
                'div',
                null,
                React.createElement(
                    'div',
                    { className: 'fans-histogram__info' },
                    React.createElement(
                        'h4',
                        null,
                        'Seguidores totales:',
                        React.createElement(
                            'span',
                            null,
                            numberWithCommas(moduleData.total)
                        )
                    ),
                    React.createElement(
                        'h4',
                        null,
                        'Crecimiento en periodo actual:',
                        React.createElement(
                            'span',
                            { className: 'fans-histogram__percent--' + typeClass },
                            moduleData.total_percent.toFixed(2).replace(/\.0$/, '') || 0,
                            ' %'
                        )
                    )
                ),
                React.createElement(ChartColumns, {
                    dataKeys: dataKeys,
                    data: moduleData.data,
                    nameArray: nameArray
                })
            )
        ) : React.createElement(NoDataModule, { color: moduleColor })
    );
};

FansHistogram.propTypes = propTypes;
FansHistogram.defaultProps = defaultProps;

export default FansHistogram;