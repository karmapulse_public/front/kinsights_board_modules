import { css } from 'glamor';

var styles = function styles(color) {
    return css({
        width: '100%',
        minWidth: '320px',
        maxWidth: '500px',

        ' .tooltip_custom': {
            backgroundColor: 'rgba(255, 255, 255, .9)',
            boxShadow: '2px 2px 10px rgba(0, 0, 0, .2)',
            fontSize: 12,
            padding: 10,
            borderRadius: 2,
            letterSpacing: '1px',
            lineHeight: '20px'
        },
        ' .fans-histogram': {

            ' &__label': {
                display: 'flex',
                justifyContent: 'space-between',
                padding: '15px 20px 15px 20px',
                borderTop: '1px solid #dfdfdf',
                color: color,

                ' >h3': {
                    fontSize: '13px',
                    fontWeight: 600
                },
                ' >h4': {
                    fontSize: '13px',
                    fontWeight: 600,
                    opacity: '0.5'
                }
            },
            ' &__info': {
                padding: 20,
                ' >h4': {
                    fontSize: 13,
                    fontWeight: 600,
                    color: 'rgba(0,0,0,.5)',
                    '&:first-child': {
                        marginBottom: 15
                    },
                    ' span': {
                        textAlign: 'right',
                        color: '#000',
                        width: 80,
                        float: 'right',
                        fontSize: 16
                    }
                }
            },
            ' &__percent': {
                '&--positive': {
                    color: '#89cf42 !important',
                    ':before': {
                        content: '"+"'
                    }
                },
                '&--negative': {
                    color: '#e18181 !important'
                }
            },
            ' &__content': {
                padding: '15px 20px 15px 20px',

                ' .recharts-cartesian-grid-horizontal line': {
                    stroke: ' rgba(204,204,204,0.2)'
                }
            }
        }
    });
};

export default styles;