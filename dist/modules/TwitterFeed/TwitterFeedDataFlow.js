import { Observable } from 'rxjs';
import { fetchXHR } from '../../helpers/http';

export default (function (url, params) {
    return Observable.create(function (observer) {
        Observable.fromPromise(fetchXHR(url, 'GET', params)).subscribe(function (data) {
            observer.next({
                tweets: data.json.response.data,
                page: 0,
                scrollActive: true
            });
        }, function (error) {
            observer.error(error);
        });
    });
});