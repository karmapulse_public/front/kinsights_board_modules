var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import takeRight from 'lodash/takeRight';
import findIndex from 'lodash/findIndex';
import isEqual from 'lodash/isEqual';
import isEmpty from 'lodash/isEmpty';

import setDates from '../../helpers/setDates';
import fetchTwitterFeed from './TwitterFeedDataFlow';

import LoadingModule from '../LoadingModule';
import FeedTwitter from './visualizations/FeedTwitter';

var propTypes = {
    side: PropTypes.string,
    color: PropTypes.string,
    fields: PropTypes.object,
    dateRange: PropTypes.object,
    services: PropTypes.object,
    miniMode: PropTypes.bool,
    viewLabels: PropTypes.string,
    filters: PropTypes.object,
    withCache: PropTypes.bool,
    type: PropTypes.string
};

var defaultProps = {
    side: 'left',
    color: '#666',
    fields: {},
    dateRange: {},
    services: {},
    viewLabels: '',
    miniMode: false,
    filters: {},
    withCache: true,
    type: 'search'
};

var spliceNewData = function spliceNewData(newData, oldData) {
    if (isEmpty(oldData)) return newData.length;
    var idLastOld = oldData[0]._source.id;
    var indexDiff = findIndex(newData, function (elto) {
        return elto._source.id === idLastOld;
    });
    if (indexDiff === -1) {
        return newData.length;
    }
    return indexDiff;
};

var TwitterFeed = function (_Component) {
    _inherits(TwitterFeed, _Component);

    function TwitterFeed(props) {
        _classCallCheck(this, TwitterFeed);

        var _this = _possibleConstructorReturn(this, (TwitterFeed.__proto__ || Object.getPrototypeOf(TwitterFeed)).call(this, props));

        _this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color,
            miniMode: props.miniMode,
            filters: props.filters,
            newTweets: []
        };

        _this.scrollHandler = _this.infiniteScrollHandler.bind(_this);
        _this.dropNewTweets = _this.dropNewTweets.bind(_this);
        return _this;
    }

    _createClass(TwitterFeed, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var _this2 = this;

            var _props = this.props,
                dateRange = _props.dateRange,
                withCache = _props.withCache;

            var params = Object.assign({
                recipe_id: 'module_tw_feed',
                rule_id: this.state.moduleConfig.search_id,
                is_curated: false
            }, setDates(dateRange, withCache), {
                filters: Object.assign({}, this.props.filters)
            });

            fetchTwitterFeed(this.props.services.twitter, params).subscribe(function (data) {
                return _this2.setState({
                    moduleData: Object.assign({}, data, {
                        scrollActive: true,
                        page: 0
                    }),
                    moduleLoading: false
                });
            }, function (error) {
                console.log(error);
            }, null);
        }
    }, {
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProps) {
            var _this3 = this;

            var filters = nextProps.filters;
            var _state = this.state,
                moduleData = _state.moduleData,
                newTweets = _state.newTweets;
            var dateRange = nextProps.dateRange;


            var params = Object.assign({
                recipe_id: 'module_tw_feed',
                rule_id: this.state.moduleConfig.search_id,
                is_curated: false
            }, setDates(dateRange, this.props.withCache), {
                filters: Object.assign({}, nextProps.filters)
            });

            if (!isEqual(this.props.filters, nextProps.filters) || nextProps.type === 'search') {
                fetchTwitterFeed(this.props.services.twitter, params, 0).subscribe(function (data) {
                    _this3.setState({
                        filters: filters,
                        moduleData: {
                            tweets: data.tweets,
                            page: 0,
                            scrollActive: true
                        },
                        moduleLoading: false,
                        newTweets: []
                    });
                }, function (error) {
                    console.log(error);
                }, null);
            } else if (this.props.dateRange !== nextProps.dateRange) {
                fetchTwitterFeed(this.props.services.twitter, params, 0).subscribe(function (data) {
                    var oldTweets = moduleData ? moduleData.tweets : [];
                    var currentData = newTweets.length ? newTweets : oldTweets;
                    var indexDiff = spliceNewData(data.tweets, currentData);
                    _this3.setState({
                        newTweets: data.tweets.slice(0, indexDiff).concat(newTweets)
                    });
                }, function (error) {
                    console.log(error);
                }, null);
            } else {
                this.setState({
                    moduleColor: nextProps.color
                });
            }
        }
    }, {
        key: 'infiniteScrollHandler',
        value: function infiniteScrollHandler(page) {
            var _this4 = this;

            var lastTweet = takeRight(this.state.moduleData.tweets);
            var _props2 = this.props,
                dateRange = _props2.dateRange,
                withCache = _props2.withCache;


            var params = Object.assign({
                recipe_id: 'module_tw_feed',
                rule_id: this.state.moduleConfig.search_id,
                is_curated: false
            }, setDates(dateRange, withCache), {
                search_after: lastTweet[0].sort,
                filters: Object.assign({}, this.state.filters)
            });

            fetchTwitterFeed(this.props.services.twitter, params, page).subscribe(function (data) {
                var newTweets = _this4.state.moduleData.tweets.concat(data.tweets);
                _this4.setState({
                    moduleData: {
                        tweets: newTweets,
                        page: page,
                        scrollActive: true
                    },
                    moduleLoading: false
                });
            }, function (error) {
                console.log(error);
            }, null);
        }
    }, {
        key: 'dropNewTweets',
        value: function dropNewTweets() {
            this.setState({
                moduleData: {
                    tweets: this.state.newTweets.concat(this.state.moduleData.tweets),
                    page: 0,
                    scrollActive: true
                },
                newTweets: []
            });
        }
    }, {
        key: 'renderByViz',
        value: function renderByViz() {
            var _this5 = this;

            var _state2 = this.state,
                moduleConfig = _state2.moduleConfig,
                moduleData = _state2.moduleData;


            if (moduleData) {
                var visualizations = {
                    list_tweets: function list_tweets(params) {
                        return React.createElement(FeedTwitter, Object.assign({}, params, {
                            scroll: _this5.scrollHandler,
                            viewLabels: _this5.props.viewLabels,
                            dropNewTweets: _this5.dropNewTweets
                        }));
                    }
                };
                return visualizations[moduleConfig.visualization](this.state);
            }

            return React.createElement(LoadingModule, null);
        }
    }, {
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'm-twitter-feed' },
                this.renderByViz()
            );
        }
    }]);

    return TwitterFeed;
}(Component);

TwitterFeed.propTypes = propTypes;
TwitterFeed.defaultProps = defaultProps;

export default TwitterFeed;