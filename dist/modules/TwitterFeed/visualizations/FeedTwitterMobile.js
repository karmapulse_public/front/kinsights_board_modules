var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import { Observable } from 'rxjs';

import TwitterCardMobile from './customComponents/TwitterCardMobile';
import Gallery from './customComponents/TwitterGallery';
import styles from './FeedTwitterStyles';

var propTypes = {
    scroll: PropTypes.func,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    miniMode: PropTypes.bool
};

var defaultProps = {
    scroll: 'hey',
    moduleData: {},
    moduleColor: '#666',
    miniMode: false
};

var FeedTwitterMobile = function (_Component) {
    _inherits(FeedTwitterMobile, _Component);

    function FeedTwitterMobile(props) {
        _classCallCheck(this, FeedTwitterMobile);

        var _this = _possibleConstructorReturn(this, (FeedTwitterMobile.__proto__ || Object.getPrototypeOf(FeedTwitterMobile)).call(this, props));

        _this.state = {
            tweets: [],
            scrollActive: true
        };
        return _this;
    }

    _createClass(FeedTwitterMobile, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var _this2 = this;

            var feed = ReactDOM.findDOMNode(this);
            var feedTweets = feed.getElementsByClassName('feed-container-list')[0];
            var isUserScrollingDown = function isUserScrollingDown(positions) {
                return positions[0].sT < positions[1].sT;
            };

            var isScrollExpectedPercent = function isScrollExpectedPercent(position, percent) {
                return (position.sT + position.cH) / position.sH > percent / 100;
            };

            Observable.fromEvent(feedTweets, 'scroll').map(function (event) {
                return {
                    sH: event.target.scrollHeight,
                    sT: event.target.scrollTop,
                    cH: event.target.clientHeight
                };
            }).pairwise().filter(function (positions) {
                return isUserScrollingDown(positions) && isScrollExpectedPercent(positions[1], 95) && _this2.state.scrollActive;
            }).debounceTime(100).subscribe(function () {
                _this2.props.scroll(_this2.props.moduleData.page + 18);

                _this2.setState({
                    scrollActive: false
                });
            }, function (error) {
                return console.log(error);
            }, null);
        }
    }, {
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProps) {
            this.setState({
                scrollActive: nextProps.moduleData.scrollActive
            });
        }
    }, {
        key: 'renderTweet',
        value: function renderTweet(tweet, index) {
            return React.createElement(
                TwitterCardMobile,
                Object.assign({}, tweet, { key: index }),
                React.createElement(Gallery, { items: tweet._source.images })
            );
        }
    }, {
        key: 'render',
        value: function render() {
            var _this3 = this;

            var _props = this.props,
                moduleData = _props.moduleData,
                moduleColor = _props.moduleColor,
                miniMode = _props.miniMode;
            var tweets = moduleData.tweets;

            return React.createElement(
                'div',
                Object.assign({ className: 'feed-twitter-mobile' }, styles(moduleColor)),
                React.createElement(
                    'div',
                    { className: 'feed-container-list' },
                    function () {
                        if (miniMode) {
                            return _this3.renderTweet(tweets[0], 0);
                        }
                        return tweets.map(function (tweet, index) {
                            return _this3.renderTweet(tweet, index);
                        });
                    }()
                )
            );
        }
    }]);

    return FeedTwitterMobile;
}(Component);

FeedTwitterMobile.propTypes = propTypes;
FeedTwitterMobile.defaultProps = defaultProps;

export default FeedTwitterMobile;