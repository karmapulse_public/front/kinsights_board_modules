import { css } from 'glamor';

var styles = function styles() {
    return css({
        ' .twitter-card': {
            backgroundColor: '#ffffff',
            boxShadow: '0 1px 8px 0 rgba(0, 0, 0, 0.2)',
            width: '100%',
            maxWidth: 590,
            minWidth: 300,
            margin: '0px auto 5px',
            position: 'relative',
            fontSize: 10,

            '&__user-image': {
                position: 'absolute',
                top: 20,
                left: 20,
                width: 48,
                height: 48,
                borderRadius: '2px',
                ' img': {
                    width: '100%'
                }
            },

            '&__container': {
                padding: 20,
                width: '100%',
                boxSizing: 'border-box'
            }
        },
        ' .container': {

            '&__header': {
                height: 48,
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'flex-start',
                marginBottom: 10,
                paddingLeft: 58,

                ' > div': {

                    ' >a': {
                        display: 'block',
                        ' >svg': {
                            marginLeft: 5,
                            verticalAlign: 'middle'
                        }
                    }
                },
                '&__name': {
                    fontSize: '1.4em',
                    fontWeight: 500,
                    color: '#000',
                    textDecoration: 'none'
                },

                '&__username': {
                    marginTop: 3,
                    fontSize: '1.2em',
                    fontWeight: 500,
                    color: '#000',
                    opacity: 0.54,
                    textDecoration: 'none'
                },

                '&__datetime': {
                    fontSize: '1.2em',
                    fontWeight: 500,
                    color: '#000',
                    opacity: 0.54,
                    textDecoration: 'none',
                    '&:nth-of-type(2)': {
                        marginTop: 3,
                        textAlign: 'right'
                    }
                }
            },

            '&__body': {
                fontSize: '1.4em',
                fontWeight: 400,
                color: 'rgba(0, 0, 0, 0.84)',
                lineHeight: 1.29,
                marginBottom: 20,
                wordWrap: 'break-word',

                ' > em': {
                    backgroundColor: '#FFEBB9'
                },
                ' > a': {
                    color: '#5c6bc0',
                    textDecoration: 'none'
                }
            },

            '&__images': {
                width: '100%',
                marginBottom: 10
            },

            '&__footer': {
                display: 'flex',
                alignItems: 'flex-end',
                justifyContent: 'space-between',

                '&__reactions': {
                    display: 'flex',
                    alignItems: 'center',
                    ' >a': {
                        marginRight: 20
                    },

                    ' > div': {
                        marginRight: 15,

                        ' h5': {
                            fontSize: '1.4em',
                            color: '#5c6bc0',
                            marginBottom: 2
                        },
                        ' h4': {
                            fontSize: '1.6em',
                            fontWeight: 500,
                            color: '#000'
                        }
                    }
                },

                '&__sentiment': {
                    display: 'flex',
                    alignItems: 'center',

                    ' h5': {
                        fontSize: '1.2em',
                        fontWeight: 500
                    },

                    '&__oval': {
                        width: 10,
                        height: 10,
                        marginRight: 5,
                        borderRadius: '50%'
                    },

                    ' .oval-Positivo': {
                        backgroundColor: '#4caf50'
                    },

                    ' .oval-Neutral': {
                        backgroundColor: '#607d8b'
                    },

                    ' .oval-Negativo': {
                        backgroundColor: '#f44336'
                    },

                    ' .text-Positivo': {
                        color: '#4caf50'
                    },

                    ' .text-Neutral': {
                        color: '#607d8b'
                    },

                    ' .text-Negativo': {
                        color: '#f44336'
                    }
                }
            }
        }
    });
};

export default styles;