function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import { css } from 'glamor';

var styles = function styles(n) {
    var s = [];
    var collect = {};

    for (var i = 1; i <= n; i += 1) {
        var figure = _defineProperty({}, ' figure:nth-child(' + i + ')', {
            width: n === 1 ? '100%' : i === 1 ? 100 - 100 / n + '%' : 100 / n + '%',
            height: i === 1 ? '100%' : 100 / (n - 1) + '%',
            boxSizing: 'border-box',
            float: i === 1 ? 'left' : 'right',
            backgroundSize: 'cover',
            backgroundColor: '#eee',
            backgroundPosition: 'center',
            border: '1px solid #ffffff'
        });

        s = s.concat(figure);
    }
    s.map(function (e, i) {
        collect[' figure:nth-child(' + (i + 1) + ')'] = e[' figure:nth-child(' + (i + 1) + ')'];
    });

    return css(collect, {
        width: '100%',
        height: 378,
        marginBottom: 15,
        overflowY: 'hidden'
    });
};
export default styles;