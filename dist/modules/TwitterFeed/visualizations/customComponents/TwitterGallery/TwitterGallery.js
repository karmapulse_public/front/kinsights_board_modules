import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';

import styles from './TwitterGalleryStyles';
// import imageDefault from '../../../../../helpers/icons/imageDeleted';

var propTypes = {
    items: PropTypes.array
};

var defaultProps = {
    items: []
};

var TwitterGallery = function TwitterGallery(props) {
    var images = function images() {
        return props.items.map(function (e, i) {
            return React.createElement('figure', {
                key: i,
                style: { backgroundImage: 'url(' + (e.url ? e.url.replace('http:', 'https:') : '') + ')' }
            });
        });
    };

    var galeryItems = function galeryItems() {
        if (!isEmpty(props.items)) {
            var n = props.items.length;
            return React.createElement(
                'div',
                styles(n),
                images()
            );
        }
        return '';
    };

    return React.createElement(
        'div',
        null,
        galeryItems()
    );
};

TwitterGallery.propTypes = propTypes;
TwitterGallery.defaultProps = defaultProps;

export default TwitterGallery;