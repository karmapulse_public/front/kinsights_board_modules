var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';
import { injectIntl } from 'react-intl';
import isEqual from 'lodash/isEqual';
import iconsExploration from 'helpers/icons/dataExploration';
import UserIcon from 'helpers/icons/dataExploration/userIcon';

var propTypes = {
    childTweet: PropTypes.object,
    type: PropTypes.string,
    intl: PropTypes.object
};

var defaultProps = {
    childTweet: {},
    type: ''
};

var Retweet = function (_Component) {
    _inherits(Retweet, _Component);

    function Retweet(props) {
        _classCallCheck(this, Retweet);

        var _this = _possibleConstructorReturn(this, (Retweet.__proto__ || Object.getPrototypeOf(Retweet)).call(this, props));

        _this.handleNoImage = _this.handleNoImage.bind(_this);
        return _this;
    }

    _createClass(Retweet, [{
        key: 'handleNoImage',
        value: function handleNoImage() {
            var Image = this.imgContainerRetweet.getElementsByClassName('profilephotoRetweet');
            this.imgContainerRetweet.removeChild(Image[0]);
            ReactDOM.render(iconsExploration.noUser(), this.imgContainerRetweet);
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var _props = this.props,
                childTweet = _props.childTweet,
                type = _props.type;


            if (isEqual(type, 'retweet')) {
                var tweetRender = function tweetRender() {
                    return {
                        username: { __html: '@' + childTweet.user.username }
                    };
                };
                return React.createElement(
                    'div',
                    { className: 'twitter-card__retweets' },
                    iconsExploration.retweetIcon(),
                    React.createElement(
                        'a',
                        {
                            className: 'twitter-card__retweets__user-image',
                            href: childTweet.user.link,
                            target: '_blank',
                            rel: 'noopener noreferrer',
                            ref: function ref(c) {
                                _this2.imgContainerRetweet = c;
                            }
                        },
                        React.createElement('img', {
                            src: childTweet.user.image,
                            alt: this.props.intl.formatMessage({ id: 'DataExploration.AltPhotoUserRTweet' }),
                            className: 'profilephotoRetweet',
                            onError: this.handleNoImage
                        })
                    ),
                    React.createElement(
                        'a',
                        {
                            href: childTweet.user.link,
                            target: '_blank',
                            rel: 'noopener noreferrer'
                        },
                        React.createElement('span', { dangerouslySetInnerHTML: tweetRender().username })
                    ),
                    React.createElement(
                        'p',
                        null,
                        'retwitte\xF3'
                    ),
                    React.createElement(
                        'div',
                        { className: 'twitter-card__retweets__counters' },
                        React.createElement(
                            'div',
                            { className: 'twitter-card__retweets__counters__followers' },
                            React.createElement(
                                'h5',
                                null,
                                React.createElement(UserIcon, { color: '#000' }),
                                childTweet.user.followers
                            )
                        )
                    ),
                    React.createElement('div', { className: 'twitter-card__retweets__line' })
                );
            }
            return React.createElement('div', null);
        }
    }]);

    return Retweet;
}(Component);

Retweet.propTypes = propTypes;
Retweet.defaultProps = defaultProps;

export default injectIntl(Retweet);