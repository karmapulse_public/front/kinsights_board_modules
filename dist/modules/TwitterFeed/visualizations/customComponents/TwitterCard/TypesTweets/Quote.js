import React from 'react';
import PropTypes from 'prop-types';
import isEqual from 'lodash/isEqual';
import Gallery from '../../TwitterGallery';

var propTypes = {
    childTweet: PropTypes.object,
    type: PropTypes.string
};

var defaultProps = {
    childTweet: {},
    type: ''
};
var Quote = function Quote(_ref) {
    var childTweet = _ref.childTweet,
        type = _ref.type;

    if (isEqual(type, 'quote')) {
        var tweetRender = function tweetRender() {
            return {
                body: { __html: childTweet.body },
                name: { __html: childTweet.user.name },
                username: { __html: '@' + childTweet.user.username }
            };
        };
        return React.createElement(
            'div',
            { className: 'twitter-card__quote' },
            function () {
                if (!childTweet.images) {
                    return React.createElement('div', null);
                }
                return React.createElement(
                    'div',
                    { className: 'container__media' },
                    React.createElement(Gallery, {
                        items: childTweet.images, link: childTweet.link
                    })
                );
            }(),
            React.createElement(
                'div',
                null,
                React.createElement(
                    'div',
                    { className: 'container__header' },
                    React.createElement(
                        'div',
                        null,
                        React.createElement(
                            'a',
                            {
                                className: 'container__header__name',
                                href: childTweet.user.link,
                                target: '_blank',
                                rel: 'noopener noreferrer'
                            },
                            React.createElement('span', { dangerouslySetInnerHTML: tweetRender().name })
                        ),
                        React.createElement(
                            'a',
                            {
                                className: 'container__header__username',
                                href: childTweet.user.link,
                                target: '_blank',
                                rel: 'noopener noreferrer'
                            },
                            React.createElement('span', { dangerouslySetInnerHTML: tweetRender().username })
                        )
                    )
                ),
                React.createElement(
                    'div',
                    { className: 'container__body' },
                    React.createElement('div', { dangerouslySetInnerHTML: tweetRender().body })
                )
            )
        );
    }
    return React.createElement('div', null);
};

Quote.propTypes = propTypes;
Quote.defaultProps = defaultProps;

export default Quote;