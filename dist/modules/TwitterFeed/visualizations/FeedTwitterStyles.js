import { css } from 'glamor';

var styles = function styles(color) {
    return css({
        backgroundColor: 'transparent',
        ' .feed-label': {
            position: 'relative',
            maxWidth: 590,
            backgroundColor: color === undefined ? 'transparent' : color,
            margin: '0 auto 10px',
            padding: 20,
            ' >h1': {
                color: '#fff',
                ' svg': {
                    marginRight: 10
                }
            },
            '@media screen and (min-width:1280px)': {
                maxWidth: 590
            }
        },

        ' .feed-container-list': {
            position: 'relative',
            maxWidth: 590,
            width: '100%',
            height: 'calc(100vh - 160px)',
            overflowY: 'auto',
            columnCount: 'initial !important',
            WebkitOverflowScrolling: 'touch',
            margin: '0 auto',
            padding: '5px 0px',
            color: 'rgba(0,0,0, 0.5)',
            ' >div': {
                maxWidth: 590,
                margin: '0 auto'
            },
            '@media screen and (min-width:1280px)': {
                maxWidth: 1200,
                columnCount: 2,
                '>div': {
                    breakInside: 'avoid'
                }
            }
        },
        ' .feed-new-data': {
            position: 'absolute',
            top: '99%',
            left: '0px',
            zIndex: 10,
            width: '100%',
            cursor: 'pointer'
        },

        '&.feed-twitter-mobile .container__body > a': {
            color: color ? color : '#5c6bc0 !important',
            fontWeight: '700 !important'
        }
    });
};

export default styles;