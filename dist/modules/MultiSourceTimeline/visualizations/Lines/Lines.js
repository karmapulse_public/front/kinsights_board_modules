import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Label, ResponsiveContainer } from 'recharts';
import styles from './styles';
import NoDataModule from '../../../NoDataModule';
import AxisX from '../../../Charts/CustomComponents/AxisX';
import AxisY from '../../../Charts/CustomComponents/AxisY';
import TooltipCustom from '../../../Charts/CustomComponents/TooltipCustom';
import { numberWithCommas } from '../../../../helpers/number';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    viewLabels: PropTypes.string,
    moduleColor: PropTypes.string,
    intervalType: PropTypes.string,
    colors: PropTypes.object
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    viewLabels: '',
    intervalType: 'day',
    colors: {
        twitter: '#00acee',
        facebook: '#3b5998'
        // instagram: '#bc2a8d',
    }
};

var TimeLine = function TimeLine(props) {
    var moduleConfig = props.moduleConfig,
        moduleData = props.moduleData,
        moduleColor = props.moduleColor,
        colors = props.colors,
        intervalType = props.intervalType;
    var _moduleConfig$title = moduleConfig.title,
        title = _moduleConfig$title === undefined ? 'SENTIMIENTO EN EL TIEMPO' : _moduleConfig$title;

    var dataKeys = [{
        key: 'twitter',
        color: '#00acee'
    }, {
        key: 'facebook',
        color: '#3b5998'
    }];
    var nameArray = ['twitter', 'facebook']; //, 'instagram'];
    var labels = moduleConfig.labelTitles || { x: 'Periodo', y: 'Volumen de datos' };

    var renderTooltip = function renderTooltip(innerProps) {
        return React.createElement(TooltipCustom, Object.assign({}, innerProps, {
            noUtc: true
        }));
    };

    var renderLines = function renderLines() {
        return React.createElement(
            ResponsiveContainer,
            null,
            React.createElement(
                LineChart,
                {
                    data: moduleData.data,
                    margin: {
                        top: 30, right: 30, left: 0, bottom: 25
                    }
                },
                React.createElement(
                    XAxis,
                    {
                        dataKey: 'key',
                        tickLine: false,
                        stroke: 'rgba(230,236,240, 0.15)',
                        tick: React.createElement(AxisX, {
                            style: { fontSize: 10, fontWeight: 600, stroke: 'rgba(0, 0, 0, 0.8)', strokeWidth: '0.05px' },
                            color: '#000',
                            intervalType: intervalType,
                            noUtc: true
                        })
                    },
                    React.createElement(Label, { value: labels.x, className: 'line-data__label', offset: -10, position: 'insideBottom' })
                ),
                React.createElement(
                    YAxis,
                    {
                        axisLine: false,
                        tick: React.createElement(AxisY, { color: '#000', style: { fontSize: '0.625em' } })
                    },
                    React.createElement(Label, { value: labels.y, className: 'line-data__label', offset: -15, angle: -90, position: 'left' })
                ),
                React.createElement(CartesianGrid, { vertical: false }),
                React.createElement(Tooltip, { content: renderTooltip, intervalType: intervalType }),
                nameArray.map(function (item) {
                    return React.createElement(Line, {
                        type: 'linear',
                        strokeWidth: 2,
                        key: item,
                        dataKey: item,
                        stroke: colors[item],
                        dot: true,
                        activeDot: { stroke: colors[item], strokeWidth: 0 }
                    });
                })
            )
        );
    };
    return React.createElement(
        'div',
        Object.assign({ className: 'multisrc-timeline-chart' }, styles),
        moduleData.total === 0 ? React.createElement(
            'div',
            { className: 'noData' },
            React.createElement(NoDataModule, { color: moduleColor })
        ) : React.createElement(
            Fragment,
            null,
            React.createElement(
                'div',
                { className: 'multisrc-timeline__chart' },
                renderLines()
            ),
            React.createElement(
                'div',
                { className: 'multisrc-timeline__totals' },
                React.createElement(
                    'div',
                    null,
                    React.createElement(
                        'h5',
                        { style: { color: dataKeys[0].color } },
                        numberWithCommas(moduleData.total_twitter)
                    ),
                    React.createElement(
                        'h6',
                        null,
                        moduleConfig.totalTitles ? moduleConfig.totalTitles.tw : 'Twitter'
                    )
                ),
                React.createElement(
                    'div',
                    null,
                    React.createElement(
                        'h5',
                        { style: { color: dataKeys[1].color } },
                        numberWithCommas(moduleData.total_facebook)
                    ),
                    React.createElement(
                        'h6',
                        null,
                        moduleConfig.totalTitles ? moduleConfig.totalTitles.fb : 'Facebook'
                    )
                )
            )
        )
    );
};

TimeLine.propTypes = propTypes;
TimeLine.defaultProps = defaultProps;

export default TimeLine;