import { css } from 'glamor';

var styles = css({
    ' .multisrc-timeline__chart': {
        width: '100%',
        height: '350px'
    },
    ' .tooltip_custom': {
        backgroundColor: '#FFF',
        boxShadow: '2px 2px 10px rgba(0, 0, 0, .2)',
        fontSize: 12,
        padding: 10,
        borderRadius: 2,
        letterSpacing: '1px',
        lineHeight: '20px'
    },
    ' .line-data__label': {
        fontSize: 12,
        textAlign: 'justify',
        opacity: 0.5,
        color: 'rgb(43, 43, 43)',
        textAnchor: 'middle',
        textTransform: 'uppercase',
        fontWeight: 700
    },
    ' .recharts-default-tooltip': {
        border: 'none',
        borderRadius: 2,
        ' p': {
            fontSize: 12,
            color: '#000',
            opacity: 0.4
        }
    }
});

export default styles;