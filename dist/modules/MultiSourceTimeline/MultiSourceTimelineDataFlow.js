import { Observable } from 'rxjs';
import moment from 'moment';
import paddingDates from '../../helpers/paddingDates';
import { fetchXHR } from '../../helpers/http';

var mapData = function mapData(data) {
    var maxKey = 'twitter';
    Object.keys(data).forEach(function (key) {
        maxKey = data[key].length < data[maxKey].length ? maxKey : key;
    });

    return data[maxKey].map(function (item, index) {
        var values = function () {
            var objectResult = {};
            Object.keys(data).forEach(function (key) {
                var elto = data[key];
                objectResult[key] = elto.length ? elto[index].total : 0;
            });
            return objectResult;
        }();
        return Object.assign({
            key: moment(item.date).format()
        }, values);
    });
};

export default (function (url, params) {
    return Observable.create(function (observer) {
        Observable.fromPromise(fetchXHR(url, 'GET', params)).subscribe(function (data) {
            var dataDura = data.json.response;
            var filters = {
                filters: { interval: 'day' }
            };
            var padDataDura = {
                twitter: paddingDates(dataDura.twitter_timeline, Object.assign({}, params, filters)),
                facebook: paddingDates(dataDura.facebook_timeline, Object.assign({}, params, filters))
                // instagram: paddingDates(dataDura.instagram_timeline, { ...params, ...filters }),
            };
            observer.next({
                data: mapData(padDataDura),
                total_twitter: dataDura.total_twitter,
                total_facebook: dataDura.total_facebook
                // total_instagram: dataDura.total_instagram,
            });
        }, function (error) {
            observer.error(error);
        });
    });
});