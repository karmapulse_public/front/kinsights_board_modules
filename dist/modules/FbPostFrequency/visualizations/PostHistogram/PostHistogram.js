import React from 'react';
import PropTypes from 'prop-types';
import NoDataModule from '../../../NoDataModule';
import { numberWithCommas } from '../../../../helpers/number';

import styles from './styles';
import ChartColumns from '../../../Charts/ChartColumns/ChartColumns';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666'
};

var PostHistogram = function PostHistogram(_ref) {
    var moduleColor = _ref.moduleColor,
        moduleData = _ref.moduleData,
        moduleConfig = _ref.moduleConfig;

    var dataKeys = [{
        key: 'text',
        color: '#6f9cbc'
    }, {
        key: 'link',
        color: '#c4d5cc'
    }, {
        key: 'image',
        color: '#ce9089'
    }, {
        key: 'video',
        color: '#f3ccad'
    }];
    var nameArray = ['Texto', 'Link', 'Imagen', 'Video'];
    return React.createElement(
        'article',
        Object.assign({ className: 'post-histogram' }, styles(moduleColor)),
        React.createElement(
            'div',
            { className: 'post-histogram__label' },
            React.createElement(
                'h3',
                null,
                moduleConfig.title
            )
        ),
        moduleData.total !== 0 ? React.createElement(
            'div',
            { className: 'post-histogram__content' },
            React.createElement(
                'h4',
                null,
                'Publicaciones totales: ',
                React.createElement(
                    'span',
                    null,
                    numberWithCommas(moduleData.total)
                )
            ),
            React.createElement(ChartColumns, {
                dataKeys: dataKeys,
                nameArray: nameArray,
                data: moduleData.data
            })
        ) : React.createElement(NoDataModule, null)
    );
};

PostHistogram.propTypes = propTypes;
PostHistogram.defaultProps = defaultProps;

export default PostHistogram;