import React from 'react';
import PropTypes from 'prop-types';
import NoDataModule from '../../../NoDataModule';
import { numberWithCommas } from '../../../../helpers/number';

import styles from './styles';
import { ConversationIcon } from '../../../../helpers/icons/gob';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    noDataYet: PropTypes.func
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    noDataYet: null
};

var PostTotals = function PostTotals(_ref) {
    var moduleColor = _ref.moduleColor,
        moduleData = _ref.moduleData,
        moduleConfig = _ref.moduleConfig,
        noDataYet = _ref.noDataYet;


    var moduleImage = moduleData.data.reduce(function (prev, item) {
        return prev += item.image;
    }, 0);
    var moduleLink = moduleData.data.reduce(function (prev, item) {
        return prev += item.link;
    }, 0);
    var moduleText = moduleData.data.reduce(function (prev, item) {
        return prev += item.text;
    }, 0);
    var moduleVideo = moduleData.data.reduce(function (prev, item) {
        return prev += item.video;
    }, 0);

    return React.createElement(
        'article',
        Object.assign({ className: 'card-total-numers' }, styles(moduleColor)),
        React.createElement(
            'div',
            { className: 'mentions' },
            React.createElement(
                'div',
                { className: 'mentions__title' },
                React.createElement(
                    'h3',
                    null,
                    moduleConfig.title
                )
            )
        ),
        function () {
            if (noDataYet === null) {
                return moduleData.total !== 0 ? React.createElement(
                    'div',
                    { className: 'mentions__body' },
                    React.createElement(
                        'div',
                        { className: 'mentions__body__total' },
                        React.createElement(
                            'div',
                            null,
                            React.createElement(ConversationIcon, null),
                            React.createElement(
                                'h4',
                                null,
                                numberWithCommas(moduleData.total)
                            )
                        ),
                        React.createElement(
                            'h5',
                            null,
                            'Publicaciones totales'
                        )
                    ),
                    React.createElement(
                        'div',
                        { className: 'mentions__body__numbers' },
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'h4',
                                { className: 'mentions__body__numbers__video' },
                                moduleVideo
                            ),
                            React.createElement(
                                'h5',
                                null,
                                'Video'
                            )
                        ),
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'h4',
                                { className: 'mentions__body__numbers__imagenes' },
                                moduleImage
                            ),
                            React.createElement(
                                'h5',
                                null,
                                'Imagen'
                            )
                        ),
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'h4',
                                { className: 'mentions__body__numbers__texto' },
                                moduleText
                            ),
                            React.createElement(
                                'h5',
                                null,
                                'Texto'
                            )
                        ),
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'h4',
                                { className: 'mentions__body__numbers__link' },
                                moduleLink
                            ),
                            React.createElement(
                                'h5',
                                null,
                                'Link'
                            )
                        )
                    )
                ) : React.createElement(NoDataModule, null);
            }

            return noDataYet();
        }()
    );
};

PostTotals.propTypes = propTypes;
PostTotals.defaultProps = defaultProps;

export default PostTotals;