var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';

import withDrawer from './withDrawer';
import Treemap from '../../Charts/Treemap';
import styles from './treeMapTopTrendsStyles';
import NoDataModule from '../../NoDataModule';
import { numberWithCommas } from '../../../helpers/number';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    onClickSquare: PropTypes.func
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    onClickSquare: function onClickSquare() {
        return {};
    }
};

var TreeMapTopTrends = function (_Component) {
    _inherits(TreeMapTopTrends, _Component);

    function TreeMapTopTrends(props) {
        _classCallCheck(this, TreeMapTopTrends);

        var _this = _possibleConstructorReturn(this, (TreeMapTopTrends.__proto__ || Object.getPrototypeOf(TreeMapTopTrends)).call(this, props));

        _this.state = {
            width: props.moduleConfig.width
        };
        _this.contenedor = null;
        _this.changeWidth = _this.changeWidth.bind(_this);
        return _this;
    }

    _createClass(TreeMapTopTrends, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var _this2 = this;

            window.addEventListener('resize', function () {
                _this2.changeWidth(_this2.contenedor.clientWidth);
            });
            this.changeWidth(this.contenedor.clientWidth);
        }
    }, {
        key: 'changeWidth',
        value: function changeWidth(newWidth) {
            this.setState({
                width: newWidth - 30
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var _this3 = this;

            var _props = this.props,
                moduleColor = _props.moduleColor,
                moduleConfig = _props.moduleConfig,
                moduleData = _props.moduleData,
                onClickSquare = _props.onClickSquare;
            var width = this.state.width;
            var _moduleConfig$title = moduleConfig.title,
                title = _moduleConfig$title === undefined ? 'TOP DE TEMAS' : _moduleConfig$title,
                height = moduleConfig.height,
                buttonExport = moduleConfig.buttonExport;
            var values = moduleData.values;

            var onClick = moduleConfig.hasClick ? onClickSquare : function () {
                return {};
            };
            return React.createElement(
                'div',
                Object.assign({
                    className: 'treemap-trends-sentiment'
                }, styles(moduleColor), {
                    ref: function ref(container) {
                        _this3.contenedor = container;
                    }
                }),
                React.createElement(
                    'div',
                    { className: 'treemap__label' },
                    React.createElement(
                        'h3',
                        null,
                        title
                    )
                ),
                isEmpty(values) ? React.createElement(
                    'div',
                    { className: 'noData' },
                    React.createElement(NoDataModule, { color: moduleColor })
                ) : React.createElement(
                    'div',
                    {
                        className: 'treemap-trends__chart'
                    },
                    React.createElement(
                        'div',
                        { className: 'treemap-trends__body' },
                        React.createElement(
                            'div',
                            null,
                            React.createElement(Treemap, {
                                data: values,
                                dataKey: 'total',
                                enableTooltip: true,
                                fill: '#fff',
                                fontFamily: 'Roboto',
                                fontSize: 12,
                                height: height,
                                stroke: '#fff',
                                width: width,
                                onDataClick: onClick,
                                tooltip: function tooltip(props) {
                                    var payload = props.payload.length >= 1 ? props.payload[0].payload : { name: '', value: '' };
                                    var sentiment = payload.root ? payload.root.name : '';
                                    return React.createElement(
                                        'div',
                                        { className: 'chart__tooltip' },
                                        React.createElement(
                                            'h4',
                                            { className: '' + sentiment },
                                            payload.name
                                        ),
                                        React.createElement(
                                            'h5',
                                            { className: 'toltipText' },
                                            numberWithCommas(parseFloat(payload.total)),
                                            ' menciones'
                                        ),
                                        React.createElement(
                                            'h5',
                                            { className: 'toltipText' },
                                            sentiment
                                        )
                                    );
                                }
                            })
                        )
                    ),
                    React.createElement(
                        'div',
                        { className: 'treemap-trends__footer' },
                        React.createElement(
                            'h5',
                            null,
                            'Positivo'
                        ),
                        React.createElement(
                            'h5',
                            null,
                            'Neutral'
                        ),
                        React.createElement(
                            'h5',
                            null,
                            'Negativo'
                        )
                    )
                )
            );
        }
    }]);

    return TreeMapTopTrends;
}(Component);

TreeMapTopTrends.propTypes = propTypes;
TreeMapTopTrends.defaultProps = defaultProps;

export default withDrawer(TreeMapTopTrends);