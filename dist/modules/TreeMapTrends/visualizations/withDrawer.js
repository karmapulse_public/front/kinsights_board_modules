var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import toLower from 'lodash/toLower';

import DrawerFeed from '../../DrawerFeed';
import DrawerContext from '../../../helpers/context/ctxDrawer';

export default (function (WrappedComponent) {
    var HOCDrawerTreemap = function (_Component) {
        _inherits(HOCDrawerTreemap, _Component);

        function HOCDrawerTreemap(props) {
            _classCallCheck(this, HOCDrawerTreemap);

            var _this = _possibleConstructorReturn(this, (HOCDrawerTreemap.__proto__ || Object.getPrototypeOf(HOCDrawerTreemap)).call(this, props));

            _this.handleClickEvent = _this.handleClickEvent.bind(_this);
            _this.handleCloseDrawer = _this.handleCloseDrawer.bind(_this);
            _this.initialState = {
                openDrawer: false,
                drawerData: {
                    type: 'TWITTER',
                    service: '',
                    title: '',
                    subtitleLeft: '',
                    subtitleRight: '',
                    queryData: {}
                }
            };
            _this.state = _this.initialState;
            return _this;
        }

        _createClass(HOCDrawerTreemap, [{
            key: 'handleCloseDrawer',
            value: function handleCloseDrawer() {
                this.setState(Object.assign({}, this.initialState));
            }
        }, {
            key: 'handleClickEvent',
            value: function handleClickEvent(data) {
                var _props = this.props,
                    moduleRange = _props.moduleRange,
                    moduleConfig = _props.moduleConfig,
                    moduleServices = _props.moduleServices;
                var _moduleConfig$title = moduleConfig.title,
                    title = _moduleConfig$title === undefined ? 'TWEETS EN EL TIEMPO' : _moduleConfig$title;

                var queryData = {
                    'recipe-id': 'module_tw_explore_tweets',
                    'rule-id': moduleConfig.search_id,
                    'initial-date': moment(moduleRange.startDate).format(),
                    'final-date': moment(moduleRange.endDate).format(),
                    filters: {
                        limit: 10,
                        phrase: data.name,
                        sentiment: toLower(data.root.name)
                    }
                };
                this.setState({
                    openDrawer: true,
                    drawerData: {
                        type: 'TWITTER',
                        service: moduleServices.twitter,
                        title: title,
                        subtitleLeft: 'Tweets con el tema "' + data.name + '" con sentimiento ' + data.root.name,
                        subtitleRight: '',
                        queryData: queryData
                    }
                });
            }
        }, {
            key: 'render',
            value: function render() {
                return React.createElement(
                    DrawerContext.Provider,
                    {
                        value: {
                            open: this.state.openDrawer,
                            drawerData: this.state.drawerData,
                            onCloseDrawer: this.handleCloseDrawer
                        }
                    },
                    React.createElement(WrappedComponent, Object.assign({}, this.props, {
                        onClickSquare: this.handleClickEvent
                    })),
                    React.createElement(DrawerFeed, null)
                );
            }
        }]);

        return HOCDrawerTreemap;
    }(Component);

    HOCDrawerTreemap.propTypes = {
        moduleColor: PropTypes.string,
        intervalType: PropTypes.string,
        moduleData: PropTypes.shape({}),
        moduleRange: PropTypes.shape({}),
        moduleConfig: PropTypes.shape({}),
        moduleServices: PropTypes.shape({})
    };

    HOCDrawerTreemap.defaultProps = {
        moduleConfig: {},
        moduleData: {},
        moduleRange: {},
        moduleServices: {},
        moduleColor: '#666',
        intervalType: 'month'
    };

    return HOCDrawerTreemap;
});