var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import isNull from 'lodash/isNull';

import setDates from '../../helpers/setDates';

import fetchTreeMapTrends from './TreeMapTrendsDataFlow';
import LoadingModule from '../LoadingModule';
import TreeMapTopTrends from './visualizations/TreeMapTopTrends';

var propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    dateRange: PropTypes.object,
    services: PropTypes.object,
    size: PropTypes.object,
    filters: PropTypes.object,
    withCache: PropTypes.bool
};

var defaultProps = {
    fields: {},
    side: 'left',
    color: '#666',
    dateRange: {},
    services: {},
    size: {
        width: 600,
        height: 400
    },
    filters: {},
    withCache: true
};

var TreeMapTrends = function (_Component) {
    _inherits(TreeMapTrends, _Component);

    function TreeMapTrends(props) {
        _classCallCheck(this, TreeMapTrends);

        var _this = _possibleConstructorReturn(this, (TreeMapTrends.__proto__ || Object.getPrototypeOf(TreeMapTrends)).call(this, props));

        _this.state = {
            moduleConfig: Object.assign({}, props.fields, props.size),
            moduleServices: props.services,
            moduleRange: props.dateRange,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color
        };
        return _this;
    }

    _createClass(TreeMapTrends, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            this.updateDataFlow(this.props);
        }
    }, {
        key: 'componentDidUpdate',
        value: function componentDidUpdate(prevProps) {
            var _props$dateRange = this.props.dateRange,
                startDate = _props$dateRange.startDate,
                endDate = _props$dateRange.endDate;
            var _props$fields = this.props.fields,
                hasClick = _props$fields.hasClick,
                search_id = _props$fields.search_id;

            if (startDate !== prevProps.dateRange.startDate || endDate !== prevProps.dateRange.endDate || search_id !== prevProps.fields.search_id) {
                this.updateDataFlow(this.props);
            }
            if (hasClick !== prevProps.fields.hasClick) {
                this.changeState(hasClick);
            }
        }
    }, {
        key: 'changeState',
        value: function changeState(hasClick) {
            this.setState({
                moduleConfig: Object.assign({}, this.state.moduleConfig, {
                    hasClick: hasClick
                })
            });
        }
    }, {
        key: 'updateDataFlow',
        value: function updateDataFlow(props) {
            var _this2 = this;

            var fields = props.fields,
                dateRange = props.dateRange,
                filters = props.filters,
                withCache = props.withCache,
                size = props.size;

            var params = Object.assign({
                recipe_id: 'module_tw_tree_map',
                rule_id: fields.search_id
            }, setDates(dateRange, withCache), {
                filters: Object.assign({
                    top: fields.top_limit
                }, filters)
            });

            fetchTreeMapTrends(props.services.twitter, params).subscribe(function (data) {
                _this2.setState({
                    moduleData: data,
                    moduleConfig: Object.assign({}, fields, size),
                    moduleLoading: false,
                    moduleServices: props.services,
                    moduleColor: props.color,
                    moduleSide: props.side,
                    moduleRange: props.dateRange
                });
            }, function (error) {
                console.log(error);
            }, null);
        }
    }, {
        key: 'renderByViz',
        value: function renderByViz() {
            var _state = this.state,
                moduleConfig = _state.moduleConfig,
                moduleData = _state.moduleData;

            if (!isNull(moduleData)) {
                var visualizations = {
                    treemap_top_trends: function treemap_top_trends(params) {
                        return React.createElement(TreeMapTopTrends, params);
                    }
                };
                return visualizations[this.props.view || moduleConfig.visualization](this.state);
            }
            return React.createElement(LoadingModule, null);
        }
    }, {
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'm-treemap-trends' },
                this.renderByViz()
            );
        }
    }]);

    return TreeMapTrends;
}(Component);

TreeMapTrends.propTypes = propTypes;
TreeMapTrends.defaultProps = defaultProps;

export default TreeMapTrends;