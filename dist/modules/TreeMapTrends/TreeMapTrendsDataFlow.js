import { Observable } from 'rxjs';
import isEmpty from 'lodash/isEmpty';
import { fetchXHR } from '../../helpers/http';

export default (function (url, params) {
    return Observable.create(function (observer) {
        Observable.fromPromise(fetchXHR(url, 'GET', params)).subscribe(function (data) {
            var colors = {
                positive: {
                    color: '#63d12f',
                    name: 'Positivo'
                },
                negative: {
                    color: '#ea3c3c',
                    name: 'Negativo'
                },
                neutral: {
                    color: '#89a7ce',
                    name: 'Neutral'
                }
            };
            var response = data.json.response;

            var values = [];
            Object.keys(response).forEach(function (keys) {
                if (!isEmpty(response[keys])) {
                    values.push(Object.assign({
                        children: response[keys]
                    }, colors[keys]));
                }
            });
            observer.next({
                values: values
            });
        }, function (error) {
            observer.error(error);
        });
    });
});