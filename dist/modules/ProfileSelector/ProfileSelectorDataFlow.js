import { Observable } from 'rxjs';
import request from '../../helpers/http';

var defaultParams = {
    index: 'search_tweets'
};

export default (function (url) {
    var paramsData = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : defaultParams;

    var params = Object.assign(defaultParams, paramsData);

    var query = {
        index: params.index,
        es_query: {
            size: 0,
            query: {
                bool: {
                    filter: [{
                        term: {
                            rule_id: params.search_id
                        }
                    }, {
                        term: {
                            'user.username': {
                                value: params.username
                            }
                        }
                    }]
                }
            },
            aggs: {
                totalKlout: {
                    terms: {
                        field: 'user.klout',
                        size: 1
                    }
                }
            }
        }
    };

    return Observable.create(function (observer) {
        var formatData = function formatData(data) {
            return data.aggregations.totalKlout.buckets[0].key;
        };

        Observable.fromPromise(request(url, 'GET', query)).subscribe(function (data) {
            return observer.next(formatData(data.json));
        }, function (error) {
            return observer.error(error);
        }, null);
    });
});