var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import profileSelectorStyles from './profileSelectorStyles';

var propTypes = {
    fields: PropTypes.object,
    columns: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    owner: PropTypes.bool,
    changeProfile: PropTypes.func
};

var defaultProps = {
    fields: {},
    columns: {},
    side: '',
    color: '',
    owner: {},
    changeProfile: function changeProfile() {}
};

var ProfileSelector = function (_Component) {
    _inherits(ProfileSelector, _Component);

    function ProfileSelector(props) {
        _classCallCheck(this, ProfileSelector);

        var _this = _possibleConstructorReturn(this, (ProfileSelector.__proto__ || Object.getPrototypeOf(ProfileSelector)).call(this, props));

        var profiles = [];
        profiles.push(_this.props.fields.owner);
        profiles = profiles.concat(_this.props.fields.rivals);

        _this.state = {
            dropDownStatus: false,
            profiles: profiles,
            columns: props.columns,
            side: props.side,
            color: props.color
            //kloutscore: null
        };

        _this.toggleDropDown = _this.toggleDropDown.bind(_this);
        _this.changeProfile = _this.changeProfile.bind(_this);
        return _this;
    }

    _createClass(ProfileSelector, [{
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProps) {
            this.setState({
                columns: nextProps.columns,
                side: nextProps.side,
                color: nextProps.color
            });
            // }
        }
    }, {
        key: 'changeProfile',
        value: function changeProfile(index) {
            var params = {};

            if (this.props.owner) {
                params.columns = {
                    left: index,
                    right: this.state.columns.right
                };
            } else {
                params.columns = {
                    left: this.state.columns.left,
                    right: index
                };
            }

            this.props.changeProfile(params);

            this.setState({
                dropDownStatus: false
            });
        }
    }, {
        key: 'toggleDropDown',
        value: function toggleDropDown() {
            this.setState({
                dropDownStatus: !this.state.dropDownStatus
            });
        }
    }, {
        key: 'renderDropDown',
        value: function renderDropDown() {
            var _this2 = this;

            var profiles = this.state.profiles.map(function (item, index) {
                if (_this2.state.columns.left === index || _this2.state.columns.right === index) {
                    return Object.assign(item, { used: true });
                }
                return Object.assign(item, { used: false });
            });

            if (this.state.dropDownStatus) {
                return profiles.map(function (item, index) {
                    return React.createElement(
                        'div',
                        {
                            style: item.used ? { display: 'none' } : {},
                            onClick: function onClick() {
                                return _this2.changeProfile(index);
                            },
                            className: 'item',
                            key: index
                        },
                        React.createElement('img', { src: item.fields.profile_picture.fields.file.url ? item.fields.profile_picture.fields.file.url.replace('http:', 'https:') : '', alt: '' }),
                        React.createElement(
                            'div',
                            { className: 'item__info' },
                            React.createElement(
                                'h1',
                                null,
                                item.fields.name
                            ),
                            React.createElement(
                                'h2',
                                null,
                                '@',
                                item.fields.twitter_account
                            ),
                            React.createElement(
                                'h3',
                                null,
                                item.fields.description
                            )
                        )
                    );
                });
            }

            return '';
        }

        // renderKloutscore(search_id) {
        //     if (!isUndefined(search_id)) {
        //         if (this.state.kloutscore !== null) {
        //             return (
        //                 <div className="profile-selector__ks">
        //                     <KsIconOrange />
        //                     <span>{this.state.kloutscore}</span>
        //                 </div>
        //             );
        //         }
        //         return (
        //             <div className="profile-selector__ks">
        //                 <KsIconOrange />
        //                 <span>{this.state.kloutscore}</span>
        //             </div>
        //         );
        //     }

        //     return null;
        // }

    }, {
        key: 'render',
        value: function render() {
            var _state$profiles$state = this.state.profiles[this.state.columns[this.props.side]].fields,
                profile_picture = _state$profiles$state.profile_picture,
                name = _state$profiles$state.name,
                twitter_account = _state$profiles$state.twitter_account,
                description = _state$profiles$state.description;

            return React.createElement(
                'div',
                Object.assign({
                    className: 'c-profile-selector'
                }, profileSelectorStyles(this.state.side, this.state.color)),
                React.createElement('img', { src: profile_picture.fields.file.url ? profile_picture.fields.file.url.replace('http:', 'https:') : '', alt: '' }),
                React.createElement(
                    'div',
                    { className: 'profile-selector__names' },
                    React.createElement(
                        'div',
                        null,
                        React.createElement(
                            'h1',
                            null,
                            React.createElement(
                                'span',
                                null,
                                name
                            ),
                            React.createElement(
                                'span',
                                null,
                                '@',
                                twitter_account
                            )
                        ),
                        React.createElement(
                            'h2',
                            null,
                            description
                        )
                    )
                ),
                React.createElement(
                    'div',
                    {
                        className: 'profile-selector__dropdown',
                        onClick: this.toggleDropDown
                    },
                    React.createElement('div', { className: 'profile-selector__dropdown__triangle' })
                ),
                React.createElement(
                    'div',
                    { className: 'drop-list' },
                    this.renderDropDown()
                )
            );
        }
    }]);

    return ProfileSelector;
}(Component);

ProfileSelector.propTypes = propTypes;
ProfileSelector.defaultProps = defaultProps;

export default ProfileSelector;