import { css } from 'glamor';

var styles = function styles() {
    return css({
        width: '100%',
        minWidth: 300,
        maxWidth: 1042,
        margin: '0 auto',
        padding: '40px 5% 40px 0',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: '#fff',
        ' img': {
            width: 37,
            height: 37
        },
        ' h3': {
            fontSize: 14
        },
        ' h4': {
            fontSize: 11,
            opacity: 0.5
        },
        ' strong': {
            fontSize: 16,
            fontWeight: 'bold'
        },
        ' .cta__first-user': {
            width: '35%',
            padding: '0 20px',
            textAlign: 'center',
            ' img': {
                width: 77,
                height: 77,
                marginBottom: 13
            },
            ' strong': {
                display: 'block',
                marginTop: 20
            }
        },
        ' .cta__list-users': {
            width: '65%',
            height: 320,
            display: 'flex',
            justifyContent: 'space-between',
            alignContent: 'space-between',
            flexWrap: 'wrap',
            flexDirection: 'column',
            counterReset: 'user',
            ' li': {
                width: '42%',
                minWidth: 250,
                display: 'flex',
                alignItems: 'center',
                '&:not(:nth-child(5n))': {
                    marginBottom: 30
                },
                '&:nth-child(-n+5)': {
                    marginRight: 20
                },
                ' img': {
                    marginRight: 10
                },
                ' div': {
                    alignSelf: 'flex-start'
                },
                ' h4': {
                    marginTop: 5
                },
                ' strong': {
                    marginLeft: 'auto'
                },
                '&::before': {
                    counterIncrement: 'user',
                    content: 'counter(user)"."',
                    opacity: 0.5,
                    marginRight: 15,
                    fontSize: 14,
                    fontWeight: 500
                }
            }
        },
        '@media screen and (max-width: 900px)': {
            flexDirection: 'column',
            padding: '40px 5%',
            ' >div, ol': {
                width: '100% !important'
            },
            ' .cta__first-user': {
                marginBottom: 40
            }
        },
        '@media screen and (max-width: 600px)': {
            ' .cta__first-user': {
                display: 'none'
            },
            ' .cta__list-users': {
                height: 'auto',
                ' li': {
                    width: '100%',
                    ' &:not(:last-child)': {
                        marginBottom: '30px !important'
                    }
                }
            }
        }
    });
};

export default styles;