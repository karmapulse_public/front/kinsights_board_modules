var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import clone from 'lodash/clone';
import moment from 'moment-timezone';
import isUndefined from 'lodash/isUndefined';

import fetchTopAudience from './TopAudienceDataFlow';

import LoadingModule from '../LoadingModule';
import CardListAudience from './visualizations/CardListAudience';
import CardTopAudience from './visualizations/CardTopAudience';

var propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    services: PropTypes.object,
    dateRange: PropTypes.object
};

var defaultProps = {
    fields: {},
    side: 'left',
    color: '#666',
    services: {},
    dateRange: {}
};

var TotalAudience = function (_Component) {
    _inherits(TotalAudience, _Component);

    function TotalAudience(props) {
        _classCallCheck(this, TotalAudience);

        var _this = _possibleConstructorReturn(this, (TotalAudience.__proto__ || Object.getPrototypeOf(TotalAudience)).call(this, props));

        _this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color
        };
        return _this;
    }

    _createClass(TotalAudience, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var _this2 = this;

            var _props$dateRange = this.props.dateRange,
                startDate = _props$dateRange.startDate,
                endDate = _props$dateRange.endDate;


            var tz = 'America/Mexico_City';
            var strStartDate = clone(startDate).format('YYYY-MM-DD hh:mm:ss');
            var strEndDate = clone(endDate).format('YYYY-MM-DD hh:mm:ss');

            var utcOffsetStart = moment.tz(strStartDate, tz).isDST() ? '-05:00' : '-06:00';
            var utcOffsetEnd = moment.tz(strEndDate, tz).isDST() ? '-05:00' : '-06:00';

            startDate = startDate.utcOffset(utcOffsetStart).set({
                hour: 0,
                minute: 0,
                second: 0,
                millisecond: 0
            }).toISOString();

            endDate = endDate.utcOffset(utcOffsetEnd).set({
                minute: 5 * Math.floor(moment().minute() / 5),
                second: 0,
                millisecond: 0
            }).toISOString();

            var params = {
                recipe_id: 'module_tw_top_sentiment_audience',
                rule_id: this.state.moduleConfig.search_id,
                initial_date: startDate,
                final_date: endDate
            };

            fetchTopAudience(this.props.services.twitter, params).subscribe(function (data) {
                var formattedData = null;
                if (_this2.state.moduleConfig.visualization === 'card_list_audience') {
                    formattedData = {
                        audiencePositiveList: isUndefined(data.positivo) ? [] : data.positivo,
                        audienceNegativeList: isUndefined(data.negativo) ? [] : data.negativo
                    };
                } else {
                    formattedData = data;
                }
                _this2.setState({
                    moduleData: formattedData,
                    moduleLoading: false
                });
            }, function (error) {
                console.log(error);
            }, null);
        }
    }, {
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProps) {
            var _this3 = this;

            var _nextProps$dateRange = nextProps.dateRange,
                startDate = _nextProps$dateRange.startDate,
                endDate = _nextProps$dateRange.endDate;


            var tz = 'America/Mexico_City';
            var strStartDate = clone(startDate).format('YYYY-MM-DD hh:mm:ss');
            var strEndDate = clone(endDate).format('YYYY-MM-DD hh:mm:ss');

            var utcOffsetStart = moment.tz(strStartDate, tz).isDST() ? '-05:00' : '-06:00';
            var utcOffsetEnd = moment.tz(strEndDate, tz).isDST() ? '-05:00' : '-06:00';

            startDate = startDate.utcOffset(utcOffsetStart).set({
                hour: 0,
                minute: 0,
                second: 0,
                millisecond: 0
            }).toISOString();

            endDate = endDate.utcOffset(utcOffsetEnd).set({
                hour: 23,
                minute: 59,
                second: 59,
                millisecond: 999
            }).toISOString();

            var params = {
                recipe_id: 'module_tw_top_sentiment_audience',
                rule_id: this.state.moduleConfig.search_id,
                initial_date: startDate,
                final_date: endDate
            };

            fetchTopAudience(nextProps.services.twitter, params).subscribe(function (data) {
                var formattedData = null;
                if (nextProps.fields.visualization === 'card_list_audience') {
                    formattedData = {
                        audiencePositiveList: isUndefined(data.positivo) ? [] : data.positivo,
                        audienceNegativeList: isUndefined(data.negativo) ? [] : data.negativo
                    };
                } else {
                    formattedData = data;
                }

                _this3.setState({
                    moduleData: formattedData,
                    moduleLoading: false,
                    moduleSide: nextProps.side,
                    moduleColor: nextProps.color
                });
            }, function (error) {
                console.log(error);
            }, null);
        }
    }, {
        key: 'renderByViz',
        value: function renderByViz() {
            var _state = this.state,
                moduleConfig = _state.moduleConfig,
                moduleData = _state.moduleData;


            if (moduleData) {
                var visualizations = {
                    card_list_audience: function card_list_audience(params) {
                        return React.createElement(CardListAudience, params);
                    },
                    card_top_audience: function card_top_audience(params) {
                        return React.createElement(CardTopAudience, params);
                    }
                };

                return visualizations[this.props.view || moduleConfig.visualization](this.state);
            }

            return React.createElement(LoadingModule, null);
        }
    }, {
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'm-total-audience' },
                this.renderByViz()
            );
        }
    }]);

    return TotalAudience;
}(Component);

TotalAudience.propTypes = propTypes;
TotalAudience.defaultProps = defaultProps;

export default TotalAudience;