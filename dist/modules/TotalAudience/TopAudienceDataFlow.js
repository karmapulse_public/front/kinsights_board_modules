import { Observable } from 'rxjs';
import { fetchXHR } from '../../helpers/http';

export default (function (url, params) {
    return Observable.create(function (observer) {
        Observable.fromPromise(fetchXHR(url, 'GET', params)).subscribe(function (data) {
            var sentiments = data.json.response.aggregations.sentiment.buckets;
            var items = {};

            sentiments.forEach(function (item) {
                items[item.key] = item.top_users.buckets;
            });

            observer.next(items);
        }, function (error) {
            observer.error(error);
        });
    });
});