var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import FeedTweets from './visualizations/FeedTweets';
import FeedMilitants from './visualizations/FeedMilitants';
import DrawerContext from '../../helpers/context/ctxDrawer';
import drawerStyles, { overlayStyles } from './DrawerStyles';

var propTypes = {
    open: PropTypes.bool,
    drawerData: PropTypes.object,
    onCloseDrawer: PropTypes.func
};

var defaultProps = {
    open: false,
    onCloseDrawer: function onCloseDrawer() {
        return {};
    },
    drawerData: {
        type: 'TWITTER',
        title: '',
        subtitleLeft: '',
        subtitleRight: '',
        queryData: {}
    }
};

var DrawerCmp = function (_Component) {
    _inherits(DrawerCmp, _Component);

    function DrawerCmp(props) {
        _classCallCheck(this, DrawerCmp);

        var _this = _possibleConstructorReturn(this, (DrawerCmp.__proto__ || Object.getPrototypeOf(DrawerCmp)).call(this, props));

        _this.handleChangeSubtitle = _this.handleChangeSubtitle.bind(_this);
        var _props$drawerData = props.drawerData,
            subtitleLeft = _props$drawerData.subtitleLeft,
            subtitleRight = _props$drawerData.subtitleRight;

        _this.state = {
            subtitleLeft: subtitleLeft,
            subtitleRight: subtitleRight
        };
        return _this;
    }

    _createClass(DrawerCmp, [{
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProps) {
            var _nextProps$drawerData = nextProps.drawerData,
                subtitleLeft = _nextProps$drawerData.subtitleLeft,
                subtitleRight = _nextProps$drawerData.subtitleRight;

            this.setState({
                subtitleLeft: subtitleLeft,
                subtitleRight: subtitleRight
            });
        }
    }, {
        key: 'handleChangeSubtitle',
        value: function handleChangeSubtitle(type, data) {
            this.setState(_defineProperty({}, 'subtitle' + type, data));
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var _props = this.props,
                open = _props.open,
                onCloseDrawer = _props.onCloseDrawer,
                drawerData = _props.drawerData;
            var _state = this.state,
                subtitleLeft = _state.subtitleLeft,
                subtitleRight = _state.subtitleRight;

            var visualizations = {
                TWITTER: function TWITTER(params) {
                    return React.createElement(FeedTweets, {
                        queryData: params,
                        service: drawerData.service,
                        onChangeSubtitle: _this2.handleChangeSubtitle
                    });
                },
                MILITANTS: function MILITANTS(params) {
                    return React.createElement(FeedMilitants, {
                        queryData: params,
                        service: drawerData.service,
                        onChangeSubtitle: _this2.handleChangeSubtitle
                    });
                }
            };
            return React.createElement(
                Fragment,
                null,
                React.createElement(
                    'div',
                    Object.assign({ className: 'raw-data__container' }, drawerStyles(open, drawerData.type)),
                    function () {
                        if (!open) return null;
                        return React.createElement(
                            Fragment,
                            null,
                            React.createElement(
                                'div',
                                { className: 'raw-data__header' },
                                React.createElement(
                                    'div',
                                    null,
                                    React.createElement(
                                        'h3',
                                        null,
                                        drawerData.title
                                    ),
                                    React.createElement(
                                        'div',
                                        { className: 'raw-data__header__subtitle' },
                                        React.createElement(
                                            'div',
                                            null,
                                            React.createElement(
                                                'span',
                                                null,
                                                subtitleLeft
                                            )
                                        ),
                                        React.createElement(
                                            'div',
                                            null,
                                            React.createElement(
                                                'span',
                                                null,
                                                subtitleRight
                                            )
                                        )
                                    )
                                )
                            ),
                            visualizations[drawerData.type](drawerData.queryData)
                        );
                    }()
                ),
                React.createElement('div', Object.assign({ // eslint-disable-line
                    className: 'raw-data__overlay'
                }, overlayStyles(open), {
                    onClick: onCloseDrawer
                }))
            );
        }
    }]);

    return DrawerCmp;
}(Component);

DrawerCmp.propTypes = propTypes;
DrawerCmp.defaultProps = defaultProps;

var HocDrawerFeed = function HocDrawerFeed() {
    return React.createElement(
        DrawerContext.Consumer,
        null,
        function (_ref) {
            var open = _ref.open,
                onCloseDrawer = _ref.onCloseDrawer,
                drawerData = _ref.drawerData;
            return React.createElement(DrawerCmp, {
                open: open,
                drawerData: drawerData,
                onCloseDrawer: onCloseDrawer
            });
        }
    );
};

export default HocDrawerFeed;