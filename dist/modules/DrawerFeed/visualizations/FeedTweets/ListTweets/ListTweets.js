import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import Gallery from '../TwitterGallery';
import TwitterCard from '../TwitterCard';
import LoadingModule from '../../../../LoadingModule';
import { listTweetsStyles } from './ListTweetsStyles';

var propTypes = {
    skip: PropTypes.number,
    limit: PropTypes.number,
    isReloading: PropTypes.bool,
    fetchMoreData: PropTypes.func,
    data: PropTypes.arrayOf(PropTypes.shape({}))
};

var defaultProps = {
    skip: 1,
    limit: 0,
    data: [],
    isReloading: false,
    fetchMoreData: function fetchMoreData() {
        return {};
    }
};

var ListTweets = function ListTweets(props) {
    var data = props.data,
        fetchMoreData = props.fetchMoreData,
        skip = props.skip,
        limit = props.limit,
        isReloading = props.isReloading;

    var renderShowMore = function renderShowMore() {
        if (skip < limit) {
            return React.createElement(
                'h4',
                { className: 'nomore' },
                'No hay m\xE1s datos que mostrar'
            );
        }
        return React.createElement(
            'div',
            { className: 'showmore' },
            React.createElement(
                'button',
                {
                    onClick: function onClick() {
                        fetchMoreData();
                    }
                },
                'Mostrar m\xE1s'
            )
        );
    };
    return React.createElement(
        'div',
        listTweetsStyles(),
        function () {
            if (isReloading) {
                return React.createElement(
                    'div',
                    { className: 'tweet__loading' },
                    React.createElement(LoadingModule, null)
                );
            }
            return React.createElement(
                Fragment,
                null,
                data.map(function (tweet) {
                    return React.createElement(
                        TwitterCard,
                        {
                            tweet: tweet,
                            key: tweet.id,
                            viewLabels: 'vacio'
                        },
                        React.createElement(Gallery, { items: tweet.images })
                    );
                }),
                renderShowMore()
            );
        }()
    );
};

ListTweets.propTypes = propTypes;
ListTweets.defaultProps = defaultProps;

export default ListTweets;