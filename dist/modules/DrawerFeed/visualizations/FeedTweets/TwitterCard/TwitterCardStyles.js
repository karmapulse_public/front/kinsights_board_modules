import { css } from 'glamor';
import isEqual from 'lodash/isEqual';

var styles = function styles(type) {
    return css({
        ' .twitter-card': {
            borderRadius: '2px',
            backgroundColor: '#ffffff',
            boxShadow: '0 2px 2px 0 rgba(0, 0, 0, 0.12), 0 0 2px 0 rgba(0, 0, 0, 0.12)',
            width: '100%',
            maxWidth: 590,
            minWidth: 300,
            margin: '0px auto 15px',
            position: 'relative',

            '&__user-image': {
                position: 'absolute',
                top: isEqual(type, 'retweet') ? 15 : 20,
                left: 20,
                width: 48,
                height: 48,
                borderRadius: '2px',
                ' img': {
                    width: '100%'
                }
            },

            '&__container': {
                padding: isEqual(type, 'retweet') ? '15px 20px 20px 78px' : '20px 20px 20px 78px',
                width: '100%',
                boxSizing: 'border-box'
            },

            '&__general': {
                position: 'relative'
            },
            '&__reply': {
                display: 'flex',
                fontSize: '12px',
                margin: '10px 0px',
                alignItems: 'center',

                ' a': {
                    color: '#3949ab',
                    fontWeight: 500,
                    textDecoration: 'none',
                    margin: '0px 5px'
                },

                ' p': {
                    color: 'rgba(0, 0, 0, 0.54)',
                    fontWeight: 500,
                    margin: '0 5px'
                }
            },
            '&__retweets': {
                position: 'relative',
                display: 'flex',
                padding: '20px 0px 15px 78px',
                fontSize: 12,
                alignItems: 'center',

                ' a': {
                    color: '#3949ab',
                    fontWeight: 'bold',
                    textDecoration: 'none'
                },
                ' p': {
                    display: 'inline-block',
                    margin: '0px 5px',
                    color: 'rgba(0, 0, 0, 0.54)',
                    fontWeight: 'bold'
                },
                '&__line': {
                    position: 'absolute',
                    bottom: 0,
                    width: '84%',
                    height: 0,
                    borderBottom: 'solid 1px #e0e0e0'
                },
                '&__user-image': {
                    position: 'relative',
                    width: 16,
                    height: 16,
                    borderRadius: '10%',
                    margin: '0px 10px',

                    ' > img': {
                        width: '100%',
                        height: 'auto'
                    },

                    ' > svg': {
                        borderRadius: '2px',
                        width: '100%',
                        height: 'auto'
                    }
                },
                '&__counters': {
                    display: 'flex',
                    alignItems: 'center',
                    marginLeft: 15,

                    ' > div': {
                        display: 'flex',
                        alignItems: 'center',
                        marginRight: 20
                    },

                    '&__followers': {

                        ' h5': {
                            fontSize: 14,
                            fontWeight: 500,
                            color: '#000',
                            '>svg': {
                                marginRight: 5,
                                verticalAlign: 'middle'
                            }
                        }
                    }
                }
            },
            '&__quote': {
                display: 'flex',
                padding: '20px 20px 0 20px',
                margin: '15px 0px',
                border: '1px solid rgba(0, 0, 0, 0.1)',
                borderRadius: '2px',
                fontSize: 12,

                ' a': {
                    color: '#3949ab',
                    fontWeight: 500,
                    fontSize: '14px',
                    textDecoration: 'none'
                },
                ' a.container__header__username': {
                    margin: '0px 5px',
                    color: 'rgba(0, 0, 0, 0.54)',
                    fontSize: '12px',
                    fontWeight: 500,
                    textDecoration: 'none'
                },
                ' p': {
                    display: 'inline-block',
                    color: 'rgba(0, 0, 0, 0.54)',
                    fontWeight: 'bold'
                },

                ' .container__body a': {
                    fontWeight: 400
                },

                ' .container__media': {
                    width: '100px',
                    marginRight: 20,

                    ' > div > div': {
                        height: '100px'
                    },

                    ' a': {
                        width: 100,
                        height: 100,
                        marginBottom: 20
                    }
                }
            }
        },
        ' .container': {

            '&__header': {
                display: 'flex',
                justifyContent: 'space-between',
                marginBottom: 9,

                ' > div': {
                    display: 'flex',
                    alignItems: 'center',

                    ' >svg': {
                        marginLeft: 5,
                        verticalAlign: 'middle'
                    }
                },
                '&__name': {
                    fontSize: 14,
                    fontWeight: 500,
                    color: '#5c6bc0',
                    marginRight: 10,
                    textDecoration: 'none',
                    '>svg': {
                        marginLeft: 5,
                        verticalAlign: 'middle'
                    }
                },

                '&__username, &__datetime': {
                    fontSize: 12,
                    fontWeight: 500,
                    color: 'rgba(0, 0, 0, 0.54)',
                    textDecoration: 'none'
                }
            },

            '&__sub-header': {
                display: 'flex',
                alignItems: 'center',
                marginBottom: 8,

                ' > div': {
                    display: 'flex',
                    alignItems: 'center',
                    marginRight: 20
                },
                '&__followers': {
                    ' h5': {
                        fontSize: 14,
                        fontWeight: 500,
                        color: '#000',
                        margin: 0,
                        ' > svg': {
                            marginRight: 5,
                            verticalAlign: 'middle'
                        }
                    }
                }
            },

            '&__body > div:first-child': {
                fontSize: 14,
                fontWeight: 400,
                color: 'rgba(0, 0, 0, 0.84)',
                lineHeight: '17px',
                marginBottom: 20,
                wordWrap: 'break-word',

                ' > em': {
                    backgroundColor: '#FFEBB9'
                },
                ' > a': {
                    color: '#5c6bc0',
                    textDecoration: 'none'
                }
            },

            '&__footer': {
                display: 'flex',
                alignItems: 'flex-end',
                justifyContent: 'space-between',

                '&__reactions': {
                    display: 'flex',
                    alignItems: 'center',
                    ' > div': {
                        marginRight: 15,
                        ' h5': {
                            fontSize: 14,
                            color: '#5c6bc0',
                            marginBottom: 2,
                            margin: 0,
                            padding: 0
                        },
                        ' h4': {
                            fontSize: 16,
                            fontWeight: 500,
                            color: '#000',
                            margin: 0,
                            padding: 0
                        }
                    }
                },

                '&__sentiment': {
                    display: 'flex',
                    alignItems: 'center',

                    ' h5': {
                        fontSize: 12,
                        fontWeight: 500,
                        margin: 0,
                        padding: 0
                    },

                    '&__oval': {
                        width: 10,
                        height: 10,
                        marginRight: 5,
                        borderRadius: '50%'
                    },

                    ' .oval-Positivo': {
                        backgroundColor: '#4caf50'
                    },

                    ' .oval-Neutral': {
                        backgroundColor: '#607d8b'
                    },

                    ' .oval-Negativo': {
                        backgroundColor: '#f44336'
                    },

                    ' .text-Positivo': {
                        color: '#4caf50'
                    },

                    ' .text-Neutral': {
                        color: '#607d8b'
                    },

                    ' .text-Negativo': {
                        color: '#f44336'
                    }
                }
            }
        }
    });
};

export default styles;