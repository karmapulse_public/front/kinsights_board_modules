import React from 'react';
import PropTypes from 'prop-types';
import isEqual from 'lodash/isEqual';
import isUndefined from 'lodash/isUndefined';
import replyIcon from '../../../../../../helpers/icons/replyIcon';

var propTypes = {
    childTweet: PropTypes.object,
    type: PropTypes.string,
    intl: PropTypes.object //eslint-disable-line
};

var defaultProps = {
    childTweet: {},
    type: ''
};
var Reply = function Reply(props) {
    if (isEqual(props.type, 'reply')) {
        var users = void 0;
        if (isUndefined(props.childTweet.mentions)) {
            var regex = /^https?:\/\/(www\.)?twitter\.com\/(#!\/)?([^/]+)(\/\w+)*$/;
            var regexNew = /^https?:\/\/(www\.)?twitter\.com\/\/(#!\/)?([^/]+)(\/\w+)*$/;
            var match = regex.exec(props.childTweet.link);
            if (!match) match = regexNew.exec(props.childTweet.link);
            users = function users() {
                return React.createElement(
                    'p',
                    null,
                    'En respuesta a',
                    React.createElement(
                        'a',
                        { href: 'http://www.twitter.com/' + match[3] },
                        '@',
                        match[3]
                    )
                );
            };
        } else if (props.childTweet.mentions.length > 1) {
            users = function users() {
                return React.createElement(
                    'p',
                    null,
                    'En respuesta a',
                    React.createElement(
                        'a',
                        { href: 'http://www.twitter.com/' + props.childTweet.mentions[0] },
                        '@',
                        props.childTweet.mentions[0]
                    ),
                    'y a ',
                    props.childTweet.mentions.length - 1,
                    ' m\xE1s'
                );
            };
        } else {
            users = function users() {
                return React.createElement(
                    'p',
                    null,
                    'En respuesta a',
                    React.createElement(
                        'a',
                        { href: 'http://www.twitter.com/' + props.childTweet.mentions[0] },
                        '@',
                        props.childTweet.mentions[0]
                    )
                );
            };
        }
        return React.createElement(
            'div',
            { className: 'twitter-card__reply' },
            replyIcon({ color: '#000' }),
            users()
        );
    }
    return React.createElement('div', null);
};

Reply.propTypes = propTypes;
Reply.defaultProps = defaultProps;

export default Reply;