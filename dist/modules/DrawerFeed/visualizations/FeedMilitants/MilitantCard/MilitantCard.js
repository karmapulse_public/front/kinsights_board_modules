var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

import React, { useState } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import upperFirst from 'lodash/upperFirst';
import Button from 'kpulse_design_system/dist/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronUp, faChevronDown } from '@fortawesome/free-solid-svg-icons';

import styles from './styles';

var capitalLetter = function capitalLetter(str) {
    if (str && str !== '') {
        var newStr = str.split(' ');
        for (var i = 0, x = newStr.length; i < x; i += 1) {
            if (newStr[i][0]) {
                newStr[i] = newStr[i][0].toUpperCase() + newStr[i].substr(1);
            }
        }
        return newStr.join(' ');
    }
    return '';
};

var dictZone = {
    R: 'Rural',
    U: 'Urbana',
    M: 'Mixta'
};

var hasText = function hasText(str) {
    if (str === '' || str === 'NULL' || str === 'Null') {
        return '---';
    }
    return str;
};

var MilitantCard = function MilitantCard(_ref) {
    var militant = _ref.militant;

    var _useState = useState(false),
        _useState2 = _slicedToArray(_useState, 2),
        expanded = _useState2[0],
        setExpanded = _useState2[1];

    var curClassName = expanded ? 'detail-militant--expanded' : 'detail-militant--collapsed';

    var mName = capitalLetter(militant.name);
    var mPaternal = upperFirst(militant.paternal_surname);
    var mMaternal = upperFirst(militant.maternal_surname);
    var mStreet = hasText(upperFirst(militant.street));
    var mSuburb = capitalLetter(militant.suburb);
    var mMunicipality = upperFirst(militant.municipality);
    var mState = upperFirst(militant.state);
    var mAge = moment().diff(militant.birthdate, 'years');
    return React.createElement(
        'div',
        styles,
        React.createElement(
            'div',
            { className: 'main-militant' },
            React.createElement(
                'div',
                null,
                React.createElement(
                    'h6',
                    null,
                    'Nombre:'
                ),
                React.createElement(
                    'p',
                    null,
                    mName + ' ' + mPaternal + ' ' + mMaternal
                )
            ),
            React.createElement(
                'div',
                null,
                React.createElement(
                    'h6',
                    null,
                    'Sexo:'
                ),
                React.createElement(
                    'p',
                    null,
                    militant.gender
                )
            ),
            React.createElement(
                'div',
                null,
                React.createElement(
                    'h6',
                    null,
                    'Edad:'
                ),
                React.createElement(
                    'p',
                    null,
                    mAge
                )
            ),
            React.createElement(
                'div',
                null,
                React.createElement(
                    'h6',
                    null,
                    'Celular:'
                ),
                React.createElement(
                    'p',
                    null,
                    hasText(militant.mobile)
                )
            ),
            React.createElement(
                'div',
                null,
                React.createElement(
                    'h6',
                    null,
                    'Tel\xE9fono:'
                ),
                React.createElement(
                    'p',
                    null,
                    hasText(militant.phone)
                )
            ),
            React.createElement(
                'div',
                null,
                React.createElement(
                    'h6',
                    null,
                    'Clave Electoral:'
                ),
                React.createElement(
                    'p',
                    null,
                    militant.electoral_code
                )
            ),
            React.createElement(
                'div',
                null,
                React.createElement(
                    'h6',
                    null,
                    'Direcci\xF3n:'
                ),
                React.createElement(
                    'p',
                    null,
                    mStreet + ' ' + militant.exterior_number + ' ' + militant.interior_number + ' ' + mSuburb + ', ' + mMunicipality + ', ' + mState + ', C.P. ' + militant.postal_code
                )
            ),
            function () {
                if (expanded) {
                    return null;
                }
                return React.createElement(
                    'div',
                    null,
                    React.createElement(
                        Button,
                        {
                            ghost: true,
                            bgColor: '#FFF',
                            style: { margin: 10 },
                            onClick: function onClick() {
                                return setExpanded(!expanded);
                            }
                        },
                        'M\xE1s informaci\xF3n',
                        React.createElement(FontAwesomeIcon, { icon: faChevronDown })
                    )
                );
            }()
        ),
        React.createElement(
            'div',
            { className: 'detail-militant ' + curClassName },
            React.createElement(
                'div',
                { className: 'detail-militant__location' },
                React.createElement(
                    'h5',
                    null,
                    'Ubicaci\xF3n'
                ),
                React.createElement(
                    'div',
                    { className: 'detail-militant__location--grid' },
                    React.createElement(
                        'div',
                        { className: 'detail-militant__location--grid--1' },
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'h6',
                                null,
                                'Estado:'
                            ),
                            React.createElement(
                                'p',
                                null,
                                mState
                            )
                        ),
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'h6',
                                null,
                                'Estado ID:'
                            ),
                            React.createElement(
                                'p',
                                null,
                                militant.state_id
                            )
                        ),
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'h6',
                                null,
                                'Municipio:'
                            ),
                            React.createElement(
                                'p',
                                null,
                                mMunicipality
                            )
                        ),
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'h6',
                                null,
                                'Municipio ID:'
                            ),
                            React.createElement(
                                'p',
                                null,
                                militant.municipality_id
                            )
                        ),
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'h6',
                                null,
                                'Cabecera local:'
                            ),
                            React.createElement(
                                'p',
                                null,
                                upperFirst(militant.local_headboard)
                            )
                        ),
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'h6',
                                null,
                                'Cabecera local ID:'
                            ),
                            React.createElement(
                                'p',
                                null,
                                militant.local_headboard_id + ' (' + militant.local_headboard_roman_id + ')'
                            )
                        ),
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'h6',
                                null,
                                'Cabecera federal:'
                            ),
                            React.createElement(
                                'p',
                                null,
                                upperFirst(militant.federal_headboard)
                            )
                        ),
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'h6',
                                null,
                                'Cabecera federal ID:'
                            ),
                            React.createElement(
                                'p',
                                null,
                                militant.federal_headboard_id
                            )
                        )
                    ),
                    React.createElement(
                        'div',
                        { className: 'detail-militant__location--grid--2' },
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'h6',
                                null,
                                'Calle:'
                            ),
                            React.createElement(
                                'p',
                                null,
                                mStreet
                            )
                        ),
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'h6',
                                null,
                                'Colonia:'
                            ),
                            React.createElement(
                                'p',
                                null,
                                hasText(mSuburb)
                            )
                        ),
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'h6',
                                null,
                                'N\xFAmero exterior:'
                            ),
                            React.createElement(
                                'p',
                                null,
                                militant.exterior_number
                            )
                        ),
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'h6',
                                null,
                                'Manzana:'
                            ),
                            React.createElement(
                                'p',
                                null,
                                militant.square
                            )
                        ),
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'h6',
                                null,
                                'N\xFAmero interior:'
                            ),
                            React.createElement(
                                'p',
                                null,
                                hasText(militant.interior_number)
                            )
                        ),
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'h6',
                                null,
                                'C\xF3digo postal:'
                            ),
                            React.createElement(
                                'p',
                                null,
                                militant.postal_code
                            )
                        )
                    )
                ),
                React.createElement(
                    'div',
                    { className: 'detail-militant__location--flex' },
                    React.createElement(
                        'div',
                        null,
                        React.createElement(
                            'h6',
                            null,
                            'Secci\xF3n:'
                        ),
                        React.createElement(
                            'p',
                            null,
                            militant.section_id
                        )
                    ),
                    React.createElement(
                        'div',
                        null,
                        React.createElement(
                            'h6',
                            null,
                            'Zona:'
                        ),
                        React.createElement(
                            'p',
                            null,
                            dictZone[militant.type]
                        )
                    )
                )
            ),
            React.createElement(
                'div',
                { className: 'detail-militant__personal' },
                React.createElement(
                    'h5',
                    null,
                    'Datos personales'
                ),
                React.createElement(
                    'div',
                    { className: 'detail-militant__personal--row' },
                    React.createElement(
                        'div',
                        null,
                        React.createElement(
                            'h6',
                            null,
                            'Nombre:'
                        ),
                        React.createElement(
                            'p',
                            null,
                            mName
                        )
                    ),
                    React.createElement(
                        'div',
                        null,
                        React.createElement(
                            'h6',
                            null,
                            'Apellido paterno:'
                        ),
                        React.createElement(
                            'p',
                            null,
                            mPaternal
                        )
                    ),
                    React.createElement(
                        'div',
                        null,
                        React.createElement(
                            'h6',
                            null,
                            'Apellido materno:'
                        ),
                        React.createElement(
                            'p',
                            null,
                            mMaternal
                        )
                    )
                ),
                React.createElement(
                    'div',
                    { className: 'detail-militant__personal--row' },
                    React.createElement(
                        'div',
                        null,
                        React.createElement(
                            'h6',
                            null,
                            'Edad:'
                        ),
                        React.createElement(
                            'p',
                            null,
                            mAge
                        )
                    ),
                    React.createElement(
                        'div',
                        null,
                        React.createElement(
                            'h6',
                            null,
                            'Sexo:'
                        ),
                        React.createElement(
                            'p',
                            null,
                            militant.gender
                        )
                    ),
                    React.createElement(
                        'div',
                        null,
                        React.createElement(
                            'h6',
                            null,
                            'Fecha de nacimiento:'
                        ),
                        React.createElement(
                            'p',
                            null,
                            moment(militant.birthdate).format('L')
                        )
                    )
                ),
                React.createElement(
                    'div',
                    { className: 'detail-militant__personal--row' },
                    React.createElement(
                        'div',
                        null,
                        React.createElement(
                            'h6',
                            null,
                            'Clave electoral:'
                        ),
                        React.createElement(
                            'p',
                            null,
                            militant.electoral_code
                        )
                    ),
                    React.createElement(
                        'div',
                        null,
                        React.createElement(
                            'h6',
                            null,
                            'N\xFAmero de credencial:'
                        ),
                        React.createElement(
                            'p',
                            null,
                            hasText(militant.electoral_card_number)
                        )
                    )
                ),
                React.createElement(
                    'div',
                    { className: 'detail-militant__personal--row' },
                    React.createElement(
                        'div',
                        null,
                        React.createElement(
                            'h6',
                            null,
                            'Tel\xE9fono:'
                        ),
                        React.createElement(
                            'p',
                            null,
                            hasText(militant.phone)
                        )
                    ),
                    React.createElement(
                        'div',
                        null,
                        React.createElement(
                            'h6',
                            null,
                            'Celular:'
                        ),
                        React.createElement(
                            'p',
                            null,
                            hasText(militant.mobile)
                        )
                    ),
                    React.createElement(
                        'div',
                        null,
                        React.createElement(
                            'h6',
                            null,
                            'Correo:'
                        ),
                        React.createElement(
                            'p',
                            null,
                            hasText(militant.email)
                        )
                    )
                )
            ),
            React.createElement(
                'div',
                { className: 'detail-militant__afiliation' },
                React.createElement(
                    'h5',
                    null,
                    'Afiliaci\xF3n'
                ),
                React.createElement(
                    'div',
                    { className: 'detail-militant__afiliation--row' },
                    React.createElement(
                        'div',
                        null,
                        React.createElement(
                            'h6',
                            null,
                            'Fecha de Afiliaci\xF3n:'
                        ),
                        React.createElement(
                            'p',
                            null,
                            moment(militant.affilation_date).format('L')
                        )
                    ),
                    React.createElement(
                        'div',
                        null,
                        React.createElement(
                            'h6',
                            null,
                            'Cargo:'
                        ),
                        React.createElement(
                            'p',
                            null,
                            hasText(militant.partisan_position)
                        )
                    ),
                    React.createElement(
                        'div',
                        null,
                        React.createElement(
                            'h6',
                            null,
                            'Categoria:'
                        ),
                        React.createElement(
                            'p',
                            null,
                            hasText(militant.category)
                        )
                    )
                )
            ),
            React.createElement(
                'div',
                { className: 'detail-militant__button' },
                React.createElement(
                    Button,
                    {
                        ghost: true,
                        bgColor: '#FFF',
                        onClick: function onClick() {
                            return setExpanded(false);
                        }
                    },
                    'Menos informaci\xF3n',
                    React.createElement(FontAwesomeIcon, { icon: faChevronUp })
                )
            )
        )
    );
};

MilitantCard.propTypes = {
    militant: PropTypes.shape({})
};

MilitantCard.defaultProps = {
    militant: {}
};

export default MilitantCard;