import { css } from 'glamor';

export default css({
    ' .main-militant': {
        display: 'grid',
        gridTemplateAreas: '\n        "name name name gender age mobile mobile phone phone"\n        "electoral electoral address address address address address address address"\n        "more more more more more more more more more"\n    ',
        gridGap: '10px',
        width: '100%',
        padding: '40px 50px 25px 50px',
        boxSizing: 'border-box',
        backgroundColor: '#273136',
        borderBottom: '1px solid rgb(70, 77, 81)',
        ' > div:nth-child(1)': {
            gridArea: 'name'
        },
        ' > div:nth-child(2)': {
            gridArea: 'gender'
        },
        ' > div:nth-child(3)': {
            gridArea: 'age'
        },
        ' > div:nth-child(4)': {
            gridArea: 'mobile'
        },
        ' > div:nth-child(5)': {
            gridArea: 'phone'
        },
        ' > div:nth-child(6)': {
            gridArea: 'electoral'
        },
        ' > div:nth-child(7)': {
            gridArea: 'address'
        },
        ' > div:nth-child(8)': {
            gridArea: 'more',
            justifySelf: 'center'
        },
        ' h6': {
            fontSize: 14,
            color: 'rgba(255, 255, 255, 0.5)',
            margin: '5px 0px'
        },
        ' p': {
            fontSize: 14,
            color: '#FFF',
            margin: '5px 0px'
        }
    },

    ' .detail-militant': {
        backgroundColor: '#1f2b31',
        boxSizing: 'border-box',
        transition: 'display 0.3s, opacity 0.2s, height 0.3s, padding 0.3s ease',
        '&--expanded': {
            display: 'block',
            height: 'auto',
            opacity: 1,
            padding: '40px 50px'
        },
        '&--collapsed': {
            display: 'none',
            opacity: 0,
            height: 0,
            padding: '0px 50px'
        },
        '&__location': {
            borderBottom: '1px solid rgb(70, 77, 81)',
            ' > h5': {
                fontSize: 14,
                fontWeight: 500,
                textTransform: 'uppercase',
                margin: '0px 0px 20px 0px',
                color: '#FFF'
            },
            ' h6': {
                fontSize: 14,
                color: 'rgba(255, 255, 255, 0.5)',
                margin: '5px 0px'
            },
            ' p': {
                fontSize: 14,
                color: '#FFF',
                margin: '5px 0px'
            },
            '&--grid': {
                width: '100%',
                display: 'grid',
                gridTemplateAreas: '\n                    "dtstate dtstate dtstate dtstate dtstate dtaddress dtaddress dtaddress"\n                    "dtstate dtstate dtstate dtstate dtstate dtaddress dtaddress dtaddress"\n                    "dtstate dtstate dtstate dtstate dtstate dtaddress dtaddress dtaddress"\n                    "dtstate dtstate dtstate dtstate dtstate dtaddress dtaddress dtaddress"\n                ',
                gridColumnGap: '10px',
                '&--1': {
                    gridArea: 'dtstate',
                    display: 'grid',
                    gridTemplateAreas: '\n                        "state state state stateid stateid"\n                        "munic munic munic municid municid"\n                        "local local local localid localid"\n                        "fedrl fedrl fedrl fedrlid fedrlid"\n                    ',
                    gridGap: '10px',
                    ' > div:nth-child(1)': {
                        gridArea: 'state'
                    },
                    ' > div:nth-child(2)': {
                        gridArea: 'stateid'
                    },
                    ' > div:nth-child(3)': {
                        gridArea: 'munic'
                    },
                    ' > div:nth-child(4)': {
                        gridArea: 'municid'
                    },
                    ' > div:nth-child(5)': {
                        gridArea: 'local'
                    },
                    ' > div:nth-child(6)': {
                        gridArea: 'localid'
                    },
                    ' > div:nth-child(7)': {
                        gridArea: 'fedrl'
                    },
                    ' > div:nth-child(8)': {
                        gridArea: 'fedrlid'
                    }
                },
                '&--2': {
                    gridArea: 'dtaddress',
                    display: 'grid',
                    gridTemplateAreas: '\n                        "street street street street"\n                        "suburb suburb suburb suburb"\n                        "next next manz manz"\n                        "nint nint codp codp"\n                    ',
                    gridGap: '10px',
                    paddingLeft: 35,
                    boxSizing: 'border-box',
                    borderLeft: '1px solid rgb(70, 77, 81)',
                    ' > div:nth-child(1)': {
                        gridArea: 'street'
                    },
                    ' > div:nth-child(2)': {
                        gridArea: 'suburb'
                    },
                    ' > div:nth-child(3)': {
                        gridArea: 'next'
                    },
                    ' > div:nth-child(4)': {
                        gridArea: 'manz'
                    },
                    ' > div:nth-child(5)': {
                        gridArea: 'nint'
                    },
                    ' > div:nth-child(6)': {
                        gridArea: 'codp'
                    }
                }
            },
            '&--flex': {
                display: 'flex',
                margin: '35px 0px',
                ' > div': {
                    marginRight: 40
                }
            }
        },

        '&__personal': {
            borderBottom: '1px solid rgb(70, 77, 81)',
            ' > h5': {
                fontSize: 14,
                fontWeight: 500,
                textTransform: 'uppercase',
                margin: '20px 0px',
                color: '#FFF'
            },
            ' h6': {
                fontSize: 14,
                color: 'rgba(255, 255, 255, 0.5)',
                margin: '5px 0px'
            },
            ' p': {
                fontSize: 14,
                color: '#FFF',
                margin: '5px 0px'
            },
            '&--row': {
                display: 'flex',
                margin: '20px 0px',
                ' > div': {
                    marginRight: 40
                }
            }
        },

        '&__afiliation': {
            ' > h5': {
                fontSize: 14,
                fontWeight: 500,
                textTransform: 'uppercase',
                margin: '20px 0px',
                color: '#FFF'
            },
            ' h6': {
                fontSize: 14,
                color: 'rgba(255, 255, 255, 0.5)',
                margin: '5px 0px'
            },
            ' p': {
                fontSize: 14,
                color: '#FFF',
                margin: '5px 0px'
            },
            '&--row': {
                display: 'flex',
                margin: '20px 0px',
                ' > div': {
                    marginRight: 40
                }
            }
        },

        '&__button': {
            width: '100%',
            display: 'flex',
            justifyContent: 'center'
        }
    }
});