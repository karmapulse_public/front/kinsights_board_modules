import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import Button from 'kpulse_design_system/dist/Button';

import LoadingModule from '../../../../LoadingModule';
import MilitantCard from '../MilitantCard';
import { listStyles } from './styles';

var propTypes = {
    skip: PropTypes.number,
    limit: PropTypes.number,
    isReloading: PropTypes.bool,
    fetchMoreData: PropTypes.func,
    data: PropTypes.arrayOf(PropTypes.shape({}))
};

var defaultProps = {
    skip: 1,
    limit: 0,
    data: [],
    isReloading: false,
    fetchMoreData: function fetchMoreData() {
        return {};
    }
};

var ListMilitants = function ListMilitants(props) {
    var data = props.data,
        fetchMoreData = props.fetchMoreData,
        skip = props.skip,
        limit = props.limit,
        isReloading = props.isReloading;

    var renderShowMore = function renderShowMore() {
        if (isEmpty(data)) {
            return React.createElement(
                'h4',
                { className: 'nomore' },
                'La informaci\xF3n estar\xE1 lista pronto'
            );
        } else if (skip < limit) {
            return React.createElement(
                'h4',
                { className: 'nomore' },
                'No hay m\xE1s datos que mostrar'
            );
        }
        return React.createElement(
            'div',
            { className: 'showmore' },
            React.createElement(
                Button,
                {
                    large: true,
                    round: true,
                    secondary: true,
                    bgColor: '#FFF',
                    style: { margin: '20px auto' },
                    onClick: function onClick() {
                        fetchMoreData();
                    }
                },
                'Mostrar m\xE1s'
            )
        );
    };
    return React.createElement(
        'div',
        listStyles(),
        function () {
            if (isReloading) {
                return React.createElement(
                    'div',
                    { className: 'loading' },
                    React.createElement(LoadingModule, null)
                );
            }
            return React.createElement(
                Fragment,
                null,
                data.map(function (m) {
                    return React.createElement(MilitantCard, { key: m.id, militant: m });
                }),
                renderShowMore()
            );
        }()
    );
};

ListMilitants.propTypes = propTypes;
ListMilitants.defaultProps = defaultProps;

export default ListMilitants;