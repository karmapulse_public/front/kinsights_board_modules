import { css } from 'glamor';

export var listStyles = function listStyles() {
    return css({
        backgroundColor: '#263238',
        width: '100%',
        height: 'calc(100% - 75px)',
        overflow: 'auto',
        padding: '0px',
        ' .loading': {
            height: '90%',
            position: 'relative',
            ' > div': {
                position: 'absolute',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)'
            }
        },
        ' .nomore': {
            textAlign: 'center',
            margin: 0,
            padding: '25px 0 35px',
            boxSizing: 'border-box',
            fontWeight: 500,
            fontSize: 14,
            color: '#ffff'
        }
    });
};

export var cardFeedStyles = function cardFeedStyles() {
    return css({
        position: 'relative',
        width: 560
    });
};