import { css } from 'glamor';

export var loadingBoxStyles = css({
    position: 'relative',
    width: 'auto',
    height: 'calc(100vh - 75px)'
});

export var stylesMaterial = {
    loading: {
        position: 'absolute',
        top: '46%',
        left: '50%',
        transform: 'translate(-50%, -50%)'
    }
};