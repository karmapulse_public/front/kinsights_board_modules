import { Observable } from 'rxjs';
import { fetchXHR } from '../../../../helpers/http';

export default (function (url, params) {
    return Observable.create(function (observer) {
        Observable.fromPromise(fetchXHR(url, 'GET', params)).subscribe(function (data) {
            var response = data.json.response;

            observer.next(Object.assign({}, response));
        }, function (error) {
            observer.error(error);
        });
    });
});