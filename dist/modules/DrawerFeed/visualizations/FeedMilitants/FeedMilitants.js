var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cloneDeep from 'lodash/cloneDeep';

import fetchExp from './queries';
import ListMilitants from './ListMilitants';
import { loadingBoxStyles } from './styles';
import LoadingModule from '../../../LoadingModule';

var propTypes = {
    service: PropTypes.string,
    queryData: PropTypes.object
};

var defaultProps = {
    service: '',
    queryData: {}
};

var FeedTweets = function (_Component) {
    _inherits(FeedTweets, _Component);

    function FeedTweets(props) {
        _classCallCheck(this, FeedTweets);

        var _this = _possibleConstructorReturn(this, (FeedTweets.__proto__ || Object.getPrototypeOf(FeedTweets)).call(this, props));

        _this.fetchMoreData = _this.fetchMoreData.bind(_this);
        _this.state = {
            skip: 0,
            data: [],
            isLoading: true,
            paginationId: '',
            isReloading: false,
            queryData: props.queryData
        };
        return _this;
    }

    _createClass(FeedTweets, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var queryData = this.props.queryData;

            this.fetchData(queryData);
        }
    }, {
        key: 'fetchData',
        value: function fetchData(queryData) {
            var _this2 = this;

            fetchExp(this.props.service, queryData).subscribe(function (response) {
                var newData = cloneDeep(_this2.state.data);
                if (_this2.state.isReloading) {
                    newData = response.data;
                } else {
                    var _newData;

                    (_newData = newData).push.apply(_newData, _toConsumableArray(response.data));
                }
                // setStringPosts(
                //          ${response.data.explorationInGraphic.total} de ${datasetActive.consumption_total} tweets`);
                _this2.setState({
                    data: newData,
                    isLoading: false,
                    skip: response.data.length,
                    paginationId: response.pagination_id
                });
            }, function (error) {
                console.log(error); // eslint-disable-line
                _this2.setState({
                    data: [],
                    isLoading: false
                });
            }, null);
        }
    }, {
        key: 'fetchMoreData',
        value: function fetchMoreData() {
            var _state = this.state,
                queryData = _state.queryData,
                paginationId = _state.paginationId;

            var newQuery = Object.assign({}, queryData, {
                filters: Object.assign({}, queryData.filters, {
                    pagination_id: paginationId
                })
            });
            this.fetchData(newQuery);
        }
    }, {
        key: 'render',
        value: function render() {
            var _state2 = this.state,
                data = _state2.data,
                isLoading = _state2.isLoading,
                isReloading = _state2.isReloading,
                skip = _state2.skip;
            var queryData = this.props.queryData;

            if (isLoading) {
                return React.createElement(
                    'div',
                    loadingBoxStyles,
                    React.createElement(LoadingModule, null)
                );
            }
            return React.createElement(ListMilitants, {
                data: data,
                skip: skip,
                isReloading: isReloading,
                limit: queryData.filters.limit,
                fetchMoreData: this.fetchMoreData
            });
        }
    }]);

    return FeedTweets;
}(Component);

FeedTweets.propTypes = propTypes;
FeedTweets.defaultProps = defaultProps;

export default FeedTweets;