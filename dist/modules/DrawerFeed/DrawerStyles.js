import { css } from 'glamor';

var typeStyles = {
    TWITTER: {
        backgroundColor: 'rgb(255, 255, 255)',
        width: 610,
        transform: 'translate(620px, 0px)',
        header: {
            height: 65,
            backgroundColor: '#566b72'
        }
    },
    MILITANTS: {
        backgroundColor: '#263238',
        width: 807,
        transform: 'translate(810px, 0px)',
        header: {
            height: 75,
            backgroundColor: '#1f2b31'
        }
    }
};

export default (function (isOpen, type) {
    var transform = isOpen ? 'translate(0px, 0px)' : typeStyles[type].transform;
    return css({
        backgroundColor: typeStyles[type].backgroundColor,
        transition: 'transform 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
        boxSizing: 'border-box',
        boxShadow: 'rgba(0, 0, 0, 0.16) 0px 3px 10px, rgba(0, 0, 0, 0.23) 0px 3px 10px',
        height: '100%',
        width: typeStyles[type].width,
        position: 'fixed',
        zIndex: 1300,
        top: 0,
        left: 'auto',
        right: 0,
        transform: transform,
        ' .raw-data__header': {
            position: 'relative',
            height: typeStyles[type].header.height,
            padding: '15px 15px 15px 40px',
            color: '#fff',
            backgroundColor: typeStyles[type].header.backgroundColor,
            boxSizing: 'border-box',
            ' h3': {
                width: 'calc(100% - 30px)',
                margin: 0,
                fontSize: 16,
                fontWeight: 500,
                letterSpacing: 0.6,
                textTransform: 'uppercase'
            },
            ' span': {
                display: 'block',
                fontSize: 12,
                letterSpacing: 0.4,
                '&::first-letter': {
                    textTransform: 'capitalize'
                }
            },
            '&__back': {
                position: 'absolute',
                top: '0px',
                left: '0px',
                display: 'flex',
                width: 40,
                height: '100%',
                alignItems: 'center',
                justifyContent: 'center',
                cursor: 'pointer'
            },
            '&__subtitle': {
                display: 'flex',
                justifyContent: 'space-between',
                marginTop: 5,
                ' div': {
                    display: 'inline-block'
                },
                ' div:first-child': {
                    textTransform: 'uppercase'
                }
            }
        }
    });
});

export var overlayStyles = function overlayStyles(isOpen) {
    var zIndex = isOpen ? 100 : -1;
    var opacity = isOpen ? 1 : 0;
    return css({
        width: '100%',
        height: '100%',
        backgroundColor: 'rgba(0,0,0,.7)',
        position: 'fixed',
        top: 0,
        left: 0,
        zIndex: zIndex,
        opacity: opacity,
        transition: 'all .2s ease'
    });
};