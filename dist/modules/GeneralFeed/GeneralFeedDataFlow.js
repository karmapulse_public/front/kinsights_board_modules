import { Observable } from 'rxjs';
import { fetchXHR } from '../../helpers/http';

export default (function (url, params) {
    return Observable.create(function (observer) {
        Observable.fromPromise(fetchXHR(url, 'GET', params)).subscribe(function (data) {
            observer.next({
                data: data.json.response
            });
        }, function (error) {
            observer.error(error);
        });
    });
});