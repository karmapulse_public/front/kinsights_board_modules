var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import clone from 'lodash/clone';
import moment from 'moment-timezone';
import fetchGeneralFeed from './GeneralFeedDataFlow';

import LoadingModule from '../LoadingModule';
import List from './visualizations/List';
import Screen from './visualizations/Screen';

var propTypes = {
    side: PropTypes.string,
    color: PropTypes.string,
    fields: PropTypes.object,
    dateRange: PropTypes.object,
    services: PropTypes.object,
    filters: PropTypes.object,
    viewLabels: PropTypes.string,
    tw_search_id: PropTypes.string,
    fb_search_id: PropTypes.string,
    in_search_id: PropTypes.string
};

var defaultProps = {
    side: 'left',
    color: '#666',
    fields: {},
    dateRange: {},
    services: {},
    filters: {},
    viewLabels: '',
    tw_search_id: '',
    fb_search_id: '',
    in_search_id: ''
};

var GeneralFeed = function (_Component) {
    _inherits(GeneralFeed, _Component);

    function GeneralFeed(props) {
        _classCallCheck(this, GeneralFeed);

        var _this = _possibleConstructorReturn(this, (GeneralFeed.__proto__ || Object.getPrototypeOf(GeneralFeed)).call(this, props));

        _this.state = {
            moduleConfig: Object.assign({}, props.fields, props.services),
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color
        };
        return _this;
    }

    _createClass(GeneralFeed, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var _this2 = this;

            var _props = this.props,
                dateRange = _props.dateRange,
                tw_search_id = _props.tw_search_id,
                fb_search_id = _props.fb_search_id,
                in_search_id = _props.in_search_id;
            var startDate = dateRange.startDate,
                endDate = dateRange.endDate;


            var params = {
                recipe_id: 'module_general_feed',
                'rule-id': tw_search_id, // twitter rule id
                'facebook-rule-id': fb_search_id,
                'instagram-rule-id': in_search_id
            };

            if (dateRange && Object.keys(dateRange).length > 0) {
                var tz = 'America/Mexico_City';
                var strStartDate = clone(startDate).format('YYYY-MM-DD hh:mm:ss');
                var strEndDate = clone(endDate).format('YYYY-MM-DD hh:mm:ss');

                var utcOffsetStart = moment.tz(strStartDate, tz).isDST() ? '-05:00' : '-06:00';
                var utcOffsetEnd = moment.tz(strEndDate, tz).isDST() ? '-05:00' : '-06:00';

                startDate = startDate.utcOffset(utcOffsetStart).set({
                    hour: 0,
                    minute: 0,
                    second: 0,
                    millisecond: 0
                }).toISOString();

                endDate = endDate.utcOffset(utcOffsetEnd).set({
                    minute: 5 * Math.floor(moment().minute() / 5),
                    second: 0,
                    millisecond: 0
                }).toISOString();

                params.initial_date = startDate;
                params.final_date = endDate;
            }

            if (this.props.filters) {
                params.filters = Object.assign({}, this.props.filters);
            }

            fetchGeneralFeed(this.props.services.twitter, params).subscribe(function (data) {
                _this2.setState({
                    moduleData: data
                });
            }, function (error) {
                console.log(error);
            }, null);
        }
    }, {
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProp) {
            var _this3 = this;

            var _nextProp$dateRange = nextProp.dateRange,
                startDate = _nextProp$dateRange.startDate,
                endDate = _nextProp$dateRange.endDate;
            var tw_search_id = nextProp.tw_search_id,
                fb_search_id = nextProp.fb_search_id,
                in_search_id = nextProp.in_search_id;


            var params = {
                recipe_id: 'module_general_feed',
                'rule-id': tw_search_id, // twitter rule id
                'facebook-rule-id': fb_search_id,
                'instagram-rule-id': in_search_id
            };

            if (nextProp.dateRange && Object.keys(nextProp.dateRange).length > 0) {
                var tz = 'America/Mexico_City';
                var strStartDate = clone(startDate).format('YYYY-MM-DD hh:mm:ss');
                var strEndDate = clone(endDate).format('YYYY-MM-DD hh:mm:ss');

                var utcOffsetStart = moment.tz(strStartDate, tz).isDST() ? '-05:00' : '-06:00';
                var utcOffsetEnd = moment.tz(strEndDate, tz).isDST() ? '-05:00' : '-06:00';

                startDate = startDate.utcOffset(utcOffsetStart).set({
                    hour: 0,
                    minute: 0,
                    second: 0,
                    millisecond: 0
                }).toISOString();

                endDate = endDate.utcOffset(utcOffsetEnd).set({
                    minute: 5 * Math.floor(moment().minute() / 5),
                    second: 0,
                    millisecond: 0
                }).toISOString();

                params.initial_date = startDate;
                params.final_date = endDate;
            }

            if (this.props.filters) {
                params.filters = Object.assign({}, this.props.filters);
            }

            fetchGeneralFeed(this.props.services.twitter, params).subscribe(function (data) {
                _this3.setState({
                    moduleConfig: Object.assign({}, nextProp.fields, nextProp.services),
                    moduleData: data,
                    moduleLoading: false,
                    moduleSide: nextProp.side,
                    moduleColor: nextProp.color
                });
            }, function (error) {
                console.log(error);
            }, null);
        }
    }, {
        key: 'renderByViz',
        value: function renderByViz() {
            var _state = this.state,
                moduleConfig = _state.moduleConfig,
                moduleData = _state.moduleData;


            if (moduleData) {
                var visualizations = {
                    list: function list(params) {
                        return React.createElement(List, params);
                    },
                    screen: function screen(params) {
                        return React.createElement(Screen, params);
                    }
                };
                return visualizations[moduleConfig.visualization](this.state);
            }

            return React.createElement(LoadingModule, null);
        }
    }, {
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'm-general-feed' },
                this.renderByViz()
            );
        }
    }]);

    return GeneralFeed;
}(Component);

GeneralFeed.propTypes = propTypes;
GeneralFeed.defaultProps = defaultProps;

export default GeneralFeed;