import React from 'react';
import PropTypes from 'prop-types';
import groupBy from 'lodash/groupBy';

import Gallery from '../../../../helpers/customComponents/Gallery';
import Header from '../../../../helpers/customComponents/Header';
import Sentiment from '../../../../helpers/customComponents/Sentiment';
import BodyText from '../../../../helpers/customComponents/BodyText';
import Card from '../../../../helpers/customComponents/Card';
import Video from '../../../../helpers/customComponents/Video';
import Link from '../../../../helpers/customComponents/Link';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    viewLabels: PropTypes.string,
    moduleColor: PropTypes.string
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    viewLabels: ''
};

// <Gallery items={item.} />
var List = function List(props) {
    var isSource = function isSource(source, item) {
        var t = {
            instagram: function instagram() {
                return React.createElement(
                    React.Fragment,
                    null,
                    function () {
                        if (item.media_type === 'IMAGE') {
                            return React.createElement(Gallery, {
                                items: item.images,
                                height: '300px',
                                type: source,
                                url: item.media_url
                            });
                        }
                        if (item.media_type === 'VIDEO') {
                            var soruces = [{
                                url: item.media_url
                            }];
                            return React.createElement(Video, {
                                sources: soruces,
                                height: '300px'
                            });
                        }
                        if (item.media_type === 'CAROUSEL_ALBUM') {
                            return React.createElement(Gallery, {
                                items: item.images,
                                height: '300px',
                                type: source,
                                url: item.media_url
                            });
                        }
                        return null;
                    }(),
                    React.createElement(BodyText, { text: item.body, type: source })
                );
            },
            facebook: function facebook() {
                return React.createElement(
                    React.Fragment,
                    null,
                    React.createElement(BodyText, { text: item.body, type: source }),
                    function () {
                        if (item.type === 'photo') {
                            return React.createElement(Gallery, {
                                items: item.images,
                                height: '300px',
                                type: source,
                                url: item.attachment.url
                            });
                        }
                        if (item.type === 'video') {
                            return React.createElement(Gallery, {
                                items: item.images,
                                height: '300px',
                                type: source,
                                video: true,
                                url: item.attachment.url
                            });
                        }
                        if (item.type === 'link') {
                            return (
                                // eslint-disable-next-line jsx-a11y/anchor-is-valid
                                React.createElement(Link, {
                                    url: item.attachment.url,
                                    imageUrl: item.attachment.media,
                                    title: item.attachment.title,
                                    text: item.attachment.description,
                                    type: source
                                })
                            );
                        }
                        return null;
                    }()
                );
            },
            twitter: function twitter() {
                return React.createElement(
                    React.Fragment,
                    null,
                    React.createElement(BodyText, { text: item.body, type: source }),
                    function () {
                        if (item.media_type === 'image') {
                            return React.createElement(Gallery, {
                                items: item.images,
                                height: '300px',
                                type: source,
                                url: item.link
                            });
                        }
                        if (item.media_type === 'gif') {
                            return React.createElement(Video, {
                                sources: JSON.parse(item.gif.variants),
                                cover: item.gif.cover,
                                type: source,
                                gif: true
                            });
                        }
                        if (item.media_type === 'video') {
                            return React.createElement(Video, {
                                sources: JSON.parse(item.video.variants),
                                cover: item.video.cover,
                                type: source
                            });
                        }
                        if (item.urls) {
                            return (
                                // eslint-disable-next-line jsx-a11y/anchor-is-valid
                                React.createElement(Link, {
                                    url: item.urls[0].url,
                                    title: item.urls[0].title,
                                    text: item.urls[0].description,
                                    type: source
                                })
                            );
                        }
                        return null;
                    }()
                );
            }
        };
        // console.log(t[source]);
        return t[source]();
    };
    var noSource = Object.keys(groupBy(props.moduleData.data, function (i) {
        return i.source_type;
    })).length <= 1;
    return React.createElement(
        React.Fragment,
        null,
        props.moduleData.data.map(function (item) {
            return React.createElement(
                Card,
                { key: item.id },
                React.createElement(Header, { noSource: noSource, item: item, services: props.moduleConfig }),
                isSource(item.source_type, item),
                React.createElement(Sentiment, { item: item, noSource: noSource })
            );
        })
    );
};

List.propTypes = propTypes;
List.defaultProps = defaultProps;

export default List;