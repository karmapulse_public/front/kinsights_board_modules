import { css } from 'glamor';

var styles = function styles() {
    return css({

        ' div': {
            ' > section': {
                display: 'flex',
                position: 'relative',
                padding: '0px 0px 10px',
                margin: '10px 5px',
                color: '#000',
                border: '1px solid #C9C9C9',
                borderRadius: '4px',
                flexDirection: 'column',
                justifyContent: 'space-between',
                ' a header': {
                    '.header': {
                        justifyContent: 'center'
                    },
                    ' h6': {
                        color: '#000'
                    },
                    ' p': {
                        color: 'rgba(0, 0, 0, 0.5)'
                    }
                },
                ' a > article, > div ': {
                    height: 'auto',
                    zIndex: 1,
                    marginTop: 10
                },
                ' a > div > p': {
                    // maxHeight: '81px',
                    padding: '15px 15px 25px 15px',
                    color: '#000'
                    // overflow: 'hidden',
                }
            }
        }

    });
};

export default styles;