import React from 'react';
import PropTypes from 'prop-types';
import groupBy from 'lodash/groupBy';
import isUndefined from 'lodash/isUndefined';
import './masonry.css';
// eslint-disable-next-line import/first
import Masonry from 'react-masonry-css';

import Header from '../../../../helpers/customComponents/Header';
import Footer from '../../../../helpers/customComponents/Footer';
import BodyText from '../../../../helpers/customComponents/BodyText';
import Card from '../../../../helpers/customComponents/Card';
import styles from './ScreenStyles';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    viewLabels: PropTypes.string,
    moduleColor: PropTypes.string
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    viewLabels: ''
};

var ID = function ID() {
    return '_' + Math.random().toString(36).substr(2, 9);
};

var List = function List(props) {

    var isSource = function isSource(source, item) {
        var t = {
            instagram: function instagram() {
                if (item.media_type === 'IMAGE') {
                    return [item.media_url, item.media_type, 'INSTAGRAM'];
                }
                if (item.media_type === 'VIDEO') {
                    return [item.media_url, item.media_type, 'INSTAGRAM'];
                }
                if (item.media_type === 'CAROUSEL_ALBUM') {
                    var e = item.images[0];
                    return isUndefined(e.url) ? [e.media_url, item.media_type, 'INSTAGRAM'] : [e.url, item.media_type, 'INSTAGRAM'];
                }
                return null;
            },
            facebook: function facebook() {
                if (item.type === 'photo' || item.type === 'video') {
                    var e = item.images[0];
                    return isUndefined(e.url) ? [e.media_url, item.type.toUpperCase(), 'FACEBOOK'] : [e.url, item.type.toUpperCase(), 'FACEBOOK'];
                }
                return '';
            },
            twitter: function twitter() {
                if (item.media_type === 'image') {
                    var e = item.images[0];
                    return isUndefined(e.url) ? [e.media_url, item.media_type.toUpperCase(), 'TWITTER'] : [e.url, item.media_type.toUpperCase(), 'TWITTER'];
                }
                if (item.media_type === 'gif') {
                    return [item.gif.cover, item.media_type.toUpperCase(), 'TWITTER'];
                }
                if (item.media_type === 'video') {
                    return [item.video.cover, item.media_type.toUpperCase(), 'TWITTER'];
                }
                return [];
            }
        };
        return t[source]();
    };
    var noSource = Object.keys(groupBy(props.moduleData.data, function (i) {
        return i.source_type;
    })).length <= 1;
    return React.createElement(
        'div',
        styles(),
        React.createElement(
            Masonry,
            {
                breakpointCols: 3,
                className: 'my-masonry-grid',
                columnClassName: 'my-masonry-grid_column'
            },
            props.moduleData.data.map(function (item) {
                var type = isSource(item.source_type, item);
                return React.createElement(
                    Card,
                    { key: '' + item.id + ID(), isScreen: true, link: item.link },
                    React.createElement(Header, {
                        noDate: true,
                        noSource: noSource,
                        isScreen: true,
                        item: item,
                        services: props.moduleConfig
                    }),
                    React.createElement(
                        'div',
                        null,
                        React.createElement(BodyText, { text: item.body, type: item.source_type }),
                        React.createElement(Footer, { item: item, type: type[1], isScreen: true })
                    )
                );
            })
        )
    );
};

List.propTypes = propTypes;
List.defaultProps = defaultProps;

export default List;