import React from 'react';
import { css } from 'glamor';
import NoDataIcon from '../../helpers/icons/noDataIcon';

var styles = css({
    width: '100%',
    height: '100%',
    minHeight: 200,
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    paddingTop: 45,

    ' h1:last-child': {
        marginTop: 20,
        fontSize: 13,
        fontWeight: 600,
        letterSpacing: '1.1px',
        opacity: '0.5'
    }
});

var NoDataModule = function NoDataModule(_ref) {
    var color = _ref.color;
    return React.createElement(
        'div',
        Object.assign({ className: 'no-data-module' }, styles),
        React.createElement(NoDataIcon, { color: 'rgba(0,0,0,0.5)' }),
        React.createElement(
            'h1',
            null,
            'NO HAY DATOS PARA MOSTRAR'
        )
    );
};

export default NoDataModule;