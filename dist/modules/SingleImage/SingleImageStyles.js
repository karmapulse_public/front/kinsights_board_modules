import { css } from 'glamor';

var styles = function styles(color) {
    return css({
        ' .single-image__title': {
            ' h3': {
                textTransform: 'uppercase',
                color: color
            }
        }
    });
};

export default styles;