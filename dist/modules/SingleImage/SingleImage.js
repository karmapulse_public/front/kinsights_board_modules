import React from 'react';
import PropTypes from 'prop-types';

import styles from './SingleImageStyles';

var propTypes = {
    maxWidth: PropTypes.string,
    maxHeight: PropTypes.string,
    fields: PropTypes.object,
    color: PropTypes.string
};

var defaultProps = {
    maxWidth: 'none',
    maxHeight: 'none',
    fields: {},
    color: '#555'
};

var SingleImage = function SingleImage(props) {
    var img = props.fields.image.fields;
    return React.createElement(
        'div',
        Object.assign({ className: 'm-single-image' }, styles(props.color)),
        React.createElement(
            'div',
            { className: 'single-image__title' },
            React.createElement(
                'h3',
                null,
                props.fields.title
            )
        ),
        React.createElement('img', {
            alt: img.title,
            src: img.file.url ? img.file.url.replace('http:', 'https:') : '',
            style: {
                maxWidth: props.maxWidth,
                maxHeight: props.maxHeight
            }
        })
    );
};

SingleImage.propTypes = propTypes;
SingleImage.defaultProps = defaultProps;

export default SingleImage;