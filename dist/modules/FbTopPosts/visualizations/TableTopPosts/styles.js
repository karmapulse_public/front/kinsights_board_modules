import { css } from 'glamor';

var styles = function styles(color) {
    return css({
        ' .top-posts': {
            ' &__label': {
                display: 'flex',
                justifyContent: 'space-between',
                padding: '15px 20px 15px 20px',
                borderTop: '1px solid #dfdfdf',
                color: color,

                ' h3': {
                    fontSize: '13px',
                    fontWeight: 600
                }
            },
            '&__list': {
                counterReset: 'postsCounter',
                ' >li': {
                    position: 'relative',
                    padding: '0px 0px 30px 40px',
                    counterIncrement: 'postsCounter',
                    '&:first-of-type': {
                        marginTop: 15
                    },
                    '&::before': {
                        content: 'counter(postsCounter) "."',
                        position: 'absolute',
                        top: 5,
                        left: 15,
                        fontSize: 13
                    }
                },
                ' h4': {
                    fontSize: 13,
                    fontWeight: 500,
                    verticalAlign: 'middle',
                    textTransform: 'uppercase',
                    borderTop: '1px solid #dfdfdf',
                    ' span': {
                        display: 'inline-block',
                        verticalAlign: 'middle',
                        textTransform: 'capitalize',
                        padding: '6px 10px',
                        backgroundColor: '#d8d8d8',
                        marginRight: 10
                    }
                },
                ' a': {
                    padding: '25px 25px 35px 0px',
                    display: 'flex',
                    alignItems: 'center',
                    color: 'rgba(0,0,0,0.5)',
                    textDecoration: 'none',
                    transition: 'all .5s ease',
                    ' >div': {
                        minWidth: 40,
                        minHeight: 40,
                        marginRight: 20,
                        backgroundSize: 'cover',
                        backgroundColor: '#ececec',
                        backgroundPosition: 'center center'
                    },
                    ' p': {
                        fontSize: 13,
                        fontWeight: 500,
                        transition: 'all .2s ease'
                    },
                    ':hover': {
                        ' p': {
                            color: 'rgba(0,0,0,1)',
                            textDecoration: 'underline'
                        }
                    }
                }
            },
            '&__interactions': {
                display: 'flex',
                ' >div': {
                    ' >h5': {
                        fontSize: 13,
                        fontWeight: 500,
                        marginBottom: 5,
                        ' span': {
                            float: 'right',
                            color: 'rgba(0,0,0,1)'
                        }
                    },
                    '&:first-of-type': {
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'center',
                        paddingRight: 15,
                        borderRight: '1px solid #dfdfdf',
                        ' h5': {
                            color: 'rgba(0,0,0,0.5)',
                            width: 135
                        }
                    },
                    '&:nth-of-type(2)': {
                        position: 'relative',
                        paddingLeft: 15,
                        ' >h5': {
                            marginBottom: 10
                        },
                        ' ul': {
                            display: 'inline-block',
                            height: 15,
                            verticalAlign: 'middle',
                            ' li': {
                                float: 'left',
                                ' svg': {
                                    width: 20,
                                    height: 20,
                                    marginTop: -2,
                                    marginRight: -5
                                }
                            }
                        },
                        ' >span': {
                            marginLeft: 10,
                            verticalAlign: 'middle',
                            fontSize: 16,
                            lineHeight: '20px',
                            fontWeight: 500
                        },
                        '&:hover': {
                            ' .top-posts__tooltip': {
                                opacity: 1,
                                visibility: 'visible'
                            }
                        }
                    }
                }
            },
            '&__tooltip': {
                position: 'absolute',
                zIndex: 10,
                left: '40%',
                top: '50%',
                width: 150,
                padding: '13px 15px',
                boxShadow: '0 2px 8px 0 rgba(0, 0, 0, 0.2)',
                backgroundColor: '#fff',
                visibility: 'hidden',
                opacity: 0,
                transition: 'all .2s ease',
                transform: 'translate(0px, -50%)',
                ' svg': {
                    width: 13,
                    height: 13,
                    marginRight: 5,
                    verticalAlign: 'middle'
                },
                ' >h5': {
                    fontSize: 10,
                    fontWeight: 500,
                    '&:not(:last-child)': {
                        marginBottom: 7
                    }
                },
                ' span': {
                    verticalAlign: 'middle',
                    '&:first-of-type': {
                        color: 'rgba(0,0,0,0.5)',
                        marginRight: 5
                    }
                }
            }
        }
    });
};

export default styles;