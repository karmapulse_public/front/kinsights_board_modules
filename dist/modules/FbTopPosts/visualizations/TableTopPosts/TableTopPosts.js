import React from 'react';
import PropTypes from 'prop-types';
import toPairs from 'lodash/toPairs';
import NoDataModule from '../../../NoDataModule';

import { numberWithCommas } from '../../../../helpers/number';
import { LikeIcon, LoveIcon, HahaIcon, WowIcon, SadIcon, AngryIcon } from '../../../../helpers/icons/fb';
import styles from './styles';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666'
};

var TableTopPosts = function TableTopPosts(_ref) {
    var moduleConfig = _ref.moduleConfig,
        moduleData = _ref.moduleData,
        moduleColor = _ref.moduleColor;

    var mapDenominations = {
        photo: {
            name: 'Imagen',
            icon: ''
        },
        video: {
            name: 'Video',
            icon: '>'
        },
        link: {
            name: 'Enlace',
            icon: ''
        },
        text: {
            name: 'Texto',
            icon: '='
        },
        status: {
            name: 'Estatus',
            icon: ''
        }
    };

    var mapReactions = function mapReactions(reactions) {
        return {
            LIKE: {
                text: 'Me gusta',
                value: reactions.LIKE,
                icon: React.createElement(LikeIcon, null)
            },
            LOVE: {
                text: 'Me encanta',
                value: reactions.LOVE,
                icon: React.createElement(LoveIcon, null)
            },
            HAHA: {
                text: 'Me divierte',
                value: reactions.HAHA,
                icon: React.createElement(HahaIcon, null)
            },
            WOW: {
                text: 'Me asombra',
                value: reactions.WOW,
                icon: React.createElement(WowIcon, null)
            },
            SAD: {
                text: 'Me entristece',
                value: reactions.SAD,
                icon: React.createElement(SadIcon, null)
            },
            ANGRY: {
                text: 'Me enfada',
                value: reactions.ANGRY,
                icon: React.createElement(AngryIcon, null)
            }
        };
    };

    var formatBody = function formatBody(body) {
        if (body.length > 70) {
            return body.substring(0, 65) + '...';
        }
        return body;
    };

    var getReactionsIcons = function getReactionsIcons(reactions) {
        var reactionsOrdered = mapReactions(reactions);
        var reactionsArray = toPairs(reactionsOrdered);
        var reactionsFiltered = reactionsArray.filter(function (reaction) {
            return reaction[1].value !== 0;
        });
        return reactionsFiltered.map(function (reaction, index) {
            return React.createElement(
                'li',
                { key: index },
                reaction[1].icon
            );
        });
    };

    var renderReactionsTooltip = function renderReactionsTooltip(total, reactions) {
        var reactionsOrdered = mapReactions(reactions);
        var reactionsArray = toPairs(reactionsOrdered);
        var reactionsFiltered = reactionsArray.filter(function (reaction) {
            return reaction[1].value !== 0;
        });

        return React.createElement(
            'div',
            { className: 'top-posts__tooltip' },
            React.createElement(
                'h5',
                null,
                'Total: ',
                numberWithCommas(total)
            ),
            reactionsFiltered.map(function (reaction, index) {
                return React.createElement(
                    'h5',
                    { key: index },
                    reaction[1].icon,
                    React.createElement(
                        'span',
                        null,
                        reaction[1].text,
                        ':'
                    ),
                    React.createElement(
                        'span',
                        null,
                        numberWithCommas(reaction[1].value)
                    )
                );
            })
        );
    };

    var renderPostsList = function renderPostsList() {
        return React.createElement(
            'ul',
            { className: 'top-posts__list' },
            moduleData.data.map(function (post, index) {
                return React.createElement(
                    'li',
                    { key: index },
                    React.createElement(
                        'h4',
                        null,
                        React.createElement(
                            'span',
                            null,
                            mapDenominations[post.denomination].name
                        ),
                        'Interacciones: ',
                        numberWithCommas(post.interactions_total)
                    ),
                    React.createElement(
                        'a',
                        {
                            href: post.publication.media.url,
                            target: '_blank',
                            rel: 'noopener noreferrer',
                            className: 'top-posts__post'
                        },
                        React.createElement(
                            'div',
                            {
                                className: post.denomination,
                                style: { backgroundImage: 'url(' + (post.publication.media.cover ? post.publication.media.cover.replace('http:', 'https:') : '') + ')' }
                            },
                            mapDenominations[post.denomination].icon
                        ),
                        React.createElement(
                            'p',
                            null,
                            formatBody(post.publication.body)
                        )
                    ),
                    React.createElement(
                        'div',
                        { className: 'top-posts__interactions' },
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'h5',
                                null,
                                'Compartidas: ',
                                React.createElement(
                                    'span',
                                    null,
                                    numberWithCommas(post.shares_total)
                                )
                            ),
                            React.createElement(
                                'h5',
                                null,
                                'Comentarios: ',
                                React.createElement(
                                    'span',
                                    null,
                                    numberWithCommas(post.comments_total)
                                )
                            )
                        ),
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'h5',
                                null,
                                'Reacciones desglosadas'
                            ),
                            React.createElement(
                                'ul',
                                null,
                                getReactionsIcons(post.reactions)
                            ),
                            React.createElement(
                                'span',
                                null,
                                numberWithCommas(post.reactions_total)
                            ),
                            renderReactionsTooltip(post.reactions_total, post.reactions)
                        )
                    )
                );
            })
        );
    };

    return React.createElement(
        'div',
        Object.assign({ className: 'top-posts' }, styles(moduleColor)),
        React.createElement(
            'div',
            { className: 'top-posts__label' },
            React.createElement(
                'h3',
                null,
                moduleConfig.title
            )
        ),
        moduleData.total !== 0 ? renderPostsList() : React.createElement(NoDataModule, null)
    );
};

TableTopPosts.propTypes = propTypes;
TableTopPosts.defaultProps = defaultProps;

export default TableTopPosts;