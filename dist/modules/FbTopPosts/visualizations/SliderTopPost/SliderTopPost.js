import React from 'react';
import PropTypes from 'prop-types';
import toPairs from 'lodash/toPairs';
import Slider from 'kpulse_design_system/dist/Carousel';
import { faFacebookF } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import NoDataModule from '../../../NoDataModule';
import styles from './styles';

import Gallery from '../../../../helpers/customComponents/Gallery';
import Header from '../../../../helpers/customComponents/Header';
import BodyText from '../../../../helpers/customComponents/BodyText';
import Card from '../../../../helpers/customComponents/Card';
import Link from '../../../../helpers/customComponents/Link';

import { numberWithCommas } from '../../../../helpers/number';
import { LikeIcon, LoveIcon, HahaIcon, WowIcon, SadIcon, AngryIcon } from '../../../../helpers/icons/fb';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    noDataYet: PropTypes.func
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    noDataYet: null
};

var mapReactions = function mapReactions(reactions) {
    return {
        LIKE: {
            text: 'Me gusta',
            value: reactions.LIKE,
            icon: React.createElement(LikeIcon, null)
        },
        LOVE: {
            text: 'Me encanta',
            value: reactions.LOVE,
            icon: React.createElement(LoveIcon, null)
        },
        HAHA: {
            text: 'Me divierte',
            value: reactions.HAHA,
            icon: React.createElement(HahaIcon, null)
        },
        WOW: {
            text: 'Me asombra',
            value: reactions.WOW,
            icon: React.createElement(WowIcon, null)
        },
        SAD: {
            text: 'Me entristece',
            value: reactions.SAD,
            icon: React.createElement(SadIcon, null)
        },
        ANGRY: {
            text: 'Me enfada',
            value: reactions.ANGRY,
            icon: React.createElement(AngryIcon, null)
        }
    };
};

var formatBody = function formatBody(body) {
    if (body.length > 195) {
        return body.substring(0, 190) + '...';
    }
    return body;
};

var getReactionsIcons = function getReactionsIcons(reactions) {
    var reactionsOrdered = mapReactions(reactions);
    var reactionsArray = toPairs(reactionsOrdered);
    var reactionsFiltered = reactionsArray.filter(function (reaction) {
        return reaction[1].value !== 0;
    });
    return reactionsFiltered.map(function (reaction, index) {
        return React.createElement(
            'li',
            { key: index },
            reaction[1].icon
        );
    });
};

var renderReactionsTooltip = function renderReactionsTooltip(total, reactions) {
    var reactionsOrdered = mapReactions(reactions);
    var reactionsArray = toPairs(reactionsOrdered);
    var reactionsFiltered = reactionsArray.filter(function (reaction) {
        return reaction[1].value !== 0;
    });

    return React.createElement(
        'div',
        { className: 'card__tooltip' },
        reactionsFiltered.map(function (reaction, index) {
            return React.createElement(
                'h5',
                { key: index },
                reaction[1].icon,
                React.createElement(
                    'span',
                    null,
                    numberWithCommas(reaction[1].value)
                )
            );
        })
    );
};

var CarouselTopPosts = function CarouselTopPosts(_ref) {
    var moduleData = _ref.moduleData,
        moduleConfig = _ref.moduleConfig,
        moduleColor = _ref.moduleColor,
        noDataYet = _ref.noDataYet;

    var id = moduleConfig.fields.search_id === undefined ? '' : moduleConfig.fields.search_id;

    var renderContent = function renderContent() {
        return React.createElement(
            'div',
            { style: { position: 'relative' } },
            React.createElement(
                Slider,
                { width: '100%', height: '410px', dotsColor: '#b38e5d', arrowsColor: '#d4c19c', arrowsHeight: '20px' },
                moduleData.data.map(function (item, index) {
                    var mappedItem = Object.assign({}, item.publication, { source_type: 'facebook', rule_id: id, posted_time: item.publication.date });
                    return React.createElement(
                        'div',
                        { className: 'card', key: index },
                        React.createElement(
                            'div',
                            { className: 'card__number' },
                            React.createElement(
                                'p',
                                null,
                                index + 1
                            )
                        ),
                        React.createElement(
                            'div',
                            { className: 'card__data' },
                            React.createElement(
                                Card,
                                { key: index },
                                React.createElement(Header, { noSource: true, item: mappedItem, services: moduleConfig }),
                                React.createElement(BodyText, { text: formatBody(item.publication.body), type: item.denomination }),
                                function () {
                                    if (item.denomination === 'photo') {
                                        return React.createElement(Gallery, {
                                            items: [{ url: item.publication.media.cover }],
                                            height: '212px',
                                            width: '100%',
                                            type: 'facebook'
                                        });
                                    }
                                    if (item.denomination === 'video') {
                                        return React.createElement(Gallery, {
                                            items: [{ url: item.publication.media.cover }],
                                            height: '212px',
                                            width: '100%',
                                            type: 'facebook',
                                            video: true
                                        });
                                    }
                                    if (item.denomination === 'link') {
                                        return (
                                            // eslint-disable-next-line jsx-a11y/anchor-is-valid
                                            React.createElement(Link, {
                                                url: item.publication.media.url,
                                                imageUrl: item.publication.media.cover,
                                                title: item.publication.media.title,
                                                text: item.publication.body,
                                                type: item.denomination
                                            })
                                        );
                                    }
                                    return null;
                                }()
                            )
                        ),
                        React.createElement(
                            'div',
                            { className: 'card__interactions' },
                            React.createElement(
                                'h5',
                                null,
                                React.createElement(
                                    'span',
                                    null,
                                    numberWithCommas(item.shares_total)
                                ),
                                ' Compartidas'
                            ),
                            React.createElement(
                                'h5',
                                null,
                                React.createElement(
                                    'span',
                                    null,
                                    numberWithCommas(item.comments_total)
                                ),
                                ' Comentarios'
                            ),
                            React.createElement(
                                'div',
                                null,
                                React.createElement(
                                    'h5',
                                    null,
                                    numberWithCommas(item.reactions_total)
                                ),
                                React.createElement(
                                    'ul',
                                    null,
                                    getReactionsIcons(item.reactions)
                                ),
                                renderReactionsTooltip(item.reactions_total, item.reactions)
                            )
                        )
                    );
                })
            )
        );
    };

    return React.createElement(
        'div',
        Object.assign({ className: 'top-posts' }, styles),
        React.createElement(
            'div',
            { className: 'top-posts__label' },
            React.createElement(
                'h3',
                null,
                'PUBLICACIONES CON M\xC1S INTERACCI\xD3N'
            ),
            React.createElement(
                'div',
                null,
                React.createElement(FontAwesomeIcon, { icon: faFacebookF, color: 'white' })
            )
        ),
        function () {
            if (noDataYet !== null) {
                return noDataYet();
            }

            return moduleData.total !== 0 ? renderContent() : React.createElement(NoDataModule, null);
        }()
    );
};

CarouselTopPosts.propTypes = propTypes;
CarouselTopPosts.defaultProps = defaultProps;

export default CarouselTopPosts;