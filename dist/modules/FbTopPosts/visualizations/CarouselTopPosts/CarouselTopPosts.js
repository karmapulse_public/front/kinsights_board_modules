var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import toPairs from 'lodash/toPairs';
import moment from 'moment';
import NoDataModule from '../../../NoDataModule';

import { numberWithCommas } from '../../../../helpers/number';
import { LikeIcon, LoveIcon, HahaIcon, WowIcon, SadIcon, AngryIcon } from '../../../../helpers/icons/fb';
import { AngleLeft, AngleRight } from '../../../../helpers/icons/gob';
import styles from './styles';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    noDataYet: PropTypes.func
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    noDataYet: null
};

var mapDenominations = {
    photo: {
        name: 'Imagen',
        icon: ''
    },
    video: {
        name: 'Video',
        icon: ''
    },
    link: {
        name: 'Enlace',
        icon: ''
    },
    text: {
        name: 'Texto',
        icon: '='
    },
    status: {
        name: 'Estatus',
        icon: ''
    }
};

var mapReactions = function mapReactions(reactions) {
    return {
        LIKE: {
            text: 'Me gusta',
            value: reactions.LIKE,
            icon: React.createElement(LikeIcon, null)
        },
        LOVE: {
            text: 'Me encanta',
            value: reactions.LOVE,
            icon: React.createElement(LoveIcon, null)
        },
        HAHA: {
            text: 'Me divierte',
            value: reactions.HAHA,
            icon: React.createElement(HahaIcon, null)
        },
        WOW: {
            text: 'Me asombra',
            value: reactions.WOW,
            icon: React.createElement(WowIcon, null)
        },
        SAD: {
            text: 'Me entristece',
            value: reactions.SAD,
            icon: React.createElement(SadIcon, null)
        },
        ANGRY: {
            text: 'Me enfada',
            value: reactions.ANGRY,
            icon: React.createElement(AngryIcon, null)
        }
    };
};

var formatBody = function formatBody(body) {
    if (body.length > 70) {
        return body.substring(0, 65) + '...';
    }
    return body;
};

var getReactionsIcons = function getReactionsIcons(reactions) {
    var reactionsOrdered = mapReactions(reactions);
    var reactionsArray = toPairs(reactionsOrdered);
    var reactionsFiltered = reactionsArray.filter(function (reaction) {
        return reaction[1].value !== 0;
    });
    return reactionsFiltered.map(function (reaction, index) {
        return React.createElement(
            'li',
            { key: index },
            reaction[1].icon
        );
    });
};

var renderReactionsTooltip = function renderReactionsTooltip(total, reactions) {
    var reactionsOrdered = mapReactions(reactions);
    var reactionsArray = toPairs(reactionsOrdered);
    var reactionsFiltered = reactionsArray.filter(function (reaction) {
        return reaction[1].value !== 0;
    });

    return React.createElement(
        'div',
        { className: 'post__tooltip' },
        React.createElement(
            'h5',
            null,
            'Total: ',
            numberWithCommas(total)
        ),
        reactionsFiltered.map(function (reaction, index) {
            return React.createElement(
                'h5',
                { key: index },
                reaction[1].icon,
                React.createElement(
                    'span',
                    null,
                    reaction[1].text,
                    ':'
                ),
                React.createElement(
                    'span',
                    null,
                    numberWithCommas(reaction[1].value)
                )
            );
        })
    );
};

var CarouselTopPosts = function (_Component) {
    _inherits(CarouselTopPosts, _Component);

    function CarouselTopPosts(props) {
        _classCallCheck(this, CarouselTopPosts);

        var _this = _possibleConstructorReturn(this, (CarouselTopPosts.__proto__ || Object.getPrototypeOf(CarouselTopPosts)).call(this, props));

        _this.state = {
            postActive: 0
        };
        _this.numPosts = props.moduleData.data.length;
        _this.changeLeft = _this.changeLeft.bind(_this);
        _this.changeRight = _this.changeRight.bind(_this);
        return _this;
    }

    _createClass(CarouselTopPosts, [{
        key: 'changeLeft',
        value: function changeLeft() {
            if (this.state.postActive > 0) {
                this.setState({
                    postActive: this.state.postActive - 1
                });
            }
        }
    }, {
        key: 'changeRight',
        value: function changeRight() {
            if (this.state.postActive < this.numPosts - 1) {
                this.setState({
                    postActive: this.state.postActive + 1
                });
            }
        }
    }, {
        key: 'renderActivePost',
        value: function renderActivePost() {
            var post = this.props.moduleData.data[this.state.postActive];
            var postDate = moment(post.publication.date).format('DD MMM YY');

            return React.createElement(
                'div',
                { className: 'post' },
                React.createElement(
                    'h4',
                    null,
                    React.createElement(
                        'span',
                        null,
                        this.state.postActive + 1 + '.'
                    ),
                    React.createElement(
                        'span',
                        null,
                        mapDenominations[post.denomination].name
                    ),
                    React.createElement(
                        'span',
                        null,
                        postDate
                    )
                ),
                React.createElement(
                    'a',
                    {
                        href: post.publication.media.url,
                        target: '_blank',
                        rel: 'noopener noreferrer',
                        className: 'post__body'
                    },
                    React.createElement(
                        'div',
                        {
                            className: post.denomination,
                            style: { backgroundImage: 'url(' + (post.publication.media.cover ? post.publication.media.cover.replace('http:', 'https:') : '') + ')' }
                        },
                        mapDenominations[post.denomination].icon
                    ),
                    React.createElement(
                        'p',
                        null,
                        formatBody(post.publication.body)
                    )
                ),
                React.createElement(
                    'div',
                    { className: 'post__interactions' },
                    React.createElement(
                        'div',
                        null,
                        React.createElement(
                            'h5',
                            null,
                            'Compartidas: ',
                            React.createElement(
                                'span',
                                null,
                                numberWithCommas(post.shares_total)
                            )
                        ),
                        React.createElement(
                            'h5',
                            null,
                            'Comentarios: ',
                            React.createElement(
                                'span',
                                null,
                                numberWithCommas(post.comments_total)
                            )
                        )
                    ),
                    React.createElement(
                        'div',
                        null,
                        React.createElement(
                            'h5',
                            null,
                            'Reacciones desglosadas'
                        ),
                        React.createElement(
                            'ul',
                            null,
                            getReactionsIcons(post.reactions)
                        ),
                        React.createElement(
                            'span',
                            null,
                            numberWithCommas(post.reactions_total)
                        ),
                        renderReactionsTooltip(post.reactions_total, post.reactions)
                    )
                )
            );
        }
    }, {
        key: 'renderControls',
        value: function renderControls() {
            return React.createElement(
                'div',
                { className: 'controls' },
                React.createElement(
                    'button',
                    {
                        onClick: this.changeLeft
                    },
                    React.createElement(AngleLeft, {
                        size: '13px',
                        color: '#000',
                        opacity: this.state.postActive === 0 ? 0.2 : 1
                    })
                ),
                React.createElement(
                    'button',
                    {
                        onClick: this.changeRight
                    },
                    React.createElement(AngleRight, {
                        size: '13px',
                        color: '#000',
                        opacity: this.state.postActive === this.numPosts - 1 ? 0.2 : 1
                    })
                )
            );
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var _props = this.props,
                moduleConfig = _props.moduleConfig,
                moduleData = _props.moduleData,
                moduleColor = _props.moduleColor;

            return React.createElement(
                'div',
                Object.assign({ className: 'top-posts' }, styles(moduleColor)),
                React.createElement(
                    'div',
                    { className: 'top-posts__label' },
                    React.createElement(
                        'h3',
                        null,
                        moduleConfig.title
                    )
                ),
                function () {
                    if (_this2.props.noDataYet !== null) {
                        return _this2.props.noDataYet();
                    }

                    return moduleData.total !== 0 ? React.createElement(
                        'div',
                        { className: 'top-posts__content' },
                        _this2.renderActivePost(),
                        _this2.renderControls()
                    ) : React.createElement(NoDataModule, null);
                }()
            );
        }
    }]);

    return CarouselTopPosts;
}(Component);

CarouselTopPosts.propTypes = propTypes;
CarouselTopPosts.defaultProps = defaultProps;

export default CarouselTopPosts;