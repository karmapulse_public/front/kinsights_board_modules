import { css } from 'glamor';

var styles = function styles(color) {
    return css({
        ' .top-posts': {
            ' &__label': {
                display: 'flex',
                justifyContent: 'space-between',
                padding: '15px 20px 15px 20px',
                borderTop: '1px solid #dfdfdf',
                color: color,

                ' h3': {
                    fontSize: '13px',
                    fontWeight: 600
                }
            },
            '&__content': {
                padding: 30
            }
        },
        ' .post': {
            counterReset: 'postsCounter',
            position: 'relative',
            ' h4': {
                borderTop: '1px solid #dfdfdf',
                fontSize: 13,
                fontWeight: 500,
                verticalAlign: 'middle',
                textTransform: 'uppercase',
                ' span': {
                    display: 'inline-block',
                    verticalAlign: 'middle',
                    textTransform: 'capitalize',
                    backgroundColor: '#d8d8d8',
                    '&:first-of-type': {
                        backgroundColor: color,
                        color: '#fff',
                        width: 40,
                        height: 40,
                        lineHeight: '40px',
                        textAlign: 'center',
                        verticalAlign: 'top'
                    },
                    '&:nth-of-type(2)': {
                        padding: '5px 10px',
                        color: '#000'
                    },
                    '&:last-of-type': {
                        float: 'right',
                        backgroundColor: '#fff',
                        fontWeight: 700,
                        color: 'rgba(0,0,0,0.5)',
                        lineHeight: '23px',
                        textTransform: 'capitalize'
                    }
                }
            },
            ' a': {
                padding: '0px 0px 20px 0px',
                display: 'flex',
                alignItems: 'center',
                color: 'rgba(0,0,0,0.5)',
                textDecoration: 'none',
                transition: 'all .5s ease',
                ' >div': {
                    height: 40,
                    width: 40,
                    marginRight: 20,
                    backgroundSize: 'cover',
                    backgroundColor: '#ececec',
                    backgroundPosition: 'center center'
                },
                ' p': {
                    fontSize: 13,
                    fontWeight: 500,
                    transition: 'all .2s ease'
                },
                ':hover': {
                    ' p': {
                        color: 'rgba(0,0,0,1)',
                        textDecoration: 'underline'
                    }
                }
            },
            '&__interactions': {
                display: 'flex',
                alignItems: 'flex-start',
                ' >div': {
                    ' >h5': {
                        fontSize: 13,
                        fontWeight: 500,
                        marginBottom: 5,
                        ' span': {
                            display: 'block',
                            marginTop: 3,
                            color: 'rgba(0,0,0,1)'
                        }
                    },
                    '&:first-of-type': {
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'center',
                        paddingRight: 15,
                        ' h5': {
                            color: 'rgba(0,0,0,0.5)'
                        }
                    },
                    '&:nth-of-type(2)': {
                        position: 'relative',
                        padding: '5px 0px 5px 15px',
                        borderLeft: '1px solid #dfdfdf',
                        ' >h5': {
                            marginBottom: 10
                        },
                        ' ul': {
                            display: 'inline-block',
                            height: 15,
                            verticalAlign: 'middle',
                            ' li': {
                                float: 'left',
                                ' svg': {
                                    width: 20,
                                    height: 20,
                                    marginTop: -2,
                                    marginRight: -5
                                }
                            }
                        },
                        ' >span': {
                            marginLeft: 10,
                            verticalAlign: 'middle',
                            fontSize: 16,
                            lineHeight: '20px',
                            fontWeight: 500
                        },
                        '&:hover': {
                            ' .post__tooltip': {
                                opacity: 1,
                                visibility: 'visible'
                            }
                        }
                    }
                }
            },
            '&__tooltip': {
                position: 'absolute',
                zIndex: 10,
                left: '40%',
                top: '50%',
                width: 150,
                padding: '13px 15px',
                boxShadow: '0 2px 8px 0 rgba(0, 0, 0, 0.2)',
                backgroundColor: '#fff',
                visibility: 'hidden',
                opacity: 0,
                transition: 'all .2s ease',
                transform: 'translate(0px, -50%)',
                ' svg': {
                    width: 13,
                    height: 13,
                    marginRight: 5,
                    verticalAlign: 'middle'
                },
                ' >h5': {
                    fontSize: 10,
                    fontWeight: 500,
                    '&:not(:last-child)': {
                        marginBottom: 7
                    }
                },
                ' span': {
                    verticalAlign: 'middle',
                    '&:first-of-type': {
                        color: 'rgba(0,0,0,0.5)',
                        marginRight: 5
                    }
                }
            }
        },
        ' .controls': {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-end',
            ' button': {
                width: 43,
                height: 43,
                border: 'none',
                backgroundColor: '#eee',
                padding: 0,
                '&:hover': {
                    cursor: 'pointer'
                },
                '&:first-child': {
                    marginRight: 5
                }
            }
        }
    });
};

export default styles;