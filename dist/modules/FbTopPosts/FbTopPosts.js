var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import fetchFbTopPosts from './FbTopPostsDataFlow';

import LoadingModule from '../LoadingModule';
import NoDataYet from '../NoDataYet';
import TableTopPosts from './visualizations/TableTopPosts';
import CarouselTopPosts from './visualizations/CarouselTopPosts';
import SliderTopPost from './visualizations/SliderTopPost';

var propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    dateRange: PropTypes.object,
    services: PropTypes.object,
    showNoDataYet: PropTypes.bool
};

var defaultProps = {
    fields: {},
    side: 'left',
    color: '#444',
    dateRange: {},
    services: {},
    showNoDataYet: false
};

var FbTopPosts = function (_Component) {
    _inherits(FbTopPosts, _Component);

    function FbTopPosts(props) {
        _classCallCheck(this, FbTopPosts);

        var _this = _possibleConstructorReturn(this, (FbTopPosts.__proto__ || Object.getPrototypeOf(FbTopPosts)).call(this, props));

        _this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color
        };
        return _this;
    }

    _createClass(FbTopPosts, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            this.updateDataFlow(this.props);
        }
    }, {
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProps) {
            this.updateDataFlow(nextProps);
        }
    }, {
        key: 'updateDataFlow',
        value: function updateDataFlow(props) {
            var _this2 = this;

            var params = {
                rule_id: props.fields.search_id,
                initial_date: props.dateRange.startDate.toISOString(),
                final_date: props.dateRange.endDate.toISOString(),
                module_id: 'module_fb_top_posts',
                top: props.top
            };

            fetchFbTopPosts(props.services.facebook, params).subscribe(function (data) {
                _this2.setState({
                    moduleConfig: props,
                    moduleData: data,
                    moduleLoading: false,
                    moduleSide: props.side,
                    moduleColor: props.color
                });
            }, function (error) {
                console.log(error);
            }, null);
        }
    }, {
        key: 'renderByViz',
        value: function renderByViz() {
            var _state = this.state,
                moduleConfig = _state.moduleConfig,
                moduleData = _state.moduleData;
            var _props = this.props,
                _props$dateRange = _props.dateRange,
                startDate = _props$dateRange.startDate,
                endDate = _props$dateRange.endDate,
                showNoDataYet = _props.showNoDataYet;

            var renderNoDataYet = null;

            if (showNoDataYet && moment().diff(endDate, 'minutes') <= 0 && endDate.diff(startDate, 'days') <= 1) {
                renderNoDataYet = function renderNoDataYet() {
                    return React.createElement(NoDataYet, null);
                };
            }
            if (moduleData) {
                var visualizations = {
                    table_top_posts: function table_top_posts(params) {
                        return React.createElement(TableTopPosts, params);
                    },
                    carousel_top_posts: function carousel_top_posts(params) {
                        return React.createElement(CarouselTopPosts, Object.assign({}, params, { noDataYet: renderNoDataYet }));
                    },
                    slider_top_posts: function slider_top_posts(params) {
                        return React.createElement(SliderTopPost, Object.assign({}, params, { noDataYet: renderNoDataYet }));
                    }
                };

                return visualizations[moduleConfig.fields.visualization](this.state);
            }

            return React.createElement(LoadingModule, null);
        }
    }, {
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'm-fb-top-posts' },
                this.renderByViz()
            );
        }
    }]);

    return FbTopPosts;
}(Component);

FbTopPosts.propTypes = propTypes;
FbTopPosts.defaultProps = defaultProps;

export default FbTopPosts;