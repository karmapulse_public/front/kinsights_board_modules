import { Observable } from 'rxjs';
import { requestPost } from '../../helpers/http';

export default (function (url, params) {
    return Observable.create(function (observer) {
        Observable.fromPromise(requestPost(url, 'POST', params)).subscribe(function (data) {
            observer.next(data.json);
        }, function (error) {
            observer.error(error);
        });
    });
});