var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import 'moment-timezone';
import setDates from '../../helpers/setDates';
import getIntervalByDates from '../../helpers/getIntervalByDates';
import fetchTopicSight from './TopSightDataFlow';

import LoadingModule from '../LoadingModule';
import AreaItem from './visualizations/AreaItem';

var propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    dateRange: PropTypes.object,
    services: PropTypes.object,
    topic: PropTypes.object,
    extraFunction: PropTypes.func,
    goToURL: PropTypes.func
};

var defaultProps = {
    fields: {},
    side: 'left',
    color: '#444',
    dateRange: {},
    services: {},
    extraFunction: function extraFunction() {
        return {};
    },
    topic: {},
    goToURL: function goToURL() {
        return {};
    }
};

var getData = function getData(dateRange, searchId, serviceRecipesTw, thenfunction) {
    // const date = moment().format('YYYY-MM-DD hh:mm:ss');
    // const utcOffset = moment.tz(date, tz).isDST() ? '-05:00' : '-06:00';
    // const startDate = moment().subtract(1, 'days').utcOffset(utcOffset).toISOString();
    // const endDate = moment().utcOffset(utcOffset).toISOString();

    var params = Object.assign({
        recipe_id: 'module_tw_histogram',
        rule_id: searchId
    }, setDates(dateRange), {
        filters: {
            interval: 'day'
        }
    });

    params.filters.interval = getIntervalByDates(moment(params.initial_date), moment(params.final_date));

    fetchTopicSight(serviceRecipesTw, params).subscribe(function (resultData) {
        thenfunction(resultData);
    }, function (error) {
        console.log(error);
    }, null);
};

var TopicSight = function (_Component) {
    _inherits(TopicSight, _Component);

    function TopicSight(props) {
        _classCallCheck(this, TopicSight);

        var _this = _possibleConstructorReturn(this, (TopicSight.__proto__ || Object.getPrototypeOf(TopicSight)).call(this, props));

        _this.state = {
            moduleConfig: Object.assign({}, props.fields, props.topic),
            moduleData: {},
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color
        };
        return _this;
    }

    _createClass(TopicSight, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var _this2 = this;

            getData(this.props.dateRange, this.state.moduleConfig.search_id, this.props.services.twitter, function (data) {
                _this2.props.extraFunction(data);
                _this2.setState({
                    moduleData: data,
                    moduleLoading: false
                });
            });
        }
    }, {
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProps) {
            var _this3 = this;

            if (nextProps.dateRange !== this.props.dateRange) {
                getData(nextProps.dateRange, this.state.moduleConfig.search_id, this.props.services.twitter, function (data) {
                    _this3.props.extraFunction(data);
                    _this3.setState({
                        moduleData: data
                    });
                });
            }
        }
    }, {
        key: 'renderByViz',
        value: function renderByViz() {
            var _state = this.state,
                moduleConfig = _state.moduleConfig,
                moduleData = _state.moduleData;

            if (moduleData) {
                var visualizations = {
                    area_chart: function area_chart(params) {
                        return React.createElement(AreaItem, params);
                    }
                };
                var mergeProps = Object.assign({}, this.state, { goToURL: this.props.goToURL });
                return visualizations[this.props.view || moduleConfig.visualization](mergeProps);
            }

            return React.createElement(LoadingModule, null);
        }
    }, {
        key: 'render',
        value: function render() {
            return this.renderByViz();
        }
    }]);

    return TopicSight;
}(Component);

TopicSight.propTypes = propTypes;
TopicSight.defaultProps = defaultProps;

export default TopicSight;