import React from 'react';
import PropTypes from 'prop-types';
import AreaChart from '../../Charts/ChartArea';
import { numberWithCommas } from '../../../helpers/number';
import RightArrowIcon from '../../../helpers/icons/seg/rightArrowIcon';
import styles from './AreaItemStyles';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleSide: PropTypes.string,
    moduleColor: PropTypes.string,
    goToURL: PropTypes.func
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleSide: 'left',
    moduleColor: '#666',
    goToURL: function goToURL() {
        return {};
    }
};

var AreaItem = function AreaItem(_ref) {
    var moduleConfig = _ref.moduleConfig,
        moduleData = _ref.moduleData,
        moduleSide = _ref.moduleSide,
        moduleColor = _ref.moduleColor,
        goToURL = _ref.goToURL;
    var title = moduleConfig.title,
        slug = moduleConfig.slug;

    var dataKeys = [{ key: 'total', color: moduleColor }];

    return React.createElement(
        'div',
        Object.assign({
            className: 'topicSight__topic',
            onClick: function onClick() {
                return goToURL(slug.substring(1));
            }
        }, styles(moduleColor)),
        React.createElement(
            'h1',
            null,
            title,
            React.createElement(RightArrowIcon, null)
        ),
        React.createElement(
            'div',
            { className: 'topicSight__topic__chart' },
            React.createElement(
                'h2',
                null,
                numberWithCommas(moduleData.total || 0)
            ),
            React.createElement(
                'span',
                null,
                'Menciones'
            ),
            React.createElement(
                'div',
                null,
                React.createElement(AreaChart, {
                    data: moduleData.intervals,
                    dataKeys: dataKeys,
                    hasAxis: false,
                    hasGrid: false,
                    hasLegend: false
                })
            )
        )
    );
};

AreaItem.propTypes = propTypes;
AreaItem.defaultProps = defaultProps;

export default AreaItem;