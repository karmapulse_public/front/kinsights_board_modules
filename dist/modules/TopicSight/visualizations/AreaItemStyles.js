import { css } from 'glamor';

var AreaTimeStyles = function AreaTimeStyles(color) {
    return css({
        '.topicSight__topic': {
            position: 'relative',
            padding: '20px',
            minWidth: 310,
            cursor: 'pointer',
            textDecoration: 'none',
            ':after': {
                content: "''",
                position: 'absolute',
                bottom: '0px',
                left: '0px',
                width: '100%',
                height: '90px'
            },
            ':hover': {
                ' h2': {
                    color: color
                },
                ' h1 svg': {
                    width: 'auto',
                    opacity: 1,
                    visibility: 'visible'
                }
            },
            ' h2': {
                transition: 'all 0.25s ease'
            },
            ' h1': {
                textTransform: 'uppercase',
                ' svg': {
                    width: 0,
                    opacity: 0,
                    visibility: 'hidden',
                    transition: 'all 0.25s ease'
                }
            }
        },
        ' .topicSight__topic__chart': {
            position: 'relative',
            height: 140,
            ' >div': {
                width: '100%',
                height: '73px',
                position: 'absolute',
                bottom: '-5px',
                left: '0px',
                overflow: 'hidden'
            },
            ' .recharts-responsive-container': {
                width: '104% !important'
            }
        }
    });
};

export default AreaTimeStyles;