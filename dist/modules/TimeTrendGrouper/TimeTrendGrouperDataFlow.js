import { Observable } from 'rxjs';
import { fetchXHR } from '../../helpers/http';
import paddingDates from '../../helpers/paddingDates';

export default (function (url, params) {
    return Observable.create(function (observer) {
        Observable.fromPromise(fetchXHR(url, 'GET', params)).subscribe(function (data) {
            if (data.json.response.total !== 0) {
                observer.next({
                    intervals: paddingDates(data.json.response.intervals, params),
                    total: data.json.response.total
                });
            } else {
                observer.next({
                    intervals: [],
                    total: 0
                });
            }
        }, function (error) {
            observer.error(error);
        });
    });
});