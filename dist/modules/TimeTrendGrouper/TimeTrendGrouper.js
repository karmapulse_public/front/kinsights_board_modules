var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import getIntervalByDates from '../../helpers/getIntervalByDates';
import setDates from '../../helpers/setDates';
import fetchTimeTrendGrouper from './TimeTrendGrouperDataFlow';
import LoadingModule from '../LoadingModule';
import Lines from './visualizations/Line';

var propTypes = {
    fields: PropTypes.object,
    labels: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    dateRange: PropTypes.object,
    services: PropTypes.object,
    data: PropTypes.array,
    filters: PropTypes.object,
    withCache: PropTypes.bool
};

var defaultProps = {
    fields: {},
    labels: {
        x: '',
        y: ''
    },
    side: 'left',
    color: '#444',
    dateRange: {},
    services: {},
    data: [],
    filters: {},
    withCache: true
};

var mapColors = function mapColors(data) {
    var colors = {};
    data.forEach(function (item) {
        colors[item.title] = item.color;
    });
    return colors;
};

var mapData = function mapData(data) {
    var maxIndex = 0;
    data.forEach(function (item, index) {
        maxIndex = item.intervals.length < data[maxIndex].intervals.length ? maxIndex : index;
    });

    return data[maxIndex].intervals.map(function (item, index) {
        var values = function () {
            var objectResult = {};
            data.forEach(function (elto) {
                objectResult[elto.title || 'total'] = elto.intervals[index] ? elto.intervals[index].total : 0;
            });
            return objectResult;
        }();
        return Object.assign({
            key: moment(item.date).format()
        }, values);
    });
};

var TimeTrendGrouper = function (_Component) {
    _inherits(TimeTrendGrouper, _Component);

    function TimeTrendGrouper(props) {
        _classCallCheck(this, TimeTrendGrouper);

        var _this = _possibleConstructorReturn(this, (TimeTrendGrouper.__proto__ || Object.getPrototypeOf(TimeTrendGrouper)).call(this, props));

        _this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color
        };
        return _this;
    }

    _createClass(TimeTrendGrouper, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var _this2 = this;

            var type = this.state.moduleConfig.type;
            var dateRange = this.props.dateRange;

            if (type === 'agrupador') {
                if (this.props.data.length) {
                    this.setState({
                        moduleData: mapData(this.props.data),
                        colors: mapColors(this.props.data),
                        moduleLoading: false,
                        intervalType: getIntervalByDates(moment(dateRange.startDate), moment(dateRange.endDate))
                    });
                }
            } else if (type === 'simple') {
                var params = Object.assign({
                    recipe_id: 'module_tw_histogram',
                    rule_id: this.state.moduleConfig.search_id
                }, setDates(this.props.dateRange, this.props.withCache), {
                    filters: Object.assign({}, this.props.filters)
                });

                params.filters.interval = getIntervalByDates(moment(params.initial_date), moment(params.final_date));
                fetchTimeTrendGrouper(this.props.services.twitter, params).subscribe(function (data) {
                    _this2.setState({
                        moduleData: mapData([data]),
                        moduleLoading: false,
                        colors: { total: _this2.props.color },
                        intervalType: params.filters.interval
                    });
                }, function (error) {
                    console.log(error);
                }, null);
            }
        }
    }, {
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProps) {
            var _this3 = this;

            var type = this.state.moduleConfig.type;

            if (type === 'agrupador') {
                var dateRange = nextProps.dateRange;

                if (nextProps.data.length) {
                    this.setState({
                        moduleData: mapData(nextProps.data),
                        colors: mapColors(nextProps.data),
                        moduleLoading: false,
                        intervalType: getIntervalByDates(moment(dateRange.startDate), moment(dateRange.endDate))
                    });
                }
            } else if (type === 'simple') {
                var params = Object.assign({
                    recipe_id: 'module_tw_histogram',
                    rule_id: this.state.moduleConfig.search_id
                }, setDates(nextProps.dateRange, this.props.withCache), {
                    filters: Object.assign({}, nextProps.filters),
                    noUtc: true
                });
                params.filters.interval = getIntervalByDates(moment(params.initial_date), moment(params.final_date));

                fetchTimeTrendGrouper(this.props.services.twitter, params).subscribe(function (data) {
                    _this3.setState({
                        moduleData: mapData([data]),
                        colors: { total: _this3.props.color },
                        moduleLoading: false,
                        intervalType: params.filters.interval
                    });
                }, function (error) {
                    console.log(error);
                }, null);
            }
        }
    }, {
        key: 'renderByViz',
        value: function renderByViz() {
            var _this4 = this;

            var _state = this.state,
                moduleConfig = _state.moduleConfig,
                moduleData = _state.moduleData,
                intervalType = _state.intervalType;


            if (moduleData) {
                var visualizations = {
                    lines: function lines(params) {
                        return React.createElement(Lines, Object.assign({}, params, {
                            labels: _this4.props.labels,
                            intervalType: intervalType
                        }));
                    }
                };
                return visualizations[this.props.view || moduleConfig.visualization](this.state);
            }

            return React.createElement(LoadingModule, null);
        }
    }, {
        key: 'render',
        value: function render() {
            return this.renderByViz();
        }
    }]);

    return TimeTrendGrouper;
}(Component);

TimeTrendGrouper.propTypes = propTypes;
TimeTrendGrouper.defaultProps = defaultProps;

export default TimeTrendGrouper;