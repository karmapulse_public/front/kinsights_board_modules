import React from 'react';
import PropTypes from 'prop-types';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer, Label } from 'recharts';

import AxisX from '../../Charts/CustomComponents/AxisX';
import AxisY from '../../Charts/CustomComponents/AxisY';
import TooltipCustom from '../../Charts/CustomComponents/TooltipCustom';
import NoDataModule from '../../NoDataModule';

var propTypes = {
    moduleConfig: PropTypes.object,
    labels: PropTypes.object,
    moduleData: PropTypes.array,
    moduleSide: PropTypes.string,
    moduleColor: PropTypes.string,
    colors: PropTypes.object,
    noUtc: PropTypes.bool,
    intervalType: PropTypes.string
};

var defaultProps = {
    moduleConfig: {},
    labels: {
        x: '',
        y: ''
    },
    moduleData: [],
    moduleSide: 'left',
    moduleColor: '#666',
    colors: {},
    noUtc: false,
    intervalType: 'hour'
};

var LinesChart = function LinesChart(props) {
    var renderTooltip = function renderTooltip(innerProps) {
        return React.createElement(TooltipCustom, Object.assign({}, innerProps, {
            noUtc: true
        }));
    };

    var renderLine = function renderLine() {
        var moduleData = props.moduleData,
            colors = props.colors,
            labels = props.labels;


        return React.createElement(
            ResponsiveContainer,
            null,
            React.createElement(
                LineChart,
                {
                    data: moduleData,
                    margin: { top: 30, right: 30, left: 0, bottom: 25 }
                },
                React.createElement(
                    XAxis,
                    {
                        dataKey: 'key',
                        tickLine: false,
                        stroke: 'rgba(230,236,240, 0.15)',
                        tick: React.createElement(AxisX, { color: '#FFF', intervalType: props.intervalType, noUtc: true })
                    },
                    React.createElement(Label, { value: labels.x, className: 'column-data__label', offset: -7, position: 'insideBottom' })
                ),
                React.createElement(
                    YAxis,
                    {
                        axisLine: false,
                        tick: React.createElement(AxisY, { color: '#FFF', style: { fontSize: '0.625em' } })
                    },
                    React.createElement(Label, { value: labels.y, className: 'column-data__label', offset: -25, angle: -90, position: 'left' })
                ),
                React.createElement(CartesianGrid, { vertical: false }),
                React.createElement(Tooltip, { content: renderTooltip, intervalType: props.intervalType }),
                Object.keys(moduleData[0]).map(function (item) {
                    if (item !== 'key') {
                        return React.createElement(Line, {
                            type: 'linear',
                            strokeWidth: 2,
                            key: item,
                            dataKey: item,
                            stroke: colors[item],
                            dot: true,
                            activeDot: { stroke: colors[item], strokeWidth: 0 }
                        });
                    }
                    return null;
                })
            )
        );
    };

    var renderLineBasic = function renderLineBasic() {
        return React.createElement(
            'div',
            { className: 'line-chart' },
            React.createElement(
                'div',
                { className: 'line-chart__label' },
                React.createElement(
                    'h3',
                    null,
                    props.moduleConfig.title
                )
            ),
            function () {
                if (props.moduleData.length === 0) {
                    return React.createElement(
                        'div',
                        { className: 'noData' },
                        React.createElement(NoDataModule, null)
                    );
                }
                return React.createElement(
                    'div',
                    { className: 'line-chart__chart' },
                    React.createElement(
                        'div',
                        { className: 'chart-line' },
                        renderLine()
                    )
                );
            }()
        );
    };

    return renderLineBasic();
};

LinesChart.propTypes = propTypes;
LinesChart.defaultProps = defaultProps;

export default LinesChart;