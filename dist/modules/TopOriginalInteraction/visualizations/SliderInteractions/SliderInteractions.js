import React from 'react';
import PropTypes from 'prop-types';
import { faTwitter } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { numberWithCommas } from '../../../../helpers/number';
import styles from './styles';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string
};

var defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleColor: '#666'
};

var TotalNumbers = function TotalNumbers(_ref) {
    var moduleData = _ref.moduleData,
        moduleConfig = _ref.moduleConfig,
        moduleColor = _ref.moduleColor;

    var interactionTotal = moduleData.total;
    var color = '#6ab9dc';
    return React.createElement(
        'div',
        Object.assign({ className: 'list-interactions' }, styles(moduleColor)),
        React.createElement(
            'div',
            { className: 'list-interactions__label' },
            React.createElement(
                'h3',
                null,
                'INTERACCIONES TWITTER'
            ),
            React.createElement(
                'div',
                null,
                React.createElement(FontAwesomeIcon, { icon: faTwitter, color: 'white' })
            )
        ),
        React.createElement(
            'div',
            { className: 'list-interactions__content' },
            React.createElement(
                'h4',
                null,
                React.createElement(
                    'span',
                    null,
                    numberWithCommas(interactionTotal)
                ),
                React.createElement(
                    'p',
                    null,
                    'Totales '
                )
            ),
            React.createElement(
                'div',
                { style: { position: 'relative', marginTop: '31px' } },
                React.createElement(
                    'div',
                    { className: 'slider__card' },
                    React.createElement(
                        'h5',
                        null,
                        React.createElement(
                            'span',
                            null,
                            numberWithCommas(moduleData.total_favorites)
                        ),
                        React.createElement(
                            'p',
                            null,
                            'Me gusta'
                        )
                    ),
                    React.createElement(
                        'h5',
                        null,
                        React.createElement(
                            'span',
                            null,
                            numberWithCommas(moduleData.total_retweets)
                        ),
                        React.createElement(
                            'p',
                            null,
                            'Retweets'
                        )
                    )
                )
            )
        )
    );
};

TotalNumbers.propTypes = propTypes;
TotalNumbers.defaultProps = defaultProps;

export default TotalNumbers;