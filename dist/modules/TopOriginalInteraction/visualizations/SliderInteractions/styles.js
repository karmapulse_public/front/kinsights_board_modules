import { css } from 'glamor';

var styles = function styles(color) {
    return css({
        width: '100%',
        ' h4': {
            fontSize: 14,
            display: 'flex',
            color: '#4e232e',
            minHeight: '61px',
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'column',
            position: 'relative',
            marginTop: '10px',
            ' span': {
                fontSize: 18,
                fontWeight: 700,
                marginBottom: '5px'
            },
            ' p': {
                opacity: 0.5
            },
            ' &::after': {
                content: '""',
                position: 'absolute',
                bottom: 0,
                left: '50%',
                width: '85%',
                height: 1,
                backgroundColor: '#b38e5d',
                opacity: 0.5,
                transform: 'translate(-50%, 0)'
            }
        },

        ' .list-interactions': {
            ' &__label': {
                display: 'flex',
                paddingLeft: '18px',
                alignItems: 'center',
                justifyContent: 'space-between',
                borderBottom: '1px solid rgba(212, 193, 156, 0.5)',
                ' h3': {
                    fontSize: '14px',
                    fontWeight: 700,
                    color: '#b38e5d'
                },
                ' div': {
                    width: '44px',
                    height: '44px',
                    display: 'flex',
                    alignItems: 'center',
                    background: '#00acee',
                    justifyContent: 'center'
                }
            },
            ' &__content': {
                padding: '0px 23px',
                ' h5': {
                    fontSize: 12,
                    display: 'flex',
                    color: '#4e232e',
                    alignItems: 'center',
                    justifyContent: 'center',
                    flexDirection: 'column',
                    borderBottom: 'none',
                    ' span': {
                        fontSize: 14,
                        fontWeight: 700,
                        marginBottom: '1px'
                    },
                    ' p': {
                        opacity: 0.5
                    }
                },
                ' .slider': {
                    width: '86%',
                    ' div': {
                        width: '100%'
                    }
                },
                ' .controls': {
                    ' button': {
                        background: 'transparent',
                        ' svg': {
                            width: 13,
                            height: 23.5,
                            color: '#b38e5d',
                            opacity: 0.5
                        }
                    }
                }
            }
        },
        ' .slider__card': {
            display: 'flex',
            justifyContent: 'space-around',
            ' > h5 > p': {
                marginTop: 5
            }
        }
    });
};

export default styles;