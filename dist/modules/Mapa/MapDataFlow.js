import { Observable } from 'rxjs';
import { fetchXHR } from '../../helpers/http';

export default (function (url, params) {
    return Observable.create(function (observer) {
        Observable.fromPromise(fetchXHR(url, 'GET', params)).subscribe(function (data) {
            observer.next({
                moduleData: { dots: data.json.response.data }
            });
        }, function (error) {
            observer.error(error);
        });
    });
});

export var fetchBoundingBox = function fetchBoundingBox(url, params) {
    return Observable.create(function (observer) {
        Observable.fromPromise(fetchXHR(url, 'GET', params)).subscribe(function (response) {
            var bBox = response.json.response;
            if (!bBox || Object.keys(bBox).length === 0) {
                observer.next({
                    boundingBox: {},
                    moduleLoading: false
                });
            } else {
                var bottom_right = bBox.bottom_right,
                    top_left = bBox.top_left;
                /*
                    Se hace una suma de 2 para agregar un padding al componente del
                    mapa y así, no queden los punto cortados por el tamaño del mapa
                */

                var boundingBox = {
                    _sw: {
                        lng: top_left.lon - 2,
                        lat: bottom_right.lat - 2
                    },
                    _ne: {
                        lng: bottom_right.lon + 2,
                        lat: top_left.lat + 2
                    }
                };
                var moduleLoading = true;
                if (top_left.lon === bottom_right.lon && top_left.lat === bottom_right.lat) {
                    boundingBox = {};
                    moduleLoading = false;
                }
                observer.next({
                    boundingBox: boundingBox,
                    moduleLoading: moduleLoading
                });
            }
        }, function (error) {
            observer.error(error);
        });
    });
};