import { css } from 'glamor';

var styles = function styles(color) {
    return css({
        width: '100%',
        color: color,
        ' .dot-map__label': {
            '> h3': {
                textAlign: 'left',
                textTransform: 'uppercase'
            }
        },
        ' .dot-map__chart': {
            position: 'relative',
            padding: '15px',
            overflow: 'hidden',
            ' >div': {
                margin: '0px auto'
            },
            '.no-map .no-data-module': {
                position: 'absolute',
                top: 0,
                left: 0,
                zIndex: 10
            }
        }
    });
};

export default styles;