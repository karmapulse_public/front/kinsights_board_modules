var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import isEmpty from 'lodash/isEmpty';
import styles from './dotMapsStyles';
import NoDataModule from '../../NoDataModule';
import Map from '../../Charts/Map';
import Dot from '../../../helpers/icons/dotMapIcon';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    boundingBox: PropTypes.object,
    getData: PropTypes.func
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    boundingBox: {},
    getData: function getData() {}
};

var DotMapa = function (_Component) {
    _inherits(DotMapa, _Component);

    function DotMapa(props) {
        _classCallCheck(this, DotMapa);

        var _this = _possibleConstructorReturn(this, (DotMapa.__proto__ || Object.getPrototypeOf(DotMapa)).call(this, props));

        _this.state = {
            boundingBox: null,
            width: props.moduleConfig.width
        };
        _this.contenedor = null;
        _this.mapRef = null;
        _this.viewportTimer = null;
        _this.changeWidth = _this.changeWidth.bind(_this);
        _this.onViewportChange = _this.onViewportChange.bind(_this);
        return _this;
    }

    _createClass(DotMapa, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var _this2 = this;

            window.addEventListener('resize', function () {
                _this2.changeWidth(_this2.contenedor.clientWidth);
            });
            this.changeWidth(this.contenedor.clientWidth);
        }
    }, {
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProps) {
            var _this3 = this;

            if (nextProps.boundingBox !== this.props.boundingBox) {
                var _nextProps$boundingBo = nextProps.boundingBox,
                    _sw = _nextProps$boundingBo._sw,
                    _ne = _nextProps$boundingBo._ne;

                if (_sw && _ne) {
                    var bounds = {
                        top_left: {
                            lon: _sw.lng,
                            lat: _ne.lat
                        },
                        bottom_right: {
                            lon: _ne.lng,
                            lat: _sw.lat
                        }
                    };
                    this.setState({
                        boundingBox: [[_sw.lng, _sw.lat], [_ne.lng, _ne.lat]]
                    }, function () {
                        _this3.props.getData(_this3.mapRef, bounds);
                    });
                } else {
                    this.setState({
                        boundingBox: []
                    });
                }
            }
            if (nextProps.moduleData !== this.props.moduleData) {
                this.setState({
                    module: module
                });
            }
        }
    }, {
        key: 'renderIcon',
        value: function renderIcon(color, data) {
            // eslint-disable-line
            return Dot({ // eslint-disable-line
                color: color,
                value: data.value,
                styles: {
                    fontFamily: 'Roboto',
                    fontSize: '14px',
                    fontWeight: 900,
                    letterSpacing: '0.5px',
                    fill: '#ffffff'
                }
            });
        }
    }, {
        key: 'onViewportChange',
        value: function onViewportChange(viewport) {
            var _this4 = this;

            clearTimeout(this.viewportTimer);
            this.viewportTimer = setTimeout(function () {
                var bounding_box = viewport.bounding_box;
                var top_left = bounding_box.top_left,
                    bottom_right = bounding_box.bottom_right;

                _this4.props.getData(_this4.mapRef, bounding_box);
                _this4.setState({
                    boundingBox: [[top_left.lon, bottom_right.lat], [bottom_right.lon, top_left.lat]]
                });
            }, 100);
        }
    }, {
        key: 'changeWidth',
        value: function changeWidth(newWidth) {
            this.setState({
                width: newWidth - 30
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var _this5 = this;

            var _props = this.props,
                moduleData = _props.moduleData,
                moduleConfig = _props.moduleConfig,
                moduleColor = _props.moduleColor;
            var _state = this.state,
                boundingBox = _state.boundingBox,
                width = _state.width;
            var _moduleConfig$title = moduleConfig.title,
                title = _moduleConfig$title === undefined ? 'TOP DE TEMAS' : _moduleConfig$title,
                height = moduleConfig.height;
            var dots = moduleData.dots;

            return React.createElement(
                'div',
                Object.assign({
                    className: 'dot-map'
                }, styles(moduleColor), {
                    ref: function ref(container) {
                        if (container) _this5.contenedor = container;
                    }
                }),
                React.createElement(
                    'div',
                    { className: 'dot-map__label' },
                    React.createElement(
                        'h3',
                        null,
                        title
                    )
                ),
                React.createElement(
                    'div',
                    {
                        className: 'dot-map__chart ' + (isEmpty(boundingBox) ? 'no-map' : '')
                    },
                    isEmpty(boundingBox) ? React.createElement(NoDataModule, null) : null,
                    React.createElement(Map, {
                        ref: function ref(map) {
                            if (map) _this5.mapRef = map;
                        },
                        token: 'pk.eyJ1Ijoia2FybWFib2FyZCIsImEiOiJtVDltdF9RIn0.HIXtQ102rscFzfF32gAVZA',
                        mapStyle: 'mapbox://styles/karmaboard/cjl6t6r4z12pw2sl4vd4wcsja',
                        data: dots,
                        valueName: 'total',
                        positionName: 'centroid',
                        boundingBox: isEmpty(boundingBox) ? [[1, 1], [2, 2]] : boundingBox,
                        width: width,
                        height: height,
                        colors: [{ key: 'positivo', color: '#89cf41' }, { key: 'negativo', color: '#e18080' }, { key: 'neutral', color: '#95bbd4' }],
                        colorFind: function colorFind(colors, colorData) {
                            return colors.find(function (elto) {
                                return elto.key === colorData.sentiment;
                            });
                        },
                        singleIcon: this.renderIcon,
                        groupIcon: this.renderIcon,
                        onViewportChange: this.onViewportChange
                    })
                )
            );
        }
    }]);

    return DotMapa;
}(Component);

DotMapa.propTypes = propTypes;
DotMapa.defaultProps = defaultProps;

export default DotMapa;