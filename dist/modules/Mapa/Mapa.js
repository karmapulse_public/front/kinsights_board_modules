var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import isNull from 'lodash/isNull';
import setDates from '../../helpers/setDates';

import fetchMap, { fetchBoundingBox } from './MapDataFlow';
import LoadingModule from '../LoadingModule';
import DotMapa from './visualizations/DotMapa';

var propTypes = {
    fields: PropTypes.object,
    size: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    dateRange: PropTypes.object,
    services: PropTypes.object,
    filters: PropTypes.object,
    withCache: PropTypes.bool
};

var defaultProps = {
    fields: {},
    side: 'left',
    color: '#666',
    dateRange: {},
    size: {},
    services: {},
    filters: {},
    withCache: true
};

var mappingZoom = function mappingZoom(zoom) {
    return Math.round((zoom - 0) * 11 / 20 + 1);
};

var mapLng = function mapLng(lng) {
    if (lng >= -180 && lng <= 180) return lng;
    var dir = lng < 0 ? 1 : -1;
    var diff = Math.abs(lng) % 180;
    return dir * (180 - diff);
};

var Mapa = function (_Component) {
    _inherits(Mapa, _Component);

    function Mapa(props) {
        _classCallCheck(this, Mapa);

        var _this = _possibleConstructorReturn(this, (Mapa.__proto__ || Object.getPrototypeOf(Mapa)).call(this, props));

        _this.state = {
            moduleConfig: Object.assign({}, props.fields, props.size),
            moduleData: {},
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color,
            boundingBox: {}
        };
        _this.getData = _this.getData.bind(_this);
        return _this;
    }

    _createClass(Mapa, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            this.getInitialView(this.props);
        }
    }, {
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProps) {
            this.getInitialView(nextProps);
        }
    }, {
        key: 'getInitialView',
        value: function getInitialView(info) {
            var _this2 = this;

            var params = Object.assign({
                recipe_id: 'module_tw_bounding_box',
                rule_id: info.fields.search_id
            }, setDates(info.dateRange, this.props.withCache), {
                filters: Object.assign({}, info.filters)
            });

            fetchBoundingBox(this.props.services.twitter, params).subscribe(function (data) {
                _this2.setState(Object.assign({}, data));
            }, function (error) {
                console.log(error);
            }, null);
        }
    }, {
        key: 'getData',
        value: function getData(referencia, bounds) {
            var _this3 = this;

            var params = Object.assign({
                recipe_id: 'module_tw_map'
            }, setDates(this.props.dateRange, this.props.withCache), {
                rule_id: this.props.fields.search_id,
                filters: Object.assign({
                    bounding_box: {
                        top_left: Object.assign({}, bounds.top_left, {
                            lon: mapLng(bounds.top_left.lon)
                        }),
                        bottom_right: Object.assign({}, bounds.bottom_right, {
                            lon: mapLng(bounds.bottom_right.lon)
                        })
                    },
                    precision: { value: mappingZoom(referencia.state.viewport.zoom) }
                }, this.props.filters)
            });
            fetchMap(this.props.services.twitter, params).subscribe(function (data) {
                _this3.setState(Object.assign({}, data, {
                    moduleLoading: false
                }));
            }, function (error) {
                console.log(error);
            }, null);
        }
    }, {
        key: 'renderByViz',
        value: function renderByViz() {
            var _this4 = this;

            var _state = this.state,
                moduleConfig = _state.moduleConfig,
                moduleData = _state.moduleData;

            if (!isNull(moduleData)) {
                var visualizations = {
                    dot_map: function dot_map(params) {
                        return React.createElement(DotMapa, Object.assign({}, params, { getData: _this4.getData }));
                    }
                };
                return visualizations[this.props.view || moduleConfig.visualization](this.state);
            }

            return React.createElement(LoadingModule, null);
        }
    }, {
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'm-map' },
                this.renderByViz()
            );
        }
    }]);

    return Mapa;
}(Component);

Mapa.propTypes = propTypes;
Mapa.defaultProps = defaultProps;

export default Mapa;