import React from 'react';
import PropTypes from 'prop-types';
import isUndefined from 'lodash/isUndefined';
import orderBy from 'lodash/orderBy';
import NoDataModule from '../../../NoDataModule';
import { numberWithCommas } from '../../../../helpers/number';

import styles from './styles';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string
};

var defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleColor: '#666'
};

var TotalNumbers = function TotalNumbers(_ref) {
    var moduleData = _ref.moduleData,
        moduleConfig = _ref.moduleConfig,
        moduleColor = _ref.moduleColor;

    var data = function data() {
        var array = [];
        var entries = Object.entries(moduleData);
        var types = {
            fans_total: {
                title: 'Seguidores totales',
                index: 1
            },
            fans_reached: {
                title: 'Seguidores nuevos',
                index: 2
            },
            posts_total: {
                title: 'Publicaciones realizadas',
                index: 3
            },
            posts_fans_total: {
                title: 'Publicaciones de seguidores',
                index: 4
            },
            interactions_total: {
                title: 'Interacciones totales',
                index: 5
            }
        };

        entries.map(function (item) {
            return array.push({
                key: item[0],
                title: types[item[0]] ? types[item[0]].title : undefined,
                index: types[item[0]] ? types[item[0]].index : undefined,
                total: item[1]
            });
        });
        return orderBy(array, ['index']);
    };

    return React.createElement(
        'article',
        Object.assign({ className: 'total-numbers' }, styles(moduleColor)),
        React.createElement(
            'div',
            { className: 'total-numbers__label' },
            React.createElement(
                'h3',
                null,
                moduleConfig.title
            )
        ),
        data()[0].total !== '0' ? React.createElement(
            'div',
            { className: 'total-numbers__content' },
            data().map(function (item) {
                if (!isUndefined(item.title)) {
                    return React.createElement(
                        'div',
                        { key: item.key, className: 'total-numbers__content__data' },
                        React.createElement(
                            'h4',
                            null,
                            item.title
                        ),
                        React.createElement(
                            'h4',
                            {
                                className: item.key === 'fans_reached' ? item.total > 0 ? 'positive' : 'negative' : ''
                            },
                            numberWithCommas(item.total)
                        )
                    );
                }
                return null;
            })
        ) : React.createElement(NoDataModule, { color: moduleColor })
    );
};

TotalNumbers.propTypes = propTypes;
TotalNumbers.defaultProps = defaultProps;

export default TotalNumbers;