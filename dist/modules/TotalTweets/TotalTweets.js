var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import fetchTotalTweets from './TotalTweetsDataFlow';

import LoadingModule from '../LoadingModule';
import CardHorizontalBars from './visualizations/CardHorizontalBars';
import CardTotalNumbers from './visualizations/CardTotalNumbers';
import CardInline from './visualizations/CardInline';

var propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    services: PropTypes.object,
    dateRange: PropTypes.object
};

var defaultProps = {
    fields: {},
    side: 'left',
    color: '#666',
    services: {},
    dateRange: {}
};

var TotalTweets = function (_Component) {
    _inherits(TotalTweets, _Component);

    function TotalTweets(props) {
        _classCallCheck(this, TotalTweets);

        var _this = _possibleConstructorReturn(this, (TotalTweets.__proto__ || Object.getPrototypeOf(TotalTweets)).call(this, props));

        _this.state = {
            moduleServices: props.services,
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color,
            moduleRange: props.dateRange
        };
        return _this;
    }

    _createClass(TotalTweets, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            this.updateDataFlow(this.props);
        }
    }, {
        key: 'componentDidUpdate',
        value: function componentDidUpdate(prevProps) {
            var _props$dateRange = this.props.dateRange,
                startDate = _props$dateRange.startDate,
                endDate = _props$dateRange.endDate;
            var _props$fields = this.props.fields,
                hasClick = _props$fields.hasClick,
                search_id = _props$fields.search_id;

            if (startDate !== prevProps.dateRange.startDate || endDate !== prevProps.dateRange.endDate || search_id !== prevProps.fields.search_id) {
                this.updateDataFlow(this.props);
            }
            if (hasClick !== prevProps.fields.hasClick) {
                this.changeState(hasClick);
            }
        }
    }, {
        key: 'changeState',
        value: function changeState(hasClick) {
            this.setState({
                moduleConfig: Object.assign({}, this.state.moduleConfig, {
                    hasClick: hasClick
                })
            });
        }
    }, {
        key: 'updateDataFlow',
        value: function updateDataFlow(props) {
            var _this2 = this;

            var fields = props.fields,
                _props$dateRange2 = props.dateRange,
                startDate = _props$dateRange2.startDate,
                endDate = _props$dateRange2.endDate,
                services = props.services;

            var params = {
                recipe_id: 'module_tw_total_tweets',
                rule_id: fields.search_id,
                initial_date: startDate.utc().toISOString(),
                final_date: endDate.utc().toISOString()
            };

            fetchTotalTweets(services.twitter, params).subscribe(function (data) {
                _this2.setState({
                    moduleConfig: props.fields,
                    moduleData: data,
                    moduleLoading: false,
                    moduleSide: props.side,
                    moduleColor: props.color,
                    moduleRange: props.dateRange
                });
            }, function (error) {
                console.log(error);
            }, null);
        }
    }, {
        key: 'renderByViz',
        value: function renderByViz() {
            var _state = this.state,
                moduleConfig = _state.moduleConfig,
                moduleData = _state.moduleData;


            if (moduleData) {
                var visualizations = {
                    card_horizontal_bars: function card_horizontal_bars(params) {
                        return React.createElement(CardHorizontalBars, params);
                    },
                    card_total_numbers: function card_total_numbers(params) {
                        return React.createElement(CardTotalNumbers, params);
                    },
                    card_total_inline: function card_total_inline(params) {
                        return React.createElement(CardInline, params);
                    }
                };
                return visualizations[moduleConfig.visualization](this.state);
            }

            return React.createElement(LoadingModule, null);
        }
    }, {
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'm-total-tweets' },
                this.renderByViz()
            );
        }
    }]);

    return TotalTweets;
}(Component);

TotalTweets.propTypes = propTypes;
TotalTweets.defaultProps = defaultProps;

export default TotalTweets;