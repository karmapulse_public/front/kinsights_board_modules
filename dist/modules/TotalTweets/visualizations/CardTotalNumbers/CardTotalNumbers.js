import React from 'react';
import PropTypes from 'prop-types';
import omit from 'lodash/omit';
import isUndefined from 'lodash/isUndefined';

import { numberWithCommas } from '../../../../helpers/number';
import styles from './cardTotalNumbersStyles';
import ConversationIcon from '../../../../helpers/icons/ceo/conversationIcon';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleSide: PropTypes.string,
    moduleColor: PropTypes.string
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleSide: 'left',
    moduleColor: '#666'
};

var CardTotalNumbers = function CardTotalNumbers(_ref) {
    var moduleConfig = _ref.moduleConfig,
        moduleData = _ref.moduleData,
        moduleSide = _ref.moduleSide,
        moduleColor = _ref.moduleColor;

    // New logic -> add originals with replies
    var validateAdd = function validateAdd() {
        if (!isUndefined(moduleData.reply)) {
            return moduleData.original + moduleData.reply;
        }
        return moduleData.original;
    };

    var tweetsTotal = {};

    if (isUndefined(moduleData.original)) {
        tweetsTotal = moduleData;
    } else {
        tweetsTotal = Object.assign({}, moduleData, {
            original: validateAdd()
        });
    }

    var mapLabels = {
        original: 'Originales',
        retweet: 'Retweets',
        quote: 'Citados'
    };

    var formatedTweets = Object.keys(omit(tweetsTotal, ['total', 'reply'])).map(function (type) {
        return {
            label: mapLabels[type],
            total: tweetsTotal[type],
            color: function () {
                var types = {
                    original: '#7b98ba',
                    quote: '#e68f4b',
                    retweet: '#67c5b0'
                };
                return types[type];
            }()
        };
    });

    var renderTotalNumbers = function renderTotalNumbers() {
        return formatedTweets.map(function (type, index) {
            return React.createElement(
                'div',
                { key: index },
                React.createElement(
                    'h4',
                    null,
                    numberWithCommas(type.total)
                ),
                React.createElement(
                    'h5',
                    null,
                    type.label
                )
            );
        });
    };

    var _moduleConfig$title = moduleConfig.title,
        title = _moduleConfig$title === undefined ? 'RELEVANCIA SOCIAL' : _moduleConfig$title;


    return React.createElement(
        'div',
        Object.assign({ className: 'card-total-numbers' }, styles(moduleSide, moduleColor)),
        React.createElement(
            'div',
            { className: 'mentions' },
            React.createElement(
                'div',
                { className: 'mentions__title' },
                React.createElement(
                    'h3',
                    null,
                    title
                )
            ),
            React.createElement(
                'div',
                { className: 'mentions__body' },
                React.createElement(
                    'div',
                    { className: 'mentions__body__total' },
                    React.createElement(
                        'div',
                        null,
                        React.createElement(ConversationIcon, { color: '#6AB9DC' }),
                        React.createElement(
                            'h4',
                            null,
                            numberWithCommas(moduleData.total)
                        )
                    ),
                    React.createElement(
                        'h5',
                        null,
                        'Menciones totales'
                    )
                ),
                React.createElement(
                    'div',
                    { className: 'mentions__body__numbers' },
                    renderTotalNumbers()
                )
            )
        )
    );
};

CardTotalNumbers.propTypes = propTypes;
CardTotalNumbers.defaultProps = defaultProps;

export default CardTotalNumbers;