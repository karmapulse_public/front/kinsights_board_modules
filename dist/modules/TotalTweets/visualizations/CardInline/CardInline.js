import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import omit from 'lodash/omit';
import isUndefined from 'lodash/isUndefined';
import Typography from 'kpulse_design_system/dist/Typography';

import withDrawer from '../../../withDrawer';

import { numberWithCommas } from '../../../../helpers/number';
import uniqueId from '../../../../helpers/uniqueId';
import styles from './cardInlineStyles';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleSide: PropTypes.string,
    moduleColor: PropTypes.string,
    onClick: PropTypes.func
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleSide: 'left',
    moduleColor: '#666',
    onClick: function onClick() {
        return {};
    }
};

var Paragraph = Typography.Paragraph;


var CardInline = function CardInline(_ref) {
    var moduleConfig = _ref.moduleConfig,
        moduleData = _ref.moduleData,
        moduleSide = _ref.moduleSide,
        moduleColor = _ref.moduleColor,
        onClick = _ref.onClick;

    var refExport = React.useRef(null);
    var mapLabels = {
        original: 'Originales',
        retweet: 'Retweets',
        quote: 'Citados',
        reply: 'Respuestas'
    };
    var titleConfig = function titleConfig(type) {
        return {
            title: moduleConfig.title || 'Tweets en el tiempo',
            subtitleLeft: type ? mapLabels[type] : '',
            subtitleRight: ''
        };
    };
    var onRealClick = moduleConfig.hasClick ? onClick : function () {
        return {};
    };

    // New logic -> add originals with replies
    var validateAdd = function validateAdd() {
        // if (!isUndefined(moduleData.reply)) {
        //     return moduleData.original + moduleData.reply;
        // }
        return moduleData.original;
    };

    var tweetsTotal = {};

    if (isUndefined(moduleData.original)) {
        tweetsTotal = moduleData;
    } else {
        tweetsTotal = Object.assign({}, moduleData, {
            original: validateAdd()
        });
    }

    var getMapLabelsKey = function getMapLabelsKey(value) {
        return Object.entries(mapLabels).find(function (i) {
            return i[1] === value;
        })[0];
    };

    var formatedTweets = Object.keys(omit(tweetsTotal, ['total'])).map(function (type) {
        return {
            label: mapLabels[type],
            total: tweetsTotal[type]
        };
    });

    var renderTotalNumbers = function renderTotalNumbers() {
        return formatedTweets.map(function (type) {
            return React.createElement(
                'div',
                {
                    key: uniqueId(),
                    className: type.label,
                    onClick: function onClick() {
                        onRealClick({ filters: { type: getMapLabelsKey(type.label) } }, titleConfig(getMapLabelsKey(type.label)));
                    }
                },
                React.createElement(
                    Paragraph,
                    { small: true, color: 'rgba(0,0,0,0.5)' },
                    type.label
                ),
                React.createElement(
                    Paragraph,
                    { large: true },
                    numberWithCommas(type.total)
                )
            );
        });
    };

    var _moduleConfig$title = moduleConfig.title,
        title = _moduleConfig$title === undefined ? 'DISTRIBUCIÓN TWEETS' : _moduleConfig$title,
        buttonExport = moduleConfig.buttonExport;


    return React.createElement(
        'div',
        Object.assign({
            className: 'card-total-inline'
        }, styles(moduleSide, moduleColor, moduleConfig.hasClick), {
            ref: refExport
        }),
        React.createElement(
            'div',
            { className: 'mentions' },
            React.createElement(
                'div',
                { className: 'mentions__title' },
                React.createElement(
                    'h3',
                    null,
                    title
                )
            ),
            React.createElement(
                'div',
                { className: 'mentions__body' },
                React.createElement(
                    'div',
                    {
                        className: 'mentions__body__total',
                        onClick: function onClick() {
                            return onRealClick({}, titleConfig());
                        }
                    },
                    React.createElement(
                        'div',
                        null,
                        React.createElement(
                            Paragraph,
                            { small: true, bold: true },
                            'Total de tweets'
                        ),
                        React.createElement(
                            Paragraph,
                            { large: true, bold: true, color: '#53b2db' },
                            numberWithCommas(moduleData.total)
                        )
                    )
                ),
                React.createElement(
                    'div',
                    { className: 'mentions__body__numbers' },
                    renderTotalNumbers()
                )
            )
        )
    );
};

CardInline.propTypes = propTypes;
CardInline.defaultProps = defaultProps;

export default withDrawer(CardInline);