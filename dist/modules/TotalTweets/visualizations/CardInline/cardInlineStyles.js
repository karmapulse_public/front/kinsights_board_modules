import { css } from 'glamor';

var styles = function styles(side, color, hasClick) {
    return css({
        ' .mentions': {

            ' &__title': {
                '>h3': {
                    padding: '15px 20px',
                    textTransform: 'uppercase',
                    fontSize: 16,
                    fontWeight: 400,
                    textAlign: side,
                    color: color
                }
            },

            ' &__body': {
                position: 'relative',
                display: 'flex',
                alignItems: 'center',

                ' p': {
                    lineHeight: 'normal'
                },

                ' &__total': {
                    cursor: hasClick ? 'pointer' : 'default'
                },

                ' &__numbers': {
                    position: 'relative',
                    display: 'flex',
                    marginLeft: '30px',
                    justifyContent: 'space-around',
                    alignItems: 'center',

                    ' p': {
                        maxWidth: 60,
                        overflow: 'hidden',
                        textOverflow: 'ellipsis',
                        whiteSpace: 'nowrap'
                    },

                    ' > div': {
                        width: '33.33333%',
                        order: 4,
                        textAlign: 'left',
                        cursor: hasClick ? 'pointer' : 'default',
                        ':not(:last-child)': {
                            marginRight: 15
                        },
                        '.Retweets': {
                            order: 2
                        },
                        '.Originales': {
                            order: 1
                        },
                        '.Respuestas': {
                            order: 3
                        }
                    },

                    ':after': {
                        content: '""',
                        position: 'absolute',
                        top: '50%',
                        right: 'calc(100% + 15px)',
                        width: '1px',
                        height: '100%',
                        borderRight: 'solid 1px rgba(0, 0, 0, 0.1)',
                        transform: 'translate(0%, -50%)'
                    }
                }

            }
        }
    });
};

export default styles;