import { css } from 'glamor';

var styles = function styles(side, color) {
    var bgColor = '#FFFFFF';

    return css({
        minWidth: 300,
        height: '100%',
        backgroundColor: bgColor,
        color: 'rgba(0,0,0, 0.5)',
        ' .card__divider': {
            marginBottom: 15,
            height: 2,
            backgroundColor: '#ededed !important',
            '@media screen and (min-width:1024px)': {
                display: 'none'
            }
        },

        '@media screen and (min-width:1024px)': {
            minWidth: 480
        },

        ' .mentions': {
            position: 'relative',

            '&__label': {
                height: 55,
                '> h3': {
                    padding: '15px 20px 20px',
                    textTransform: 'uppercase',
                    textAlign: side,
                    fontSize: 16,
                    fontWeight: 400,
                    color: color
                }
            },

            '&__number': {
                position: 'absolute',
                top: 0,
                left: side === 'left' ? 'auto' : 0,
                right: side === 'left' ? 0 : 'auto',
                height: 55,
                padding: '0px 20px',
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                textAlign: side === 'left' ? 'right' : 'left',

                '> h4': {
                    fontSize: 12,
                    fontWeight: 400,
                    marginBottom: 5,
                    color: '#4a4a4a'
                },

                '> h5': {
                    fontSize: 16,
                    fontWeight: 400,
                    color: color
                },

                '@media screen and (min-width:1024px)': {
                    top: '50%',
                    left: side === 'left' ? 0 : 'auto',
                    right: side === 'left' ? 'auto' : 0,
                    textAlign: 'center',
                    transform: 'translate(0px, -30%)',
                    '>h4': {
                        fontSize: 16
                    },
                    '>h5': {
                        fontSize: 18
                    }
                }
            },

            '&__graphic': {
                padding: '0 20px 20px 20px',

                '> div:not(.card__divider)': {
                    height: 30,
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',

                    '> h4': {
                        width: 70,
                        marginBottom: 5,
                        fontSize: 12,
                        fontWeight: 400,
                        textAlign: side,
                        color: '#485465',
                        order: side === 'left' ? 1 : 3
                    },

                    '> div': {
                        position: 'relative',
                        flex: '1',
                        order: 2,
                        alignItems: 'center',
                        height: 10,

                        '> .percentage': {
                            width: '100%',
                            height: '100%',
                            float: side,
                            backgroundColor: color === undefined ? '#666' : color
                        },
                        '&::before': {
                            content: '""',
                            position: 'absolute',
                            left: side === 'left' ? 0 : 'auto',
                            right: side === 'left' ? 'auto' : 0,
                            top: -10,
                            width: 1,
                            height: 30,
                            backgroundColor: '#000',
                            opacity: '0.2'
                        }
                    },

                    '> h5': {
                        width: 50,
                        fontSize: 12,
                        fontWeight: 700,
                        marginBottom: 5,
                        color: color,
                        textAlign: side === 'left' ? 'right' : 'left',
                        order: side === 'left' ? 3 : 1,
                        '@media screen and (min-width:1024px)': {
                            fontSize: 14
                        }
                    }
                },
                '@media screen and (min-width:1024px)': {
                    width: 320,
                    marginLeft: side === 'left' ? 'auto' : 0,
                    marginRight: side === 'right' ? 'auto' : 0
                }
            }
        }
    });
};

export default styles;