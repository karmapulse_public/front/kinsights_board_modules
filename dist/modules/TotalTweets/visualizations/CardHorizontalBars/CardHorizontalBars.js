import React from 'react';
import PropTypes from 'prop-types';
import omit from 'lodash/omit';
import isUndefined from 'lodash/isUndefined';

import { numberWithCommas } from '../../../../helpers/number';
import styles from './cardHorizontalBarsStyles';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleSide: PropTypes.string,
    moduleColor: PropTypes.string
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleSide: 'left',
    moduleColor: '#666'
};

var CardHorizontalBars = function CardHorizontalBars(_ref) {
    var moduleConfig = _ref.moduleConfig,
        moduleData = _ref.moduleData,
        moduleSide = _ref.moduleSide,
        moduleColor = _ref.moduleColor;

    // New logic -> add originals with replies
    var validateAdd = function validateAdd() {
        if (!isUndefined(moduleData.reply)) {
            return moduleData.original + moduleData.reply;
        }
        return moduleData.original;
    };

    var tweetsTotal = {};

    if (isUndefined(moduleData.original)) {
        tweetsTotal = moduleData;
    } else {
        tweetsTotal = Object.assign({}, moduleData, {
            original: validateAdd()
        });
    }

    var formatedTweets = Object.keys(omit(tweetsTotal, ['total', 'reply'])).map(function (type) {
        return {
            label: type.charAt(0).toUpperCase() + type.slice(1),
            total: tweetsTotal[type],
            percentage: Math.ceil(tweetsTotal[type] * 100 / tweetsTotal.total)
        };
    });

    var renderBars = function renderBars() {
        return formatedTweets.map(function (type, index) {
            return React.createElement(
                'div',
                { key: index },
                React.createElement(
                    'h4',
                    null,
                    type.label
                ),
                React.createElement(
                    'div',
                    { className: 'mentions__graphic__bar' },
                    React.createElement('div', { className: 'percentage', style: { width: type.percentage } })
                ),
                React.createElement(
                    'h5',
                    null,
                    numberWithCommas(type.total)
                )
            );
        });
    };

    var _moduleConfig$title = moduleConfig.title,
        title = _moduleConfig$title === undefined ? 'MENCIONES' : _moduleConfig$title;


    return React.createElement(
        'div',
        Object.assign({ className: 'card-horizontal' }, styles(moduleSide, moduleColor)),
        React.createElement(
            'div',
            { className: 'mentions' },
            React.createElement(
                'div',
                { className: 'mentions__label' },
                React.createElement(
                    'h3',
                    null,
                    title
                )
            ),
            React.createElement(
                'div',
                { className: 'mentions__number' },
                React.createElement(
                    'h4',
                    null,
                    'Total de menciones'
                ),
                React.createElement(
                    'h5',
                    null,
                    numberWithCommas(moduleData.total)
                )
            ),
            React.createElement(
                'div',
                { className: 'mentions__graphic' },
                React.createElement('div', { className: 'card__divider' }),
                renderBars()
            )
        )
    );
};

CardHorizontalBars.propTypes = propTypes;
CardHorizontalBars.defaultProps = defaultProps;

export default CardHorizontalBars;