import { Observable } from 'rxjs';
import fromPairs from 'lodash/fromPairs';
import isUndefined from 'lodash/isUndefined';
import find from 'lodash/find';
import { fetchXHR } from '../../helpers/http';

var formatData = function formatData(buckets) {
    var collect = [];
    var tweetsTypes = ['retweet', 'original', 'reply', 'quote'];
    tweetsTypes.map(function (item) {
        var s = find(buckets, { key: item });
        if (!isUndefined(s)) return collect.push([[s.key], s.doc_count]);
        return collect.push([[item], 0]);
    });
    return collect;
};

export default (function (url, params) {
    return Observable.create(function (observer) {
        Observable.fromPromise(fetchXHR(url, 'GET', params)).subscribe(function (data) {
            observer.next(Object.assign({ total: data.json.response.hits.total }, fromPairs(formatData(data.json.response.aggregations.tweets_by_type.buckets))));
        }, function (error) {
            observer.error(error);
        });
    });
});