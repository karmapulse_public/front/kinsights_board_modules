import React from 'react';
import PropTypes from 'prop-types';

import { LikeIcon, LoveIcon, HahaIcon, WowIcon, SadIcon, AngryIcon } from '../../../../helpers/icons/fb';

import { numberWithCommas } from '../../../../helpers/number';

import styles from './styles';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    noDataYet: PropTypes.func
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    noDataYet: null
};

var ListInteractions = function ListInteractions(_ref) {
    var moduleData = _ref.moduleData,
        moduleConfig = _ref.moduleConfig,
        moduleColor = _ref.moduleColor,
        noDataYet = _ref.noDataYet;
    var interactions_total = moduleData.interactions_total,
        reactions_total = moduleData.reactions_total,
        shares_total = moduleData.shares_total,
        comments_total = moduleData.comments_total,
        reactions = moduleData.reactions,
        reactions_avg = moduleData.reactions_avg,
        shares_avg = moduleData.shares_avg,
        comments_avg = moduleData.comments_avg;


    var renderReactions = function renderReactions() {
        return React.createElement(
            'ul',
            null,
            React.createElement(
                'li',
                null,
                React.createElement(LikeIcon, null),
                React.createElement(
                    'span',
                    null,
                    numberWithCommas(reactions.LIKE)
                )
            ),
            React.createElement(
                'li',
                null,
                React.createElement(LoveIcon, null),
                React.createElement(
                    'span',
                    null,
                    numberWithCommas(reactions.LOVE)
                )
            ),
            React.createElement(
                'li',
                null,
                React.createElement(HahaIcon, null),
                React.createElement(
                    'span',
                    null,
                    numberWithCommas(reactions.HAHA)
                )
            ),
            React.createElement(
                'li',
                null,
                React.createElement(WowIcon, null),
                React.createElement(
                    'span',
                    null,
                    numberWithCommas(reactions.WOW)
                )
            ),
            React.createElement(
                'li',
                null,
                React.createElement(SadIcon, null),
                React.createElement(
                    'span',
                    null,
                    numberWithCommas(reactions.SAD)
                )
            ),
            React.createElement(
                'li',
                null,
                React.createElement(AngryIcon, null),
                React.createElement(
                    'span',
                    null,
                    numberWithCommas(reactions.ANGRY)
                )
            )
        );
    };

    var renderContent = function renderContent() {
        return React.createElement(
            'div',
            { className: 'list-interactions__content' },
            React.createElement(
                'h4',
                null,
                'Interacciones totales',
                React.createElement(
                    'span',
                    null,
                    numberWithCommas(interactions_total)
                )
            ),
            React.createElement(
                'h4',
                null,
                'Comentarios totales',
                React.createElement(
                    'span',
                    null,
                    numberWithCommas(comments_total)
                )
            ),
            React.createElement(
                'h4',
                null,
                'Contenido total compartido',
                React.createElement(
                    'span',
                    null,
                    numberWithCommas(shares_total)
                )
            ),
            React.createElement(
                'h4',
                null,
                'Reacciones totales',
                React.createElement(
                    'span',
                    null,
                    numberWithCommas(reactions_total)
                )
            ),
            renderReactions(),
            React.createElement(
                'h4',
                null,
                'Promedio de comentarios por publicaci\xF3n',
                React.createElement(
                    'span',
                    null,
                    numberWithCommas(Math.floor(comments_avg))
                )
            ),
            React.createElement(
                'h4',
                null,
                'Promedio de veces compartida por publicaci\xF3n',
                React.createElement(
                    'span',
                    null,
                    numberWithCommas(Math.floor(shares_avg))
                )
            ),
            React.createElement(
                'h4',
                null,
                'Promedio reacciones por publicaci\xF3n',
                React.createElement(
                    'span',
                    null,
                    numberWithCommas(Math.floor(reactions_avg))
                )
            )
        );
    };

    return React.createElement(
        'div',
        Object.assign({ className: 'list-interactions' }, styles(moduleColor)),
        React.createElement(
            'div',
            { className: 'list-interactions__label' },
            React.createElement(
                'h3',
                null,
                moduleConfig.title
            )
        ),
        noDataYet !== null ? noDataYet() : renderContent()
    );
};

ListInteractions.propTypes = propTypes;
ListInteractions.defaultProps = defaultProps;

export default ListInteractions;