import React from 'react';
import PropTypes from 'prop-types';
import { faFacebookF } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Slider from 'kpulse_design_system/dist/Carousel';

import { LikeIcon, LoveIcon, HahaIcon, WowIcon, SadIcon, AngryIcon } from '../../../../helpers/icons/fb';

import { numberWithCommas } from '../../../../helpers/number';

import styles from './styles';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    noDataYet: PropTypes.func
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    noDataYet: null
};

var SliderInteractions = function SliderInteractions(_ref) {
    var moduleData = _ref.moduleData,
        moduleConfig = _ref.moduleConfig,
        moduleColor = _ref.moduleColor,
        noDataYet = _ref.noDataYet;
    var interactions_total = moduleData.interactions_total,
        reactions_total = moduleData.reactions_total,
        shares_total = moduleData.shares_total,
        comments_total = moduleData.comments_total,
        reactions = moduleData.reactions,
        reactions_avg = moduleData.reactions_avg,
        shares_avg = moduleData.shares_avg,
        comments_avg = moduleData.comments_avg;


    var renderReactions = function renderReactions() {
        return React.createElement(
            Slider,
            { width: '90%', height: '103px', withDots: false, arrowsColor: '#d4c19c' },
            React.createElement(
                'div',
                { className: 'slider__card' },
                React.createElement(
                    'h5',
                    null,
                    React.createElement(
                        'span',
                        null,
                        numberWithCommas(comments_total)
                    ),
                    React.createElement(
                        'p',
                        null,
                        'Comentarios'
                    )
                ),
                React.createElement(
                    'h5',
                    null,
                    React.createElement(
                        'span',
                        null,
                        numberWithCommas(shares_total)
                    ),
                    React.createElement(
                        'p',
                        null,
                        'Compartidos'
                    )
                ),
                React.createElement(
                    'h5',
                    null,
                    React.createElement(
                        'span',
                        null,
                        numberWithCommas(reactions_total)
                    ),
                    React.createElement(
                        'p',
                        null,
                        'Reacciones'
                    )
                )
            ),
            React.createElement(
                'div',
                { className: 'slider__card' },
                React.createElement(
                    'h5',
                    null,
                    React.createElement(
                        'span',
                        null,
                        numberWithCommas(reactions.LIKE)
                    ),
                    React.createElement(LikeIcon, null)
                ),
                React.createElement(
                    'h5',
                    null,
                    React.createElement(
                        'span',
                        null,
                        numberWithCommas(reactions.LOVE)
                    ),
                    React.createElement(LoveIcon, null)
                ),
                React.createElement(
                    'h5',
                    null,
                    React.createElement(
                        'span',
                        null,
                        numberWithCommas(reactions.HAHA)
                    ),
                    React.createElement(HahaIcon, null)
                )
            ),
            React.createElement(
                'div',
                { className: 'slider__card' },
                React.createElement(
                    'h5',
                    null,
                    React.createElement(
                        'span',
                        null,
                        numberWithCommas(reactions.WOW)
                    ),
                    React.createElement(WowIcon, null)
                ),
                React.createElement(
                    'h5',
                    null,
                    React.createElement(
                        'span',
                        null,
                        numberWithCommas(reactions.SAD)
                    ),
                    React.createElement(SadIcon, null)
                ),
                React.createElement(
                    'h5',
                    null,
                    React.createElement(
                        'span',
                        null,
                        numberWithCommas(reactions.ANGRY)
                    ),
                    React.createElement(AngryIcon, null)
                )
            )
        );
    };

    var renderContent = function renderContent() {
        return React.createElement(
            'div',
            { className: 'list-interactions__content' },
            React.createElement(
                'h4',
                null,
                React.createElement(
                    'span',
                    null,
                    numberWithCommas(interactions_total)
                ),
                React.createElement(
                    'p',
                    null,
                    'Totales '
                )
            ),
            React.createElement(
                'div',
                { style: { position: 'relative' } },
                renderReactions()
            )
        );
    };

    return React.createElement(
        'div',
        Object.assign({ className: 'list-interactions' }, styles(moduleColor)),
        React.createElement(
            'div',
            { className: 'list-interactions__label' },
            React.createElement(
                'h3',
                null,
                'INTERACCIONES FACEBOOK'
            ),
            React.createElement(
                'div',
                null,
                React.createElement(FontAwesomeIcon, { icon: faFacebookF, color: 'white' })
            )
        ),
        noDataYet !== null ? noDataYet() : renderContent()
    );
};

SliderInteractions.propTypes = propTypes;
SliderInteractions.defaultProps = defaultProps;

export default SliderInteractions;