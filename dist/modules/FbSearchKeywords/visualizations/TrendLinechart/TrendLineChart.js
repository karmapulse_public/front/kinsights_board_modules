import React, { useRef } from 'react';
import PropTypes from 'prop-types';

import styles from './trendLineChartStyles';
import Lines from '../../../Charts/ChartsLines';
import NoDataModule from '../../../NoDataModule';

var colors = [{ r: 199, g: 89, b: 147 }, { r: 129, g: 117, b: 204 }, { r: 204, g: 95, b: 66 }, { r: 91, g: 169, b: 101 }, { r: 84, g: 161, b: 211 }];

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    noDataYet: PropTypes.func
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    noDataYet: null
};

var TrendLineChart = function TrendLineChart(_ref) {
    var moduleConfig = _ref.moduleConfig,
        moduleData = _ref.moduleData,
        moduleColor = _ref.moduleColor,
        noDataYet = _ref.noDataYet;
    var _moduleConfig$title = moduleConfig.title,
        title = _moduleConfig$title === undefined ? 'TEMAS EN EL TIEMPO' : _moduleConfig$title,
        buttonExport = moduleConfig.buttonExport;

    var refExport = useRef(null);
    return React.createElement(
        'div',
        Object.assign({
            ref: refExport,
            className: 'trend-line-chart'
        }, styles(moduleColor)),
        React.createElement(
            'div',
            { className: 'trend__label' },
            React.createElement(
                'h3',
                null,
                title
            )
        ),
        function () {
            if (noDataYet !== null) {
                return noDataYet();
            }
            return moduleData.values.aggregations.keywords.buckets.reduce(function (prev, item) {
                return prev += item.doc_count;
            }, 0) === 0 ? React.createElement(
                'div',
                { className: 'noData' },
                React.createElement(NoDataModule, { color: moduleColor })
            ) : React.createElement(
                'div',
                { className: 'trend__chart' },
                React.createElement(Lines, {
                    data: moduleData.values,
                    type: 'multiple',
                    colors: colors,
                    entity: 'keywords'
                })
            );
        }()
    );
};

TrendLineChart.propTypes = propTypes;
TrendLineChart.defaultProps = defaultProps;

export default TrendLineChart;