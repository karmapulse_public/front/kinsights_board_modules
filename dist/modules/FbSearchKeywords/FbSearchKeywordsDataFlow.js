import { Observable } from 'rxjs';
import { requestPost } from '../../helpers/http';

export default (function (url, params) {
    return Observable.create(function (observer) {
        Observable.fromPromise(requestPost(url, 'POST', params)).subscribe(function (data) {
            var parsedData = {};
            parsedData.total = data.json.total;
            parsedData.values = {
                aggregations: {
                    keywords: {
                        buckets: function () {
                            var arr = [];
                            for (var key in data.json.data) {
                                arr.push({
                                    key: key,
                                    doc_count: data.json.data[key].doc_count,
                                    keywords_by_time: {
                                        buckets: data.json.data[key].keywords_by_time.buckets
                                    }
                                });
                            }
                            return arr;
                        }()
                    }
                }
            };
            observer.next(parsedData);
        }, function (error) {
            observer.error(error);
        });
    });
});