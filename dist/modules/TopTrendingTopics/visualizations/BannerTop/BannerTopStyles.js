import { css } from 'glamor';

var styles = function styles(color, side) {
    var bgColor = 'transparent';

    return css({
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        backgroundColor: bgColor,
        color: 'rgba(0,0,0, 0.5)',
        height: '150px',
        '>ul': {
            display: 'flex',
            width: '100%',
            height: 'inherit',
            flexDirection: 'column',
            flexWrap: 'wrap',
            justifyContent: 'space-between'
        },

        ' .tt__label': {
            '> h3': {
                lineHeight: 'normal',
                textTransform: 'uppercase',
                fontSize: 14,
                fontWeight: 400,
                textAlign: 'left',
                color: color
            },
            ' > h3:last-child': {
                fontSize: 24,
                fontWeight: '700'
            }
        },

        ' .tt__item': {
            display: 'flex',
            alignItems: 'center'
        },

        ' .tt__index': {
            width: 20,
            height: 20,
            fontSize: '14px',
            fontWeight: 500,
            lineHeight: '20px',
            textAlign: 'center'
        },

        ' .tt__nameplace': {
            flex: 1,
            padding: '0px 10px 0px 5px'
        },

        ' .tt__name': {
            textAlign: side === 'left' ? 'left' : 'right',
            fontSize: 14,
            padding: 0,
            margin: 0,
            fontWeight: 500,
            color: '#000',
            lineHeight: 'normal'
        },

        ' .tt__value': {
            fontSize: 12,
            fontWeight: 500,
            color: color,
            lineHeight: 'normal',
            opacity: 0.8
        },
        ' .no-data-module': { minHeight: 200 }
    });
};

export default styles;