import React from 'react';
import PropTypes from 'prop-types';

import styles from './BannerTopStyles';
import NoDataModule from '../../../NoDataModule';
import { numberWithCommas } from '../../../../helpers/number';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.array,
    moduleColor: PropTypes.string,
    moduleSide: PropTypes.string,
    region: PropTypes.string
};

var defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleColor: '#666',
    moduleSide: 'right',
    region: ''
};

var displayRegion = function displayRegion(region) {
    var names = {
        'Ciudad de México': 'CDMX'
    };
    return names[region] || region;
};

var ListTopTT = function ListTopTT(_ref) {
    var moduleConfig = _ref.moduleConfig,
        moduleData = _ref.moduleData,
        moduleColor = _ref.moduleColor,
        moduleSide = _ref.moduleSide,
        region = _ref.region;

    var dataFormat = moduleData.values.map(function (item, index) {
        return {
            id: index + 1,
            subject: item.trend,
            total: item.total
        };
    });

    var renderTitle = function renderTitle() {
        if (moduleConfig.show_title) {
            var _moduleConfig$title = moduleConfig.title,
                title = _moduleConfig$title === undefined ? 'Tendencias' : _moduleConfig$title;

            return React.createElement(
                'div',
                { className: 'tt__label' },
                React.createElement(
                    'h3',
                    null,
                    title
                ),
                React.createElement(
                    'h3',
                    null,
                    displayRegion(region)
                )
            );
        }
        return '';
    };

    var showItem = function showItem(index, _ref2) {
        var subject = _ref2.subject,
            total = _ref2.total,
            id = _ref2.id;

        var value = subject.length < 35 ? subject : subject.substring(0, 35).concat('...');
        return React.createElement(
            'li',
            { className: 'tt__item', key: index },
            React.createElement(
                'h5',
                { className: 'tt__index' },
                id,
                '.'
            ),
            React.createElement(
                'div',
                { className: 'tt__nameplace' },
                React.createElement(
                    'h4',
                    { className: 'tt__name' },
                    value
                ),
                React.createElement(
                    'p',
                    { className: 'tt__value' },
                    numberWithCommas(total)
                )
            )
        );
    };

    var renderItems = function renderItems() {
        return dataFormat.map(function (item, index) {
            return showItem(index, item);
        });
    };

    return React.createElement(
        'div',
        Object.assign({ className: 'trending-topics' }, styles(moduleColor, moduleSide)),
        renderTitle(),
        dataFormat.length === 0 ? React.createElement(NoDataModule, null) : React.createElement(
            'ul',
            null,
            renderItems()
        )
    );
};

ListTopTT.propTypes = propTypes;
ListTopTT.defaultProps = defaultProps;

export default ListTopTT;