import { css } from 'glamor';

var styles = function styles(color, side) {
    var bgColor = 'transparent';

    return css({
        width: '100%',
        height: 'auto',
        display: 'flex',
        alignItems: 'center',
        flexWrap: 'wrap',
        backgroundColor: bgColor,
        color: 'rgba(0,0,0, 0.5)',

        '>ul': {
            width: '100%',
            padding: '20px'
        },

        ' .tt__titles': {
            width: '100%',
            padding: '5px 20px 20px',
            display: 'flex',
            flexDirection: side === 'left' ? 'row' : 'row-reverse',
            justifyContent: 'space-between',
            ' > h5': {
                fontSize: '10px'
            }
        },

        ' .tt__label': {
            width: '100%',
            '> h3': {
                padding: '20px',
                textTransform: 'uppercase',
                fontSize: 16,
                fontWeight: 400,
                textAlign: 'left',
                color: color
            },
            ' ~ ul': {
                paddingTop: 0
            }
        },

        ' .tt__item': {
            '&:not(:last-of-type)': {
                marginBottom: 20
            },
            flexDirection: side === 'left' ? 'row' : 'row-reverse',
            display: 'flex',
            alignItems: 'center'
        },

        ' .tt__index': {
            width: 20,
            height: 20,
            fontSize: '13px',
            fontWeight: 500,
            lineHeight: '20px',
            textAlign: 'center',
            background: '#f2f2f2'
        },

        ' .tt__nameplace': {
            flex: 1,
            padding: '0px 10px'
        },

        ' .tt__name': {
            textAlign: side === 'left' ? 'left' : 'right',
            fontSize: 12,
            padding: 0,
            margin: 0,
            fontWeight: 500,
            color: '#000'
        },

        ' .tt__place': {
            textAlign: side === 'left' ? 'left' : 'right',
            fontSize: 10,
            padding: 0,
            margin: 0,
            color: 'rgba(0, 0, 0, 0.7)'
        },

        ' .tt__value': {
            fontSize: 12,
            fontWeight: 500,
            color: color,
            opacity: 0.8
        },
        ' .no-data-module': { minHeight: 200 }
    });
};

export default styles;