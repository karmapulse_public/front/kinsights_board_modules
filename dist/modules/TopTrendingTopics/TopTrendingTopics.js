var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';

import ListTopTT from './visualizations/ListTopTT';
import BannerTop from './visualizations/BannerTop';
import fetchTopTT from './TopTTDataFlow';
import LoadingModule from '../LoadingModule';

var propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    view: PropTypes.string,
    services: PropTypes.object,
    dateRange: PropTypes.object
};

var defaultProps = {
    fields: {},
    side: 'left',
    color: '#666',
    services: {},
    dateRange: {},
    view: ''
};

var TopTrendingTopics = function (_Component) {
    _inherits(TopTrendingTopics, _Component);

    function TopTrendingTopics(props) {
        _classCallCheck(this, TopTrendingTopics);

        var _this = _possibleConstructorReturn(this, (TopTrendingTopics.__proto__ || Object.getPrototypeOf(TopTrendingTopics)).call(this, props));

        _this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color,
            moduleView: props.view
        };
        return _this;
    }

    _createClass(TopTrendingTopics, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            this.fetchData(this.props);
        }
    }, {
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProps) {
            this.fetchData(nextProps);
        }
    }, {
        key: 'fetchData',
        value: function fetchData(newProps) {
            var _this2 = this;

            var _newProps$dateRange = newProps.dateRange,
                startDate = _newProps$dateRange.startDate,
                endDate = _newProps$dateRange.endDate;

            var params = {
                recipe_id: 'module_tw_top_trends',
                region_id: parseInt(newProps.fields.region_id.split('|')[1].trim(), 10),
                initial_date: moment(startDate).utc().toISOString(),
                final_date: moment(endDate).utc().toISOString(),
                filters: {
                    top: newProps.fields.top_limit
                }
            };

            fetchTopTT(this.props.services.twitter, params).subscribe(function (data) {
                _this2.setState({
                    moduleData: data,
                    moduleConfig: newProps.fields,
                    moduleLoading: false,
                    moduleColor: newProps.color,
                    moduleSide: newProps.side,
                    region: newProps.fields.region_id.split('|')[0].trim()
                });
            }, function (error) {
                console.log(error);
            }, null);
        }
    }, {
        key: 'renderByViz',
        value: function renderByViz() {
            var _state = this.state,
                moduleData = _state.moduleData,
                moduleConfig = _state.moduleConfig;

            if (moduleData) {
                var visualizations = {
                    list_top_trends: function list_top_trends(params) {
                        return React.createElement(ListTopTT, params);
                    },
                    banner_top_trends: function banner_top_trends(params) {
                        return React.createElement(BannerTop, params);
                    }
                };
                var v = this.state.moduleView !== '' ? this.state.moduleView : moduleConfig.visualization;
                return visualizations[v](this.state);
            }
            return React.createElement(LoadingModule, null);
        }
    }, {
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'm-top-trending-topics' },
                this.renderByViz()
            );
        }
    }]);

    return TopTrendingTopics;
}(Component);

TopTrendingTopics.propTypes = propTypes;
TopTrendingTopics.defaultProps = defaultProps;

export default TopTrendingTopics;