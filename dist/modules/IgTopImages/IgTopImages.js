var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import setDates from '../../helpers/setDates';
import fetchIgTopImages from './IgTopImagesDataFlow';

import LoadingModule from '../LoadingModule';
import NoDataYet from '../NoDataYet';
import ContentCarousel from './visualizations/ContentCarousel';
import SliderTopPost from './visualizations/SliderTopPost';

var propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    services: PropTypes.object,
    dateRange: PropTypes.object,
    filters: PropTypes.object,
    size: PropTypes.number,
    withCache: PropTypes.bool,
    showNoDataYet: PropTypes.bool
};

var defaultProps = {
    fields: {},
    side: 'left',
    color: '#666',
    services: {},
    dateRange: {},
    filters: {},
    size: 0,
    withCache: true,
    showNoDataYet: false
};

var IgTopImages = function (_Component) {
    _inherits(IgTopImages, _Component);

    function IgTopImages(props) {
        _classCallCheck(this, IgTopImages);

        var _this = _possibleConstructorReturn(this, (IgTopImages.__proto__ || Object.getPrototypeOf(IgTopImages)).call(this, props));

        _this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color
        };
        return _this;
    }

    _createClass(IgTopImages, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var _this2 = this;

            var filters = {};
            if (this.props.size > 0) {
                filters.size = this.props.size;
            }
            filters = Object.assign({}, filters, this.props.filters);
            var params = Object.assign({
                recipe_id: 'module_ig_top_post',
                rule_id: this.state.moduleConfig.search_id
            }, setDates(this.props.dateRange, this.props.withCache), {
                filters: Object.assign({}, filters, {
                    top: this.props.fields.top
                })
            });

            if (this.props.size) {
                params.size = this.props.size;
            }

            var showChildTweet = this.state.moduleConfig.show_child_tweet || false;

            fetchIgTopImages(this.props.services.twitter, params, showChildTweet).subscribe(function (data) {
                return _this2.setState({
                    moduleData: data,
                    moduleLoading: false
                });
            }, function (error) {
                console.log(error);
            }, null);
        }
    }, {
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProp) {
            var _this3 = this;

            var filters = {};
            if (nextProp.size > 0) {
                filters.size = nextProp.size;
            }
            filters = Object.assign({}, filters, nextProp.filters);
            var params = Object.assign({
                recipe_id: 'module_ig_top_post',
                rule_id: nextProp.fields.search_id
            }, setDates(nextProp.dateRange, this.props.withCache), {
                filters: Object.assign({}, filters)
            });

            if (this.props.size) {
                params.size = this.props.size;
            }

            var showChildTweet = this.state.moduleConfig.show_child_tweet || false;

            fetchIgTopImages(this.props.services.twitter, params, showChildTweet).subscribe(function (data) {
                _this3.setState({
                    moduleConfig: nextProp.fields,
                    moduleData: data,
                    moduleLoading: false,
                    moduleSide: nextProp.side,
                    moduleColor: nextProp.color
                });
            }, function (error) {
                console.log(error);
            }, null);
        }
    }, {
        key: 'renderByViz',
        value: function renderByViz() {
            var _state = this.state,
                moduleConfig = _state.moduleConfig,
                moduleData = _state.moduleData;
            var _props = this.props,
                _props$dateRange = _props.dateRange,
                startDate = _props$dateRange.startDate,
                endDate = _props$dateRange.endDate,
                showNoDataYet = _props.showNoDataYet;

            var renderNoDataYet = null;

            if (showNoDataYet && moment().diff(endDate, 'minutes') <= 0 && endDate.diff(startDate, 'days') <= 1) {
                renderNoDataYet = function renderNoDataYet() {
                    return React.createElement(NoDataYet, null);
                };
            }
            if (moduleData) {
                var visualizations = {
                    content_carousel: function content_carousel(params) {
                        return React.createElement(ContentCarousel, Object.assign({}, params, { noDataYet: renderNoDataYet }));
                    },
                    slider_top_posts: function slider_top_posts(params) {
                        return React.createElement(SliderTopPost, Object.assign({}, params, { noDataYet: renderNoDataYet }));
                    }
                };

                return visualizations[this.props.view || moduleConfig.visualization](this.state);
            }

            return React.createElement(LoadingModule, null);
        }
    }, {
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'm-shared-content' },
                this.renderByViz()
            );
        }
    }]);

    return IgTopImages;
}(Component);

IgTopImages.propTypes = propTypes;
IgTopImages.defaultProps = defaultProps;

export default IgTopImages;