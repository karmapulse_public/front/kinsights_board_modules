var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import NoDataModule from '../../../NoDataModule';
import { numberWithCommas } from '../../../../helpers/number';
import LikeIcon from '../../../../helpers/icons/ceo/likesIcon';
import ComentsIcon from '../../../../helpers/icons/ceo/dialogIcon';

import { AngleLeft, AngleRight } from '../../../../helpers/icons/gob';
import styles from './contentCarouselStyles';

var propTypes = {
    moduleData: PropTypes.array,
    moduleConfig: PropTypes.object,
    moduleColor: PropTypes.string,
    moduleSide: PropTypes.string,
    noDataYet: PropTypes.func
};

var defaultProps = {
    moduleData: [],
    moduleConfig: {},
    moduleColor: '#666',
    moduleSide: 'left',
    noDataYet: null
};
var formatBody = function formatBody(body) {
    if (body.length > 70) {
        return body.substring(0, 65) + '...';
    }
    return body;
};

var ContentCarousel = function (_Component) {
    _inherits(ContentCarousel, _Component);

    function ContentCarousel(props) {
        _classCallCheck(this, ContentCarousel);

        var _this = _possibleConstructorReturn(this, (ContentCarousel.__proto__ || Object.getPrototypeOf(ContentCarousel)).call(this, props));

        _this.state = {
            igActive: 0
        };
        _this.handleNext = _this.handleNext.bind(_this);
        _this.handlePrev = _this.handlePrev.bind(_this);
        return _this;
    }

    _createClass(ContentCarousel, [{
        key: 'handlePrev',
        value: function handlePrev() {
            if (this.state.igActive > 0) {
                this.setState({
                    igActive: this.state.igActive - 1
                });
            }
        }
    }, {
        key: 'handleNext',
        value: function handleNext() {
            var igActive = this.state.igActive + 1;
            if (igActive <= this.props.moduleData.length - 1 && this.state.igActive < 2) {
                this.setState({
                    igActive: igActive
                });
            }
        }
    }, {
        key: 'renderigActive',
        value: function renderigActive() {
            var igActive = this.state.igActive;

            var ig = this.props.moduleData[igActive];
            var igDate = moment(ig.ig.postedTime).format('DD MMM YY');
            var etiquetas = ig.ig.displayName;

            var types = function types() {
                if (etiquetas === 'IMAGE') {
                    return 'Imagen';
                }
                if (etiquetas === 'VIDEO') {
                    return 'Video';
                }
                return 'Carrusel';
            };

            return React.createElement(
                'div',
                { className: 'carousel__ig' },
                React.createElement(
                    'h4',
                    null,
                    React.createElement(
                        'span',
                        null,
                        igActive + 1 + '.'
                    ),
                    React.createElement(
                        'span',
                        null,
                        types()
                    ),
                    React.createElement(
                        'span',
                        null,
                        igDate
                    )
                ),
                React.createElement(
                    'a',
                    {
                        href: ig.ig.link,
                        target: '_blank',
                        rel: 'noopener noreferrer'
                    },
                    React.createElement('div', {
                        style: { backgroundImage: 'url(' + (ig.ig.image ? ig.ig.image.replace('http:', 'https:') : '') + ')' }
                    }),
                    React.createElement('p', {
                        dangerouslySetInnerHTML: {
                            __html: formatBody(ig.ig.body)
                        }
                    })
                ),
                React.createElement(
                    'div',
                    { className: 'ig__interactions' },
                    React.createElement(
                        'div',
                        null,
                        React.createElement(LikeIcon, { color: 'rgba(0,0,0,0.5)' }),
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'span',
                                null,
                                'Likes:'
                            ),
                            React.createElement(
                                'span',
                                null,
                                numberWithCommas(ig.ig.favorites)
                            )
                        )
                    ),
                    React.createElement(
                        'div',
                        null,
                        React.createElement(ComentsIcon, { color: 'rgba(0,0,0,0.5)' }),
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'span',
                                null,
                                'Comentarios:'
                            ),
                            React.createElement(
                                'span',
                                null,
                                numberWithCommas(ig.ig.coments)
                            )
                        )
                    )
                )
            );
        }
    }, {
        key: 'renderControls',
        value: function renderControls() {
            var opacityNext = 1;
            if (this.props.moduleData.length - 1 === this.state.igActive || this.state.igActive === 2) {
                opacityNext = 0.2;
            }

            return React.createElement(
                'div',
                { className: 'controls' },
                React.createElement(
                    'button',
                    {
                        onClick: this.handlePrev
                    },
                    React.createElement(AngleLeft, {
                        size: '13px',
                        color: '#000',
                        opacity: this.state.igActive === 0 ? 0.2 : 1
                    })
                ),
                React.createElement(
                    'button',
                    {
                        onClick: this.handleNext
                    },
                    React.createElement(AngleRight, {
                        size: '13px',
                        color: '#000',
                        opacity: opacityNext
                    })
                )
            );
        }
    }, {
        key: 'render',
        value: function render() {
            var _this2 = this;

            var _props$moduleConfig$t = this.props.moduleConfig.title,
                title = _props$moduleConfig$t === undefined ? 'CONTENIDO MÁS COMPARTIDO' : _props$moduleConfig$t;

            return React.createElement(
                'div',
                Object.assign({
                    className: 'content_carousel'
                }, styles(this.props.moduleSide, this.props.moduleColor)),
                React.createElement(
                    'div',
                    { className: 'carousel__label' },
                    React.createElement(
                        'h3',
                        null,
                        title
                    )
                ),
                function () {
                    if (_this2.props.noDataYet !== null) {
                        return _this2.props.noDataYet();
                    }

                    return _this2.props.moduleData.length > 0 ? React.createElement(
                        'div',
                        { className: 'carousel__content' },
                        _this2.renderigActive(),
                        _this2.renderControls()
                    ) : React.createElement(NoDataModule, { color: _this2.props.moduleColor });
                }()
            );
        }
    }]);

    return ContentCarousel;
}(Component);

ContentCarousel.propTypes = propTypes;
ContentCarousel.defaultProps = defaultProps;

export default ContentCarousel;