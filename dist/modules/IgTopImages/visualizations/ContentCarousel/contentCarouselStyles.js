import { css } from 'glamor';

var styles = function styles(side, color) {
    var bgColor = 'transparent';

    return css({
        minWidth: 300,
        height: '100%',
        backgroundColor: bgColor,
        color: 'rgba(0,0,0, 0.5)',
        ' .carousel__label': {
            '> h3': {
                padding: '15px 20px 20px',
                textTransform: 'uppercase',
                fontSize: 16,
                fontWeight: 400,
                textAlign: side,
                color: color
            }
        },
        ' .carousel__content': {
            padding: 30,
            minHeight: 271,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between'
        },
        ' .carousel__ig': {
            ' >span': {
                display: 'flex',
                position: 'relative',
                left: 124,
                fontSize: 13,
                fontWeight: 500,
                maxHeight: '3em',
                paddingBottom: 6,
                ' p': {
                    fontWeight: 700,
                    fontSize: 16,
                    color: '#000000',
                    marginLeft: 5
                }
            },
            ' h4': {
                borderTop: '1px solid #dfdfdf',
                fontSize: 13,
                fontWeight: 500,
                verticalAlign: 'middle',
                textTransform: 'uppercase',
                ' span': {
                    display: 'inline-block',
                    verticalAlign: 'middle',
                    textTransform: 'capitalize',
                    backgroundColor: '#d8d8d8',
                    '&:first-of-type': {
                        backgroundColor: color,
                        color: '#fff',
                        width: 40,
                        height: 40,
                        lineHeight: '40px',
                        textAlign: 'center',
                        verticalAlign: 'top'
                    },
                    '&:nth-of-type(2)': {
                        padding: '5px 10px',
                        color: '#000000'
                    },
                    '&:last-of-type': {
                        float: 'right',
                        backgroundColor: '#fff',
                        lineHeight: '23px',
                        fontSize: 13,
                        fontWeight: 700,
                        maxHeight: '3em'
                    }
                }
            },
            ' a': {
                color: 'rgba(0,0,0,0.5)',
                textDecoration: 'none',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'flex-start',
                marginBottom: 35,
                ' >div': {
                    minWidth: 40,
                    height: 40,
                    backgroundSize: 'cover'
                },
                ' p': {
                    marginLeft: 20,
                    fontSize: 13,
                    fontWeight: 500,
                    maxHeight: '3em',
                    overflow: 'hidden'
                },
                ':hover': {
                    ' p': {
                        color: 'rgba(0,0,0,1)',
                        textDecoration: 'underline'
                    }
                }
            },
            ' .ig__interactions': {
                fontWeight: 500,
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'flex-start',
                ' >div': {
                    display: 'flex',
                    alignItems: 'center'
                },
                ' >div:first-child': {
                    marginRight: 15
                },
                ' svg': {
                    marginRight: 10
                },
                ' span': {
                    fontSize: 13,
                    fontWeight: 700,
                    color: '#000',
                    display: 'block'
                },
                ' span:first-child': {
                    color: 'rgba(0,0,0,0.5)',
                    marginBottom: 3
                }
            }
        },
        ' .list-interactions': {
            ' &__label': {
                display: 'flex',
                justifyContent: 'space-between',
                padding: '15px 20px 15px 20px',
                borderTop: '1px solid #dfdfdf',
                ' h3': {
                    fontSize: '13px',
                    fontWeight: 600
                }
            }
        },
        ' .controls': {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-end',
            ' button': {
                width: 43,
                height: 43,
                border: 'none',
                backgroundColor: '#eee',
                padding: 0,
                '&:hover': {
                    cursor: 'pointer'
                },
                '&:first-child': {
                    marginRight: 5
                }
            }
        }
    });
};

export default styles;