import React from 'react';
import PropTypes from 'prop-types';
import Slider from 'kpulse_design_system/dist/Carousel';

import { faInstagram } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import NoDataModule from '../../../NoDataModule';
import { numberWithCommas } from '../../../../helpers/number';
import Gallery from '../../../../helpers/customComponents/Gallery';
import BodyText from '../../../../helpers/customComponents/BodyText';
import Card from '../../../../helpers/customComponents/Card';
import Video from '../../../../helpers/customComponents/Video';
import styles from './contentCarouselStyles';

var propTypes = {
    moduleData: PropTypes.array,
    moduleConfig: PropTypes.object,
    moduleColor: PropTypes.string,
    moduleSide: PropTypes.string,
    noDataYet: PropTypes.func
};

var defaultProps = {
    moduleData: [],
    moduleConfig: {},
    moduleColor: '#666',
    moduleSide: 'left',
    noDataYet: null
};
var formatBody = function formatBody(body) {
    if (body.length > 70) {
        return body.substring(0, 65) + '...';
    }
    return body;
};

var SliderTopPost = function SliderTopPost(_ref) {
    var moduleData = _ref.moduleData,
        moduleConfig = _ref.moduleConfig,
        moduleColor = _ref.moduleColor,
        noDataYet = _ref.noDataYet;


    var renderContent = function renderContent() {
        return React.createElement(
            'div',
            { style: { position: 'relative' } },
            React.createElement(
                Slider,
                { width: '100%', height: '410px', dotsColor: '#b38e5d', arrowsColor: '#d4c19c', arrowsHeight: '20px' },
                moduleData.map(function (item, index) {
                    return React.createElement(
                        'div',
                        { className: 'card', key: index },
                        React.createElement(
                            'div',
                            { className: 'card__number' },
                            React.createElement(
                                'p',
                                null,
                                index + 1
                            )
                        ),
                        React.createElement(
                            'div',
                            { className: 'card__data' },
                            React.createElement(
                                Card,
                                { key: index },
                                React.createElement(BodyText, { text: formatBody(item.ig.body), type: 'instagram' }),
                                function () {
                                    if (item.ig.displayName === 'IMAGE') {
                                        return React.createElement(Gallery, {
                                            items: [{ url: item.ig.image }],
                                            width: '100%',
                                            height: '212px',
                                            type: 'instagram',
                                            url: [item.ig.link]
                                        });
                                    }
                                    if (item.ig.displayName === 'VIDEO') {
                                        var soruces = [{
                                            url: item.ig.link
                                        }];
                                        return React.createElement(Video, {
                                            sources: soruces,
                                            height: '212px'
                                        });
                                    }
                                    if (item.ig.displayName === 'CAROUSEL_ALBUM') {
                                        return React.createElement(Gallery, {
                                            items: [{ url: item.ig.image }],
                                            width: '100%',
                                            height: '212px',
                                            type: 'instagram',
                                            url: [item.ig.link]
                                        });
                                    }
                                    return null;
                                }()
                            )
                        ),
                        React.createElement(
                            'div',
                            { className: 'card__interactions' },
                            React.createElement(
                                'h5',
                                null,
                                React.createElement(
                                    'span',
                                    null,
                                    numberWithCommas(item.ig.favorites)
                                ),
                                ' Me gusta'
                            ),
                            React.createElement(
                                'h5',
                                null,
                                React.createElement(
                                    'span',
                                    null,
                                    numberWithCommas(item.ig.coments)
                                ),
                                ' Comentarios'
                            )
                        )
                    );
                })
            )
        );
    };

    return React.createElement(
        'div',
        Object.assign({
            className: 'top-posts'
        }, styles),
        React.createElement(
            'div',
            { className: 'top-posts__label' },
            React.createElement(
                'h3',
                null,
                'PUBLICACIONES CON M\xC1S INTERACCI\xD3N'
            ),
            React.createElement(
                'div',
                null,
                React.createElement(FontAwesomeIcon, { icon: faInstagram, color: 'white' })
            )
        ),
        function () {
            if (noDataYet !== null) {
                return noDataYet();
            }

            return moduleData.length > 0 ? renderContent() : React.createElement(NoDataModule, { color: moduleColor });
        }()
    );
};

SliderTopPost.propTypes = propTypes;
SliderTopPost.defaultProps = defaultProps;

export default SliderTopPost;