import { css } from 'glamor';

var styles = css({
    ' .top-posts': {
        ' &__label': {
            display: 'flex',
            paddingLeft: '18px',
            alignItems: 'center',
            justifyContent: 'space-between',
            borderBottom: '1px solid rgba(212, 193, 156, 0.5)',
            ' h3': {
                fontSize: '14px',
                fontWeight: 700,
                color: '#b38e5d'
            },
            ' div': {
                width: '44px',
                height: '44px',
                display: 'flex',
                alignItems: 'center',
                background: '#bc2a8d',
                justifyContent: 'center'
            }
        }
    },
    ' .carousel': {
        '&-items__item': {
            height: '100%'
        },
        '&-ctrDots--bottom': {
            marginTop: '20px !important'
        },
        '&-ctrArrow__left': {
            left: '15px !important'
        },
        '&-ctrArrow__right': {
            right: '15px !important',
            left: 'auto !important'
        }
    },
    ' .card': {
        display: 'flex',
        width: '100%',
        height: '100%',
        flexWrap: 'wrap',
        justifyContent: 'center',
        ' .custom-gallery': {
            padding: 0
        },
        '&__number': {
            position: 'absolute',
            top: 0,
            left: 0,
            ' >p': {
                width: '45px',
                height: '45px',
                display: 'flex',
                color: '#ffffff',
                alignItems: 'center',
                background: '#b38e5d',
                justifyContent: 'center'
            }
        },
        '&__data': {
            display: 'flex',
            width: 360,
            ' section': {
                display: 'inherit',
                padding: 0,
                margin: 0,
                flexDirection: 'column-reverse',
                boxShadow: 'none',
                ' > p:first-of-type': {
                    padding: 0
                }
            }
        },
        '&__interactions': {
            position: 'relative',
            display: 'flex',
            width: 360,
            marginBottom: '2px',
            alignItems: 'center',
            alignSelf: 'flex-end',
            ' >h5': {
                fontSize: 14,
                display: 'flex',
                fontWeight: 500,
                alignItems: 'center',
                flexDirection: 'column',
                justifyContent: 'center',
                color: 'rgba(78, 35, 46,0.5)',
                ' span': {
                    marginBottom: 5,
                    display: 'block',
                    color: '#4e232e',
                    fontSize: 14,
                    fontWeight: 700,
                    textAlign: 'center'
                }
            },
            ' > h5:not(:first-of-type)': {
                marginLeft: 50
            },
            ' &::after': {
                content: '""',
                position: 'absolute',
                top: -12,
                left: 0,
                width: '100%',
                height: 2,
                backgroundColor: 'rgba(212, 193, 156, 0.5)'
            }
        }
    }
});

export default styles;