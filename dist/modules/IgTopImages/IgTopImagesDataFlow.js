import { Observable } from 'rxjs';
import { fetchXHR } from '../../helpers/http';

export default (function (url, params) {
    return Observable.create(function (observer) {
        Observable.fromPromise(fetchXHR(url, 'GET', params)).subscribe(function (data) {
            var ig = void 0;
            var formatHits = data.json.response.map(function (item) {
                ig = item;
                return {
                    ig: {
                        id: ig._source.id,
                        body: ig._source.body || '',
                        displayName: ig._source.media_type,
                        image: ig._source.media_url,
                        postedTime: ig._source.posted_time,
                        link: ig._source.link,
                        totalInteraction: ig._source.engagement_count,
                        coments: ig._source.comments_count,
                        favorites: ig._source.likes_count
                    }
                };
            });

            observer.next(formatHits);
        }, function (error) {
            observer.error(error);
        });
    });
});