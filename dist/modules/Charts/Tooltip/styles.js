import css from 'glamor-jss';

export var styles = function styles() {
    return css({
        backgroundColor: 'rgba(255, 255, 255, 0.85)',
        boxShadow: '2px 2px 10px rgba(0, 0, 0, .2)',
        color: '#444',
        padding: '10px',
        pointerEvents: 'none',
        position: 'absolute',
        display: 'block',
        transition: 'all 0.25s ease',
        '& p, h1, h2, h3, h4': {
            margin: 0,
            borderRadius: 2,
            letterSpacing: '1px',
            lineHeight: '20px',
            fontFamily: 'arial',
            fontSize: '12px'
        }
    });
};

export default styles;