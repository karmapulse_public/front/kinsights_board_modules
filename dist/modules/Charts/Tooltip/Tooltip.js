import React from 'react';
import { styles } from './styles';

var Tooltip = function Tooltip(props) {
    if (props.isEnabled) {
        var ocultar = props.x != '0' ? props.x : '-50px';
        return React.createElement(
            'div',
            Object.assign({}, styles(), {
                className: 'react-chartsjs__tooltip',
                style: { left: ocultar, top: props.y, zIndex: 100 }
            }),
            props.children
        );
    }
    return null;
};

export default Tooltip;