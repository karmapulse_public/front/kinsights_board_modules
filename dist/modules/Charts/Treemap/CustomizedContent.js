import React from 'react';

/* eslint no-mixed-operators:
    ["error", {"groups": [["&", "|", "^", "~", "<<", ">>", ">>>"], ["&&", "||"]]}]
*/

var CustomizedContent = function CustomizedContent(props) {
    var root = props.root,
        depth = props.depth,
        x = props.x,
        y = props.y,
        width = props.width,
        height = props.height,
        index = props.index,
        payload = props.payload,
        colors = props.colors,
        rank = props.rank,
        name = props.name,
        onDataClick = props.onDataClick;

    var color = props.depth === 2 ? props.root.color : null;

    return React.createElement(
        'g',
        { onClick: function onClick() {
                return onDataClick(props);
            } },
        React.createElement('rect', {
            x: x,
            y: y,
            width: width,
            height: height,
            style: {
                fill: color,
                stroke: '#fff',
                strokeWidth: 1.5
                // strokeWidth: 2 / (depth + 1e-10),
                // strokeOpacity: 1 / (depth + 1e-10),
            }
        }),
        function () {
            if (depth !== 1) {
                return React.createElement(
                    'text',
                    {
                        x: x + 5,
                        y: y + 15,
                        stroke: 'none',
                        textAnchor: 'start'
                    },
                    name
                );
            }
            return null;
        }()
    );
};

export default CustomizedContent;