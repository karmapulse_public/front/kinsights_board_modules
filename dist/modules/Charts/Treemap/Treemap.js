import React from 'react';
import { Treemap as TreemapRe, Tooltip, ResponsiveContainer } from 'recharts';
import CustomizedContent from './CustomizedContent';
import { propTypes, defaultProps } from './defaultValues';

var NewToolTip = function NewToolTip(props) {
    var payload = props.payload.length >= 1 ? props.payload[0].payload : { name: '', value: '' };
    return React.createElement(
        'div',
        { className: 'react-chartsjs__tooltip', style: { position: 'relative' } },
        payload.name,
        ' : ',
        payload.value
    );
};

var Treemap = function Treemap(props) {
    return React.createElement(
        ResponsiveContainer,
        { width: props.width, height: props.height },
        React.createElement(
            TreemapRe,
            {
                data: props.data,
                dataKey: props.dataKey,
                ratio: 1 / 3,
                stroke: props.stroke,
                fill: props.fill,
                fontFamily: props.fontFamily,
                fontSize: props.fontSize,
                content: React.createElement(CustomizedContent, {
                    colors: props.colors,
                    onDataClick: props.onDataClick
                })
            },
            props.enableTooltip ? React.createElement(Tooltip, { content: props.tooltip ? props.tooltip : React.createElement(NewToolTip, null)
            }) : null
        )
    );
};

Treemap.propTypes = propTypes;
Treemap.defaultProps = defaultProps;

export default Treemap;