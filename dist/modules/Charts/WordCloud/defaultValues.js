import PropTypes from 'prop-types';
import * as d3Array from 'd3-array';
import * as d3Scale from 'd3-scale';
import * as d3Selection from 'd3-selection';
import * as d3SelectionMulti from 'd3-selection-multi';
import * as d3ScaleChromatic from 'd3-scale-chromatic';

export var D3 = Object.assign({}, d3Array, d3Scale, d3Selection, d3SelectionMulti, d3ScaleChromatic);

export var MIN_HEIGHT = 200;
export var MIN_WIDTH = 200;
export var DEFAULT_COLORS = D3.schemeCategory10;

export var propTypes = {
    words: PropTypes.array,
    wordCountKey: PropTypes.string,
    wordKey: PropTypes.string,
    colors: PropTypes.array,
    fontFamily: PropTypes.string,
    height: PropTypes.number,
    maxAngle: PropTypes.number,
    maxWords: PropTypes.number,
    minAngle: PropTypes.number,
    orientations: PropTypes.number,
    scale: PropTypes.oneOf(['sqrt', 'log', 'linear', 'fontSize']),
    fontScale: PropTypes.shape({
        minFont: PropTypes.number,
        maxFont: PropTypes.number
    }),
    spiral: PropTypes.oneOf(['archimedean', 'rectangular']),
    enableTooltip: PropTypes.bool,
    transitionDuration: PropTypes.number,
    width: PropTypes.number,
    colorScale: PropTypes.func,
    tooltip: PropTypes.func,
    onWordClick: PropTypes.func
};

export var defaultProps = {
    colors: DEFAULT_COLORS,
    fontFamily: 'impact',
    fontScale: {
        minFont: 12,
        maxFont: 40
    },
    height: null,
    maxAngle: 0,
    maxWords: 300,
    minAngle: 0,
    orientations: 1,
    scale: 'fontSize',
    spiral: 'rectangular',
    enableTooltip: false,
    transitionDuration: 1000,
    width: null
};