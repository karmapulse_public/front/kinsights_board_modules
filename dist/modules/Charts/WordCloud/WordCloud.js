var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React from 'react';
import invariant from 'invariant';
import d3WordCloud from './classD3';
import Tooltip from '../Tooltip/Tooltip';

import { propTypes, defaultProps } from './defaultValues';

var WordCloud = function (_React$Component) {
    _inherits(WordCloud, _React$Component);

    function WordCloud() {
        var _ref;

        var _temp, _this, _ret;

        _classCallCheck(this, WordCloud);

        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = WordCloud.__proto__ || Object.getPrototypeOf(WordCloud)).call.apply(_ref, [this].concat(args))), _this), _this.container = React.createRef(), _this.chart = React.createRef(), _this.state = {
            tooltipContent: '',
            enableTooltip: _this.props.enableTooltip,
            tooltipX: 0,
            tooltipY: 0
        }, _this.tooltipState = function (arrs) {
            _this.setState(arrs);
        }, _this.validateProps = function () {
            var _this$props = _this.props,
                maxAngle = _this$props.maxAngle,
                minAngle = _this$props.minAngle,
                words = _this$props.words,
                wordCountKey = _this$props.wordCountKey,
                wordKey = _this$props.wordKey;

            invariant(Math.abs(minAngle) <= 90 && Math.abs(maxAngle) <= 90, 'Angles must have values between -90 to 90 degrees');
            invariant(minAngle <= maxAngle, 'minAngle must be <= maxAngle');
            if (words.length > 0) {
                var firstRow = words[0];
                // invariant(
                //     wordKey in firstRow,
                //     'Word key must be a valid key in the data'
                // );
                // invariant(
                //     wordCountKey in firstRow,
                //     'Word count key must be a valid key in the data'
                // );
            }
        }, _temp), _possibleConstructorReturn(_this, _ret);
    } // eslint-disable-line
    // eslint-disable-line

    _createClass(WordCloud, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            this.validateProps();
            d3WordCloud.create(this.props, Object.assign({}, {
                chart: this.chart.current,
                container: this.container.current,
                tooltipState: this.tooltipState
            }));
        }
    }, {
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProps) {
            d3WordCloud.update(nextProps, Object.assign({}, {
                chart: this.chart.current,
                container: this.container.current,
                tooltipState: this.tooltipState
            }));
        }
    }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            d3WordCloud.destroy(this.chart.current);
        }
    }, {
        key: 'render',
        value: function render() {
            var _state = this.state,
                tooltipContent = _state.tooltipContent,
                enableTooltip = _state.enableTooltip,
                tooltipX = _state.tooltipX,
                tooltipY = _state.tooltipY;


            return React.createElement(
                'div',
                { ref: this.container },
                React.createElement('div', { ref: this.chart }),
                React.createElement(
                    Tooltip,
                    { x: tooltipX, y: tooltipY, isEnabled: enableTooltip },
                    React.createElement(
                        'h1',
                        null,
                        tooltipContent
                    )
                )
            );
        }
    }]);

    return WordCloud;
}(React.Component);

WordCloud.propTypes = propTypes;
WordCloud.defaultProps = defaultProps;

export default WordCloud;