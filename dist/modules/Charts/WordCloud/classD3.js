import cloud from 'd3-cloud';
import { event as currentEvent } from 'd3-selection';

import uniqBy from 'lodash/uniqBy';
import isNull from 'lodash/isNull';
import maxBy from 'lodash/maxBy';
import minBy from 'lodash/minBy';
import { D3, MIN_HEIGHT, MIN_WIDTH, DEFAULT_COLORS } from './defaultValues';

export default (function () {
    // let _fontScale;
    var _height = void 0;
    var _layout = void 0;
    var _svg = void 0;
    var _vis = void 0;
    var _width = void 0;
    var _words = void 0;
    var _fontScale = void 0;

    var PROPS = {};
    var CONFIG = {};

    var setDimensions = function setDimensions(height, width) {
        var _CONFIG = CONFIG,
            container = _CONFIG.container;
        var _container$parentNode = container.parentNode,
            parentHeight = _container$parentNode.offsetHeight,
            parentWidth = _container$parentNode.offsetWidth;
        // console.log(parentHeight);

        _height = height || parentHeight;
        _width = width || parentWidth;

        if (typeof _height !== 'number' || _height < MIN_HEIGHT) {
            console.warn('Invalid/small height provided, falling back to minimum value of ' + MIN_HEIGHT);
            _height = MIN_HEIGHT;
        }

        if (typeof _width !== 'number' || _width < MIN_WIDTH) {
            console.warn('Invalid/small width provided, falling back to minimum value of ' + MIN_HEIGHT);
            _width = MIN_HEIGHT;
        }
    };

    var getScale = function getScale(scale) {
        var scales = {
            linear: D3.scaleLinear,
            log: D3.scaleLog,
            sqrt: D3.scaleSqrt,
            fontSize: null
        };
        return scales[scale];
    };

    var fontSize = function fontSize(value) {
        var _PROPS = PROPS,
            words = _PROPS.words,
            _PROPS$fontScale = _PROPS.fontScale,
            minFont = _PROPS$fontScale.minFont,
            maxFont = _PROPS$fontScale.maxFont;

        var initialValues = {
            MAX: maxBy(words, 'total'),
            MIN: minBy(words, 'total'),
            FONT_MAX: maxFont,
            FONT_MIN: minFont
        };
        var result = value === initialValues.MIN.total ? initialValues.FONT_MIN : value / initialValues.MAX.total * (initialValues.FONT_MAX - initialValues.FONT_MIN) + initialValues.FONT_MIN;
        return result;
    };

    var chooseRandom = function chooseRandom(array) {
        return array[Math.floor(Math.random() * array.length)];
    };

    var onMouseOver = function onMouseOver(d) {
        var _PROPS2 = PROPS,
            enableTooltip = _PROPS2.enableTooltip,
            wordKey = _PROPS2.wordKey,
            wordCountKey = _PROPS2.wordCountKey,
            tooltip = _PROPS2.tooltip;

        var tooltipContent = tooltip ? tooltip(d) : d[wordKey] + ' (' + d[wordCountKey] + ')';
        if (enableTooltip) {
            CONFIG.tooltipState({
                tooltipContent: tooltipContent,
                enableTooltip: true,
                tooltipX: currentEvent.pageX,
                tooltipY: currentEvent.pageY - 28
            });
        }
    };

    var onMouseOut = function onMouseOut() {
        if (PROPS.enableTooltip) {
            CONFIG.tooltipState({
                enableTooltip: false
            });
        }
    };

    var colorsScale = function colorsScale(d, i) {
        var _PROPS3 = PROPS,
            colorScale = _PROPS3.colorScale,
            colors = _PROPS3.colors;

        return colorScale ? colorScale(d, i, function () {
            return chooseRandom(colors || DEFAULT_COLORS);
        }) : chooseRandom(colors || DEFAULT_COLORS);
    };

    var transformText = function transformText(d) {
        var translate = 'translate(' + d.x + ', ' + d.y + ')';
        var rotate = typeof d.rotate === 'number' ? 'rotate(' + d.rotate + ')' : '';
        return translate + rotate;
    };

    var draw = function draw(words) {
        var _PROPS4 = PROPS,
            fontFamily = _PROPS4.fontFamily,
            transitionDuration = _PROPS4.transitionDuration,
            onWordClick = _PROPS4.onWordClick,
            wordKey = _PROPS4.wordKey;

        _words = _vis.selectAll('text').data(words);
        _words.enter().append('text').on('click', onWordClick).on('mouseover', onMouseOver).on('mouseout', onMouseOut).attrs({
            cursor: onWordClick ? 'pointer' : 'default',
            fill: colorsScale,
            'font-family': fontFamily,
            'text-anchor': 'middle',
            transform: 'translate(0, 0) rotate(0)'
        }).transition().duration(transitionDuration).attrs({
            'font-size': function fontSize(d) {
                return d.size + 'px';
            },
            transform: transformText
        }).text(function (d) {
            return d[wordKey];
        });

        _words.transition().duration(transitionDuration).attrs({
            fill: colorsScale,
            'font-family': fontFamily,
            'font-size': function fontSize(d) {
                return d.size + 'px';
            },
            transform: transformText
        }).text(function (d) {
            return d[wordKey];
        });

        _words.exit().transition().duration(transitionDuration).attr('fill-opacity', 0).remove();
    };

    var update = function update(props, config) {
        PROPS = props;
        CONFIG = config;
        var _PROPS5 = PROPS,
            fontFamily = _PROPS5.fontFamily,
            height = _PROPS5.height,
            maxAngle = _PROPS5.maxAngle,
            maxWords = _PROPS5.maxWords,
            minAngle = _PROPS5.minAngle,
            orientations = _PROPS5.orientations,
            scale = _PROPS5.scale,
            spiral = _PROPS5.spiral,
            width = _PROPS5.width,
            wordCountKey = _PROPS5.wordCountKey,
            words = _PROPS5.words,
            wordKey = _PROPS5.wordKey;


        setDimensions(height, width);
        _svg.attrs({
            height: _height,
            width: _width
        }).style('background', 'transparent');

        _vis.attr('transform', 'translate(' + _width / 2 + ', ' + _height / 2 + ')scale(1.0020877122879028,1.0020877122879028)');

        var d3Scale = getScale(scale);
        var filteredWords = words.slice(0, maxWords);

        if (!isNull(d3Scale)) {
            _fontScale = uniqBy(filteredWords, wordCountKey).length > 1 ? d3Scale().range([10, 100]) : d3Scale().range([100, 100]);
            if (filteredWords.length) {
                _fontScale.domain([D3.min(filteredWords, function (d) {
                    return d[wordCountKey];
                }), D3.max(filteredWords, function (d) {
                    return d[wordCountKey];
                })]);
            }
        } else {
            _fontScale = fontSize;
        }

        if (typeof orientations === 'number' && orientations > 0) {
            var rotations = [];
            if (orientations === 1) {
                rotations = [minAngle];
            } else {
                rotations = [minAngle, maxAngle];
                var increment = (maxAngle - minAngle) / (orientations - 1);
                var rotation = minAngle + increment;
                while (rotation < maxAngle) {
                    rotations.push(rotation);
                    rotation += increment;
                }
            }
            _layout.rotate(function () {
                return chooseRandom(rotations);
            });
        }
        _layout.size([_width, _height]).words(filteredWords).padding(1).text(function (d) {
            return d[wordKey];
        }).font(fontFamily).fontSize(function (d) {
            return _fontScale(d[wordCountKey]);
        }).spiral(spiral).on('end', function (words) {
            return draw(words);
        }).start();
        _fontScale = null;
    };

    var create = function create(props, config) {
        PROPS = props;
        CONFIG = config;

        var _PROPS6 = PROPS,
            height = _PROPS6.height,
            width = _PROPS6.width;
        var _CONFIG2 = CONFIG,
            chart = _CONFIG2.chart;


        D3.select(chart).selectAll('*').remove();

        setDimensions(height, width);
        _svg = D3.select(chart).append('svg');
        _vis = _svg.append('g');
        _layout = cloud();
        update(PROPS, CONFIG);
    };

    var destroy = function destroy(node) {
        D3.select(node).selectAll('*').remove();
        _layout = null;
        _svg.remove();
        _vis.remove();
        _words.remove();
        _fontScale = [];
    };

    return {
        create: create,
        update: update,
        destroy: destroy
    };
})();