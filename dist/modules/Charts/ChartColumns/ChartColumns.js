import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import isNull from 'lodash/isNull';
import orderBy from 'lodash/orderBy';
import isUndefined from 'lodash/isUndefined';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer, Label, Cell } from 'recharts';

import AxisX from '../CustomComponents/AxisX';
import AxisY from '../CustomComponents/AxisY';
import LegendCustom from '../CustomComponents/Legend';
import TooltipCustom from '../CustomComponents/TooltipCustom';
import DrawerFeed from '../../DrawerFeed';

var propTypes = {
    data: PropTypes.array,
    dataKeys: PropTypes.array,
    type: PropTypes.string,
    labels: PropTypes.object,
    layout: PropTypes.string,
    nameArray: PropTypes.array,
    onBarClick: PropTypes.func,
    xDataKey: PropTypes.string,
    yDataKey: PropTypes.string,
    intervalType: PropTypes.string,
    customTooltip: PropTypes.element,
    customLegend: PropTypes.element,
    customAxisX: PropTypes.element
};

var defaultProps = {
    data: [],
    dataKeys: {},
    xDataKey: '',
    yDataKey: '',
    nameArray: [],
    type: 'single',
    layout: 'horizontal',
    intervalType: 'month',
    onBarClick: null,
    customTooltip: null,
    customLegend: null,
    customAxisX: null,
    labels: {
        x: '',
        y: ''
    }
};

var ChartColumns = function ChartColumns(_ref) {
    var data = _ref.data,
        dataKeys = _ref.dataKeys,
        nameArray = _ref.nameArray,
        type = _ref.type,
        intervalType = _ref.intervalType,
        labels = _ref.labels,
        onBarClick = _ref.onBarClick,
        xDataKey = _ref.xDataKey,
        yDataKey = _ref.yDataKey,
        customTooltip = _ref.customTooltip,
        customLegend = _ref.customLegend,
        customAxisX = _ref.customAxisX,
        layout = _ref.layout;

    var XDataKey = '';
    if (xDataKey === '' && type === 'single') XDataKey = 'date';else if (xDataKey === '' && type !== 'single') XDataKey = 'denomitation';else XDataKey = xDataKey;

    var renderBarForTypes = function renderBarForTypes() {
        var types = {
            single: function () {
                return dataKeys.map(function (item, index) {
                    return React.createElement(Bar, {
                        key: index,
                        stackId: 'a',
                        fill: item.color,
                        dataKey: item.key
                    });
                });
            }(),
            multiple: function () {
                return dataKeys.map(function (item, index) {
                    if (isUndefined(item.group)) {
                        return React.createElement(Bar, {
                            key: index,
                            fill: item.color,
                            dataKey: item.key
                        });
                    }
                    return React.createElement(Bar, {
                        key: index,
                        fill: item.color,
                        dataKey: item.key,
                        stackId: item.group
                    });
                });
            }(),
            simple: function () {
                return React.createElement(
                    Bar,
                    { dataKey: yDataKey },
                    dataKeys.map(function (item) {
                        return React.createElement(Cell, { key: item.key, fill: item.color });
                    })
                );
            }()
        };
        return types[type];
    };
    var _onClick = isNull(onBarClick) ? function () {
        return {};
    } : onBarClick;

    var contentTooltip = isNull(customTooltip) ? React.createElement(TooltipCustom, {
        nameArray: nameArray,
        intervalType: intervalType === 'hour' ? 'hourPDay' : intervalType
    }) : customTooltip;

    var contentLegend = isNull(customLegend) ? React.createElement(LegendCustom, null) : customLegend;

    var tickAxisX = isNull(customAxisX) ? React.createElement(AxisX, {
        color: 'rgba(0, 0, 0, 0.8)',
        intervalType: intervalType,
        style: {
            fontSize: 10,
            fontWeight: 600,
            strokeWidth: '0.05px',
            stroke: 'rgba(0, 0, 0, 0.8)'
        }
    }) : customAxisX;

    if (layout === 'vertical') {
        return React.createElement(
            Fragment,
            null,
            React.createElement(
                ResponsiveContainer,
                { width: '100%', height: '100%', minWidth: 300, minHeight: 300 },
                React.createElement(
                    BarChart,
                    {
                        layout: 'vertical',
                        data: orderBy(data, ['date'], ['asc']),
                        onClick: function onClick(dataClick) {
                            return _onClick(dataClick);
                        }
                    },
                    React.createElement(
                        XAxis,
                        {
                            type: 'number',
                            tickLine: false,
                            axisLine: false,
                            dataKey: yDataKey,
                            tick: React.createElement(AxisY, {
                                color: 'rgba(0, 0, 0, 0.8)',
                                style: { fontSize: 10, fontWeight: 600, stroke: 'rgba(0, 0, 0, 0.8)', strokeWidth: '0.05px' }
                            })
                        },
                        React.createElement(Label, { className: 'column-data__label', value: labels.y, offset: -15, position: 'insideBottom' })
                    ),
                    React.createElement(
                        YAxis,
                        {
                            type: 'category',
                            tickLine: false,
                            tickMargin: 25,
                            tick: tickAxisX,
                            dataKey: XDataKey,
                            interval: 'preserveStartEnd',
                            stroke: 'rgb(204, 204, 204)'
                        },
                        React.createElement(Label, { className: 'column-data__label', value: labels.x, offset: -15, position: 'left', angle: -90 })
                    ),
                    React.createElement(CartesianGrid, { horizontal: false }),
                    React.createElement(Tooltip, {
                        content: contentTooltip,
                        cursor: { fill: '#000', fillOpacity: 0.05 }
                    }),
                    React.createElement(Legend, {
                        height: 36,
                        nameArray: nameArray,
                        content: contentLegend,
                        styles: {
                            fontSize: 10,
                            fontWeight: 600,
                            justifyContent: 'center',
                            textTransform: 'capitalize'
                        }
                    }),
                    renderBarForTypes()
                )
            ),
            function () {
                if (!isNull(onBarClick)) {
                    return React.createElement(DrawerFeed, null);
                }
                return null;
            }()
        );
    }

    return React.createElement(
        Fragment,
        null,
        React.createElement(
            ResponsiveContainer,
            { width: '100%', height: '100%', minWidth: 300, minHeight: 300 },
            React.createElement(
                BarChart,
                {
                    data: orderBy(data, ['date'], ['asc']),
                    onClick: function onClick(dataClick) {
                        return _onClick(dataClick);
                    }
                },
                React.createElement(
                    XAxis,
                    {
                        type: 'category',
                        tickLine: false,
                        tick: tickAxisX,
                        dataKey: XDataKey,
                        interval: 'preserveStartEnd',
                        stroke: 'rgb(204, 204, 204)'
                    },
                    React.createElement(Label, { className: 'column-data__label', value: labels.x, offset: -15, position: 'insideBottom' })
                ),
                React.createElement(
                    YAxis,
                    {
                        axisLine: false,
                        tickLine: false,
                        tick: React.createElement(AxisY, {
                            color: 'rgba(0, 0, 0, 0.8)',
                            style: { fontSize: 10, fontWeight: 600, stroke: 'rgba(0, 0, 0, 0.8)', strokeWidth: '0.05px' }
                        })
                    },
                    React.createElement(Label, { className: 'column-data__label', value: labels.y, offset: -15, position: 'left', angle: -90 })
                ),
                React.createElement(CartesianGrid, { vertical: false }),
                React.createElement(Tooltip, {
                    content: contentTooltip,
                    cursor: { fill: '#000', fillOpacity: 0.05 }
                }),
                React.createElement(Legend, {
                    height: 36,
                    nameArray: nameArray,
                    content: contentLegend,
                    styles: {
                        fontSize: 10,
                        fontWeight: 600,
                        justifyContent: 'center',
                        textTransform: 'capitalize'
                    }
                }),
                renderBarForTypes()
            )
        ),
        function () {
            if (!isNull(onBarClick)) {
                return React.createElement(DrawerFeed, null);
            }
            return null;
        }()
    );
};

ChartColumns.propTypes = propTypes;
ChartColumns.defaultProps = defaultProps;

export default ChartColumns;