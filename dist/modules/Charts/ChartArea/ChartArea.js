import React from 'react';
import PropTypes from 'prop-types';
import orderBy from 'lodash/orderBy';
import isUndefined from 'lodash/isUndefined';
import { AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

import AxisX from '../CustomComponents/AxisX';
import AxisY from '../CustomComponents/AxisY';
import LegendCustom from '../CustomComponents/Legend';
import TooltipCustom from '../CustomComponents/TooltipCustom';

var propTypes = {
    data: PropTypes.array,
    dataKeys: PropTypes.array,
    nameArray: PropTypes.array,
    type: PropTypes.string,
    hasAxis: PropTypes.bool,
    hasGrid: PropTypes.bool,
    hasLegend: PropTypes.bool
};

var defaultProps = {
    data: [],
    dataKeys: [],
    nameArray: [],
    type: 'single',
    hasAxis: true,
    hasGrid: true,
    hasLegend: true
};

var ChartArea = function ChartArea(_ref) {
    var data = _ref.data,
        dataKeys = _ref.dataKeys,
        nameArray = _ref.nameArray,
        type = _ref.type,
        hasAxis = _ref.hasAxis,
        hasGrid = _ref.hasGrid,
        hasLegend = _ref.hasLegend;

    var renderAreaForTypes = function renderAreaForTypes() {
        var types = {
            single: function () {
                return dataKeys.map(function (item, index) {
                    return React.createElement(Area, {
                        key: index,
                        activeDot: false,
                        dataKey: item.key,
                        stackId: 'a',
                        fill: item.color,
                        stroke: item.color,
                        fillOpacity: 1
                    });
                });
            }(),
            multiple: function () {
                return dataKeys.map(function (item, index) {
                    if (isUndefined(item.group)) {
                        return React.createElement(Area, {
                            key: index,
                            activeDot: false,
                            dataKey: item.key,
                            fill: item.color,
                            stroke: item.color,
                            fillOpacity: 1
                        });
                    }
                    return React.createElement(Area, {
                        key: index,
                        activeDot: false,
                        dataKey: item.key,
                        stackId: item.group,
                        fill: item.color,
                        stroke: item.color,
                        fillOpacity: 1
                    });
                });
            }()

        };
        return types[type];
    };

    return React.createElement(
        ResponsiveContainer,
        null,
        React.createElement(
            AreaChart,
            {
                data: orderBy(data, ['date'], ['asc']),
                margin: { top: 20, right: 10, left: -30, bottom: 5 }
            },
            hasAxis && React.createElement(XAxis, {
                type: 'category',
                dataKey: 'date',
                tickLine: false,
                interval: 'preserveStartEnd',
                stroke: 'rgb(204, 204, 204)',
                tick: React.createElement(AxisX, {
                    color: 'rgba(0, 0, 0, 0.8)',
                    style: { fontSize: 10, fontWeight: 600, stroke: 'rgba(0, 0, 0, 0.8)', strokeWidth: '0.05px' }
                })
            }),
            hasAxis && React.createElement(YAxis, {
                axisLine: false,
                tickLine: false,
                tick: React.createElement(AxisY, {
                    color: 'rgba(0, 0, 0, 0.8)',
                    style: { fontSize: 10, fontWeight: 600, stroke: 'rgba(0, 0, 0, 0.8)', strokeWidth: '0.05px' }
                })
            }),
            hasGrid && React.createElement(CartesianGrid, { vertical: false }),
            React.createElement(Tooltip, { content: React.createElement(TooltipCustom, null) }),
            hasLegend && React.createElement(Legend, { nameArray: nameArray, height: 36, content: React.createElement(LegendCustom, null), styles: { fontSize: 10, justifyContent: 'center', textTransform: 'capitalize', fontWeight: 600 } }),
            renderAreaForTypes()
        )
    );
};

ChartArea.propTypes = propTypes;
ChartArea.defaultProps = defaultProps;

export default ChartArea;