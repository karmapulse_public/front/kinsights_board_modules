import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';

import intervalsStyles from './IntervalsStyles';

var propTypes = {
    dateRange: PropTypes.object,
    intervalType: PropTypes.string,
    handleChangeInterval: PropTypes.func
};

var defaultProps = {
    dateRange: {},
    intervalType: 'month',
    handleChangeInterval: function handleChangeInterval() {
        return {};
    }
};

var Intervals = function Intervals(props) {
    var activeClass = 'interval__scale--active';
    var disableClass = 'interval__scale--inactive';
    var dateDiff = moment(props.dateRange.endDate).diff(props.dateRange.startDate, 'days', true);
    return React.createElement(
        'div',
        Object.assign({ className: 'chart-intervals' }, intervalsStyles()),
        React.createElement(
            'button',
            {
                className: 'interval__scale ' + (props.intervalType === 'hour' ? activeClass : '') + ' ' + (dateDiff > 15 ? disableClass : ''),
                onClick: function onClick() {
                    if (dateDiff <= 15) {
                        props.handleChangeInterval('hour');
                    }
                }
            },
            'Horas'
        ),
        React.createElement(
            'button',
            {
                className: 'interval__scale ' + (props.intervalType === 'day' ? activeClass : '') + ' ' + (dateDiff > 365 ? disableClass : ''),
                onClick: function onClick() {
                    if (dateDiff <= 365) {
                        props.handleChangeInterval('day');
                    }
                }
            },
            'D\xEDas'
        ),
        React.createElement(
            'button',
            {
                className: 'interval__scale ' + (props.intervalType === 'week' ? activeClass : '') + ' ' + (dateDiff > 2555 ? disableClass : ''),
                onClick: function onClick() {
                    if (dateDiff <= 2555) {
                        props.handleChangeInterval('week');
                    }
                }
            },
            'Semanas'
        ),
        React.createElement(
            'button',
            {
                className: 'interval__scale ' + (props.intervalType === 'month' ? activeClass : ''),
                onClick: function onClick() {
                    return props.handleChangeInterval('month');
                }
            },
            'Meses'
        )
    );
};

Intervals.propTypes = propTypes;
Intervals.defaultProps = defaultProps;

export default Intervals;