var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
// import moment from 'moment';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend, Label, ResponsiveContainer } from 'recharts';
import isNull from 'lodash/isNull';

import AxisX from '../CustomComponents/AxisX';
import AxisY from '../CustomComponents/AxisY';
import mapDataHistogram from './MapDataHistogram';
import LegendCustom from '../CustomComponents/Legend';
import TooltipCustom from '../CustomComponents/TooltipCustom';
import balanceJson from '../../../helpers/balanceJson';
import DrawerFeed from '../../DrawerFeed';

var propTypes = {
    data: PropTypes.object.isRequired, // eslint-disable-line
    type: PropTypes.string.isRequired, // eslint-disable-line
    entity: PropTypes.string.isRequired,
    intervalType: PropTypes.string,
    colors: PropTypes.array,
    labels: PropTypes.object,
    onLineClick: PropTypes.func
};

var defaultProps = {
    data: {}, // eslint-disable-line
    colors: ['#00C5FF'],
    type: 'single', // eslint-disable-line
    entity: 'tweets', // eslint-disable-line
    intervalType: 'month',
    onLineClick: null,
    labels: {
        x: '',
        y: ''
    }
};

var Lines = function (_React$Component) {
    _inherits(Lines, _React$Component);

    function Lines(props) {
        _classCallCheck(this, Lines);

        var _this = _possibleConstructorReturn(this, (Lines.__proto__ || Object.getPrototypeOf(Lines)).call(this, props));

        _this.renderLine = _this.renderLine.bind(_this);
        _this.setData = _this.setData.bind(_this);
        _this.state = {
            datum: [],
            hashtags: [],
            keywords: [],
            mentions: []
        };
        return _this;
    }

    _createClass(Lines, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            this.setData(this.props);
        }
    }, {
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProps) {
            this.setData(nextProps);
        }
    }, {
        key: 'setData',
        value: function setData(props) {
            var _this2 = this;

            var type = props.type,
                data = props.data,
                entity = props.entity;

            var types = function types(ty, da) {
                var t = {
                    multiple: function multiple() {
                        var _this2$setState;

                        var datum = void 0;
                        if (entity === 'mentions') {
                            var bu = [];
                            if (da.aggregations.top_mentions) {
                                bu = da.aggregations.top_mentions.buckets;
                            } else if (da.aggregations.mentions) {
                                bu = da.aggregations.mentions.buckets;
                            }
                            var newBuckets = balanceJson(bu, entity);
                            datum = mapDataHistogram(newBuckets, entity);
                        } else if (entity === 'users') {
                            var _bu = [];
                            if (da.aggregations.top_users) {
                                _bu = da.aggregations.top_users.buckets;
                            } else if (da.aggregations.mentions) {
                                _bu = da.aggregations.users.buckets;
                            }
                            var _newBuckets = balanceJson(_bu, entity);
                            datum = mapDataHistogram(_newBuckets, entity);
                        } else if (entity === 'keywords') {
                            var buckets = da.aggregations.keywords.buckets;

                            var _newBuckets2 = balanceJson(buckets, entity);
                            datum = mapDataHistogram(_newBuckets2, entity);
                        } else {
                            var _buckets = da.aggregations.hashtags.buckets;

                            var _newBuckets3 = balanceJson(_buckets, entity);
                            datum = mapDataHistogram(_newBuckets3, entity);
                        }

                        var formatTime = datum.data.map(function (item) {
                            return Object.assign(item, {
                                // key: Date.parse(moment.utc(item.key).utcOffset(n))
                                key: item.key
                            });
                        });

                        var sorted = formatTime.sort(function (a, b) {
                            return a.key - b.key;
                        });

                        _this2.setState((_this2$setState = {}, _defineProperty(_this2$setState, '' + entity, datum['' + entity]), _defineProperty(_this2$setState, 'datum', sorted), _this2$setState));
                    },
                    single: function single() {
                        var _this2$setState2;

                        var buckets = da.aggregations.buckets;

                        _this2.setState((_this2$setState2 = {}, _defineProperty(_this2$setState2, '' + entity, []), _defineProperty(_this2$setState2, 'datum', buckets), _this2$setState2));
                    }
                };
                return t[ty]();
            };
            types(type, data);
        }
    }, {
        key: 'renderLine',
        value: function renderLine() {
            var _props = this.props,
                colors = _props.colors,
                entity = _props.entity,
                onLineClick = _props.onLineClick,
                hasClick = _props.hasClick;

            var onClick = isNull(onLineClick) ? function () {
                return {};
            } : onLineClick;
            var cursor = hasClick ? 'pointer' : 'default';
            if (this.state['' + entity].length > 0) {
                return this.state['' + entity].map(function (entry, index) {
                    return React.createElement(Line, {
                        key: index,
                        type: 'linear',
                        strokeWidth: 2,
                        activeDot: { onClick: onClick, cursor: cursor },
                        dataKey: entry['' + entity],
                        dot: { strokeWidth: 2, r: 2 },
                        stroke: 'rgba(' + colors[index].r + ', ' + colors[index].g + ', ' + colors[index].b + ', 1)'
                    });
                });
            }
            return React.createElement(Line, {
                type: 'linear',
                strokeWidth: 2,
                dataKey: entity,
                stroke: colors[0],
                activeDot: { onClick: onClick, cursor: cursor },
                dot: { strokeWidth: 2, r: 2 }
            });
        }
    }, {
        key: 'renderLineBasic',
        value: function renderLineBasic() {
            if (this.state.datum.length > 0) {
                return React.createElement(
                    ResponsiveContainer,
                    null,
                    React.createElement(
                        LineChart,
                        {
                            data: this.state.datum
                        },
                        React.createElement(
                            XAxis,
                            {
                                dataKey: 'key',
                                tickLine: false,
                                stroke: 'rgb(204, 204, 204)',
                                tick: React.createElement(AxisX, { color: 'rgba(0, 0, 0, 0.8)', intervalType: this.props.intervalType })
                            },
                            React.createElement(Label, { className: 'line-data__label', value: this.props.labels.x, offset: -15, position: 'insideBottom' })
                        ),
                        React.createElement(
                            YAxis,
                            {
                                axisLine: false,
                                tick: React.createElement(AxisY, { color: 'rgba(0, 0, 0, 0.8)' })
                            },
                            React.createElement(Label, { className: 'line-data__label', value: this.props.labels.y, offset: -15, position: 'left', angle: -90 })
                        ),
                        React.createElement(CartesianGrid, { vertical: false }),
                        React.createElement(Tooltip, {
                            content: React.createElement(TooltipCustom, {
                                intervalType: this.props.intervalType === 'hour' ? 'hourPDay' : this.props.intervalType
                            })
                        }),
                        React.createElement(Legend, { height: 36, content: React.createElement(LegendCustom, { onClick: this.props.onLegendClick }) }),
                        this.renderLine()
                    )
                );
            }
            return React.createElement('div', { className: 'chart-line-progress' });
        }
    }, {
        key: 'render',
        value: function render() {
            var _this3 = this;

            return React.createElement(
                Fragment,
                null,
                React.createElement(
                    'div',
                    { className: 'chart-line' },
                    this.renderLineBasic()
                ),
                function () {
                    if (!isNull(_this3.props.onLineClick)) {
                        return React.createElement(DrawerFeed, null);
                    }
                    return null;
                }()
            );
        }
    }]);

    return Lines;
}(React.Component);

Lines.propTypes = propTypes;
Lines.defaultProps = defaultProps;

export default Lines;