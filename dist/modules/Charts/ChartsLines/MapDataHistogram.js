function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import range from 'lodash/range';

var typeHistogram = function typeHistogram(currentData, entity) {
    var array = range(currentData[0][entity + '_by_time'].buckets.length);

    array.forEach(function (i) {
        return array[i] = {};
    });

    currentData.map(function (i) {
        return i[entity + '_by_time'].buckets.map(function (e, index) {
            array[index][i.key] = e.doc_count;
            array[index].key = e.key;
            return array;
        });
    });

    return array;
};

var mapDataHistogram = function mapDataHistogram(currentData, entity) {
    var _ref2;

    return _ref2 = {}, _defineProperty(_ref2, '' + entity, function () {
        return currentData.map(function (i) {
            return _defineProperty({}, '' + entity, i.key);
        });
    }()), _defineProperty(_ref2, 'data', typeHistogram(currentData, entity)), _ref2;
};

export default mapDataHistogram;