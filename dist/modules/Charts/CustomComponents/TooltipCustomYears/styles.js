import css from 'glamor-jss';

export var styles = function styles() {
    return css({
        width: '140px',
        height: 'auto',
        boxShadow: '0 2px 15px 0 rgba(0, 0, 0, 0.3)',
        color: '#ffff',
        fontSize: '12px',
        fontWeight: 700,
        lineHeight: 1.42
    });
};

export default styles;