import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import { numberWithCommas } from '../../../../helpers/number';
import { styles } from './styles';

moment.locale('es');

var propTypes = {
    payload: PropTypes.array
};

var defaultProps = {
    payload: []
};

// eslint-disable-next-line
var TooltipCustomYears = function TooltipCustomYears(_ref) {
    var payload = _ref.payload;

    var values = payload === null ? [] : payload;
    var renderTotals = values.map(function (i, index) {
        return React.createElement(
            'div',
            { key: index, className: 'container' },
            React.createElement(
                'div',
                { className: 'data' },
                React.createElement(
                    'p',
                    null,
                    'A\xF1o'
                ),
                ' ',
                React.createElement(
                    'p',
                    null,
                    ' ',
                    moment(i.payload.date).format('YYYY'),
                    ' '
                )
            ),
            React.createElement(
                'div',
                { className: 'data' },
                React.createElement(
                    'p',
                    null,
                    ' Volumen '
                ),
                ' ',
                React.createElement(
                    'p',
                    null,
                    numberWithCommas(i.value),
                    ' '
                )
            )
        );
    });
    return React.createElement(
        'div',
        styles(),
        renderTotals
    );
};

TooltipCustomYears.propTypes = propTypes;
TooltipCustomYears.defaultProps = defaultProps;

export default TooltipCustomYears;