import React from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import isUndefined from 'lodash/isUndefined';
import upperFirst from 'lodash/upperFirst';
import { numberWithCommas } from '../../../../helpers/number';

moment.locale('es');

var propTypes = {
    payload: PropTypes.array,
    noUtc: PropTypes.bool,
    intervalType: PropTypes.string
};

var defaultProps = {
    payload: [],
    noUtc: false,
    intervalType: 'month'
};

var reactions = {
    angry: 'Me enfada',
    haha: 'Me divierte',
    like: 'Me gusta',
    love: 'Me encanta',
    sad: 'Me entristece',
    wow: 'Me asombra',
    comments: 'Comentarios',
    shares: 'Shares'
};

var renderDates = function renderDates(intervalType, date) {
    var component = {
        hour: function hour() {
            return React.createElement(
                'p',
                null,
                'Hora: ',
                date.format('LT')
            );
        },
        hourPDay: function hourPDay() {
            return React.createElement(
                'div',
                null,
                React.createElement(
                    'p',
                    null,
                    'Fecha: ',
                    upperFirst(date.format('MMM DD, YYYY'))
                ),
                React.createElement(
                    'p',
                    null,
                    'Hora: ',
                    date.format('hh:mm A')
                )
            );
        },
        day: function day() {
            return React.createElement(
                'p',
                null,
                'D\xEDa: ',
                date.format('MMM DD, YYYY')
            );
        },
        week: function week() {
            return React.createElement(
                'p',
                null,
                'Semana: ',
                date.format('MMM DD, YYYY'),
                ' - ',
                date.add(1, 'w').format('MMM DD, YYYY')
            );
        },
        month: function month() {
            return React.createElement(
                'p',
                null,
                'Mes: ',
                date.format('MMM YYYY')
            );
        },
        year: function year() {
            return React.createElement(
                'p',
                null,
                'A\xF1o: ',
                date.format('YYYY')
            );
        }
    };

    return component[intervalType]();
};

// eslint-disable-next-line
var TooltipCustom = function TooltipCustom(_ref) {
    var payload = _ref.payload,
        label = _ref.label,
        noUtc = _ref.noUtc,
        intervalType = _ref.intervalType;

    var dateDiv = null;
    if (!isUndefined(label)) {
        var day = moment(new Date(label));
        if (day.isValid()) {
            if (noUtc) {
                dateDiv = renderDates(intervalType, day);
            } else {
                dateDiv = renderDates(intervalType, day.utc());
            }
        } else {
            dateDiv = React.createElement(
                'p',
                null,
                'Tipo de post: ',
                label
            );
        }
    }

    var labels = function labels(name) {
        if (name === 'doc_count') {
            return 'Total';
        }return isUndefined(reactions[name]) ? name : reactions[name];
    };

    var renderTotals = payload.map(function (i, index) {
        return React.createElement(
            'li',
            { key: index, style: { color: i.color } },
            labels(upperFirst(i.name)),
            ': ',
            numberWithCommas(i.value)
        );
    });
    return React.createElement(
        'div',
        { className: 'tooltip_custom' },
        React.createElement(
            'ul',
            null,
            renderTotals
        ),
        dateDiv
    );
};

TooltipCustom.propTypes = propTypes;
TooltipCustom.defaultProps = defaultProps;

export default TooltipCustom;