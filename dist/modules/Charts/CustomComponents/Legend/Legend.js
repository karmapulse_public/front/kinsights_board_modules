import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';

var propTypes = {
    payload: PropTypes.array,
    nameArray: PropTypes.array,
    align: PropTypes.string,
    styles: PropTypes.object
};

var defaultProps = {
    payload: [],
    nameArray: [],
    align: 'left',
    styles: {
        justifyContent: 'flex-start',
        fontSize: 12,
        fontWeight: 300,
        textTransform: 'uppercase'
    }
};

var Legend = function Legend(_ref) {
    var payload = _ref.payload,
        nameArray = _ref.nameArray,
        align = _ref.align,
        styles = _ref.styles,
        _onClick = _ref.onClick;
    return React.createElement(
        'ul',
        {
            className: 'legend-entry',
            style: {
                textAlign: align,
                display: 'flex',
                flexWrap: 'wrap',
                alignItems: 'center',
                margin: ' 10px 35px',
                justifyContent: styles.justifyContent
            }
        },
        payload.map(function (entry, index) {
            return React.createElement(
                'li',
                {
                    style: {
                        margin: '5px 10px',
                        fontSize: styles.fontSize,
                        textTransform: styles.textTransform,
                        fontWeight: styles.fontWeight,
                        display: 'flex',
                        justifyContent: 'center'
                    },
                    key: 'item-' + index,
                    onClick: function onClick() {
                        return _onClick(isEmpty(nameArray) ? entry.value : nameArray[index]);
                    }
                },
                React.createElement('div', {
                    style: {
                        width: 11,
                        height: 11,
                        marginRight: 10,
                        display: 'inline-block',
                        borderRadius: '50%',
                        border: '1px solid ' + entry.color,
                        backgroundColor: '' + entry.color.replace('1)', '0.2)')
                    }
                }),
                '' + (isEmpty(nameArray) ? entry.value : nameArray[index])
            );
        })
    );
};

Legend.propTypes = propTypes;
Legend.defaultProps = defaultProps;
export default Legend;