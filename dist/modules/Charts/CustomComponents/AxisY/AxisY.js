import React from 'react';
import PropTypes from 'prop-types';
// import moment from 'moment';
import { nFormatter } from '../../../../helpers/number';

var propTypes = {
    payload: PropTypes.object,
    style: PropTypes.object,
    x: PropTypes.number,
    y: PropTypes.number,
    color: PropTypes.string
};

var defaultProps = {
    payload: {},
    style: {
        fontSize: 14
    },
    x: 0,
    y: 0,
    color: '#666'
};

var CustomAxisYTick = function CustomAxisYTick(_ref) {
    var payload = _ref.payload,
        x = _ref.x,
        y = _ref.y,
        color = _ref.color,
        style = _ref.style;

    var value = nFormatter(payload.value);
    return React.createElement(
        'text',
        {
            x: x,
            y: y,
            dy: 5,
            textAnchor: 'end',
            fill: color,
            style: Object.assign({}, style)
        },
        value
    );
};

CustomAxisYTick.propTypes = propTypes;
CustomAxisYTick.defaultProps = defaultProps;

export default CustomAxisYTick;