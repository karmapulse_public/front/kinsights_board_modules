import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

var propTypes = {
    payload: PropTypes.object,
    x: PropTypes.number,
    y: PropTypes.number,
    style: PropTypes.object,
    color: PropTypes.string,
    noUtc: PropTypes.bool,
    intervalType: PropTypes.string
};

var defaultProps = {
    payload: {},
    x: 0,
    y: 0,
    style: {},
    color: '#666',
    noUtc: false,
    intervalType: 'day'
};

var renderDatesByType = function renderDatesByType(intervalType, date) {
    var component = {
        hour: function hour() {
            return '' + date.format('LT');
        },
        day: function day() {
            return '' + date.format('MMM DD');
        },
        week: function week() {
            return '' + date.format('MMM DD');
        },
        month: function month() {
            return '' + date.format('MMM YYYY');
        },
        year: function year() {
            return '' + date.format('YYYY');
        }
    };

    return component[intervalType]();
};

var CustomAxisXTick = function CustomAxisXTick(_ref) {
    var payload = _ref.payload,
        x = _ref.x,
        y = _ref.y,
        color = _ref.color,
        style = _ref.style,
        noUtc = _ref.noUtc,
        intervalType = _ref.intervalType;

    var value = moment(new Date(payload.value));

    var typeValue = function typeValue() {
        if (value.isValid()) {
            if (noUtc) {
                return renderDatesByType(intervalType, value);
            }
            return renderDatesByType(intervalType, value.utc());
        }return payload.value;
    };

    return React.createElement(
        'text',
        {
            x: x,
            y: y,
            dy: 15,
            textAnchor: 'middle',
            fill: color,
            style: Object.assign({ textTransform: 'uppercase' }, style)
        },
        typeValue()
    );
};

CustomAxisXTick.propTypes = propTypes;
CustomAxisXTick.defaultProps = defaultProps;

export default CustomAxisXTick;