import React from 'react';
import PropTypes from 'prop-types';
import LocationIcon from './Icons/mapbox-marker';
import CircleIcon from './Icons/mapbox-circle';

export var propTypes = {
    token: PropTypes.string.isRequired,
    keyName: PropTypes.string,
    valueName: PropTypes.string,
    positionName: PropTypes.string,
    width: PropTypes.number,
    height: PropTypes.number,
    latitude: PropTypes.number,
    longitude: PropTypes.number,
    initialBound: PropTypes.array,
    zoom: PropTypes.number,
    maxZoom: PropTypes.number,
    colors: PropTypes.array, // key, color
    data: PropTypes.array, // key, value, position
    singleIcon: PropTypes.func,
    groupIcon: PropTypes.func,
    onClickPoint: PropTypes.func,
    onClickGroup: PropTypes.func,
    onViewportChange: PropTypes.func,
    textStyles: PropTypes.object,
    enableTooltip: PropTypes.bool,
    mapStyle: PropTypes.string,
    boundingBox: PropTypes.array,
    defaultColor: PropTypes.string,
    colorFind: PropTypes.func
};

export var defaultProps = {
    keyName: 'key',
    valueName: 'value',
    positionName: 'position',
    width: 900,
    height: 900,
    latitude: 19.4174043,
    longitude: -99.15973880000001,
    zoom: 15,
    maxZoom: 20,
    data: [],
    colors: [],
    singleIcon: function singleIcon(color, data, styles) {
        return React.createElement(LocationIcon, Object.assign({ color: color, textStyles: styles }, data));
    },
    groupIcon: function groupIcon(color, data, styles) {
        return React.createElement(CircleIcon, Object.assign({ color: color, textStyles: styles }, data));
    },
    onClickPoint: function onClickPoint() {
        return {};
    },
    onClickGroup: function onClickGroup() {
        return {};
    },
    onViewportChange: function onViewportChange() {
        return {};
    },
    textStyles: {
        fontFamily: 'Roboto-Regular, Roboto',
        fontSize: 10,
        fontWeight: 'normal',
        fill: '#FFF',
        textAnchor: 'middle'
    },
    enableTooltip: false,
    tooltip: function tooltip(data) {
        return React.createElement(
            'p',
            null,
            data.value
        );
    },
    initialBound: null,
    boundingBox: null,
    defaultColor: '#444444',
    colorFind: function colorFind(colors, data, keyName, defaultColor) {
        return colors.find(function (item) {
            return item[keyName] === data[keyName];
        }) || { key: data[keyName], color: defaultColor };
    }
};