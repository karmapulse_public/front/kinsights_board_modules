var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import MapGL, { Marker } from 'react-map-gl';
import React, { Component } from 'react';
import WebMercatorViewport from 'viewport-mercator-project';
import Tooltip from '../Tooltip/Tooltip';
import { styles } from './styles';
import { propTypes, defaultProps } from './defaultValues';

var guid = function guid() {
    var s4 = function s4() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    };
    return '' + s4() + s4() + '-' + s4();
};

var Map = function (_Component) {
    _inherits(Map, _Component);

    function Map(props) {
        _classCallCheck(this, Map);

        var _this = _possibleConstructorReturn(this, (Map.__proto__ || Object.getPrototypeOf(Map)).call(this, props));

        _this.state = {
            viewport: {
                width: props.width,
                height: props.height,
                latitude: props.latitude,
                longitude: props.longitude,
                zoom: props.zoom,
                maxZoom: props.maxZoom
            }
        };
        _this.changeBoudingBox = _this.changeBoudingBox.bind(_this);
        _this.onViewportChange = _this.onViewportChange.bind(_this);
        _this.renderButtonIcon = _this.renderButtonIcon.bind(_this);
        _this.onClickGroup = _this.onClickGroup.bind(_this);
        _this.onClickPoint = _this.onClickPoint.bind(_this);
        _this.getMapBounds = _this.getMapBounds.bind(_this);
        return _this;
    }

    _createClass(Map, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var initialBound = this.props.initialBound;


            if (initialBound) {
                this.changeBoudingBox(initialBound);
            }
        }
    }, {
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProps) {
            if (nextProps.boundingBox) {
                var boundingBox = nextProps.boundingBox;

                this.changeBoudingBox(boundingBox);
            }
            if (nextProps.width !== this.props.width) {
                this.setState({
                    viewport: Object.assign({}, this.state.viewport, {
                        width: nextProps.width
                    })
                });
            }
        }
    }, {
        key: 'onViewportChange',
        value: function onViewportChange(viewport) {
            var newViewport = Object.assign({}, viewport);
            this.props.onViewportChange(Object.assign({}, newViewport, { bounding_box: this.getMapBounds() }));
            this.setState({
                viewport: newViewport
            });
        }
    }, {
        key: 'onClickGroup',
        value: function onClickGroup(data) {
            var positionName = this.props.positionName;

            var newZoom = this.state.viewport.zoom + 1;
            if (newZoom > this.props.maxZoom) {
                newZoom = this.props.maxZoom;
            }
            this.props.onClickGroup({
                bounding_box: this.getMapBounds(),
                zoom: newZoom,
                data: data
            });

            this.setState({
                viewport: Object.assign({}, this.state.viewport, {
                    zoom: newZoom,
                    latitude: data[positionName][0],
                    longitude: data[positionName][1]
                })
            });
        }
    }, {
        key: 'onClickPoint',
        value: function onClickPoint(data) {
            this.props.onClickPoint({
                bounding_box: this.getMapBounds(),
                zoom: this.state.viewport.zoom,
                data: data
            });
        }
    }, {
        key: 'getMapBounds',
        value: function getMapBounds() {
            var mapGL = this.mapRef.getMap();
            var bounds = mapGL.getBounds();
            var boundingBox = {
                top_left: {
                    lat: bounds._ne.lat,
                    lon: bounds._sw.lng
                },
                bottom_right: {
                    lat: bounds._sw.lat,
                    lon: bounds._ne.lng
                }
            };
            return boundingBox;
        }
    }, {
        key: 'changeBoudingBox',
        value: function changeBoudingBox(newBounding) {
            var _fitBounds = new WebMercatorViewport(this.state.viewport).fitBounds(newBounding),
                longitude = _fitBounds.longitude,
                latitude = _fitBounds.latitude,
                zoom = _fitBounds.zoom;

            this.setState({
                viewport: Object.assign({}, this.state.viewport, {
                    latitude: latitude,
                    longitude: longitude,
                    zoom: zoom
                })
            });
        }
    }, {
        key: 'renderButtonIcon',
        value: function renderButtonIcon(data, click, icon) {
            var _props = this.props,
                colors = _props.colors,
                keyName = _props.keyName,
                valueName = _props.valueName,
                tooltip = _props.tooltip,
                defaultColor = _props.defaultColor;

            var color = this.props.colorFind(colors, data, keyName, defaultColor);

            return React.createElement(
                'div',
                Object.assign({}, styles(), {
                    role: 'button',
                    tabIndex: 0,
                    onKeyPress: function onKeyPress() {
                        click(data);
                    },
                    onClick: function onClick() {
                        click(data);
                    }
                }),
                React.createElement(
                    Tooltip,
                    { isEnabled: this.props.enableTooltip },
                    tooltip(data)
                ),
                icon(color.color, Object.assign({}, data, { value: data[valueName] }), this.props.textStyles)
            );
        }
    }, {
        key: 'renderMarkers',
        value: function renderMarkers() {
            var _this2 = this;

            var _props2 = this.props,
                valueName = _props2.valueName,
                positionName = _props2.positionName;

            var Markers = this.props.data.map(function (m) {
                var markerType = m[valueName] > 1 ? _this2.renderButtonIcon(m, _this2.onClickGroup, _this2.props.groupIcon) : _this2.renderButtonIcon(m, _this2.onClickPoint, _this2.props.singleIcon);

                return React.createElement(
                    Marker,
                    {
                        latitude: m[positionName][0] // el mapa de engage está al reves
                        , longitude: m[positionName][1],
                        offsetLeft: -20,
                        offsetTop: -10,
                        key: guid(),
                        captureClick: true
                    },
                    markerType
                );
            });

            return Markers;
        }
    }, {
        key: 'render',
        value: function render() {
            var _this3 = this;

            return React.createElement(
                MapGL,
                Object.assign({}, this.state.viewport, {
                    ref: function ref(map) {
                        if (map) _this3.mapRef = map;
                    },
                    mapboxApiAccessToken: this.props.token,
                    onViewportChange: this.onViewportChange,
                    mapStyle: this.props.mapStyle
                }),
                this.renderMarkers()
            );
        }
    }]);

    return Map;
}(Component);

Map.propTypes = propTypes;
Map.defaultProps = defaultProps;

export default Map;