import React from 'react';
import lightenDarkenColors from './IconFunctions';
import { propTypes, defaultProps } from './defaultValues';

var LocationIcon = function LocationIcon(props) {
    return React.createElement(
        'svg',
        {
            width: props.width,
            height: props.height,
            viewBox: '-2 -2 39 50',
            version: '1.1',
            xmlnsXlink: 'http://www.w3.org/1999/xlink'
        },
        React.createElement(
            'g',
            { stroke: 'none', strokeWidth: 1, fill: 'none', fillRule: 'evenodd' },
            React.createElement('path', {
                stroke: lightenDarkenColors(props.color, -30),
                strokeWidth: 2,
                d: 'M17.5 0C7.825 0 0 7.199 0 16.1 0 28.175 17.5 46 17.5 46S35 28.175 35 16.1C35 7.199 27.175 0 17.5 0z',
                fill: props.color,
                fillRule: 'nonzero'
            }),
            React.createElement(
                'text',
                props.textStyles,
                React.createElement(
                    'tspan',
                    { x: '46%', y: '50%' },
                    props.value
                )
            ),
            React.createElement('polygon', { points: '0 0 35 0 35 46 0 46' })
        )
    );
};

LocationIcon.propTypes = propTypes;
LocationIcon.defaultProps = defaultProps;

export default LocationIcon;