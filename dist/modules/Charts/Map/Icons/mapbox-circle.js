import React from 'react';
import PropTypes from 'prop-types';
import lightenDarkenColors from './IconFunctions';
import { propTypes, defaultProps } from './defaultValues';

var GroupIcon = function GroupIcon(props) {
    return React.createElement(
        'svg',
        {
            width: props.width,
            height: props.width,
            viewBox: '-2 -2 44 44',
            version: '1.1',
            xmlnsXlink: 'http://www.w3.org/1999/xlink'
        },
        React.createElement(
            'g',
            { stroke: 'none', strokeWidth: 1, fill: 'none', fillRule: 'evenodd' },
            React.createElement('circle', {
                cx: 20,
                cy: 20,
                r: 20,
                fill: props.color,
                stroke: lightenDarkenColors(props.color, -30),
                strokeWidth: 2
            }),
            React.createElement(
                'text',
                props.textStyles,
                React.createElement(
                    'tspan',
                    { x: '46%', y: '55%' },
                    props.value
                )
            ),
            React.createElement('polygon', { points: '0 0 40 0 40 40 0 40' })
        )
    );
};

GroupIcon.propTypes = Object.assign({}, propTypes, {
    radius: PropTypes.number
});
GroupIcon.defaultProps = Object.assign({}, defaultProps, {
    width: 50
});

export default GroupIcon;