import PropTypes from 'prop-types';

export var propTypes = {
    width: PropTypes.number,
    height: PropTypes.number,
    color: PropTypes.string.isRequired,
    value: PropTypes.number,
    textStyles: PropTypes.object.isRequired
};

export var defaultProps = {
    value: 0,
    width: 35,
    height: 46
};