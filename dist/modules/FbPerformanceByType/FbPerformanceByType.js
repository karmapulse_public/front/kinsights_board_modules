var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import fetchFbPerformanceByType from './FbPerformanceByTypeDataFlow';

import LoadingModule from '../LoadingModule';
import PerformanceTable from './visualizations/PerformanceTable';
import PerformanceChart from './visualizations/PerformanceChart';

var propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    dateRange: PropTypes.object,
    services: PropTypes.object
};

var defaultProps = {
    fields: {},
    side: 'left',
    color: '#444',
    dateRange: {},
    services: {}
};

var FbPerformanceByType = function (_Component) {
    _inherits(FbPerformanceByType, _Component);

    function FbPerformanceByType(props) {
        _classCallCheck(this, FbPerformanceByType);

        var _this = _possibleConstructorReturn(this, (FbPerformanceByType.__proto__ || Object.getPrototypeOf(FbPerformanceByType)).call(this, props));

        _this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color
        };
        return _this;
    }

    _createClass(FbPerformanceByType, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            this.updateDataFlow(this.props);
        }
    }, {
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProps) {
            this.updateDataFlow(nextProps);
        }
    }, {
        key: 'updateDataFlow',
        value: function updateDataFlow(props) {
            var _this2 = this;

            var params = {
                rule_id: props.fields.search_id,
                initial_date: props.dateRange.startDate.toISOString(),
                final_date: props.dateRange.endDate.toISOString(),
                module_id: 'module_fb_performance_by_type'
            };

            fetchFbPerformanceByType(props.services.facebook, params).subscribe(function (data) {
                _this2.setState({
                    moduleConfig: props.fields,
                    moduleData: data,
                    moduleLoading: false,
                    moduleSide: props.side,
                    moduleColor: props.color
                });
            }, function (error) {
                console.log(error);
            }, null);
        }
    }, {
        key: 'renderByViz',
        value: function renderByViz() {
            var _state = this.state,
                moduleConfig = _state.moduleConfig,
                moduleData = _state.moduleData;

            if (moduleData) {
                var visualizations = {
                    performance_table: function performance_table(params) {
                        return React.createElement(PerformanceTable, params);
                    },
                    performance_chart: function performance_chart(params) {
                        return React.createElement(PerformanceChart, params);
                    }
                };

                return visualizations[moduleConfig.visualization](this.state);
            }

            return React.createElement(LoadingModule, null);
        }
    }, {
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'm-fb-performance-by-type' },
                this.renderByViz()
            );
        }
    }]);

    return FbPerformanceByType;
}(Component);

FbPerformanceByType.propTypes = propTypes;
FbPerformanceByType.defaultProps = defaultProps;

export default FbPerformanceByType;