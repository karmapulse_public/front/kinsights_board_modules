import React from 'react';
import PropTypes from 'prop-types';
import ChartColumns from '../../../Charts/ChartColumns';
import { numberWithCommas } from '../../../../helpers/number';

import styles from './styles';
import NoDataModule from '../../../NoDataModule';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666'
};

var PerformanceTable = function PerformanceTable(props) {
    var moduleConfig = props.moduleConfig,
        moduleData = props.moduleData,
        moduleColor = props.moduleColor;


    var types = {
        video: 'Video',
        photo: 'Imagen',
        status: 'Texto',
        link: 'Enlace',
        angry: {
            text: 'Me enfada',
            color: '#e57272'
        },
        haha: {
            text: 'Me divierte',
            color: '#e8d17d'
        },
        like: {
            text: 'Me gusta',
            color: '#697fcd'
        },
        love: {
            text: 'Me encanta',
            color: '#c289c9'
        },
        sad: {
            text: 'Me entristece',
            color: '#8c7fac'
        },
        wow: {
            text: 'Me asombra',
            color: '#83c6b7'
        },
        comments: {
            text: 'Comentarios',
            color: '#cbd3d8'
        },
        shares: {
            text: 'Shares',
            color: '#acb8c0'
        }
    };

    var FormatData = function FormatData() {
        var data = [];
        var dataKeys = [];
        var nameArray = [];
        moduleData.data.forEach(function (el) {
            return data.push({
                denomitation: types[el.denomination],
                like: el.reactions.LIKE,
                love: el.reactions.LOVE,
                haha: el.reactions.HAHA,
                wow: el.reactions.WOW,
                sad: el.reactions.SAD,
                angry: el.reactions.ANGRY,
                comments: el.comments_total,
                shares: el.shares_total
            });
        });
        Object.keys(data[0]).forEach(function (i) {
            if (i !== 'denomitation') {
                dataKeys.push({
                    key: i,
                    group: i !== 'comments' ? i !== 'shares' ? 'interactions' : undefined : undefined,
                    color: types[i].color
                });
                nameArray.push(types[i].text);
            }
        });

        var obj = {
            data: data,
            dataKeys: dataKeys,
            nameArray: nameArray
        };
        return obj;
    };
    return React.createElement(
        'div',
        Object.assign({ className: 'performance-chart' }, styles(moduleColor)),
        React.createElement(
            'div',
            { className: 'performance-chart__label' },
            React.createElement(
                'h3',
                null,
                moduleConfig.title
            )
        ),
        moduleData.total !== 0 ? React.createElement(
            'div',
            { className: 'performance-chart__content' },
            React.createElement(
                'h4',
                null,
                'Interacciones totales: ',
                React.createElement(
                    'span',
                    null,
                    numberWithCommas(moduleData.total)
                )
            ),
            React.createElement(ChartColumns, {
                dataKeys: FormatData().dataKeys,
                nameArray: FormatData().nameArray,
                data: FormatData().data,
                type: 'multiple'
            })
        ) : React.createElement(NoDataModule, { color: moduleColor })
    );
};

PerformanceTable.propTypes = propTypes;
PerformanceTable.defaultProps = defaultProps;

export default PerformanceTable;