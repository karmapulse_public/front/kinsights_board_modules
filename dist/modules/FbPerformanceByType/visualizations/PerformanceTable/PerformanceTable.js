import React from 'react';
import PropTypes from 'prop-types';

import { LikeIcon, LoveIcon, HahaIcon, WowIcon, SadIcon, AngryIcon } from '../../../../helpers/icons/fb';

import { numberWithCommas } from '../../../../helpers/number';

import styles from './styles';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666'
};

var PerformanceTable = function PerformanceTable(props) {
    var moduleConfig = props.moduleConfig,
        moduleData = props.moduleData,
        moduleColor = props.moduleColor;


    var types = {
        video: 'video',
        photo: 'imagen',
        status: 'texto',
        link: 'link'
    };

    var renderHeader = function renderHeader() {
        return React.createElement(
            'div',
            { className: 'table__header' },
            React.createElement(
                'div',
                { className: 'header__percent' },
                React.createElement(
                    'h4',
                    null,
                    'Tipos de publicaci\xF3n'
                ),
                React.createElement(
                    'h4',
                    null,
                    'Porcentaje de interacci\xF3n'
                )
            ),
            React.createElement(
                'div',
                { className: 'header__reactions' },
                React.createElement(LikeIcon, null),
                React.createElement(LoveIcon, null),
                React.createElement(HahaIcon, null),
                React.createElement(WowIcon, null),
                React.createElement(SadIcon, null),
                React.createElement(AngryIcon, null)
            ),
            React.createElement(
                'div',
                { className: 'header__interactions' },
                React.createElement(
                    'h4',
                    null,
                    'Total de interacciones'
                )
            )
        );
    };

    var renderPercentBars = function renderPercentBars(name, num) {
        var percent = num / moduleData.total * 100;
        return React.createElement(
            'div',
            { className: 'content__percent ' + types[name] },
            React.createElement(
                'div',
                { className: 'percent__bars' },
                React.createElement(
                    'span',
                    null,
                    types[name]
                ),
                React.createElement('div', {
                    style: { width: percent + '%' }
                })
            ),
            React.createElement(
                'span',
                null,
                Math.round(percent) + '%'
            )
        );
    };

    var renderReactions = function renderReactions(reactions) {
        return React.createElement(
            'div',
            { className: 'content__reactions' },
            reactions.map(function (reaction, i) {
                return React.createElement(
                    'span',
                    { key: i },
                    numberWithCommas(reaction.total)
                );
            })
        );
    };

    var renderContent = function renderContent() {
        return moduleData.data.map(function (item, index) {
            return React.createElement(
                'div',
                { className: 'table__content', key: index },
                renderPercentBars(item.denomination, item.total),
                renderReactions(item.reactions),
                React.createElement(
                    'div',
                    { className: 'content__interactions' },
                    React.createElement(
                        'span',
                        null,
                        numberWithCommas(item.total)
                    )
                )
            );
        });
    };

    var renderFooter = function renderFooter() {
        return React.createElement(
            'div',
            { className: 'table__footer' },
            React.createElement(
                'div',
                { className: 'key__colors' },
                React.createElement(
                    'span',
                    null,
                    'Texto'
                ),
                React.createElement(
                    'span',
                    null,
                    'Link'
                ),
                React.createElement(
                    'span',
                    null,
                    'Imagen'
                ),
                React.createElement(
                    'span',
                    null,
                    'Video'
                )
            ),
            React.createElement(
                'div',
                { className: 'total' },
                React.createElement(
                    'span',
                    null,
                    'Total: ',
                    numberWithCommas(moduleData.total)
                )
            )
        );
    };

    var renderTable = function renderTable() {
        return React.createElement(
            'div',
            { className: 'performance__table' },
            renderHeader(),
            renderContent(),
            renderFooter()
        );
    };

    return React.createElement(
        'div',
        Object.assign({ className: 'performance-table' }, styles(moduleColor)),
        React.createElement(
            'div',
            { className: 'performance__label' },
            React.createElement(
                'h3',
                null,
                moduleConfig.title
            )
        ),
        renderTable()
    );
};

PerformanceTable.propTypes = propTypes;
PerformanceTable.defaultProps = defaultProps;

export default PerformanceTable;