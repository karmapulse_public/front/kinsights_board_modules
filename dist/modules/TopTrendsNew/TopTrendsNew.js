var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import clone from 'lodash/clone';
import moment from 'moment-timezone';

import fetchTopTrends from './TopTrendsNewDataFlow';

import LoadingModule from '../LoadingModule';
import ListTopTrends from './visualizations/ListTopTrends';
import ListTopTrendsSentiment from './visualizations/ListTopTrendsSentiment';
import WordCloudTrends from './visualizations/WordCloudTrends';

var propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    services: PropTypes.object,
    dateRange: PropTypes.object,
    view: PropTypes.string,
    showColors: PropTypes.bool,
    filters: PropTypes.object
};

var defaultProps = {
    fields: {},
    side: 'left',
    color: '#666',
    services: {},
    dateRange: {},
    view: '',
    filters: {},
    showColors: false
};

var TopTrendsNew = function (_Component) {
    _inherits(TopTrendsNew, _Component);

    function TopTrendsNew(props) {
        _classCallCheck(this, TopTrendsNew);

        var _this = _possibleConstructorReturn(this, (TopTrendsNew.__proto__ || Object.getPrototypeOf(TopTrendsNew)).call(this, props));

        _this.state = {
            moduleConfig: props.fields,
            moduleServices: props.services,
            moduleRange: props.dateRange,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color,
            moduleView: props.view,
            showColors: props.showColors
        };

        _this.renderByViz = _this.renderByViz.bind(_this);
        return _this;
    }

    _createClass(TopTrendsNew, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            this.updateDataFlow(this.props);
        }
    }, {
        key: 'componentDidUpdate',
        value: function componentDidUpdate(prevProps) {
            var _props$dateRange = this.props.dateRange,
                startDate = _props$dateRange.startDate,
                endDate = _props$dateRange.endDate;
            var _props$fields = this.props.fields,
                hasClick = _props$fields.hasClick,
                search_id = _props$fields.search_id;

            if (startDate !== prevProps.dateRange.startDate || endDate !== prevProps.dateRange.endDate || search_id !== prevProps.fields.search_id) {
                this.updateDataFlow(this.props);
            }
            if (hasClick !== prevProps.fields.hasClick) {
                this.changeState(hasClick);
            }
        }
    }, {
        key: 'changeState',
        value: function changeState(hasClick) {
            this.setState({
                moduleConfig: Object.assign({}, this.state.moduleConfig, {
                    hasClick: hasClick
                })
            });
        }
    }, {
        key: 'updateDataFlow',
        value: function updateDataFlow(props) {
            var _this2 = this;

            var fields = props.fields,
                _props$dateRange2 = props.dateRange,
                startDate = _props$dateRange2.startDate,
                endDate = _props$dateRange2.endDate,
                filters = props.filters;

            var params = {
                recipe_id: 'module_tw_top_phrases',
                rule_id: fields.search_id,
                initial_date: startDate.startOf('day').utc().toISOString(),
                final_date: endDate.endOf('day').utc().toISOString(),
                filters: Object.assign({
                    top: fields.top_limit
                }, filters)
            };

            fetchTopTrends(props.services.twitter, params).subscribe(function (data) {
                _this2.setState({
                    moduleData: data,
                    moduleConfig: props.fields,
                    moduleLoading: false,
                    moduleView: props.view,
                    moduleColor: props.color,
                    moduleSide: props.side,
                    showColors: props.showColors
                });
            }, function (error) {
                console.log(error);
            }, null);
        }
    }, {
        key: 'renderByViz',
        value: function renderByViz() {
            var _state = this.state,
                moduleConfig = _state.moduleConfig,
                moduleData = _state.moduleData;


            if (moduleData) {
                var visualizations = {
                    list_top_trends: function list_top_trends(params) {
                        return React.createElement(ListTopTrends, params);
                    },
                    wordclouds_top_trends: function wordclouds_top_trends(params) {
                        return React.createElement(WordCloudTrends, params);
                    },
                    list_top_trends_sentiment: function list_top_trends_sentiment(params) {
                        return React.createElement(ListTopTrendsSentiment, params);
                    }
                };
                var v = this.state.moduleView !== '' ? this.state.moduleView : moduleConfig.visualization;
                return visualizations[v](this.state);
            }

            return React.createElement(LoadingModule, null);
        }
    }, {
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'm-top-trends' },
                this.renderByViz()
            );
        }
    }]);

    return TopTrendsNew;
}(Component);

TopTrendsNew.propTypes = propTypes;
TopTrendsNew.defaultProps = defaultProps;

export default TopTrendsNew;