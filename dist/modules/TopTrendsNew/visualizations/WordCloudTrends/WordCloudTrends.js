import React, { useRef } from 'react';
import PropTypes from 'prop-types';

import withDrawer from '../../../withDrawer';
import styles from './wordCloudTrendsStyles';
import WordCloud from '../../../Charts/WordCloud';
import { numberWithCommas } from '../../../../helpers/number';
import { toPostOrder, evalAritmetic } from '../../../../helpers/resolveAritmetic';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.array,
    moduleColor: PropTypes.string,
    onClick: PropTypes.func
};

var defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleColor: '#666',
    onClick: function onClick() {
        return {};
    }
};

var WordCloudTrends = function WordCloudTrends(_ref) {
    var moduleConfig = _ref.moduleConfig,
        moduleColor = _ref.moduleColor,
        moduleData = _ref.moduleData,
        onClick = _ref.onClick;

    var refExport = useRef(null);
    var titleConfigDw = function titleConfigDw(theme) {
        return {
            title: moduleConfig.title || 'Tweets en el tiempo',
            subtitleLeft: 'Tweets con el tema ' + theme,
            subtitleRight: ''
        };
    };
    var colors = moduleConfig.fontColor || ['#5979b8'];
    var width = moduleConfig.width || 400;
    var height = moduleConfig.height || 400;
    var values = moduleData.values;

    var maxAngle = moduleConfig.maxAngle || 0;
    var minAngle = moduleConfig.minAngle || 0;
    var outlineFn = moduleConfig.outlineFn || '0';
    var outlineColor = moduleConfig.outlineColor || '#5979b8';
    var outlineNumbers = moduleConfig.outlineFn ? function () {
        var array = [];
        var postOutlineFn = toPostOrder(outlineFn);
        for (var n = 1; n <= values.length; n += 1) {
            array.push(evalAritmetic(postOutlineFn, n));
        }
        return array;
    }() : [];
    var colorScale = function colorScale(elem, index, chooseColor) {
        if (outlineNumbers.includes(index)) {
            return outlineColor;
        }
        return chooseColor();
    };

    var renderTitle = function renderTitle() {
        if (moduleConfig.show_title) {
            var _moduleConfig$title = moduleConfig.title,
                title = _moduleConfig$title === undefined ? 'RELACIÓN DE TEMAS' : _moduleConfig$title;

            return React.createElement(
                'div',
                { className: 'wordcloud__label' },
                React.createElement(
                    'h3',
                    null,
                    title
                )
            );
        }
        return '';
    };

    return React.createElement(
        'div',
        Object.assign({
            className: 'wordcloud-top-trends'
        }, styles(moduleColor), {
            ref: refExport
        }),
        renderTitle(),
        React.createElement(
            'div',
            { className: 'wordcloud__chart' },
            React.createElement(
                'div',
                null,
                React.createElement(WordCloud, {
                    colors: colors,
                    wordCountKey: 'total',
                    wordKey: 'theme',
                    fontFamily: 'Roboto',
                    enableTooltip: true,
                    height: height,
                    maxWords: 300,
                    width: width,
                    words: values,
                    onWordClick: function onWordClick(_ref2) {
                        var theme = _ref2.theme;
                        onClick({ filters: { phrase: theme } }, titleConfigDw(theme));
                    },
                    maxAngle: maxAngle,
                    minAngle: minAngle,
                    colorScale: colorScale,
                    tooltip: function tooltip(data) {
                        return React.createElement(
                            'div',
                            { className: 'tooltip-example' },
                            React.createElement(
                                'p',
                                null,
                                data.text
                            ),
                            React.createElement(
                                'p',
                                null,
                                numberWithCommas(data.total),
                                ' menciones'
                            )
                        );
                    }
                })
            )
        )
    );
};

WordCloudTrends.propTypes = propTypes;
WordCloudTrends.defaultProps = defaultProps;

export default withDrawer(WordCloudTrends);