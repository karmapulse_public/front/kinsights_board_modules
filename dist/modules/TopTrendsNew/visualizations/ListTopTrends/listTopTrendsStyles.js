import { css } from 'glamor';

var styles = function styles(color, side, hasClick) {
    var bgColor = 'transparent';

    return css({
        position: 'relative',
        width: '100%',
        height: 'auto',
        display: 'flex',
        alignItems: 'center',
        flexWrap: 'wrap',
        backgroundColor: bgColor,
        color: 'rgba(0,0,0, 0.5)',

        '>ul': {
            width: '100%',
            padding: '20px'
        },

        ' .list-top-titles': {
            width: '100%',
            padding: '5px 20px 20px',
            display: 'flex',
            flexDirection: side === 'left' ? 'row' : 'row-reverse',
            justifyContent: 'space-between',

            ' >h5': {
                fontSize: '10px'

            }
        },

        ' .list__label': {
            width: '100%',
            '> h3': {
                padding: '20px',
                textTransform: 'uppercase',
                fontSize: 16,
                fontWeight: 400,
                textAlign: 'left',
                color: color
            },
            ' ~ ul': {
                paddingTop: 0
            }
        },

        ' .list__item': {
            cursor: hasClick ? 'pointer' : 'default',
            '&:not(:last-of-type)': {
                marginBottom: 20
            },
            flexDirection: side === 'left' ? 'row' : 'row-reverse',
            display: 'flex',
            alignItems: 'center'
        },

        ' .index': {
            width: 20,
            height: 20,
            fontSize: '13px',
            fontWeight: 500,
            lineHeight: '20px',
            textAlign: 'center',
            background: '#f2f2f2'
        },

        ' .list__dot': {
            width: 11,
            height: 11,
            borderRadius: 200
        },

        ' .list__name': {
            flex: 1,
            textAlign: side === 'left' ? 'left' : 'right',
            fontSize: 12,
            padding: '0 10px',
            fontWeight: 500,
            color: '#000'
        },

        ' .list__value': {
            fontSize: 12,
            fontWeight: 500,
            color: color,
            opacity: 0.8
        },
        ' .no-data-module': { minHeight: 200 }
    });
};

export default styles;