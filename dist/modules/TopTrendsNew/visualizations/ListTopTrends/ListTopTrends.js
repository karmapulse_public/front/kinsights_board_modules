import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import maxBy from 'lodash/maxBy';
import { css } from 'glamor';

import withDrawer from '../../../withDrawer';
import styles from './listTopTrendsStyles';
import NoDataModule from '../../../NoDataModule';
import { numberWithCommas } from '../../../../helpers/number';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.array,
    moduleColor: PropTypes.string,
    moduleSide: PropTypes.string,
    showColors: PropTypes.bool,
    onClick: PropTypes.func
};

var defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleColor: '#666',
    moduleSide: 'right',
    showColors: false,
    onClick: function onClick() {
        return {};
    }
};

var ListTopTrends = function ListTopTrends(_ref) {
    var moduleConfig = _ref.moduleConfig,
        moduleData = _ref.moduleData,
        moduleColor = _ref.moduleColor,
        moduleSide = _ref.moduleSide,
        showColors = _ref.showColors,
        _onClick = _ref.onClick;

    var refExport = useRef(null);
    var titleConfigDw = function titleConfigDw(theme) {
        return {
            title: moduleConfig.title || 'Tweets en el tiempo',
            subtitleLeft: 'Tweets con el tema ' + theme,
            subtitleRight: ''
        };
    };
    var colors = ['#c75a93', '#8176cc', '#cc5f43', '#5ba966', '#54a2d3'];
    var softColors = ['rgba(199,89,147,0.21)', 'rgba(129,117,204,0.21)', 'rgba(204,95,66,0.21)', 'rgba(91,169,101,0.21)', 'rgba(84,161,211,0.21)'];

    var maxValue = maxBy(moduleData.values, 'total');
    var dataFormat = moduleData.values.map(function (item, index) {
        return {
            id: index + 1,
            subject: item.theme,
            total: item.total,
            fullMark: maxValue.total
        };
    });

    var renderTitle = function renderTitle() {
        if (moduleConfig.show_title) {
            var _moduleConfig$title = moduleConfig.title,
                title = _moduleConfig$title === undefined ? 'RELACIÓN DE TEMAS' : _moduleConfig$title;

            return React.createElement(
                'div',
                { className: 'list__label' },
                React.createElement(
                    'h3',
                    null,
                    title
                )
            );
        }
        return '';
    };

    var showColor = function showColor(index, _ref2) {
        var subject = _ref2.subject,
            total = _ref2.total,
            id = _ref2.id;

        var dotColor = css({
            backgroundColor: softColors[index],
            border: '1px solid ' + colors[index]
        });
        var value = subject.length < 35 ? subject : subject.substring(0, 35).concat('...');

        if (showColors) {
            return React.createElement(
                'li',
                { // eslint-disable-line
                    className: 'list__item',
                    key: '' + value + index,
                    onClick: function onClick() {
                        return _onClick({ filters: { phrase: value } }, titleConfigDw(value));
                    }
                },
                React.createElement('div', Object.assign({ className: 'list__dot' }, dotColor)),
                React.createElement(
                    'h4',
                    { className: 'list__name' },
                    value
                ),
                React.createElement(
                    'h5',
                    { className: 'list__value', style: { color: colors[index] } },
                    numberWithCommas(total)
                )
            );
        }
        return React.createElement(
            'li',
            { // eslint-disable-line
                className: 'list__item',
                key: '' + value + index,
                onClick: function onClick() {
                    return _onClick({ filters: { phrase: value } }, titleConfigDw(value));
                }
            },
            React.createElement(
                'h5',
                { className: 'index' },
                id,
                '.'
            ),
            React.createElement(
                'h4',
                { className: 'list__name' },
                value
            ),
            React.createElement(
                'h5',
                { className: 'list__value' },
                numberWithCommas(total)
            )
        );
    };

    var renderItems = function renderItems() {
        return dataFormat.map(function (item, index) {
            return showColor(index, item);
        });
    };

    var renderSubtitles = function renderSubtitles() {
        var subtitle = moduleConfig.subtitle;

        var list = subtitle ? subtitle.list : 'Listado';
        var count = subtitle ? subtitle.count : 'Palabras';
        return React.createElement(
            'div',
            { className: 'list-top-titles' },
            React.createElement(
                'h5',
                null,
                list
            ),
            React.createElement(
                'h5',
                null,
                count
            )
        );
    };

    return React.createElement(
        'div',
        Object.assign({
            ref: refExport,
            className: 'list-top-trends'
        }, styles(moduleColor, moduleSide, moduleConfig.hasClick)),
        renderTitle(),
        renderSubtitles(),
        dataFormat.length === 0 ? React.createElement(NoDataModule, null) : React.createElement(
            'ul',
            null,
            renderItems()
        )
    );
};

ListTopTrends.propTypes = propTypes;
ListTopTrends.defaultProps = defaultProps;

export default withDrawer(ListTopTrends);