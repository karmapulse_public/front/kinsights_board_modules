import { Observable } from 'rxjs';
import isEmpty from 'lodash/isEmpty';
import { fetchXHR } from '../../helpers/http';

export default (function (url, params) {
    return Observable.create(function (observer) {
        Observable.fromPromise(fetchXHR(url, 'GET', params)).subscribe(function (data) {
            var response = data.json.response;

            var values = [];
            Object.keys(response).forEach(function (keys) {
                if (!isEmpty(response[keys])) {
                    values.push(response[keys]);
                }
            });
            observer.next({
                values: values
            });
        }, function (error) {
            observer.error(error);
        });
    });
});