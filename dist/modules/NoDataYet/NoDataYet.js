import React from 'react';
import NoDataYetIcon from '../../helpers/icons/noDataYet';

var NoDataYet = function NoDataYet() {
    return React.createElement(
        'div',
        { className: 'noDataYet' },
        React.createElement(
            'div',
            { className: 'icon' },
            React.createElement(NoDataYetIcon, null)
        ),
        React.createElement(
            'h5',
            null,
            'Los datos podr\xE1n ser mostrados hasta las 12:00am'
        )
    );
};

export default NoDataYet;