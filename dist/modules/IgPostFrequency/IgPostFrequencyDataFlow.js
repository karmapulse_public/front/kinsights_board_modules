import { Observable } from 'rxjs';
import { fetchXHR } from '../../helpers/http';

export default (function (url, params) {
    return Observable.create(function (observer) {
        Observable.fromPromise(fetchXHR(url, 'GET', params)).subscribe(function (data) {
            observer.next(Object.assign({
                total: data.json.response.total,
                total_gallery: data.json.response.total_gallery,
                total_image: data.json.response.total_image,
                total_video: data.json.response.total_video
            }));
        }, function (error) {
            observer.error(error);
        });
    });
});