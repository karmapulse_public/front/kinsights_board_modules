import React from 'react';
import PropTypes from 'prop-types';

import { numberWithCommas } from '../../../../helpers/number';
import styles from './cardTotalNumbersStyles';
import ConversationIcon from '../../../../helpers/icons/ceo/conversationIcon';
import NoDataModule from '../../../NoDataModule';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleSide: PropTypes.string,
    moduleColor: PropTypes.string,
    noDataYet: PropTypes.func
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleSide: 'left',
    moduleColor: '#666',
    noDataYet: null
};

var CardTotalNumbers = function CardTotalNumbers(_ref) {
    var moduleConfig = _ref.moduleConfig,
        moduleData = _ref.moduleData,
        moduleSide = _ref.moduleSide,
        moduleColor = _ref.moduleColor,
        noDataYet = _ref.noDataYet;

    // New logic -> add originals with replies


    var _moduleConfig$title = moduleConfig.title,
        title = _moduleConfig$title === undefined ? 'RELEVANCIA SOCIAL' : _moduleConfig$title;


    return React.createElement(
        'div',
        Object.assign({ className: 'card-total-numbers' }, styles(moduleSide, moduleColor)),
        React.createElement(
            'div',
            { className: 'mentions' },
            React.createElement(
                'div',
                { className: 'mentions__title' },
                React.createElement(
                    'h3',
                    null,
                    title
                )
            ),
            function () {
                if (noDataYet !== null) {
                    return noDataYet();
                }
                return moduleData.total !== 0 ? React.createElement(
                    'div',
                    { className: 'mentions__body' },
                    React.createElement(
                        'div',
                        { className: 'mentions__body__total' },
                        React.createElement(
                            'div',
                            null,
                            React.createElement(ConversationIcon, { color: '#e4006a' }),
                            React.createElement(
                                'h4',
                                null,
                                numberWithCommas(moduleData.total)
                            )
                        ),
                        React.createElement(
                            'h5',
                            null,
                            'Publicaciones totales'
                        )
                    ),
                    React.createElement(
                        'div',
                        { className: 'mentions__body__numbers' },
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'h4',
                                null,
                                numberWithCommas(moduleData.total_video)
                            ),
                            React.createElement(
                                'h5',
                                null,
                                'Video'
                            )
                        ),
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'h4',
                                null,
                                numberWithCommas(moduleData.total_image)
                            ),
                            React.createElement(
                                'h5',
                                null,
                                'Imagen'
                            )
                        ),
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'h4',
                                null,
                                numberWithCommas(moduleData.total_gallery)
                            ),
                            React.createElement(
                                'h5',
                                null,
                                'Galer\xEDa'
                            )
                        )
                    )
                ) : React.createElement(NoDataModule, { color: moduleColor });
            }()
        )
    );
};

CardTotalNumbers.propTypes = propTypes;
CardTotalNumbers.defaultProps = defaultProps;

export default CardTotalNumbers;