var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import clone from 'lodash/clone';
import moment from 'moment-timezone';
import fetchTotalInstagram from './IgPostFrequencyDataFlow';

import LoadingModule from '../LoadingModule';
import NoDataYet from '../NoDataYet';
import CardTotalNumbers from './visualizations/CardTotalNumbers';

var propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    dateRange: PropTypes.object,
    services: PropTypes.object,
    showNoDataYet: PropTypes.bool
};

var defaultProps = {
    fields: {},
    side: 'left',
    color: '#666',
    dateRange: {},
    services: {},
    showNoDataYet: false
};

var IgPostFrequency = function (_Component) {
    _inherits(IgPostFrequency, _Component);

    function IgPostFrequency(props) {
        _classCallCheck(this, IgPostFrequency);

        var _this = _possibleConstructorReturn(this, (IgPostFrequency.__proto__ || Object.getPrototypeOf(IgPostFrequency)).call(this, props));

        _this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color
        };
        return _this;
    }

    _createClass(IgPostFrequency, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var _this2 = this;

            var _props$dateRange = this.props.dateRange,
                startDate = _props$dateRange.startDate,
                endDate = _props$dateRange.endDate;


            var tz = 'America/Mexico_City';
            var strStartDate = clone(startDate).format('YYYY-MM-DD hh:mm:ss');
            var strEndDate = clone(endDate).format('YYYY-MM-DD hh:mm:ss');

            var utcOffsetStart = moment.tz(strStartDate, tz).isDST() ? '-05:00' : '-06:00';
            var utcOffsetEnd = moment.tz(strEndDate, tz).isDST() ? '-05:00' : '-06:00';

            startDate = startDate.utcOffset(utcOffsetStart).set({
                hour: 0,
                minute: 0,
                second: 0,
                millisecond: 0
            }).toISOString();

            endDate = endDate.utcOffset(utcOffsetEnd).set({
                minute: 5 * Math.floor(moment().minute() / 5),
                second: 0,
                millisecond: 0
            }).toISOString();

            var params = {
                recipe_id: 'module_ig_total_media',
                rule_id: this.state.moduleConfig.search_id,
                initial_date: startDate,
                final_date: endDate
            };

            fetchTotalInstagram(this.props.services.twitter, params).subscribe(function (data) {
                _this2.setState({
                    moduleData: data,
                    moduleLoading: false
                });
            }, function (error) {
                console.log(error);
            }, null);
        }
    }, {
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProp) {
            var _this3 = this;

            var _nextProp$dateRange = nextProp.dateRange,
                startDate = _nextProp$dateRange.startDate,
                endDate = _nextProp$dateRange.endDate;


            var tz = 'America/Mexico_City';
            var strStartDate = clone(startDate).format('YYYY-MM-DD hh:mm:ss');
            var strEndDate = clone(endDate).format('YYYY-MM-DD hh:mm:ss');

            var utcOffsetStart = moment.tz(strStartDate, tz).isDST() ? '-05:00' : '-06:00';
            var utcOffsetEnd = moment.tz(strEndDate, tz).isDST() ? '-05:00' : '-06:00';

            startDate = startDate.utcOffset(utcOffsetStart).set({
                hour: 0,
                minute: 0,
                second: 0,
                millisecond: 0
            }).toISOString();

            endDate = endDate.utcOffset(utcOffsetEnd).set({
                hour: 23,
                minute: 59,
                second: 59,
                millisecond: 999
            }).toISOString();

            var params = {
                recipe_id: 'module_ig_total_media',
                rule_id: nextProp.fields.search_id,
                initial_date: startDate,
                final_date: endDate
            };

            fetchTotalInstagram(this.props.services.twitter, params).subscribe(function (data) {
                _this3.setState({
                    moduleConfig: nextProp.fields,
                    moduleData: data,
                    moduleLoading: false,
                    moduleSide: nextProp.side,
                    moduleColor: nextProp.color
                });

                _this3.forceUpdate();
            }, function (error) {
                console.log(error);
            }, null);
        }
    }, {
        key: 'renderByViz',
        value: function renderByViz() {
            var _state = this.state,
                moduleConfig = _state.moduleConfig,
                moduleData = _state.moduleData;
            var _props = this.props,
                _props$dateRange2 = _props.dateRange,
                startDate = _props$dateRange2.startDate,
                endDate = _props$dateRange2.endDate,
                showNoDataYet = _props.showNoDataYet;

            var renderNoDataYet = null;

            if (showNoDataYet && moment().diff(endDate, 'minutes') <= 0 && endDate.diff(startDate, 'days') <= 1) {
                renderNoDataYet = function renderNoDataYet() {
                    return React.createElement(NoDataYet, null);
                };
            }
            if (moduleData) {
                var visualizations = {
                    card_total_numbers: function card_total_numbers(params) {
                        return React.createElement(CardTotalNumbers, Object.assign({}, params, { noDataYet: renderNoDataYet }));
                    }
                };

                return visualizations[moduleConfig.visualization](this.state);
            }

            return React.createElement(LoadingModule, null);
        }
    }, {
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'm-total-tweets' },
                this.renderByViz()
            );
        }
    }]);

    return IgPostFrequency;
}(Component);

IgPostFrequency.propTypes = propTypes;
IgPostFrequency.defaultProps = defaultProps;

export default IgPostFrequency;