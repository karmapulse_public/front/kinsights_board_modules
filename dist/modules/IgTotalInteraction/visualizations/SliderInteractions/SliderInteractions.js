import React from 'react';
import PropTypes from 'prop-types';
import { faInstagram } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { numberWithCommas } from '../../../../helpers/number';
import NoDataModule from '../../../NoDataModule';

import styles from './styles';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    noDataYet: PropTypes.string
};

var defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleColor: '#666',
    noDataYet: null
};

var SliderInteractions = function SliderInteractions(_ref) {
    var moduleData = _ref.moduleData,
        moduleConfig = _ref.moduleConfig,
        moduleColor = _ref.moduleColor,
        noDataYet = _ref.noDataYet;


    return React.createElement(
        'div',
        Object.assign({ className: 'list-interactions' }, styles),
        React.createElement(
            'div',
            { className: 'list-interactions__label' },
            React.createElement(
                'h3',
                null,
                'INTERACCIONES INSTAGRAM'
            ),
            React.createElement(
                'div',
                null,
                React.createElement(FontAwesomeIcon, { icon: faInstagram, color: 'white' })
            )
        ),
        function () {
            if (noDataYet !== null) {
                return noDataYet();
            }
            return moduleData.total !== 0 ? React.createElement(
                'div',
                { className: 'list-interactions__content' },
                React.createElement(
                    'h4',
                    null,
                    React.createElement(
                        'span',
                        null,
                        numberWithCommas(moduleData.total_comments + moduleData.total_likes)
                    ),
                    React.createElement(
                        'p',
                        null,
                        'Totales '
                    )
                ),
                React.createElement(
                    'div',
                    { style: { position: 'relative', marginTop: '31px' } },
                    React.createElement(
                        'div',
                        { className: 'slider__card' },
                        React.createElement(
                            'h5',
                            null,
                            React.createElement(
                                'span',
                                null,
                                numberWithCommas(moduleData.total_likes)
                            ),
                            React.createElement(
                                'p',
                                null,
                                'Me gusta'
                            )
                        ),
                        React.createElement(
                            'h5',
                            null,
                            React.createElement(
                                'span',
                                null,
                                numberWithCommas(moduleData.total_comments)
                            ),
                            React.createElement(
                                'p',
                                null,
                                'Comentarios'
                            )
                        )
                    )
                )
            ) : React.createElement(NoDataModule, { color: moduleColor });
        }()
    );
};

SliderInteractions.propTypes = propTypes;
SliderInteractions.defaultProps = defaultProps;

export default SliderInteractions;