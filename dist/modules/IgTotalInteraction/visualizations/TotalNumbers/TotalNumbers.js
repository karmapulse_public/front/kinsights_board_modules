import React from 'react';
import PropTypes from 'prop-types';
import LikesIcon from '../../../../helpers/icons/ceo/likesIcon';
import ReplicesIcon from '../../../../helpers/icons/ceo/dialogIcon';
import { numberWithCommas } from '../../../../helpers/number';
import NoDataModule from '../../../NoDataModule';

import styles from './styles';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    noDataYet: PropTypes.string
};

var defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleColor: '#666',
    noDataYet: null
};

var TotalNumbers = function TotalNumbers(_ref) {
    var moduleData = _ref.moduleData,
        moduleConfig = _ref.moduleConfig,
        moduleColor = _ref.moduleColor,
        noDataYet = _ref.noDataYet;

    var color = '#F8006A';
    return React.createElement(
        'div',
        styles(moduleColor),
        React.createElement(
            'div',
            { className: 'total-num-int__label' },
            React.createElement(
                'h3',
                null,
                moduleConfig.title
            )
        ),
        function () {
            if (noDataYet !== null) {
                return noDataYet();
            }
            return moduleData.total !== 0 ? React.createElement(
                'div',
                { className: 'total-num-int__content' },
                React.createElement(
                    'div',
                    { className: 'total-num-int__content__total' },
                    React.createElement(
                        'h4',
                        null,
                        'Interacciones totales'
                    ),
                    React.createElement(
                        'p',
                        null,
                        numberWithCommas(moduleData.total_comments + moduleData.total_likes)
                    )
                ),
                React.createElement(
                    'div',
                    { className: 'total-num-int__content__data' },
                    React.createElement(
                        'h4',
                        null,
                        React.createElement(ReplicesIcon, { color: color }),
                        'Comentarios totales'
                    ),
                    React.createElement(
                        'p',
                        null,
                        numberWithCommas(moduleData.total_comments)
                    )
                ),
                React.createElement(
                    'div',
                    { className: 'total-num-int__content__data' },
                    React.createElement(
                        'h4',
                        null,
                        'Promedio por publicaci\xF3n'
                    ),
                    React.createElement(
                        'p',
                        null,
                        numberWithCommas(Math.round(moduleData.average_comments))
                    )
                ),
                React.createElement(
                    'div',
                    { className: 'total-num-int__content__data' },
                    React.createElement(
                        'h4',
                        null,
                        React.createElement(LikesIcon, { color: color }),
                        'Likes totales'
                    ),
                    React.createElement(
                        'p',
                        null,
                        numberWithCommas(moduleData.total_likes)
                    )
                ),
                React.createElement(
                    'div',
                    { className: 'total-num-int__content__data' },
                    React.createElement(
                        'h4',
                        null,
                        'Promedio por publicaci\xF3n'
                    ),
                    React.createElement(
                        'p',
                        null,
                        numberWithCommas(Math.round(moduleData.average_likes))
                    )
                )
            ) : React.createElement(NoDataModule, { color: moduleColor });
        }()
    );
};

TotalNumbers.propTypes = propTypes;
TotalNumbers.defaultProps = defaultProps;

export default TotalNumbers;