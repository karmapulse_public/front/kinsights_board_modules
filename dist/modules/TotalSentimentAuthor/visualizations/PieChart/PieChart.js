import React from 'react';
import PropTypes from 'prop-types';
import { PieChart, Pie, Cell } from 'recharts';
import NoUser from '../../../../helpers/icons/noUser';

import styles from './PieChartStyles';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleSide: PropTypes.string,
    moduleColor: PropTypes.string
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleSide: 'left',
    moduleColor: '#666'
};

var PieChartRemaster = function PieChartRemaster(_ref) {
    var moduleConfig = _ref.moduleConfig,
        moduleData = _ref.moduleData,
        moduleSide = _ref.moduleSide,
        moduleColor = _ref.moduleColor;

    var colors = {
        positive: '#89cf42',
        negative: '#e18181'
    };
    var finalData = moduleData.finalData,
        total = moduleData.total;


    if (total === 0) {
        return React.createElement(
            'div',
            { className: 'data__piechart--noData' },
            React.createElement(
                'p',
                null,
                'Reputaci\xF3n social del autor'
            ),
            React.createElement(NoUser, null),
            React.createElement(
                'p',
                null,
                'No se encontraron datos relacionados al autor'
            )
        );
    }

    var _moduleConfig$title = moduleConfig.title,
        title = _moduleConfig$title === undefined ? 'Reputación social' : _moduleConfig$title;


    return React.createElement(
        'div',
        Object.assign({ className: 'pie-chart' }, styles(moduleSide, moduleColor)),
        React.createElement(
            'div',
            { className: 'pie-chart__label' },
            React.createElement(
                'h3',
                null,
                title
            )
        ),
        React.createElement(
            'div',
            { className: 'pie-chart__content' },
            React.createElement(
                'div',
                { className: 'pie-chart__graphic' },
                React.createElement(
                    PieChart,
                    { width: 130, height: 130 },
                    React.createElement(
                        Pie,
                        {
                            innerRadius: 35,
                            outerRadius: 65,
                            labelLine: false,
                            data: finalData
                        },
                        finalData.map(function (entry, index) {
                            return React.createElement(Cell, { key: index, fill: colors['' + entry.key] });
                        })
                    )
                )
            )
        ),
        React.createElement(
            'div',
            { className: 'pie-chart__footer' },
            React.createElement(
                'h5',
                null,
                'Tweets positivos: ',
                React.createElement(
                    'p',
                    null,
                    ' ',
                    finalData[0].value,
                    ' '
                )
            ),
            React.createElement(
                'h5',
                null,
                'Tweets negativos: ',
                React.createElement(
                    'p',
                    null,
                    ' ',
                    finalData[1].value,
                    ' '
                )
            )
        ),
        React.createElement(
            'div',
            { className: 'pie-chart__sentiments' },
            React.createElement(
                'h4',
                null,
                ' Positivo '
            ),
            React.createElement(
                'h5',
                null,
                ' Negativo '
            )
        )
    );
};

PieChartRemaster.propTypes = propTypes;
PieChartRemaster.defaultProps = defaultProps;

export default PieChartRemaster;