import { Observable } from 'rxjs';
import { fetchXHR } from '../../helpers/http';

export default (function (url, params) {
    return Observable.create(function (observer) {
        Observable.fromPromise(fetchXHR(url, 'GET', params)).subscribe(function (data) {
            var response = data.json.response;

            var finalData = [{
                key: 'positive',
                name: 'Positivo',
                value: response.percentage_positive
            }, {
                key: 'negative',
                name: 'Negativo',
                value: response.percentage_negative
            }];
            observer.next({ finalData: finalData, total: response.total });
        }, function (error) {
            observer.error(error);
        });
    });
});