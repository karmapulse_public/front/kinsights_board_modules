var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import PropTypes from 'prop-types';

import setDates from '../../helpers/setDates';

import fetchTopImages from './TopImagesDataFlow';

import LoadingModule from '../LoadingModule';
import ContentGallery from './visualizations/ContentGallery';
import ContentCarousel from './visualizations/ContentCarousel';
import ContentGrid from './visualizations/ContentGrid';

var propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    services: PropTypes.object,
    dateRange: PropTypes.object,
    filters: PropTypes.object,
    size: PropTypes.number,
    withCache: PropTypes.bool
};

var defaultProps = {
    fields: {},
    side: 'left',
    color: '#666',
    services: {},
    dateRange: {},
    filters: {},
    size: 0,
    withCache: true
};

var TopImages = function (_Component) {
    _inherits(TopImages, _Component);

    function TopImages(props) {
        _classCallCheck(this, TopImages);

        var _this = _possibleConstructorReturn(this, (TopImages.__proto__ || Object.getPrototypeOf(TopImages)).call(this, props));

        _this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color
        };
        return _this;
    }

    _createClass(TopImages, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var _this2 = this;

            var filters = {};
            if (this.props.size > 0) {
                filters.size = this.props.size;
            }
            filters = Object.assign({}, filters, this.props.filters);
            var params = Object.assign({
                recipe_id: 'module_tw_top_images',
                rule_id: this.state.moduleConfig.search_id
            }, setDates(this.props.dateRange, this.props.withCache), {
                filters: Object.assign({}, filters)
            });

            if (this.props.size) {
                params.size = this.props.size;
            }

            var showChildTweet = this.state.moduleConfig.show_child_tweet || false;

            fetchTopImages(this.props.services.twitter, params, showChildTweet).subscribe(function (data) {
                return _this2.setState({
                    moduleData: data,
                    moduleLoading: false
                });
            }, function (error) {
                console.log(error);
            }, null);
        }
    }, {
        key: 'componentWillReceiveProps',
        value: function componentWillReceiveProps(nextProp) {
            var _this3 = this;

            var filters = {};
            if (nextProp.size > 0) {
                filters.size = nextProp.size;
            }
            filters = Object.assign({}, filters, nextProp.filters);
            var params = Object.assign({
                recipe_id: 'module_tw_top_images',
                rule_id: nextProp.fields.search_id
            }, setDates(nextProp.dateRange, this.props.withCache), {
                filters: Object.assign({}, filters)
            });

            if (this.props.size) {
                params.size = this.props.size;
            }

            var showChildTweet = this.state.moduleConfig.show_child_tweet || false;

            fetchTopImages(nextProp.services.twitter, params, showChildTweet).subscribe(function (data) {
                _this3.setState({
                    moduleConfig: nextProp.fields,
                    moduleData: data,
                    moduleLoading: false,
                    moduleSide: nextProp.side,
                    moduleColor: nextProp.color
                });
            }, function (error) {
                console.log(error);
            }, null);
        }
    }, {
        key: 'renderByViz',
        value: function renderByViz() {
            var _state = this.state,
                moduleConfig = _state.moduleConfig,
                moduleData = _state.moduleData;


            if (moduleData) {
                var visualizations = {
                    content_gallery: function content_gallery(params) {
                        return React.createElement(ContentGallery, params);
                    },
                    content_carousel: function content_carousel(params) {
                        return React.createElement(ContentCarousel, params);
                    },
                    content_grid: function content_grid(params) {
                        return React.createElement(ContentGrid, params);
                    }
                };

                return visualizations[this.props.view || moduleConfig.visualization](this.state);
            }

            return React.createElement(LoadingModule, null);
        }
    }, {
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'm-shared-content' },
                this.renderByViz()
            );
        }
    }]);

    return TopImages;
}(Component);

TopImages.propTypes = propTypes;
TopImages.defaultProps = defaultProps;

export default TopImages;