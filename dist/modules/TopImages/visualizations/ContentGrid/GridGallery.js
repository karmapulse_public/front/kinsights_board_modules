var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { css } from 'glamor';

import NoDataModule from '../../../NoDataModule';
import styles from './gridGalleryStyles';
import imageDefault from '../../../../helpers/icons/imageDeleted';
import TwitterLogo from '../../../../helpers/icons/twitterLogoIcon';
import { numberWithCommas } from '../../../../helpers/number';
import ModalDialog from '../../../ModalDialog';

var propTypes = {
    moduleData: PropTypes.array,
    moduleConfig: PropTypes.object,
    moduleColor: PropTypes.string,
    moduleSide: PropTypes.string
};

var defaultProps = {
    moduleData: [],
    moduleConfig: {},
    moduleColor: '#666',
    moduleSide: 'left'
};

var GridGallery = function (_Component) {
    _inherits(GridGallery, _Component);

    function GridGallery(props) {
        _classCallCheck(this, GridGallery);

        var _this = _possibleConstructorReturn(this, (GridGallery.__proto__ || Object.getPrototypeOf(GridGallery)).call(this, props));

        _this.state = {
            openModal: false,
            imageInModal: {},
            images: [],
            tweetActive: 0
        };
        _this.handleOpenModal = _this.handleOpenModal.bind(_this);
        _this.handleCloseModal = _this.handleCloseModal.bind(_this);
        _this.renderModal = _this.renderModal.bind(_this);
        return _this;
    }

    _createClass(GridGallery, [{
        key: 'handleOpenModal',
        value: function handleOpenModal(item) {
            this.setState({
                openModal: true,
                imageInModal: item
            });
        }
    }, {
        key: 'handleCloseModal',
        value: function handleCloseModal() {
            this.setState({
                openModal: false
            });
        }
    }, {
        key: 'renderModal',
        value: function renderModal() {
            var imageInModal = this.state.imageInModal;

            if (this.state.openModal) {
                var userInfo = {
                    image: imageInModal.tweet.image,
                    name: imageInModal.tweet.displayName,
                    username: imageInModal.tweet.preferredUsername
                };
                return React.createElement(ModalDialog, {
                    handleCloseModal: this.handleCloseModal,
                    tweetId: imageInModal.tweet.id,
                    postedTime: imageInModal.tweet.postedTime,
                    image: imageInModal.topImage,
                    user: userInfo,
                    body: imageInModal.tweet.body,
                    link: imageInModal.tweet.link,
                    showActions: true
                });
            }
            return '';
        }
    }, {
        key: 'renderImages',
        value: function renderImages() {
            var _this2 = this;

            var tweetActive = this.state.tweetActive;

            return this.props.moduleData.map(function (item, index) {
                var backgroundImage = css({
                    backgroundImage: 'url(' + (item.topImage ? item.topImage.replace('http:', 'https:') : '') + '), url(' + imageDefault + ')',
                    backgroundPosition: 'center',
                    backgroundSize: 'cover',
                    opacity: tweetActive === index ? 1 : 0.6
                });
                return React.createElement(
                    'div',
                    Object.assign({
                        onClick: function onClick() {
                            _this2.handleOpenModal(item);
                        },
                        key: index,
                        className: 'item'
                    }, backgroundImage),
                    React.createElement(
                        'div',
                        { className: 'item__tag' },
                        React.createElement(TwitterLogo, null),
                        React.createElement(
                            'span',
                            null,
                            numberWithCommas(item.total)
                        )
                    )
                );
            });
        }
    }, {
        key: 'render',
        value: function render() {
            var _props$moduleConfig$t = this.props.moduleConfig.title,
                title = _props$moduleConfig$t === undefined ? 'GALERÍA' : _props$moduleConfig$t;
            var moduleData = this.props.moduleData;


            return React.createElement(
                'div',
                Object.assign({ className: 'grid-gallery' }, styles()),
                React.createElement(
                    'div',
                    { className: 'grid-gallery__label' },
                    React.createElement(
                        'h3',
                        null,
                        title
                    )
                ),
                React.createElement(
                    'div',
                    { className: 'grid-gallery__images' },
                    moduleData.length ? this.renderImages() : React.createElement(NoDataModule, null)
                ),
                this.renderModal()
            );
        }
    }]);

    return GridGallery;
}(Component);

GridGallery.propTypes = propTypes;
GridGallery.defaultProps = defaultProps;

export default GridGallery;