import { css } from 'glamor';

var styles = function styles() {
    return css({
        width: '100%',
        minWidth: 300,

        ' .grid-gallery__images': {
            display: 'flex',
            alignContent: 'flex-start',
            alignItems: 'flex-start',
            justifyContent: 'space-between',
            flexWrap: 'wrap',
            overflowY: 'auto',
            boxSizing: 'border-box',

            '> .item': {
                width: 'calc(50% - 2px)',
                marginBottom: 4,
                backgroundColor: '#000',
                '&::before': {
                    content: '""',
                    backgroundColor: 'rgba(0,0,0,0.2)',
                    display: 'block',
                    paddingTop: '100%',
                    transition: 'all .2s ease',
                    cursor: 'pointer'
                },
                '&:hover': {
                    cursor: 'pointer',
                    '&::before': {
                        backgroundColor: 'rgba(0,0,0,0)'
                    }
                }
            },
            ' .item__tag': {
                display: 'flex',
                position: 'absolute',
                bottom: '0px',
                right: '0px'
            }
        }
    });
};

export default styles;