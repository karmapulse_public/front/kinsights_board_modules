var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import NoDataModule from '../../../NoDataModule';
import { numberWithCommas } from '../../../../helpers/number';
import formatBodyText from '../../../../helpers/formatBodyText';
import RtIcon from '../../../../helpers/icons/rtIcon';
import LikeIcon from '../../../../helpers/icons/ceo/likesIcon';

import { AngleLeft, AngleRight } from '../../../../helpers/icons/gob';
import styles from './contentCarouselStyles';

var propTypes = {
    moduleData: PropTypes.array,
    moduleConfig: PropTypes.object,
    moduleColor: PropTypes.string,
    moduleSide: PropTypes.string
};

var defaultProps = {
    moduleData: [],
    moduleConfig: {},
    moduleColor: '#666',
    moduleSide: 'left'
};

var ContentCarousel = function (_Component) {
    _inherits(ContentCarousel, _Component);

    function ContentCarousel(props) {
        _classCallCheck(this, ContentCarousel);

        var _this = _possibleConstructorReturn(this, (ContentCarousel.__proto__ || Object.getPrototypeOf(ContentCarousel)).call(this, props));

        _this.state = {
            tweetActive: 0
        };
        _this.handleNext = _this.handleNext.bind(_this);
        _this.handlePrev = _this.handlePrev.bind(_this);
        return _this;
    }

    _createClass(ContentCarousel, [{
        key: 'handlePrev',
        value: function handlePrev() {
            if (this.state.tweetActive > 0) {
                this.setState({
                    tweetActive: this.state.tweetActive - 1
                });
            }
        }
    }, {
        key: 'handleNext',
        value: function handleNext() {
            var tweetActive = this.state.tweetActive + 1;
            if (tweetActive <= this.props.moduleData.length - 1 && this.state.tweetActive < 2) {
                this.setState({
                    tweetActive: tweetActive
                });
            }
        }
    }, {
        key: 'renderTweetActive',
        value: function renderTweetActive() {
            var tweetActive = this.state.tweetActive;

            var tweet = this.props.moduleData[tweetActive];
            var tweetDate = moment(tweet.tweet.postedTime).format('DD MMM YY');
            return React.createElement(
                'div',
                { className: 'carousel__tweet' },
                React.createElement(
                    'h4',
                    null,
                    React.createElement(
                        'span',
                        null,
                        tweetActive + 1 + '.'
                    ),
                    React.createElement(
                        'div',
                        null,
                        React.createElement(
                            'span',
                            null,
                            '@',
                            tweet.tweet.preferredUsername
                        )
                    ),
                    React.createElement(
                        'div',
                        null,
                        React.createElement(
                            'span',
                            null,
                            tweetDate
                        )
                    )
                ),
                React.createElement(
                    'a',
                    {
                        href: tweet.tweet.link,
                        target: '_blank',
                        rel: 'noopener noreferrer'
                    },
                    React.createElement('div', {
                        style: { backgroundImage: 'url(' + (tweet.topImage ? tweet.topImage.replace('http:', 'https:') : '') + ')' }
                    }),
                    React.createElement('p', {
                        dangerouslySetInnerHTML: {
                            __html: formatBodyText(tweet.tweet.body)
                        }
                    })
                ),
                React.createElement(
                    'div',
                    { className: 'tweet__interactions' },
                    React.createElement(
                        'div',
                        null,
                        React.createElement(RtIcon, { color: 'rgba(0,0,0,0.5)' }),
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'span',
                                null,
                                'Retweets'
                            ),
                            React.createElement(
                                'span',
                                null,
                                numberWithCommas(tweet.tweet.retweets)
                            )
                        )
                    ),
                    React.createElement(
                        'div',
                        null,
                        React.createElement(LikeIcon, { color: 'rgba(0,0,0,0.5)' }),
                        React.createElement(
                            'div',
                            null,
                            React.createElement(
                                'span',
                                null,
                                'Favoritos'
                            ),
                            React.createElement(
                                'span',
                                null,
                                numberWithCommas(tweet.tweet.favorites)
                            )
                        )
                    )
                )
            );
        }
    }, {
        key: 'renderControls',
        value: function renderControls() {
            var opacityNext = 1;
            if (this.props.moduleData.length - 1 === this.state.tweetActive || this.state.tweetActive === 2) {
                opacityNext = 0.2;
            }

            return React.createElement(
                'div',
                { className: 'controls' },
                React.createElement(
                    'button',
                    {
                        onClick: this.handlePrev
                    },
                    React.createElement(AngleLeft, {
                        size: '13px',
                        color: '#000',
                        opacity: this.state.tweetActive === 0 ? 0.2 : 1
                    })
                ),
                React.createElement(
                    'button',
                    {
                        onClick: this.handleNext
                    },
                    React.createElement(AngleRight, {
                        size: '13px',
                        color: '#000',
                        opacity: opacityNext
                    })
                )
            );
        }
    }, {
        key: 'render',
        value: function render() {
            var _props$moduleConfig$t = this.props.moduleConfig.title,
                title = _props$moduleConfig$t === undefined ? 'CONTENIDO MÁS COMPARTIDO' : _props$moduleConfig$t;

            return React.createElement(
                'div',
                Object.assign({
                    className: 'content_carousel'
                }, styles(this.props.moduleSide, this.props.moduleColor)),
                React.createElement(
                    'div',
                    { className: 'carousel__label' },
                    React.createElement(
                        'h3',
                        null,
                        title
                    )
                ),
                this.props.moduleData.length > 0 ? React.createElement(
                    'div',
                    { className: 'carousel__content' },
                    this.renderTweetActive(),
                    this.renderControls()
                ) : React.createElement(NoDataModule, { color: this.props.moduleColor })
            );
        }
    }]);

    return ContentCarousel;
}(Component);

ContentCarousel.propTypes = propTypes;
ContentCarousel.defaultProps = defaultProps;

export default ContentCarousel;