import { css } from 'glamor';

var styles = function styles(side, color) {
    var bgColor = 'transparent';

    return css({
        minWidth: 300,
        height: '100%',
        backgroundColor: bgColor,
        color: 'rgba(0,0,0, 0.5)',
        ' .carousel__label': {
            '> h3': {
                padding: '15px 20px 20px',
                textTransform: 'uppercase',
                fontSize: 16,
                fontWeight: 400,
                textAlign: side,
                color: color
            }
        },
        ' .carousel__content': {
            padding: '30px',
            minHeight: 271,
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'space-between'
        },
        ' .carousel__tweet': {
            ' h4': {
                borderTop: '1px solid #dfdfdf',
                fontSize: 13,
                fontWeight: 500,
                verticalAlign: 'middle',
                textTransform: 'capitalize',
                display: 'flex',
                justifyContent: 'flex-start',
                alignItems: 'end',
                ' >span': {
                    width: 40,
                    height: 40,
                    backgroundColor: color,
                    textAlign: 'center',
                    lineHeight: '40px',
                    color: '#fff'
                },
                ' div:first-of-type': {
                    marginLeft: 20,
                    ' span': {
                        display: 'block',
                        fontWeight: 500,
                        fontSize: 10,
                        color: '#000',
                        '&:first-child': {
                            marginBottom: 3,
                            lineHeight: '23px',
                            fontSize: 13,
                            fontWeight: 700,
                            maxHeight: '3em',
                            textTransform: 'capitalize'
                        }
                    }
                },
                ' div:last-of-type': {
                    marginLeft: 'auto',
                    ' span': {
                        marginBottom: 3,
                        lineHeight: '23px',
                        fontSize: 13,
                        fontWeight: 700,
                        textTransform: 'capitalize',
                        maxHeight: '3em'
                    }
                }
            },
            ' a': {
                color: 'rgba(0,0,0,0.5)',
                textDecoration: 'none',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'flex-start',
                marginBottom: 35,
                ' >div': {
                    minWidth: 40,
                    height: 40,
                    backgroundSize: 'cover'
                },
                ' p': {
                    marginLeft: 20,
                    fontSize: 13,
                    fontWeight: 500,
                    maxHeight: '3em',
                    overflow: 'hidden'
                },
                ':hover': {
                    ' p': {
                        color: 'rgba(0,0,0,1)',
                        textDecoration: 'underline'
                    }
                }
            },
            ' .tweet__interactions': {
                fontWeight: 700,
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'flex-start',
                ' >div': {
                    display: 'flex',
                    alignItems: 'center'
                    //minHeight: 68
                },
                ' >div:first-child': {
                    marginRight: 15
                },
                ' svg': {
                    marginRight: 10
                },
                ' span': {
                    fontSize: 13,
                    color: '#000',
                    display: 'block'
                },
                ' span:first-child': {
                    color: 'rgba(0,0,0,0.5)',
                    marginBottom: 3
                }
            }
        },
        ' .controls': {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-end',
            ' button': {
                width: 43,
                height: 43,
                border: 'none',
                backgroundColor: '#eee',
                padding: 0,
                '&:hover': {
                    cursor: 'pointer'
                },
                '&:first-child': {
                    marginRight: 5
                }
            }
        }
    });
};

export default styles;