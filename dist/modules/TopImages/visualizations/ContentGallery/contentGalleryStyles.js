import { css } from 'glamor';

var styles = function styles(side, color) {
    var bgColor = 'transparent';

    return css({
        minWidth: 300,
        height: '100%',
        backgroundColor: bgColor,
        color: 'rgba(0,0,0, 0.5)',

        '@media screen and (min-width:1024px)': {
            height: 437
        },

        ' .gallery__label': {
            '> h3': {
                padding: '15px 20px 20px',
                textTransform: 'uppercase',
                fontSize: 16,
                fontWeight: 400,
                textAlign: side,
                color: color
            }
        },
        ' .gallery__container': {
            position: 'relative'
        },
        ' .gallery__tweet': {
            position: 'relative',
            height: 310,

            ' &__overlay': {
                position: 'absolute',
                left: 0,
                bottom: 0,
                width: '100%',
                backgroundColor: 'rgba(30,30,30,.8)',
                padding: '0px 10px 10px'
            },
            ' &__image': {
                width: '100%',
                height: '100%',
                border: 'none',
                cursor: 'pointer',
                '&:focus': {
                    outline: 'none'
                }
            },
            ' &__header': {
                height: 30,
                borderBottom: '1px solid rgba(255,255,255,.2)',
                color: '#fff'
            },
            ' &__counter': {
                position: 'absolute',
                top: 0,
                left: 0,
                width: 30,
                height: 30,
                textAlign: 'center',
                lineHeight: '30px',
                backgroundColor: color
            },
            ' &__retweets': {
                fontSize: 14,
                fontWeight: 400,
                lineHeight: '30px',
                letterSpacing: '0.2px',
                textAlign: 'right',
                ' >svg': {
                    marginRight: 5
                }
            },
            '&__user': {
                display: 'flex',
                alignItems: 'center',
                margin: '12px 0px 10px',
                ' img': {
                    width: 22,
                    height: 22,
                    borderRadius: 4,
                    marginRight: 5
                },
                ' h4': {
                    fontSize: 12,
                    fontWeight: 400,
                    flex: 1,
                    color: '#fff',
                    ' span': {
                        display: 'block',
                        fontSize: 10,
                        fontWeight: 300
                    },
                    ' a': {
                        color: '#FFF',
                        textDecoration: 'none'
                    }
                }
            },
            ' &__date': {
                ' span': {
                    display: 'block',
                    fontSize: 10,
                    color: 'rgba(255,255,255,.5)',
                    textAlign: 'right'
                },
                ' a': {
                    color: '#FFF',
                    textDecoration: 'none'
                }
            },
            ' &__body': {
                color: '#fff',
                lineHeight: 1.5,
                fontSize: 10,
                minHeight: 40,
                maxHeight: 70,
                overflowY: 'auto',

                ' a': {
                    color: '#fff',
                    lineHeight: 1.5,
                    fontSize: 10,
                    textDecoration: 'none'
                }
            }
        },
        ' .gallery__buttons': {
            position: 'absolute',
            top: 85,
            width: 30,
            height: 30,
            backgroundColor: 'rgba(0,0,0,.8)',
            color: '#fff',
            textAlign: 'center',
            lineHeight: '30px',
            cursor: 'pointer',
            border: 'none',
            ' &:focus': {
                outline: 'none'
            },
            ' &::before': {
                content: '""',
                display: 'block',
                margin: '0 auto',
                boxSizing: 'border-box',
                width: 5,
                height: 5,
                borderTop: '5px solid transparent',
                borderBottom: '5px solid transparent'
            }
        },
        ' .gallery__left': {
            left: 0,
            ' &::before': {
                borderRight: '5px solid #fff'
            }
        },
        ' .gallery__right': {
            right: 0,
            ' &::before': {
                borderLeft: '5px solid #fff'
            }
        },

        ' .gallery__thumbnails': {
            padding: 8,
            display: 'none',
            overflowY: 'auto',
            '>button': {
                width: 60,
                minWidth: 60,
                height: 60,
                border: 'none',
                cursor: 'pointer',
                '&:not(:last-of-type)': {
                    marginRight: 8
                },
                '&:focus': {
                    outline: 0
                }
            },
            '@media screen and (min-width:1024px)': {
                display: 'flex'
            }
        }
    });
};

export default styles;