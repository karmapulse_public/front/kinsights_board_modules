import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import Table from './Table';
import NoDataModule from '../../../NoDataModule';

import styles from './styles';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string,
    noDataYet: PropTypes.func
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666',
    noDataYet: null
};

var ListTop = function ListTop(_ref) {
    var moduleData = _ref.moduleData,
        moduleConfig = _ref.moduleConfig,
        moduleColor = _ref.moduleColor,
        noDataYet = _ref.noDataYet;

    var refExport = useRef(null);
    return React.createElement(
        'article',
        Object.assign({ ref: refExport, className: 'list-top' }, styles(moduleColor)),
        React.createElement(
            'div',
            { className: 'list-top__label' },
            React.createElement(
                'h3',
                null,
                moduleConfig.title
            )
        ),
        function () {
            if (noDataYet !== null) {
                return noDataYet();
            }
            return moduleData.data.length > 0 ? React.createElement(
                'div',
                { className: 'list-top__content ' + moduleConfig.entity_type.replace(/ /gi, '-') },
                React.createElement(Table, {
                    entity: moduleConfig.entity_type,
                    data: moduleData.data
                })
            ) : React.createElement(NoDataModule, { color: moduleColor });
        }()
    );
};

ListTop.propTypes = propTypes;
ListTop.defaultProps = defaultProps;

export default ListTop;