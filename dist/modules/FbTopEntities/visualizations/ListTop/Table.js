import React from 'react';
import PropTypes from 'prop-types';
import truncate from 'lodash/truncate';
import { nFormatter } from '../../../../helpers/number';

var propTypes = {
    data: PropTypes.array,
    entity: PropTypes.string
};

var defaultProps = {
    data: [],
    entity: ''
};

var types = function types(entity) {
    var type = {
        'hashtags en publicaciones': 'Hashtags',
        'temas en comentarios': 'Temas',
        'temas en publicaciones': 'Temas',
        'temas positivos en comentarios': 'Temas',
        'temas negativos en comentarios': 'Temas'
    };
    return type[entity];
};

var Table = function Table(_ref) {
    var entity = _ref.entity,
        data = _ref.data;
    return React.createElement(
        'div',
        null,
        React.createElement(
            'header',
            null,
            React.createElement(
                'h4',
                null,
                types(entity)
            ),
            React.createElement(
                'h4',
                null,
                'Apariciones'
            )
        ),
        React.createElement(
            'ul',
            { className: 'table' },
            data.map(function (i, index) {
                if (index < 5) {
                    return React.createElement(
                        'li',
                        { key: i.sentiment + '-' + index },
                        React.createElement(
                            'div',
                            null,
                            React.createElement('i', { className: i.sentiment }),
                            React.createElement(
                                'p',
                                null,
                                React.createElement(
                                    'span',
                                    null,
                                    index + 1,
                                    '.'
                                ),
                                React.createElement(
                                    'span',
                                    null,
                                    types(entity) === 'Hashtags' ? ' #' : ' ',
                                    truncate(i.value, { length: 50 })
                                )
                            )
                        ),
                        React.createElement(
                            'p',
                            null,
                            nFormatter(i.total)
                        )
                    );
                }
                return null;
            })
        ),
        React.createElement(
            'footer',
            null,
            React.createElement(
                'ul',
                null,
                React.createElement(
                    'li',
                    null,
                    React.createElement('i', { className: 'positivo' }),
                    React.createElement(
                        'p',
                        null,
                        'Positivo'
                    )
                ),
                React.createElement(
                    'li',
                    null,
                    React.createElement('i', { className: 'neutral' }),
                    React.createElement(
                        'p',
                        null,
                        'Neutral'
                    )
                ),
                React.createElement(
                    'li',
                    null,
                    React.createElement('i', { className: 'negativo' }),
                    React.createElement(
                        'p',
                        null,
                        'Negativo'
                    )
                )
            )
        )
    );
};

Table.propTypes = propTypes;
Table.defaultProps = defaultProps;

export default Table;