import React from 'react';
import { css } from 'glamor';

var loader = css.keyframes({
    '0%': {
        transform: 'translate(-50%, -50%) scale(0.0)',
        opacity: 1
    },
    '100%': {
        transform: 'translate(-50%, -50%) scale(1.0)',
        opacity: 0
    }
});

var styles = css({
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    ' .loader': {
        width: 70,
        height: 70,
        position: 'relative'
    },
    ' .dot1, .dot2': {
        width: 70,
        height: 70,
        position: 'absolute',
        top: '50%',
        left: '50%',
        borderRadius: '100%',
        border: '3px solid #aaa',
        transform: 'translate(-50%, -50%) scale(0.0)',
        animation: loader + ' 1s infinite ease-in-out'
    },
    ' .dot2': {
        animationDelay: '.5s'
    }
});

var LoadingModule = function LoadingModule() {
    return React.createElement(
        'div',
        Object.assign({ className: 'loading-module' }, styles),
        React.createElement(
            'div',
            { className: 'loader' },
            React.createElement('div', { className: 'dot1' }),
            React.createElement('div', { className: 'dot2' })
        )
    );
};

export default LoadingModule;