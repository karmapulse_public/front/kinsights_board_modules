import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';
import NoDataModule from '../../NoDataModule';

import styles from './ListStyles';
import withDrawer from './withDrawer';
import { numberWithCommas } from '../../../helpers/number';

var propTypes = {
    moduleData: PropTypes.array,
    moduleSide: PropTypes.string,
    moduleColor: PropTypes.string,
    moduleRange: PropTypes.object,
    moduleConfig: PropTypes.object,
    moduleServices: PropTypes.object,
    onClickUser: PropTypes.func
};

var defaultProps = {
    moduleConfig: {},
    moduleData: [],
    moduleSide: 'left',
    moduleColor: '#666',
    moduleServices: {},
    moduleRange: {},
    onClickUser: function onClickUser() {
        return {};
    }
};

var ListUsers = function ListUsers(_ref) {
    var moduleConfig = _ref.moduleConfig,
        moduleData = _ref.moduleData,
        moduleSide = _ref.moduleSide,
        moduleColor = _ref.moduleColor,
        onClickUser = _ref.onClickUser;
    var _moduleConfig$title = moduleConfig.title,
        title = _moduleConfig$title === undefined ? 'HASHTAG MÁS USADO' : _moduleConfig$title;

    return React.createElement(
        'div',
        Object.assign({ className: 'list' }, styles(moduleSide, moduleColor)),
        React.createElement(
            'div',
            { className: 'top-users__label' },
            React.createElement(
                'h3',
                null,
                title
            )
        ),
        React.createElement(
            'ul',
            { className: 'top-users__list' },
            React.createElement(
                'h4',
                null,
                React.createElement(
                    'span',
                    null,
                    'Usuarios'
                ),
                React.createElement(
                    'span',
                    null,
                    'Menciones'
                )
            ),
            !isEmpty(moduleData) ? moduleData.map(function (user, index) {
                return React.createElement(
                    'li',
                    { key: index },
                    React.createElement(
                        'h5',
                        { // eslint-disable-line
                            onClick: function onClick() {
                                return onClickUser(user.mention);
                            }
                        },
                        React.createElement(
                            'span',
                            null,
                            index + 1 + '.'
                        ),
                        React.createElement(
                            'span',
                            null,
                            '@',
                            user.mention
                        )
                    ),
                    React.createElement(
                        'h6',
                        null,
                        numberWithCommas(user.total)
                    )
                );
            }) : React.createElement(
                'div',
                { className: 'noData' },
                React.createElement(NoDataModule, { color: moduleColor })
            )
        )
    );
};

ListUsers.propTypes = propTypes;
ListUsers.defaultProps = defaultProps;

export default withDrawer(ListUsers);