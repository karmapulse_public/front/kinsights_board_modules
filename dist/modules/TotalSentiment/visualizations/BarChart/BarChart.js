import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import DialogIcon from '../../../../helpers/icons/ceo/dialogIcon';
import { numberWithCommas } from '../../../../helpers/number';
import withDrawer from '../../../withDrawer';
import styles from './BarChartStyles';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleSide: PropTypes.string,
    moduleColor: PropTypes.string,
    onClick: PropTypes.func
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleSide: 'left',
    moduleColor: '#666',
    onClick: function onClick() {
        return {};
    }
};

var BarChart = function BarChart(_ref) {
    var moduleConfig = _ref.moduleConfig,
        moduleData = _ref.moduleData,
        moduleSide = _ref.moduleSide,
        moduleColor = _ref.moduleColor,
        onClick = _ref.onClick;

    var refExport = useRef(null);
    var getPercentage = function getPercentage(n) {
        return (n * 100 / moduleData.total).toFixed(1).replace(/\.0$/, '');
    };
    var valores = moduleData.values;
    var filterSentiment = function filterSentiment(item, value) {
        return item.key === value;
    };
    var iN = moduleData.values.findIndex(function (it) {
        return filterSentiment(it, 'neutral');
    });
    var iP = moduleData.values.findIndex(function (it) {
        return filterSentiment(it, 'positivo');
    });
    var iNeg = moduleData.values.findIndex(function (it) {
        return filterSentiment(it, 'negativo');
    });
    var titleConfig = function titleConfig(sentiment) {
        return {
            title: moduleConfig.title || 'Tweets en el tiempo',
            subtitleLeft: sentiment ? 'Tweets con sentimiento ' + sentiment : '',
            subtitleRight: ''
        };
    };
    var onRealClick = moduleConfig.hasClick ? onClick : function () {
        return {};
    };
    var renderList = function renderList() {
        return React.createElement(
            'div',
            { className: 'bar-chart__header' },
            React.createElement(
                'div',
                {
                    className: 'padding',
                    onClick: function onClick() {
                        return onRealClick({ filters: { sentiment: 'positivo' } }, titleConfig('positivo'));
                    }
                },
                React.createElement(
                    'div',
                    null,
                    React.createElement(
                        'p',
                        null,
                        'Positivo'
                    ),
                    React.createElement(
                        'div',
                        { className: 'bar-chart__porcentage' },
                        React.createElement(
                            'div',
                            { className: 'bar__chart__chart' },
                            React.createElement('div', {
                                style: { width: (valores[iP] ? Math.trunc(getPercentage(valores[iP].doc_count)) : 0) + '%' },
                                className: 'bar__chart__chart__positive'
                            })
                        ),
                        React.createElement(
                            'div',
                            { className: 'bar-chart__percentages' },
                            React.createElement(
                                'p',
                                { className: 'bar-chart__percentages__positive' },
                                (valores[iP] ? getPercentage(valores[iP].doc_count) : 0) + '%'
                            )
                        )
                    )
                )
            ),
            React.createElement(
                'div',
                {
                    className: 'padding',
                    onClick: function onClick() {
                        return onRealClick({ filters: { sentiment: 'neutral' } }, titleConfig('neutral'));
                    }
                },
                React.createElement(
                    'div',
                    { className: 'neutral' },
                    React.createElement(
                        'p',
                        null,
                        'Neutral'
                    ),
                    React.createElement(
                        'div',
                        { className: 'bar-chart__porcentage' },
                        React.createElement(
                            'div',
                            { className: 'bar__chart__chart' },
                            React.createElement('div', {
                                style: { width: (valores[iN] ? Math.trunc(getPercentage(valores[iN].doc_count)) : 0) + '%' },
                                className: 'bar__chart__chart__neutral'
                            })
                        ),
                        React.createElement(
                            'div',
                            { className: 'bar-chart__percentages' },
                            React.createElement(
                                'p',
                                { className: 'bar-chart__percentages__neutral' },
                                (valores[iN] ? getPercentage(valores[iN].doc_count) : 0) + '%'
                            )
                        )
                    )
                )
            ),
            React.createElement(
                'div',
                {
                    className: 'padding',
                    onClick: function onClick() {
                        return onRealClick({ filters: { sentiment: 'negativo' } }, titleConfig('negativo'));
                    }
                },
                React.createElement(
                    'div',
                    null,
                    React.createElement(
                        'p',
                        null,
                        'Negativo'
                    ),
                    React.createElement(
                        'div',
                        { className: 'bar-chart__porcentage' },
                        React.createElement(
                            'div',
                            { className: 'bar__chart__chart' },
                            React.createElement('div', {
                                style: { width: (valores[iNeg] ? Math.trunc(getPercentage(valores[iNeg].doc_count)) : 0) + '%' },
                                className: 'bar__chart__chart__negative'
                            })
                        ),
                        React.createElement(
                            'div',
                            { className: 'bar-chart__percentages' },
                            React.createElement(
                                'p',
                                { className: 'bar-chart__percentages__negative' },
                                (valores[iNeg] ? getPercentage(valores[iNeg].doc_count) : 0) + '%'
                            )
                        )
                    )
                )
            )
        );
    };

    var _moduleConfig$title = moduleConfig.title,
        title = _moduleConfig$title === undefined ? 'tuits totales' : _moduleConfig$title,
        buttonExport = moduleConfig.buttonExport;

    return React.createElement(
        'div',
        Object.assign({
            ref: refExport
        }, styles(moduleSide, moduleColor, moduleConfig.hasClick)),
        React.createElement(
            'div',
            { className: 'bar-chart__label' },
            React.createElement(
                'h3',
                null,
                ' ',
                title,
                ' '
            )
        ),
        React.createElement(
            'div',
            { className: 'bar-chart__content' },
            React.createElement(
                'div',
                {
                    className: 'bar-chart__total-mentions',
                    onClick: function onClick() {
                        return onRealClick({}, titleConfig());
                    }
                },
                React.createElement(
                    'div',
                    { className: 'bar-chart__total-mentions--header' },
                    React.createElement(DialogIcon, { color: moduleColor }),
                    React.createElement(
                        'div',
                        { className: 'bar-chart__total-mentions--h' },
                        numberWithCommas(moduleData.total)
                    )
                ),
                React.createElement(
                    'p',
                    null,
                    ' menciones totales '
                )
            ),
            React.createElement(
                'div',
                null,
                renderList()
            )
        )
    );
};

BarChart.propTypes = propTypes;
BarChart.defaultProps = defaultProps;

export default withDrawer(BarChart);