import React from 'react';
import PropTypes from 'prop-types';
import { PieChart, Pie, Cell } from 'recharts';

import { numberWithCommas } from '../../../../helpers/number';
import TwitterIcon from '../../../../helpers/icons/twitterIcon';
import styles from './PieChartRemasterStyles';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleSide: PropTypes.string,
    moduleColor: PropTypes.string
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleSide: 'left',
    moduleColor: '#666'
};

var PieChartRemaster = function PieChartRemaster(_ref) {
    var moduleConfig = _ref.moduleConfig,
        moduleData = _ref.moduleData,
        moduleSide = _ref.moduleSide,
        moduleColor = _ref.moduleColor;

    var colors = {
        positivo: '#63d12f',
        negativo: '#ea3c3c',
        neutral: '#89a7ce'
    };

    var formatLabels = moduleData.values.map(function (item) {
        if (moduleConfig.sentiment_labels.hasOwnProperty(item.key)) {
            return {
                key: item.key,
                name: moduleConfig.sentiment_labels[item.key],
                value: item.doc_count,
                percentage: (item.doc_count * 100 / moduleData.total).toFixed(1).replace(/\.0$/, '')
            };
        }
        return {
            key: item.key,
            name: item.key.charAt(0).toUpperCase() + item.key.slice(1),
            value: item.doc_count,
            percentage: (item.doc_count * 100 / moduleData.total).toFixed(1).replace(/\.0$/, '')
        };
    });

    var renderLabels = function renderLabels() {
        return formatLabels.map(function (type, index) {
            return React.createElement(
                'div',
                { className: 'pie-chart__percentages__' + type.key, key: index },
                React.createElement(
                    'div',
                    { className: 'pie-chart__percentages__group' },
                    React.createElement('div', { className: 'pie-chart__percentages__indicator' }),
                    React.createElement(
                        'span',
                        null,
                        type.percentage,
                        '%'
                    )
                ),
                React.createElement(
                    'div',
                    { className: 'pie-chart__percentages__group' },
                    React.createElement(
                        'div',
                        { className: 'pie-chart__percentages__twitter' },
                        React.createElement(TwitterIcon, { color: '#4990e2' })
                    ),
                    React.createElement(
                        'span',
                        null,
                        numberWithCommas(type.value)
                    )
                )
            );
        });
    };

    var renderCustomizedLabel = function renderCustomizedLabel(_ref2) {
        var cx = _ref2.cx,
            cy = _ref2.cy,
            midAngle = _ref2.midAngle,
            innerRadius = _ref2.innerRadius,
            outerRadius = _ref2.outerRadius,
            percent = _ref2.percent,
            index = _ref2.index;

        var RADIAN = Math.PI / 180;
        var radius = innerRadius + (outerRadius - innerRadius) * 0.2;
        var x = cx + radius * Math.cos(-midAngle * RADIAN);
        var y = cy + radius * Math.sin(-midAngle * RADIAN);
        return React.createElement(
            'text',
            {
                x: x,
                y: y,
                fill: 'white',
                fontWeight: '100',
                fontSize: '10',
                textAnchor: x > cx ? 'start' : 'end',
                dominantBaseline: 'central'
            },
            (percent * 100).toFixed(1).replace(/\.0$/, '') + '%'
        );
    };

    var _moduleConfig$title = moduleConfig.title,
        title = _moduleConfig$title === undefined ? 'SENTIMIENTO' : _moduleConfig$title;

    return React.createElement(
        'div',
        Object.assign({ className: 'pie-chart' }, styles(moduleSide, moduleColor)),
        React.createElement(
            'div',
            { className: 'pie-chart__label' },
            React.createElement(
                'h3',
                null,
                title
            )
        ),
        React.createElement(
            'div',
            { className: 'pie-chart__content' },
            React.createElement(
                'div',
                { className: 'pie-chart__percentages' },
                renderLabels()
            ),
            React.createElement(
                'div',
                { className: 'pie-chart__graphic' },
                React.createElement(
                    PieChart,
                    { width: 129, height: 129 },
                    React.createElement(
                        Pie,
                        {
                            innerRadius: 35,
                            outerRadius: 65,
                            labelLine: false,
                            data: formatLabels,
                            label: renderCustomizedLabel
                        },
                        formatLabels.map(function (entry, index) {
                            return React.createElement(Cell, { key: index, fill: colors['' + entry.key] });
                        })
                    )
                )
            )
        ),
        React.createElement(
            'div',
            { className: 'pie-chart__footer' },
            React.createElement(
                'h5',
                null,
                'Positivo'
            ),
            React.createElement(
                'h5',
                null,
                'Neutral'
            ),
            React.createElement(
                'h5',
                null,
                'Negativo'
            ),
            React.createElement(
                'div',
                { className: 'pie-chart__footer__twitter' },
                React.createElement(TwitterIcon, { color: '#4990e2' }),
                React.createElement(
                    'h5',
                    null,
                    'Tweets'
                )
            )
        )
    );
};

PieChartRemaster.propTypes = propTypes;
PieChartRemaster.defaultProps = defaultProps;

export default PieChartRemaster;