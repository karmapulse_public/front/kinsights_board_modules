import { css } from 'glamor';

var styles = function styles(side, color, hasClick) {
    var bgColor = 'transparent';
    return css({
        backgroundColor: bgColor,
        color: 'rgba(0,0,0, 0.5)',

        ' .sentiment-inline__title': {
            '> h3': {
                textAlign: side,
                padding: '15px 20px',
                textTransform: 'uppercase',
                fontSize: 16,
                fontWeight: 400,
                color: color
            }
        },
        ' .sentiment-inline__percentages': {
            position: 'relative',
            display: 'flex',
            justifyContent: 'space-around',
            alignItems: 'center',
            textTransform: 'capitalize',
            ' > div': {
                order: 3,
                cursor: hasClick ? 'pointer' : 'default',
                ' p': {
                    lineHeight: 'normal'
                },
                '.positivo': {
                    order: 1,
                    marginRight: '20px'
                },
                '.neutral': {
                    order: 2,
                    marginRight: '20px'
                }
            }
        }
    });
};

export default styles;