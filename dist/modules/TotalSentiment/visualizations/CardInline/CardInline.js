import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import Typography from 'kpulse_design_system/dist/Typography';
import withDrawer from '../../../withDrawer';

import styles from './cardInlineStyles';

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleSide: PropTypes.string,
    moduleColor: PropTypes.string,
    onClick: PropTypes.func
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleSide: 'left',
    moduleColor: '#666',
    onClick: function onClick() {
        return {};
    }
};

var Paragraph = Typography.Paragraph;


var CardInline = function CardInline(_ref) {
    var moduleConfig = _ref.moduleConfig,
        moduleData = _ref.moduleData,
        moduleSide = _ref.moduleSide,
        moduleColor = _ref.moduleColor,
        onClick = _ref.onClick;

    var refExport = useRef(null);
    var colors = {
        positivo: '#63d12f',
        negativo: '#ea3c3c',
        neutral: '#89a7ce'
    };
    var label = {
        positivo: 'positivo',
        negativo: 'negativo',
        neutral: 'neutral'
    };
    var onRealClick = moduleConfig.hasClick ? onClick : function () {
        return {};
    };

    var titleConfig = function titleConfig(sentiment) {
        return {
            title: moduleConfig.title || 'Tweets en el tiempo',
            subtitleLeft: sentiment ? 'Tweets con sentimiento ' + label[sentiment] : '',
            subtitleRight: ''
        };
    };

    var formatLabels = Object.keys(colors).map(function (item) {
        var i = moduleData.values.filter(function (elto) {
            return elto.key === item;
        })[0];
        return {
            key: item,
            name: item.charAt(0).toUpperCase() + item.slice(1),
            percentage: i ? (i.doc_count * 100 / moduleData.total).toFixed(1).replace(/\.0$/, '') : 0
        };
    });

    var renderLabels = function renderLabels() {
        return formatLabels.map(function (type, index) {
            return React.createElement(
                'div',
                {
                    key: index,
                    className: type.key,
                    onClick: function onClick() {
                        return onRealClick({ filters: { sentiment: type.key } }, titleConfig(type.key));
                    }
                },
                React.createElement(
                    Paragraph,
                    { small: true, bold: true, color: colors[type.key] },
                    type.key
                ),
                React.createElement(
                    Paragraph,
                    { large: true },
                    type.percentage,
                    '%'
                )
            );
        });
    };

    var _moduleConfig$title = moduleConfig.title,
        title = _moduleConfig$title === undefined ? 'SENTIMIENTO' : _moduleConfig$title,
        hasClick = moduleConfig.hasClick,
        buttonExport = moduleConfig.buttonExport;

    return React.createElement(
        'div',
        Object.assign({
            ref: refExport,
            className: 'card-sentiment-inline'
        }, styles(moduleSide, moduleColor, hasClick)),
        React.createElement(
            'div',
            { className: 'sentiment-inline__title' },
            React.createElement(
                'h3',
                null,
                title
            )
        ),
        React.createElement(
            'div',
            {
                className: 'sentiment-inline__percentages'
            },
            renderLabels()
        )
    );
};

CardInline.propTypes = propTypes;
CardInline.defaultProps = defaultProps;

export default withDrawer(CardInline);