import { Observable } from 'rxjs';
import { fetchXHR } from '../../helpers/http';

export default (function (url, params) {
    return Observable.create(function (observer) {
        Observable.fromPromise(fetchXHR(url, 'GET', params)).subscribe(function (data) {
            var buckets = data.json.response.aggregations.sentiment.buckets;
            var sum = buckets.reduce(function (acc, val) {
                return acc + val.doc_count;
            }, 0);

            observer.next({
                total: sum,
                values: buckets
            });
        }, function (error) {
            observer.error(error);
        });
    });
});