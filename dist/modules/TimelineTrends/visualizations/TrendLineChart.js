import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import styles from './trendLineChartStyles';
import { numberWithCommas } from '../../../helpers/number';
import Lines from '../../Charts/ChartsLines';
import locationIcon from '../../../helpers/icons/locationIcon';
import NoDataModule from '../../NoDataModule';
import withDrawer from '../../withDrawer';
import { labelType } from '../../../helpers/paddingDates';

var colors = [{ r: 199, g: 89, b: 147 }, { r: 129, g: 117, b: 204 }, { r: 204, g: 95, b: 66 }, { r: 91, g: 169, b: 101 }, { r: 84, g: 161, b: 211 }];

var propTypes = {
    moduleConfig: PropTypes.object,
    moduleData: PropTypes.object,
    moduleColor: PropTypes.string
};

var defaultProps = {
    moduleConfig: {},
    moduleData: {},
    moduleColor: '#666'
};

var TrendLineChart = function TrendLineChart(props) {
    var refExport = useRef(null);
    var moduleConfig = props.moduleConfig,
        moduleData = props.moduleData,
        moduleColor = props.moduleColor,
        onClick = props.onClick,
        timeInterval = props.timeInterval;
    var _moduleConfig$title = moduleConfig.title,
        title = _moduleConfig$title === undefined ? 'TEMAS EN EL TIEMPO' : _moduleConfig$title,
        hasClick = moduleConfig.hasClick;
    var _moduleConfig$locatio = moduleConfig.location,
        location = _moduleConfig$locatio === undefined ? 'LOCACIÓN' : _moduleConfig$locatio;
    var entity_type = moduleConfig.entity_type,
        buttonExport = moduleConfig.buttonExport;

    var onDotClick = function onDotClick(a) {
        var startDate = moment(a.payload.key).utc();
        var finishDate = moment(a.payload.key).add(1, timeInterval).utc();
        // subtitleLeft = `${data.dataKey} del ${labelType(startDate, finishDate, intervalType)}`;
        onClick({
            'initial-date': startDate.format(),
            'final-date': finishDate.format(),
            filters: {
                phrase: a.dataKey
            }
        }, {
            title: title,
            subtitleLeft: a.dataKey + ' del ' + labelType(startDate, finishDate, timeInterval),
            subtitleRight: ''
        });
    };
    var onLegendClick = function onLegendClick(word) {
        onClick({
            filters: {
                phrase: word
            }
        }, {
            title: title,
            subtitleLeft: word,
            subtitleRight: ''
        });
    };
    return React.createElement(
        'div',
        Object.assign({
            ref: refExport,
            className: 'trend-line-chart'
        }, styles(moduleColor, hasClick)),
        React.createElement(
            'div',
            { className: 'trend__label' },
            React.createElement(
                'h3',
                null,
                title
            )
        ),
        moduleData.values.aggregations[entity_type].buckets.reduce(function (prev, item) {
            return prev += item.doc_count;
        }, 0) === 0 ? React.createElement(
            'div',
            { className: 'noData' },
            React.createElement(NoDataModule, { color: moduleColor })
        ) : React.createElement(
            'div',
            { className: 'trend__chart' },
            React.createElement(Lines, {
                data: moduleData.values,
                type: 'multiple',
                colors: colors,
                entity: moduleConfig.entity_type,
                intervalType: timeInterval,
                onLineClick: onDotClick,
                onLegendClick: onLegendClick,
                hasClick: hasClick
            })
        )
    );
};

TrendLineChart.propTypes = propTypes;
TrendLineChart.defaultProps = defaultProps;

export default withDrawer(TrendLineChart);