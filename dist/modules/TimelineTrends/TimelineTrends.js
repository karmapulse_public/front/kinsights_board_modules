var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import isNull from 'lodash/isNull';

import fetchTimelineTrends from './TimelineTrendsDataFlow';
import { calculateNewInterval } from '../../helpers/getTimeInterval';
import LoadingModule from '../LoadingModule';
import TrendLineChart from './visualizations/TrendLineChart';

var propTypes = {
    fields: PropTypes.object,
    side: PropTypes.string,
    color: PropTypes.string,
    dateRange: PropTypes.object,
    services: PropTypes.object
};

var defaultProps = {
    fields: {},
    side: 'left',
    color: '#666',
    dateRange: {},
    services: {}
};

var TimelineTrends = function (_Component) {
    _inherits(TimelineTrends, _Component);

    function TimelineTrends(props) {
        _classCallCheck(this, TimelineTrends);

        var _this = _possibleConstructorReturn(this, (TimelineTrends.__proto__ || Object.getPrototypeOf(TimelineTrends)).call(this, props));

        _this.myRef = React.createRef();
        _this.state = {
            moduleConfig: props.fields,
            moduleData: null,
            moduleErrorData: null,
            moduleLoading: true,
            moduleSide: props.side,
            moduleColor: props.color,
            moduleRange: props.dateRange,
            moduleServices: props.services
        };
        return _this;
    }

    _createClass(TimelineTrends, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            this.updateDataFlow(this.props);
        }
    }, {
        key: 'componentDidUpdate',
        value: function componentDidUpdate(prevProps) {
            var _props$dateRange = this.props.dateRange,
                startDate = _props$dateRange.startDate,
                endDate = _props$dateRange.endDate;
            var _props$fields = this.props.fields,
                hasClick = _props$fields.hasClick,
                search_id = _props$fields.search_id;

            if (startDate !== prevProps.dateRange.startDate || endDate !== prevProps.dateRange.endDate || search_id !== prevProps.fields.search_id) {
                this.updateDataFlow(this.props);
            }
            if (hasClick !== prevProps.fields.hasClick) {
                this.changeState(hasClick);
            }
        }
    }, {
        key: 'changeState',
        value: function changeState(hasClick) {
            this.setState({
                moduleConfig: Object.assign({}, this.state.moduleConfig, {
                    hasClick: hasClick
                })
            });
        }
    }, {
        key: 'updateDataFlow',
        value: function updateDataFlow(props) {
            var _this2 = this;

            var fields = props.fields,
                _props$dateRange2 = props.dateRange,
                startDate = _props$dateRange2.startDate,
                endDate = _props$dateRange2.endDate;


            var timeInterval = calculateNewInterval(startDate.startOf('day'), endDate.endOf('day'), this.myRef);

            var params = {
                recipe_id: 'module_tw_timeline_trends',
                rule_id: fields.search_id,
                initial_date: startDate.startOf('day').utc().toISOString(),
                final_date: endDate.endOf('day').utc().toISOString(),
                filters: {
                    entity: fields.entity_type,
                    query: fields.query_type,
                    words: fields.search_entities,
                    interval: timeInterval
                }
            };

            if (fields.query_type === 'search') {
                params.searchEntities = fields.search_entities.split(',');
            }

            fetchTimelineTrends(props.services.twitter, params).subscribe(function (data) {
                _this2.setState({
                    moduleData: data,
                    moduleLoading: false,
                    moduleColor: props.color,
                    timeInterval: timeInterval,
                    moduleConfig: props.fields
                });
            }, function (error) {
                console.log(error);
            }, null);
        }
    }, {
        key: 'renderByViz',
        value: function renderByViz() {
            var _state = this.state,
                moduleConfig = _state.moduleConfig,
                moduleData = _state.moduleData;

            if (!isNull(moduleData)) {
                var visualizations = {
                    trend_line_chart: function trend_line_chart(params) {
                        return React.createElement(TrendLineChart, params);
                    }
                };
                return visualizations[this.props.view || moduleConfig.visualization](this.state);
            }

            return React.createElement(LoadingModule, null);
        }
    }, {
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                { className: 'm-timeline-trends', ref: this.myRef },
                this.renderByViz()
            );
        }
    }]);

    return TimelineTrends;
}(Component);

TimelineTrends.propTypes = propTypes;
TimelineTrends.defaultProps = defaultProps;

export default TimelineTrends;