function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import { Observable } from 'rxjs';
import { fetchXHR } from '../../helpers/http';
import paddingDates from '../../helpers/paddingDates';

export default (function (url, params) {
    return Observable.create(function (observer) {
        Observable.fromPromise(fetchXHR(url, 'GET', params)).subscribe(function (data) {
            if (params.filters.query === 'search') {
                var formatData = params.filters.words.split(',').map(function (item, index) {
                    return _defineProperty({
                        key: item,
                        doc_count: data.json.response.aggregations.entity.buckets['' + index].doc_count
                    }, params.filters.entity + '_by_time', {
                        buckets: paddingDates(data.json.response.aggregations.entity.buckets['' + index].entity_by_time.buckets, params, 'key_as_string', 'doc_count')
                    });
                }).sort(function (a, b) {
                    return a.doc_count - b.doc_count;
                }).reverse();
                observer.next({
                    total: data.json.response.hits.total,
                    values: {
                        aggregations: _defineProperty({}, '' + params.filters.entity, {
                            buckets: formatData
                        })
                    }
                });
            } else {
                var _formatData = data.json.response.aggregations.entity.buckets.map(function (item, index) {
                    return _defineProperty({
                        key: item.key,
                        doc_count: item.doc_count
                    }, params.filters.entity + '_by_time', {
                        buckets: paddingDates(item.entity_by_time.buckets, params, 'key_as_string', 'doc_count')
                    });
                });
                observer.next({
                    total: data.json.response.hits.total,
                    values: {
                        aggregations: _defineProperty({}, '' + params.filters.entity, {
                            buckets: _formatData
                        })
                    }
                });
            }
        }, function (error) {
            observer.error(error);
        });
    });
});