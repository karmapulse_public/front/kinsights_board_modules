import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';

import Gallery from '../../../../helpers/customComponents/Gallery';
import Header from '../../../../helpers/customComponents/Header';
import Sentiment from '../../../../helpers/customComponents/Sentiment';
import BodyText from '../../../../helpers/customComponents/BodyText';
import Card from '../../../../helpers/customComponents/Card';
import Video from '../../../../helpers/customComponents/Video';
import Link from '../../../../helpers/customComponents/Link';

import styles from './styles';

var propTypes = {
    services: PropTypes.object,
    data: PropTypes.object,
    scroll: PropTypes.func
};

var defaultProps = {
    services: {},
    data: null,
    scroll: function scroll() {
        return {};
    }
};

var List = function List(props) {
    var refContainer = useRef();
    var prevScroll = useRef(null);
    var renderBody = function renderBody(item) {
        return React.createElement(
            React.Fragment,
            null,
            React.createElement(BodyText, { text: item.body, link: item, type: 'twitter' }),
            function () {
                if (item.media_type === 'image') {
                    return React.createElement(Gallery, {
                        items: item.images,
                        height: '300px',
                        type: 'twitter',
                        url: item.link
                    });
                }
                if (item.media_type === 'gif') {
                    return React.createElement(Video, {
                        sources: JSON.parse(item.gif.variants),
                        cover: item.gif.cover,
                        type: 'twitter',
                        gif: true
                    });
                }
                if (item.media_type === 'video') {
                    return React.createElement(Video, {
                        sources: JSON.parse(item.video.variants),
                        cover: item.video.cover,
                        type: 'twitter'
                    });
                }
                if (item.urls) {
                    return (
                        // eslint-disable-next-line jsx-a11y/anchor-is-valid
                        React.createElement(Link, {
                            url: item.urls[0].url,
                            title: item.urls[0].title,
                            text: item.urls[0].description,
                            type: 'twitter'
                        })
                    );
                }
                return null;
            }()
        );
    };

    var makeScroll = function makeScroll(e) {
        var div = e.target;
        var isUserScrollingDown = function isUserScrollingDown(position) {
            return prevScroll.current < position.scrollTop;
        };
        var isScrollExpectedPercent = function isScrollExpectedPercent(position, percent) {
            return (position.scrollTop + position.clientHeight) / position.scrollHeight > percent / 100;
        };
        if (isUserScrollingDown(div) && isScrollExpectedPercent(div, 95)) {
            props.scroll(props.data.page + 10);
        }

        prevScroll.current = div.scrollTop;
    };

    useEffect(function () {
        var container = refContainer.current;
        container.addEventListener('scroll', makeScroll);
        return function () {
            return container.removeEventListener('scroll', makeScroll);
        };
    }, [refContainer, prevScroll, props.scroll, props.data.page]);

    return React.createElement(
        'div',
        Object.assign({}, styles, { ref: refContainer, className: 'twitter-list__container' }),
        props.data.tweets.map(function (item) {
            return React.createElement(
                Card,
                { key: item._source.id },
                React.createElement(Header, { noSource: true, item: item._source, services: props.services }),
                renderBody(item._source),
                React.createElement(Sentiment, { item: item._source, noSource: true })
            );
        })
    );
};

List.propTypes = propTypes;
List.defaultProps = defaultProps;

export default List;