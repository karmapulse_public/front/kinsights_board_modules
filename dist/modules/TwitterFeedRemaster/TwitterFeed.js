var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import takeRight from 'lodash/takeRight';

import fetchTwitterFeed from './TwitterFeedDataFlow';

import LoadingModule from '../LoadingModule';
import FeedTwitter from './visualizations/List';

var propTypes = {
    fields: PropTypes.object,
    dateRange: PropTypes.object,
    services: PropTypes.object,
    filters: PropTypes.object
};

var defaultProps = {
    fields: {},
    dateRange: {},
    services: {},
    filters: {}
};

var TwitterFeedRemaster = function TwitterFeedRemaster(props) {
    var _useState = useState({
        data: null,
        loading: true,
        error: null
    }),
        _useState2 = _slicedToArray(_useState, 2),
        innerState = _useState2[0],
        setInnerState = _useState2[1];

    var fetchData = function fetchData() {
        var _props$dateRange = props.dateRange,
            startDate = _props$dateRange.startDate,
            endDate = _props$dateRange.endDate;

        var params = {
            recipe_id: 'module_tw_feed',
            rule_id: props.fields.search_id,
            initial_date: startDate.toISOString(),
            final_date: endDate.toISOString(),
            filters: Object.assign({}, props.filters)
        };

        fetchTwitterFeed(props.services.twitter, params).subscribe(function (data) {
            return setInnerState(function (prevValue) {
                return Object.assign({}, prevValue, {
                    data: Object.assign({}, data, {
                        page: 0
                    }),
                    loading: false
                });
            });
        }, function (error) {
            console.log(error);
        }, null);
    };

    useEffect(function () {
        fetchData();
    }, [setInnerState]);

    var infiniteScrollHandler = function infiniteScrollHandler(page) {
        var lastTweet = takeRight(innerState.data.tweets);
        var _props$dateRange2 = props.dateRange,
            startDate = _props$dateRange2.startDate,
            endDate = _props$dateRange2.endDate;


        var params = {
            recipe_id: 'module_tw_feed',
            rule_id: props.fields.search_id,
            initial_date: startDate.toISOString(),
            final_date: endDate.toISOString(),
            search_after: lastTweet[0].sort,
            filters: Object.assign({}, props.filters)
        };

        fetchTwitterFeed(props.services.twitter, params, page).subscribe(function (data) {
            var newTweets = innerState.data.tweets.concat(data.tweets);
            setInnerState({
                data: {
                    tweets: newTweets,
                    page: page
                },
                loading: false,
                error: false
            });
        }, function (error) {
            console.log(error);
        }, null);
    };

    var renderByViz = function renderByViz() {
        var data = innerState.data;


        if (data) {
            var visualizations = {
                list_tweets: function list_tweets(params) {
                    return React.createElement(FeedTwitter, Object.assign({}, params, {
                        scroll: infiniteScrollHandler
                    }));
                }
            };
            return visualizations[props.fields.visualization](Object.assign({}, innerState, { services: props.services }));
        }

        return React.createElement(LoadingModule, null);
    };

    return React.createElement(
        'div',
        { className: 'm-twitter-feed' },
        renderByViz()
    );
};

TwitterFeedRemaster.propTypes = propTypes;
TwitterFeedRemaster.defaultProps = defaultProps;

export default TwitterFeedRemaster;