import React from 'react';
import find from 'lodash/find';
import UnassignedModule from './UnassignedModule';
import ModuleNotAvailable from './ModuleNotAvailable';
import uID from './helpers/uniqueId';
import withExport from './helpers/withExportPDF';

import TotalTweets from './modules/TotalTweets';
import TotalSentiment from './modules/TotalSentiment';
import TotalSentimentAuthor from './modules/TotalSentimentAuthor';
import TopHashtags from './modules/TopHashtags';
import TopUsers from './modules/TopUsers';
import TotalAudience from './modules/TotalAudience';
import TopImages from './modules/TopImages';
import ProfileSelector from './modules/ProfileSelector';
import TimelineTrends from './modules/TimelineTrends';
import TopTrends from './modules/TopTrends';
import TopTrendsNew from './modules/TopTrendsNew';
import TreeMapTrends from './modules/TreeMapTrends';
import Mapa from './modules/Mapa';
import TwitterFeed from './modules/TwitterFeed';
import SingleImage from './modules/SingleImage';
import MarkdownText from './modules/MarkdownText';
import FbPostFrequency from './modules/FbPostFrequency';
import FbPerformanceByType from './modules/FbPerformanceByType';
import FbTopEntities from './modules/FbTopEntities';
import FbTopPosts from './modules/FbTopPosts';
import FbFans from './modules/FbFans';
import FbProfile from './modules/FbProfile';
import FbSentiment from './modules/FbSentiment';
import FbInteractions from './modules/FbInteractions';
import FbSearchKeywords from './modules/FbSearchKeywords';
import TopTweets from './modules/TopTweets';
import TopInteraction from './modules/TopInteraction';
import TopOriginalInteraction from './modules/TopOriginalInteraction';
import TopicSight from './modules/TopicSight';
import TimeTrendGrouper from './modules/TimeTrendGrouper';
import IgPostFrequency from './modules/IgPostFrequency';
import IgTotalInteraction from './modules/IgTotalInteraction';
import IgTopImages from './modules/IgTopImages';
import TopTrendingTopics from './modules/TopTrendingTopics';
import TimelineDistribution from './modules/TimelineDistribution';
import GeneralFeed from './modules/GeneralFeed';
import TwitterFeedRemaster from './modules/TwitterFeedRemaster';
import FbTopTopicsComments from './modules/FbTopTopicsComments';
import MultiSourceTimeline from './modules/MultiSourceTimeline';

// Map modules by module type
export default (function (components) {
    var modulesAvailable = {
        control_profile_selector: function control_profile_selector(moduleConfig) {
            return {
                component: ProfileSelector,
                moduleConfig: moduleConfig
            };
        },
        module_total_tweets: function module_total_tweets(moduleConfig) {
            return {
                component: TotalTweets,
                moduleConfig: moduleConfig
            };
        },
        module_total_sentiment: function module_total_sentiment(moduleConfig) {
            return {
                component: TotalSentiment,
                moduleConfig: moduleConfig
            };
        },
        module_total_sentiment_author: function module_total_sentiment_author(moduleConfig) {
            return {
                component: TotalSentimentAuthor,
                moduleConfig: moduleConfig
            };
        },
        module_hashtags: function module_hashtags(moduleConfig) {
            return {
                component: TopHashtags,
                moduleConfig: moduleConfig
            };
        },
        module_total_audience: function module_total_audience(moduleConfig) {
            return {
                component: TotalAudience,
                moduleConfig: moduleConfig
            };
        },
        module_shared_content: function module_shared_content(moduleConfig) {
            return {
                component: TopImages,
                moduleConfig: moduleConfig
            };
        },
        module_timeline_trends: function module_timeline_trends(moduleConfig) {
            return {
                component: TimelineTrends,
                moduleConfig: moduleConfig
            };
        },
        module_top_trends: function module_top_trends(moduleConfig) {
            return {
                component: TopTrends,
                moduleConfig: moduleConfig
            };
        },
        module_top_user: function module_top_user(moduleConfig) {
            return {
                component: TopUsers,
                moduleConfig: moduleConfig
            };
        },
        module_top_trends_new: function module_top_trends_new(moduleConfig) {
            return {
                component: TopTrendsNew,
                moduleConfig: moduleConfig
            };
        },
        module_twitter_feed: function module_twitter_feed(moduleConfig) {
            return {
                component: TwitterFeed,
                moduleConfig: moduleConfig
            };
        },
        module_single_image: function module_single_image(moduleConfig) {
            return {
                component: SingleImage,
                moduleConfig: moduleConfig
            };
        },
        module_markdown: function module_markdown(moduleConfig) {
            return {
                component: MarkdownText,
                moduleConfig: moduleConfig
            };
        },
        module_fb_post_freq: function module_fb_post_freq(moduleConfig) {
            return {
                component: FbPostFrequency,
                moduleConfig: moduleConfig
            };
        },
        module_fb_performance_by_type: function module_fb_performance_by_type(moduleConfig) {
            return {
                component: FbPerformanceByType,
                moduleConfig: moduleConfig
            };
        },
        module_fb_top_entities: function module_fb_top_entities(moduleConfig) {
            return {
                component: FbTopEntities,
                moduleConfig: moduleConfig
            };
        },
        module_fb_top_posts: function module_fb_top_posts(moduleConfig) {
            return {
                component: FbTopPosts,
                moduleConfig: moduleConfig
            };
        },
        module_fb_fans: function module_fb_fans(moduleConfig) {
            return {
                component: FbFans,
                moduleConfig: moduleConfig
            };
        },
        module_fb_profile: function module_fb_profile(moduleConfig) {
            return {
                component: FbProfile,
                moduleConfig: moduleConfig
            };
        },
        module_fb_sentiment_comments: function module_fb_sentiment_comments(moduleConfig) {
            return {
                component: FbSentiment,
                moduleConfig: moduleConfig
            };
        },
        module_fb_total_interactions: function module_fb_total_interactions(moduleConfig) {
            return {
                component: FbInteractions,
                moduleConfig: moduleConfig
            };
        },
        mdulo_fb_search_keywords: function mdulo_fb_search_keywords(moduleConfig) {
            return {
                component: FbSearchKeywords,
                moduleConfig: moduleConfig
            };
        },
        module_total_interaction: function module_total_interaction(moduleConfig) {
            return {
                component: TopInteraction,
                moduleConfig: moduleConfig
            };
        },
        module_total_interaction_r: function module_total_interaction_r(moduleConfig) {
            return {
                component: TopOriginalInteraction,
                moduleConfig: moduleConfig
            };
        },
        module_treemap_trends: function module_treemap_trends(moduleConfig) {
            return {
                component: TreeMapTrends,
                moduleConfig: moduleConfig
            };
        },
        module_map: function module_map(moduleConfig) {
            return {
                component: Mapa,
                moduleConfig: moduleConfig
            };
        },
        module_topic_sight: function module_topic_sight(moduleConfig) {
            return {
                component: TopicSight,
                moduleConfig: moduleConfig
            };
        },
        module_topic_grouper: function module_topic_grouper(moduleConfig) {
            return {
                component: TimeTrendGrouper,
                moduleConfig: moduleConfig
            };
        },
        module_ig_post_freq: function module_ig_post_freq(moduleConfig) {
            return {
                component: IgPostFrequency,
                moduleConfig: moduleConfig
            };
        },
        module_ig_total_interaction: function module_ig_total_interaction(moduleConfig) {
            return {
                component: IgTotalInteraction,
                moduleConfig: moduleConfig
            };
        },
        module_ig_top_posts: function module_ig_top_posts(moduleConfig) {
            return {
                component: IgTopImages,
                moduleConfig: moduleConfig
            };
        },
        top_trending_topics: function top_trending_topics(moduleConfig) {
            return {
                component: TopTrendingTopics,
                moduleConfig: moduleConfig
            };
        },
        module_timeline_distribution: function module_timeline_distribution(moduleConfig) {
            return {
                component: TimelineDistribution,
                moduleConfig: moduleConfig
            };
        },
        module_general_feed: function module_general_feed(moduleConfig) {
            return {
                component: GeneralFeed,
                moduleConfig: moduleConfig
            };
        },
        module_twitter_feed_remaster: function module_twitter_feed_remaster(moduleConfig) {
            return {
                component: TwitterFeedRemaster,
                moduleConfig: moduleConfig
            };
        },
        module_fb_tt_comments: function module_fb_tt_comments(moduleConfig) {
            return {
                component: FbTopTopicsComments,
                moduleConfig: moduleConfig
            };
        },
        module_multisource_timeline: function module_multisource_timeline(moduleConfig) {
            return {
                component: MultiSourceTimeline,
                moduleConfig: moduleConfig
            };
        },
        module_top_tweets: function module_top_tweets(moduleConfig) {
            return {
                component: TopTweets,
                moduleConfig: moduleConfig
            };
        }
    };

    var componentsMapped = components.map(function (component, index) {
        var componentType = component.sys ? component.sys.contentType.sys.id : null;

        if (componentType === null) {
            return {
                position: index,
                component: UnassignedModule
            };
        } else if (modulesAvailable[componentType]) {
            return {
                position: component.fields.position || index,
                component: modulesAvailable[componentType](component)
            };
        }
        return {
            position: component.position,
            component: ModuleNotAvailable
        };
    });

    return componentsMapped;
});

// Render module in specific position
export var renderComponentByPosition = function renderComponentByPosition(mapModules, position) {
    var params = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

    var module = find(mapModules, {
        position: position
    });

    if (module.component.component) {
        return withExport(React.createElement(module.component.component, null), Object.assign({}, module.component.moduleConfig, params));
    }

    return React.createElement(UnassignedModule, null);
};

export var renderComponents = function renderComponents(mapModules) {
    var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    return mapModules.map(function (module) {
        if (module.component.component) {
            return withExport(React.createElement(module.component.component, null), Object.assign({
                key: uID()
            }, module.component.moduleConfig, params));
        }
        return React.createElement(UnassignedModule, null);
    });
};